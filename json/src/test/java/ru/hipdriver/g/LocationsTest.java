package ru.hipdriver.g;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.j.Event;
import ru.hipdriver.j.Location;

public class LocationsTest {

	@Test
	public void testPackUnpackGps() {
		Event e = new Event();
		Date time = new Date();
		e.setTime(time);
		Locations.packGpsLocation(e);
		e.setTime(null);
		Locations.unpackGpsLocation(e);
		assertEquals(time, e.getTime());
	}

	@Test
	public void testPackUnpackGsm() {
		Event e = new Event();
		Date time = new Date();
		e.setTime(time);
		Locations.packGsmLocation(e);
		e.setTime(null);
		Locations.unpackGsmLocation(e);
		assertEquals(time, e.getTime());
	}

	@Test
	public void testDistance() {
		float distance = Locations.getDistance(53267480, 50223031, 53267480, 50223031);
		System.out.println(distance);
		assertTrue(0f == distance);
		distance = Locations.getDistance(53267480, 50223031, 53267478, 50223029);
		System.out.println(distance);
	}
	
	@Test
	public void testCompressTrack() {
		Location location100 = new Location();
		location100.setAcc(100);
		Location location10 = new Location();
		location10.setAcc(10);
		Location location50 = new Location();
		location50.setAcc(50);
		Location location30 = new Location();
		location30.setAcc(30);
		List<ICarLocation> locations = new ArrayList<ICarLocation>();
		locations.add(location100);
		locations.add(location10);
		locations.add(location50);
		locations.add(location30);
		
		List<ICarLocation> result = (List<ICarLocation>) Locations.compressTrack(locations, 3);
		Assert.assertNotNull(result);
		
		List<ICarLocation> expected = new ArrayList<ICarLocation>();
		expected.add(location10);
		expected.add(location50);
		expected.add(location30);
		
		Assert.assertTrue(Arrays.equals(expected.toArray(), result.toArray()));
	}

}
