package ru.hipdriver.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class ChatMsgHarvestTest {

	@Test
	public void testFrom() {
		String msg = ChatMsgComposer.makeMessage(7, 18, 7857381406968295716L, "Yep!");
		long mobileAgentId = ChatMsgParser.getMobileAgentIdFrom(msg);
		assertEquals(7L, mobileAgentId);
	}

	@Test
	public void testTo() {
		String msg = ChatMsgComposer.makeMessage(7, 18, 7857381406968295716L, "No!");
		long mobileAgentId = ChatMsgParser.getMobileAgentIdTo(msg);
		assertEquals(18L, mobileAgentId);
	}

	@Test
	public void testComponentSessions() {
		String msg = ChatMsgComposer.makeMessage(7, 18, 7857381406968295716L, "Distinguished!");
		long componentSessionsId = ChatMsgParser.getComponentSessionsId(msg);
		assertEquals(7857381406968295716L, componentSessionsId);
	}

	@Test
	public void testBody() {
		String msg = ChatMsgComposer.makeMessage(7, 18, 7857381406968295716L, "Sometime!");
		String body = ChatMsgParser.getBody(msg);
		assertEquals("Sometime!\n", body);
	}

}
