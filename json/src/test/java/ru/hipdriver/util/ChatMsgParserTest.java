package ru.hipdriver.util;

import static org.junit.Assert.*;

import org.junit.Test;

import ru.hipdriver.i.ISid;

public class ChatMsgParserTest {

	public static final String FIXTURE1_BODY = "Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.";
	public static final String FIXTURE1 = "O4vh2jytdFc=[aisFE+OR78c=]" + FIXTURE1_BODY;
	public static final String FIXTURE2_BODY = "Knowledge is power";
	public static final String FIXTURE2 = "O4vh2jytdFc=[aisFE+OR78c=](Zsyyd1uzkIw=)" + FIXTURE2_BODY;
	
	@Test
	public void testFrom() {
		long mobileAgentId = ChatMsgParser.getMobileAgentIdFrom(FIXTURE1);
		assertEquals(7L, mobileAgentId);
	}

	@Test
	public void testTo() {
		long mobileAgentId = ChatMsgParser.getMobileAgentIdTo(FIXTURE1);
		assertEquals(18L, mobileAgentId);
	}
	
	@Test
	public void testComponentSessions() {
		long componentSessionsId = ChatMsgParser.getComponentSessionsId(FIXTURE1);
		assertEquals(ISid.INVALID_COMPONENT_SESSIONS_ID, componentSessionsId);
	}

	@Test
	public void testBody() {
		String body = ChatMsgParser.getBody(FIXTURE1);
		assertEquals(FIXTURE1_BODY, body);
	}

	@Test
	public void testFrom2() {
		long mobileAgentId = ChatMsgParser.getMobileAgentIdFrom(FIXTURE2);
		assertEquals(7L, mobileAgentId);
	}

	@Test
	public void testTo2() {
		long mobileAgentId = ChatMsgParser.getMobileAgentIdTo(FIXTURE2);
		assertEquals(18L, mobileAgentId);
	}

	@Test
	public void testComponentSessions2() {
		long componentSessionsId = ChatMsgParser.getComponentSessionsId(FIXTURE2);
		assertEquals(7857381406968295716L, componentSessionsId);
	}

	@Test
	public void testBody2() {
		String body = ChatMsgParser.getBody(FIXTURE2);
		assertEquals(FIXTURE2_BODY, body);
	}

}
