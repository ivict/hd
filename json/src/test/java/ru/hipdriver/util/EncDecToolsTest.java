package ru.hipdriver.util;

import static org.junit.Assert.*;

import java.nio.charset.Charset;
import java.util.Arrays;

import org.codehaus.jackson.Base64Variants;
import org.junit.Test;

import ru.hipdriver.i.ISid;
import ru.hipdriver.util.EncDecTools;

public class EncDecToolsTest {

	@Test
	public void mobileAgentIdTest() {
		String val = EncDecTools.encodeMobileAgentId(7, ISid.SALTMA);
		System.out.printf("7 -> %s", val).println();
		long mobileAgentId = EncDecTools.decodeMobileAgentId(val, ISid.SALTMA);
		assertEquals(7L, mobileAgentId);
		
		System.out.printf("MA: 18 -> %s", EncDecTools.encodeMobileAgentId(18, ISid.SALTMA)).println();
		System.out.printf("SS: 18 -> %s", EncDecTools.encodeMobileAgentId(18, ISid.SALTSS)).println();
		System.out.printf("MA: 7 -> %s", EncDecTools.encodeMobileAgentId(7, ISid.SALTMA)).println();
		System.out.printf("SS: 7 -> %s", EncDecTools.encodeMobileAgentId(7, ISid.SALTSS)).println();
		
		System.out.printf("SC: 7857381406968295716 -> %s", EncDecTools.encodeMobileAgentId(7857381406968295716L, ISid.SALTCS)).println();
	}

	@Test
	public void encDecPasswordHash() {
		byte[] passwordHash = {-1, 0, 2, 3, 4, 5, 6, 7};
		byte[] encodedHash = EncDecTools.encodePasswordHash(passwordHash);
		byte[] decodedHash = EncDecTools.decodePasswordHash(encodedHash);
		assertTrue(Arrays.equals(passwordHash, decodedHash));

		byte[] passwordHash2 = {-1, 0, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0};
		encodedHash = EncDecTools.encodePasswordHash(passwordHash2);
		decodedHash = EncDecTools.decodePasswordHash(encodedHash);
		assertTrue(Arrays.equals(passwordHash2, decodedHash));
	}
	
	@Test
	public void encDecPasswordHashWithStringTransport() throws Throwable {
		byte[] passwordHash = {-1, 0, 2, 3, 4, 5, 6, 7};
		byte[] encodedHash = EncDecTools.encodePasswordHash(passwordHash);
		String str1 = Base64Variants.MIME_NO_LINEFEEDS.encode(encodedHash);
		byte[] encodedHash2 = EncDecTools.getBinaryValue(Base64Variants.MIME_NO_LINEFEEDS, str1);
		byte[] decodedHash = EncDecTools.decodePasswordHash(encodedHash2);
		assertTrue(Arrays.equals(passwordHash, decodedHash));

		byte[] passwordHash2 = {-1, 0, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1, 0};
		encodedHash = EncDecTools.encodePasswordHash(passwordHash2);
		str1 = Base64Variants.MIME_NO_LINEFEEDS.encode(encodedHash);
		encodedHash2 = EncDecTools.getBinaryValue(Base64Variants.MIME_NO_LINEFEEDS, str1);
		decodedHash = EncDecTools.decodePasswordHash(encodedHash2);
		assertTrue(Arrays.equals(passwordHash2, decodedHash));
	}
	
	@Test
	public void stupidTest() {
		long a = 3;
		long b = Math.round((float) a * 1.2f);
		System.out.printf("A is %s, b is %s", a, b);
		assertNotEquals(a, b);
	}
	
}
