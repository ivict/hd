package ru.hipdriver.d;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;

import org.junit.Test;

public class CBufferTest {
	
	TimestampSeeker timestampSeeker = new TimestampSeeker() {

		@Override
		public long getTimestamp(ByteBuffer byteBuffer) {
			return byteBuffer.getLong();
		}
		
	};

	@Test
	public void testOneZeroValue() {
		byte[] b = new byte[8];
		
		CBuffer cbuff = new CBuffer(0, 1, 0, b, 8);
		for (ByteBuffer bb : cbuff.intervalOf(0L, 1L, timestampSeeker)) {
			long t = bb.getLong();
			assertEquals(t, 0L);
		}
	}
	
	@Test
	public void testNineOrderedValues() {
		byte[] b = new byte[8 * 9];
		ByteBuffer data = ByteBuffer.wrap(b);
		for (long i = 0; i < 9L; i++) {
			data.putLong(i);
			System.out.printf("%d ", i);
		}
		System.out.println();
		
		CBuffer cbuff = new CBuffer(2, 5, 2, b, 8);
		boolean noElements = true;
		for (ByteBuffer bb : cbuff.intervalOf(0L, 1L, timestampSeeker)) {
			noElements = false;
		}
		assertTrue(noElements);

		int count = 0;
		for (ByteBuffer bb : cbuff.intervalOf(3L, 4L, timestampSeeker)) {
			count++;
		}
		assertEquals(2, count);
		long i = 2;
		for (ByteBuffer bb : cbuff.intervalOf(0L, 9L, timestampSeeker)) {
			long t = bb.getLong();
			assertEquals(i++, t);
		}
	}

	@Test
	public void testCyclicNineOrderedValues() {
		byte[] b = new byte[8 * 9];
		ByteBuffer data = ByteBuffer.wrap(b);
		for (long i = 3L; i < 9L; i++) {
			data.putLong(i);
			System.out.printf("%d ", i);
		}
		for (long i = 0L; i < 3L; i++) {
			data.putLong(i);
			System.out.printf("%d ", i);
		}
		System.out.println();
		
		CBuffer cbuff = new CBuffer(0, 9, 2, b, 8);
		int count = 0;
		for (ByteBuffer bb : cbuff.intervalOf(3L, 4L, timestampSeeker)) {
			count++;
		}
		assertEquals(2, count);
		
		count = 0;
		long i = 0;
		for (ByteBuffer bb : cbuff.intervalOf(0L, 4L, timestampSeeker)) {
			count++;
			long t = bb.getLong();
			assertEquals(i++, t);
		}
		assertEquals(5, count);
		
	}
	
}
