package ru.hipdriver.d;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;

import org.junit.Test;

import ru.hipdriver.d.Sensors.AverageValue;
import ru.hipdriver.j.ext.SensorData;

public class SensorsTest {

	@Test
	public void testNoValues() {
		SensorData sensorData = Sensors.makeForNMeasures(10);
		long time = System.nanoTime();
		AverageValue avalue = Sensors.averageValue(sensorData, time, 1000);
		assertNull(avalue);
	}

	@Test
	public void testZeroValues() throws Throwable {
		SensorData sensorData = Sensors.makeForNMeasures(10);
		for ( int i = 0; i < 10; i++) {
			Sensors.caapData(sensorData, 0, 0, 0, System.nanoTime());
			Thread.sleep(20);
		}
		long time = System.nanoTime();
		AverageValue avalue = Sensors.averageValue(sensorData, time, 1000000000);
		assertNotNull(avalue);
		assertTrue(avalue.x == 0f);
		assertTrue(avalue.y == 0f);
		assertTrue(avalue.z == 0f);
	}
	
	@Test
	public void linearTestInDebugMode() throws Throwable {
		SensorData sensorData = Sensors.makeForNMeasures(11 * 3);
		float linearValue = 0;
		long timeInNanos = 0;//System.nanoTime();
		for ( int i = 0; i < 11; i++) {
			Sensors.caapDataInDebugMode(sensorData, linearValue, linearValue, linearValue, timeInNanos);
			linearValue++;
			timeInNanos += 1000000000;
			//Thread.sleep(100);
			//timeInNanos = System.nanoTime();
		}
		//Check results
		byte[] data = sensorData.getData();
		assertNotNull(data);
		int countOfValues = data.length / (3 * 4 * 5);
		assertEquals(countOfValues, 11);
		float checkValue = 0;
		long time0 = -1;
		System.out.println("[timeInNanos] x,y,z -> xJ1,yJ1,zJ1 -> xJ2,yJ2,zJ2");
		for ( int i = 0 ; i < countOfValues; i++ ) {
			ByteBuffer buff;

			buff = Sensors.readBuffer(sensorData.data, i);
			float x = buff.getFloat();
			float y = buff.getFloat();
			float z = buff.getFloat();
			long timestamp = buff.getLong();
			
			buff = Sensors.readBuffer(sensorData.data, countOfValues + i);
			float xJ1 = buff.getFloat();
			float yJ1 = buff.getFloat();
			float zJ1 = buff.getFloat();
			long timestampJ1 = buff.getLong();
			
			buff = Sensors.readBuffer(sensorData.data, 2 * countOfValues + i);
			float xJ2 = buff.getFloat();
			float yJ2 = buff.getFloat();
			float zJ2 = buff.getFloat();
			long timestampJ2 = buff.getLong();
			
			assertTrue(x == checkValue);
			assertTrue(y == checkValue);
			assertTrue(z == checkValue);

			assertEquals(timestamp, timestampJ1);
			assertEquals(timestamp, timestampJ2);
			
			if (time0 < 0 ) {
				time0 = timestamp;
			}

			System.out.printf("[%d] %f,%f,%f -> %f,%f,%f -> %f,%f,%f", timestamp - time0, x, y, z, xJ1, yJ1, zJ1, xJ2, yJ2, zJ2).println();
			
			checkValue++;
		}
		
	}
	
	@Test
	public void constantTestInDebugMode() throws Throwable {
		SensorData sensorData = Sensors.makeForNMeasures(11 * 3);
		float constantValue = 1;
		long timeInNanos = 0;//System.nanoTime();
		for ( int i = 0; i < 11; i++) {
			Sensors.caapDataInDebugMode(sensorData, constantValue, constantValue, constantValue, timeInNanos);
			timeInNanos += 1000000000;
			//Thread.sleep(100);
			//timeInNanos = System.nanoTime();
		}
		//Check results
		byte[] data = sensorData.getData();
		assertNotNull(data);
		int countOfValues = data.length / (3 * 4 * 5);
		assertEquals(countOfValues, 11);
		float checkValue = 1;
		long time0 = -1;
		System.out.println("[timeInNanos] x,y,z -> xJ1,yJ1,zJ1 -> xJ2,yJ2,zJ2");
		for ( int i = 0 ; i < countOfValues; i++ ) {
			ByteBuffer buff;

			buff = Sensors.readBuffer(sensorData.data, i);
			float x = buff.getFloat();
			float y = buff.getFloat();
			float z = buff.getFloat();
			long timestamp = buff.getLong();
			
			buff = Sensors.readBuffer(sensorData.data, countOfValues + i);
			float xJ1 = buff.getFloat();
			float yJ1 = buff.getFloat();
			float zJ1 = buff.getFloat();
			long timestampJ1 = buff.getLong();
			
			buff = Sensors.readBuffer(sensorData.data, 2 * countOfValues + i);
			float xJ2 = buff.getFloat();
			float yJ2 = buff.getFloat();
			float zJ2 = buff.getFloat();
			long timestampJ2 = buff.getLong();
			
			assertTrue(x == checkValue);
			assertTrue(y == checkValue);
			assertTrue(z == checkValue);

			assertEquals(timestamp, timestampJ1);
			assertEquals(timestamp, timestampJ2);
			
			if (time0 < 0 ) {
				time0 = timestamp;
			}

			System.out.printf("[%d] %f,%f,%f -> %f,%f,%f -> %f,%f,%f", timestamp - time0, x, y, z, xJ1, yJ1, zJ1, xJ2, yJ2, zJ2).println();
		}
		
	}
	
}
