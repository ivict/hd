/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.d;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;

import ru.hipdriver.j.ext.SensorData;

/**
 * Статические функции обработки данных сенсоров.
 * Модель взаимодействия потоков записи, чтения.
 * 1) Читаем много реже нежели пишем.
 * 2) Пишет одна нить, читают многие.
 * 3) Размер буфера достаточно велик.
 * 
 * @author ivict
 * 
 */
public class Sensors {

	private Sensors() { }
	
	public static class AverageValue {
		public final float x;
		public final float y;
		public final float z;
		public final long t0;
		public final long t1;
		public final int count;

		public AverageValue (float x, float y, float z, long t0, long t1, int count) {
			this.x = x;
			this.y = y;
			this.z = z;
			this.t0 = t0;
			this.t1 = t1;
			this.count = count;
		}

		public float getY () {
			return y;
		}

		public float getZ () {
			return z;
		}

		public long getT0 () {
			return t0;
		}

		public long getT1 () {
			return t1;
		}
		
		public float getX () {
			return x;
		}

		public int getCount () {
			return count;
		}
	}
	
	/**
	 * Добавление и упаковка значений сенсора.
	 * Максимальная продолжительность измерения чуть более 596 часов, это более 20 дней.
	 * Это ограничение наложено применением типа int для времени в миллисекундах в данных
	 * @param sensorData
	 * @param x значение
	 * @param y значение
	 * @param z значение
	 * @param timeInMillis время в наносекундах
	 */
	public static void aapData(ByteBuffer buff, float x, float y, float z, long timeInNanos) {
		int pos = buff.position();
		int lim = buff.limit();
		if ( pos + 4 * 5 > lim ) {
			throw new IllegalStateException("Buffer size too small");
		}
		buff.putFloat(x);
		buff.putFloat(y);
		buff.putFloat(z);
		buff.putLong(timeInNanos);
	}
	
	public static SensorData makeForNMeasures(int maxCountOfMeasures, boolean preparingMode) {
		SensorData sensorData = new SensorData();
		sensorData.setBaseTime(System.currentTimeMillis());
		byte[] data = new byte[maxCountOfMeasures * 4 * 5];
		sensorData.setData(data);
		sensorData.setCountOfValues(0);
		sensorData.preparingMode = preparingMode;		
		return sensorData;
	}
	
	public static SensorData makeForNMeasures(int maxCountOfMeasures) {
		return makeForNMeasures(maxCountOfMeasures, false);
	}
	
	/**
	 * Циклическое добавление данных. После достижения конца буфера, начинаем наполнять его с начала.
	 * При этом счетчик количества значений, не сбрасываем, а увеличиваем непрерывно.
	 * Небезопасно для мультипоточной модели.
	 * Изменение счетчика последнего значения идет до изменения данных в буфере.
	 * @param sensorData
	 * @param x
	 * @param y
	 * @param z
	 * @param timeInNanos
	 */
	public static void caapData(SensorData sensorData, float x, float y, float z, long timeInNanos) {
		byte[] data = sensorData.data;
		if ( data == null ) {
			throw new IllegalStateException("Invalid data buffer");
		}
		if ( data.length < 4 * 5 ) {
			throw new IllegalStateException("Buffer size too small");
		}
		int countOfValues = sensorData.getCountOfValues();
		int maxValuesInBuffer = data.length / (4 * 5);
		int offsetCounter = countOfValues % maxValuesInBuffer;
		int offset = offsetCounter * 4 * 5;
		ByteBuffer buff = ByteBuffer.wrap(data);
		int lim = buff.limit();
		if ( offset + 4 * 5 > lim ) {//Reset to zero position
			offset = 0;
		}
		buff.position(offset);
		//Update buffer data
		aapData(buff, x, y, z, timeInNanos);
		
		//Increase count of values after all, warranties correct data
		//for last added value.
		sensorData.setCountOfValues(countOfValues + 1);

		if ( countOfValues == 0 ) {//Initial point
			resetNMinus2Value(sensorData, x, y, z, timeInNanos);
			Arrays.fill(sensorData.J1, 0);
			resetJ1NMinus4Value(sensorData, sensorData.J1[0], sensorData.J1[1], sensorData.J1[2], timeInNanos);
			resetJ1NMinus2Value(sensorData, sensorData.J1[0], sensorData.J1[1], sensorData.J1[2]);
			resetNMinus4Value(sensorData, x, y, z);//Call always after resetJ1NMinus4Value (reason: correlation with setup tNMinus4).  
			return;
		}
		sensorData.a[0] = x;
		sensorData.a[1] = y;
		sensorData.a[2] = z;
		//Every two next call after first calculate J1
		if ( countOfValues % 2 != 0 ) {
			resetNMinus1Value(sensorData, x, y, z);
			return;
		}
		float h = (timeInNanos - sensorData.tNMinus2) / 1000000000f;
		float s = h / 6f;
		//Correction for sleeping sensors
		if (s < 1f) {
			sensorData.J1[0] += (sensorData.xNMinus2 + 4 * sensorData.xNMinus1 + x) * s;  
			sensorData.J1[1] += (sensorData.yNMinus2 + 4 * sensorData.yNMinus1 + y) * s;  
			sensorData.J1[2] += (sensorData.zNMinus2 + 4 * sensorData.zNMinus1 + z) * s;
		}

		//Interpolation Simson -> reduce to primitive calculations
//		sensorData.J2[0] += (sensorData.J1Minus2[0] + sensorData.J1[0]) * 3 * s; 
//		sensorData.J2[1] += (sensorData.J1Minus2[1] + sensorData.J1[1]) * 3 * s; 
//		sensorData.J2[2] += (sensorData.J1Minus2[2] + sensorData.J1[2]) * 3 * s; 
		
		resetJ1NMinus2Value(sensorData, sensorData.J1[0], sensorData.J1[1], sensorData.J1[2]);
		
		
		//every four next call after first calculate J2
		if ( countOfValues % 4 != 0 || countOfValues < 4 ) {
			resetNMinus2Value(sensorData, x, y, z, timeInNanos);
			//reseting values for NMinus2 does code already above 
			return;
		}
		float hJ1 = (timeInNanos - sensorData.tNMinus4) / 1000000000f;
		float sJ1 = hJ1 / 6f; 
		//More rough calculation for apply Richardson extrapolation based on Runge rule
		sensorData.J12N[0] += (sensorData.xNMinus4 + 4 * sensorData.xNMinus2 + x) * sJ1; 
		sensorData.J12N[1] += (sensorData.yNMinus4 + 4 * sensorData.yNMinus2 + y) * sJ1; 
		sensorData.J12N[2] += (sensorData.zNMinus4 + 4 * sensorData.zNMinus2 + z) * sJ1; 

		resetNMinus2Value(sensorData, x, y, z, timeInNanos);
		resetJ1NMinus4Value(sensorData, sensorData.J1[0], sensorData.J1[1], sensorData.J1[2], timeInNanos);
		resetNMinus4Value(sensorData, x, y, z);//Call always after resetJ1NMinus4Value (reason: correlation with setup tNMinus4).
	}
	
	public static void caapDataInDebugMode(SensorData sensorData, float x, float y, float z, long timeInNanos) {
		byte[] data = sensorData.data;
		if ( data == null ) {
			throw new IllegalStateException("Invalid data buffer");
		}
		if ( data.length < 3 * 4 * 5 ) {
			throw new IllegalStateException("Buffer size too small");
		}
		//In debug mode reset control countOfValues
		int countOfValues = sensorData.getCountOfValues();
		int maxValuesInBuffer = data.length / (3 * 4 * 5);
		//Hack set countOfValues always less then maxValuesInBuffer
		//For correct working of caapData in debugging mode
		int virtualCountOfValues = countOfValues % maxValuesInBuffer;
		sensorData.setCountOfValues(virtualCountOfValues);
		caapData(sensorData, x, y, z, timeInNanos);
		sensorData.setCountOfValues(countOfValues + (sensorData.getCountOfValues() - virtualCountOfValues));

		ByteBuffer buff = ByteBuffer.wrap(data);
		
		//Store J1 value
		int j1Offset = (maxValuesInBuffer + virtualCountOfValues) * 4 * 5;
		buff.position(j1Offset);
		
		//Every 18 measure we try correct values
		if ( countOfValues % 18 == 0  && countOfValues > 0) {
			// Richardson formula: (2^p * J1 - J12N) / (2^p - 1)
			// Where p = 4 for Simpson method
			// And J12N in 2 times rougher measuring then J1
			float[] J1 = sensorData.J1;
			float[] J12N = sensorData.J12N;
			float[] ERR = sensorData.J1ERR;
			ERR[0] = Math.abs((J1[0] - J12N[0]) / (16f - 1f));
			ERR[1] = Math.abs((J1[1] - J12N[1]) / (16f - 1f));
			ERR[2] = Math.abs((J1[2] - J12N[2]) / (16f - 1f));
			
			float resolution = sensorData.getResolution();
			if (ERR[0] < 0.5f * resolution) {
				J1[0] = ((16f * J1[0] - J12N[0]) / (16f - 1f));
			} else {
				J1[0] = 0f;
			}
			
			if (ERR[1] < 0.5f * resolution) {
				J1[1] = ((16f * J1[1] - J12N[1]) / (16f - 1f));
			} else {
				J1[1] = 0f;
			}
			
			if (ERR[2] < 0.5f * resolution) {
				J1[2] = ((16f * J1[2] - J12N[2]) / (16f - 1f));
			} else {
				J1[2] = 0f;
			}
		}
		
		//Update buffer data
		aapData(buff, sensorData.J1[0], sensorData.J1[1], sensorData.J1[2], timeInNanos);
		//Store ANGLES value
		int anglesOffset = (2 * maxValuesInBuffer + virtualCountOfValues) * 4 * 5;
		buff.position(anglesOffset);
		//Update buffer data
		aapData(buff, sensorData.ANGLES[0], sensorData.ANGLES[1], sensorData.ANGLES[2], timeInNanos);
	}
	

	private static void resetNMinus4Value(SensorData sensorData, float x,
			float y, float z) {
		sensorData.xNMinus4 = x;
		sensorData.yNMinus4 = y;
		sensorData.zNMinus4 = z;
	}

	private static void resetJ1NMinus4Value(SensorData sensorData, float xJ1,
			float yJ1, float zJ1, long timeInNanos) {
		sensorData.J1Minus4[0] = xJ1;
		sensorData.J1Minus4[1] = yJ1;
		sensorData.J1Minus4[2] = zJ1;
		sensorData.tNMinus4 = timeInNanos;
	}

	private static void resetJ1NMinus2Value(SensorData sensorData, float xJ1, float yJ1, float zJ1) {
		sensorData.J1Minus2[0] = xJ1;
		sensorData.J1Minus2[1] = yJ1;
		sensorData.J1Minus2[2] = zJ1;
	}

	private static void resetNMinus2Value(SensorData sensorData, float x,
			float y, float z, long timeInNanos) {
		sensorData.xNMinus2 = x;
		sensorData.yNMinus2 = y;
		sensorData.zNMinus2 = z;
		sensorData.tNMinus2 = timeInNanos;
	}
	
	private static void resetNMinus1Value(SensorData sensorData, float x,
			float y, float z) {
		sensorData.xNMinus1 = x;
		sensorData.yNMinus1 = y;
		sensorData.zNMinus1 = z;
	}
	
	public static SensorData shrinkData(SensorData sensorData) {
		byte[] data = sensorData.data;
		if ( data == null ) {
			return null;
		}
		if ( data.length < 4 * 5 ) {
			return null;
		}
		int countOfValues = sensorData.getCountOfValues();
		int maxValuesInBuffer = data.length / (4 * 5);
		if ( maxValuesInBuffer <= countOfValues ) {//Shrink by maxValuesInBuffer
			countOfValues = maxValuesInBuffer;
		}
		int newLimit = countOfValues * 4 * 5;
		byte[] shrinkedData = new byte[newLimit];
		System.arraycopy(data, 0, shrinkedData, 0, newLimit);
		SensorData sd = cloneSensorData(sensorData, shrinkedData);
		return sd;
	}

	public static SensorData shrinkData(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		byte[] data = sensorData.data;
		if ( data == null ) {
			return null;
		}
		if ( data.length < 4 * 5 ) {
			return null;
		}
		
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if (cursor == null) {
			return null;
		}
		int initialPosition = initialPositionBackwardFromCursorPosition(timeBackInNanos, cursor);
		if (initialPosition < 0) {
			return null;
		}
		
		int countOfValues = cursor.positionOfValue - initialPosition;
		int maxValuesInBuffer = data.length / (4 * 5);
		if ( maxValuesInBuffer <= countOfValues ) {//Shrink by maxValuesInBuffer
			countOfValues = maxValuesInBuffer;
		}
		int newLimit = countOfValues * 4 * 5;
		int initialOffsetInValues = initialPosition % maxValuesInBuffer;
		int initialOffset = initialOffsetInValues * 4 * 5;
		byte[] shrinkedData = new byte[newLimit];
		System.arraycopy(data, initialOffset, shrinkedData, 0, newLimit);
		SensorData sd = cloneSensorData(sensorData, shrinkedData);
		return sd;
	}

	public static SensorData cloneSensorData(SensorData sensorData,
			byte[] shrinkedData) {
		SensorData sd = new SensorData();
		sd.setName(sensorData.getName());
		sd.setVendor(sensorData.getVendor());
		sd.setVersion(sensorData.getVersion());
		sd.setMaximumRange(sensorData.getMaximumRange());
		sd.setResolution(sensorData.getResolution());
		sd.setPower(sensorData.getPower());
		sd.setBaseTime(sensorData.getBaseTime());
		sd.setCountOfValues(sensorData.getCountOfValues());
		sd.setData(shrinkedData);
		sd.preparingMode = sensorData.preparingMode;
		return sd;
	}

	public static SensorData shrinkDataInDebugMode(SensorData sensorData) {
		byte[] data = sensorData.data;
		if ( data == null ) {
			return null;
		}
		if ( data.length < 3 * 4 * 5 ) {
			return null;
		}
		int countOfValues = sensorData.getCountOfValues();
		int maxValuesInBuffer = data.length / (3 * 4 * 5);
		if ( maxValuesInBuffer <= countOfValues ) {//Shrink by maxValuesInBuffer
			countOfValues = maxValuesInBuffer;
		}
		int dataLength = countOfValues * 4 * 5; 
		int segmentSize = maxValuesInBuffer * 4 * 5;
		int newLimit = countOfValues * 3 * 4 * 5;
		byte[] shrinkedData = new byte[newLimit];
		//Copy values
		System.arraycopy(data, 0, shrinkedData, 0, dataLength);
		//Copy J1
		System.arraycopy(data, segmentSize, shrinkedData, dataLength, dataLength);
		//Copy J2
		System.arraycopy(data, 2 * segmentSize, shrinkedData, 2 * dataLength, dataLength);
		SensorData sd = cloneSensorData(sensorData, shrinkedData);
		return sd;
	}

	public static SensorData shrinkDataInDebugMode(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		byte[] data = sensorData.data;
		if ( data == null ) {
			return null;
		}
		if ( data.length < 3 * 4 * 5 ) {
			return null;
		}
		
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if (cursor == null) {
			return null;
		}
	
		int initialPosition = initialPositionBackwardFromCursorPosition(timeBackInNanos, cursor);
		if (initialPosition < 0) {
			return null;
		}
		
		int countOfValues = cursor.positionOfValue - initialPosition;
		int maxValuesInBuffer = data.length / (3 * 4 * 5);
		if ( maxValuesInBuffer <= countOfValues ) {//Shrink by maxValuesInBuffer
			countOfValues = maxValuesInBuffer;
		}
		int dataLength = countOfValues * 4 * 5; 
		int segmentSize = maxValuesInBuffer * 4 * 5;
		int newLimit = countOfValues * 3 * 4 * 5;
		int initialOffsetInValues = initialPosition % maxValuesInBuffer;
		int initialOffset = initialOffsetInValues * 4 * 5;
		byte[] shrinkedData = new byte[newLimit];
		//Copy values
		System.arraycopy(data, initialOffset, shrinkedData, 0, dataLength);
		//Copy J1
		System.arraycopy(data, segmentSize + initialOffset, shrinkedData, dataLength, dataLength);
		//Copy J2
		System.arraycopy(data, 2 * segmentSize + initialOffset, shrinkedData, 2 * dataLength, dataLength);
		
		SensorData sd = cloneSensorData(sensorData, shrinkedData);
		return sd;
	}
	
	private static class SensorDataPosition {
		byte[] data;
		int positionOfValue;
		int minimalOffset;
		int maximalPosition;
		boolean cyclic;
		long tN;
	}
	
	//Non blocking read of buffer (real-time-issues).
	public static AverageValue averageValue(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		
		return averageValueBackwardFromCursorPosition(timeBackInNanos, cursor);
	}

	//Non blocking read of buffer (real-time-issues).
	//Avalable only wirh debugMode == true
	public static AverageValue averageValueJ1(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 1, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		
		return averageValueBackwardFromCursorPosition(timeBackInNanos, cursor);
	}

	//Non blocking read of buffer (real-time-issues).
	//Avalable only wirh debugMode == true
	public static AverageValue averageValueJ2(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 2, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		
		return averageValueBackwardFromCursorPosition(timeBackInNanos, cursor);
	}

	private static AverageValue averageValueBackwardFromCursorPosition(
			long timeBackInNanos, SensorDataPosition cursor) {
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		int count = 0;
		float x = 0;
		float y = 0;
		float z = 0;
		long t0 = 0;
		long amountOfTime = 0;
		for ( int i = positionOfValue; i >= cursor.minimalOffset; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			x += value.getFloat();
			y += value.getFloat();
			z += value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
		}
		AverageValue avalue = new AverageValue(x / (float) count, y / (float) count, z / (float) count, t0, t1, count);
		return avalue;
	}

	private static int initialPositionBackwardFromCursorPosition(
			long timeBackInNanos, SensorDataPosition cursor) {
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		int count = 0;
		long t0 = 0;
		long amountOfTime = 0;
		for ( int i = positionOfValue; i >= cursor.minimalOffset; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			value.position(4 * 3);
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
		}
		if (count > positionOfValue) {
			//TODO: cyclic search from at the end of buffer
			return 0;
		}
		return positionOfValue - count;
	}
	
	//Non blocking read of buffer (real-time-issues).
	//Average of square of absolute value.
	public static float averageSqabsValue(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return -1f;
		}
		
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		int count = 0;
		float sumSqabsValue = 0;
		long t0 = 0;
		long amountOfTime = 0;
		for ( int i = positionOfValue; i >= 0; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			t0 = value.getLong();
			
			sumSqabsValue += x * x + y * y + z * z; 
			
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
		}
		float avalue = sumSqabsValue / (float) count;
		return avalue;
	}
	
	public static class MinMaxSqValue {
		public final float minValue;
		public final float maxValue;
		public final int count;
		public MinMaxSqValue(float minValue, float maxValue, int count) {
			super();
			this.minValue = minValue;
			this.maxValue = maxValue;
			this.count = count;
		}
		@Override
		public String toString() {
			return String.format("%s::%s::%s", minValue, maxValue, count) ;
		}
	}
	
	public static class MinMaxValue {
		public final float[] minValue;
		public final float[] maxValue;
		public final float[] avgValue;
		public final int count;
		public MinMaxValue(float[] minValue, float[] maxValue, float[] avgValue, int count) {
			super();
			this.minValue = minValue;
			this.maxValue = maxValue;
			this.avgValue = avgValue;
			this.count = count;
		}
	}
	
	//Non blocking read of buffer (real-time-issues).
	public static MinMaxSqValue minMaxSqabsValue(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		MinMaxSqValue result = minMaxValueCalculation(timeBackInNanos, cursor);
		return result;
	}

	private static MinMaxSqValue minMaxValueCalculation(long timeBackInNanos,
			SensorDataPosition cursor) {
		float minValue = Float.MAX_VALUE;
		float maxValue = 0;
		
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		int count = 0;
		long t0 = 0;
		long amountOfTime = 0;
		for ( int i = positionOfValue; i >= cursor.minimalOffset; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			float valueSq = x * x + y * y + z * z;
			if ( valueSq < minValue ) {
				minValue = valueSq;
			}
			if ( valueSq > maxValue ) {
				maxValue = valueSq;
			}
		}
		MinMaxSqValue result = new MinMaxSqValue(minValue, maxValue, count);
		return result;
	}
	
	private static MinMaxValue getMinMaxValueLPF(long timeBackInNanos,
			SensorDataPosition cursor, float alpha) {
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		float[] minValue = new float[3];
		Arrays.fill(minValue, Float.MAX_VALUE);
		float[] maxValue = new float[3];
		Arrays.fill(maxValue, Float.MIN_VALUE);
		float[] lpfValue = new float[3];
		float[] avgValue = new float[3];
		int count = 0;
		long t0 = 0;
		long amountOfTime = 0;
		for ( int i = positionOfValue; i >= cursor.minimalOffset; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			//Low-Pass filter
			if (i == positionOfValue) {
				//Initial values
				lpfValue[0] = x;			
				lpfValue[1] = y;			
				lpfValue[2] = z;
			} else {
				lpfValue[0] = lpfValue[0] + alpha * (x - lpfValue[0]); 
				lpfValue[1] = lpfValue[1] + alpha * (y - lpfValue[1]); 
				lpfValue[2] = lpfValue[2] + alpha * (z - lpfValue[2]); 
			}
			
			for (int j = 0; j < 3; j++) {
				if (lpfValue[j] < minValue[j]) {
					minValue[j] = lpfValue[j];
				}
				if (lpfValue[j] > maxValue[j]) {
					maxValue[j] = lpfValue[j];
				}
				avgValue[j] += lpfValue[j];
			}
		}
		for (int i = 0; i < 3; i++) {
			avgValue[i] = avgValue[i] / (float) count; 
		}
		MinMaxValue result = new MinMaxValue(minValue, maxValue, avgValue, count);
		return result;
	}

	private static MinMaxValue getMinMaxValueWithAveragyByNPointsAndLPF(long timeBackInNanos,
			SensorDataPosition cursor, float alpha, int n) {
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		//Calculate mean values
		int count = 0;
		long t0 = 0;
		long amountOfTime = 0;
		float meanX = 0;
		float meanY = 0;
		float meanZ = 0;
		List<Float> listX = new ArrayList<Float>();
		List<Float> listY = new ArrayList<Float>();
		List<Float> listZ = new ArrayList<Float>();
		for ( int i = positionOfValue; i >= cursor.minimalOffset; i-- ) {
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			count++;
			listX.add(x);
			meanX +=x;
			listY.add(y);
			meanY +=y;
			listZ.add(z);
			meanZ +=z;
		}
		if (count <= 0) {
			return null;
		}
		meanX = meanX / (float) count;
		meanY = meanY / (float) count;
		meanZ = meanZ / (float) count;
		//Calculate standard deviation
		float sX = 0;
		float sY = 0;
		float sZ = 0;
		for ( int i = 0; i < count; i++ ) {
			float x = listX.get(i);
			float y = listY.get(i);
			float z = listZ.get(i);
			sX += (x - meanX) * (x - meanX);
			sY += (y - meanY) * (y - meanY);
			sZ += (z - meanZ) * (z - meanZ);
		}
		sX = (float) Math.sqrt(sX / (float) (count - 1)); 
		sY = (float) Math.sqrt(sY / (float) (count - 1)); 
		sZ = (float) Math.sqrt(sZ / (float) (count - 1));
		
		if (sX > 1) {
			System.out.println();
		}
		
		float[] minValue = new float[3];
		Arrays.fill(minValue, Float.MAX_VALUE);
		float[] maxValue = new float[3];
		Arrays.fill(maxValue, Float.MIN_VALUE);
		float[] lpfValue = new float[3];
		float[] avgValue = new float[3];
		float[] avgAvgValue = {meanX, meanY, meanZ};
		int c = 0;
		for ( int i = 0; i < count; i++ ) {
			float x = listX.get(i);
			float y = listY.get(i);
			float z = listZ.get(i);
			//Culling of jumps
			if (Math.abs(x - meanX) >  1.2 * sX
					|| Math.abs(y - meanY) >  1.2 * sY
					|| Math.abs(z - meanZ) >  1.2 * sZ) {
				continue;
			}
			c++;
			//Low-Pass filter
			if (c == 1) {
				//Initial values
				lpfValue[0] = x;			
				lpfValue[1] = y;			
				lpfValue[2] = z;
			} else {
				lpfValue[0] = lpfValue[0] + alpha * (x - lpfValue[0]); 
				lpfValue[1] = lpfValue[1] + alpha * (y - lpfValue[1]); 
				lpfValue[2] = lpfValue[2] + alpha * (z - lpfValue[2]); 
			}
			
			for (int j = 0; j < 3; j++) {
				avgAvgValue[j] = lpfValue[j];
			}
			
			for (int j = 0; j < 3; j++) {
				avgValue[j] += lpfValue[j];
			}
			if (c % n != 0 ) {
				continue;
			}
			for (int j = 0; j < 3; j++) {
				avgValue[j] += avgValue[j] / (float) n;
			}
			
			for (int j = 0; j < 3; j++) {
				if (lpfValue[j] < minValue[j]) {
					minValue[j] = lpfValue[j];
				}
				if (lpfValue[j] > maxValue[j]) {
					maxValue[j] = lpfValue[j];
				}
			}
		}
		
//		for (int j = 0; j < 3; j++) {
//			avgAvgValue[j] = avgAvgValue[j] / (float) count;
//		}
		MinMaxValue result = new MinMaxValue(minValue, maxValue, avgAvgValue, count);
//		if (avgAvgValue[0] < 40f) {
//			System.out.println();
//		}
		return result;
	}
	
	private static VectorData2 getData(long timeBackInNanos, int vectorIndex0, int vectorIndex1,
			SensorDataPosition cursor) {
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		//Calculate mean values
		long t0 = 0;
		long lastT = t1;
		long amountOfTime = 0;
		List<Float> values0 = new ArrayList<Float>();
		List<Float> values1 = new ArrayList<Float>();
		for ( int i = positionOfValue; i >= cursor.minimalOffset; i-- ) {
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			switch(vectorIndex0) {
			case 0:
				values0.add(x);
				break;
			case 1:
				values0.add(y);
				break;
			case 2:
				values0.add(z);
				break;
			}
			switch(vectorIndex1) {
			case 0:
				values1.add(x);
				break;
			case 1:
				values1.add(y);
				break;
			case 2:
				values1.add(z);
				break;
			}
			lastT = t0;
		}
		assert values0.size() == values1.size();
		int count = values0.size();
		double[] result0 = new double[count * 2];
		double[] result1 = new double[count * 2];
		for (int i = 0; i < count; i++) {
			int realI = 2 * i;
			int imageI = 2 * i + 1;
			result0[realI] = values0.get(i);
			result1[realI] = values1.get(i);
			result0[imageI] = 0;
			result1[imageI] = 0;
		}
		VectorData2 result = new VectorData2(result0, result1, count, t1 - lastT);
		return result;
	}
	
	private static MinMaxValue getMinMaxValueWithAveragyByNPointsAndLPFCyclic(long timeBackInNanos,
			SensorDataPosition cursor, float alpha, int n) {
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		//Calculate mean values
		int count = 0;
		long t0 = 0;
		long amountOfTime = 0;
		float meanX = 0;
		float meanY = 0;
		float meanZ = 0;
		List<Float> listX = new ArrayList<Float>();
		List<Float> listY = new ArrayList<Float>();
		List<Float> listZ = new ArrayList<Float>();
		for ( int i = positionOfValue; i >= cursor.minimalOffset; i-- ) {
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			count++;
			listX.add(x);
			meanX +=x;
			listY.add(y);
			meanY +=y;
			listZ.add(z);
			meanZ +=z;
		}
		if (count <= 0) {
			return null;
		}
		meanX = meanX / (float) count;
		meanY = meanY / (float) count;
		meanZ = meanZ / (float) count;
		//Calculate standard deviation
		float sX = 0;
		float sY = 0;
		float sZ = 0;
		for ( int i = 0; i < count; i++ ) {
			float x = listX.get(i);
			float y = listY.get(i);
			float z = listZ.get(i);
			sX += (x - meanX) * (x - meanX);
			sY += (y - meanY) * (y - meanY);
			sZ += (z - meanZ) * (z - meanZ);
		}
		sX = (float) Math.sqrt(sX / (float) (count - 1)); 
		sY = (float) Math.sqrt(sY / (float) (count - 1)); 
		sZ = (float) Math.sqrt(sZ / (float) (count - 1));
		
		if (sX > 1) {
			System.out.println();
		}
		
		float[] minValue = new float[3];
		Arrays.fill(minValue, Float.MAX_VALUE);
		float[] maxValue = new float[3];
		Arrays.fill(maxValue, Float.MIN_VALUE);
		float[] lpfValue = new float[3];
		float[] avgValue = new float[3];
		float[] avgAvgValue = {meanX, meanY, meanZ};
		int c = 0;
		for ( int i = 0; i < count; i++ ) {
			float x = listX.get(i);
			float y = listY.get(i);
			float z = listZ.get(i);
			//Culling of jumps
			if (alpha < 1f && (Math.abs(x - meanX) >  1.2 * sX
					|| Math.abs(y - meanY) >  1.2 * sY
					|| Math.abs(z - meanZ) >  1.2 * sZ)) {
				continue;
			}
			c++;
			//Low-Pass filter
			if (c == 1) {
				//Initial values
				lpfValue[0] = x;			
				lpfValue[1] = y;			
				lpfValue[2] = z;
			} else {
				lpfValue[0] = lpfValue[0] + alpha * (x - lpfValue[0]); 
				lpfValue[1] = lpfValue[1] + alpha * (y - lpfValue[1]); 
				lpfValue[2] = lpfValue[2] + alpha * (z - lpfValue[2]); 
			}
			
			for (int j = 0; j < 3; j++) {
				avgAvgValue[j] = lpfValue[j];
			}
			
			for (int j = 0; j < 3; j++) {
				avgValue[j] += lpfValue[j];
			}
			if (c % n != 0 ) {
				continue;
			}
			for (int j = 0; j < 3; j++) {
				avgValue[j] += avgValue[j] / (float) n;
			}
			
			for (int j = 0; j < 3; j++) {
				if (lpfValue[j] < minValue[j]) {
					minValue[j] = lpfValue[j];
				}
				if (lpfValue[j] > maxValue[j]) {
					maxValue[j] = lpfValue[j];
				}
			}
		}
		
//		for (int j = 0; j < 3; j++) {
//			avgAvgValue[j] = avgAvgValue[j] / (float) count;
//		}
		MinMaxValue result = new MinMaxValue(minValue, maxValue, avgAvgValue, count);
//		if (avgAvgValue[0] < 40f) {
//			System.out.println();
//		}
		return result;
	}

	public static class Value {
		public final float[] value;
		public final int count;
		public Value(float[] value, int count) {
			this.value = value;
			this.count = count;
		}
	}

	public static class ScalarValue {
		public final float value;
		public final int count;
		public ScalarValue(float value, int count) {
			this.value = value;
			this.count = count;
		}
	}

	private static Value getStandardDeviationCyclic(long timeBackInNanos,
			SensorDataPosition cursor) {
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		//Calculate mean values
		int count = 0;
		long t0 = 0;
		long amountOfTime = 0;
		float meanX = 0;
		float meanY = 0;
		float meanZ = 0;
		List<Float> listX = new ArrayList<Float>();
		List<Float> listY = new ArrayList<Float>();
		List<Float> listZ = new ArrayList<Float>();
		for ( int i = positionOfValue; i >= cursor.minimalOffset; i-- ) {
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			count++;
			listX.add(x);
			meanX +=x;
			listY.add(y);
			meanY +=y;
			listZ.add(z);
			meanZ +=z;
		}
		if (count <= 0) {
			return null;
		}
		meanX = meanX / (float) count;
		meanY = meanY / (float) count;
		meanZ = meanZ / (float) count;
		//Calculate standard deviation
		float sX = 0;
		float sY = 0;
		float sZ = 0;
		for ( int i = 0; i < count; i++ ) {
			float x = listX.get(i);
			float y = listY.get(i);
			float z = listZ.get(i);
			sX += (x - meanX) * (x - meanX);
			sY += (y - meanY) * (y - meanY);
			sZ += (z - meanZ) * (z - meanZ);
		}
		sX = (float) Math.sqrt(sX / (float) (count - 1)); 
		sY = (float) Math.sqrt(sY / (float) (count - 1)); 
		sZ = (float) Math.sqrt(sZ / (float) (count - 1));
		Value standardDeviation = new Value(new float[]{sX, sY, sZ}, count);
		return standardDeviation;
	}
	
	private static ScalarValue getJerkProjectionOnGravityCyclic(long timeBackInNanos,
			SensorDataPosition cursor0, SensorDataPosition cursor2) {
		int positionOfValue = cursor0.positionOfValue;
		long t1 = cursor0.tN;
		byte[] data = cursor0.data;
		
		//Calculate mean values
		int count = 0;
		long t0 = 0;
		long amountOfTime = 0;
		float meanX = 0;
		float meanY = 0;
		float meanZ = 0;
		List<Float> listX = new ArrayList<Float>();
		List<Float> listY = new ArrayList<Float>();
		List<Float> listZ = new ArrayList<Float>();
		for ( int i = positionOfValue; i >= cursor0.minimalOffset; i-- ) {
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			count++;
			listX.add(x);
			meanX +=x;
			listY.add(y);
			meanY +=y;
			listZ.add(z);
			meanZ +=z;
		}
		if (count <= 0) {
			return null;
		}
		int count2 = 0;
		List<Float> anglesX = new ArrayList<Float>();
		List<Float> anglesY = new ArrayList<Float>();
		List<Float> anglesZ = new ArrayList<Float>();
		for ( int i = cursor2.positionOfValue; i >= cursor2.minimalOffset; i-- ) {
			ByteBuffer value = readBuffer(data, i);
			float anglex = value.getFloat();
			float angley = value.getFloat();
			float anglez = value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			count2++;
			anglesX.add(anglex);
			anglesY.add(angley);
			anglesZ.add(anglez);
		}
		if (count2 <= 0 || count != count2) {
			return null;
		}
		
		float jpr = 0;
		//Calculate projection
		for ( int i = 0; i < count; i++ ) {
			float x = listX.get(i);
			float y = listY.get(i);
			float z = listZ.get(i);
			float anglex = (anglesX.get(i) / 180f) * (float) Math.PI;
			float angley = (anglesY.get(i) / 180f) * (float) Math.PI;
			float anglez = (anglesZ.get(i) / 180f) * (float) Math.PI;
			float ex = (float) Math.cos(anglex);
			float ey = (float) Math.cos(angley);
			float ez = (float) Math.cos(anglez);
			float eSq = ex * ex + ey * ey + ez * ez;
			//assert eSq == 0.999999f;
			jpr += x * ex + y * ey + z * ez; 
		}
		ScalarValue jerkProjectionOnGravity = new ScalarValue(jpr, count);
		return jerkProjectionOnGravity;
	}
	
	private static MinMaxSqValue minMaxValueCalculationWithAveragingByNPointsLPF(long timeBackInNanos,
			SensorDataPosition cursor, int n, float alpha) {
		float minValue = Float.MAX_VALUE;
		float maxValue = 0;
		
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		float absValueSq = 0;
		
		int count = 0;
		long t0 = 0;
		long amountOfTime = 0;
		float valueSq = 0;
		for ( int i = positionOfValue; i >= cursor.minimalOffset; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			t0 = value.getLong();
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			if (i == positionOfValue) {
				absValueSq = x * x + y * y + z * z;
			} else {
				absValueSq = absValueSq + alpha * (x * x + y * y + z * z - absValueSq);
			}
			
			valueSq += absValueSq;			
			if (count % n != 0 ) {
				continue;
			}
			float avgValueSq = valueSq / (float) n;
			if ( avgValueSq < minValue ) {
				minValue = avgValueSq;
			}
			if ( avgValueSq > maxValue ) {
				maxValue = avgValueSq;
			}
			valueSq = 0;
		}
		MinMaxSqValue result = new MinMaxSqValue(minValue, maxValue, count);
		return result;
	}
	

	//Non blocking read of buffer (real-time-issues).
	public static MinMaxSqValue minMaxSqabsValueJ1(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 1, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		MinMaxSqValue result = minMaxValueCalculation(timeBackInNanos, cursor);
		return result;
	}

	//Non blocking read of buffer (real-time-issues).
	public static MinMaxValue minMaxValueLPF(SensorData sensorData, long timeInNanos, long timeBackInNanos, float alpha, int segmentIndex) {
		SensorDataPosition cursor = makeCursor(sensorData, segmentIndex, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		MinMaxValue result = getMinMaxValueLPF(timeBackInNanos, cursor, alpha);
		return result;
	}
	
	//Non blocking read of buffer (real-time-issues).
	public static MinMaxValue minMaxValueAverageByNPointsLPF(SensorData sensorData, int segmentIndex, long timeInNanos, long timeBackInNanos, float alpha, int n) {
		SensorDataPosition cursor = makeCursor(sensorData, segmentIndex, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		MinMaxValue result = getMinMaxValueWithAveragyByNPointsAndLPF(timeBackInNanos, cursor, alpha, n);
		return result;
	}
	
	//Non blocking read of buffer (real-time-issues).
	public static MinMaxValue minMaxValueAverageByNPointsLPFCyclic(SensorData sensorData, int segmentIndex, long timeInNanos, long timeBackInNanos, float alpha, int n) {
		SensorDataPosition cursor = makeCyclicCursor(sensorData, segmentIndex, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		MinMaxValue result = getMinMaxValueWithAveragyByNPointsAndLPFCyclic(timeBackInNanos, cursor, alpha, n);
		return result;
	}
	
	//Non blocking read of buffer (real-time-issues).
	public static Value standardDeviationCyclic(SensorData sensorData, int segmentIndex, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCyclicCursor(sensorData, segmentIndex, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		Value result = getStandardDeviationCyclic(timeBackInNanos, cursor);
		return result;
	}
	
	//Non blocking read of buffer (real-time-issues).
	public static ScalarValue jerkProjectionOnGravityCyclic(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor0 = makeCyclicCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		SensorDataPosition cursor2 = makeCyclicCursor(sensorData, 2, timeInNanos,
				timeBackInNanos);
		if ( cursor0 == null || cursor2 == null ) {
			return null;
		}
		ScalarValue result = getJerkProjectionOnGravityCyclic(timeBackInNanos, cursor0, cursor2);
		return result;
	}
	
	//Non blocking read of buffer (real-time-issues).
	//Available if debug mode == true
	public static MinMaxSqValue minMaxSqabsValueWithAveragingByNPointsLPF(SensorData sensorData, int segmentIndex, long timeInNanos, long timeBackInNanos, int n, float alpha) {
		SensorDataPosition cursor = makeCursor(sensorData, 1, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		MinMaxSqValue result = minMaxValueCalculationWithAveragingByNPointsLPF(timeBackInNanos, cursor, n, alpha);
		return result;
	}
	
	//Non blocking read of buffer (real-time-issues).
	public static AverageValue averageValueD1(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		int count = 0;
		float ix = 0;
		float iy = 0;
		float iz = 0;
		long t0 = t1;
		long amountOfTime = 0;
		float nextX = 0;
		float nextY = 0;
		float nextZ = 0;
		for ( int i = positionOfValue; i >= 0; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			long t = value.getLong();
			float timeInS = ((t0 - t) / 1000) / 1000000f;
			if ( timeInS > 0 ) {
				ix += (nextX - x) * timeInS;
				iy += (nextY - y) * timeInS;
				iz += (nextZ - z) * timeInS;
			}
			t0 = t;
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			nextX = x;
			nextY = y;
			nextZ = z;
		}
		//AverageValue avalue = new AverageValue(ix / (float) count, iy / (float) count, iz / (float) count, t0, t1, count);
		AverageValue avalue = new AverageValue(ix, iy, iz, t0, t1, count);
		return avalue;
	}
	
	//Non blocking read of buffer (real-time-issues).
	public static AverageValue averageValueI1(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		int count = 0;
		float ix = 0;
		float iy = 0;
		float iz = 0;
		long t0 = t1;
		long amountOfTime = 0;
		for ( int i = positionOfValue; i >= 0; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			long t = value.getLong();
			float timeInS = ((t0 - t) / 1000) / 1000000f;
			ix += x * timeInS;
			iy += y * timeInS;
			iz += z * timeInS;
			t0 = t;
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
		}
		AverageValue avalue = new AverageValue(ix / (float) count, iy / (float) count, iz / (float) count, t0, t1, count);
		return avalue;
	}

	//Non blocking read of buffer (real-time-issues).
	public static AverageValue valueI3(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		int count = 0;
		float ix = 0;
		float iy = 0;
		float iz = 0;
		float ox = 0;
		float oy = 0;
		float oz = 0;
		long t0 = t1;
		long amountOfTime = 0;
		for ( int i = positionOfValue; i >= 0; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			long t = value.getLong();
			float timeInS = ((t0 - t) / 1000) / 1000000f;
			float f1 = timeInS * timeInS / 2f;
			float f2 = timeInS * timeInS / 2f;
			ix += ox * f1 + ox * f2;
			iy += oy * f1 + oy * f2;
			iz += oz * f1 + oz * f2;
			t0 = t;
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			ox = x;
			oy = y;
			oz = z;
		}
		AverageValue avalue = new AverageValue(ix, iy, iz, t0, t1, count);
		return avalue;
	}
	
	//Non blocking read of buffer (real-time-issues).
	public static AverageValue valueI2(SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		int count = 0;
		float ix = 0;
		float iy = 0;
		float iz = 0;
		float ox = 0;
		float oy = 0;
		float oz = 0;
		long t0 = t1;
		long amountOfTime = 0;
		for ( int i = positionOfValue; i >= 0; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			long t = value.getLong();
			float timeInS = ((t0 - t) / 1000) / 1000000f;
			float f = timeInS * timeInS / 2f;
			ix += ox * f;
			iy += oy * f;
			iz += oz * f;
			t0 = t;
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
			ox = x;
			oy = y;
			oz = z;
		}
		AverageValue avalue = new AverageValue(ix, iy, iz, t0, t1, count);
		return avalue;
	}
	
	public static class Shift {
		public final float vertical;
		public final float horizontal1;
		public final float horizontal2;
		public final long t0;
		public final long t1;
		public final int count;
		public Shift(float vertical, float horizontal1, float horizontal2, long t0, long t1, int count) {
			super();
			this.vertical = vertical;
			this.horizontal1 = horizontal1;
			this.horizontal2 = horizontal2;
			this.t0 = t0;
			this.t1 = t1;
			this.count = count;
		}
	}

	//Non blocking read of buffer (real-time-issues).
	//e1, e2, e3 ortogonal basis norma(e1) == 1, norma(e2) == 1, norma(e3) == 1
	public static Shift calculateShift(SensorData sensorData, float[] e1, long timeInNanos, long timeBackInNanos, float[] e2, float e3[]) {
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		
		int positionOfValue = cursor.positionOfValue;
		long t1 = cursor.tN;
		byte[] data = cursor.data;
		
		float sqGravityNorma = 1f;//e1[0] * e1[0] + e1[1] * e1[1] + e1[2] * e1[2];
		float gravityNorma = 1f;//(float) Math.sqrt(e1[0] * e1[0] + e1[1] * e1[1] + e1[2] * e1[2]);
		
		int count = 0;
		float vertical = 0;//e1 projection
		float horizontal1 = 0;//e2 projection
		float horizontal2 = 0;//e3 projection
		long t0 = t1;
		long amountOfTime = 0;
		for ( int i = positionOfValue; i >= 0; i-- ) {
			count++;
			ByteBuffer value = readBuffer(data, i);
			float x = value.getFloat();
			float y = value.getFloat();
			float z = value.getFloat();
			long t = value.getLong();
			float timeInS = ((t0 - t) / 1000) / 1000000f;
//			float sqTimeInSDiv2 = timeInS * timeInS / 2f;
//			
//			float i2x = x * sqTimeInSDiv2 + x * timeInS;
//			float i2y = y * sqTimeInSDiv2 + y * timeInS;
//			float i2z = z * sqTimeInSDiv2 + z * timeInS;
			float f = timeInS * timeInS * timeInS / 6f;//Shift in coords Third integral from jerk
			x = Math.abs(x * f);  
			y = Math.abs(y * f);  
			z = Math.abs(z * f);  
			
			float scpXYZe1 = x * e1[0] + y * e1[1] + z * e1[2];
			//Value of projection on vector gravity
			vertical += scpXYZe1;// / gravityNorma;
			//Values in horizontal plane
			float scpXYZe2 = x * e2[0] + y * e2[1] + z * e2[2];
			horizontal1 += scpXYZe2;// / e2Norma;
			float scpXYZe3 = x * e3[0] + y * e3[1] + z * e3[2];
			horizontal2 += scpXYZe3;// / e3Norma;
			
			t0 = t;
			amountOfTime = t1 - t0;
			if ( amountOfTime > timeBackInNanos ) {//time - t > minusDeltaInMillis ) {
				break;
			}
		}
		Shift shift = new Shift(vertical, horizontal1, horizontal2, t0, t1, count);
		return shift;
	}


	private static SensorDataPosition makeCursor(SensorData sensorData, int segmentIndex,
			long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = new SensorDataPosition();
		cursor.data = sensorData.data;
		if ( cursor.data == null ) {
			return null;
		}
		//Constants
		final int structSize = 4 * 5;
		final int maxCountOfSegments = 3;
		final int structSizeFactor = sensorData.preparingMode ? structSize * maxCountOfSegments : structSize;  
		if ( cursor.data.length < structSizeFactor ) {
			return null;
		}
		final int maxValuesInBuffer = cursor.data.length / structSizeFactor;

		int countOfValues = sensorData.getCountOfValues();
		if ( countOfValues == 0 ) {
			return null;
		}
		int offsetInValues = countOfValues % maxValuesInBuffer;
		return cursorPositioning(timeInNanos, timeBackInNanos, cursor,
				segmentIndex * maxValuesInBuffer, (segmentIndex + 1) * maxValuesInBuffer, countOfValues, offsetInValues);
	}

	private static SensorDataPosition makeCyclicCursor(SensorData sensorData, int segmentIndex,
			long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = new SensorDataPosition();
		cursor.data = sensorData.data;
		if ( cursor.data == null ) {
			return null;
		}
		//Constants
		final int structSize = 4 * 5;
		final int maxCountOfSegments = 3;
		final int structSizeFactor = sensorData.preparingMode ? structSize * maxCountOfSegments : structSize;  
		if ( cursor.data.length < structSizeFactor ) {
			return null;
		}
		final int maxValuesInBuffer = cursor.data.length / structSizeFactor;

		int countOfValues = sensorData.getCountOfValues();
		if ( countOfValues == 0 ) {
			return null;
		}
		int relativeOffsetInValues = (countOfValues - 1) % maxValuesInBuffer;
		int maxOffsetInValues = (segmentIndex + 1) * maxValuesInBuffer - 1;
		cursor = cursorPositioningCyclic(timeInNanos, timeBackInNanos, cursor,
				segmentIndex * maxValuesInBuffer, maxOffsetInValues, relativeOffsetInValues);
		if (cursor == null || cursor.positionOfValue < 0) {
			return null;
		}
		//Cyclic case detect
		if (maxValuesInBuffer < countOfValues) {
			ByteBuffer lastPositionValue = readBuffer(cursor.data, maxOffsetInValues);
			lastPositionValue.position(4 * 3);//Read x, y, z
			long lastPositionValueTimestamp = lastPositionValue.getLong();;
			cursor.cyclic = lastPositionValueTimestamp > timeInNanos - timeBackInNanos;
			//If cursor cyclic and position of value is zero
			//Try search from the end to initialCursorPosition
		}
		return cursor;
	}

	private static SensorDataPosition cursorPositioning(long timeInNanos,
			long timeBackInNanos, SensorDataPosition cursor,
			int minOffsetInValues, int maxOffsetInValues, int countOfValues, int offsetInValues) {
		//boolean overload = countOfValues /maxValuesInBuffer == 0;
		
		if ( offsetInValues == 0 && maxOffsetInValues < countOfValues) {
			return null;
		}
		
		cursor.minimalOffset = minOffsetInValues;
		cursor.maximalPosition = maxOffsetInValues;
		
		//If buffer big, change countOfValues not critical
		if ( offsetInValues == 0 ){
			cursor.positionOfValue = maxOffsetInValues - 1;
		} else {
			cursor.positionOfValue = minOffsetInValues + offsetInValues - 1;
		}
		ByteBuffer lastValue = readBuffer(cursor.data, cursor.positionOfValue);
		lastValue.position(4 * 3);//Read x, y, z
		cursor.tN = lastValue.getLong();;
		long lastTime = cursor.tN;
		
		//Check time bounds
		//if ( baseTimeNanos > timeInNanos ) {
		//	return null;
		//}
		if ( lastTime < timeInNanos - timeBackInNanos ) {
			return null;//Cyclic case detect
		}
		//Search offset in values for time
		if ( timeInNanos < lastTime ) {
			for ( int i = cursor.positionOfValue - 1; i >= minOffsetInValues; i-- ) {
				ByteBuffer value = readBuffer(cursor.data, i);
				value.position(4 * 3);//Read x, y, z
				cursor.tN = value.getLong();
				lastTime = cursor.tN;
				if ( timeInNanos >= lastTime ) {
					cursor.positionOfValue = i;
					break;
				}
			}
		}
		return cursor;
	}

	private static SensorDataPosition cursorPositioningCyclic(long timeInNanos,
			long timeBackInNanos, SensorDataPosition cursor,
			int minOffsetInValues, int maxOffsetInValues, int relativeOffsetInValues) {
		cursor.minimalOffset = minOffsetInValues;
		cursor.maximalPosition = maxOffsetInValues;
		assert relativeOffsetInValues >= 0 &&relativeOffsetInValues <= maxOffsetInValues - minOffsetInValues: "Argument constraint violation";  
		cursor.positionOfValue = minOffsetInValues + relativeOffsetInValues;
		ByteBuffer lastValue = readBuffer(cursor.data, cursor.positionOfValue);
		lastValue.position(4 * 3);//Read x, y, z
		cursor.tN = lastValue.getLong();;
		long lastTime = cursor.tN;
		
		//Data out of horizon case
		if (lastTime < timeInNanos - timeBackInNanos) {
			return null;
		}
		//Search offset in values for time
		if ( timeInNanos < lastTime ) {
			int i = cursor.positionOfValue;
			for ( ; i >= minOffsetInValues; i-- ) {
				ByteBuffer value = readBuffer(cursor.data, i);
				value.position(4 * 3);//Read x, y, z
				cursor.tN = value.getLong();
				lastTime = cursor.tN;
				if ( timeInNanos >= lastTime ) {
					cursor.positionOfValue = i;
					break;
				}
			}
			if (i < minOffsetInValues) {
				cursor.positionOfValue = -1;
			}
		}
		return cursor;
	}

	public static ByteBuffer readBuffer(byte[] data, int offsetInValues) {
		ByteBuffer buff = ByteBuffer.wrap(data);
		byte[] value = new byte[4 * 5];
		buff.position(offsetInValues * (4 * 5));
		buff.get(value, 0, 4 * 5);
		ByteBuffer lastValue = ByteBuffer.wrap(value);
		return lastValue;
	}

	/**
	 * Одинарное интегрирование по времени.
	 * @param sensorData данные сенсора.
	 * @param timeInNanos время в миллисекундах.
	 * @param timeBackInNanos дельта в прошлое в  миллисекундах.
	 * @return
	 */
	public static AverageValue singleIntegratedValue(
			SensorData sensorData, long timeInNanos, long timeBackInNanos) {
		SensorDataPosition cursor = makeCursor(sensorData, 0, timeInNanos,
				timeBackInNanos);
		if ( cursor == null ) {
			return null;
		}
		
		int positionOfValue = cursor.positionOfValue;
		long tN = cursor.tN;
		byte[] data = cursor.data;
		
		if ( positionOfValue < 3 ) {//Simpson method use quadratic interpolation
			return null;
		}
		
		ByteBuffer value = readBuffer(data, positionOfValue);
		float x = value.getFloat();
		float y = value.getFloat();
		float z = value.getFloat();
		long t0 = value.getLong();
		
		long amountOfTime = 0;
		long tNminus2 = tN;
		int count = 0;
		
		for ( int i = positionOfValue - 1; i >= 0; i-- ) {
			count++;
			amountOfTime = tN - t0;
			if ( amountOfTime > timeBackInNanos ) {//timeInMillis - t0 > minusDeltaInMillis ) {
				break;
			}
			value = readBuffer(data, i);
			int r = positionOfValue - i;
			final float factor;
			if ( amountOfTime == timeBackInNanos ) {
				factor = 1f;
			} else if (	r % 2 == 0 ) {
				factor = 2f;
			} else {
				factor = 4f;
			}
			x += value.getFloat() * factor;
			y += value.getFloat() * factor;
			z += value.getFloat() * factor;
			t0 = value.getLong();
			
			if ( positionOfValue - i == 2 ) {//TODO: investigate interaction of h production on every term in formula 
				tNminus2 = t0;
			}
		}
		float h = (tN - tNminus2) / 1000000000f;//millis to second
		
		AverageValue sivalue = new AverageValue( x * h / 6f, y * h / 6f, z * h / 6f, t0, tN, count);
		return sivalue;
	}

	public static long getLastTimestamp(SensorData sensorData) {
		byte[] data = sensorData.data;
		int minCountOfValues;
		if (sensorData.preparingMode) {
			minCountOfValues = data.length / (4 * 5 * 3);
		} else {
			minCountOfValues = data.length / (4 * 5);
		}
		minCountOfValues = Math.min(minCountOfValues, sensorData.getCountOfValues());
		int pos = -1;
		long maxTimestamp = 0;
		//Read first segment only
		for (int i = 0; i < minCountOfValues; i ++) {
			ByteBuffer value = readBuffer(data, i);
			value.position(4 * 3);//Read x, y, z
			long timestamp = value.getLong();
			if (maxTimestamp < timestamp) {
				maxTimestamp = timestamp;
				pos = i;
			}
		}
		if (pos < 0) {
			return 0;
		}
		return maxTimestamp;
	}

	public static int getFirstPosition(SensorData sensorData) {
		byte[] data = sensorData.data;
		int minCountOfValues;
		if (sensorData.preparingMode) {
			minCountOfValues = data.length / (4 * 5 * 3);
		} else {
			minCountOfValues = data.length / (4 * 5);
		}
		minCountOfValues = Math.min(minCountOfValues, sensorData.getCountOfValues());
		int pos = -1;
		long minTimestamp = Long.MAX_VALUE;
		//Read first segment only
		for (int i = 0; i < minCountOfValues; i ++) {
			ByteBuffer value = readBuffer(data, i);
			value.position(4 * 3);//Read x, y, z
			long timestamp = value.getLong();
			if (minTimestamp > timestamp) {
				minTimestamp = timestamp;
				pos = i;
			}
		}
		if (pos < 0) {
			throw new IllegalStateException("Position not found");
		}
		return pos;
	}
	
	/**
	 * Input data for discrete fourier transformation.
	 */
	public static class VectorData2 {
		public final double[] values0;
		public final double[] values1;
		public final int count;
		public final long t;
		public VectorData2(double[] values0, double[] values1, int count, long t) {
			super();
			this.values0 = values0;
			this.values1 = values1;
			this.count = count;
			this.t = t;
		}
	}

	public static VectorData2 getData(SensorData sensorData, int segmentIndex, int vectorIndex0, int vectorIndex1,
			long timeInNanos, long timeBackNs) {
		SensorDataPosition cursor = makeCursor(sensorData, segmentIndex, timeInNanos,
				timeBackNs);
		if ( cursor == null ) {
			return null;
		}
		VectorData2 result = getData(timeBackNs, vectorIndex0, vectorIndex1, cursor);
		return result;
	}
}

interface TimestampSeeker {
	long getTimestamp(ByteBuffer byteBuffer);
}

/**
 * Cyclic buffer
 * @author ivict
 *
 */
class CBuffer {
	static final String BUFF_LENGTH_NOT_CORRELATED_WITH_STRUCT_SIZE = "Buffer length not adequate to size of structures";
	static final String INDEX_OUT_OF_BOUNDS = "Index out of bounds";
	static final String ILLEGAL_BOUNDS_OF_INTERVAL = "Illegal bounds of interval";
	static final String BUFFER_TOO_SMALL_OR_STRUCTURE_OF_DATA_TOO_BIG = "Too small buffer or too big structure size";
	static final String INITIAL_POSITION_OUT_OF_BOUNDS = "Initial position out of bounds";
	static final String INVALID_STRUCT_SIZE = "Invalid size of data structure";
	/**
	 * Inclusive possition.
	 */
	final int minPos;
	/**
	 * Exclusive position.
	 */
	final int maxPos;
	int pos;
	
	final byte[] buff;
	final int structSize;

	/**
	 * Create accessor to cyclic buffer.
	 * @param minPos minimal position inclusive
	 * @param maxPos maximal position exclusive
	 * @param pos heuristic position (for start seek)
	 * @param buff undegraund buffer
	 * @param structSize size of structure in bytes, real buffer capacity structSize * maxPos
	 */
	public CBuffer(int minPos, int maxPos, int pos, byte[] buff, int structSize) {
		if (buff.length % structSize > 0) {
			throw new IllegalArgumentException(BUFF_LENGTH_NOT_CORRELATED_WITH_STRUCT_SIZE);
		}
		int length = buff.length / structSize;
		if (minPos < 0) {
			throw new IllegalArgumentException(INDEX_OUT_OF_BOUNDS);
		}
		if (minPos >= length) {
			throw new IllegalArgumentException(INDEX_OUT_OF_BOUNDS);
		}
		if (minPos > maxPos) {
			throw new IllegalArgumentException(ILLEGAL_BOUNDS_OF_INTERVAL);
		}
		if (maxPos > length) {
			throw new IllegalArgumentException(INDEX_OUT_OF_BOUNDS);
		}
		if (structSize < 0) {
			throw new IllegalArgumentException(INVALID_STRUCT_SIZE);
		}
		if (maxPos - minPos < 1) {
			throw new IllegalArgumentException(BUFFER_TOO_SMALL_OR_STRUCTURE_OF_DATA_TOO_BIG);
		}
		if (pos < minPos || pos >= maxPos) {
			throw new IllegalArgumentException(INITIAL_POSITION_OUT_OF_BOUNDS);
		}
		this.minPos = minPos;
		this.maxPos = maxPos;
		this.pos = pos;
		this.buff = buff;
		this.structSize = structSize;
	}
	
	public Iterable<ByteBuffer> intervalOf(long timestamp0, long timestamp1, TimestampSeeker timestampSeeker) {
		if (timestamp1 < timestamp0) {
			throw new IllegalArgumentException(ILLEGAL_BOUNDS_OF_INTERVAL);
		}
		final long t0 = timestamp0;
		final long t1 = timestamp1;
		final TimestampSeeker tSeeker = timestampSeeker;
		return new Iterable<ByteBuffer>() {

			@Override
			public Iterator<ByteBuffer> iterator() {
				return new TimeIntervalIterator(t0, t1, tSeeker);
			}
			
		};
	}
	
	ByteBuffer readBuffer(int pos) {
		ByteBuffer buff = ByteBuffer.wrap(this.buff);
		byte[] value = new byte[structSize];
		buff.position(pos * structSize);
		buff.get(value, 0, structSize);
		return ByteBuffer.wrap(value);
	}
	
	class TimeIntervalIterator implements Iterator<ByteBuffer> {
		final long t0;
		final long t1;
		final TimestampSeeker tSeeker;
		private int i;
		private long t;
		private boolean hasNext;
		private boolean hasPrev;
		private int count;
		
		TimeIntervalIterator(long t0, long t1, TimestampSeeker tSeeker) {
			this.t0 = t0;
			this.t1 = t1;
			this.tSeeker = tSeeker;
			//Search t0
			//Heuristic position
			i = pos;
			ByteBuffer e = readBuffer(i);
			t = safetyGetTimestamp(e);
			hasPrev = t0 < t;
			//Exclusive situation search to bigger index 
			if (!hasPrev && t < t1) {
				motonicSearchRightT0(maxPos);
				if (i == maxPos - 1) {
					i = minPos;
					motonicSearchRightT0(pos);
				}
				if (i == pos) {
					hasNext = false;
					return;
				}
				return;
			}
			//Seeking to lower index
			strictlyMonotonicSearchLeftT0(minPos);
			//If next have prev then cyclic shift
			if (hasPrev) {
				int prevI = i;
				i = maxPos;
				strictlyMonotonicSearchLeftT0(pos);
				//Check if search failed 
				if (i == pos) {
					hasNext = false;
					return;
				}
				//Restore index if search stop on maxPos
				if (i == maxPos) {
					i = prevI;
				}
			}
			hasNext = t < t1;
		}

		private void strictlyMonotonicSearchLeftT0(int minPos) {
			ByteBuffer e;
			int prevI = i;
			long prevT = t;
			while (hasPrev && --i >= minPos) {
				e = readBuffer(i);
				t = safetyGetTimestamp(e);
				//Check monotonic condition violation
				if (prevT < t) {
					i = prevI;
					t = prevT;
					hasPrev = false;
					return;
				}
				prevI = i;
				prevT = t;
				hasPrev = t0 < t;
			}
			//Index and t correction
			i = prevI;
			t = prevT;
			hasPrev = t0 < t;
		}

		private void motonicSearchRightT0(int maxPos) {
			ByteBuffer e;
			int prevI = i;
			long prevT = t;
			while (!hasPrev && ++i < maxPos) {
				prevI = i - 1;
				prevT = t;
				e = readBuffer(i);
				t = safetyGetTimestamp(e);
				//Check monotonic condition violation
				if (prevT > t) {
					i = prevI;
					t = prevT;
					hasPrev = false;
					return;
				}
				hasPrev = t0 < t;
			}
			//Index and t correction
			i = prevI;
			t = prevT;
			hasNext = t <= t1;
		}

		private long safetyGetTimestamp(ByteBuffer byteBuffer) {
			long timestamp = tSeeker.getTimestamp(byteBuffer);
			byteBuffer.rewind();
			return timestamp;
		}

		private void throwOnConcurrentModification() {
			if ( t > t1 ) {
				throw new ConcurrentModificationException();
			}
		}

		@Override
		public boolean hasNext() {
			return hasNext;
		}

		@Override
		public ByteBuffer next() {
			ByteBuffer e = readBuffer(i++);
			t = safetyGetTimestamp(e);
			throwOnConcurrentModification();			
			hasNext = t < t1 && maxPos - minPos > ++count;
			//Cyclic
			if (i >= maxPos) {
				i = minPos;
			}
			return e;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Read only iterator");
		}
		
	}

	public Object all() {
		return new Iterable<ByteBuffer>() {

			@Override
			public Iterator<ByteBuffer> iterator() {
				return new SimpleIterator();
			}
			
		};
	}
	
	class SimpleIterator implements Iterator<ByteBuffer> {
		private int index;
		private int count;
		
		SimpleIterator() {
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public ByteBuffer next() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
	}
	
	
}
