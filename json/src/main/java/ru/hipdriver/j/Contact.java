/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IContact;

/**
 * JSON Object, контакт пользователя
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact implements IContact {

	@JsonProperty(value = "id")
	public long id;
	
	@JsonProperty(value = "user-id")
	public int userId;
	
	@JsonProperty(value = "contact-type-id")
	public short contactTypeId;
	
	@JsonProperty(value = "name")
	public String name;

	/**
	 * @see ru.hipdriver.i.IContact#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/**
	 * @see ru.hipdriver.i.IContact#setId(long)
	 */
	@Override
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @see ru.hipdriver.i.IContact#getUserId()
	 */
	@Override
	public int getUserId() {
		return userId;
	}

	/**
	 * @see ru.hipdriver.i.IContact#setUserId(int)
	 */
	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @see ru.hipdriver.i.IContact#getContactTypeId()
	 */
	@Override
	public short getContactTypeId() {
		return contactTypeId;
	}

	/**
	 * @see ru.hipdriver.i.IContact#setContactTypeId(short)
	 */
	@Override
	public void setContactTypeId(short contactTypeId) {
		this.contactTypeId = contactTypeId;
	}

	/**
	 * @see ru.hipdriver.i.IContact#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @see ru.hipdriver.i.IContact#setName(String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
}
