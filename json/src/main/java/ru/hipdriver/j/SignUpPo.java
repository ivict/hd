/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.ISignUpPo;

/**
 * <b>Запрос на регистрацию</b> пользователя drupal.
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignUpPo implements ISignUpPo {
	@JsonProperty(value = "user-name")
	public String name;

	@JsonProperty(value = "e-mail")
	public String email;

	@JsonProperty(value = "password-hash")
	public byte[] passwordHash;

	@JsonProperty(value = "id")
	public long id;

	@JsonProperty(value = "referral-id")
	public long referralId;

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public byte[] getPasswordHash() {
		return passwordHash;
	}

	@Override
	public void setPasswordHash(byte[] passwordHash) {
		this.passwordHash = passwordHash;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public long getReferralId() {
		return referralId;
	}

	@Override
	public void setReferralId(long referredId) {
		this.referralId = referredId;
	}

}
