/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IPlacemark;

/**
 * JSON Object, метка для маршрута в состоянии тревоги (содержит время нахождения в заданной локации).
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlertPlacemark extends StandardResponse implements IPlacemark {
	
	@JsonProperty(value="timestamp")
	public Date timestamp;
	
	@JsonProperty(value="placemark")
	public float[] placemark;

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @see ru.hipdriver.i.IPlacemark#getPlacemark()
	 */
	@Override
	public float[] getPlacemark() {
		return placemark;
	}

	/**
	 * @see ru.hipdriver.i.IPlacemark#setPlacemark(float[])
	 */
	@Override
	public void setPlacemark(float[] placemark) {
		this.placemark = placemark;
	}
	
}
