/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.ISignInMaResponse;
import ru.hipdriver.i.IUserLimit;

/**
 * <b>Ответ сервера мобильному агенту</b>, содержит как сессионный ключ
 * так и настройки профиля мобильного агента.
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignInMaResponse extends StandardResponse implements ISignInMaResponse {

	@JsonProperty(value = "a")
	public long a;
	
	@JsonProperty(value = "mobile-agent-id")
	public long mobileAgentId;
	
	@JsonProperty(value = "delay-push-alert-event-in-seconds")
	public int delayPushAlertEventInSeconds = IMobileAgentProfile.DEFAULT_DELAY_PUSH_ALERT_EVENT_IN_SECONDS;

	@JsonProperty(value = "delay-where-am-i-request-in-seconds")
	public int delayWhereAmIRequestInSeconds = IMobileAgentProfile.DEFAULT_DELAY_WHERE_AM_I_REQUEST_IN_SECONDS;

	@JsonProperty(value = "enable-debug-mode")
	public boolean enableDebugMode = IMobileAgentProfile.DEFAULT_ENABLE_DEBUG_MODE;

	@JsonProperty(value = "asd-max-jerk")
	public float asdMaxJerk = IMobileAgentProfile.DEFAULT_ASD_MAX_JERK;

	@JsonProperty(value = "asd-max-acceleration")
	public float asdMaxAcceleration = IMobileAgentProfile.DEFAULT_ASD_MAX_ACCELERATION;

	@JsonProperty(value = "asd-max-angle")
	public float asdMaxAngle = IMobileAgentProfile.DEFAULT_ASD_MAX_ANGLE;
	
	@JsonProperty(value = "enable-energy-saving-mode")
	public boolean enableEnergySavingMode = IMobileAgentProfile.DEFAULT_ENABLE_ENERGY_SAVING_MODE;
	
	@JsonProperty(value = "watchdog-service-start-delay-in-seconds")
	public int watchdogServiceStartDelayInSeconds = IMobileAgentProfile.DEFAULT_WATCHDOG_SERVICE_START_DELAY_IN_SECONDS;
	
	@JsonProperty(value = "properties")
	public Map<String, Object> properties;
	
	@JsonProperty(value = IUserLimit.TIME_LIMITED)
	public boolean timeLimited = IUserLimit.DEFAULT_TIME_LIMITED;

	@JsonProperty(value = IUserLimit.MAX_DEVICES)
	public int maxDevices = IUserLimit.DEFAULT_MAX_DEVICES;

	@JsonProperty(value = IMobileAgentProfile.CONTROL_MODE_ID)
	public short controlModeId = IMobileAgentProfile.DEFAULT_CONTROL_MODE_ID;

	@JsonProperty(value = IMobileAgentProfile.MAP_PROVIDER_ID)
	public short mapProviderId = IMobileAgentProfile.DEFAULT_MAP_PROVIDER_ID;

	/**
	 * @see ru.hipdriver.i.ISid#getA()
	 */
	@Override
	public long getA() {
		return a;
	}

	/**
	 * @see ru.hipdriver.i.ISid#setA()
	 */
	@Override
	public void setA(long a) {
		this.a = a;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getMobileAgentId()
	 */
	@Override
	public long getMobileAgentId() {
		return mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setMobileAgentId(long)
	 */
	@Override
	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getDelayPushAlertEventInSeconds()
	 */
	@Override
	public int getDelayPushAlertEventInSeconds() {
		return delayPushAlertEventInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setDelayPushAlertEventInSeconds(int)
	 */
	@Override
	public void setDelayPushAlertEventInSeconds(int delayPushAlertEventInSeconds) {
		this.delayPushAlertEventInSeconds = delayPushAlertEventInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getDelayWhereAmIRequestInSeconds()
	 */
	@Override
	public int getDelayWhereAmIRequestInSeconds() {
		return delayWhereAmIRequestInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setDelayWhereAmIRequestInSeconds(int)
	 */
	@Override
	public void setDelayWhereAmIRequestInSeconds(int delayWhereAmIRequestInSeconds) {
		this.delayWhereAmIRequestInSeconds = delayWhereAmIRequestInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#isEnableDebugMode()
	 */
	@Override
	public boolean isEnableDebugMode() {
		return enableDebugMode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setEnableDebugMode(boolean)
	 */
	@Override
	public void setEnableDebugMode(boolean enableDebugMode) {
		this.enableDebugMode = enableDebugMode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getAsdMaxJerk()
	 */
	@Override
	public float getAsdMaxJerk() {
		return asdMaxJerk;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setAsdMaxJerk(float)
	 */
	@Override
	public void setAsdMaxJerk(float asdMaxJerk) {
		this.asdMaxJerk = asdMaxJerk;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getAsdMaxAcceleration()
	 */
	@Override
	public float getAsdMaxAcceleration() {
		return asdMaxAcceleration;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setAsdMaxAcceleration(float)
	 */
	@Override
	public void setAsdMaxAcceleration(float asdMaxAcceleration) {
		this.asdMaxAcceleration = asdMaxAcceleration;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getAsdMaxAngle()
	 */
	@Override
	public float getAsdMaxAngle() {
		return asdMaxAngle;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setAsdMaxAngle(float)
	 */
	@Override
	public void setAsdMaxAngle(float asdMaxAngle) {
		this.asdMaxAngle = asdMaxAngle;
	}

	/**
	 * @see ru.hipdriver.i.IProperties#getProperties()
	 */
	@Override
	public Map<String, Object> getProperties() {
		return properties;
	}

	/**
	 * @see ru.hipdriver.i.IProperties#setProperties(Map)
	 */
	@Override
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#isEnableEnergySavingMode()
	 */
	@Override
	public boolean isEnableEnergySavingMode() {
		return enableEnergySavingMode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setEnableEnergySavingMode(boolean)
	 */
	@Override
	public void setEnableEnergySavingMode(boolean enableEnergySavingMode) {
		this.enableEnergySavingMode = enableEnergySavingMode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getWatchdogServiceStartDelayInSeconds()
	 */
	@Override
	public int getWatchdogServiceStartDelayInSeconds() {
		return watchdogServiceStartDelayInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setWatchdogServiceStartDelayInSeconds(int)
	 */
	@Override
	public void setWatchdogServiceStartDelayInSeconds(
			int watchdogServiceStartDelayInSeconds) {
		this.watchdogServiceStartDelayInSeconds = watchdogServiceStartDelayInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#isTimeLimited()
	 */
	@Override
	public boolean isTimeLimited() {
		return timeLimited;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#setTimeLimited(boolean)
	 */
	@Override
	public void setTimeLimited(boolean timeLimited) {
		this.timeLimited = timeLimited;
	}
	
	/**
	 * @see ru.hipdriver.i.IUserLimit#getMaxDevices()
	 */
	@Override
	public int getMaxDevices() {
		return maxDevices;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#setMaxDevices(int)
	 */
	@Override
	public void setMaxDevices(int maxDevices) {
		this.maxDevices = maxDevices;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getControlModeId()
	 */
	public short getControlModeId() {
		return controlModeId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setControlModeId(short)
	 */
	public void setControlModeId(short controlModeId) {
		this.controlModeId = controlModeId;
	}
	
	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getMapProviderId()
	 */
	public short getMapProviderId() {
		return mapProviderId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setMapProviderId(short)
	 */
	public void setMapProviderId(short mapProviderId) {
		this.mapProviderId = mapProviderId;
	}
	
}
