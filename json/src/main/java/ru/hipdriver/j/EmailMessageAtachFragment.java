/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IEmailMessageAtachFragment;

/**
 * JSON Object, фрагмент атача к письму
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailMessageAtachFragment implements IEmailMessageAtachFragment {
	public static final String FRAGMENT_NUMBER = "fragment-number";
	
	@JsonProperty(value = "identity")
	public int identity;
	
	@JsonProperty(value = "message-atach-identity")
	public int messageAtachIdentity;
	
	@JsonProperty(value = FRAGMENT_NUMBER)
	public int fragmentNumber;
	
	@JsonProperty(value = "content")
	public byte[] content;
	
	/**
	 * @see ru.hipdriver.i.IEmailObject#getIdentity()
	 */
	public int getIdentity() {
		return identity;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailObject#setIdentity(int)
	 */
	public void setIdentity(int identity) {
		this.identity = identity;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtachFragment#getMessageAtachIdentity()
	 */
	public int getMessageAtachIdentity() {
		return messageAtachIdentity;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtachFragment#setMessageAtachIdentity(int)
	 */
	public void setMessageAtachIdentity(int messageAtachIdentity) {
		this.messageAtachIdentity = messageAtachIdentity;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtachFragment#getFragmentNumber()
	 */
	public int getFragmentNumber() {
		return fragmentNumber;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtachFragment#setFragmentNumber(int)
	 */
	public void setFragmentNumber(int fragmentNumber) {
		this.fragmentNumber = fragmentNumber;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtachFragment#getContent()
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtachFragment#setContent(byte[])
	 */
	public void setContent(byte[] content) {
		this.content = content;
	}

}
