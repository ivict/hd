package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IEventAtach;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventWithAtach extends Event implements IEventAtach {
	
	@JsonProperty(value = "description")
	public String description;

	@JsonProperty(value = "content")
	public byte[] content;

	@JsonProperty(value = "content-check-sum")
	public long contentCheckSum;

	@Override
	public long getEventId() {
		return id;
	}

	@Override
	public void setEventId(long eventId) {
		this.id = eventId;
	}

	@Override
	public byte[] getContent() {
		return content;
	}

	@Override
	public void setContent(byte[] content) {
		this.content = content;
	}

	@Override
	public long getContentCheckSum() {
		return contentCheckSum;
	}

	@Override
	public void setContentCheckSum(long contentCheckSum) {
		this.contentCheckSum = contentCheckSum;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

}
