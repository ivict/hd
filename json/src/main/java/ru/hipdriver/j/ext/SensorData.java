/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j.ext;

import java.util.Arrays;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * <b>Данные сенсора</b>, могут быть использованы для отладочных целей.
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SensorData {

	@JsonProperty(value = "max-range")
	public float maximumRange;

	@JsonProperty(value = "min-delay")
	public int minDelay;

	@JsonProperty(value = "name")
	public String name;

	@JsonProperty(value = "power")
	public float power;
	
	@JsonProperty(value = "resolution")
	public float resolution;

	@JsonProperty(value = "type")
	public int type;

	@JsonProperty(value = "vendor")
	public String vendor;

	@JsonProperty(value = "version")
	public int version;

	@JsonProperty(value = "base-time")
	public long baseTime;
	
	@JsonProperty(value = "alert-time-ns")
	public long alertTimeInNs;
	
	@JsonProperty(value = "data")
	public byte[] data;
	
	@JsonIgnore
	public int countOfValues;

	/**
	 * Internal values for calculation integrals J1 and J2
	 */
	@JsonIgnore
	public float xNMinus2;
	@JsonIgnore
	public float yNMinus2;
	@JsonIgnore
	public float zNMinus2;
	@JsonIgnore
	public long tNMinus2;
	@JsonIgnore
	public float xNMinus1;
	@JsonIgnore
	public float yNMinus1;
	@JsonIgnore
	public float zNMinus1;
	@JsonIgnore
	public float[] J1 = new float[3];//Usual three point calculation (Simpson method) 
	@JsonIgnore
	public float xNMinus4;
	@JsonIgnore
	public float yNMinus4;
	@JsonIgnore
	public float zNMinus4;
	@JsonIgnore
	public float[] J12N = new float[3];//Reduce points for calculation (use for apply Richardson extrapolation based on Runge rule)
	@JsonIgnore
	public float[] J1ERR = new float[3];//Runge error evaluation for J1 calculation
	@JsonIgnore
	public float[] J1Minus4 = new float[3];
	@JsonIgnore
	public long tNMinus4;
	@JsonIgnore
	public float[] J1Minus2 = new float[3];
	@JsonIgnore
	//public float[] J2 = new float[3];
	public float[] ANGLES = new float[3];
	@JsonProperty(value = "prepared-mode" )
	public boolean preparingMode;
	
	@JsonIgnore
	public float[] a = new float[3];//Last acceleration 
	
	
	public float getMaximumRange() {
		return maximumRange;
	}

	public void setMaximumRange(float maximumRange) {
		this.maximumRange = maximumRange;
	}

	public int getMinDelay() {
		return minDelay;
	}

	public void setMinDelay(int minDelay) {
		this.minDelay = minDelay;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPower() {
		return power;
	}

	public void setPower(float power) {
		this.power = power;
	}

	public float getResolution() {
		return resolution;
	}

	public void setResolution(float resolution) {
		this.resolution = resolution;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public long getBaseTime() {
		return baseTime;
	}

	public void setBaseTime(long baseTime) {
		this.baseTime = baseTime;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public int getCountOfValues() {
		return countOfValues;
	}

	//TODO: extract set zero case and refactoring into reset method.
	public void setCountOfValues(int countOfValues) {
		this.countOfValues = countOfValues;
	}

	public void reset() {
		setCountOfValues(0);
		Arrays.fill(J1, 0f);
		Arrays.fill(J12N, 0f);
		Arrays.fill(J1ERR, 0f);
		Arrays.fill(J1Minus4, 0f);
		Arrays.fill(J1Minus2, 0f);
	}

	@Deprecated
	@JsonProperty(value = "debug-mode" )
	public void setDebugMode(boolean debugMode) {
		this.preparingMode = debugMode;
	}
	

}
