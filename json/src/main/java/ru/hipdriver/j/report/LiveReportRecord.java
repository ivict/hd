/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j.report;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.report.ILiveReportRecord;

/**
 * JSON Object, линейный набор данных для отчета.
 * В простых отчетах соответствует одной строке.
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LiveReportRecord implements ILiveReportRecord {
	
	@JsonProperty(value="key")
	public Object key;
	
	@JsonProperty(value="data")
	public Map<String, String> data;
	
	public LiveReportRecord() {
	}
	
	public LiveReportRecord(Object key, Map<String, String> data) {
		super();
		this.key = key;
		this.data = data;
	}

	/**
	 * @see ru.hipdriver.i.reports.ILiveReportRecord#getKey()
	 */
	@Override
	public Object getKey() {
		return key;
	}

	/**
	 * @see ru.hipdriver.i.reports.ILiveReportRecord#setKey(Object)
	 */
	@Override
	public void setKey(Object key) {
		this.key = key;
	}

	/**
	 * @see ru.hipdriver.i.reports.ILiveReportRecord#getData()
	 */
	@Override
	public Map<String, String> getData() {
		return data;
	}

	/**
	 * @see ru.hipdriver.i.reports.ILiveReportRecord#setData(Map)
	 */
	@Override
	public void setData(Map<String, String> data) {
		this.data = data;
	}

}
