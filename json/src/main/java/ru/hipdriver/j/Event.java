/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.g.Locations;
import ru.hipdriver.i.IEvent;
import ru.hipdriver.i.ILocation;

/**
 * JSON Object, зеркалка для Event из пакета entity
 * 
 * @see ru.hipdriver.kernel.entity.Event
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Event implements IEvent {

	@JsonIgnore
	public long id;
	@JsonIgnore
	public int userId;
	@JsonIgnore
	public long mobileAgentId;
	@JsonProperty(value = "cls")
	public short eventClassId;
	@JsonProperty(value = "time")
	public Date time;
	//Latitude
	@JsonIgnore
	public int lat;
	//Longitude
	@JsonIgnore
	public int lon;
	//Altitude
	@JsonIgnore
	public int alt = ILocation.UNKNOWN_ALTITUDE;
	//Accuracy (in meters)
	@JsonIgnore
	public int acc;
	//Mobile Country Code
	@JsonIgnore
	public int mcc;
	//Mobile Network Code
	@JsonIgnore
	public int mnc;
	//Location Area Code
	@JsonIgnore
	public int lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
	//Cell Id
	@JsonIgnore
	public int cid = ILocation.UNKNOWN_CELL_ID;
	@JsonProperty(value = "gsm")
	public byte[] gsmLocation;
	@JsonProperty(value = "gps")
	public byte[] gpsLocation;
	@JsonIgnore
	public Date receivedTime;

	/**
     * @see ru.hipdriver.i.IEvent#getId()
     */
	@Override
	public long getId() {
		return id;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setId(long)
     */
	@Override
	public void setId(long id) {
		this.id = id;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getUserId()
     */
	@Override
	public int getUserId() {
		return userId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setUserId(int)
     */
	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getMobileAgent()
     */
	@Override
	public long getMobileAgentId() {
		return mobileAgentId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setId(long)
     */
	@Override
	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getEventClassId()
     */
	@Override
	public short getEventClassId() {
		return eventClassId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setEventClassId(short)
     */
	@Override
	public void setEventClassId(short eventClassId) {
		this.eventClassId = eventClassId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getTime()
     */
	@Override
	public Date getTime() {
		return time;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setTime(Date)
     */
	@Override
	public void setTime(Date time) {
		this.time = time;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getLat()
     */
	@Override
	public int getLat() {
		return lat;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setLat()
     */
	@Override
	public void setLat(int lat) {
		this.lat = lat;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getLon()
     */
	@Override
	public int getLon() {
		return lon;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setLon(int)
     */
	@Override
	public void setLon(int lon) {
		this.lon = lon;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getAlt()
     */
	@Override
	public int getAlt() {
		return alt;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setAlt(int)
     */
	@Override
	public void setAlt(int alt) {
		this.alt = alt;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getAcc()
     */
	@Override
	public int getAcc() {
		return acc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setAcc(int)
     */
	@Override
	public void setAcc(int acc) {
		this.acc = acc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getMcc()
     */
	@Override
	public int getMcc() {
		return mcc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setMcc(int)
     */
	@Override
	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getMnc()
     */
	@Override
	public int getMnc() {
		return mnc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setMnc(int)
     */
	@Override
	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getLac()
     */
	@Override
	public int getLac() {
		return lac;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setLac(int)
     */
	@Override
	public void setLac(int lac) {
		this.lac = lac;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getCid()
     */
	@Override
	public int getCid() {
		return cid;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setCid(int)
     */
	@Override
	public void setCid(int cid) {
		this.cid = cid;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getFactor()
     */
	@Override
	public int getFactor() {
		return Locations.GEO_FACTOR;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setFactor(int)
     */
	@Override
	public void setFactor(int factor) {
		throw new IllegalStateException("Read only field");
	}

	/**
     * @see ru.hipdriver.i.IEvent#getReceivedTime()
     */
	@Override
	public Date getReceivedTime() {
		return receivedTime;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setReceivedTime(Date)
     */
	@Override
	public void setReceivedTime(Date receivedTime) {
		this.setReceivedTime(receivedTime);
	}
	
}
