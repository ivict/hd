/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IUserLimit;

/**
 * JSON Object, ограничения пользователя.
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserLimit extends StandardResponse implements IUserLimit {

	@JsonProperty(value=IUserLimit.TIME_LIMITED)
	public boolean timeLimited;
	
	@JsonProperty(value=IUserLimit.MAX_DEVICES)
	public int maxDevices;

	/**
	 * @see ru.hipdriver.i.IUserLimit#isTimeLimited();
	 */
	@Override
	public boolean isTimeLimited() {
		return timeLimited;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#setTimeLimited(boolean)
	 */
	@Override
	public void setTimeLimited(boolean timeLimited) {
		this.timeLimited = timeLimited;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#getMaxDevices()
	 */
	@Override
	public int getMaxDevices() {
		return maxDevices;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#setMaxDevices(int)
	 */
	@Override
	public void setMaxDevices(int maxDevices) {
		this.maxDevices = maxDevices;
	}
	
}
