/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.ISignInMaRequest;

/**
 * <b>Запрос на начало сессии</b> от мобильного агента.
 * 
 * @author ivict
 * 
 * @see ru.hipdriver.i.ISignInMaRequest
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignInMaRequest implements ISignInMaRequest {

	@JsonProperty(value = "user-id")
	public int userId;

	@JsonProperty(value = "mobile-agent-id")
	public long mobileAgentId;

	@JsonProperty(value = "properties")
	public Map<String, Object> properties;

	/**
	 * @see ru.hipdriver.i.ISignInMaRequest#getUserId()
	 */
	@Override
	public int getUserId() {
		return userId;
	}

	/**
	 * @see ru.hipdriver.i.ISignInMaRequest#setUserId(int)
	 */
	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @see ru.hipdriver.i.ISignInMaRequest#getMobileAgentId()
	 */
	@Override
	public long getMobileAgentId() {
		return mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.ISignInMaRequest#setMobileAgentId(long)
	 */
	@Override
	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.ISignInMaRequest#getProperties()
	 */
	@Override
	public Map<String, Object> getProperties() {
		return properties;
	}

	/**
	 * @see ru.hipdriver.i.ISignInMaRequest#setProperties(Map)
	 */
	@Override
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

}
