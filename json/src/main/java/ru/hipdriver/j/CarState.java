/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.ICarState;

/**
 * JSON Object, состояние машины
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarState implements ICarState {

	@JsonProperty(value = "id")
	public short id;
	@JsonProperty(value = "name")
	public String name;
	@JsonProperty(value = "description")
	public String description;

	/**
	 * @see ru.hipdriver.i.ICarState#getId()
	 */
	@Override
	public short getId() {
		return id;
	}

	/**
	 * @see ru.hipdriver.i.ICarState#setId(short)
	 */
	@Override
	public void setId(short id) {
		this.id = id;
	}

	/**
	 * @see ru.hipdriver.i.ICarState#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @see ru.hipdriver.i.ICarState#setName(String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see ru.hipdriver.i.ICarState#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * @see ru.hipdriver.i.ICarState#setDescription(String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}
}
