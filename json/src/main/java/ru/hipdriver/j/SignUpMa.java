/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.i.IMobileAgentVersion;
import ru.hipdriver.i.IProperties;
import ru.hipdriver.i.ISignUpMa;

/**
 * <b>Запрос на регистрацию</b> мобильного агента в системе.
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignUpMa implements ISignUpMa, IMobileAgentState, IMobileAgentVersion, IProperties {

	@JsonProperty(value = "name")
	public String name;

	@JsonProperty(value = "password-hash")
	public byte[] passwordHash;

	@JsonProperty(value = "skey-value")
	public byte[] sKeyValue;

	@JsonProperty(value = "imei")
	public long imei;

	@JsonProperty(value = "mobile-agent-name")
	public String mobileAgentName;
	
	@JsonProperty(value = "gsm-signal-strength")
	public int gsmSignalStrength = IMobileAgentState.UNKNOWN_GSM_SIGNAL_STRENGTH;
	@JsonProperty(value = "gsm-bit-error-rate")
	public int gsmBitErrorRate = IMobileAgentState.UNKNOWN_GSM_BIT_ERROR_RATE;

	@JsonProperty(value = "battery-status")
	public int batteryStatus;
	@JsonProperty(value = "battery-pct")
	public float batteryPct;

	@JsonProperty(value = "app-state-id")
	public short appStateId;
	@JsonProperty(value = "car-state-id")
	public short carStateId;
	@JsonProperty(value = "last-location")
	public Location lastLocation;

	@JsonProperty(value = "version-code")
	public int versionCode;
	@JsonProperty(value = "version-name")
	public String versionName;
	
	@JsonProperty(value = "first-sign-up")
	public boolean firstSignup;

	@JsonProperty(value = "properties")
	public Map<String, Object> properties;

	/**
	 * @see ru.hipdriver.i.ISignUpMa#getName() 
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#setName(String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#getPasswordHash()
	 */
	@Override
	public byte[] getPasswordHash() {
		return passwordHash;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#setPasswordHash(byte[])
	 */
	@Override
	public void setPasswordHash(byte[] passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#getSKeyValue()
	 */
	@Override
	public byte[] getSKeyValue() {
		return sKeyValue;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#setSKeyValue(byte[])
	 */
	@Override
	public void setSKeyValue(byte[] sKeyValue) {
		this.sKeyValue = sKeyValue;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#getImei()
	 */
	@Override
	public long getImei() {
		return imei;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#setImei(long)
	 */
	@Override
	public void setImei(long imei) {
		this.imei = imei;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#getMobileAgentName()
	 */
	@Override
	public String getMobileAgentName() {
		return mobileAgentName;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#setMobileAgentName(String)
	 */
	@Override
	public void setMobileAgentName(String mobileAgentName) {
		this.mobileAgentName = mobileAgentName;
	}

	public byte[] getsKeyValue() {
		return sKeyValue;
	}

	public void setsKeyValue(byte[] sKeyValue) {
		this.sKeyValue = sKeyValue;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getGsmSignalStrength() 
	 */
	@Override
	public int getGsmSignalStrength() {
		return gsmSignalStrength;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setGsmSignalStrength(int) 
	 */
	@Override
	public void setGsmSignalStrength(int gsmSignalStrength) {
		this.gsmSignalStrength = gsmSignalStrength;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getGsmBitErrorRate() 
	 */
	@Override
	public int getGsmBitErrorRate() {
		return gsmBitErrorRate;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setGsmBitErrorRate(int)
	 */
	@Override
	public void setGsmBitErrorRate(int gsmBitErrorRate) {
		this.gsmBitErrorRate = gsmBitErrorRate;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getBatteryStatus()
	 */
	@Override
	public int getBatteryStatus() {
		return batteryStatus;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setBatteryStatus(int)
	 */
	@Override
	public void setBatteryStatus(int batteryStatus) {
		this.batteryStatus = batteryStatus;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getBatteryPct()
	 */
	@Override
	public float getBatteryPct() {
		return batteryPct;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setBatteryPct(float)
	 */
	@Override
	public void setBatteryPct(float batteryPct) {
		this.batteryPct = batteryPct;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getAppStateId()
	 */
	@Override
	public short getAppStateId() {
		return appStateId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setAppStateId(short)
	 */
	@Override
	public void setAppStateId(short appStateId) {
		this.appStateId = appStateId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getCarStateId()
	 */
	@Override
	public short getCarStateId() {
		return carStateId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setCarStateId(short) 
	 */
	@Override
	public void setCarStateId(short carStateId) {
		this.carStateId = carStateId;
	}
	
	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getMobileAgentId() 
	 */
	@Override
	public long getMobileAgentId() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setMobileAgentId(long) 
	 */
	@Override
	public void setMobileAgentId(long mobileEventId) {
		// TODO Auto-generated method stub
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getLastLocation() 
	 */
	@Override
	public Location getLastLocation() {
		return lastLocation;
	}

	public void setLastLocation(Location lastLocation) {
		this.lastLocation = lastLocation;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentVersion#getVersionCode()
	 */
	@Override
	public int getVersionCode() {
		return versionCode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentVersion#setVersionCode(int)
	 */
	@Override
	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentVersion#getVersionName()
	 */
	@Override
	public String getVersionName() {
		return versionName;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentVersion#setVersionName(String)
	 */
	@Override
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setLastLocation(ILocation) 
	 */
	@Override
	public void setLastLocation(ILocation lastLocation) {
		if (lastLocation == null) {
			throw new IllegalStateException("Last location is null, can't update her fields.");
		}
		this.lastLocation.setLat(lastLocation.getLat());
		this.lastLocation.setLon(lastLocation.getLon());
		this.lastLocation.setAlt(lastLocation.getAlt());
		this.lastLocation.setAcc(lastLocation.getAcc());
		this.lastLocation.setMcc(lastLocation.getMcc());
		this.lastLocation.setMnc(lastLocation.getMnc());
		this.lastLocation.setLac(lastLocation.getLac());
		this.lastLocation.setCid(lastLocation.getCid());
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#isFirstSignup() 
	 */
	@Override
	public boolean isFirstSignup() {
		return firstSignup;
	}

	/**
	 * @see ru.hipdriver.i.ISignUpMa#setFirstSignup(boolean) 
	 */
	@Override
	public void setFirstSignup(boolean firstSignup) {
		this.firstSignup = firstSignup;
	}

	/**
	 * @see ru.hipdriver.i.IProperties#getProperties() 
	 */
	@Override
	public Map<String, Object> getProperties() {
		return properties;
	}

	/**
	 * @see ru.hipdriver.i.IProperties#setProperties(Map) 
	 */
	@Override
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

}
