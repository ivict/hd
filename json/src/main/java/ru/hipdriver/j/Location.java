/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.ICarLocation;

/**
 * JSON Object, географическая локация
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Location extends StandardResponse implements ICarLocation {

	@JsonProperty(value = "factor")
	public int factor = Locations.GEO_FACTOR;

	@JsonProperty(value = "lat")
	public int lat;

	@JsonProperty(value = "lon")
	public int lon;
	
	@JsonProperty(value = "alt")
	public int alt = Integer.MIN_VALUE;
	
	@JsonProperty(value = "acc")
	public int acc;
	
	//Mobile Country Code
	@JsonProperty(value = "mcc")
	public int mcc;
	
	//Mobile Network Code
	@JsonProperty(value = "mnc")
	public int mnc;
	
	//Location Area Code
	@JsonProperty(value = "lac")
	public int lac = -1;
	
	//Cell Id
	@JsonProperty(value = "cid")
	public int cid = -1;
	
	//Timestamp
	@JsonProperty(value = "time")
	public Date time;
	
	//Car state id
	@JsonProperty(value = "car-state-id")
	public short carStateId;

	//Car velocity
	@JsonProperty(value = "velocity")
	public float velocity;

	/**
	 * @see ru.hipdriver.i.ILocation#getFactor()
	 */
	@Override
	public int getFactor() {
		return factor;
	}

	/**
	 * @see ru.hipdriver.i.ILocation#setFactor(int)
	 */
	@Override
	public void setFactor(int factor) {
		this.factor = factor;
	}

	/**
	 * @see ru.hipdriver.i.ILocation#getLat()
	 */
	@Override
	public int getLat() {
		return lat;
	}

	/**
	 * @see ru.hipdriver.i.ILocation#setLat(int)
	 */
	@Override
	public void setLat(int lat) {
		this.lat = lat;
	}

	/**
	 * @see ru.hipdriver.i.ILocation#getLon()
	 */
	@Override
	public int getLon() {
		return lon;
	}

	/**
	 * @see ru.hipdriver.i.ILocation#setLon(int)
	 */
	@Override
	public void setLon(int lon) {
		this.lon = lon;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getAlt()
     */
	@Override
	public int getAlt() {
		return alt;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setAlt(int)
     */
	@Override
	public void setAlt(int alt) {
		this.alt = alt;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getAcc()
     */
	@Override
	public int getAcc() {
		return acc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setAcc(int)
     */
	@Override
	public void setAcc(int acc) {
		this.acc = acc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getMcc()
     */
	@Override
	public int getMcc() {
		return mcc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setMcc(int)
     */
	@Override
	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getMnc()
     */
	@Override
	public int getMnc() {
		return mnc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setMnc(int)
     */
	@Override
	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getLac()
     */
	@Override
	public int getLac() {
		return lac;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setLac(int)
     */
	@Override
	public void setLac(int lac) {
		this.lac = lac;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getCid()
     */
	@Override
	public int getCid() {
		return cid;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setCid(int)
     */
	@Override
	public void setCid(int cid) {
		this.cid = cid;
	}

	/**
     * @see ru.hipdriver.i.ICarLocation#getTime()
     */
	@Override
	public Date getTime() {
		return time;
	}

	/**
     * @see ru.hipdriver.i.ICarLocation#setTime(Date)
     */
	@Override
	public void setTime(Date time) {
		this.time = time;
	}

	/**
     * @see ru.hipdriver.i.ICarLocation#getCarStateId()
     */
	@Override
	public short getCarStateId() {
		return carStateId;
	}

	/**
     * @see ru.hipdriver.i.ICarLocation#setCarStateId(short)
     */
	@Override
	public void setCarStateId(short carStateId) {
		this.carStateId = carStateId;
	}

	/**
     * @see ru.hipdriver.i.ICarLocation#getVelocity()
     */
	@Override
	public float getVelocity() {
		return velocity;
	}

	/**
     * @see ru.hipdriver.i.ICarLocation#setVelocity(float)
     */
	@Override
	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}
	
}
