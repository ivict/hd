/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.i.IProperties;

/**
 * JSON Object, зеркалка для MobileAgentState из пакета entity
 * 
 * @see ru.hipdriver.MobileAgentWithState.entity.MobileAgentState
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MobileAgentWithState extends MobileAgent implements IMobileAgentState, IProperties {

	@JsonProperty(value = "gsm-signal-strength")
	public int gsmSignalStrength = IMobileAgentState.UNKNOWN_GSM_SIGNAL_STRENGTH;
	@JsonProperty(value = "gsm-bit-error-rate")
	public int gsmBitErrorRate = IMobileAgentState.UNKNOWN_GSM_BIT_ERROR_RATE;

	@JsonProperty(value = "battery-status")
	public int batteryStatus;
	@JsonProperty(value = "battery-pct")
	public float batteryPct;

	@JsonProperty(value = "app-state-id")
	public short appStateId;
	@JsonProperty(value = "car-state-id")
	public short carStateId;
	@JsonProperty(value = "last-location")
	public Location lastLocation;
	@JsonProperty(value = "placemark")
	public float[] placemark;
	@JsonProperty(value = "properties")
	public Map<String, Object> properties;
	@JsonProperty(value = "control-mode-id")
	public short controlModeId;
	

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getGsmSignalStrength() 
	 */
	@Override
	public int getGsmSignalStrength() {
		return gsmSignalStrength;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setGsmSignalStrength(int) 
	 */
	@Override
	public void setGsmSignalStrength(int gsmSignalStrength) {
		this.gsmSignalStrength = gsmSignalStrength;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getGsmBitErrorRate() 
	 */
	@Override
	public int getGsmBitErrorRate() {
		return gsmBitErrorRate;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setGsmBitErrorRate(int)
	 */
	@Override
	public void setGsmBitErrorRate(int gsmBitErrorRate) {
		this.gsmBitErrorRate = gsmBitErrorRate;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getBatteryStatus()
	 */
	@Override
	public int getBatteryStatus() {
		return batteryStatus;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setBatteryStatus(int)
	 */
	@Override
	public void setBatteryStatus(int batteryStatus) {
		this.batteryStatus = batteryStatus;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getBatteryPct()
	 */
	@Override
	public float getBatteryPct() {
		return batteryPct;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setBatteryPct(float)
	 */
	@Override
	public void setBatteryPct(float batteryPct) {
		this.batteryPct = batteryPct;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getAppStateId()
	 */
	@Override
	public short getAppStateId() {
		return appStateId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setAppStateId(short)
	 */
	@Override
	public void setAppStateId(short appStateId) {
		this.appStateId = appStateId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getCarStateId()
	 */
	@Override
	public short getCarStateId() {
		return carStateId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setCarStateId(short) 
	 */
	@Override
	public void setCarStateId(short carStateId) {
		this.carStateId = carStateId;
	}
	
	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getMobileAgentId() 
	 */
	@Override
	public long getMobileAgentId() {
		return super.getId();
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setMobileAgentId(long) 
	 */
	@Override
	public void setMobileAgentId(long mobileAgentId) {
		super.setId(mobileAgentId);
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getLastLocation() 
	 */
	@Override
	public ILocation getLastLocation() {
		return lastLocation;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setLastLocation(ILocation) 
	 * If argument is null, reset lastLocation, otherwise copy fields.
	 */
	@Override
	public void setLastLocation(ILocation lastLocation) {
		if (lastLocation == null) {
			//throw new IllegalArgumentException("set null value")
			this.lastLocation = null;
			return;
		}
		if (this.lastLocation == null) {
//			throw new IllegalStateException("Last location is null, can't update her fields.");
			this.lastLocation = new Location();
		}
		this.lastLocation.setLat(lastLocation.getLat());
		this.lastLocation.setLon(lastLocation.getLon());
		this.lastLocation.setAlt(lastLocation.getAlt());
		this.lastLocation.setAcc(lastLocation.getAcc());
		this.lastLocation.setMcc(lastLocation.getMcc());
		this.lastLocation.setMnc(lastLocation.getMnc());
		this.lastLocation.setLac(lastLocation.getLac());
		this.lastLocation.setCid(lastLocation.getCid());
	}
	
	public void setLastLocation(Location lastLocation) {
		this.lastLocation = lastLocation;
	}

	public float[] getPlacemark() {
		return placemark;
	}

	public void setPlacemark(float[] placemark) {
		this.placemark = placemark;
	}

	/**
	 * @see ru.hipdriver.i.IProperties#getProperties()
	 */
	@Override
	public Map<String, Object> getProperties() {
		return properties;
	}

	/**
	 * @see ru.hipdriver.i.IProperties#setProperties(Map)
	 */
	@Override
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	public short getControlModeId() {
		return controlModeId;
	}

	public void setControlModeId(short controlModeId) {
		this.controlModeId = controlModeId;
	}

}
