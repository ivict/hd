/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.ISKey;
import ru.hipdriver.i.ISignInMaResponse;

/**
 * Открытая часть rsa ключа (s-key-rsa), который сервер генерит по запросу
 * регистрации (sign-up). Эта часть ключа сохраняется мобильным агентом
 * локально. Закрытая часть ключа, совместно с imei и открытой чатью rsa ключа
 * (ma-key-rsa), привязана к id мобильного агента на момент генерации.
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SKey extends StandardResponse implements ISKey {

	private static final String SKEY_WITHOUT_CURRENT_SIGN_IN_MA_RESPONSE = "SKey without current sign in mobile agent response"; 

	@JsonProperty(value = "value")
	public byte[] value;

	@Deprecated
	@JsonProperty(value = "current-sid")
	public Sid currentSid;

	@JsonProperty(value = "current-sign-in-ma-response")
	public SignInMaResponse currentSignInMaResponse;

	@JsonProperty(value = "user-id")
	public int userId;

	@JsonProperty(value = "mobile-agent-id")
	public long mobileAgentId;

	@JsonProperty(value = "mobile-agent-count")
	public int mobileAgentCount;

	/**
	 * @see ru.hipdriver.i.ISKey#getValue()
	 */
	@Override
	public byte[] getValue() {
		return value;
	}

	/**
	 * @see ru.hipdriver.i.ISKey#setValue(value)
	 */
	@Override
	public void setValue(byte[] value) {
		this.value = value;
	}

	/**
	 * @see ru.hipdriver.i.ISKey#getUserId()
	 */
	@Override
	public int getUserId() {
		return userId;
	}

	/**
	 * @see ru.hipdriver.i.ISKey#setUserId(int)
	 */
	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @see ru.hipdriver.i.ISKey#getMobileAgentId()
	 */
	@Override
	public long getMobileAgentId() {
		return mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.ISKey#setMobileAgentId(long)
	 */
	@Override
	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.ISKey#getCurrentSignInMaResponse()
	 */
	@Override
	public ISignInMaResponse getCurrentSignInMaResponse() {
		return currentSignInMaResponse;
	}

	/**
	 * @see ru.hipdriver.i.ISKey#setCurrentSignInMaResponse(ISignInMaResponse)
	 */
	@Override
	public void setCurrentSignInMaResponse(ISignInMaResponse currentSignInMaResponse) {
		if (this.currentSignInMaResponse == null) {
			throw new IllegalStateException(SKEY_WITHOUT_CURRENT_SIGN_IN_MA_RESPONSE);
		}
		//TODO: refactoring
		this.currentSignInMaResponse.setA(currentSignInMaResponse.getA());
		this.currentSignInMaResponse.setMobileAgentId(currentSignInMaResponse.getMobileAgentId());
		this.currentSignInMaResponse.setDelayPushAlertEventInSeconds(currentSignInMaResponse.getDelayPushAlertEventInSeconds());
		this.currentSignInMaResponse.setDelayWhereAmIRequestInSeconds(currentSignInMaResponse.getDelayWhereAmIRequestInSeconds());
		this.currentSignInMaResponse.setEnableDebugMode(currentSignInMaResponse.isEnableDebugMode());
		this.currentSignInMaResponse.setAsdMaxJerk(currentSignInMaResponse.getAsdMaxJerk());
		this.currentSignInMaResponse.setAsdMaxAcceleration(currentSignInMaResponse.getAsdMaxAcceleration());
		this.currentSignInMaResponse.setAsdMaxAngle(currentSignInMaResponse.getAsdMaxAngle());
		this.currentSignInMaResponse.setProperties(currentSignInMaResponse.getProperties());
		this.currentSignInMaResponse.setTimeLimited(currentSignInMaResponse.isTimeLimited());
		this.currentSignInMaResponse.setMaxDevices(currentSignInMaResponse.getMaxDevices());
	}

	public void setCurrentSignInMaResponse(SignInMaResponse currentSignInMaResponse) {
		this.currentSignInMaResponse = currentSignInMaResponse;
	}
	
	/**
	 * @see ru.hipdriver.i.ISKey#setMobileAgentCount(int)
	 */
	@Override
	public void setMobileAgentCount(int mobileAgentCount) {
		this.mobileAgentCount = mobileAgentCount;
	}

	/**
	 * @see ru.hipdriver.i.ISKey#getMobileAgentCount()
	 */
	@Override
	public int getMobileAgentCount() {
		return mobileAgentCount;
	}

}
