/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.ITrack;

/**
 * JSON Object, маршрут мобильного агента в состоянии тревоги.
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlertTrack extends StandardResponse implements ITrack {

	@JsonProperty(value="placemarks")
	public List<AlertPlacemark> placemarks;
	
	@JsonProperty(value="polyline")
	public float[][] polyline;

	@JsonProperty(value="alert-time")
	public Date alertTime;

	@JsonProperty(value="car-state-id")
	public short carStateId;

	//GroupByTrack#id
	@JsonProperty(value="track-id")
	public long trackId;

	public List<AlertPlacemark> getPlacemarks() {
		return placemarks;
	}

	public void setPlacemarks(List<AlertPlacemark> placemarks) {
		this.placemarks = placemarks;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#getPolyline()
	 */
	@Override
	public float[][] getPolyline() {
		return polyline;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#setPolyline(float[][])
	 */
	@Override
	public void setPolyline(float[][] polyline) {
		this.polyline = polyline;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#getTimestamps()
	 */
	@Override
	public long[] getTimestamps() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#setTimestamps(long[])
	 */
	@Override
	public void setTimestamps(long[] timestamps) {
		// TODO Auto-generated method stub
	}

	public Date getAlertTime() {
		return alertTime;
	}

	public void setAlertTime(Date alertTime) {
		this.alertTime = alertTime;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#getCarStateId()
	 */
	@Override
	public short getCarStateId() {
		return carStateId;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#setCarStateId(short)
	 */
	@Override
	public void setCarStateId(short carStateId) {
		this.carStateId = carStateId;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#getReceivedTime()
	 */
	@Override
	public Date getReceivedTime() {
		return alertTime;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#setReceivedTime(Date)
	 */
	@Override
	public void setReceivedTime(Date receivedTime) {
		this.alertTime = receivedTime;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#getTrackId()
	 */
	@Override
	public long getTrackId() {
		return trackId;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#setTrackId(long)
	 */
	@Override
	public void setTrackId(long trackId) {
		this.trackId = trackId;
	}
	
}
