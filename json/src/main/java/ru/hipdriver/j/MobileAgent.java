/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentState;

/**
 * JSON Object, зеркалка для MobileAgent из пакета entity
 * 
 * @see ru.hipdriver.kernel.entity.MobileAgent
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MobileAgent implements IMobileAgent {

	@JsonProperty(value = "id")
	public long id;
	@JsonProperty(value = "user-id")
	public int userId;
	@JsonProperty(value = "name")
	public String name;
	@JsonProperty(value = "imei")
	public long imei;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public long getImei() {
		return imei;
	}

	public void setImei(long imei) {
		this.imei = imei;
	}

}
