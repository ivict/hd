/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IUserCommandType;

/**
 * JSON Object, тип контакта
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCommandType implements IUserCommandType {

	@JsonProperty(value = "id")
	public short id;
	@JsonProperty(value = "name")
	public String name;
	@JsonProperty(value = "description")
	public String description;

	/**
	 * @see ru.hipdriver.i.IUserCommandType#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommandType#setDescription(String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommandType#getId()
	 */
	@Override
	public short getId() {
		return id;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommandType#setId(short)
	 */
	@Override
	public void setId(short id) {
		this.id = id;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommandType#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommandType#setName(String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
}
