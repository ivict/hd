/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IEmailMessageAtach;

/**
 * JSON Object, атач к письму
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailMessageAtach implements IEmailMessageAtach {
	public static final String FRAGMENTS_COUNT = "fragments-count";
	
	@JsonProperty(value = "identity")
	public int identity;
	
	@JsonProperty(value = "message-identity")
	public int messageIdentity;
	
	@JsonProperty(value = "atach_number")
	public int atachNumber;
	
	@JsonProperty(value = "content-type")
	public String contentType;

	@JsonProperty(value = "name")
	public String name;
	
	@JsonProperty(value = FRAGMENTS_COUNT)
	public int fragmentsCount;
	
	/**
	 * @see ru.hipdriver.i.IEmailObject#getIdentity()
	 */
	public int getIdentity() {
		return identity;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailObject#setIdentity(int)
	 */
	public void setIdentity(int identity) {
		this.identity = identity;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#getMessageIdentity()
	 */
	public int getMessageIdentity() {
		return messageIdentity;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#setMessageIdentity(int)
	 */
	public void setMessageIdentity(int messageIdentity) {
		this.messageIdentity = messageIdentity;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#getContentType()
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#setContentType(String)
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#setName(String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#getFragmentsCount()
	 */
	public int getFragmentsCount() {
		return fragmentsCount;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#setFragmentsCount(int)
	 */
	public void setFragmentsCount(int fragmentsCount) {
		this.fragmentsCount = fragmentsCount;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#getAtachNumber()
	 */
	public int getAtachNumber() {
		return atachNumber;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessageAtach#setAtachNumber(int)
	 */
	public void setAtachNumber(int atachNumber) {
		this.atachNumber = atachNumber;
	}
	
}
