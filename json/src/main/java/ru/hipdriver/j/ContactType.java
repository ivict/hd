/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IContactType;

/**
 * JSON Object, тип контакта
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactType implements IContactType {

	@JsonProperty(value = "id")
	public short id;
	@JsonProperty(value = "name")
	public String name;

	/**
	 * @see ru.hipdriver.i.IContactType#getId()
	 */
	@Override
	public short getId() {
		return id;
	}

	/**
	 * @see ru.hipdriver.i.IContactType#setId(short)
	 */
	@Override
	public void setId(short id) {
		this.id = id;
	}

	/**
	 * @see ru.hipdriver.i.IContactType#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @see ru.hipdriver.i.IContactType#setName(String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
}
