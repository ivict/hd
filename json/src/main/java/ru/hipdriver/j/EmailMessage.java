/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IEmailMessage;

/**
 * JSON Object, письмо
 * 
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailMessage implements IEmailMessage {
	
	public static final String ATACHS_COUNT = "atachs-count";

	@JsonProperty(value = "identity")
	public int identity;
	@JsonProperty(value = "from")
	public String from;
	@JsonProperty(value = "to")
	public String to;
	@JsonProperty(value = "subject")
	public String subject;
	@JsonProperty(value = "body")
	public String body;
	@JsonProperty(value = ATACHS_COUNT)
	public int atachsCount;
	
	/**
	 * @see ru.hipdriver.i.IEmailObject#getIdentity()
	 */
	public int getIdentity() {
		return identity;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailObject#setIdentity(int)
	 */
	public void setIdentity(int identity) {
		this.identity = identity;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailMessage#getFrom()
	 */
	public String getFrom() {
		return from;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailMessage#setFrom(String)
	 */
	public void setFrom(String from) {
		this.from = from;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailMessage#getTo()
	 */
	public String getTo() {
		return to;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailMessage#setTo(String)
	 */
	public void setTo(String to) {
		this.to = to;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailMessage#getSubject()
	 */
	public String getSubject() {
		return subject;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailMessage#setSubject(String)
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailMessage#getBody()
	 */
	public String getBody() {
		return body;
	}
	
	/**
	 * @see ru.hipdriver.i.IEmailMessage#setBody(String)
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessage#getAtachsCount()
	 */
	public int getAtachsCount() {
		return atachsCount;
	}

	/**
	 * @see ru.hipdriver.i.IEmailMessage#setAtachsCount(int)
	 */
	public void setAtachsCount(int atachsCount) {
		this.atachsCount = atachsCount;
	}

}
