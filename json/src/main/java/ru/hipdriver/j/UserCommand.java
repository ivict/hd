/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.IUserCommand;

/**
 * JSON Object, команда пользователя мобильному клиенту.
 * 
 * @see ru.hipdriver.kernel.entity.UserCommand
 * @author ivict
 * 
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCommand implements IUserCommand {

	@JsonProperty(value = "id")
	public long id;

	@JsonProperty(value = "user-id")
	public int userId;

	@JsonProperty(value = "mobile-agent-id")
	public long mobileAgentId;

	@JsonProperty(value = "user-command-type-id")
	public short userCommandTypeId;

	@JsonProperty(value = "args")
	public byte[] args;

	@JsonProperty(value = "args-check-sum")
	public long argsCheckSum;

	@JsonProperty(value = "send-time")
	public Date sendTime;

	/**
	 * @see ru.hipdriver.i.IUserCommand#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#setId(long)
	 */
	@Override
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#getUserId()
	 */
	@Override
	public int getUserId() {
		return userId;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#setUserId(int)
	 */
	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#getMobileAgentId()
	 */
	@Override
	public long getMobileAgentId() {
		return mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#setMobileAgentId(long)
	 */
	@Override
	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#getUserCommandTypeId()
	 */
	@Override
	public short getUserCommandTypeId() {
		return userCommandTypeId;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#setUserCommandTypeId(short)
	 */
	@Override
	public void setUserCommandTypeId(short userCommandTypeId) {
		this.userCommandTypeId = userCommandTypeId;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#getArgs()
	 */
	@Override
	public byte[] getArgs() {
		return args;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#setArgs(byte[])
	 */
	@Override
	public void setArgs(byte[] args) {
		this.args = args;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#getArgsCheckSum()
	 */
	@Override
	public long getArgsCheckSum() {
		return argsCheckSum;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#setArgsCheckSum(long)
	 */
	@Override
	public void setArgsCheckSum(long argsCheckSum) {
		this.argsCheckSum = argsCheckSum;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#getSendTime()
	 */
	@Override
	public Date getSendTime() {
		return sendTime;
	}

	/**
	 * @see ru.hipdriver.i.IUserCommand#setSendTime(Date)
	 */
	@Override
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

}
