/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.j;

import java.util.Map;

import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentWithState;
import ru.hipdriver.i.IPlacemark;

public class MobileAgentWithStateFactory {

	public static MobileAgentWithState newMobileAgent(IMobileAgentWithState e, Map<String, Object> properties, IPlacemark placemark) {
		MobileAgentWithState m = new MobileAgentWithState();
		m.setMobileAgentId(e.getMobileAgentId());
		m.setUserId(e.getUserId());
		m.setName(e.getName());
		m.setImei(e.getImei());
		m.setAppStateId(e.getAppStateId());
		m.setCarStateId(e.getCarStateId());
		m.setBatteryStatus(e.getBatteryStatus());
		m.setBatteryPct(e.getBatteryPct());
		m.setGsmBitErrorRate(e.getGsmBitErrorRate());
		m.setGsmSignalStrength(e.getGsmSignalStrength());
		//Deprecated TODO: refactoring
		ILocation lastLocation = e.getLastLocation();
		if (lastLocation != null) {
			Location l = new Location();
			l.setLat(lastLocation.getLat());
			l.setLon(lastLocation.getLon());
			l.setAlt(lastLocation.getAlt());
			l.setAcc(lastLocation.getAcc());
			l.setMcc(lastLocation.getMcc());
			l.setMnc(lastLocation.getMnc());
			l.setLac(lastLocation.getLac());
			l.setCid(lastLocation.getCid());
			m.setLastLocation(l);
			m.setPlacemark(placemark.getPlacemark());
		}
		m.setProperties(properties);
		return m;
	}


}
