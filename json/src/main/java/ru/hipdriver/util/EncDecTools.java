/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.util;

import java.nio.ByteBuffer;

import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.Base64Variants;
import org.codehaus.jackson.JsonLocation;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.util.ByteArrayBuilder;

import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.ISid;

public class EncDecTools {
	
	private EncDecTools () { }
	
	public static String encodeMobileAgentId(long mobileAgentId, long salt) {
		ByteBuffer bb = ByteBuffer.wrap(new byte[8]); 
		//TODO: MORE STRONG SECURITY
		long mid = mobileAgentId ^ salt;
		bb.position(0);
		bb.putLong(mid);
		return Base64Variants.MIME.encode(bb.array());
	}

	public static String encodeComponentSessionsId(long componentSessionsId, long salt) {
		ByteBuffer bb = ByteBuffer.wrap(new byte[8]); 
		//TODO: MORE STRONG SECURITY
		long mid = componentSessionsId ^ salt;
		bb.position(0);
		bb.putLong(mid);
		return Base64Variants.MIME.encode(bb.array());
	}
	
	public static long decodeMobileAgentId(String value, long salt) {
		if (value == null) {
			return IMobileAgent.INVALID_ID;
		}
		if (value.length() == 0) {
			return IMobileAgent.INVALID_ID;
		}
		byte[] data = null;
		try {
			data = getBinaryValue(Base64Variants.MIME, value);
		} catch (JsonParseException e) {
			return IMobileAgent.INVALID_ID;
		}
		ByteBuffer bb = ByteBuffer.wrap(data);
		long mid = bb.getLong();
		//TODO: MORE STRONG SECURITY
		long mobileAgentId = mid ^ salt;
		return mobileAgentId;
	}
	
	public static long decodeComponentSessionsId(String value, long salt) {
		if (value == null) {
			return ISid.INVALID_COMPONENT_SESSIONS_ID;
		}
		if (value.length() == 0) {
			return ISid.INVALID_COMPONENT_SESSIONS_ID;
		}
		byte[] data = null;
		try {
			data = getBinaryValue(Base64Variants.MIME, value);
		} catch (JsonParseException e) {
			return ISid.INVALID_COMPONENT_SESSIONS_ID;
		}
		ByteBuffer bb = ByteBuffer.wrap(data);
		long mid = bb.getLong();
		//TODO: MORE STRONG SECURITY
		long mobileAgentId = mid ^ salt;
		return mobileAgentId;
	}
	
	/**
	 * Method for accessing textual contents assuming they were base64 encoded;
	 * if so, they are decoded and resulting binary data is returned.
	 * 
	 * @throws JsonParseException
	 */
	public static byte[] getBinaryValue(Base64Variant b64variant, String value)
			throws JsonParseException {
		ByteArrayBuilder builder = new ByteArrayBuilder(100);
		try {
			final String str = value;
			int ptr = 0;
			int len = str.length();
	
			main_loop: while (ptr < len) {
				// first, we'll skip preceding white space, if any
				char ch;
				do {
					ch = str.charAt(ptr++);
					if (ptr >= len) {
						break main_loop;
					}
				} while (ch <= ' ');
				int bits = b64variant.decodeBase64Char(ch);
				if (bits < 0) {
					_reportInvalidBase64(b64variant, ch, 0);
				}
				int decodedData = bits;
				// then second base64 char; can't get padding yet, nor ws
				if (ptr >= len) {
					reportBase64EOF();
				}
				ch = str.charAt(ptr++);
				bits = b64variant.decodeBase64Char(ch);
				if (bits < 0) {
					_reportInvalidBase64(b64variant, ch, 1);
				}
				decodedData = (decodedData << 6) | bits;
				// third base64 char; can be padding, but not ws
				if (ptr >= len) {
					// but as per [JACKSON-631] can be end-of-input, iff not using
					// padding
					if (!b64variant.usesPadding()) {
						// Got 12 bits, only need 8, need to shift
						decodedData >>= 4;
						builder.append(decodedData);
						break;
					}
					reportBase64EOF();
				}
				ch = str.charAt(ptr++);
				bits = b64variant.decodeBase64Char(ch);
	
				// First branch: can get padding (-> 1 byte)
				if (bits < 0) {
					if (bits != Base64Variant.BASE64_VALUE_PADDING) {
						_reportInvalidBase64(b64variant, ch, 2);
					}
					// Ok, must get padding
					if (ptr >= len) {
						reportBase64EOF();
					}
					ch = str.charAt(ptr++);
					if (!b64variant.usesPaddingChar(ch)) {
						reportInvalidBase64(
								b64variant,
								ch,
								3,
								"expected padding character '"
										+ b64variant.getPaddingChar() + "'");
					}
					// Got 12 bits, only need 8, need to shift
					decodedData >>= 4;
					builder.append(decodedData);
					continue;
				}
				// Nope, 2 or 3 bytes
				decodedData = (decodedData << 6) | bits;
				// fourth and last base64 char; can be padding, but not ws
				if (ptr >= len) {
					// but as per [JACKSON-631] can be end-of-input, iff not using
					// padding
					if (!b64variant.usesPadding()) {
						decodedData >>= 2;
						builder.appendTwoBytes(decodedData);
						break;
					}
					reportBase64EOF();
				}
				ch = str.charAt(ptr++);
				bits = b64variant.decodeBase64Char(ch);
				if (bits < 0) {
					if (bits != Base64Variant.BASE64_VALUE_PADDING) {
						_reportInvalidBase64(b64variant, ch, 3);
					}
					decodedData >>= 2;
					builder.appendTwoBytes(decodedData);
				} else {
					// otherwise, our triple is now complete
					decodedData = (decodedData << 6) | bits;
					builder.appendThreeBytes(decodedData);
				}
			}
			return builder.toByteArray();
		} finally {
			builder.close();
		}
	}

	private static void _reportInvalidBase64(Base64Variant b64variant, char ch,
			int bindex) throws JsonParseException {
		reportInvalidBase64(b64variant, ch, bindex, null);
	}

	private static void reportInvalidBase64(Base64Variant b64variant, char ch,
			int bindex, String msg) throws JsonParseException {
		String base;
		if (ch <= ' ') {
			base = "Illegal white space character (code 0x"
					+ Integer.toHexString(ch) + ") as character #"
					+ (bindex + 1)
					+ " of 4-char base64 unit: can only used between units";
		} else if (b64variant.usesPaddingChar(ch)) {
			base = "Unexpected padding character ('"
					+ b64variant.getPaddingChar()
					+ "') as character #"
					+ (bindex + 1)
					+ " of 4-char base64 unit: padding only legal as 3rd or 4th character";
		} else if (!Character.isDefined(ch) || Character.isISOControl(ch)) {
			// Not sure if we can really get here... ? (most illegal xml chars
			// are caught at lower level)
			base = "Illegal character (code 0x" + Integer.toHexString(ch)
					+ ") in base64 content";
		} else {
			base = "Illegal character '" + ch + "' (code 0x"
					+ Integer.toHexString(ch) + ") in base64 content";
		}
		if (msg != null) {
			base = base + ": " + msg;
		}
		throw new JsonParseException(base, JsonLocation.NA);
	}

	private static void reportBase64EOF() throws JsonParseException {
		throw new JsonParseException(
				"Unexpected end-of-String when base64 content", JsonLocation.NA);
	}
	
	public static byte[] encodePasswordHash(byte[] passwordHash) {
		ByteBuffer salt = ByteBuffer.wrap(new byte[8]);
		salt.putLong(ISid.SALTPW);
		ByteBuffer hash = ByteBuffer.wrap(passwordHash);
		ByteBuffer encodedHash = ByteBuffer.allocate(passwordHash.length);
		//TODO: more complicated algorithm
		for (int i = 0; i < passwordHash.length; i ++) {
			byte b = hash.get();
			//Reset salt buffer position
			if (i % 8 == 0) {
				salt.position(0);
			}
			byte s = salt.get();
			encodedHash.put((byte) (b ^ s));
		}
		return encodedHash.array();
	}

	public static byte[] decodePasswordHash(byte[] encodedHash) {
		ByteBuffer salt = ByteBuffer.wrap(new byte[8]);
		salt.putLong(ISid.SALTPW);
		ByteBuffer hash = ByteBuffer.wrap(encodedHash);
		ByteBuffer passwordHash = ByteBuffer.allocate(encodedHash.length);
		//TODO: more complicated algorithm
		for (int i = 0; i < encodedHash.length; i ++) {
			byte b = hash.get();
			//Reset salt buffer position
			if (i % 8 == 0) {
				salt.position(0);
			}
			byte s = salt.get();
			passwordHash.put((byte) (b ^ s));
		}
		return passwordHash.array();
	}

}
