package ru.hipdriver.util;

import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.ISid;

public class ChatMsgParser {
	
	/**
	 * Message format:
	 * FROM[TO](COMPONENT_SESSIONS_ID)BODY
	 * 
	 * FROM and TO base64 encoded longs
	 * COMPONENT_SESSIONS_ID base64 encoded long
	 * BODY any text UTF-8 encoded
	 */
	
	private ChatMsgParser() { }

	public static long getMobileAgentIdFrom(String msg) {
		int pos = msg.indexOf('[');
		if (pos < 0) {
			return IMobileAgent.INVALID_ID;
		}
		String encodedMobileAgentId = msg.substring(0, pos);
		return EncDecTools.decodeMobileAgentId(encodedMobileAgentId, ISid.SALTMA);
	}

	public static long getMobileAgentIdTo(String msg) {
		int pos0 = msg.indexOf('[');
		if (pos0 < 0) {
			return IMobileAgent.INVALID_ID;
		}
		int pos1 = msg.indexOf(']');
		if (pos1 < 0) {
			return IMobileAgent.INVALID_ID;
		}
		if (pos0 + 1 >= pos1) {
			return IMobileAgent.INVALID_ID;
		}
		String encodedMobileAgentId = msg.substring(pos0 + 1, pos1);
		return EncDecTools.decodeMobileAgentId(encodedMobileAgentId, ISid.SALTSS);
	}

	public static long getComponentSessionsId(String msg) {
		int pos0 = msg.indexOf('(');
		if (pos0 < 0) {
			return ISid.INVALID_COMPONENT_SESSIONS_ID;
		}
		int pos1 = msg.indexOf(')');
		if (pos1 < 0) {
			return ISid.INVALID_COMPONENT_SESSIONS_ID;
		}
		String encodedComponentSessionsAgentId = msg.substring(pos0 + 1, pos1);
		return EncDecTools.decodeComponentSessionsId(encodedComponentSessionsAgentId, ISid.SALTCS);
	}

	public static String getMobileAgentIdToAsString(String msg) {
		int pos0 = msg.indexOf('[');
		if (pos0 < 0) {
			return "";
		}
		int pos1 = msg.indexOf(']');
		if (pos1 < 0) {
			return "";
		}
		return msg.substring(pos0 + 1, pos1);
	}

	public static String getBody(String msg) {
		if (msg == null) {
			return null;
		}
		//Position if component sessions not exists
		int pos0 = msg.indexOf(']');
		if (pos0 < 0) {
			return null;
		}
		if (pos0 + 1 >= msg.length()) {
			return null;
		}
		//Position with component sessions
		int pos1 = msg.indexOf(')');
		if (pos1 < 0) {
			return msg.substring(pos0 + 1);
		}
		if (pos1 + 1 >= msg.length()) {
			return msg.substring(pos0 + 1);
		}
		return msg.substring(pos1 + 1);
	}

	public static String getComponentSessionsIdAsString(String msg) {
		int pos0 = msg.indexOf('[');
		if (pos0 < 0) {
			return "";
		}
		int pos1 = msg.indexOf(']');
		if (pos1 < 0) {
			return "";
		}
		//Position with component sessions
		int pos2 = msg.indexOf('(');
		if (pos2 < 0) {
			return "";
		}
		int pos3 = msg.indexOf(')');
		if (pos3 < 0) {
			return "";
		}
		return msg.substring(pos2 + 1, pos3);
	}
}
