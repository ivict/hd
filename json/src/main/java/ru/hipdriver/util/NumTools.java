/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.util;

public class NumTools {

	private NumTools() {
	}

	public static long getLong(String number, long defaultValue) {
		if (number != null) {
			try {
				return Long.decode(number);
			} catch (NumberFormatException e) {
			}
		}
		return defaultValue;
	}

	/**
	 * Round to n digit after zero.
	 * @param maxChangeOfAngle
	 * @param n count of digit after zero in decimal numeric system
	 * @return
	 */
	public static float round(float f, int n) {
		float r = 1;
		for (int i = 1; i < n; i++) {
			r *= 10;
		}
		return Math.round(f * r) / r;
	}

}
