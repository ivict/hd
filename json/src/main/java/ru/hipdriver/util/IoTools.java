/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IoTools {

	private IoTools() {
	}

	public static void pipe(InputStream is, OutputStream os) throws IOException {
		pipe(is, os, 4096);
    }

	public static void pipe(InputStream is, OutputStream os, int bufSize) throws IOException {
        byte[] buffer = new byte[bufSize];
        while(is.read(buffer) > -1) {
            os.write(buffer);   
        }
    }
}
