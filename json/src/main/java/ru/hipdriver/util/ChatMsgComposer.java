package ru.hipdriver.util;

import ru.hipdriver.i.ISid;

public class ChatMsgComposer {
	
	/**
	 * Message format:
	 * FROM[TO]BODY
	 * 
	 * FROM and TO base64 encoded longs
	 * BODY any text UTF-8 encoded
	 */
	
	private ChatMsgComposer() { }
	
	public static String makeMessage(long mobileAgentFromId, long mobileAgentToId, long componentSessionsId, String body) {
		StringBuilder text = new StringBuilder();
		String encodedMobileAgentFromId = EncDecTools.encodeMobileAgentId(mobileAgentFromId, ISid.SALTMA);
		text.append(encodedMobileAgentFromId);
		text.append('[');
		String encodedMobileAgentToId = EncDecTools.encodeMobileAgentId(mobileAgentToId, ISid.SALTSS);
		text.append(encodedMobileAgentToId);
		text.append(']');
		text.append('(');
		String encodedComponentSessionsId = EncDecTools.encodeComponentSessionsId(componentSessionsId, ISid.SALTCS);
		text.append(encodedComponentSessionsId);
		text.append(')');
		//TODO: replace delimiters
		text.append(body);
		text.append('\n');
		return text.toString();
	}

}
