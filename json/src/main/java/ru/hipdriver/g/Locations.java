/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.g;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.j.Event;
import ru.hipdriver.j.Location;

/**
 * Статические функции работы с локациями.
 * 
 * @author ivict
 * 
 */
public class Locations {

	/**
	 * Для преобразования вещественных координат в целочисленные, используется
	 * проекция Меркатора, с последующим произведением на гео-фактор и
	 * отбрасыванием дробной части.
	 */
	public static final int GEO_FACTOR = 1000000;
	
	private Locations() {
	}

	public static int integerLatitude(double latitude) {
		// TODO: Проекция Меркатора
		return (int) Math.ceil(latitude * GEO_FACTOR);
	}

	public static int integerLongitude(double longitude) {
		// TODO: Проекция Меркатора
		return (int) Math.ceil(longitude * GEO_FACTOR);
	}

	public static double floatLatitude(int latitude) {
		// TODO: Обратное преобразование из проекции Меркатора
		return latitude / (double) GEO_FACTOR;
	}

	public static double floatLongitude(int longitude) {
		// TODO: Обратное преобразование из проекции Меркатора
		return longitude / (double) GEO_FACTOR;
	}

	public static void packGsmLocation(Event event) {
		Date date = event.getTime();
		if ( date == null ) {
			throw new IllegalStateException("Time undefined");
		}
		ByteBuffer buff = ByteBuffer.allocate(24);
		buff.putInt(event.getMcc());
		buff.putInt(event.getMnc());
		buff.putInt(event.getLac());
		buff.putInt(event.getCid());
		buff.putLong(date.getTime());
		event.gsmLocation = buff.array();
	}

	public static void packGpsLocation(Event event) {
		Date date = event.getTime();
		if ( date == null ) {
			throw new IllegalStateException("Time undefined");
		}
		ByteBuffer buff = ByteBuffer.allocate(24);
		buff.putInt(event.getLat());
		buff.putInt(event.getLon());
		buff.putInt(event.getAlt());
		buff.putInt(event.getAcc());
		buff.putLong(date.getTime());
		event.gpsLocation = buff.array();
	}
	
	public static void unpackGsmLocation(Event event) {
		ByteBuffer buff = ByteBuffer.wrap(event.gsmLocation);
		event.setMcc(buff.getInt());
		event.setMnc(buff.getInt());
		event.setLac(buff.getInt());
		event.setCid(buff.getInt());
		event.setTime(new Date(buff.getLong()));
	}

	public static void unpackGpsLocation(Event event) {
		ByteBuffer buff = ByteBuffer.wrap(event.gpsLocation);
		event.setLat(buff.getInt());
		event.setLon(buff.getInt());
		event.setAlt(buff.getInt());
		event.setAcc(buff.getInt());
		event.setTime(new Date(buff.getLong()));
	}

	public static Location makeLocation(int lat, int lon, int alt, int acc, int mcc, int mnc, int lac, int cid, long time, short carStateId, float velocity) {
		Location location = new Location();
		location.setFactor(GEO_FACTOR);
		location.setLat(lat);
		location.setLon(lon);
		location.setAlt(alt);
		location.setAcc(acc);
		location.setMcc(mcc);
		location.setMnc(mnc);
		location.setLac(lac);
		location.setCid(cid);
		location.setTime(new Date(time));
		location.setCarStateId(carStateId);
		location.setVelocity(velocity);
		return location;
	}

	public static byte[] makeTrack(List<? extends ICarLocation> track) {
		try {
			ObjectMapper om = new ObjectMapper();
			return om.writeValueAsBytes(track);
		} catch (Throwable th) {
			th.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Get distance by cousine formula
	 * @param latitude1 integer latitude
	 * @param longitude1 integer longitude
	 * @param latitude2 integer latitude
	 * @param longitude2 integer longitude
	 * @return distance in meters
	 */
	public static float getDistance(int latitude1, int longitude1, int latitude2, int longitude2) {
		float R = 6371f;
		float lat1 = latitude1 / (float) Locations.GEO_FACTOR;
		float lon1 = longitude1 / (float) Locations.GEO_FACTOR;
		float lat2 = latitude2 / (float) Locations.GEO_FACTOR;
		float lon2 = longitude2 / (float) Locations.GEO_FACTOR;
		//float distanceInKm = (float) Math.acos(FloatMath.sin(lat1)*FloatMath.sin(lat2) +
		//	FloatMath.cos(lat1)*FloatMath.cos(lat2)*FloatMath.cos(lon2 - lon1))*R;
		float distanceInKm = (float) Math.acos(Math.sin(lat1)*Math.sin(lat2) +
			Math.cos(lat1)*Math.cos(lat2)*Math.cos(lon2 - lon1))*R;
		return distanceInKm * 10f;
	}

	public static ILocation readonlyLocation(final int lat, final int lon, final int alt,
			final int acc, final int mcc, final int mnc, final int lac, final int cid) {
		return new ILocation() {
			private final String READONLY_LOCATION = "Location is read only";

			@Override
			public int getFactor() {
				return Locations.GEO_FACTOR;
			}

			@Override
			public void setFactor(int factor) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getLat() {
				return lat;
			}

			@Override
			public void setLat(int latitude) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getLon() {
				return lon;
			}

			@Override
			public void setLon(int longitude) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getAlt() {
				return alt;
			}

			@Override
			public void setAlt(int alt) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getAcc() {
				return acc;
			}

			@Override
			public void setAcc(int acc) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getMcc() {
				return mcc;
			}

			@Override
			public void setMcc(int mcc) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getMnc() {
				return mnc;
			}

			@Override
			public void setMnc(int mnc) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getLac() {
				return lac;
			}

			@Override
			public void setLac(int lac) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getCid() {
				return cid;
			}

			@Override
			public void setCid(int cid) {
				throw new IllegalStateException(READONLY_LOCATION);
			}
			
		};
	}
	
	public static ICarLocation readonlyCarLocation(ILocation location, final Date time, final short carStateId, final float vel) {
		if (location == null) {
			return null;
		}
		final int lat = location.getLat(); 
		final int lon = location.getLon(); 
		final int alt = location.getAlt(); 
		final int acc = location.getAcc(); 
		final int mcc = location.getMcc(); 
		final int mnc = location.getMnc(); 
		final int lac = location.getLac(); 
		final int cid = location.getCid(); 
		
		return new ICarLocation() {
			private final String READONLY_LOCATION = "Location is read only";

			@Override
			public int getFactor() {
				return Locations.GEO_FACTOR;
			}

			@Override
			public void setFactor(int factor) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getLat() {
				return lat;
			}

			@Override
			public void setLat(int latitude) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getLon() {
				return lon;
			}

			@Override
			public void setLon(int longitude) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getAlt() {
				return alt;
			}

			@Override
			public void setAlt(int alt) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getAcc() {
				return acc;
			}

			@Override
			public void setAcc(int acc) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getMcc() {
				return mcc;
			}

			@Override
			public void setMcc(int mcc) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getMnc() {
				return mnc;
			}

			@Override
			public void setMnc(int mnc) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getLac() {
				return lac;
			}

			@Override
			public void setLac(int lac) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public int getCid() {
				return cid;
			}

			@Override
			public void setCid(int cid) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public Date getTime() {
				return time;
			}

			@Override
			public void setTime(Date time) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public short getCarStateId() {
				return carStateId;
			}

			@Override
			public void setCarStateId(short carStateId) {
				throw new IllegalStateException(READONLY_LOCATION);
			}

			@Override
			public float getVelocity() {
				return vel;
			}

			@Override
			public void setVelocity(float velocity) {
				throw new IllegalStateException(READONLY_LOCATION);
			}
			
		};
	}
	
	//TODO: more effective algorithm
	public static List<? extends ICarLocation> compressTrack(
			List<? extends ICarLocation> track, int maxPointsCount) {
		if (track == null) {
			return null;
		}
		if (track.size() <= maxPointsCount) {
			return track;
		}
		List<Accuracy> listOfAccuracy = new ArrayList<Accuracy>();
		for (ICarLocation location : track) {
			listOfAccuracy.add(new Accuracy(location.getAcc(), location));
		}
		Collections.sort(listOfAccuracy);
		Set<ICarLocation> selectedPoints = new HashSet<ICarLocation>();
		for (int i = 0; i < maxPointsCount; i++) {
			Accuracy accuracy = listOfAccuracy.get(i);
			selectedPoints.add(accuracy.location);
		}
		List<ICarLocation> compressedTrack = new ArrayList<ICarLocation>();
		for (ICarLocation location : track) {
			if (selectedPoints.contains(location)) {
				compressedTrack.add(location);
			}
		}
		return compressedTrack;
	}

	
}

class Accuracy implements Comparable {
	final int acc;
	final ICarLocation location;
	
	public Accuracy(int acc, ICarLocation location) {
		super();
		this.acc = acc;
		this.location = location;
	}

	@Override
	public int compareTo(Object arg0) {
		if (arg0 == null) {
			return -1;
		}
		if (!(arg0 instanceof Accuracy)) {
			return -1;
		}
		return acc - ((Accuracy) arg0).acc;
	}
}
