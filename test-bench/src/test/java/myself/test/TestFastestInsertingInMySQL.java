package myself.test;

import java.sql.Connection;
import java.sql.SQLException;

import org.testng.annotations.Test;

import com.jolbox.bonecp.BoneCPDataSource;

public class TestFastestInsertingInMySQL {

	@Test
	public void testInsertion() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver"); 	// load the DB driver
	 	BoneCPDataSource ds = new BoneCPDataSource();  // create a new datasource object
	 	ds.setJdbcUrl("jdbc:mysql://localhost/hipdriver_ke");		// set the JDBC url
		ds.setUsername("root");				// set the username
		ds.setPassword("lbcrtnf57");				// set the password
		
		//ds.setXXXX(...);				// (other config options here)
		
		Connection connection;
		connection = ds.getConnection(); 		// fetch a connection
		
		
		
		connection.close();				// close the connection
		ds.close();		
	}
}
