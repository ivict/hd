package ru.hipdriver.tools.event12;

import java.io.File;

import ru.hipdriver.d.SensorsAdapter;


public class App {
	
	private static void say(String template, Object... args) {
		System.out.printf(template, args).println();
	}

	public static void main(String[] args) {
		//System.out.println("OK!");
		if (args.length < 1) {
			say("Add argument in command line, file name to open!");
			return;
		}
		if (args.length > 2) {
			say("Too more arguments in command line!");
			return;
		}
		File inputFile = new File(args[0]);
		if ( !inputFile.exists() ) {
			say("File : %s not exists!", inputFile);
			return;
		}
		SensorsAdapter adapter = new SensorsAdapter(inputFile);
		try {
			adapter.parse();
			say("Success!");
			say("Store to output files into dir: %s", adapter.getOutputDirectory());
		} catch (Throwable th) {
			th.printStackTrace();
			say("##############################################");
			say("Error in parse : %s", th.getLocalizedMessage());
		}
	}
	
}
