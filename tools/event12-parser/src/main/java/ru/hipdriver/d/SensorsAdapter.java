package ru.hipdriver.d;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.ByteBuffer;

import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.j.EventWithAtach;
import ru.hipdriver.j.ext.SensorData;

public class SensorsAdapter {
	
	private File out;
	private File outJ1;
	private File outJ2;
	
	final private File inputFile;
	private File outputDirectory;

	public SensorsAdapter(File inputFile) {
		this.inputFile = inputFile;
		outputDirectory = inputFile.getParentFile();
	}

	public void deserializationDebugMode(SensorData sensorData) throws Throwable {
		if ( !sensorData.preparingMode ) {
			throw new IllegalStateException("Not implemented!");
		}
		long time0 = 0;
		byte[] data = sensorData.getData();
		if ( data == null ) {
			throw new IllegalStateException("No data");
		}
		int countOfValues = data.length / (4 * 5 * 3);
		BufferedWriter writer = new BufferedWriter(new FileWriter(out, false));
		BufferedWriter writerJ1 = new BufferedWriter(new FileWriter(outJ1, false));
		BufferedWriter writerJ2 = new BufferedWriter(new FileWriter(outJ2, false));
		for ( int i = 0 ; i < countOfValues; i++ ) {
			ByteBuffer buff;
			buff = Sensors.readBuffer(sensorData.data, i);
			float x = buff.getFloat();
			float y = buff.getFloat();
			float z = buff.getFloat();
			long timestamp = buff.getLong();
			
			buff = Sensors.readBuffer(sensorData.data, countOfValues + i);
			float xJ1 = buff.getFloat();
			float yJ1 = buff.getFloat();
			float zJ1 = buff.getFloat();
			
			buff = Sensors.readBuffer(sensorData.data, 2 * countOfValues + i);
			float xJ2 = buff.getFloat();
			float yJ2 = buff.getFloat();
			float zJ2 = buff.getFloat();
			
			if ( i == 0 ) {
				writer.write("x,y,z,timestamp");
				writer.newLine();
				writerJ1.write("xJ1,yJ1,zJ1,timestamp");
				writerJ1.newLine();
				writerJ2.write("xJ2,yJ2,zJ2,timestamp");
				writerJ2.newLine();
				time0 = timestamp;
			}
			float timeInSeconds = ((timestamp - time0) / 1000000) / 1000f;
			writer.write("" + x + "," + y + "," + z + "," + timeInSeconds);
			writer.newLine();
			writerJ1.write("" + xJ1 + "," + yJ1 + "," + zJ1 + "," + timeInSeconds);
			writerJ1.newLine();
			writerJ2.write("" + xJ2 + "," + yJ2 + "," + zJ2 + "," + timeInSeconds);
			writerJ2.newLine();
		}
		writer.close();
		writerJ1.close();
		writerJ2.close();
		
	}

	public void deserializationNormalMode(SensorData sensorData) throws Throwable {
		if ( sensorData.preparingMode ) {
			throw new IllegalStateException("Not implemented!");
		}
		long time0 = 0;
		byte[] data = sensorData.getData();
		if ( data == null ) {
			throw new IllegalStateException("No data");
		}
		int countOfValues = data.length / (4 * 5);
		BufferedWriter writer = new BufferedWriter(new FileWriter(out, false));
		for ( int i = 0 ; i < countOfValues; i++ ) {
			ByteBuffer buff = Sensors.readBuffer(sensorData.data, i);
			float x = buff.getFloat();
			float y = buff.getFloat();
			float z = buff.getFloat();
			long timestamp = buff.getLong();
			if ( i == 0 ) {
				writer.write("x,y,z,timestamp");
				time0 = timestamp;
				writer.newLine();
			}
			float timeInSeconds = ((timestamp - time0) / 1000000) / 1000f;
			writer.write("" + x + "," + y + "," + z + "," + timeInSeconds);
			writer.newLine();
		}
		writer.close();
	}

	public void parse() throws Throwable {
		ObjectMapper om = new ObjectMapper();
		SensorData sensorData;
		try {
			EventWithAtach event = om.readValue(inputFile, EventWithAtach.class);
			byte[] content = event.getContent();
			sensorData = om.readValue(content, SensorData.class);
		} catch (Throwable th) {
			System.out.printf("Not event12 format, try to usual sensor-data format.").println();
			sensorData = om.readValue(inputFile, SensorData.class);
		}
		System.out.printf("Base time [%s]", sensorData.baseTime).println();
		System.out.printf("%s", sensorData.getName()).println();
		System.out.printf("Vendor[Version]: %s [%s]", sensorData.getVendor(), sensorData.getVersion()).println();
		if (sensorData.preparingMode) {
			System.out.printf("Contains prepared data").println();
		}
		out = new File(outputDirectory, "out.csv");
		outJ1 = new File(outputDirectory, "outJ1.csv");
		outJ2 = new File(outputDirectory, "outJ2.csv");
		if (sensorData.preparingMode) {
			deserializationDebugMode(sensorData);			
		} else {
			deserializationNormalMode(sensorData);			
		}
	}

	public File getOutputDirectory() {
		return outputDirectory;
	}
	
}
