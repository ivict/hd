package ru.hipdriver.android.tools;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.d.Sensors;
import ru.hipdriver.j.EventWithAtach;
import ru.hipdriver.j.ext.SensorData;
import ru.hipdriver.j.support.SensorDataPusher;
import ru.hipdriver.silem.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.FloatMath;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class SilemActivity extends Activity {
	private static final String TAG = SilemActivity.class.getName();
	
	private boolean measuring;
	private URI lastSavedFileUri;
	
	private static final float _100 = 100f;
	
	// Tracking data container
	// Max reaction for Human is 40 ms, then take 20 ms interval
	// Max measure time 15 мин.
	// 15 * 50 * 60 = 45000, byte array size 45000 * 20 = 879 KB
	SensorData accelerometerData = Sensors.makeForNMeasures(45000);
	// Max calibration rate 1ms, calibration timeout 3s
	SensorData calibrationData = Sensors.makeForNMeasures(3000);
	
	class AccSensorListener extends SensorDataPusher implements SensorEventListener {

		public AccSensorListener(SensorData sensorData,
				SensorData calibrationData) {
			super(sensorData, calibrationData);
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			//Not use event.timestamp Megafon Phone issue
			pushValues(System.nanoTime(), event.values);
		}
	
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			Log.w(TAG, "Accuracy of sensor changed");
		}
	
		public void getGravity(float[] result) {
			System.arraycopy(gravity, 0, result, 0, 3);
		}
	
		public void setSensorTid(int sensorTid) {
			super.sensorTid = sensorTid; 
		}
	
		public int getSensorTid() {
			return sensorTid;
		}
		
		@Override
		protected int myTid() {
			return android.os.Process.myTid();
		}

		@Override
		protected float sqrt(float value) {
			return FloatMath.sqrt(value);
		}

		@Override
		public void switchOnCalibration() {
			super.switchOnCalibration();
			debugStatus("Калибровка");
		}

		@Override
		public void switchOffCalibration() {
			super.switchOffCalibration();
			debugStatus("Измерение");
		}
		
		
	
	}
	
	private AccSensorListener sensorEventListener = new AccSensorListener(accelerometerData, calibrationData);
	private Sensor sensor;
	private SensorManager sensorManager;
	private Timer timer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.silem_screen);

		initSensors();
		
		final Button actionButton = (Button) findViewById(R.id.action_button);
		actionButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!measuring) {
					startMeasure();
					actionButton.setText(getResources().getString(R.string.stop));
					measuring = true;
				} else {
					stopMeasure();
					actionButton.setText(getResources().getString(R.string.start));
					measuring = false;
				}
			}
			
		});
		Button sendEmailButton = (Button) findViewById(R.id.send_email_button);
		sendEmailButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (lastSavedFileUri == null) {
					debugStatus("Нечего прикрепить к письму");
					return;
				}
				Intent emailIntent = new Intent(Intent.ACTION_SEND);
				emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"ivict@rambler.ru"});		  
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Accelerometer tracking info");
				EditText editText = (EditText) findViewById(R.id.test_case_editor);
				String text = editText.getText().toString();
				emailIntent.putExtra(Intent.EXTRA_TEXT, text);
				emailIntent.setType("message/rfc822");
				emailIntent.putExtra(Intent.EXTRA_STREAM, lastSavedFileUri);
				startActivity(Intent.createChooser(emailIntent, "Выбор почтового клиета :"));			}
		});
		
		EditText testCaseEditor = (EditText) findViewById(R.id.test_case_editor);
		testCaseEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.test_case_editor && !hasFocus) {
		            hideSoftKeyboard(v);
		        }
		    }
		});
	}
	
	private void initSensors() {
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		final List<Sensor> sensors = sensorManager
				.getSensorList(Sensor.TYPE_ACCELEROMETER);

		if (sensors.isEmpty()) {
			Log.d(TAG, "No accelerometer sensors");
			return;
		}

		if (sensors.size() > 1) {
			Log.w(TAG, "Many accelerometer sensors");
		}
		sensor = sensors.get(0);

	}
	
	private void startMeasure() {
		accelerometerData.setMaximumRange(sensor.getMaximumRange());
		//accelerometerData.setMinDelay(sensor.getMinDelay());
		accelerometerData.setName(sensor.getName());
		accelerometerData.setPower(sensor.getPower());
		accelerometerData.setResolution(sensor.getResolution());
		accelerometerData.setType(sensor.getType());
		accelerometerData.setVendor(sensor.getVendor());
		accelerometerData.setVersion(sensor.getVersion());
		accelerometerData.setCountOfValues(0);
		
		// Setup debug mode for accelerometer data
		// Set to true write velocity and coordinates (J1 and J2 integrals).
		CheckBox debugModeCheckbox = (CheckBox) findViewById(R.id.preparing_mode);
		boolean checked = debugModeCheckbox.isChecked();
		accelerometerData.preparingMode = checked;

		registrationSensorListener();
		if (timer == null) {
			timer = new Timer("SensorUtilTimer");
		}
		timer.schedule(new TimerTask() {
 			@Override
			public void run() {
				try {
					while (sensorEventListener.getSensorTid() == -1) {
						Thread.sleep(500);
					}
					//Update measure thread priority
					android.os.Process.setThreadPriority(sensorEventListener.getSensorTid(), -20);
					sensorEventListener.switchOffCalibration();
				} catch (InterruptedException e) { }
			}
			
		}, 1);
	}
	
	private void registrationSensorListener() {
		if (sensorManager == null) {
			Log.w(TAG,
					"Sensor manager is null, cancel registration sensor event listener.");
			return;
		}
		if (sensorEventListener == null) {
			Log.w(TAG,
					"Sensor event listener is null, cancel registration sensor event listener.");
			return;
		}
		if (sensor == null) {
			Log.w(TAG,
					"Sensor is null, cancel registration sensor event listener.");
			return;
		}
		sensorEventListener.setSensorTid(-1);
		sensorManager.unregisterListener(sensorEventListener, sensor);
		sensorEventListener.reset();
		sensorEventListener.resetJ1J2J1N();
		accelerometerData.setCountOfValues(0);
		sensorEventListener.switchOnCalibration();
		int sensorDelay = SensorManager.SENSOR_DELAY_NORMAL;
	 	//String brand = android.os.Build.BRAND;
	 	String buildHardware = android.os.Build.HARDWARE;
	 	String buildId = android.os.Build.ID;
	 	//String manufactur = android.os.Build.MANUFACTURER;
		if ( "mt6575".equals(buildHardware) && "JRO03C".equals(buildId)) {//MTS device
			sensorDelay = SensorManager.SENSOR_DELAY_FASTEST;
		}
		sensorManager.registerListener(sensorEventListener, sensor,
				sensorDelay);
		//Hack for MTS phone
		setupHighPriorityForSensorThread();
	}

	private void setupHighPriorityForSensorThread() {
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
		//Search sensor thread
		for (Thread thread : threadSet) {
			if (thread.getName().contains("SensorThread")) {
				ThreadGroup threadGroup = thread.getThreadGroup();
				int maxPriority = threadGroup != null ? threadGroup.getMaxPriority() : Thread.MAX_PRIORITY;
				thread.setPriority(maxPriority);
			}
		}
	}
	
	public byte[] getAccelerometerTrack() {
		if (accelerometerData == null) {
			return null;
		}
		try {
			final SensorData sensorData;
			ObjectMapper om = new ObjectMapper();
			// Shrink byte buffer
			if (accelerometerData.preparingMode) {
				sensorData = Sensors.shrinkDataInDebugMode(accelerometerData);
			} else {
				// //TODO: remove below code
				// int countOfValues = accelerometerData.getCountOfValues();
				// double[] ddobechi = new double[countOfValues];
				// double[] rdobechi = new double[countOfValues];
				// byte[] data = accelerometerData.data;
				// long time0 = -1;
				// long time1;
				// long timeStep = 20000000000L;
				// int j = 0;
				// for ( int i = 0; i < countOfValues; i++ ) {
				// ByteBuffer value = Sensors.readBuffer(data, i);
				// float x = value.getFloat();
				// value.position(3 * 4);
				// time1 = value.getLong();
				// if (time0 < 0) {
				// time0 = time1;
				// }
				// ddobechi[i] = x;
				// rdobechi[i] = x;
				// j++;
				// boolean lastIndex = i == countOfValues - 1;
				// if ((time1 - time0) > timeStep || lastIndex) {//Every time
				// step
				// int n = j - 1;
				// int offset = i - n;
				// double[] dbuff = new double[n];
				// double[] rbuff = new double[n];
				// System.arraycopy(ddobechi, offset, dbuff, 0, n);
				// System.arraycopy(rdobechi, offset, rbuff, 0, n);
				// daub d = new daub();
				// d.invDaubTrans(dbuff);
				// d.daubTrans(rbuff);
				// d.invDaubTrans(rbuff);
				// d.daubTrans(rbuff);
				// d.invDaubTrans(rbuff);
				// System.arraycopy(dbuff, 0, ddobechi, offset, n);
				// System.arraycopy(rbuff, 0, rdobechi, offset, n);
				// time0 = time1;
				// j = 0;
				// }
				//
				// }
				// ByteBuffer buff = ByteBuffer.wrap(data);
				// for ( int i = 0; i < countOfValues; i++ ) {
				// buff.position(i * (4 * 5));
				// buff.getFloat();
				// buff.putFloat((float) ddobechi[i]);
				// buff.putFloat((float) rdobechi[i]);
				// }

				sensorData = Sensors.shrinkData(accelerometerData);
			}

			String data = om.writeValueAsString(sensorData);
			return data.getBytes("UTF8");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return null;
	}
	
	public boolean saveAccelerometerTrackIntoFolder(File folder) {
		if (folder == null) {
			throw new IllegalArgumentException();
		}
		if (!folder.isDirectory()) {
			debugStatus("Папка отключена,\nтрек не сохранен");
			return false;
		}
		try {
			ObjectMapper om = new ObjectMapper();
			EventWithAtach event12 = new EventWithAtach();
			event12.setEventClassId((short) 12);
			event12.setDescription("Stored track at " + new Date());
			byte[] accelerometerTrack = getAccelerometerTrack();
			event12.setContent(accelerometerTrack);

			SimpleDateFormat dateTimeFormat = new SimpleDateFormat(
					"yyyyMMddHHmmss");
			String fileName = "event12-" + dateTimeFormat.format(new Date())
					+ ".json";

			File savedFile = new File(folder,
					fileName);
			FileOutputStream fos = new FileOutputStream(savedFile);

			BufferedOutputStream bos = new BufferedOutputStream(fos);
			om.writeValue(bos, event12);

			debugStatus("Трек сохранен");

			lastSavedFileUri = URI.create("file://" + savedFile.getAbsolutePath());
			
			return true;
		} catch (Throwable th) {
			debugStatus(th.getLocalizedMessage());
			Log.wtf(TAG, th);
			return false;
		}
	}

	public void debugStatus(final String debugMessage) {
		safetySetTextOutput(R.id.status_bar_text, debugMessage, false);
		//statusBar.setText(debugMessage);
	}

	private void stopMeasure() {
		if (sensorManager != null) {
			sensorManager.unregisterListener(sensorEventListener);
		}
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		sensorEventListener.setSensorTid(-1);
		File folder = new File("/mnt/sdcard/tmp");
		if (!folder.exists()) {
			folder.mkdirs();
		}
		saveAccelerometerTrackIntoFolder(folder);
	}
	
	private void safetySetTextOutput(int textViewId, final String text, final boolean append) {
		final TextView textView = (TextView) findViewById(textViewId);
		textView.post(new Runnable() {

			@Override
			public void run() {
				String finalText =  append ? textView.getText() + text : text;
				textView.setText(finalText);
			}
			
		});
	}
	
	protected void hideSoftKeyboard(View v) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

}
