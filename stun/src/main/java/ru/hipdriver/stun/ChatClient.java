/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.stun;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.Scanner;

public class ChatClient {
	
	private final String host;
	private final int port;

	public ChatClient(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public void run() throws Exception {
		EventLoopGroup group = new NioEventLoopGroup();
		
        Scanner sc = new Scanner(System.in);
		
		try {
			Bootstrap bootstrap = new Bootstrap()
				.group(group)
				.channel(NioSocketChannel.class)
				.handler(new ChatClientInitializer());
			
			Channel channel = bootstrap.connect(host, port).sync().channel();
			//BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
	        while(sc.hasNextLine()) {
				String line = sc.nextLine() + "\n";
	        	channel.write(line);
	        	channel.flush();
	        }
		} finally {
			group.shutdownGracefully();
			sc.close();
		}
	}
	
	public static void main(String[] arg) throws Exception {
		new ChatClient("stun.hipdriver.ru", 5766).run();
		//new ChatClient("localhost", 5766).run();
	}
}
