package ru.hipdriver.stun;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.GlobalEventExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChatServer {
	
	private final static Logger log = LoggerFactory.getLogger(ChatServer.class);
	
	static final String NO_DEST_CH = "NO_DEST_CH";
	static final String NO_DEST_CH_NL = "NO_DEST_CH\n";
	
	static final int DEFAULT_CAPACITY = 10000;
	
	static final long MAX_TIMEOUT_IN_MILLIS = TimeUnit.MINUTES.toMillis(6);
	static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
	static final Map<Channel, Long> index = new ConcurrentHashMap<Channel, Long>(DEFAULT_CAPACITY);
	static final Map<Long, Channel> reverseIndex = new ConcurrentHashMap<Long, Channel>(DEFAULT_CAPACITY);
	static final Map<Channel, Long> timestamps = new ConcurrentHashMap<Channel, Long>(DEFAULT_CAPACITY);

	public static final String NL = "\n";
	
	private final int port;

	public ChatServer(int port) {
		this.port = port;
	}
	
	public void run() throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		Thread deadlinkCollectorThread = new DeadlinkCollectorThread();
		
		try {
			ServerBootstrap bootstrap = new ServerBootstrap()
				.group(bossGroup, workerGroup)
				.channel(NioServerSocketChannel.class)
				.childHandler(new ChatServerInitializer());
			
			log.info(" /===================\\");
			log.info("|| Start chat server ||");
			log.info(" \\===================/");
			log.debug(bootstrap.toString());
			
			deadlinkCollectorThread.start();
			
			bootstrap.bind(port).sync().channel().closeFuture().sync();
		} finally {
			deadlinkCollectorThread.interrupt();
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}
	
	public static void main(String[] args) {
		try {
			log.info("================================================================================");
			log.info("Good hour for work.");
			new ChatServer(5766).run();
		} catch (Throwable th) {
			log.error("Server shutdown unexpectly", th);
			System.exit(0);
		}
	}
	
	protected static void updateIndexes(Channel channel, long mobileAgentId) {
		ChatServer.index.put(channel, mobileAgentId);
		ChatServer.reverseIndex.put(mobileAgentId, channel);
		ChatServer.timestamps.put(channel, System.currentTimeMillis());
	}

	protected static void removeIndexes(Channel channel) {
		Long mobileAgentId = ChatServer.index.remove(channel);
		ChatServer.timestamps.remove(channel);
		if (mobileAgentId != null) {
			ChatServer.reverseIndex.remove(mobileAgentId);
		}
	}
}
