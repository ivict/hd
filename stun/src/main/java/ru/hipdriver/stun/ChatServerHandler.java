package ru.hipdriver.stun;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.util.ChatMsgParser;

public class ChatServerHandler extends SimpleChannelInboundHandler<String> {
	

	private final static Logger log = LoggerFactory.getLogger(ChatServer.class);
	private static final GenericFutureListener<? extends Future<? super Void>> CLOSE_CHANNEL_LISTENER =
	new ChannelFutureListener() {
        @Override
        public void operationComplete(ChannelFuture future) throws Exception {
        	Channel channel = future.channel();
    		ChatServer.removeIndexes(channel);
        }
    };
	
	@Override
	public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
		Channel incoming = ctx.channel();
		ChatServer.channels.add(incoming);
		//Add into dead-link-collector field-of-view
		ChatServer.timestamps.put(incoming, System.currentTimeMillis());
		incoming.closeFuture().addListener(CLOSE_CHANNEL_LISTENER);
		String msg = "[SERVER] - " + incoming.remoteAddress() + " has joined!";
		log.info(msg);
	}

	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		Channel incoming = ctx.channel();
		ChatServer.channels.remove(incoming);
		String msg = "[SERVER] - " + incoming.remoteAddress() + " has left!";
		log.info(msg);
	}
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg)
			throws Exception {
		final Channel incoming = ctx.channel();
		
		Long mobileAgentId = ChatServer.index.get(incoming);
		//New channel
		if (mobileAgentId == null) {
			newIncomingConnectionHandler(incoming, msg);
			return;
		}
		
		//Exist channel:
		ChatServer.updateIndexes(incoming, mobileAgentId);
		
		//Single byte is emulate TCP_KEEPALIVE option
		if (msg.length() <= 1) {
			log.debug("SO_KEEPALIVE emulation {}", incoming);
			return;
		}
		
		//Get second parameter and redirect message
		long mobileAgentIdTo = ChatMsgParser.getMobileAgentIdTo(msg);
		if (mobileAgentIdTo == IMobileAgent.INVALID_ID) {
			log.debug("Broadcasting not supported");
			return;
		}
		
		String processStatus = null;
		try {
			Channel destinationChannel = ChatServer.reverseIndex.get(mobileAgentIdTo);
			if (destinationChannel == null) {
				processStatus = ChatServer.NO_DEST_CH;
				if (incoming.isWritable()) {
					StringBuilder text = new StringBuilder(ChatServer.NO_DEST_CH);
					String componentSessionsIdAsString = ChatMsgParser.getComponentSessionsIdAsString(msg);
					if (!componentSessionsIdAsString.isEmpty()) {
						text.append('(').append(componentSessionsIdAsString).append(')');
					}
					text.append(ChatServer.NL);
					try {
						incoming.writeAndFlush(text.toString());
					} catch (Throwable th) {
						processStatus = "FAILED";
						log.error("FAILED PROCESSING", th);
					}
				}
				return;
			}
			
			try {
				destinationChannel.writeAndFlush(msg + ChatServer.NL);
				processStatus = "PROCESSED";
			} catch (Throwable th) {
				processStatus = "FAILED";
				log.error("FAILED PROCESSING", th);
			}
			
			
		} finally {
			if (log.isDebugEnabled()) {
				log.debug((processStatus == null ? "": "[" + processStatus + "] ") + "[" + incoming.remoteAddress() + "] [" + mobileAgentId + " => " + mobileAgentIdTo + "] " + msg);
			}
		}
	}

	private void newIncomingConnectionHandler(Channel channel, String msg) {
		long mobileAgentIdFrom = ChatMsgParser.getMobileAgentIdFrom(msg);
		if (mobileAgentIdFrom == IMobileAgent.INVALID_ID) {
			log.debug("invalid-mobile-agent-id");
			channel.close();
			return;
		}
		
		//TODO: Filtering by third parameter
		//Client before sending AUTHORIZATION REQUEST CAN SEND HASH-SUM FOR AUTHENTIFICATION
		//HASH SUM is a key from application server (sid).
		//ChatMsgParser.getComponentSessionsId(msg);
		//Link from application server mobileAgentIdFrom <-> componentSessionId as hash sum for daily or hourly basis.

		long mobileAgentIdTo = ChatMsgParser.getMobileAgentIdTo(msg);
		if (mobileAgentIdTo == IMobileAgent.INVALID_ID) {
			log.debug("Broadcasting not supported");
			return;
		}

		String processStatus = null;
		try {
			ChatServer.updateIndexes(channel, mobileAgentIdFrom);

			Channel destinationChannel = ChatServer.reverseIndex.get(mobileAgentIdTo);
			if (destinationChannel == null) {
				processStatus = ChatServer.NO_DEST_CH;
				if (channel.isWritable()) {
					try {
						channel.writeAndFlush(ChatServer.NO_DEST_CH + "\n");
					} catch (Throwable th) {
						processStatus = "FAILED";
						log.error("FAILED PROCESSING", th);
					}
				}
				return;
			}
			
			try {
				destinationChannel.writeAndFlush(msg + "\n");
				processStatus = "PROCESSED";
			} catch (Throwable th) {
				processStatus = "FAILED";
				log.error("FAILED PROCESSING", th);
			}
			
			
		} finally {
			if (log.isDebugEnabled()) {
				log.debug((processStatus == null ? "": "[" + processStatus + "] ") + "[" + channel.remoteAddress() + "] [" + mobileAgentIdFrom + " => " + mobileAgentIdTo + "] " + msg);
			}
		}
	}

}
