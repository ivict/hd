package ru.hipdriver.stun;

import io.netty.channel.Channel;

import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeadlinkCollectorThread extends Thread {

	private final static Logger log = LoggerFactory.getLogger(DeadlinkCollectorThread.class);

	public DeadlinkCollectorThread() {
		super(new Runnable() {

			@Override
			public void run() {
				try {
					while (true) {
						Thread.sleep(TimeUnit.MINUTES.toMillis(1));
						for (Entry<Channel, Long> e : ChatServer.timestamps.entrySet()) {
							if (Thread.interrupted()) {
								return;
							}
							Channel channel = e.getKey();
							Long timestamp = e.getValue();
							if (timestamp == null) {
								continue;
							}
							long timeInFuture = timestamp + ChatServer.MAX_TIMEOUT_IN_MILLIS;
							if (timeInFuture < System.currentTimeMillis()) {
								safetyCloseChannel(channel);
							}
						}
					}
				} catch (InterruptedException e) {
					log.info("deadlink-collector-interrupted");
				}
			}

			private void safetyCloseChannel(Channel channel) {
				try {
					log.debug("channel-timeout [" + channel.toString() + "]");
					channel.close();
				} catch (Throwable th) {
					log.error("Unexpected situation", th);
				}
			}
			
		}, "dead-link-collector");
	}
	
}
