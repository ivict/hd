/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Состояние машины. В качестве примера можно привести:
 * В угоне, колеса снимаются, машина эвакуируется.
 * @author ivict
 */
public interface ICarState {

	short getId();
	void setId(short id);
	
	String getName();
	void setName(String name);
	
	String getDescription();
	void setDescription(String description);

}
