/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Запрос на регистрацию пользователя Drupal.
 * New private office registration. 
 * 
 * @author ivict
 * 
 */
public interface ISignUpPo extends ISignUp {

	// USER ID in drupal
	long getId();
	void setId(long id);
	
	// e-mail in drupal
	String getEmail();
	void setEmail(String email);

	// REFERRAL USER ID in drupal
	long getReferralId();
	void setReferralId(long id);
	
}
