/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

import java.util.Date;

/**
 * Трек мобильного агента.
 * <b><font color="green">Warning:</font>Содержит неявное утверждение о равности размеров структур</b><br>
 * Содержит идентификатор состояния машины, список координат и набор времен.
 * 
 * @author ivict
 */
public interface ITrack {
	
	/**
	 * Get polyline for mobile agent track, contains array of lat, lon values.<br>
	 * Example: {{13.38f, 52.51f}, {30.30f, 50.27f}}
	 * @return null if polyline not defined. 
	 */
	float[][] getPolyline();

	/**
	 * Set polyline for mobile agent track, array contains lat, lon pairs.<br>
	 * Example: {{13.38f, 52.51f}, {30.30f, 50.27f}}
	 * @param polyline null if not defined. 
	 */
	void setPolyline(float[][] polyline);

	/**
	 * Get timestamps for mobile agent track, contains array of measured time in milliseconds.<br>
	 * Example: {{1388724697000L}, {1388724715000L}}
	 * Length of array must be equals to length of polyline array.
	 * @return null if polyline not defined. 
	 */
	long[] getTimestamps();

	/**
	 * Set timestamps for mobile agent track, contains array of measured time in milliseconds.<br>
	 * Example: {{1388724697000L}, {1388724715000L}}
	 * Length of array must be equals to length of polyline array.
	 * @param null if polyline not defined. 
	 */
	void setTimestamps(long[] timestamps);
	
	/**
	 * Get car state identity.
	 * @return 0 if not set.
	 */
	short getCarStateId();
	
	/**
	 * Set car state identity.
	 * @param carStateId 0 if not set.
	 */
	void setCarStateId(short carStateId);
	
	/**
	 * Get server timestamp for pushed event.
	 * @return null if not applying.
	 */
	Date getReceivedTime();
	
	/**
	 * Set server timestamp for pushed event.
	 * @param receivedTime null if not applying.
	 */
	void setReceivedTime(Date receivedTime);

	/**
	 * Get track identity.
	 * @return group-by-track value.
	 */
	long getTrackId();
	
	/**
	 * Set track identity.
	 * @param group-by-track value.
	 */
	void setTrackId(long trackId);
	

}
