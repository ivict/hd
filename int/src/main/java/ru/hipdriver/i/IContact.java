/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Контакт пользователя.
 * 
 * @author ivict
 */
public interface IContact {

	/**
	 * Get contact id, i.e. registered number in database.
	 * Unique for user id.
	 * 
	 * @return contact id or 0 if undefined.
	 */
	long getId();

	/**
	 * Set contact id, i.e. registered number in database.
	 * Unique for user id.
	 * 
	 * @param contact id or 0 if undefined.
	 */
	void setId(long id);

	/**
	 * Get user id, i.e. registered number in database.
	 * 
	 * @return user id or 0 if undefined.
	 */
	public int getUserId();

	/**
	 * Set user id, i.e. registered number in database.
	 * 
	 * @param user id or 0 if undefined.
	 */
	public void setUserId(int userId);

	/**
	 * Get contact type id, i.e. registered number in database.
	 * @return contact type id or 0 if undefined.
	 */
	short getContactTypeId();

	/**
	 * Set contact type id, i.e. registered number in database.
	 * @param contact type id or 0 if undefined.
	 */
	void setContactTypeId(short contactTypeId);

	/**
	 * Get name of contact, name contains payload for additional info.
	 * @return name of contact (e-mail, skype or other contact type. 
	 */
	String getName();

	/**
	 * Set name of contact, name contains payload for additional info.
	 * @param name of contact (e-mail, skype or other contact type. 
	 */
	void setName(String name);

}
