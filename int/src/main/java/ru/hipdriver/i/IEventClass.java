/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;


/**
 * Интерфейс класса события
 * 
 * @author ivict
 */
public interface IEventClass {

	/**
	 * Get event class id, i.e. registered number in database.
	 * @return event class id or 0 if undefined.
	 */
	public short getId();
	/**
	 * Set event class id, i.e. registered number in database.
	 * @param event id or 0 if undefined.
	 */
	public void setId(short id);
	
	/**
	 * Get event class name.
	 * @return valid unique name for event class. 
	 */
	public String getName();
	/**
	 * Set event class name.
	 * @param valid unique name for event class. 
	 */
	public void setName(String name);

}
