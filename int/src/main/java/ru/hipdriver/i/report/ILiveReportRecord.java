/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i.report;

import java.util.Map;

/**
 * Запись к "живым" отчетам.
 * @author ivict
 */
public interface ILiveReportRecord {
	
	/**
	 * Get record primary key.
	 * @return key which can be serialized to/from json.
	 */
	public Object getKey();
	
	/**
	 * Set record primary key.
	 * @param key which can be serialized to/from json.
	 */
	public void setKey(Object key);

	/**
	 * Get record data.
	 * @return data for record entry, for simple record it's a one row in table.
	 */
	public Map<String, String> getData();

	/**
	 * Set record data.
	 * @param data for record entry, for simple record it's a one row in table.
	 */
	public void setData(Map<String, String> data);
	
}
