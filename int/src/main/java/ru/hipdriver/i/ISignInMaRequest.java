/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

import java.util.Map;

/**
 * Запрос на начало пользовательской сессии c мобильным агентом.
 * Передача userId и mobileAgentId по открытому каналу, относительно безопасна.
 * Поскольку возвращаемый ISid зашифрован открытой частью ключа (ma-key-rsa),
 * мобильного агента.
 * @author ivict
 *
 */
public interface ISignInMaRequest {

	//User id
	int getUserId();
	void setUserId(int userId);
	
	//Mobile agent id
	long getMobileAgentId();
	void setMobileAgentId(long mobileAgentId);
	
	/**
	 * Get mobile agent memory entries. 
	 * 
	 * @return null if no changes.
	 */
	Map<String, Object> getProperties();
	
	/**
	 * Set mobile agent memory entries. 
	 * 
	 * @return null if no changes.
	 */
	void setProperties(Map<String, Object> properties);
	
}
