/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;


/**
 * Интерфейс атача к письму. 
 * @author valentina
 *
 */
public interface IEmailMessageAtach extends IEmailObject {
	
	int getMessageIdentity();
	
	void setMessageIdentity(int messageIdentity);

	String getContentType();
	
	void setContentType(String contentType);
	
	String getName();
	
	void setName(String name);
	
	int getAtachNumber();
	
	void setAtachNumber(int atachNumber);

	int getFragmentsCount();
	
	void setFragmentsCount(int fragmentsCount);
	
}
