/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Состояние приложения. В качестве примера можно привести:
 * Сигнализация включена, Сигнализация отключена.
 * @author ivict
 */
public interface IAppState {
	short getId();
	void setId(short id);
	
	String getName();
	void setName(String name);

	String getDescription();
	void setDescription(String description);
}
