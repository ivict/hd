/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Мобильный агент. 
 * @author ivict
 */
public interface IMobileAgent {
	
	final static String ALERT_PHONE_NUMBER_KEY = "alert-phone-number";
	final static String MOBILE_AGENT_NAME_KEY = "mobile-agent-name";
	final static String INCOMING_MESSAGE_KEY = "incoming-message";
	final static String USER_KEY_EXPIRED_IN_MINUTES_KEY = "user-key-expired-in-minutes";
	final static String MOBILE_AGENT_ID_KEY = "mobile-agent-id";
	final static String CURRENT_MOBILE_AGENT_VERSION_KEY = "current-mobile-agent-version";
	final static String MOBILE_AGENT_VERSION_NAME_KEY = "mobile-agent-version";
	final static String MOBILE_AGENT_VERSION_CODE_KEY = "mobile-agent-version-code";
	final static String ALERT_EMAIL_KEY = "alert-e-mail";
	static final long INVALID_ID = 0;
	
	long getId();

	void setId(long id);

	int getUserId();

	void setUserId(int userId);

	@Deprecated
	String getName();

	@Deprecated
	void setName(String name);

	long getImei();

	void setImei(long imei);
	
}
