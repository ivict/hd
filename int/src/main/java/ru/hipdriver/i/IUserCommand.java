/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

import java.util.Date;

/**
 * Команда пользователя мобильному агенту. 
 * @author ivict
 */
public interface IUserCommand {

	long getId();
	void setId(long id);
	
	int getUserId();
	void setUserId(int userId);
	
	long getMobileAgentId();
	void setMobileAgentId(long mobileAgentId);
	
	short getUserCommandTypeId();
	void setUserCommandTypeId(short commandTypeId);
	
	byte[] getArgs();
	void setArgs(byte[] args);
	
	long getArgsCheckSum();
	void setArgsCheckSum(long argsCheckSum);

	public Date getSendTime();
	public void setSendTime(Date sendTime);
}
