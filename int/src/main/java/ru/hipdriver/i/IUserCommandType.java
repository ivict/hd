/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Тип команды пользователя, которую он желает передать
 * мобильному устройству. В качестве примера можно привести:
 * Включить ("Сигнализацию"), Выключить ("Сигнализацию")
 * @author ivict
 */
public interface IUserCommandType {

	short getId();
	void setId(short id);
	
	String getName();
	void setName(String name);

	String getDescription();
	void setDescription(String description);

}
