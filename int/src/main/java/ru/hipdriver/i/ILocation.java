/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Геграфическая локация.
 * 
 * @author ivict
 */
public interface ILocation {

	/**
	 * Константа обозначающая неизвестную высоту.
	 */
	public static final int UNKNOWN_ALTITUDE = Integer.MIN_VALUE;
	/**
	 * Константа обозначающая неизвестную локацию для сотовой сети.
	 */
	public static final int UNKNOWN_LOCATION_AREA_CODE = -1;
	/**
	 * Константа обозначающая неизвестный идентификатор соты.
	 */
	public static final int UNKNOWN_CELL_ID = -1;
	/**
	 * Константа обозначающая неизвестную широту.
	 */
	public static final int UNKNOWN_LATITUDE = 0;
	/**
	 * Константа обозначающая неизвестную долготу.
	 */
	public static final int UNKNOWN_LONGITUDE = 0;
	/**
	 * Константа обозначающая неизвестную точность.
	 */
	public static final int UNKNOWN_ACCURACY = -1;
	/**
	 * Константа обозначающая неизвестный код страны.
	 * @see http://en.wikipedia.org/wiki/Mobile_country_code
	 * TODO: add mask for count of leading zero
	 */
	public static final int UNKNOWN_MOBILE_COUNTRY_CODE = 0;
	/**
	 * Константа обозначающая неизвестный код мобильной сети.
	 * @see http://en.wikipedia.org/wiki/Mobile_network_code
	 */
	public static final int UNKNOWN_MOBILE_NETWORK_CODE = 0;
	
	
	/**
	 * Factor for getting real values for latitude and longitude.
	 * 
	 * @return natural number always more then 1;
	 */
	public int getFactor();

	/**
	 * Set factor for getting real values for latitude and longitude.
	 * 
	 * @param factor natural number always more then 1;
	 */
	public void setFactor(int factor);

	/**
	 * Latitude, real latitude getting as getLat() / getFactor()
	 * 
	 * @return integer latitude
	 */
	public int getLat();

	/**
	 * Set latitude, integer latitude setting as (real latitude) * getFactor()
	 * 
	 * @param latitude integer latitude
	 */
	public void setLat(int latitude);

	/**
	 * Longitude, real longitude getting as getLon() / getFactor()
	 * 
	 * @return integer longitude
	 */
	public int getLon();

	/**
	 * Set longitude, integer longitude setting as (real longitude) *
	 * getFactor()
	 * 
	 * @param longitude integer longitude
	 */
	public void setLon(int longitude);
	
	/**
	 * Set altitude in centimeters.
	 * @return altitude based on sea level, Integer.MIN_VALUE if undefined 
	 */
	public int getAlt();
	/**
	 * Set altitude in centimeters.
	 * @param altitude based on sea level, Integer.MIN_VALUE if undefined 
	 */
	public void setAlt(int alt);

	/**
	 * Get accuracy in centimeters.
	 * @return measure accuracy, 0 if undefined 
	 */
	public int getAcc();
	/**
	 * Set accuracy in centimeters.
	 * @param measure accuracy, 0 if undefined 
	 */
	public void setAcc(int acc);

	/**
	 * Get mobile country code, for GSM operator.
	 * @return valid mcc or 0 if undefined
 	 * TODO: add mask for count of leading zero in text representation
 	 * @see http://en.wikipedia.org/wiki/Mobile_country_code
	 */
	public int getMcc();
	/**
	 * Set mobile country code, for GSM operator.
	 * @param valid mcc or 0 if undefined 
 	 * TODO: add mask for count of leading zero in text representation
 	 * @see http://en.wikipedia.org/wiki/Mobile_country_code
	 */
	public void setMcc(int mcc);

	/**
	 * Get mobile network code, for GSM operator.
	 * @return valid mnc or 0 if undefined 
 	 * TODO: add mask for count of leading zero in text representation
 	 * @see http://en.wikipedia.org/wiki/Mobile_network_code
	 */
	public int getMnc();
	/**
	 * Get mobile network code, for GSM operator.
	 * @param valid mnc or 0 if undefined
 	 * TODO: add mask for count of leading zero in text representation
 	 * @see http://en.wikipedia.org/wiki/Mobile_network_code
	 */
	public void setMnc(int mnc);

	/**
	 * Get location area code, for GSM operator.
	 * @return valid lac or -1 if undefined 
	 */
	public int getLac();
	/**
	 * Set location area code, for GSM operator.
	 * @param valid lac or -1 if undefined 
	 */
	public void setLac(int lac);

	/**
	 * Get cell id, for GSM operator.
	 * @return valid cell id or -1 if undefined 
	 */
	public int getCid();
	/**
	 * Set cell id, for GSM operator.
	 * @param valid cell id or -1 if undefined 
	 */
	public void setCid(int cid);

	/**
	 * Incapsulate location validation
	 * 
	 * @author ivict
	 *
	 */
	public static class Validator {
		private Validator() {};
		
		public static boolean isUnknownGsmLocation(ILocation location) {
			return location.getLac() == UNKNOWN_LOCATION_AREA_CODE;
		}

		public static boolean isUnknownGsmLocation(int mcc, int mnc, int lac, int cid) {
			return lac == UNKNOWN_LOCATION_AREA_CODE;
		}

		public static boolean isUnknownCdmaLocation(ILocation location) {
			return location.getCid() == UNKNOWN_CELL_ID;
		}
		
		public static boolean isUnknownCdmaLocation(int mcc, int mnc, int lac, int cid) {
			return cid == UNKNOWN_CELL_ID;
		}
		
		public static boolean isUnknownLocation(ILocation location) {
			return location.getLat() == UNKNOWN_LATITUDE ||
					location.getLon() == UNKNOWN_LONGITUDE;
		}

		public static boolean isUnknownLocation(int lat, int lon) {
			return lat == UNKNOWN_LATITUDE ||
					lon == UNKNOWN_LONGITUDE;
		}
	}

}
