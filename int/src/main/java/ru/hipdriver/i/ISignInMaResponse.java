/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;


/**
 * Ответ сервера на запрос начала пользовательской сессии c мобильным агентом.
 * @author ivict
 *
 */
public interface ISignInMaResponse extends ISid, IMobileAgentProfile, IProperties, IUserLimit { }
