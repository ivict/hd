/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Профиль мобильного агентв, набор фич мобильного агента, таких как задержка
 * при повторной отправке события тревоги и т.д.
 * 
 * @author ivict
 * 
 */
public interface IMobileAgentProfile {
	
	public static final String DELAY_PUSH_ALERT_EVENT_IN_SECONDS = "delay-push-alert-event-in-seconds";
	public static final String DELAY_WHERE_AM_I_REQUEST_IN_SECONDS = "delay-where-am-i-request-in-seconds";
	public static final String ENABLE_DEBUG_MODE = "enable-debug-mode";
	public static final String ASD_MAX_JERK = "asd-max-jerk";
	public static final String ASD_MAX_ACCELERATION = "asd-max-acceleration";
	public static final String ASD_MAX_ANGLE = "asd-max-angle";
	public static final String OPTIMAL_ASD_MAX_JERK = "optimal-asd-max-jerk";
	public static final String OPTIMAL_ASD_MAX_ACCELERATION = "optimal-asd-max-acceleration";
	public static final String OPTIMAL_ASD_MAX_ANGLE = "optimal-asd-max-angle";
	public static final String REQUEST_SERVER_IF_UNKNOWN_NUMBER = "request-server-if-unknown-number";
	public static final String ENABLE_ENERGY_SAVING_MODE = "enable-energy-saving-mode";
	public static final String SWITCH_ON_BEACON_MODE = "switch-on-beacon-mode";
	public static final String WATCHDOG_SERVICE_START_DELAY_IN_SECONDS = "watchdog-service-start-delay-in-seconds";
	public static final String BEACON_INTERVAL_IN_MINUTES = "beacon-interval-in-minutes";
	public static final String BEACON_PWR_TRESHOLD_PERCENTS = "beacon-pwr-treshold-percents";
	public static final String CAR_AUTO_ALARMS_AFTER_CAR_POKE = "car-auto-alarms-after-car-poke";
	public static final String CAR_AUTO_ALARMS_AFTER_STEAL_WHEELS = "car-auto-alarms-after-steal-wheels";
	public static final String ALERT_CALLS_COUNT = "alert-calls-count";
	public static final String ALERT_CALLS_INTERVAL_IN_SECONDS = "alert-calls-interval-in-seconds";
	public static final String ALERT_CALLS_DURATION_IN_SECONDS = "alert-calls-duration-in-seconds";
	public static final String SEND_SMS_AFTER_EVENT_CLASS_DETECTION = "send-sms-after-event-class-detection";
	public static final String SEND_EMAIL_AFTER_EVENT_CLASS_DETECTION = "send-email-after-event-class-detection";
	public static final String SEND_EMAIL_ON_POWER_EVENT = "send-email-on-power-event";
	public static final String SEND_EMAIL_ON_SWITCH_ON_SWITCH_OFF = "send-email-on-switch-on-switch-off";
	public static final String SEND_PHOTO_AFTER_ALERT = "send-photo-after-alert";
	public static final String COUNT_OF_PHOTOS = "count-of-photos";
	public static final String INTERVAL_BETWEEN_PHOTOS_IN_SECONDS = "interval-between-photos-in-seconds";
	public static final String RUN_AFTER_REBOOT = "run-after-reboot";//If application end of work in normal state, then not run after reboot
	public static final String WAIT_UNTIL_THE_ALERT_CALL = "wait-until-the-alert-call";
	public static final String CONTROL_MODE_ID = "control-mode-id";
	public static final String ALARM_ON_POWER_CABLE_CONNECT = "alarm-on-power-cable-connect";
	public static final String ALARM_ON_POWER_CABLE_DISCONNECT = "alarm-on-power-cable-disconnect";
	public static final String MAP_PROVIDER_ID = "map-provider-id";
	//If change this constant - update database defaults
	public static final int DEFAULT_DELAY_PUSH_ALERT_EVENT_IN_SECONDS = 30;
	//If change this constant - update database defaults
	public static final int DEFAULT_DELAY_WHERE_AM_I_REQUEST_IN_SECONDS = 5;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_ENABLE_DEBUG_MODE = false;
	//If change this constant - update database defaults
	public static final float DEFAULT_ASD_MAX_JERK = 1000f;
	//If change this constant - update database defaults
	public static final float DEFAULT_ASD_MAX_ACCELERATION = 80f;
	//If change this constant - update database defaults
	public static final float DEFAULT_ASD_MAX_ANGLE = 1.7f;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_REQUEST_SERVER_IF_UNKNOWN_NUMBER = false;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_ENABLE_ENERGY_SAVING_MODE = false;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_SWITCH_ON_BEACON_MODE = false;
	//If change this constant - update database defaults
	public static final int DEFAULT_WATCHDOG_SERVICE_START_DELAY_IN_SECONDS = 30;
	//If change this constant - update database defaults
	public static final int DEFAULT_BEACON_INTERVAL_IN_MINUTES = 60;
	//If change this constant - update database defaults
	public static final float DEFAULT_BEACON_PWR_TRESHOLD_PERCENTS = 10f;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_CAR_AUTO_ALARMS_AFTER_CAR_POKE = false;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_CAR_AUTO_ALARMS_AFTER_STEAL_WHEELS = false;
	//If change this constant - update database defaults
	public static final int DEFAULT_ALERT_CALLS_COUNT = 1;
	//If change this constant - update database defaults
	public static final int DEFAULT_ALERT_CALLS_INTERVAL_IN_SECONDS = 60;
	//If change this constant - update database defaults
	public static final int DEFAULT_ALERT_CALLS_DURATION_IN_SECONDS = 20;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_SEND_SMS_AFTER_EVENT_CLASS_DETECTION = false;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_SEND_EMAIL_AFTER_EVENT_CLASS_DETECTION = false;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_SEND_EMAIL_ON_POWER_EVENT = false;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_SEND_EMAIL_ON_SWITCH_ON_SWITCH_OFF = false;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_SEND_PHOTO_AFTER_ALERT = false;
	//If change this constant - update database defaults
	public static final int DEFAULT_COUNT_OF_PHOTOS = 1;
	//If change this constant - update database defaults
	public static final int DEFAULT_INTERVAL_BETWEEN_PHOTOS_IN_SECONDS = 5;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_RUN_AFTER_REBOOT = true;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_WAIT_UNTIL_THE_ALERT_CALL = false;
	//If change this constant - update database defaults
	public static final short DEFAULT_CONTROL_MODE_ID = 7;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_ALARM_ON_POWER_CABLE_CONNECT = false;
	//If change this constant - update database defaults
	public static final boolean DEFAULT_ALARM_ON_POWER_CABLE_DISCONNECT = false;
	//If change this constant - update database defaults
	public static final short DEFAULT_MAP_PROVIDER_ID = 0;

	/**
	 * Get mobile agent id, i.e. registered number in database.
	 * 
	 * @return event class id or 0 if undefined.
	 */
	long getMobileAgentId();

	/**
	 * Set mobile agent id, i.e. registered number in database.
	 * 
	 * @param mobileAgentId
	 *            event class id or 0 if undefined.
	 */
	void setMobileAgentId(long mobileAgentId);

	/**
	 * Get delay push alert event in seconds.
	 * 
	 * @return zero value switch of this function.
	 */
	int getDelayPushAlertEventInSeconds();

	/**
	 * Set delay push alert event in seconds.
	 * 
	 * @param delayPushAlertEventInSeconds
	 *            zero value switch of this function.
	 */
	void setDelayPushAlertEventInSeconds(int delayPushAlertEventInSeconds);

	/**
	 * Get delay where am i request (fill track on mobile agent) in seconds.
	 * 
	 * @return if zero or less then zero, behavior undefined
	 */
	int getDelayWhereAmIRequestInSeconds();

	/**
	 * Set delay where am i request (fill track on mobile agent) in seconds.
	 * 
	 * @param delayWhereAmIRequestInSeconds
	 *            if zero or less then zero, behavior undefined.
	 */
	void setDelayWhereAmIRequestInSeconds(int delayWhereAmIRequestInSeconds);

	/**
	 * Is debug mode switch on.
	 * In debug mode mobile agent send additional info like small sensor-track and so on.
	 * @return true if enabled
	 */
	boolean isEnableDebugMode();
	
	/**
	 * Set debug mode switch on/off.
	 * In debug mode mobile agent send additional info like small sensor-track and so on.
	 * @param enableDebugMode true for enabling false for disabling.
	 */
	void setEnableDebugMode(boolean enableDebugMode);
	
	/**
	 * Get alert state detector constraint parameter: maximum of jerk in (cm/s^3).
	 * 
	 * @return always more then zero.
	 */
	float getAsdMaxJerk();

	/**
	 * Set alert state detector constraint parameter: maximum of jerk in (cm/s^3).
	 * 
	 * @param asdMaxJerk
	 *            always more then zero.
	 */
	void setAsdMaxJerk(float asdMaxJerk);

	/**
	 * Get alert state detector constraint parameter: maximum of acceleration in (cm/s^2).
	 * 
	 * @return always more then zero.
	 */
	float getAsdMaxAcceleration();

	/**
	 * Set alert state detector constraint parameter: maximum of acceleration in (cm/s^2).
	 * 
	 * @param asdMaxJerk
	 *            always more then zero.
	 */
	void setAsdMaxAcceleration(float asdMaxAcceleration);

	/**
	 * Get alert state detector constraint parameter: maximum of angle variation in degree.
	 * 
	 * @return always more then zero.
	 */
	float getAsdMaxAngle();

	/**
	 * Set alert state detector constraint parameter: maximum of angle variation in degree.
	 * 
	 * @param asdMaxAngle
	 *            always more then zero.
	 */
	void setAsdMaxAngle(float asdMaxAngle);

	/**
	 * Get flag of energy saving mode.
	 * 
	 * @return always more then zero.
	 */
	boolean isEnableEnergySavingMode();

	/**
	 * Set flag for energy saving mode.
	 * 
	 * @param asdMaxAngle
	 *            always more then zero.
	 */
	void setEnableEnergySavingMode(boolean energySavingMode);
	
	/**
	 * Get watchdog service start delay in seconds: user go out from the alert zone.
	 * 
	 * @return always more then zero.
	 */
	int getWatchdogServiceStartDelayInSeconds();
	

	/**
	 * Set watchdog service start delay in seconds: user go out from the alert zone.
	 * 
	 * @param watchdogServiceStartDelayInSeconds
	 *            always more then zero.
	 */
	void setWatchdogServiceStartDelayInSeconds(int watchdogServiceStartDelayInSeconds);
	
	/**
	 * Get control mode identity.
	 * 
	 * @return always more then zero.
	 */
	short getControlModeId();
	

	/**
	 * Set control mode identity.
	 * 
	 * @param controlModeId.
	 */
	void setControlModeId(short controlModeId);
	
	
	/**
	 * Get map provider identity.
	 * 
	 * @return always more then zero.
	 */
	short getMapProviderId();
	

	/**
	 * Set map provider identity.
	 * 
	 * @param controlModeId.
	 */
	void setMapProviderId(short mapProviderId);
	

}
