/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;


/**
 * Послание по электронной почте
 * @author valentina
 *
 */
public interface IEmailMessage extends IEmailObject {

	String getFrom();
	
	void setFrom(String from);
	
	String getTo();
	
	void setTo(String to);

	String getSubject();
	
	void setSubject(String subject);

	String getBody();
	
	void setBody(String body);
	
	int getAtachsCount();
	
	void setAtachsCount(int atachsCount);
	
}
