/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;


/**
 * Интерфейс для доступа к атачу сообщения.
 * 
 * @author ivict
 */
public interface IEventAtach {

	public long getEventId();
	public void setEventId(long eventId);
	
	public String getDescription();
	public void setDescription(String description);

	public byte[] getContent();
	public void setContent(byte[] content);

	public long getContentCheckSum();
	public void setContentCheckSum(long contentCheckSum);

}
