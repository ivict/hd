/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Метоположение мобильного агента, для отрисовки на карте. 
 * @author ivict
 */
public interface IPlacemark {

	/**
	 * Get placemark for mobile agent, contains pair of lat, lon values.<br>
	 * Example: {53.25f, 50.22f}
	 * @return null if not defined
	 */
	float[] getPlacemark();

	/**
	 * Set placemark for mobile agent, contains pair of lat, lon values.<br>
	 * Example: {53.25f, 50.22f}
	 * @param placemark null if not defined
	 */
	void setPlacemark(float[] placemark);
	
}
