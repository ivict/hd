/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Профиль сервиса, набор фич сервиса, таких как название и т.д.
 * @author ivict
 *
 */
public interface IServiceProfile {

	short getId();
	void setId(short id);
	
	String getName();
	void setName(String name);
	
	String getDescription();
	void setDescription(String description);
	
}
