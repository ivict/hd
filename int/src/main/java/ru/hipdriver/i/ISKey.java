/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Security key, ключ безопасности, который передается мобильному агенту однократно.<br>
 * В дальнейшем ключ используется мобильным агентом для шифрования сессионнного идентификатора.
 * @author ivict
 */
public interface ISKey {
	/**
	 * Get public rsa key.
	 * @return rsa key with length 2048 or 4096
	 */
	byte[] getValue();
	/**
	 * Set public rsa key.
	 * @param value rsa key with length 2048 or 4096
	 */
	void setValue(byte[] value);
	
	/**
	 * Get user id, i.e. registered number in database.
	 * @return user identity or 0 if undefined.
	 */
	int getUserId();
	/**
	 * Set user id, i.e. registered number in database.
	 * @param userId user identity or 0 if undefined.
	 */
	void setUserId(int userId);

	/**
	 * Get mobile agent id, i.e. registered number in database.
	 * @return mobile agent id or 0 if undefined.
	 */
	long getMobileAgentId();
	/**
	 * Set mobile agent id, i.e. registered number in database.
	 * @param mobileAgentId mobile agent identity or 0 if undefined.
	 */
	void setMobileAgentId(long mobileAgentId);

	//Network payload optimization
	/**
	 * Get additional payload for network exchange optimization.
	 * Standard server response to mobile agent after sign in request. 
	 * @return sign in mobile agent response
	 */
	ISignInMaResponse getCurrentSignInMaResponse();
	/**
	 * Set additional payload for network exchange optimization.
	 * Standard server response to mobile agent after sign in request. 
	 * @param currentSignInMaResponse sign in mobile agent response
	 */
	void setCurrentSignInMaResponse(ISignInMaResponse currentSignInMaResponse);
	
	/**
	 * Get mobile agent count, i.e. count of enabled mobile agents in in database.
	 * @return 1 or more.
	 */
	int getMobileAgentCount();
	/**
	 * Set mobile agent count, i.e. count of enabled mobile agents in in database.
	 * @param mobileAgentCount mobile agent count 1 or more.
	 */
	void setMobileAgentCount(int mobileAgentCount);

}
