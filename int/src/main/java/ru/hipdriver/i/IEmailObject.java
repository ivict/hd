/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;


/**
 * Объект e-mail
 * @author valentina
 *
 */
public interface IEmailObject {

	int getIdentity();
	
	void setIdentity(int identity);
	
}
