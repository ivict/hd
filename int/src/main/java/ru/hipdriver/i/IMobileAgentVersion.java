/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Версия мобильного агента.
 * 
 * @author ivict
 */
public interface IMobileAgentVersion {

	/**
	 * Константа обозначающая неизвестную версию мобильного агента. 
	 */
	public static final int UNKNOWN_VERSION = -1;
	
	/**
	 * Get version code of application.
	 * @return more then zero or -1 if invalid version.
	 */
	int getVersionCode();
	
	/**
	 * Set version code of application.
	 * @param versionCode more then zero or -1 if invalid version.
	 */
	void setVersionCode(int versionCode);
	
	/**
	 * Get version name of application.
	 * @return string description with additional info.
	 */
	String getVersionName();
	
	/**
	 * Set version name of application.
	 * @param string description with additional info.
	 */
	void setVersionName(String versionName);
	

}
