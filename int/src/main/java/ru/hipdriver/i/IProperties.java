/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

import java.util.Map;

/**
 * Абстракция набора свойств.
 * 
 * @author ivict
 * 
 */
public interface IProperties {

	/**
	 * Get mobile agent memory entries. 
	 * 
	 * @return null if no changes.
	 */
	Map<String, Object> getProperties();
	
	/**
	 * Set mobile agent memory entries. 
	 * 
	 * @return null if no changes.
	 */
	void setProperties(Map<String, Object> properties);

}
