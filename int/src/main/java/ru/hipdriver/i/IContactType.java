/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Тип контакта пользователя.
 * В качестве примера: skype, e-mail, ICQ и многое другое.
 * 
 * @author ivict
 */
public interface IContactType {

	/**
	 * Get contact type id, i.e. registered number in database.
	 * 
	 * @return contact id or 0 if undefined.
	 */
	short getId();

	/**
	 * Set contact type id, i.e. registered number in database.
	 * 
	 * @param contact id or 0 if undefined.
	 */
	void setId(short id);
	
	/**
	 * Get unique name of contact type.
	 * @return name of contact (e-mail, skype or other contact type). 
	 */
	String getName();

	/**
	 * Set unique name of contact type.
	 * @param name of contact (e-mail, skype or other contact type). 
	 */
	void setName(String name);

}
