/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Наши драгоценные пользователи.
 * В качестве имени может быть использован e-mail.<br>
 * Id светим всюду, поскольку никакого влияния на безопасность
 * он не оказывает, а вот на скорость взаимодействия различных
 * частей системы, его отсутствие, наносит большой импакт. 
 * @author ivict.
 */
public interface IUser {
	
	public static final String USER_ID_KEY = "user-id";
	public static final int INVALID_ID = 0;
	
	int getId();

	void setId(int id);

	String getName();

	void setName(String name);

	byte[] getPasswordHash();

	void setPasswordHash(byte[] passwordHash);
	
}
