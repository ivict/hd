/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Session identity, сессионный ключ для сеанса связи с мобильным агентом. 
 * @author ivict
 */
public interface ISid {
	
	long SALTMA = 4290771397437650000L;//0x3B8BE1DA3CAD7450
	long SALTSS = 7650213974999429077L;//0x6A2B0513E391EFD5
	long SALTCS =  848845745081098664L;//0x0BC7B4B530A921A8
	long INVALID_COMPONENT_SESSIONS_ID = 0;
	long SALTPW = 1059601205490352054L;//0x0EB475AEB0D3fFB6

	/**
	 * Session identity factor A
	 * @return always be a big number
	 */
	long getA();
	/**
	 * Set session identity factor A
	 * @param a always be a big number
	 */
	void setA(long a);
	
}
