/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Запрос на регистрацию в системе.
 * @author ivict
 *
 */
public interface ISignUp {

	//User name or e-mail
	String getName();
	void setName(String name);
	
	//Password hash
	//TODO: Необратимая функция с зависимостью от времени?
	// Пример: md5("String") xor time-hour-accuracy
	byte[] getPasswordHash();
	void setPasswordHash(byte[] passwordHash);
}
