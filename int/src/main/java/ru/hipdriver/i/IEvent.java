/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

import java.util.Date;

/**
 * Базовый интерфейс, который описывает событие,
 * представляющее интерес для сервиса Hipdriver.ru <br>
 * 
 * Основной смысл - ответ на вопросы:
 * (WhoWhatWhenWhere)
 * <li>Кто?</li>
 * <li>Что?</li>
 * <li>Когда?</li>
 * <li>Где?</li>
 * @author ivict
 */
//TODO: refactoring: inheritance from ITimestampedLocation
public interface IEvent extends ILocation {
	
	/**
	 * Get event id, i.e. registered number in database.
	 * @return event id or 0 if undefined.
	 */
	public long getId();
	/**
	 * Set event id, i.e. registered number in database.
	 * @param event id or 0 if undefined.
	 */
	public void setId(long id);
	
	/**
	 * Get user id, i.e. registered number in database.
	 * @return user identity or 0 if undefined.
	 */
	public int getUserId();
	/**
	 * Set user id, i.e. registered number in database.
	 * @param userId user identity or 0 if undefined.
	 */
	public void setUserId(int userId);
	
	/**
	 * Get mobile agent id, i.e. registered number in database.
	 * @return mobile agent id or 0 if undefined.
	 */
	public long getMobileAgentId();
	/**
	 * Set mobile agent id, i.e. registered number in database.
	 * @param mobileAgentId mobile agent identity or 0 if undefined.
	 */
	public void setMobileAgentId(long mobileAgentId);
	
	public short getEventClassId();
	public void setEventClassId(short eventClassId);
	
	/**
	 * Get event time
	 * @return event date-time
	 */
	public Date getTime();
	/**
	 * Set event time
	 * @param time event date-time
	 */
	public void setTime(Date time);
	
	/**
	 * Get received event time
	 * @return received event date-time on server-side
	 */
	public Date getReceivedTime();
	/**
	 * Set received event time
	 * @param receivedTime received event date-time on server-side
	 */
	public void setReceivedTime(Date receivedTime);
	
	/**
	 * Get integer latitude.
	 * @see ru.hipdriver.g.Locations
	 * @return int-latitude or 0 if undefined
	 */
	@Override
	public int getLat();
	/**
	 * Set integer latitude.
	 * @see ru.hipdriver.g.Locations
	 * @param int-latitude or 0 if undefined
	 */
	@Override
	public void setLat(int lat);
	
	/**
	 * Get integer longitude.
	 * @see ru.hipdriver.g.Locations
	 * @return int-longitude or 0 if undefined
	 */
	@Override
	public int getLon();
	/**
	 * Set integer longitude.
	 * @see ru.hipdriver.g.Locations
	 * @param int-longitude or 0 if undefined
	 */
	@Override
	public void setLon(int lon);

	/**
	 * Set altitude in meters.
	 * @return altitude based on sea level, Integer.MIN_VALUE if undefined 
	 */
	@Override
	public int getAlt();
	/**
	 * Set altitude in meters.
	 * @param altitude based on sea level, Integer.MIN_VALUE if undefined 
	 */
	@Override
	public void setAlt(int alt);

	/**
	 * Get accuracy in meters.
	 * @return measure accuracy, 0 if undefined 
	 */
	@Override
	public int getAcc();
	/**
	 * Set accuracy in meters.
	 * @param measure accuracy, 0 if undefined 
	 */
	@Override
	public void setAcc(int acc);

	/**
	 * Get mobile country code, for GSM operator.
	 * @return valid mcc or 0 if undefined 
	 */
	@Override
	public int getMcc();
	/**
	 * Set mobile country code, for GSM operator.
	 * @param valid mcc or 0 if undefined 
	 */
	@Override
	public void setMcc(int mcc);

	/**
	 * Get mobile network code, for GSM operator.
	 * @return valid mnc or 0 if undefined 
	 */
	@Override
	public int getMnc();
	/**
	 * Get mobile network code, for GSM operator.
	 * @param valid mnc or 0 if undefined 
	 */
	@Override
	public void setMnc(int mnc);

	/**
	 * Get location area code, for GSM operator.
	 * @return valid lac or -1 if undefined 
	 */
	@Override
	public int getLac();
	/**
	 * Set location area code, for GSM operator.
	 * @param valid lac or -1 if undefined 
	 */
	@Override
	public void setLac(int lac);

	/**
	 * Get cell id, for GSM operator.
	 * @return valid cell id or -1 if undefined 
	 */
	@Override
	public int getCid();
	/**
	 * Set cell id, for GSM operator.
	 * @param valid cell id or -1 if undefined 
	 */
	@Override
	public void setCid(int cid);

}
