/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

import java.util.Date;

/**
 * Интерфейс соединения мобильного агента.<br/>
 * Содержит информацию о хосте, с которого мобильный агент<br/>
 * начинал последнюю сессию.
 * @author ivict
 *
 */
public interface IMobileAgentConnection {

	/**
	 * Возвращает адрес удаленного хоста.
	 * @return IP4/IP6 адрес
	 */
	String getAddr();
	/**
	 * Устанавливает адрес удаленного хоста.
	 * @param addr IP4/IP6 адрес
	 */
	void setAddr(String addr);
	
	/**
	 * Возвращает dns имя удаленного хоста.
	 * @return dns имя
	 */
	String getHost();
	/**
	 * Устанавливает dns имя удаленного хоста.
	 * @param dns имя
	 */
	void setHost(String host);
	
	/**
	 * Возвращает порт сокета на удаленном хосте.
	 * @return номер порта 0..65535
	 */
	int getPort();
	/**
	 * Устанавливает порт сокета на удаленном хосте.
	 * @param port номер порта 0..65535
	 */
	void setPort(int port);
	
	/**
	 * Возвращает время последнего sign in.
	 * @return время
	 */
	Date getSignInTime();
	/**
	 * Устанавливает время последнего sign in.
	 * @param signInTime время
	 */
	void setSignInTime(Date signInTime);
	
}
