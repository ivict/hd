/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Состояние мобильного агента.
 * 
 * @author ivict
 */
public interface IMobileAgentState {

	/**
	 * Константа обозначающая неизвестную силу сигнала GSM. 
	 */
	public static final int UNKNOWN_GSM_SIGNAL_STRENGTH = -1;
	/**
	 * Константа обозначающая неизвестую частотность ошибок GSM.
	 */
	public static final int UNKNOWN_GSM_BIT_ERROR_RATE = -1;
	
	long getMobileAgentId();
	void setMobileAgentId(long mobileAgentId);

	//(0-31,99)
	//0 - reserved state (no connection) @see DeadLinkTimer.
	int getGsmSignalStrength();
	void setGsmSignalStrength(int gsmSignalStrength);
	
	//(0-7,99)
	int getGsmBitErrorRate();
	void setGsmBitErrorRate(int gsmBitErrorRate);
	
	// int | BATTERY_STATUS_CHARGING, BATTERY_STATUS_FULL,
	// BATTERY_PLUGGED_USB, BATTERY_PLUGGED_AC
	// Статус батареи
	int getBatteryStatus();
	void setBatteryStatus(int batteryStatus);
	
	// Заряд батареи -1 не определено
	float getBatteryPct();
	void setBatteryPct(float batteryPct);
	
	//car alarm activated, switched off
	//Состояние мобильного приложения, null неопределено
	short getAppStateId();
	void setAppStateId(short appStateId);
	
	//Alarm, Car-Stolen, Steal-Wheels, Car-Evacuate
	//Состояние машины, null неопределено
	short getCarStateId();
	void setCarStateId(short carStateId);
	
	/**
	 * Возвращает последнее известное местоположение мобильного агента.
	 * @return null если не имеет смысла.
	 */
	ILocation getLastLocation();
	
	/**
	 * Устанавливает последнее известное местоположение мобильного агента.
	 * @param lastLocation null если не имеет смысла.
	 */
	void setLastLocation(ILocation lastLocation);

}
