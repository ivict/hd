/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

/**
 * Композиция интерфейсов. 
 * @author ivict
 */
public interface IMobileAgentWithState extends IMobileAgent, IMobileAgentState, IMobileAgentVersion { }
