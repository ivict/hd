/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;


/**
 * Интерфейс ограничений пользователя.<br/>
 * @author ivict
 *
 */
public interface IUserLimit {
	
	final static String TIME_LIMITED = "time-limited";
	final static String MAX_DEVICES = "max-devices";
	//If change this constant - update database defaults
	static final boolean DEFAULT_TIME_LIMITED = true;
	static final int DEFAULT_MAX_DEVICES = 5;

	/**
	 * Возвращает наличие ограничения по времени.
	 * @return true если пользователь не купил платную версию и не проходит по реферальной программе.
	 */
	boolean isTimeLimited();
	/**
	 * Устанавливает ограничение пользователя по времени.
	 * @param timeLimited, true если пользователь не купил платную версию и не проходит по реферальной программе.
	 */
	void setTimeLimited(boolean timeLimited);
	
	/**
	 * Возвращает наличие ограничения по количеству устройств.
	 * @return значение больше 0 или равное 0 //На данный момент 0 никак не интерпретируется.
	 */
	int getMaxDevices();

	/**
	 * Устанавливает ограничение пользователя по количеству подключенных устройств.
	 * @param  значение больше 0 или равное 0 //На данный момент 0 никак не интерпретируется.
	 */
	void setMaxDevices(int maxDevices);
	
}
