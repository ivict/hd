/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;

import java.util.Date;

/**
 * Геграфическая локация машины отмеченная в определенное время.
 * 
 * @author ivict
 */
public interface ICarLocation extends ILocation {

	/**
	 * Get timestamp.
	 * @return null if not defined
	 */
	Date getTime();
	
	/**
	 * Set timestamp.
	 * @param time null if not defined.
	 */
	void setTime(Date time);
	
	/**
	 * Get car state identity.
	 * @return 0 if not set.
	 */
	short getCarStateId();
	
	/**
	 * Set car state identity.
	 * @param carStateId 0 if not set.
	 */
	void setCarStateId(short carStateId);

	/**
	 * Get car velocity.
	 * @return 0 if not set.
	 */
	float getVelocity();
	
	/**
	 * Set car velocity.
	 * @param carStateId 0 if not set.
	 */
	void setVelocity(float velocity);

}
