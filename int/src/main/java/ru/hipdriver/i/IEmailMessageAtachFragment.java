/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i;


/**
 * Интерфейс фрагмента атача к письму. 
 * @author valentina
 *
 */
public interface IEmailMessageAtachFragment extends IEmailObject {
	
	int getMessageAtachIdentity();
	
	void setMessageAtachIdentity(int atachIdentity);
	
	int getFragmentNumber();
	
	void setFragmentNumber(int fragmentNumber);

	byte[] getContent();
	
	void setContent(byte[] content);

}
