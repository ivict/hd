package ru.hipdriver.android.app.maps;

import ru.hipdriver.android.util.TextTools;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public class GoogleMapTools {
	
	private static final String GOOGLE_MAP_FRAGMENT_CAMPOS_ZOOM = "google-map-fragment-campos-zoom";
	private static final String GOOGLE_MAP_FRAGMENT_CAMPOS_TILT = "google-map-fragment-campos-tilt";
	private static final String GOOGLE_MAP_FRAGMENT_CAMPOS_LON = "google-map-fragment-campos-lon";
	private static final String GOOGLE_MAP_FRAGMENT_CAMPOS_LAT = "google-map-fragment-campos-lat";
	private static final String GOOGLE_MAP_FRAGMENT_CAMPOS_BEARING = "google-map-fragment-campos-bearing";
	public static final String TAG = TextTools.makeTag(GoogleMapTools.class);
	public static final int FACTOR = 1000000;
    private static Boolean googleMapsAvailable;

    private GoogleMapTools() { }

	public static void safetySavePosition(Context context, CameraPosition camPos) {
		try {
			savePosition(context, camPos);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	private static void savePosition(Context context, CameraPosition camPos) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = preferences.edit();
		editor.putInt(GOOGLE_MAP_FRAGMENT_CAMPOS_BEARING, Math.round(camPos.bearing * FACTOR));
		editor.putInt(GOOGLE_MAP_FRAGMENT_CAMPOS_LAT, (int) Math.round(camPos.target.latitude * FACTOR));
		editor.putInt(GOOGLE_MAP_FRAGMENT_CAMPOS_LON, (int) Math.round(camPos.target.longitude * FACTOR));
		editor.putInt(GOOGLE_MAP_FRAGMENT_CAMPOS_TILT, Math.round(camPos.tilt * FACTOR));
		editor.putInt(GOOGLE_MAP_FRAGMENT_CAMPOS_ZOOM, Math.round(camPos.zoom));
		editor.commit();
	}
	
	public static CameraPosition restorePosition(Context context) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		float bearing = preferences.getInt(GOOGLE_MAP_FRAGMENT_CAMPOS_BEARING, 0) / (float) FACTOR;
		double latitude = preferences.getInt(GOOGLE_MAP_FRAGMENT_CAMPOS_LAT, 53251132) / (double) FACTOR;
		double longitude = preferences.getInt(GOOGLE_MAP_FRAGMENT_CAMPOS_LON, 50222687) / (double) FACTOR;
		float tilt = preferences.getInt(GOOGLE_MAP_FRAGMENT_CAMPOS_TILT, 0) / (float) FACTOR;
		float zoom = preferences.getInt(GOOGLE_MAP_FRAGMENT_CAMPOS_BEARING, 16) / (float) FACTOR;
		if (zoom <= 0f) {
			zoom = 16f;//Default value
		}
		return CameraPosition.fromLatLngZoom(new LatLng(latitude, longitude), zoom);
	}

    /**
     * Cahched version of checking google-maps availability.
     * @param context
     * @return true if service available on device.
     */
    public static boolean isGoogleMapsAvailable(Context context) {
        if (googleMapsAvailable != null) {
            return googleMapsAvailable;
        }
        synchronized (GoogleMapTools.class) {
            if (googleMapsAvailable != null) {
                return googleMapsAvailable;
            }
            googleMapsAvailable = checkGoogleMapsAvailability(context);
            return googleMapsAvailable;
        }
    }

    private static boolean checkGoogleMapsAvailability(Context context) {
        int code = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        switch (code) {
            case ConnectionResult.SERVICE_MISSING:
            case ConnectionResult.INTERNAL_ERROR:
            case ConnectionResult.SERVICE_INVALID:
            case ConnectionResult.DEVELOPER_ERROR:
            case ConnectionResult.LICENSE_CHECK_FAILED:
                return false;
            default:
                return true;
        }
    }

}
