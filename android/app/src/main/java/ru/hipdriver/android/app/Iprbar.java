package ru.hipdriver.android.app;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class Iprbar extends ProgressBar {

	private Drawable logo;
	private float initialWidth = 64;
	private float initialHeight = 64;

	public Iprbar(Context context) {
		super(context);
		//logo = context.getResources().getDrawable(R.drawable.lock);
		//setBackgroundDrawable(logo);
	}

	public Iprbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		//logo = context.getResources().getDrawable(R.drawable.lock);
		//setBackgroundDrawable(logo);
	}

	public Iprbar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		//logo = context.getResources().getDrawable(R.drawable.lock);
		//setBackgroundDrawable(logo);
	}
	
	public Drawable getLogo() {
		return logo;
	}

	public void setLogo(int resourceId, int initialWidth, int initialHeight) {
		this.initialHeight = initialHeight;
		this.initialWidth = initialWidth;
		logo = getContext().getResources().getDrawable(resourceId);
		
		this.setIndeterminate(false);
		this.setIndeterminateDrawable(null);
		
		this.setBackgroundDrawable(logo);
		this.invalidateDrawable(logo);
	}
	
	public void setIndeterminate(int resourceId) {
		this.initialHeight = 64;
		this.initialWidth = 64;
		Drawable indeterminateDrawable = getContext().getResources().getDrawable(resourceId);
		this.setIndeterminate(true);
		this.setIndeterminateDrawable(indeterminateDrawable);
	}	

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		int width = MeasureSpec.getSize(widthMeasureSpec);
//		int height = width * logo.getIntrinsicHeight()
//				/ logo.getIntrinsicWidth();
		
		int width = (int) Pixels.dipToPixels(getContext(), initialWidth);
		int height = (int) Pixels.dipToPixels(getContext(), initialHeight);
		
		
		setMeasuredDimension(width, height);
	}
}
