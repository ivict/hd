package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgentProfile;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import ru.hipdriver.android.app.R;

public class PowersavingsCarAlarmsSettingsFragment extends FaceSelectCollectionHipdriverFragment {

	private static final String TAG = TextTools.makeTag(PowersavingsCarAlarmsSettingsFragment.class);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.powersavings_car_alarms_settings_fragment,
				container, false);
		refresh(view);
		return view;
	}

	private void refresh(final View rootView) {
		if (rootView == null) {
			return;
		}
		FragmentActivity activity = getActivity();
		
//		//Setup behavior
//		Button button = (Button) findViewById(R.id.car_alarms_ext_settings_close_button);
//		button.setText(Html.fromHtml(getResources().getString(R.string.close)));
//		button.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick (View p1) {
//					//debugStatus("");
//					//Check settings changed by user
//					//boolean isChecked = getA().getBoolean(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT);
//	    			//if (isChecked && getA().getShowCount(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT) < 1) {
//	    			//	getA().showModalMessage(MainActivity.this, getResources().getString(R.string.disable_screen_on_sensor_silent_hint2));
//	    			//	getA().increaseShowCount(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT);
//	    			//}
//					//Check settings changed by user
//					boolean isChecked = getA().getBoolean(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE);
//	    			if (isChecked && !AirplaneMode.isStable() && getA().getShowCount(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE) < 1 && getA().getMemoryVersion() < getA().getMemoryVersion(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE)) {
//	    				showModalMessage(getResources().getString(R.string.energy_saving_mode_warn_hint2));
//	    				getA().increaseShowCount(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE);
//	    			}
//					
//					//close
//					//showCarAlarmsSettingsScreen();
//				}
//			
//		});
		
		final HintedCheckBox energySavingMode = (HintedCheckBox) rootView.findViewById(R.id.energy_saving_mode_checkbox);
		energySavingMode.setChecked(getA().getBoolean(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE));
		energySavingMode.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {//&& !AirplaneMode.isStable()) { 
					getA().makeToastWarning(R.string.energy_saving_mode_hint);
					
	    			if (isChecked && !AirplaneMode.isStable() && getA().getShowCount(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE) < 1 && getA().getMemoryVersion() < getA().getMemoryVersion(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE)) {
	    				getA().showMessage(R.string.energy_saving_mode_warn_hint2);
	    				getA().increaseShowCount(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE);
	    			}
					
				}
				
				getA().setBoolean(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE);
    			getA().clearShowCount(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE);    			
			}
		});
		
		HintedCheckBox sobmCheckbox = (HintedCheckBox) rootView.findViewById(R.id.switch_on_beacon_mode_checkbox);
		OnCheckedChangeListener sobmOnCheckedListener = new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				final ViewGroup viewGroup1 = (ViewGroup) rootView.findViewById(R.id.switch_on_beacon_mode_checkbox1);
				final ViewGroup viewGroup2 = (ViewGroup) rootView.findViewById(R.id.switch_on_beacon_mode_checkbox2);
				if (isChecked) {
//					final ScrollView scrollView = (ScrollView) findViewById(R.id.scroll_view);
//					final int scrollY0 = scrollView.getScrollY();
//					scrollView.post(new Runnable() {
//						@Override
//						public void run() {
//							view1.setVisibility(View.VISIBLE);
//							view2.setVisibility(View.VISIBLE);
//							view1.invalidate();
//							view2.invalidate();
//						}
//					});
//					scrollView.postDelayed(new Runnable() {
//						@Override
//						public void run() {
//							scrollView.fullScroll(ScrollView.FOCUS_DOWN);							
//							scrollView.fullScroll(ScrollView.FOCUS_UP);							
//							scrollView.scrollTo(0, scrollY0);
//						}
//					}, 100);
					enableAllChildsInViewGroup(viewGroup1);
					enableAllChildsInViewGroup(viewGroup2);
				} else {
					disableAllChildsInViewGroup(viewGroup1);
					disableAllChildsInViewGroup(viewGroup2);
				}
				getA().setBoolean(IMobileAgentProfile.SWITCH_ON_BEACON_MODE, isChecked);
			}
			
		};
		sobmCheckbox.setOnCheckedChangeListener(sobmOnCheckedListener);
		boolean isSobmCheckBoxChecked = getA().getBoolean(IMobileAgentProfile.SWITCH_ON_BEACON_MODE);
		sobmCheckbox.setChecked(isSobmCheckBoxChecked);
		sobmOnCheckedListener.onCheckedChanged(sobmCheckbox, isSobmCheckBoxChecked);
		
		EditText beaconPwrTresholdEditor = (EditText) rootView.findViewById(R.id.beacon_pwr_treshold_editor);
		beaconPwrTresholdEditor.setText(String.valueOf(getA().getFloat(IMobileAgentProfile.BEACON_PWR_TRESHOLD_PERCENTS)));
		beaconPwrTresholdEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.beacon_pwr_treshold_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		beaconPwrTresholdEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String beaconPwrTresholdText = s.toString();
    			float beaconPwrTreshold = TextTools.safetyGetFloat(beaconPwrTresholdText, IMobileAgentProfile.DEFAULT_BEACON_PWR_TRESHOLD_PERCENTS);
    			getA().setFloat(IMobileAgentProfile.BEACON_PWR_TRESHOLD_PERCENTS, beaconPwrTreshold);
    			getA().setMemoryVersion(IMobileAgentProfile.BEACON_PWR_TRESHOLD_PERCENTS);
			}
			
		});
		
		EditText beaconIntervalEditor = (EditText) rootView.findViewById(R.id.beacon_interval_editor);
		beaconIntervalEditor.setText(String.valueOf(getA().getInt(IMobileAgentProfile.BEACON_INTERVAL_IN_MINUTES)));
		beaconIntervalEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.beacon_interval_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		beaconIntervalEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String beaconIntervalText = s.toString();
    			int beaconInterval = TextTools.safetyGetInt(beaconIntervalText, IMobileAgentProfile.DEFAULT_BEACON_INTERVAL_IN_MINUTES);
    			getA().setInt(IMobileAgentProfile.BEACON_INTERVAL_IN_MINUTES, beaconInterval);
    			getA().setMemoryVersion(IMobileAgentProfile.BEACON_INTERVAL_IN_MINUTES);
			}
			
		});
		
		//Setup behaviour for conrol-mode switch
		new ControlModsEnumControl(getA(), rootView); 
		
	}
}
