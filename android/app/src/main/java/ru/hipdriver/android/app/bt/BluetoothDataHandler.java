package ru.hipdriver.android.app.bt;

import java.io.IOException;

import android.bluetooth.BluetoothDevice;

public interface BluetoothDataHandler {

	/**
	 * Handle of input data from bluetooth socket.
	 * @param device device which writed data.
	 * @param length of data which read from bluetooth socket.
	 * @param offset for data which read from bluetooth socket.
	 * @param data which read from bluetooth socket.
	 */
	void handle(BluetoothDevice device, int length, int offset, byte[] data);
	
	/**
	 * Connect establish handle
	 * @param device connected to device
	 */
	void connect(BluetoothDevice device);
	
	/**
	 * Data transmission error handle
	 * @param write/read error.
	 */
	void error(IOException e);
	
}
