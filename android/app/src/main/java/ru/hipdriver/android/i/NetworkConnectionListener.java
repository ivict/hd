package ru.hipdriver.android.i;

public interface NetworkConnectionListener {

	void changeConnectionState(boolean isConnectingOrConnected);
	
}
