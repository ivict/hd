package ru.hipdriver.android.app.sslucs;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import ru.hipdriver.android.i.RequestCallback;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.ISid;
import android.content.Context;
import android.util.Log;

abstract public class ChatConnectorWithClientRole extends ChatConnector<ChatConnectionWithClientRoleTask> {
	
	private static final String TAG = TextTools.makeTag(ChatConnectorWithClientRole.class);

	private AtomicReference<Long> requestIdRef;
	
	public ChatConnectorWithClientRole(Context context, long mobileAgentId, String chatServerHostName, int chatServerPort) {
		super(context, mobileAgentId, chatServerHostName, chatServerPort);
		this.requestIdRef = new AtomicReference<Long>();
	}

	protected Object waitForAnswer(long timeout, TimeUnit unit) {
		Long requestId = requestIdRef.get();
		if (requestId == null) {
			Log.w(TAG, "call-waiting-for-answer-from-not-same-thread-then-send-request-or-not-call-send-request-before-waiting-for-answer");
			return null;
		}
		try {
			if (requestId == ISid.INVALID_COMPONENT_SESSIONS_ID) {
				Log.w(TAG, "invalid-request-id");
				return null;
			}
			return chatConnectionTask.waitForAnswer(requestId);
		} catch (InterruptedException e) {
			return null;
		} finally {
			requestIdRef.set(null);
		}
	}

	/**
	 * Send request to remote mobile agent
	 * @param mobileAgentIdTo
	 * @param request
	 * @param returnValueTypes
	 * @param requestCallback
	 * @param timeout
	 * @param unit
	 * @param longTtl
	 * @param delayedPacketFactor
	 * @param key for long ttl requests
	 * @return sessions component identity.
	 */
	protected long sendRequest(long mobileAgentIdTo, Object request, Class<?>[] returnValueTypes, RequestCallback requestCallback, long timeout, TimeUnit unit, boolean longTtl, int delayedPacketFactor, Object key) {
		try {
			long requestId = chatConnectionTask.sendRequest(mobileAgentIdTo, request, returnValueTypes, requestCallback, timeout, unit, longTtl, 1, key);
			if (requestId == ISid.INVALID_COMPONENT_SESSIONS_ID) {
				Log.w(TAG, "bad-request[" + String.valueOf(request) + "]");
				return ISid.INVALID_COMPONENT_SESSIONS_ID;
			}
			requestIdRef.set(requestId);
			return requestId;
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return ISid.INVALID_COMPONENT_SESSIONS_ID;
		}
	}

	@Override
	protected ChatConnectionWithClientRoleTask createChatConnectionTask(InetAddress chatServerAddress, int chatServerPort) {
		return new ChatConnectionWithClientRoleTask(getContext(), getMobileAgentId(), chatServerAddress, chatServerPort);
	}
}
