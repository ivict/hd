package ru.hipdriver.android.app;

import java.util.Date;

import android.util.Log;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.android.util.UCExecutor;
import ru.hipdriver.g.Locations;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.i.support.UserCommandTypesEnum;
import ru.hipdriver.j.ErrorMessage;
import ru.hipdriver.j.Location;
import ru.hipdriver.j.StandardResponse;

public class CarAlarmsFaceExecutor implements UCExecutor {
	
	private static final String TAG = TextTools.makeTag(CarAlarmsFaceExecutor.class);
	private final HipdriverApplication hipdriverApplication;

	public CarAlarmsFaceExecutor(HipdriverApplication hipdriverApplication) {
		super();
		this.hipdriverApplication = hipdriverApplication;
	}

	@Override
	public StandardResponse exec(UserCommandTypesEnum commandType,
			Object... args) {
		IUserCommandsExecutor userCommandsExecutor = hipdriverApplication
				.getUserCommandsExecutor();
		switch (commandType) {
		case ACA:
			if (userCommandsExecutor != null) {
				hipdriverApplication.switchControlModeIfChanged();
				userCommandsExecutor.signin();
				//Check billing key
				if (hipdriverApplication.getDate(HipdriverApplication.EXPIRED_DATE).before(new Date())) {
					StandardResponse standardResponse = new StandardResponse() {};
					ErrorMessage errorMessage = new ErrorMessage();
					errorMessage.setId(-1);
					errorMessage.setDescription("Expired user key");						
					standardResponse.setErrorMessage(errorMessage);
					return standardResponse;
				}
				boolean switchOnBeaconMode = hipdriverApplication.getBoolean(IMobileAgentProfile.SWITCH_ON_BEACON_MODE);
				if (switchOnBeaconMode) {
					userCommandsExecutor.switchOnBeaconMode();
				} else {
					userCommandsExecutor.switchOnWatch();
				}
			}
			break;
		case SOCA:
			if (userCommandsExecutor != null) {
				hipdriverApplication.switchControlModeIfChanged();
				Log.d(TAG, "switch-off-watch[chat-command-exec]");				
				userCommandsExecutor.switchOffWatch();
			}
			break;
		case WAY:
			// TODO: eventually initialize GPS
			short carStateId = DBLinks.link(hipdriverApplication.getCarState());	
			WatchdogService watchdogService = hipdriverApplication.getWatchdogService();
			if (watchdogService == null) {
				return null;
			}
			Location location = Locations.makeLocation(watchdogService.getLat(),
					watchdogService.getLon(), watchdogService.getAlt(),
					watchdogService.getAcc(), watchdogService.getMcc(),
					watchdogService.getMnc(), watchdogService.getLac(),
					watchdogService.getCid(), watchdogService.time, carStateId,
					watchdogService.vel);
			return (StandardResponse) location;
		}
		return null;
	}

};
