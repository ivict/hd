package ru.hipdriver.android.app.help;

import ru.hipdriver.android.app.HipdriverFragment;
import ru.hipdriver.android.app.R;
import ru.hipdriver.android.util.TextTools;
import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

public abstract class ShortHelpHipdriverFragment extends HipdriverFragment {
	
	private static final String TAG = TextTools.makeTag(ShortHelpHipdriverFragment.class);

	protected ShortHelpCollectionPagerAdapter getShortHelpCollectionPagerAdapter() {
		Activity activity = getActivity();
		if (activity == null) {
			Log.w(TAG, "get-short-help-collection-pager-adapter-impossible[activity-is-null]");
			return null;
		}
		ViewPager pager = (ViewPager) activity.findViewById(R.id.short_help_pager);
		if (pager == null) {
			Log.w(TAG, "get-short-help-collection-pager-adapter-impossible[pager-not-found]");
			return null;
		}
		PagerAdapter adapter = pager.getAdapter();
		if (!(adapter instanceof ShortHelpCollectionPagerAdapter)) {
			Log.w(TAG, "get-short-help-collection-pager-adapter-impossible[adapter-not-cast]");
			return null;
		}
		return (ShortHelpCollectionPagerAdapter) adapter; 		
	}
	
	
}
