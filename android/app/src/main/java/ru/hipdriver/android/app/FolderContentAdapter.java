package ru.hipdriver.android.app;

import android.app.*;
import android.content.*;
import android.hardware.*;
import android.view.*;
import android.widget.*;
import java.io.*;
import java.util.*;
import ru.hipdriver.android.app.R;

public class FolderContentAdapter extends ArrayAdapter<File> {

	private int layoutResourceId;
	private FileFilter fileFilter;

	private boolean fullPathInclude = true;
	
	/**
	 * Sort by name but directory above then file.
	 */
	private class SortByName implements Comparator<File> {

		public int compare(File file1, File file2) {
			if ( file1.isDirectory() && !file2.isDirectory() ) {
				return -1;
			}
			if ( !file1.isDirectory() && file2.isDirectory() ) {
				return 1;
			}
			return file1.getName().compareTo(file2.getName());
		}
		
	}
	
	public FolderContentAdapter(Context context, int layoutResourceId) {
		super(context, layoutResourceId, new ArrayList<File>());
		this.layoutResourceId = layoutResourceId;
	}
	
	public void setFilter(FileFilter filter) {
		this.fileFilter = filter;
	}
	
	public synchronized void setFolder(File folder) {
		clear();
		final File[] files;
		if ( fileFilter == null ) {
			files = folder.listFiles();
		} else {
			files = folder.listFiles(fileFilter);
		}
		if ( files != null ) {
			Arrays.sort(files, new SortByName());
			for ( File file : files ) {
				add(file);
			}
		}
		if ( !fullPathInclude ) {
			insert(new File(folder, ".."), 0);
		}
	}
	
	public void setFullPathInclude(boolean fullPathInclude) {
		this.fullPathInclude = fullPathInclude;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		final File file;

		if (row == null) {
			LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
		}
		file = getItem(position);

		TextView tv = (TextView) row.findViewById(R.id.file_name);
		if ( fullPathInclude ) {
			tv.setText(file.toString());
			row.invalidate();
			return row;
		}
		ImageView iv = (ImageView) row.findViewById(R.id.file_type);
		iv.setVisibility(View.VISIBLE);
		if ( file.isDirectory() ) {
			iv.setImageResource(R.drawable.ic_folder);
		} else if ( isKnownContent(file) ) {
			iv.setImageResource(R.drawable.ic_known_doc);
		} else {
			iv.setImageResource(R.drawable.ic_unknown_file);
		}
		tv.setText(file.getName());
		row.invalidate();
		
		return row;
	}

	protected boolean isKnownContent(File file) {
		return file.getName().endsWith(".json");
	}

}