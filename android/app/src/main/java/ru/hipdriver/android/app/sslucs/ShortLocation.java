package ru.hipdriver.android.app.sslucs;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.j.Location;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortLocation {

	@JsonProperty(value = "lat")
	public int lat;

	@JsonProperty(value = "lon")
	public int lon;
	
	@JsonProperty(value = "alt")
	public int alt = Integer.MIN_VALUE;
	
	@JsonProperty(value = "acc")
	public int acc;
	
	//Mobile Country Code
	@JsonProperty(value = "mcc")
	public int mcc;
	
	//Mobile Network Code
	@JsonProperty(value = "mnc")
	public int mnc;
	
	//Location Area Code
	@JsonProperty(value = "lac")
	public int lac = -1;
	
	//Cell Id
	@JsonProperty(value = "cid")
	public int cid = -1;
	
	//Timestamp
	@JsonProperty(value = "time")
	public Date time;
	
	//Car state id
	@JsonProperty(value = "car-state-id")
	public short carStateId;

	public int getLat() {
		return lat;
	}

	public void setLat(int lat) {
		this.lat = lat;
	}

	public int getLon() {
		return lon;
	}

	public void setLon(int lon) {
		this.lon = lon;
	}

	public int getAlt() {
		return alt;
	}

	public void setAlt(int alt) {
		this.alt = alt;
	}

	public int getAcc() {
		return acc;
	}

	public void setAcc(int acc) {
		this.acc = acc;
	}

	public int getMcc() {
		return mcc;
	}

	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	public int getMnc() {
		return mnc;
	}

	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	public int getLac() {
		return lac;
	}

	public void setLac(int lac) {
		this.lac = lac;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public short getCarStateId() {
		return carStateId;
	}

	public void setCarStateId(short carStateId) {
		this.carStateId = carStateId;
	}
	
}
