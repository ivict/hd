package ru.hipdriver.android.app.sslucs;

import ru.hipdriver.i.support.UserCommandTypesEnum;

public interface UserCommandExecutor {

	Object exec(UserCommandTypesEnum userCommandType, byte[] args, long requestId, long remoteControlMobileAgentId);
	
}
