package ru.hipdriver.android.app;

import java.io.UnsupportedEncodingException;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import ru.hipdriver.i.ISid;

public class HttpRequestFactory {

	private HttpRequestFactory() {
		
	}
	
	public static HttpPost newPost(String url, int connectionTimeoutInSecs, int socketTimeoutInSecs) {
		HttpPost postRequest = new HttpPost(url);
		setTimeouts(postRequest, connectionTimeoutInSecs * 1000, socketTimeoutInSecs * 1000);
		return postRequest;
	}
	
	public static HttpPost newPost(String host, String service, int connectionTimeoutInSecs, int socketTimeoutInSecs) {
		HttpPost postRequest = new HttpPost("http://" + host + service);
		setTimeouts(postRequest, connectionTimeoutInSecs * 1000, socketTimeoutInSecs * 1000);
		return postRequest;
	}
	
	public static HttpPost newJsonPost(String host, String service, int connectionTimeoutInSecs, int socketTimeoutInSecs, ISid sid, String postMessage) throws UnsupportedEncodingException {
		HttpPost postRequest = newPost(host, service, connectionTimeoutInSecs, socketTimeoutInSecs);
		postRequest.setEntity(new ByteArrayEntity(postMessage.toString().getBytes(
				"UTF8")));
		postRequest.addHeader("Accept", "application/json");
		postRequest.addHeader("Content-type", "application/json");
		if (sid != null) {
			postRequest.addHeader("a", String.valueOf(sid.getA()));
		}
		return postRequest;
	}
	
	public static HttpGet newGet(String host, String service, int connectionTimeoutInSecs, int socketTimeoutInSecs) {
		return newGet("http://" + host + service, connectionTimeoutInSecs, socketTimeoutInSecs);
	}
	
	public static HttpGet newGet(String url, int connectionTimeoutInSecs, int socketTimeoutInSecs) {
		HttpGet getRequest = new HttpGet(url);
		setTimeouts(getRequest, connectionTimeoutInSecs * 1000, socketTimeoutInSecs * 1000);
		return getRequest;
	}
	
	protected static void setTimeouts(HttpRequestBase request, int timeoutConnectionInMillis, int timeoutSocketInMillis) {
		HttpParams requestParams = request.getParams();
		// Set the timeout in milliseconds until a connection is established.
		// The default value is zero, that means the timeout is not used. 
		HttpConnectionParams.setConnectionTimeout(requestParams, (int) timeoutConnectionInMillis);
		// Set the default socket timeout (SO_TIMEOUT) 
		// in milliseconds which is the timeout for waiting for data.
		HttpConnectionParams.setSoTimeout(requestParams, (int) timeoutSocketInMillis);
	}

	public static HttpPut newPut(String host, String service, int connectionTimeoutInSecs, int socketTimeoutInSecs) {
		HttpPut putRequest = new HttpPut("http://" + host + service);
		setTimeouts(putRequest, connectionTimeoutInSecs * 1000, socketTimeoutInSecs * 1000);
		return putRequest;
	}
	
}
