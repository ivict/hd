package ru.hipdriver.android.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class MobileAgentNameEditText extends EditText {

	public MobileAgentNameEditText(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	public MobileAgentNameEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MobileAgentNameEditText(Context context) {
		super(context);
	}
	
	//@see http://stackoverflow.com/questions/6070805/prevent-enter-key-on-edittext-but-still-show-the-text-as-multi-line
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode==KeyEvent.KEYCODE_ENTER) 
        {
            // Just ignore the [Enter] key
            return true;
        }
        // Handle all other keys in the default way
        return super.onKeyDown(keyCode, event);
    }	

}
