package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.support.AppFacesEnum;
import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

public class AppFaceSelectionFragment extends FaceSelectCollectionHipdriverFragment {

	private static final String TAG = TextTools.makeTag(AppFaceSelectionFragment.class);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.app_face_selection_fragment,
				container, false);
		refresh(view);
		return view;
	}

	private void refresh(final View rootView) {
		if (rootView == null) {
			return;
		}
		final RadioButton carAlarmsButton = uiComponent(rootView, R.id.app_face_selection_fragment_$select_car_alarms$_button);
		carAlarmsButton.setText(Html.fromHtml(getResources().getString(
				R.string.select_car_alarms_button2)));
		final RadioButton remoteControlButton = uiComponent(rootView, R.id.select_remote_control_button);
		remoteControlButton.setText(Html.fromHtml(getResources().getString(
				R.string.select_remote_control_button2)));
        final ImageButton additionalRefForRemoteControlSettings = uiComponent(rootView, R.id.app_face_selection_fragment$remote_control_settings$_button);
        final ImageButton additionalRefForCarAlarmsSettings = uiComponent(rootView, R.id.app_face_selection_fragment$car_alarms_settings$_button);
		final RelativeLayout carAlarmsLayout = uiComponent(rootView, R.id.app_face_selection_fragment$car_alarms$_relative_layout);
		final RelativeLayout remoteControlLayout = uiComponent(rootView, R.id.app_face_selection_fragment$remote_control$_relative_layout);

		OnTouchListener frameSelection = new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                	selectionView(carAlarmsLayout, remoteControlLayout, v);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                	deselectionView(carAlarmsLayout, remoteControlLayout, v);
                    break;
                	case MotionEvent.ACTION_MOVE:
                		break;
                }
                return false;
			}
		};
		carAlarmsLayout.setOnTouchListener(frameSelection);
		remoteControlLayout.setOnTouchListener(frameSelection);
		remoteControlButton.setOnTouchListener(frameSelection);
		carAlarmsButton.setOnTouchListener(frameSelection);
        OnClickListener onClickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                //OnClick:
                AppFacesEnum prevAppFace = getA().getAppFace();
                if (!chooseView(rootView, v, prevAppFace)) {
                    showSettings();
                    return;
                }
                changeScreenOrientationSupport();
                FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = getFaceSelectCollectionPagerAdapter();
                if (faceSelectCollectionPagerAdapter != null) {
                    faceSelectCollectionPagerAdapter.onChangeAppFace(getA().getAppFace());
                }

            }
        };
        carAlarmsLayout.setOnClickListener(onClickListener);
        remoteControlLayout.setOnClickListener(onClickListener);
        remoteControlButton.setOnClickListener(onClickListener);
        carAlarmsButton.setOnClickListener(onClickListener);
        OnClickListener showSettingsClick = new OnClickListener() {
            @Override
            public void onClick(View v) {
                showSettings();
            }
        };
        additionalRefForCarAlarmsSettings.setOnClickListener(showSettingsClick);
        additionalRefForRemoteControlSettings.setOnClickListener(showSettingsClick);
		// Toggle current mode
		switch (getA().getAppFace()) {
		case CAR_ALARMS:
			onChangedToCarAlarmsFace(rootView);
			break;
		case REMOTE_CONTROL:
			onChangedToRemoteControlFace(rootView);
			break;
		}
		
		final Button showAppFaceButton = (Button) rootView
				.findViewById(R.id.show_app_face_button);
		showAppFaceButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Activity activity = getActivity();
				if (activity == null) {
					Log.w(TAG, "can-not-show-app-face[activity is null]");
					return;
				}
				if (!(activity instanceof MainActivity)) {
					Log.w(TAG, "can-not-show-app-face[activity not cast to main-activity]");
					return;
				}
				MainActivity mainActivity = (MainActivity) activity;
				final ImageButton faceSelectButton = (ImageButton) mainActivity.findViewById(R.id.face_select_button);
				mainActivity.fireClickTopRightButton(faceSelectButton);
			}
			
		});
	}

    private void showSettings() {
        FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = getFaceSelectCollectionPagerAdapter();
        //Show settings page if double click
        if (faceSelectCollectionPagerAdapter != null) {// && prevAppFace == currentAppFace) {
            faceSelectCollectionPagerAdapter.show(1);
        }
    }

    private void deselectionView(View carAlarmsLayout,
			View remoteControlLayout, View view) {
		// Check which radio button was clicked
		switch (view.getId()) {
			case R.id.app_face_selection_fragment_$select_car_alarms$_button:
			case R.id.app_face_selection_fragment$car_alarms$_relative_layout:
				carAlarmsLayout.setSelected(false);
				break;
			case R.id.select_remote_control_button:
			case R.id.app_face_selection_fragment$remote_control$_relative_layout:
				remoteControlLayout.setSelected(false);
				break;
		}
	}

	private void selectionView(View carAlarmsLayout,
			View remoteControlLayout, View view) {
		// Check which radio button was clicked
		switch (view.getId()) {
			case R.id.app_face_selection_fragment_$select_car_alarms$_button:
			case R.id.app_face_selection_fragment$car_alarms$_relative_layout:
				carAlarmsLayout.setSelected(true);
				break;
			case R.id.select_remote_control_button:
			case R.id.app_face_selection_fragment$remote_control$_relative_layout:
				remoteControlLayout.setSelected(true);
				break;
		}
	}

	protected boolean chooseView(final View rootView, View view, AppFacesEnum appFace) {
		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.app_face_selection_fragment_$select_car_alarms$_button:
		case R.id.app_face_selection_fragment$car_alarms$_relative_layout:
			// resetFocus();
            if (appFace == AppFacesEnum.CAR_ALARMS) {
                return false;
            }
			getA().setAppFace(AppFacesEnum.CAR_ALARMS);

            onChangedToCarAlarmsFace(rootView);

			return true;
		case R.id.select_remote_control_button:
		case R.id.app_face_selection_fragment$remote_control$_relative_layout:
			// resetFocus();
            if (appFace == AppFacesEnum.REMOTE_CONTROL) {
                return false;
            }

			getA().setAppFace(AppFacesEnum.REMOTE_CONTROL);

            onChangedToRemoteControlFace(rootView);

			return true;
		}
        return false;
	}

    private void onChangedToRemoteControlFace(View rootView) {
        final RadioButton carAlarmsButton = uiComponent(rootView, R.id.app_face_selection_fragment_$select_car_alarms$_button);
        final RadioButton remoteControlButton = uiComponent(rootView, R.id.select_remote_control_button);
        final ImageButton additionalRefForRemoteControlSettings = uiComponent(rootView, R.id.app_face_selection_fragment$remote_control_settings$_button);
        final ImageButton additionalRefForCarAlarmsSettings = uiComponent(rootView, R.id.app_face_selection_fragment$car_alarms_settings$_button);
        additionalRefForRemoteControlSettings.setVisibility(View.VISIBLE);
        additionalRefForCarAlarmsSettings.setVisibility(View.GONE);

        carAlarmsButton.setChecked(false);
        remoteControlButton.toggle();
    }

    private void onChangedToCarAlarmsFace(View rootView) {
        final RadioButton carAlarmsButton = uiComponent(rootView, R.id.app_face_selection_fragment_$select_car_alarms$_button);
        final RadioButton remoteControlButton = uiComponent(rootView, R.id.select_remote_control_button);
        final ImageButton additionalRefForRemoteControlSettings = uiComponent(rootView, R.id.app_face_selection_fragment$remote_control_settings$_button);
        final ImageButton additionalRefForCarAlarmsSettings = uiComponent(rootView, R.id.app_face_selection_fragment$car_alarms_settings$_button);

        additionalRefForRemoteControlSettings.setVisibility(View.GONE);
        additionalRefForCarAlarmsSettings.setVisibility(View.VISIBLE);

        remoteControlButton.setChecked(false);
        carAlarmsButton.toggle();
    }

    private void changeScreenOrientationSupport() {
        Activity activity = getActivity();
        if (activity == null) {
            Log.w(TAG, "can-not-show-app-face[activity is null]");
            return;
        }
        if (!(activity instanceof MainActivity)) {
            Log.w(TAG, "can-not-show-app-face[activity not cast to main-activity]");
            return;
        }
        MainActivity mainActivity = (MainActivity) activity;
        mainActivity.changeScreenOrientation();
    }

}
