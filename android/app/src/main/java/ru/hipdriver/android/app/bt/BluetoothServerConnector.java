package ru.hipdriver.android.app.bt;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicReference;

import ru.hipdriver.android.util.TextTools;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class BluetoothServerConnector implements BluetoothConnection {

	public static final String TAG = TextTools.makeTag(BluetoothServerConnector.class);
	private BluetoothAdapter bluetoothAdapter;
	
	private final String name;
	private final UUID uuid;
	
	private BluetoothDataHandler dataHandler;
	
	private final Map<BluetoothDevice, BluetoothPipe> handlers = new WeakHashMap<BluetoothDevice, BluetoothPipe>(1);
	
	private class AcceptThread extends Thread {
	    private final BluetoothServerSocket serverSocket;
	 
	    public AcceptThread() {
	        // Use a temporary object that is later assigned to mmServerSocket,
	        // because mmServerSocket is final
	        BluetoothServerSocket tmp = null;
	        try {
	            // uuid is the app's UUID string, also used by the client code
	            tmp = bluetoothAdapter.listenUsingRfcommWithServiceRecord(name, uuid);
	        } catch (IOException e) {
	        	Log.wtf(TAG, e);
	        }
	        serverSocket = tmp;
	    }
	 
	    public void run() {
	        setName("bt-accept-thread[" + name + "]");
	        BluetoothSocket socket = null;
	        // Keep listening until exception occurs or a socket is returned
	        while (!Thread.interrupted()) {
	            try {
	                socket = serverSocket.accept();
	            } catch (IOException e) {
	            	Log.wtf(TAG, e);
	                break;
	            }
	            // If a connection was accepted
	            if (socket != null) {
	                // Do work to manage the connection (in a separate thread)
	                handleSocket(socket);
	                try {
						serverSocket.close();
					} catch (IOException e) {
						Log.wtf(TAG, e);
					}
	                break;
	            }
	        }
	    }
	    
	    /** Will cancel the listening socket, and cause the thread to finish */
	    public void cancel() {
	        try {
	            serverSocket.close();
	        } catch (IOException e) {
	        	Log.wtf(TAG, e);
	        }
	    }
	}	
    private AtomicReference<AcceptThread> acceptThreadRef = new AtomicReference<AcceptThread>();
	
	public BluetoothServerConnector(String name, UUID uuid) {
		this.name = name;
		this.uuid = uuid;
	}
	
	protected void handleSocket(BluetoothSocket socket) {
		if (dataHandler == null) {
			return;
		}
		BluetoothDevice remoteDevice = socket.getRemoteDevice();
		BluetoothPipe prevBluetoothPipe = handlers.get(remoteDevice);
		if (prevBluetoothPipe != null) {
			prevBluetoothPipe.cancel();
			prevBluetoothPipe.interrupt();
		}
		BluetoothPipe newBluetoothPipe = new BluetoothPipe(socket, dataHandler);
		handlers.put(remoteDevice, newBluetoothPipe);
		newBluetoothPipe.start();
		Log.d(TAG, "handling-server-connection-for-device[" + remoteDevice + "]");
		dataHandler.connect(remoteDevice);
	}

	@Override
	public boolean open(BluetoothDataHandler dataHandler) {
		this.dataHandler = dataHandler;
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (bluetoothAdapter == null) {
			return false;
		}
		if (!bluetoothAdapter.isEnabled()) {
			return false;
		}
		AcceptThread acceptThread = new AcceptThread();
		AcceptThread prevAcceptThread = acceptThreadRef.getAndSet(acceptThread);
		if (prevAcceptThread != null) {
			prevAcceptThread.cancel();
			prevAcceptThread.interrupt();
		}
		acceptThread.start();
		Thread.yield();
		return true;
	}

	@Override
	public void close() {
		AcceptThread prevAcceptThread = acceptThreadRef.getAndSet(null);
		if (prevAcceptThread != null) {
			prevAcceptThread.cancel();
			prevAcceptThread.interrupt();
		}
		for (Map.Entry<BluetoothDevice, BluetoothPipe> e : handlers.entrySet()) {
			BluetoothPipe bluetoothPipe = e.getValue();
			bluetoothPipe.cancel();
			bluetoothPipe.interrupt();
		}
	}
	
	public BluetoothPipe getPipe(BluetoothDevice device) {
		return handlers.get(device);
	}
}
