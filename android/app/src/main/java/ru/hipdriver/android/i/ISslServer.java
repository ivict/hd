package ru.hipdriver.android.i;


public interface ISslServer {
	
	void shutdown();
	
	void sendResponse(long mobileAgentIdTo, long requestId, Object object);

	void reconnect();	
	
}
