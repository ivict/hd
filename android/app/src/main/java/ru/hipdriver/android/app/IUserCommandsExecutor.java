package ru.hipdriver.android.app;

public interface IUserCommandsExecutor {
	
	Object switchOffWatch();
	Object switchOnWatch();
	//TODO: refactoring: move to another interface
	void safetySetTextOutput(int textViewResourceId, CharSequence text, boolean append, boolean bold, boolean italic);
	//Connect to server for get settings and receive changed settings
	void signin();
	void switchOnBeaconMode();
	
}
