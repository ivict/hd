package ru.hipdriver.android.app;

public class Invariants {

	private Invariants() { }
	
	public static final String TAG_GEO = "ru.hipdriver:tag_geo";
	
	//Maximum size of data payload for success sending through mobile network 
	public static final int DATA_CHUNK_SIZE = 159;//158;
	
}
