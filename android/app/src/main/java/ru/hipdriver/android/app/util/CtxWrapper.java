package ru.hipdriver.android.app.util;

import android.content.Context;
import android.content.pm.PackageManager;

public class CtxWrapper {
	
	private final Context context;
	
	private CtxWrapper(Context context) {
		this.context = context;
	}
	
	public static CtxWrapper make(Context context) {
		return new CtxWrapper(context);
	}
	
	//@see http://stackoverflow.com/questions/3922606/detect-an-application-is-installed-or-not
	public boolean isAppInstalled(String packageName) {
	    PackageManager pm = context.getPackageManager();
	    boolean installed = false;
	    try {
	       pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
	       installed = true;
	    } catch (PackageManager.NameNotFoundException e) {
	       installed = false;
	    }
	    return installed;
	}

	public Context getContext() {
		return context;
	}

}
