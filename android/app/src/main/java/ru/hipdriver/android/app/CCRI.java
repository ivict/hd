package ru.hipdriver.android.app;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Cross-compiling resources identities<br>
 * Encoding/Decoding tables.
 * @author ivict
 *
 */
public class CCRI {

	private CCRI() { }
	
	private static Map<Integer, Integer> INTERNAL = new HashMap<Integer, Integer>();
	private static Map<Integer, Integer> INTERNAL_REVERSE = new HashMap<Integer, Integer>();
	//Read only map 
	protected static Map<Integer, Integer> RESOURCES = Collections.unmodifiableMap(INTERNAL);
	//Read only map 
	protected static Map<Integer, Integer> INDEXES = Collections.unmodifiableMap(INTERNAL_REVERSE);
	
	private static int MARK_OF_STRING_RESOURCE = 0x02000000; 
	
	static {
		//TODO: use reflection for automation or parse additional attributes in xml (like msgid).
		makeStringItem(R.string.accelerometer_hovered, 0x1);
		makeStringItem(R.string.activity_hipdriver_ru, 0x2);
		makeStringItem(R.string.alert, 0x3);
		makeStringItem(R.string.alert_calls_count_hint, 0x4);
		makeStringItem(R.string.alert_calls_count_text, 0x5);
		makeStringItem(R.string.alert_calls_duration_text, 0x6);
		makeStringItem(R.string.alert_calls_interval_duration_hint, 0x7);
		makeStringItem(R.string.alert_calls_interval_editor_hint, 0x8);
		makeStringItem(R.string.alert_calls_interval_text, 0x9);
		makeStringItem(R.string.alert_processing, 0xA);
		makeStringItem(R.string.application_version_text, 0xB);
		makeStringItem(R.string.asd_acceleration_default_value, 0xC);
		makeStringItem(R.string.asd_acceleration_hint, 0xD);
		makeStringItem(R.string.asd_acceleration_value_hint, 0xE);
		makeStringItem(R.string.asd_angle_default_value, 0xF);
		makeStringItem(R.string.asd_angle_hint, 0x10);
		makeStringItem(R.string.asd_angle_value_hint, 0x11);
		//makeStringItem(R.string.auth_client_needs_enabling_title, 0x12);
		//makeStringItem(R.string.auth_client_needs_installation_title, 0x13);
		//makeStringItem(R.string.auth_client_needs_update_title, 0x14);
		//makeStringItem(R.string.auth_client_play_services_err_notification_msg, 0x15);
		//makeStringItem(R.string.auth_client_requested_by_msg, 0x16);
		//makeStringItem(R.string.auth_client_using_bad_version_title, 0x17);
		makeStringItem(R.string.authorization, 0x18);
		makeStringItem(R.string.auto_beacon_mode_if_power_low_hint, 0x19);
		makeStringItem(R.string.auto_beacon_mode_if_power_low_percent_text, 0x1A);
		makeStringItem(R.string.auto_beacon_mode_if_power_low_text, 0x1B);
		makeStringItem(R.string.bad_email, 0x1C);
		makeStringItem(R.string.bad_phone_number, 0x1D);
		makeStringItem(R.string.beacon_interval_editor_hint, 0x1E);
		makeStringItem(R.string.beacon_interval_hours_text, 0x1F);
		makeStringItem(R.string.beacon_interval_hours_value_hint, 0x20);
		makeStringItem(R.string.beacon_interval_minutes_text, 0x21);
		makeStringItem(R.string.beacon_interval_minutes_value_hint, 0x22);
		makeStringItem(R.string.beacon_interval_settings_text, 0x23);
		makeStringItem(R.string.beacon_interval_text, 0x24);
		makeStringItem(R.string.beacon_mode, 0x25);
		makeStringItem(R.string.beacon_parameters, 0x26);
		makeStringItem(R.string.beacon_pwr_treshold_editor_hint, 0x27);
		makeStringItem(R.string.beacon_settings_text, 0x28);
		makeStringItem(R.string.beacon_treshold_hint, 0x29);
		makeStringItem(R.string.beacon_treshold_text, 0x2A);
		makeStringItem(R.string.can_not_enabled_energy_saving_mode_hint, 0x2B);
		makeStringItem(R.string.car_alarms_additional_features, 0x2D);
		makeStringItem(R.string.car_alarms_ext_settings_text, 0x2E);
		makeStringItem(R.string.car_alarms_settings_affecting_the_balance_of_payments_text, 0x2F);
		makeStringItem(R.string.car_alarms_settings_text, 0x30);
		makeStringItem(R.string.car_alarms_switched_off, 0x31);
		makeStringItem(R.string.car_auto_alarms_after_car_poke, 0x32);
		makeStringItem(R.string.car_auto_alarms_after_steal_wheels, 0x33);
		makeStringItem(R.string.car_auto_alarms_parameters, 0x34);
		makeStringItem(R.string.car_evacuate, 0x35);
		makeStringItem(R.string.car_jam, 0x36);
		makeStringItem(R.string.car_poke, 0x37);
		makeStringItem(R.string.car_stolen, 0x38);
		makeStringItem(R.string.close, 0x39);
		makeStringItem(R.string.close_app_button_text, 0x3A);
		//makeStringItem(R.string.common_google_play_services_enable_button, 0x3B);
		//makeStringItem(R.string.common_google_play_services_enable_text, 0x3C);
		//makeStringItem(R.string.common_google_play_services_enable_title, 0x3D);
		//makeStringItem(R.string.common_google_play_services_install_button, 0x3E);
		//makeStringItem(R.string.common_google_play_services_install_text_phone, 0x3F);
		//makeStringItem(R.string.common_google_play_services_install_text_tablet, 0x40);
		//makeStringItem(R.string.common_google_play_services_install_title, 0x41);
		//makeStringItem(R.string.common_google_play_services_invalid_account_text, 0x42);
		//makeStringItem(R.string.common_google_play_services_invalid_account_title, 0x43);
		//makeStringItem(R.string.common_google_play_services_network_error_text, 0x44);
		//makeStringItem(R.string.common_google_play_services_network_error_title, 0x45);
		//makeStringItem(R.string.common_google_play_services_unknown_issue, 0x46);
		//makeStringItem(R.string.common_google_play_services_unsupported_date_text, 0x47);
		//makeStringItem(R.string.common_google_play_services_unsupported_text, 0x48);
		//makeStringItem(R.string.common_google_play_services_unsupported_title, 0x49);
		//makeStringItem(R.string.common_google_play_services_update_button, 0x4A);
		//makeStringItem(R.string.common_google_play_services_update_text, 0x4B);
		//makeStringItem(R.string.common_google_play_services_update_title, 0x4C);
		//makeStringItem(R.string.common_signin_button_text, 0x4D);
		//makeStringItem(R.string.common_signin_button_text_long, 0x4E);
		makeStringItem(R.string.connection_error, 0x4F);
		makeStringItem(R.string.control_mode_choice, 0x50);
		makeStringItem(R.string.count_of_photos, 0x51);
		makeStringItem(R.string.count_of_photos_editor_hint, 0x52);
		makeStringItem(R.string.debug_make_network_connection, 0x53);
		makeStringItem(R.string.debug_settings_dialog_negative_button, 0x54);
		makeStringItem(R.string.debug_settings_dialog_positive_button, 0x55);
		makeStringItem(R.string.debug_settings_dialog_title, 0x56);
		makeStringItem(R.string.demo_signup, 0x57);
		makeStringItem(R.string.show_phone_editor_$title$_text_view, 0x58);
		makeStringItem(R.string.disable_screen_on_sensor_silent, 0x59);
		makeStringItem(R.string.disable_screen_on_sensor_silent_hint1, 0x5A);
		makeStringItem(R.string.disable_screen_on_sensor_silent_hint2, 0x5B);
		makeStringItem(R.string.email, 0x5C);
		makeStringItem(R.string.empty_phone_number_text, 0x5D);
		makeStringItem(R.string.energy_saving_mode, 0x5E);
		makeStringItem(R.string.energy_saving_mode_hint, 0x5F);
		makeStringItem(R.string.energy_saving_mode_warn_hint1, 0x60);
		makeStringItem(R.string.energy_saving_mode_warn_hint2, 0x61);
		makeStringItem(R.string.energy_saving_parameters, 0x62);
		makeStringItem(R.string.expect_call, 0x63);
		makeStringItem(R.string.expired_user_key, 0x64);
		makeStringItem(R.string.function_not_available_in_phone_calls_mode, 0x65);
		makeStringItem(R.string.ga_trackingId, 0x66);
		makeStringItem(R.string.google_maps, 0x67);
		makeStringItem(R.string.horizontal_line, 0x69);
		makeStringItem(R.string.ic_make_photo_text, 0x6A);
		makeStringItem(R.string.ic_remote_listen_text, 0x6B);
		makeStringItem(R.string.internet_control_mode, 0x6C);
		makeStringItem(R.string.interval_between_photos, 0x6D);
		makeStringItem(R.string.interval_between_photos_in_seconds_editor_hint, 0x6E);
		makeStringItem(R.string.lighthouse_settings_close, 0x6F);
		makeStringItem(R.string.linking, 0x70);
		//makeStringItem(R.string.location_client_powered_by_google, 0x71);
		makeStringItem(R.string.login_or_email, 0x72);
		makeStringItem(R.string.geo_provider_choice, 0x73);
		makeStringItem(R.string.max_sensitivity, 0x74);
		makeStringItem(R.string.maxacc_hint, 0x75);
		makeStringItem(R.string.min_sensitivity, 0x76);
		makeStringItem(R.string.minutes, 0x77);
		makeStringItem(R.string.mobile_agent_choice, 0x78);
		makeStringItem(R.string.mobile_agent_name, 0x79);
		makeStringItem(R.string.modal_alert_positive_button, 0x7A);
		makeStringItem(R.string.modal_alert_title, 0x7B);
		makeStringItem(R.string.no_cell_network_available_but_we_try_to_switch_remote_mobile_agent_into_phone_call_control_mode, 0x7C);
		makeStringItem(R.string.no_phone_number_for_remote_agent, 0x7D);
		makeStringItem(R.string.non_tl_version_link_text, 0x7E);
		makeStringItem(R.string.norm_sensitivity, 0x7F);
		makeStringItem(R.string.others_parameters, 0x80);
		makeStringItem(R.string.password, 0x81);
		makeStringItem(R.string.percent, 0x82);
		makeStringItem(R.string.phone_calls_control_mode, 0x83);
		makeStringItem(R.string.phone_number, 0x84);
		makeStringItem(R.string.phone_number_dialog_positive_button, 0x85);
		makeStringItem(R.string.phone_number_editor_hint, 0x86);
		makeStringItem(R.string.phone_power_switch_off_after_minute, 0x87);
		makeStringItem(R.string.photo_sended, 0x88);
		makeStringItem(R.string.program_face_the_control_panel, 0x89);
		makeStringItem(R.string.program_face_the_watch_dog, 0x8A);
		makeStringItem(R.string.remote_cmd_remote_listen, 0x8B);
		makeStringItem(R.string.remote_mobile_agent_can_not_connected_to_cell_network, 0x8C);
		makeStringItem(R.string.request_server_if_unknown_number, 0x8D);
		makeStringItem(R.string.request_server_if_unknown_number_hint2, 0x8E);
		makeStringItem(R.string.run_after_reboot, 0x8F);
		makeStringItem(R.string.run_after_reboot_hint2, 0x90);
		makeStringItem(R.string.save_into_file_dialog_title, 0x91);
		makeStringItem(R.string.save_into_file_dialog_title_u, 0x92);
		makeStringItem(R.string.save_into_folder_dialog_negative_button, 0x93);
		makeStringItem(R.string.save_into_folder_dialog_positive_button, 0x94);
		makeStringItem(R.string.save_into_folder_editor_hint, 0x95);
		makeStringItem(R.string.schpwronoff_beacon, 0x96);
		makeStringItem(R.string.schpwronoff_beacon_hint, 0x97);
		makeStringItem(R.string.seconds, 0x98);
		makeStringItem(R.string.select_button, 0x99);
		makeStringItem(R.string.select_car_alarms_button, 0x9A);
		makeStringItem(R.string.select_remote_control_button, 0x9B);
		makeStringItem(R.string.send_email_after_event_class_detection, 0x9C);
		makeStringItem(R.string.send_email_on_power_event, 0x9D);
		makeStringItem(R.string.send_email_on_power_event_hint2, 0x9E);
		makeStringItem(R.string.send_log_after_switch_off_to_developers, 0x9F);
		makeStringItem(R.string.send_log_button_text, 0x100);
		makeStringItem(R.string.send_photo_after_alert, 0x101);
		makeStringItem(R.string.send_sms_after_event_class_detection, 0x102);
		makeStringItem(R.string.sensitivity_settings_text, 0x103);
		makeStringItem(R.string.settings, 0x104);
		makeStringItem(R.string.short_help, 0x105);
		makeStringItem(R.string.short_minutes, 0x106);
		makeStringItem(R.string.short_seconds, 0x107);
		makeStringItem(R.string.short_space, 0x108);
		makeStringItem(R.string.show_first_screen, 0x109);
		makeStringItem(R.string.sign_up_title, 0x10A);
		makeStringItem(R.string.signup, 0x10B);
		makeStringItem(R.string.slogan1, 0x10C);
		makeStringItem(R.string.slogan2, 0x10D);
		makeStringItem(R.string.sms_control_mode, 0x10E);
		makeStringItem(R.string.sms_parameters, 0x10F);
		makeStringItem(R.string.software_quality_improvement, 0x110);
		makeStringItem(R.string.space, 0x111);
		makeStringItem(R.string.steal_wheels, 0x112);
		makeStringItem(R.string.switch_control_mode_activity, 0x113);
		makeStringItem(R.string.switch_off, 0x114);
		makeStringItem(R.string.switch_on, 0x115);
		makeStringItem(R.string.switched_off_by_timer, 0x116);
		makeStringItem(R.string.three_points, 0x117);
		makeStringItem(R.string.timeout_editor_hint, 0x118);
		makeStringItem(R.string.timer_settings_text, 0x119);
		makeStringItem(R.string.user_sensitivity, 0x11A);
		makeStringItem(R.string.wait_until_the_alert_call, 0x11B);
		makeStringItem(R.string.watch, 0x11C);
		makeStringItem(R.string.what_happened, 0x11D);
		makeStringItem(R.string.wssd_minutes_text, 0x11E);
		makeStringItem(R.string.wssd_minutes_value_hint, 0x11F);
		makeStringItem(R.string.wssd_seconds_text, 0x120);
		makeStringItem(R.string.wssd_seconds_value_hint, 0x121);
		makeStringItem(R.string.yandex_maps, 0x122);
		//makeStringItem(R.string.ymk_findme_not_found, 0x123);
		//makeStringItem(R.string.ymk_kilometers_short, 0x124);
		//makeStringItem(R.string.ymk_lang, 0x125);
		//makeStringItem(R.string.ymk_meters_short, 0x126);
		//makeStringItem(R.string.ymk_my_place, 0x127);
		makeStringItem(R.string.enter, 0x128);
		makeStringItem(R.string.registration, 0x129);
	}

	protected static void makeStringItem(int resourceId, int invariantId) {
		if (INTERNAL.containsKey(resourceId)) {
			throw new IllegalStateException("Not unique resource-id");
		}
		if (INTERNAL_REVERSE.containsKey(MARK_OF_STRING_RESOURCE | invariantId)) {
			throw new IllegalStateException("Not unique invariant-id");
		}
		INTERNAL.put(resourceId, MARK_OF_STRING_RESOURCE | invariantId);
		INTERNAL_REVERSE.put(MARK_OF_STRING_RESOURCE | invariantId, resourceId);
	}
	
	/**
	 * Throw IllegalStateException if invariant id not found.
	 * @param invariantId
	 * @return
	 */
	public static int getResourceId(int invariantId) {
		Integer resourceId = INDEXES.get(invariantId);
		if (resourceId == null) {
			throw new IllegalStateException("Unknown invariant-id");
		}
		return resourceId;
	}

	public static int getResourceId(int invariantId, int defaultResourceId) {
		Integer resourceId = INDEXES.get(invariantId);
		if (resourceId == null) {
			return defaultResourceId;
		}
		return resourceId;
	}
	/**
	 * Throw IllegalStateException if resource id not found.
	 * @param invariantId
	 * @return
	 */
	public static int getInvariantId(int resourceId) {
		Integer invariantId = RESOURCES.get(resourceId);
		if (invariantId == null) {
			throw new IllegalStateException("Unknown resource-id");
		}
		return invariantId;
	}
}
