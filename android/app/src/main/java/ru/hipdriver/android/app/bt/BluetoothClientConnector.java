package ru.hipdriver.android.app.bt;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.util.TextTools;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class BluetoothClientConnector implements BluetoothConnection {

	public static final String TAG = TextTools.makeTag(BluetoothClientConnector.class);
	private BluetoothAdapter bluetoothAdapter;
	
	private final UUID uuid;
	
	private BluetoothDataHandler dataHandler;
	
	private final Map<BluetoothDevice, BluetoothPipe> handlers = new WeakHashMap<BluetoothDevice, BluetoothPipe>(1);

	private final Map<BluetoothDevice, ConnectThread> connects = new WeakHashMap<BluetoothDevice, ConnectThread>(1);
	
	private class ConnectThread extends Thread {
	    private BluetoothSocket bluetoothSocket;
	    private final BluetoothDevice bluetoothDevice;
	 
	    public ConnectThread(BluetoothDevice device) {
	        this.bluetoothDevice = device;
	    }
	 
	    public void run() {
	        setName("bt-connect-thread[" + bluetoothDevice + "]");
	 
	    	while(!isInterrupted()) {
		        try {
		            // uuid is the app's UUID string, also used by the server code
		        	bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(uuid);
			        // Cancel discovery because it will slow down the connection
			    	bluetoothAdapter.cancelDiscovery();
		            // Connect the device through the socket. This will block
		            // until it succeeds or throws an exception
		            bluetoothSocket.connect();
		        } catch (IOException connectException) {
		            // Unable to connect; close the socket and get out
		            try {
		                bluetoothSocket.close();
		            } catch (IOException closeException) {
		            	Log.wtf(TAG, closeException);
		            }
		            try {
						Thread.sleep(TimeUnit.SECONDS.toMillis(15));
					} catch (InterruptedException e) {
		            	Log.wtf(TAG, e);
						return;
					}
		            continue;
		        }
		        break;
	    	}
	 
	        // Do work to manage the connection (in a separate thread)
	        handleSocket(bluetoothSocket);
	        
	        //Stop thread
	        connects.remove(bluetoothDevice);	        
	    }
	 
	    /** Will cancel an in-progress connection, and close the socket */
	    public void cancel() {
	        try {
	            bluetoothSocket.close();
	        } catch (IOException e) {
	        	Log.wtf(TAG, e);
	        }
	    }
	}
	
	public BluetoothClientConnector(UUID uuid) {
		this.uuid = uuid;
	}
	
	protected void handleSocket(BluetoothSocket socket) {
		if (dataHandler == null) {
			return;
		}
		BluetoothDevice remoteDevice = socket.getRemoteDevice();
		BluetoothPipe prevBluetoothPipe = handlers.get(remoteDevice);
		if (prevBluetoothPipe != null) {
			prevBluetoothPipe.cancel();
			prevBluetoothPipe.interrupt();
		}
		BluetoothPipe newBluetoothPipe = new BluetoothPipe(socket, dataHandler);
		handlers.put(remoteDevice, newBluetoothPipe);
		newBluetoothPipe.start();
		Log.d(TAG, "handling-client-connection-for-device[" + remoteDevice + "]");
		dataHandler.connect(remoteDevice);
	}

	@Override
	public boolean open(BluetoothDataHandler dataHandler) {
		this.dataHandler = dataHandler;
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (bluetoothAdapter == null) {
			return false;
		}
		if (!bluetoothAdapter.isEnabled()) {
			return false;
		}
		Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
		// If there are paired devices
		if (pairedDevices.isEmpty()) {
			return false;
		}
		boolean hasBondedDevice = false;
	    // Loop through paired devices
	    for (BluetoothDevice device : pairedDevices) {
	    	if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
	    		continue;
	    	}
	    	ConnectThread connectThread = new ConnectThread(device);
	    	connects.put(device, connectThread);
			connectThread.start();
	    	hasBondedDevice = true;
	    }
	    return hasBondedDevice;
	}
	
	public BluetoothPipe getPipe(BluetoothDevice device) {
		return handlers.get(device);
	}

	@Override
	public void close() {
		for (Entry<BluetoothDevice, ConnectThread> e : connects.entrySet()) {
			ConnectThread connectThread = e.getValue();
			connectThread.cancel();
			connectThread.interrupt();
		}
		for (Map.Entry<BluetoothDevice, BluetoothPipe> e : handlers.entrySet()) {
			BluetoothPipe bluetoothPipe = e.getValue();
			bluetoothPipe.cancel();
			bluetoothPipe.interrupt();
		}
	}
}
