package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MainService extends HipdriverService {
	private static final String TAG = TextTools.makeTag(MainService.class);

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onStart(Intent intent, int startId) {
		Log.d(TAG, "start-main-service");
		
		//Service can be start only on autoboot and no need to restart.
		//http://stackoverflow.com/questions/5856861/why-android-service-crashes-with-nullpointerexception
		if (intent == null) {
			return;
		}
		
		AppStatesEnum prevLifeAppState = (AppStatesEnum) intent.getSerializableExtra(HipdriverApplication.EXTRA_APPLICATION_STATE);
		CarStatesEnum prevLifeCarState = (CarStatesEnum) intent.getSerializableExtra(HipdriverApplication.EXTRA_CAR_STATE);
		CarStatesEnum prevLifeFirstDetectionCarState = (CarStatesEnum) intent.getSerializableExtra(HipdriverApplication.EXTRA_FIRST_DETECTION_CAR_STATE);
		Boolean prevLifePaused = (Boolean) intent.getSerializableExtra(HipdriverApplication.EXTRA_PAUSED);
		Boolean prevLifeDebugMode = (Boolean) intent.getSerializableExtra(HipdriverApplication.EXTRA_DEBUG_MODE);
		
		Intent intentStart = new Intent(getBaseContext(), MainActivity.class);
		intentStart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		if (prevLifeAppState != null) {
			intentStart.putExtra(HipdriverApplication.EXTRA_APPLICATION_STATE, prevLifeAppState);
		}
		if (prevLifeCarState != null) {
			intentStart.putExtra(HipdriverApplication.EXTRA_CAR_STATE, prevLifeCarState);
		}
		if (prevLifeFirstDetectionCarState != null) {
			intentStart.putExtra(HipdriverApplication.EXTRA_FIRST_DETECTION_CAR_STATE, prevLifeFirstDetectionCarState);
		}
		if (prevLifePaused != null) {
			intentStart.putExtra(HipdriverApplication.EXTRA_PAUSED, prevLifePaused);
		}
		if (prevLifeDebugMode != null) {
			intentStart.putExtra(HipdriverApplication.EXTRA_DEBUG_MODE, prevLifeDebugMode);
		}
		startActivity(intentStart);
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "stop-main-service");
		super.onDestroy();
	}
	

}
