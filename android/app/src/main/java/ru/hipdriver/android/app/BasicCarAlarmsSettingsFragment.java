package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import ru.hipdriver.android.app.R;

public class BasicCarAlarmsSettingsFragment extends FaceSelectCollectionHipdriverFragment {

	private static final String TAG = TextTools.makeTag(BasicCarAlarmsSettingsFragment.class);
	
	static interface OnEditPhoneNumber {
		void onChangeText(String text);
	}
	private OnEditPhoneNumber onEditPhoneNumber;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.basic_car_alarms_settings_fragment,
				container, false);
		refresh(view);
		return view;
	}

	private void refresh(final View rootView) {
		if (rootView == null) {
			return;
		}
//		TODO: uncomment below code
//		Button pftcpButton = (Button) findViewById(R.id.program_face_the_control_panel_button);
//		pftcpButton.setText(Html.fromHtml(getResources().getString(R.string.program_face_the_control_panel)));
//		Button pftwdButton = (Button) findViewById(R.id.program_face_the_watch_dog_button);
//		pftwdButton.setText(Html.fromHtml(getResources().getString(R.string.program_face_the_watch_dog)));
		
		setTextTo(rootView, R.id.application_version_text, R.string.application_version_text, ' ', Hardwares.getApplicationVersion(getA()));
		
		setTextTo(rootView, R.id.mobile_agent_name_editor, MainActivity.MOBILE_AGENT_NAME_EDITOR);
		EditText mobileAgentNameEditor = (EditText) rootView.findViewById(R.id.mobile_agent_name_editor);
		mobileAgentNameEditor.addTextChangedListener(new OnTextEdit() {
			
			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String mobileAgentName = s.toString();
    			getA().setString(MainActivity.MOBILE_AGENT_NAME_EDITOR, mobileAgentName);
    			getA().setMemoryVersion(IMobileAgent.MOBILE_AGENT_NAME_KEY);
			}
			
		});
		
		setTextTo(rootView, R.id.phone_number_editor, MainActivity.PHONE_NUMBER_EDITOR);
		EditText phoneNumberEditor = (EditText) rootView.findViewById(R.id.phone_number_editor);
		phoneNumberEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.phone_number_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		phoneNumberEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String phoneNumber = s.toString();
    			getA().setString(MainActivity.PHONE_NUMBER_EDITOR, phoneNumber);
    			getA().setMemoryVersion(IMobileAgent.ALERT_PHONE_NUMBER_KEY);
    			fireOnEditPhoneNumber(phoneNumber);
			}
			
		});
		
		EditText wssdMinutesEditor = (EditText) rootView.findViewById(R.id.wssd_minutes_editor);
		wssdMinutesEditor.setText(String.valueOf(getA().getInt(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS) / 60));
		wssdMinutesEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.wssd_minutes_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		wssdMinutesEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String wssdMinutesText = s.toString();
    			int wssdMinutes = TextTools.safetyGetInt(wssdMinutesText, 0);
				EditText wssdSecondsEditor = (EditText) rootView.findViewById(R.id.wssd_seconds_editor);
				String wssdSecondsText = wssdSecondsEditor.getText().toString();
				int wssdSeconds = TextTools.safetyGetInt(wssdSecondsText, 0);
    			
    			getA().setInt(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS, wssdMinutes * 60 + wssdSeconds);
    			getA().setMemoryVersion(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS);
			}
			
		});
		
		EditText wssdSecondsEditor = (EditText) rootView.findViewById(R.id.wssd_seconds_editor);
		wssdSecondsEditor.setText(String.valueOf(getA().getInt(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS) % 60));
		wssdSecondsEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.wssd_seconds_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		wssdSecondsEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String wssdSecondsText = s.toString();
    			int wssdSeconds = TextTools.safetyGetInt(wssdSecondsText, IMobileAgentProfile.DEFAULT_WATCHDOG_SERVICE_START_DELAY_IN_SECONDS % 60);
				EditText wssdMinutesEditor = (EditText) rootView.findViewById(R.id.wssd_minutes_editor);
    			String wssdMinutesText = wssdMinutesEditor.toString();
    			int wssdMinutes = TextTools.safetyGetInt(wssdMinutesText, IMobileAgentProfile.DEFAULT_WATCHDOG_SERVICE_START_DELAY_IN_SECONDS / 60);
    			
    			getA().setInt(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS, wssdMinutes * 60 + wssdSeconds);
    			getA().setMemoryVersion(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS);
			}
			
		});
		
		//Setup behaviour for conrol-mode switch
		new ControlModsEnumControl(getA(), rootView); 
		//Setup behaviour for geography switch
		new MapProvidersEnumControl(getA(), rootView);
	}
	
	protected void fireOnEditPhoneNumber(String phoneNumber) {
		// TODO Auto-generated method stub
		OnEditPhoneNumber onEditPhoneNumber = getOnEditPhoneNumber();
		if (onEditPhoneNumber == null) {
			return;
		}
		onEditPhoneNumber.onChangeText(phoneNumber);
	}

	protected FaceSelectCollectionPagerAdapter getFaceSelectCollectionPagerAdapter() {
		Activity activity = getActivity();
		if (activity == null) {
			Log.w(TAG, "get-face-select-collection-pager-adapter-impossible[activity-is-null]");
			return null;
		}
		ViewPager pager = (ViewPager) activity.findViewById(R.id.face_select_pager);
		if (pager == null) {
			Log.w(TAG, "get-face-select-collection-pager-adapter-impossible[pager-not-found]");
			return null;
		}
		PagerAdapter adapter = pager.getAdapter();
		if (!(adapter instanceof FaceSelectCollectionPagerAdapter)) {
			Log.w(TAG, "get-face-select-collection-pager-adapter-impossible[adapter-not-cast]");
			return null;
		}
		return (FaceSelectCollectionPagerAdapter) adapter; 		
	}

	public OnEditPhoneNumber getOnEditPhoneNumber() {
		return onEditPhoneNumber;
	}

	public void setOnEditPhoneNumber(OnEditPhoneNumber onEditPhoneNumber) {
		this.onEditPhoneNumber = onEditPhoneNumber;
	}

}
