package ru.hipdriver.android.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.TableLayout;
import ru.hipdriver.android.app.R;

public abstract class AbstractScreen extends TableLayout {

	protected float preferredLayoutWidth;
	protected float preferredLayoutHeight;

	public AbstractScreen(Context context) {
		super(context);
	}

	public AbstractScreen(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray ta = context.obtainStyledAttributes(attrs,
				R.styleable.LimitedExtensible);

		final int indexCount = ta.getIndexCount();
		for (int i = 0; i < indexCount; ++i) {
			int attr = ta.getIndex(i);
			switch (attr) {
			case R.styleable.LimitedExtensible_prefLayouthWidth:
				preferredLayoutWidth = ta.getDimension(attr, 0);
				break;
			case R.styleable.LimitedExtensible_prefLayouthHeight:
				preferredLayoutHeight = ta.getDimension(attr, 0);
				break;
			}
		}
		ta.recycle();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
		int parentWidthMode = MeasureSpec.getMode(widthMeasureSpec);

		int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
		int parentHeightMode = MeasureSpec.getMode(heightMeasureSpec);

		DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
		int maxWidth = displayMetrics.widthPixels;
		int maxHeight = displayMetrics.heightPixels;
		int prefWidth = (int) Pixels.dipToPixels(getContext(), preferredLayoutWidth);
		int prefHeight = (int) Pixels.dipToPixels(getContext(), preferredLayoutHeight);
		
		if ( prefWidth > 0 && parentWidth > 0 &&
			prefWidth >= getSuggestedMinimumWidth() &&
			(prefWidth < parentWidth || prefWidth < maxWidth) ) {
			//Override parent settings
			parentWidth = prefWidth + (getPaddingLeft() + getPaddingRight()) / 2;
		}
		if ( prefHeight > 0 && parentHeight > 0 &&
			prefHeight >= getSuggestedMinimumHeight() &&
			(prefHeight < parentHeight || prefHeight < maxHeight) ) {
			//Override parent settings
			parentHeight = prefHeight + (getPaddingTop() + getPaddingBottom()) /2;
		}
		
		super.onMeasure(MeasureSpec.makeMeasureSpec(parentWidth, parentWidthMode),
						MeasureSpec.makeMeasureSpec(parentHeight, parentHeightMode));
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
//		DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
//		int maxWidth = displayMetrics.widthPixels;
//		int prefWidth = (int) Pixels.dipToPixels(getContext(), preferredLayoutWidth);
//		if ( prefWidth < (r - l) || prefWidth < maxWidth ) {
//			ViewGroup.LayoutParams layoutParams = getLayoutParams();
//			layoutParams.width = prefWidth;
//			setLayoutParams(layoutParams);
//		}
		super.onLayout(changed, l, t, r, b);
	}
	

}
