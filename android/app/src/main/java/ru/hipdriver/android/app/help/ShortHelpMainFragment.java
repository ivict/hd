package ru.hipdriver.android.app.help;

import ru.hipdriver.android.app.R;
import ru.hipdriver.android.util.TextTools;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class ShortHelpMainFragment extends ShortHelpHipdriverFragment {

	private static final String TAG = TextTools.makeTag(ShortHelpMainFragment.class);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.short_help_main_fragment,
				container, false);
		refresh(view);
		return view;
	}

	private void refresh(final View view) {
		if (view == null) {
			return;
		}
		
		//final Button basicCarAlarmsSettingsButton = (Button) view.findViewById(R.id.basic_car_alarms_settings_button);
		//TODO: check invalid phone number
		
		View shortHelpControlWithRemoteControlAppButton = view.findViewById(R.id.short_help_control_with_remote_control_app_button);
		shortHelpControlWithRemoteControlAppButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ShortHelpCollectionPagerAdapter shortHelpCollectionPagerAdapter = getShortHelpCollectionPagerAdapter();
				if (shortHelpCollectionPagerAdapter == null) {
					Log.w(TAG, "short-help-collection-pager-adapter-is-null");
					return;
				}
				shortHelpCollectionPagerAdapter.show(1, new ShortHelpContentFragment(), R.string.short_help_control_with_remote_control_app, R.string.short_help_control_with_remote_control_app_content);
			}
			
		});
		
		View shortHelpControlByCallsButton = view.findViewById(R.id.short_help_control_by_calls_button);
		shortHelpControlByCallsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ShortHelpCollectionPagerAdapter shortHelpCollectionPagerAdapter = getShortHelpCollectionPagerAdapter();
				if (shortHelpCollectionPagerAdapter == null) {
					Log.w(TAG, "short-help-collection-pager-adapter-is-null");
					return;
				}
				shortHelpCollectionPagerAdapter.show(1, new ShortHelpContentFragment(), R.string.short_help_control_by_calls, R.string.short_help_control_by_calls_content);
			}
			
		});
		
		View shortHelpControlBySMSButton = view.findViewById(R.id.short_help_control_by_sms_button);
		shortHelpControlBySMSButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ShortHelpCollectionPagerAdapter shortHelpCollectionPagerAdapter = getShortHelpCollectionPagerAdapter();
				if (shortHelpCollectionPagerAdapter == null) {
					Log.w(TAG, "short-help-collection-pager-adapter-is-null");
					return;
				}
				shortHelpCollectionPagerAdapter.show(1, new ShortHelpContentFragment(), R.string.short_help_control_by_sms, R.string.short_help_control_by_sms_content);
			}
			
		});
		
		View shortHelpExitButton = view.findViewById(R.id.short_help_exit_button);
		shortHelpExitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FragmentActivity activity = getActivity();
				if (activity == null) {
					Log.w(TAG, "activity-is-null");
					return;
				}
				activity.finish();
			}
			
		});
		
	}

	private void notifyButton(Button button,
			int stringResourceId) {
		CharSequence text = Html.fromHtml(getString(stringResourceId) + " (<font color='#f17602'>!</font>)");
		button.setText(text);
	}
	
	private void clearNotifyForButton(Button button,
			int stringResourceId) {
		CharSequence text = Html.fromHtml(getString(stringResourceId));
		button.setText(text);
	}

}
