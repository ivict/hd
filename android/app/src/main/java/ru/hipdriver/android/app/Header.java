package ru.hipdriver.android.app;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import ru.hipdriver.android.app.R;

//@see http://stackoverflow.com/questions/2991110/android-how-to-stretch-an-image-to-the-screen-width-while-maintaining-aspect-ra
public class Header extends View {

	private Drawable logo;

	public Header(Context context) {
		super(context);
		setDefault();
	}

	public Header(Context context, AttributeSet attrs) {
		super(context, attrs);
		setDefault();
	}

	public Header(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setDefault();
	}
	
	public void setRemoteControlAppFace() {
		logo = getContext().getResources().getDrawable(R.drawable.header_less);
		setBackgroundDrawable(logo);
	}

	public void setDefault() {
		logo = getContext().getResources().getDrawable(R.drawable.header_less);
		setBackgroundDrawable(logo);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int height = width * logo.getIntrinsicHeight()
				/ logo.getIntrinsicWidth();
		setMeasuredDimension(width, height);
	}
}
