package ru.hipdriver.android.app;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.android.app.maps.GoogleMapFragment;
import ru.hipdriver.android.app.maps.GoogleMapTools;
import ru.hipdriver.android.app.maps.YandexMapFragment;
import ru.hipdriver.android.app.sslucs.MobileAgentStateDescription;
import ru.hipdriver.android.app.sslucs.ShortMobileAgentState;
import ru.hipdriver.android.i.ISslClient;
import ru.hipdriver.android.i.NetworkConnectionListener;
import ru.hipdriver.android.i.RequestCallback;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.ISid;
import ru.hipdriver.i.IUserLimit;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.i.support.ControlModsEnum;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.i.support.MapProvidersEnum;
import ru.hipdriver.i.support.UserCommandTypesEnum;
import ru.hipdriver.j.MobileAgentWithState;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.location.MyLocationItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.Camera.CameraInfo;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import ru.hipdriver.android.app.R;

public class RemoteControlFragment extends HipdriverFragment implements NetworkConnectionListener {
	
	//protected static final String REMOTE_CONTROL_BUTTON_IS_CHECKED = "remote_control_button_is_checked";
	protected static final String TAG = TextTools.makeTag(RemoteControlFragment.class);
	private static final long PING_PERIOD_IN_MILLIS = TimeUnit.MINUTES.toMillis(10);
	private static final long PING_PERIOD_IN_ALERT_STATE_IN_MILLIS = TimeUnit.SECONDS.toMillis(10);
	private static final long PING_PERIOD_IN_ALERT_STATE_IN_MILLIS_IF_CONTROL_MODE_INTERNET = TimeUnit.MINUTES.toMillis(1);
	public static final String LAST_LONGTTL_REQUEST_ID = "last_longttl_request_id";
	public static final int LED_NOTIFICATION_ID = 636960158;
	
	public static final int MAX_TRY_COUNT_TO_CHECK_STATE_CHANGE = 9;
	public static final String EXTRA_PICTURE_DATA = "extra_picture_data";
	protected static final float LOW_BATTERY_TRESHOLD = 20f;
	protected static final String FIRST_CLICK_ON_MAIN_BUTTON = "first_click_on_main_button";
	private static final String COUNTDOWN_TIMER_EXPIRED_DATE = "countdown_timer_expired_date";
	protected static final String NUMBER_OF_REMOTE_CAMERAS = "$number-of-remote-cameras$";  
	
	private Thread refinePositionThread;
	
	private class GooglePosition {
		private MarkerOptions markerOptions;
		private Marker marker;
		private CircleOptions circleOptions;
		private Circle circle;
		public GoogleMap googleMap;
	}
	
	private GooglePosition currentPositionForGoogle;
	
	private class YandexPosition {
		private MyLocationItem overlayItem;
		private BalloonItem balloonItem;
		private Overlay overlay;
	}
	
	//Orange f15c03
	//Green 6ba02c
	private enum TextColors {
		GRAY(Color.rgb(0xaf, 0xaf, 0xaf)), NORM(Color.rgb(0x6b, 0xa0, 0x2c)), ALARMS(Color.rgb(0xf1, 0x5c, 0x03));
		private final int color;
		private TextColors(int color) {
			this.color = color;
		}
		public int getColor() {
			return color;
		}
	}
	
	private YandexPosition currentPositionForYandex;
	private Timer timer;
	
	private View rootView;
	
	private MobileAgentWithState mas;
	private boolean masGpsOn;
	private int masRid = R.string.space;
	
	//Reference for stored last map position
	private Object lastMapView;
	
	private boolean mapPositioned;
	private AppStatesEnum prevRemoteAgentState;
	
	private AtomicReference<TimerTask> pingTaskRef;
	
	private AtomicReference<TimerTask> alertPingTaskRef;
	
	private AtomicReference<TimerTask> countdownTimerTaskRef;
	
	private String remoteAgentStatusDescription;
	private long lastLongttlRequestId = ISid.INVALID_COMPONENT_SESSIONS_ID;
	
	private boolean showCountdownTimer = false;
	
	private RequestCallback changeRemoteAgentStateCallback = new RequestCallback() {

		@Override
		public boolean onResponse(Object returnValue,
				boolean isCommunicationError,
				boolean isTimeout) {
			updatePingLight(isCommunicationError, isTimeout);
			if (isCommunicationError || isTimeout) {
				return true;
			}
			if (returnValue == null) {
				return false;
			}
			if (returnValue instanceof ShortMobileAgentState) {
				if (rootView == null) {
					return false;
				}
				View view = rootView;
				final ImageButton remoteControlButton = (ImageButton) view.findViewById(R.id.remote_control_main);
				final Iprbar iprBar = (Iprbar) view.findViewById(R.id.remote_mobile_agent_state_iprbar);
				final ImageView appStateImage = (ImageView) view.findViewById(R.id.remote_mobile_agent_state_image);
				final ImageView stateBlockSignalStrengthImage = (ImageView) view.findViewById(R.id.state_block_ss_image);
				final ImageView stateBlockGpsImage = (ImageView) view.findViewById(R.id.state_block_gps_image);
				final ImageView stateBlockBatteryLevelImage = (ImageView) view.findViewById(R.id.state_block_bl_image);
				setVisualStateInBasicMode(remoteControlButton, iprBar,
						appStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage,
						(ShortMobileAgentState) returnValue);
			} else {
				MobileAgentStateDescription mobileAgentStateDescription = (MobileAgentStateDescription) returnValue;
				remoteAgentStatusDescription = mobileAgentStateDescription.getDescription();
				showStatus(mobileAgentStateDescription.getDescription(), TextColors.GRAY);
			}
			return true;
		}

		@Override
		public void onSendRequest(long requestId, boolean longTtlRequest, boolean isTimeout,
				boolean isCommunicationError) {
			if (longTtlRequest && requestId != ISid.INVALID_COMPONENT_SESSIONS_ID) {
				lastLongttlRequestId = requestId;
				getA().setLong(LAST_LONGTTL_REQUEST_ID, requestId);
			}
		}

	};
	private AppStatesEnum prevRemoteAppState;
	private CarStatesEnum prevRemoteCarState;
	private AppStatesEnum remoteAppState;
	private CarStatesEnum remoteCarState;
	private Date timerExpired = new Date(0);
	
	public RemoteControlFragment() {
		super();
		pingTaskRef = new AtomicReference<TimerTask>();
		alertPingTaskRef = new AtomicReference<TimerTask>();
		countdownTimerTaskRef = new AtomicReference<TimerTask>();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View  view = inflater.inflate(R.layout.remote_control_fragment, container, false);
		
		//showCountdownTimer = TextTools.isTimeLimited(Hardwares.getApplicationVersion(getA()));
		
		updateRemoteMobileAgentSettings();
		
		refresh(view);
		
		//Create ping task if doesn't created
		if (pingTaskRef.compareAndSet(null, new TimerTask() {

			@Override
			public void run() {
				View view = getView();
				if (view == null) {
					return;
				}
				if (!getUserVisibleHint()) {
					return;
				}
				view.post(new Runnable() {

					@Override
					public void run() {
						updateRemoteAgentState();
					}
					
				});
			}
			
		})) {
			getTimer().schedule(pingTaskRef.get(), PING_PERIOD_IN_MILLIS, PING_PERIOD_IN_MILLIS);
		}
		
		//showStatus(view, masRid, TextColors.NORM);
		
		//Update connection state listener
		getA().setNetworkConnectionListener(this);
		
		timerExpired = getA().getDate(COUNTDOWN_TIMER_EXPIRED_DATE, new Date(0));
		if (System.currentTimeMillis() < timerExpired.getTime()) {
			restartCountdownTimer();
		}
		
		return view;
	}
	
	private void restoreRemoteAgentListener() {
		if (lastLongttlRequestId == ISid.INVALID_COMPONENT_SESSIONS_ID) {
			return;
		}
		if (getA().getControlMode() != ControlModsEnum.INTERNET) {
			return;
		}
		getTimer().schedule(new TimerTask() {

			@Override
			public void run() {
				getA().waitForUcClientMaking(TimeUnit.SECONDS.toMillis(10));
				ISslClient ucClient = getA().getUcClient();
				if (ucClient == null) {
					Log.w(TAG, "can-not-restore-longttl-request-listener[ucClient is null]");
					return;
				}
				ucClient.restoreLongttlRequestListener(lastLongttlRequestId, changeRemoteAgentStateCallback);
			}
			
		}, 0);
		
	}

	private void firePressMainButtonInReserveMode(final ImageButton remoteControlButton, final Iprbar iprBar, final ImageView appStateImage, final ImageView stateBlockSignalStrengthImage, final ImageView stateBlockGpsImage, final ImageView stateBlockBatteryLevelImage) {
		appStateImage.setVisibility(View.GONE);
		iprBar.setVisibility(View.VISIBLE);
		remoteControlButton.setEnabled(false);
		prevRemoteAgentState = getA().getRemoteAppState();
		showStatus(R.string.linking, TextColors.GRAY);
		getA().runPhoneTask(new Runnable() {
			@Override
			public void run() {
				if (getA().getRemoteAppState() == AppStatesEnum.ASO) {
					getA().getRemoteUserCommandsExecutor().switchOnWatch();
				} else {
					Log.d(TAG, "switch-off-watch[fire-press-button]");				
					getA().getRemoteUserCommandsExecutor().switchOffWatch();
				}
			}
		});
		//Reset state for update.
		mas = null;
		updateDelayed(iprBar,
				appStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage,
				remoteControlButton, TimeUnit.SECONDS.toMillis(5), true, MAX_TRY_COUNT_TO_CHECK_STATE_CHANGE);
	}
	
	private void firePressMainButtonInBasicMode(final ImageButton remoteControlButton, final Iprbar iprBar, final ImageView appStateImage, final ImageView stateBlockSignalStrengthImage, final ImageView stateBlockGpsImage, final ImageView stateBlockBatteryLevelImage) {
		remoteControlButton.post(new Runnable() {

			@Override
			public void run() {
				appStateImage.setVisibility(View.GONE);
				iprBar.setVisibility(View.VISIBLE);
				remoteControlButton.setEnabled(false);
			}
			
		});
		showStatus(R.string.linking, TextColors.GRAY);
		getA().runPhoneTask(new Runnable() {
			@Override
			public void run() {
				UserCommandTypesEnum userCommand;
				boolean longTtlRequest;
				if (remoteControlButton.isSelected()) {
					userCommand = UserCommandTypesEnum.SOCA;
					longTtlRequest = false;
				} else {
					userCommand = UserCommandTypesEnum.ACA;
					longTtlRequest = true;
				}
				//Send command to remote agent
				final long remoteMobileAgentId = getA().getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
				getA().getUcClient().execCommandAsync(remoteMobileAgentId, userCommand, null, changeRemoteAgentStateCallback, longTtlRequest, 6);
			}
		});
	}
	
	protected void safetySelected(
			final View view) {
		view.post(new Runnable() {
			
			@Override
			public void run() {
				view.setSelected(false);
			}
		});
	}
	
	protected void getRemoteMobileAgentStateInReserveMode(final Iprbar iprBar,
			final ImageView appStateImage, final ImageView stateBlockSignalStrengthImage, final ImageView stateBlockGpsImage, final ImageView stateBlockBatteryLevelImage,
			final ImageButton remoteControlButton, final boolean waitForChangeState, final int tryCount) {
		long remoteMobileAgentId = getA().getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
		if (remoteMobileAgentId <=0) {
			return;
		}
		remoteControlButton.post(new Runnable() {

			@Override
			public void run() {
				appStateImage.setVisibility(View.GONE);
				iprBar.setVisibility(View.VISIBLE);
				remoteControlButton.setEnabled(false);
			}
			
		});
		masRid = R.string.three_points;
		UITaskFactory.getMas(getA(), new After() {

			@Override
			public void success(Object returnValue) {
				try {
					if (returnValue == null) {
						return;
					}
//					if (mas != null && eq(mas, returnValue)) {// && isMapPositioned()) {
//						iprBar.post(new Runnable() {
//							
//							@Override
//							public void run() {
//								iprBar.setVisibility(View.GONE);
//								appStateImage.setVisibility(View.VISIBLE);
//							}
//						});
//						return;
//					}
					mas = (MobileAgentWithState) returnValue;
					AppStatesEnum remoteAppState = DBLinks.unlinkAppState(mas.getAppStateId());
					if (remoteAppState == null) {
						return;
					}
					boolean isLocationChanged = false;
					ILocation lastLocation = mas.getLastLocation();
					if (lastLocation != null) {
//						int newLat = getA().getInt(MainActivity.SELECTED_DEVICE_LAT, lastLocation.getLat());
//						getA().setInt(MainActivity.SELECTED_DEVICE_LAT, lastLocation.getLat());
//						int newLon = getA().getInt(MainActivity.SELECTED_DEVICE_LON, lastLocation.getLon());
//						getA().setInt(MainActivity.SELECTED_DEVICE_LON, lastLocation.getLon());
//						int newAcc = getA().getInt(MainActivity.SELECTED_DEVICE_ACC, lastLocation.getAcc());
//						getA().setInt(MainActivity.SELECTED_DEVICE_ACC, lastLocation.getAcc());
//						isLocationChanged = newLat != lastLocation.getLat() ||
//								newLon != lastLocation.getLon() ||
//								newAcc != lastLocation.getAcc();
						isLocationChanged = true;
					} else {
						Log.w(TAG, "empty-last-location");
					}
					//Update only if lat, lon or acc changed
					//if (isLocationChanged && !isMapPositioned()) {
						showRemoteAgentLocation(remoteControlButton, lastLocation);
					//}
					
					//TODO: refactoring (now it's hardly linked with onChangedCheckedState for remoteControlButton) 
					getA().setRemoteAppState(remoteAppState);
				} finally {
					//Check if remote state not changed
					//Retry request until mas state is not stable
					//String stateA = String.valueOf(prevRemoteAgentState);
					//String stateB = String.valueOf(getA().getRemoteAppState());
					
					if (waitForChangeState && prevRemoteAgentState == getA().getRemoteAppState() && tryCount > 1) {
						long waitTimeInMillis = TimeUnit.SECONDS.toMillis(2) + (tryCount < MAX_TRY_COUNT_TO_CHECK_STATE_CHANGE - 3 ? TimeUnit.SECONDS.toMillis(10) : 0);
						updateDelayed(iprBar, appStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage, remoteControlButton, waitTimeInMillis, waitForChangeState, tryCount - 1);
						showSuggestionIfItLastTry(tryCount == 2);
					} else {
						endUpdateQuery(iprBar, appStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage,
								remoteControlButton);
					}

				}
			}

			@Override
			public void failure(int returnCode, String failureReason) {
				endUpdateQuery(iprBar,
						appStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage,
						remoteControlButton);
			}

		}, remoteMobileAgentId);
	}
	
	
	
	protected void showSuggestionIfItLastTry(boolean lastTry) {
		if (!lastTry) {
			return;
		}
		showStatus(R.string.suggestion_check_phone_number, TextColors.GRAY);
	}

	private boolean eq(MobileAgentWithState mas, Object returnValue) {
		if (!(returnValue instanceof MobileAgentWithState)) {
			return false;
		}
		MobileAgentWithState mas2 = (MobileAgentWithState) returnValue;
		if (mas.appStateId != mas2.appStateId) {
			Log.d(TAG, "app-state-id");
			return false;
		}
		if (mas.batteryPct != mas2.batteryPct) {
			Log.d(TAG, "battery-pct");
			return false;
		}
		if (mas.batteryStatus != mas2.batteryStatus) {
			Log.d(TAG, "battery-status");
			return false;
		}
		if (mas.carStateId != mas2.carStateId) {
			Log.d(TAG, "car-state-id");
			return false;
		}
		if (mas.gsmBitErrorRate != mas2.gsmBitErrorRate) {
			Log.d(TAG, "gsm-bit-error-rate");
			return false;
		}
		if (mas.gsmSignalStrength != mas2.gsmSignalStrength) {
			Log.d(TAG, "gsm-signal-strength");
			return false;
		}
		if (mas.lastLocation.lat != mas2.lastLocation.lat) {
			Log.d(TAG, "last-location-lat");
			return false;
		}
		if (mas.lastLocation.lon != mas2.lastLocation.lon) {
			Log.d(TAG, "last-location-lon");
			return false;
		}
		if (mas.lastLocation.alt != mas2.lastLocation.alt) {
			Log.d(TAG, "last-location-alt");
			return false;
		}
		if (mas.lastLocation.acc != mas2.lastLocation.acc) {
			Log.d(TAG, "last-location-acc");
			return false;
		}
		return true;
	}
	
	public boolean isMapPositioned() {
		return mapPositioned;
	}
	
	protected void updateDelayed(final Iprbar iprBar,
			final ImageView appStateImage, final ImageView stateBlockSignalStrengthImage, final ImageView stateBlockGpsImage, final ImageView stateBlockBatteryLevelImage,
			final ImageButton remoteControlButton,
			long millis,
			final boolean waitForChangeState,
			final int tryCount) {
		getTimer().schedule(new TimerTask() {
			@Override
			public void run() {
				FragmentActivity activity = getActivity();
				if (activity == null) {
					//TODO: make new task
					return;
				}
				getRemoteMobileAgentStateInReserveMode(iprBar, appStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage, remoteControlButton, waitForChangeState, tryCount);
			}
		}, millis);
	}
	
	protected void showRemoteAgentLocation(ImageView remoteControlButton, final ILocation lastLocation) {
		remoteControlButton.post(new Runnable() {
			
			@Override
			public void run() {
				MapProvidersEnum mapProvider = DBLinks.unlinkMapProvider((short) getA().getInt(IMobileAgentProfile.MAP_PROVIDER_ID));
				//If message is too late and fragment class is changed check fragment class
				// Getting reference to the SupportMapFragment
		        FragmentManager fragmentManager = getFragmentManager();
		        if (fragmentManager == null) {
					mapPositioned = true;		
		        	return;
		        }
				Fragment mapFragment = fragmentManager.findFragmentByTag("map_fragment");
				switch(mapProvider) {
				case GOOGLE:
					//Check class if message execute too late
					if (!(mapFragment instanceof GoogleMapFragment)) {
						break;
					}
					setRemoteAgentLocationForGoogle(lastLocation, (GoogleMapFragment) mapFragment);
					break;
				case YANDEX:
					//Check class if message execute too late
					if (!(mapFragment instanceof YandexMapFragment)) {
						break;
					}
					setRemoteAgentLocationForYandex(lastLocation, (YandexMapFragment) mapFragment);
					break;
				}
				mapPositioned = true;		
			}
		});
	}
	
	private void setRemoteAgentLocationForGoogle(
			ILocation lastLocation, GoogleMapFragment mapFragment) {
        //Get accuracy for last location
        if (lastLocation == null) {
        	return;
        }
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getA());
		  
		if (resultCode != ConnectionResult.SUCCESS) {
			return;
		}
		
        if (mapFragment == null) {
        	Log.wtf(TAG, "can't find map_fragment");
        	return;
        }
        // Getting GoogleMap object from the fragment
        GoogleMap googleMap = mapFragment.getMap();
        lastMapView = googleMap;
        
        int acc = lastLocation.getAcc() <= 0 ? 30000: lastLocation.getAcc();
    	double radiusInMeters = (acc / 100d) * Math.E;//check gps info 68% probability for accuracy
        
        // Getting latitude of the current location
        //float[] placemark = mas.placemark;
		double latitude = Locations.floatLatitude(lastLocation.getLat());
 
        // Getting longitude of the current location
        double longitude = Locations.floatLongitude(lastLocation.getLon());
 
        // Creating a LatLng object for the current location
        LatLng latlng = new LatLng(latitude, longitude);
        
        //// Showing the current location in Google Map
        //googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
 
		String selectDevice = getString(R.string.select_device);
        String deviceName = getA().getString(MainActivity.SELECTED_DEVICE_NAME, selectDevice);
        if (currentPositionForGoogle == null || currentPositionForGoogle.googleMap != googleMap) {
        	currentPositionForGoogle = new GooglePosition();
        	currentPositionForGoogle.markerOptions = new MarkerOptions().position(latlng).title(deviceName).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_baloon_blue));
        	currentPositionForGoogle.marker = googleMap.addMarker(currentPositionForGoogle.markerOptions);

            int strokeColor = 0xffff0000; //red outline
            int shadeColor = 0x44ff0000; //opaque red fill

            currentPositionForGoogle.circleOptions = new CircleOptions().center(latlng).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(1);
            currentPositionForGoogle.circle = googleMap.addCircle(currentPositionForGoogle.circleOptions);
            currentPositionForGoogle.googleMap = googleMap;
        }
        currentPositionForGoogle.marker.setTitle(deviceName);
        currentPositionForGoogle.marker.setPosition(latlng);
        currentPositionForGoogle.circle.setCenter(latlng);
        currentPositionForGoogle.circle.setRadius(radiusInMeters);
        
        // Zoom in the Google Map
        // Showing the current location in Google Map        
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 16f));
        if (googleMap.getUiSettings().isZoomControlsEnabled()) {
        	Log.d(TAG, "zoom-controls-enabled");
        }
	}
	
	private void setRemoteAgentLocationForYandex(
			ILocation lastLocation, YandexMapFragment mapFragment) {
        //Get accuracy for last location
        if (lastLocation == null) {
        	return;
        }
		
		if (mapFragment == null) {
        	Log.wtf(TAG, "can't find map_fragment");
			return;
		}
		MapView mapView = (MapView) mapFragment.getView().findViewById(R.id.yandex_map);
		lastMapView = mapView;
        if (mapView == null) {
        	return;
        }
		mapView.showZoomButtons(true);
		
        int acc = lastLocation.getAcc() <= 0 ? 30000: lastLocation.getAcc();
    	double radiusInMeters = acc / 100d;
        
        // Getting latitude of the current location
        //float[] placemark = mas.placemark;
		double latitude = Locations.floatLatitude(lastLocation.getLat());
 
        // Getting longitude of the current location
        double longitude = Locations.floatLongitude(lastLocation.getLon());
 
        // Creating a GeoPoint object for the current location
        GeoPoint latlng = new GeoPoint(latitude, longitude);
        
        MapController mapController = mapView.getMapController();
 
		String selectDevice = getString(R.string.select_device);
        String deviceName = getA().getString(MainActivity.SELECTED_DEVICE_NAME, selectDevice);
        if (currentPositionForYandex == null || currentPositionForYandex.overlay.getMapController() != mapController) {
        	currentPositionForYandex = new YandexPosition();
        	currentPositionForYandex.overlayItem = new MyLocationItem(new GeoPoint(latitude, longitude), getResources().getDrawable(R.drawable.ic_baloon_blue));
        	//currentPositionForYandex.overlayItem.s
        	currentPositionForYandex.balloonItem = new BalloonItem(getActivity(), currentPositionForYandex.overlayItem.getGeoPoint());
        	currentPositionForYandex.overlay = new Overlay(mapController);
        	currentPositionForYandex.overlay.addOverlayItem(currentPositionForYandex.overlayItem);
        	currentPositionForYandex.overlayItem.setBalloonItem(currentPositionForYandex.balloonItem);
        	mapController.getOverlayManager().addOverlay(currentPositionForYandex.overlay);
        }
        currentPositionForYandex.overlayItem.setGeoPoint(latlng);
        currentPositionForYandex.balloonItem.setGeoPoint(latlng);
        currentPositionForYandex.balloonItem.setText(deviceName);
        
        // Showing the current location in Yandex Map
        // Zoom in the Yandex Map
        mapController.setPositionAnimationTo(latlng, 16f);
	}

	public void updateRemoteAgentState() {
		final View view = getView() == null? rootView : getView();
		if (view == null) {
			return;
		}
		
		final Iprbar iprBar = (Iprbar) view.findViewById(R.id.remote_mobile_agent_state_iprbar);  
		
		final ImageView remoteMobileAgentStateImage = (ImageView) view.findViewById(R.id.remote_mobile_agent_state_image);  
		
		//Gps, signal-strength and battery-pct
		final ImageView stateBlockSignalStrengthImage = (ImageView) view.findViewById(R.id.state_block_ss_image);
		final ImageView stateBlockGpsImage = (ImageView) view.findViewById(R.id.state_block_gps_image);
		final ImageView stateBlockBatteryLevelImage = (ImageView) view.findViewById(R.id.state_block_bl_image);
		
		final ImageButton remoteControlButton = (ImageButton) view.findViewById(R.id.remote_control_main);
		
		showStatus(R.string.linking, TextColors.GRAY);
		
		final ControlModsEnum controlMode = getA().getControlMode();
		//updatePingLight(false, false);
		
		//Prepare update
		view.post(new Runnable() {
			
			@Override
			public void run() {
				Activity activity = getActivity();
				if (activity != null) {
					View internetStatusButton = activity.findViewById(R.id.link_status_button);
					if (controlMode == ControlModsEnum.INTERNET) {
						internetStatusButton.setSelected(true);
					} else {
						internetStatusButton.setSelected(false);
					}
				}
			}
		});
		
		switch (controlMode) {
	    case INTERNET:
			getRemoteMobileAgentStateInBasicMode(iprBar, remoteMobileAgentStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage, remoteControlButton);
	    	break;
	    case PHONE_CALLS:
	    case SMS:
			getRemoteMobileAgentStateInReserveMode(iprBar, remoteMobileAgentStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage, remoteControlButton, false, 1);
	    	break;
		}
	}
	
	private void getRemoteMobileAgentStateInBasicMode(final Iprbar iprBar,
			final ImageView appStateImage, final ImageView stateBlockSignalStrengthImage, final ImageView stateBlockGpsImage, final ImageView stateBlockBatteryLevelImage,
			final ImageButton remoteControlButton) {
		final long remoteMobileAgentId = getA().getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
		if (remoteMobileAgentId <=0) {
			return;
		}
		remoteControlButton.post(new Runnable() {

			@Override
			public void run() {
				appStateImage.setVisibility(View.GONE);
				iprBar.setVisibility(View.VISIBLE);
				remoteControlButton.setEnabled(false);
			}
			
		});
		ISslClient ucClient = getA().getUcClient();
		if (ucClient == null) {
			return;
		}
		ucClient.execCommandAsync(remoteMobileAgentId, UserCommandTypesEnum.WAY, null, new RequestCallback() {
			
			@Override
			public boolean onResponse(Object returnValue, boolean isCommunicationError,
					boolean isTimeout) {
				updatePingLight(isCommunicationError, isTimeout);
				if (returnValue == null) {
					//TODO: setup bad response from remote agent 
					return false;
				}
				if (returnValue instanceof ShortMobileAgentState) {
					setVisualStateInBasicMode(remoteControlButton, iprBar,
							appStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage,
							(ShortMobileAgentState) returnValue);
				} else {
					MobileAgentStateDescription mobileAgentStateDescription = (MobileAgentStateDescription) returnValue;
					remoteAgentStatusDescription = mobileAgentStateDescription.getDescription();
					showStatus(mobileAgentStateDescription.getDescription(), TextColors.GRAY);
				}
				return false;
			}

			@Override
			public void onSendRequest(long requestId,
					boolean longTtlRequest, boolean isCommunicationError, boolean isTimeout) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					return;
				}
				if (isCommunicationError || isTimeout) {
					updatePingLight(isCommunicationError, isTimeout);
				}
			}
		}, false, 6);
	}

	private ILocation getStoredLastLocation() {
		int lat = getA().getInt(MainActivity.SELECTED_DEVICE_LAT, 53251132);
		int lon = getA().getInt(MainActivity.SELECTED_DEVICE_LON, 50222687);
		int acc = getA().getInt(MainActivity.SELECTED_DEVICE_ACC, 30000);
		return Locations.readonlyLocation(lat, lon, ILocation.UNKNOWN_ALTITUDE, acc, ILocation.UNKNOWN_MOBILE_COUNTRY_CODE, ILocation.UNKNOWN_MOBILE_NETWORK_CODE, ILocation.UNKNOWN_LOCATION_AREA_CODE, ILocation.UNKNOWN_CELL_ID);
	}

	@Override
	public void onDestroyView() {
		try {
			//Save current position for map view in remote conrol
			saveMapPosition();
			if (timer != null) {
				timer.cancel();
				timer = null;
			}
		} finally {
			super.onDestroyView();
		}
	}

	@Override
	public void onDestroy() {
		onDestroyView();
		super.onDestroy();
	}

	protected void saveMapPosition() {
		if (lastMapView == null) {
			return;
		}
		//TODO: refactoring
		if (lastMapView instanceof MapView) {
			MapView mapView = (MapView) lastMapView;
			MapController mapController = mapView.getMapController();
			GeoPoint mapCenter = mapController.getMapCenter();
			LatLng latlng = new LatLng(mapCenter.getLat(), mapCenter.getLon());
			float zoom = mapController.getZoomCurrent();
			CameraPosition camPos = CameraPosition.fromLatLngZoom(latlng, zoom);
			GoogleMapTools.safetySavePosition(getA(), camPos);
		} else if (lastMapView instanceof GoogleMap){
			GoogleMap mapView = (GoogleMap) lastMapView;
			GoogleMapTools.safetySavePosition(getA(), mapView.getCameraPosition());
		}
	}
	

	private void restartRefinePosition() {
		if (refinePositionThread != null) {
			refinePositionThread.interrupt();
		}
		refinePositionThread = new Thread(new Runnable() {

			@Override
			public void run() {
				
			}
			
		}, "Refine RMA position");
	}

	public static Fragment newMapFragment(HipdriverApplication hipdriverApplication) {
		//Map views behavior
		MapProvidersEnum mapProvider = DBLinks.unlinkMapProvider((short) hipdriverApplication.getInt(IMobileAgentProfile.MAP_PROVIDER_ID));
		String fragmentTag;
		
		GoogleMapOptions googleMapOptions = new GoogleMapOptions();
		googleMapOptions.camera(GoogleMapTools.restorePosition(hipdriverApplication));
		switch (mapProvider) {
		case GOOGLE:
			fragmentTag = "google_map_fragment";
			return ru.hipdriver.android.app.maps.GoogleMapFragment.newInstance2(googleMapOptions);
		case YANDEX:
			fragmentTag = "yandex_map_fragment";
			ru.hipdriver.android.app.maps.YandexMapFragment yandexMapFragment = new ru.hipdriver.android.app.maps.YandexMapFragment();
			yandexMapFragment.setGoogleMapOptions(googleMapOptions);
			return yandexMapFragment;
		default:
			throw new IllegalStateException("Unknown map provider");
		}
	}
	
	public static Class<? extends Fragment> getMapFragmentClass(HipdriverApplication hipdriverApplication) {
		MapProvidersEnum mapProvider = DBLinks.unlinkMapProvider((short) hipdriverApplication.getInt(IMobileAgentProfile.MAP_PROVIDER_ID));
		switch (mapProvider) {
		case GOOGLE:
			return ru.hipdriver.android.app.maps.GoogleMapFragment.class;
		case YANDEX:
			return ru.hipdriver.android.app.maps.YandexMapFragment.class;
		default:
			throw new IllegalStateException("Unknown map provider");
		}
	}
	
	
	private synchronized Timer getTimer() {
		//Create timer
		if (timer == null) {
			timer = new Timer("UpdateBigMainButtonState");
		}
		return timer;
	}

	public void setMapPositioned(boolean mapPositioned) {
		this.mapPositioned = mapPositioned;
	}

	private void endUpdateQuery(final Iprbar iprBar,
			final ImageView appStateImage, final ImageView stateBlockSignalStrengthImage, final ImageView stateBlockGpsImage, final ImageView stateBlockBatteryLevelImage,
			final ImageButton remoteControlButton) {
		remoteControlButton.post(new Runnable() {

			@Override
			public void run() {
				//Prevent NPE (if call getResources())
				if (getActivity() == null) {
					return;
				}
				int resourceId = R.string.three_points;
				AppStatesEnum remoteAppState = getA().getRemoteAppState();
				CarStatesEnum remoteCarState = DBLinks.unlinkCarState(mas.getCarStateId());
				if (remoteAppState == AppStatesEnum.ASO) {
					appStateImage.setImageDrawable(getResources().getDrawable(R.drawable.car_nolink));
					remoteControlButton.setSelected(false);
					if (remoteAgentStatusDescription == null) {
						resourceId = getStatusMessageId(masRid, R.string.car_alarms_switched_off);
						//TODO: refactoring messages onto watchdog-application face
						if (R.string.watch == resourceId
								|| R.string.car_evacuate == resourceId
								|| R.string.steal_wheels == resourceId
								|| R.string.car_poke == resourceId
								|| R.string.car_stolen == resourceId
								|| R.string.car_jam == resourceId
								|| R.string.alert == resourceId
								|| R.string.space == resourceId
								) {
							resourceId = R.string.car_alarms_switched_off;
						}
						showStatus(resourceId, TextColors.NORM);
					}
				} else if (remoteCarState != null) {
					switch(remoteCarState){
					case U:
						appStateImage.setImageDrawable(getResources().getDrawable(R.drawable.car_norm));
						resourceId = getStatusMessageId(masRid, R.string.watch);
						//TODO: correct message SslServer side.
						if (R.string.car_alarms_switched_off == resourceId
								|| R.string.space == resourceId
								|| R.string.alert == resourceId) {
							resourceId = R.string.watch;
						}
						showStatus(resourceId, TextColors.NORM);
						break;
					case CE:
						appStateImage.setImageDrawable(getResources().getDrawable(R.drawable.ss0a));
						resourceId = getStatusMessageId(masRid, R.string.car_evacuate);
						if (R.string.car_alarms_switched_off == resourceId
								|| R.string.space == resourceId
								|| R.string.alert == resourceId) {
							resourceId = R.string.watch;
						}
						showStatus(resourceId, TextColors.ALARMS);
						break;
					case WS:
						appStateImage.setImageDrawable(getResources().getDrawable(R.drawable.ss1a));
						resourceId = getStatusMessageId(masRid, R.string.steal_wheels);
						if (R.string.car_alarms_switched_off == resourceId
								|| R.string.space == resourceId
								|| R.string.alert == resourceId) {
							resourceId = R.string.watch;
						}
						showStatus(masRid, TextColors.ALARMS);
						break;
					case CP:
						appStateImage.setImageDrawable(getResources().getDrawable(R.drawable.ss2a));
						resourceId = getStatusMessageId(masRid, R.string.car_poke);
						if (R.string.car_alarms_switched_off == resourceId
								|| R.string.space == resourceId
								|| R.string.alert == resourceId) {
							resourceId = R.string.watch;
						}
						showStatus(resourceId, TextColors.ALARMS);
						break;
					case CS:
						appStateImage.setImageDrawable(getResources().getDrawable(R.drawable.ss3a));
						resourceId = getStatusMessageId(masRid, R.string.car_stolen);
						if (R.string.car_alarms_switched_off == resourceId
								|| R.string.space == resourceId
								|| R.string.alert == resourceId) {
							resourceId = R.string.watch;
						}
						showStatus(resourceId, TextColors.ALARMS);
						break;
					case JAM:
						appStateImage.setImageDrawable(getResources().getDrawable(R.drawable.ss4a));
						resourceId = getStatusMessageId(masRid, R.string.car_jam);
						if (R.string.car_alarms_switched_off == resourceId
								|| R.string.space == resourceId
								|| R.string.alert == resourceId) {
							resourceId = R.string.watch;
						}
						showStatus(resourceId, TextColors.ALARMS);
						break;
					//case A: and so on
					default:
						appStateImage.setImageDrawable(getResources().getDrawable(R.drawable.car_alarm));
						resourceId = getStatusMessageId(masRid, R.string.alert);
						if (R.string.car_alarms_switched_off == resourceId
								|| R.string.space == resourceId) {
							resourceId = R.string.watch;
						}
						showStatus(resourceId, TextColors.ALARMS);
					}
					remoteControlButton.setSelected(true);
				}
				iprBar.setVisibility(View.GONE);
				appStateImage.setVisibility(View.VISIBLE);
				
				//Signal strength
				if (mas.getGsmSignalStrength() < 5) {
					stateBlockSignalStrengthImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_ss_1));
				} else if (mas.getGsmSignalStrength() < 10) {
					stateBlockSignalStrengthImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_ss_2));
				} else {
					stateBlockSignalStrengthImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_ss_3));
				}
				//Gps status
				if (masGpsOn) {
					stateBlockGpsImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_gps_on));
				} else {
					stateBlockGpsImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_gps_off));
				}
				if (isCharging(mas)) {

					stateBlockBatteryLevelImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_bc));
					
				} else {
					
					//Battery level
					float defaultBeaconModeLevel = IMobileAgentProfile.DEFAULT_BEACON_PWR_TRESHOLD_PERCENTS;
					if (mas.getBatteryPct() < LOW_BATTERY_TRESHOLD) {
						stateBlockBatteryLevelImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_bl_0));
					} else if (mas.getBatteryPct() < (33f + defaultBeaconModeLevel)) {
						stateBlockBatteryLevelImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_bl_1));
					} else if (mas.getBatteryPct() < (66f + defaultBeaconModeLevel)) {
						stateBlockBatteryLevelImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_bl_2));
					} else {
						stateBlockBatteryLevelImage.setImageDrawable(getResources().getDrawable(R.drawable.state_block_bl_3));
					}
					
				}
				
				//ControlModsEnum controlMode = getA().getControlMode();
				remoteControlButton.setEnabled(true);
				onUpdateRemoteAgentState(remoteAppState, remoteCarState);
			}
			
		});
	}
	
	protected void onUpdateRemoteAgentState(AppStatesEnum remoteAppState,
			CarStatesEnum remoteCarState) {
		this.remoteAppState = remoteAppState;
		this.remoteCarState = remoteCarState;
		if (prevRemoteAppState != remoteAppState && remoteAppState != null) {
			onChangeRemoteAppState(remoteAppState, remoteCarState);
			prevRemoteAppState = remoteAppState;
		}
		if (prevRemoteCarState != remoteCarState && remoteCarState != null) {
			if (remoteCarState == CarStatesEnum.A) {
				Log.d(TAG, "usual-alarm");
				alarm();
			}
			if (prevRemoteCarState == null && (
					remoteCarState == CarStatesEnum.CE ||
					remoteCarState == CarStatesEnum.CP ||
					remoteCarState == CarStatesEnum.CS ||
					remoteCarState == CarStatesEnum.WS ||
					remoteCarState == CarStatesEnum.JAM	)) {
				Log.d(TAG, "alarm-on-reconnect-and-detect-alert-state");
				alarm();
			}
			if ( remoteCarState == CarStatesEnum.A ||
					remoteCarState == CarStatesEnum.CE ||
					remoteCarState == CarStatesEnum.CP ||
					remoteCarState == CarStatesEnum.CS ||
					remoteCarState == CarStatesEnum.WS ||
					remoteCarState == CarStatesEnum.JAM	) {
				TimerTask newAlertPingTask = new TimerTask() {

					@Override
					public void run() {
						updateRemoteAgentState();
					}
					
				};
				TimerTask oldAlertPingTask = alertPingTaskRef.getAndSet(newAlertPingTask);
				if (oldAlertPingTask != null) {
					oldAlertPingTask.cancel();
				}
				//TODO: get update time from remote agent settings 
				long timeForUpdateInMillis = PING_PERIOD_IN_ALERT_STATE_IN_MILLIS;
				if (getA().getControlMode() == ControlModsEnum.INTERNET) {
					timeForUpdateInMillis = PING_PERIOD_IN_ALERT_STATE_IN_MILLIS_IF_CONTROL_MODE_INTERNET;
				}
				getTimer().schedule(newAlertPingTask, timeForUpdateInMillis, timeForUpdateInMillis);
				
			} else {
				TimerTask alertPingTask = alertPingTaskRef.getAndSet(null);
				if (alertPingTask != null) {
					alertPingTask.cancel();
				}
			}
			prevRemoteCarState = remoteCarState;
		}

	}
	
	private void onChangeRemoteAppState(AppStatesEnum remoteAppState, CarStatesEnum remoteCarState) {
		final WatchdogService watchdogService = getA().getWatchdogService();
		if (watchdogService == null) {
			return;
		}
		Log.d(TAG, "remote-app-state-changed");
		//Low battery warning
		
		if (mas != null && mas.getBatteryPct() < LOW_BATTERY_TRESHOLD && !isCharging(mas)) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					watchdogService.beepLowBattery();
				}
				
			}, TimeUnit.SECONDS.toMillis(1));
		}
		switch (remoteAppState) {
		case CAC:
			watchdogService.beepCac();
			break;
		case ASO:
			watchdogService.beepCaso();
			break;
		default:
			return;
		}
		
	}

	protected boolean isCharging(MobileAgentWithState mas) {
		int batteryStatus = mas.getBatteryStatus();
		return batteryStatus == BatteryManager.BATTERY_STATUS_CHARGING ||
				batteryStatus == BatteryManager.BATTERY_STATUS_FULL;
	}

	private int getStatusMessageId(int remoteMobileAgentMessageId, int defaultMessageId) {
		if (remoteMobileAgentMessageId != R.string.three_points) {
			return remoteMobileAgentMessageId;
		}
		return defaultMessageId;
	}

	private void setVisualStateInBasicMode(
			final ImageButton remoteControlButton, final Iprbar iprBar,
			final ImageView appStateView, final ImageView stateBlockSignalStrengthImage, final ImageView stateBlockGpsImage, final ImageView stateBlockBatteryLevelImage,
			ShortMobileAgentState remoteMobileAgentState) {
		if (remoteMobileAgentState == null) {
			iprBar.post(new Runnable() {
				
				@Override
				public void run() {
					//TODO: make watchdog unconnected state
					iprBar.setVisibility(View.GONE);
					appStateView.setVisibility(View.VISIBLE);
					//remoteControlButton.setEnabled(true);
					//showStatus("Нет связи");
				}
			});
			Thread.yield();
		} else {
			mas = new MobileAgentWithState();
			mas.setAppStateId(remoteMobileAgentState.getAppStateId());
			mas.setLastLocation(remoteMobileAgentState.getLastLocation());
			mas.setCarStateId(remoteMobileAgentState.getLastLocation().getCarStateId());
			mas.setGsmSignalStrength(remoteMobileAgentState.getGsmSignalStrength());
			mas.setBatteryPct(remoteMobileAgentState.getBatteryPct());
			mas.setBatteryStatus(remoteMobileAgentState.getBatteryStatus());
			masGpsOn = remoteMobileAgentState.isGpsOn();
			masRid = CCRI.getResourceId(remoteMobileAgentState.getIid(), R.string.space);
			AppStatesEnum remoteAppState = DBLinks.unlinkAppState(mas.getAppStateId());
			if (remoteAppState == null) {
				return;
			}
			boolean isLocationChanged = false;
			final ILocation lastLocation = mas.getLastLocation();
			if (lastLocation != null && lastLocation.getLat() != ILocation.UNKNOWN_ALTITUDE) {
				int newLat = getA().getInt(MainActivity.SELECTED_DEVICE_LAT, lastLocation.getLat());
				getA().setInt(MainActivity.SELECTED_DEVICE_LAT, lastLocation.getLat());
				int newLon = getA().getInt(MainActivity.SELECTED_DEVICE_LON, lastLocation.getLon());
				getA().setInt(MainActivity.SELECTED_DEVICE_LON, lastLocation.getLon());
				int newAcc = getA().getInt(MainActivity.SELECTED_DEVICE_ACC, lastLocation.getAcc());
				getA().setInt(MainActivity.SELECTED_DEVICE_ACC, lastLocation.getAcc());
				isLocationChanged = newLat != lastLocation.getLat() ||
						newLon != lastLocation.getLon() ||
						newAcc != lastLocation.getAcc();
			} else {
				Log.w(TAG, "wrong-or-empty-last-location");
			}
			//Update only if lat, lon or acc changed
			if (isLocationChanged) {
				showRemoteAgentLocation(remoteControlButton, lastLocation);
			}
			
			//TODO: refactoring (now it's hardly linked with onChangedCheckedState for remoteControlButton) 
			getA().setRemoteAppState(remoteAppState);
			endUpdateQuery(iprBar,
					appStateView, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage,
					remoteControlButton);
		}
	}
	
	protected void showStatus(int stringResourceId, TextColors textColor) {
		final View view = getView() == null? rootView : getView();		
		if (view == null) {
			Log.w(TAG, String.format("status[%s]-not-displayed[%s]", stringResourceId, "parent view is null"));
			return;
		}
		showStatus(view, stringResourceId, textColor);
	}
	
	private void showStatus(View view, final int stringResourceId, final TextColors textColor) {
		if (view == null) {
			return;
		}
		final TextView remoteControlStatusText = (TextView) view.findViewById(R.id.remote_control_status_text);
		final TextView remoteControlExtStatusText = (TextView) view.findViewById(R.id.remote_control_ext_status_text);
		remoteControlStatusText.post(new Runnable() {

			@Override
			public void run() {
				Activity activity = getActivity();
				if (activity == null) {
					Log.w(TAG, String.format("status[%s]-not-displayed[%s]", stringResourceId, "fragment not linked to activity"));
					return;
				}
				String text = "err";
				//Correct error in communication protocol.
				try {
					text = getResources().getString(stringResourceId);
				} catch (Resources.NotFoundException e) {
					Log.wtf(TAG, e);
				}
				if (stringResourceId == R.string.watch && isShowCountdownTimer()) {
					text = getResources().getString(R.string.counterdown_timer2, getHours(), getMinutes());
					CharSequence extText = Html.fromHtml(getResources().getString(R.string.non_tl_version_link_text2));
					remoteControlExtStatusText.setText(extText);
					remoteControlExtStatusText.setVisibility(View.VISIBLE);
				} else {
					remoteControlExtStatusText.setVisibility(View.GONE);
				}
				remoteControlStatusText.setText(text);
				remoteControlStatusText.setTextColor(textColor.getColor());
			}
			
		});
	}
	
	private void showStatus(final String text, final TextColors textColor) {
		if (rootView == null) {
			return;
		}
		final TextView remoteControlStatusText = (TextView) rootView.findViewById(R.id.remote_control_status_text);
		remoteControlStatusText.post(new Runnable() {

			@Override
			public void run() {
				remoteControlStatusText.setText(text);
				remoteControlStatusText.setTextColor(textColor.getColor());
			}
			
		});
	}
	
	private void updatePingLight(final boolean isCommunicationError, final boolean isTimeout) {
		Activity activity = getActivity();
		if (activity == null) {
			Log.w(TAG, "no-parent-activity");
			return;
		}
		final View internetStatusButton = activity.findViewById(R.id.link_status_button);
		if (internetStatusButton == null) {
			Log.w(TAG, "internet-status-button-not-found");
			return;
		}
		internetStatusButton.post(new Runnable() {

			@Override
			public void run() {
				if (getActivity() == null) {
					return;
				}
				
				if (isCommunicationError || isTimeout) {
					internetStatusButton.setSelected(true);
					ImageView remoteMobileAgentStateImage = (ImageView) getViewFromRootViewById(R.id.remote_mobile_agent_state_image);
					Iprbar iprBar = (Iprbar) getViewFromRootViewById(R.id.remote_mobile_agent_state_iprbar);
					if (remoteMobileAgentStateImage != null) {
						remoteMobileAgentStateImage.setImageDrawable(getResources().getDrawable(R.drawable.car_nolink));
						iprBar.setVisibility(View.GONE);
						remoteMobileAgentStateImage.setVisibility(View.VISIBLE);
					} else {
						Log.d(TAG, "remote-mobile-agent-state-image-not-found");
					}
					showStatus(R.string.connection_error, TextColors.GRAY);
				} else {
					internetStatusButton.setSelected(false);
				}
			}
			
		});
	}
	
	private void alarm() {
		if (getA().isAlarmActive()) {
			return;
		}
		vibro();
		beep();
		led();
		getA().setAlarmActive(true);
	}
	
	private void vibro() {
    	try {
    		Vibrator vibro = (Vibrator) getA().getSystemService(Context.VIBRATOR_SERVICE);
    		vibro.vibrate(50L);
    		Thread.sleep(200L);
    		vibro.vibrate(50L);
    		Thread.sleep(200L);
    		vibro.vibrate(50L);
    		Thread.sleep(200L);
    		vibro.vibrate(75L);
    		Thread.sleep(400L);
    		vibro.vibrate(75L);
    		Thread.sleep(400L);
    		vibro.vibrate(75L);
    		Thread.sleep(400L);
    		vibro.vibrate(50L);
    		Thread.sleep(200L);
    		vibro.vibrate(50L);
    		Thread.sleep(200L);
    		vibro.vibrate(50L);
    		Thread.sleep(200L);
    	} catch (InterruptedException e) {
    		Log.wtf(TAG,  e);
    	}
	}
	
	private void led() {
		NotificationManager notificationManager = (NotificationManager) getA()
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notif = new Notification();
		notif.ledARGB = 0xFFFF0000; // Red
		notif.flags = Notification.FLAG_SHOW_LIGHTS;
		notif.ledOnMS = 200;
		notif.ledOffMS = 200;
		notificationManager.notify(LED_NOTIFICATION_ID, notif);
	}
	
	private void beep() {
		WatchdogService watchdogService = getA().getWatchdogService();
		if (watchdogService == null) {
			return;
		}
		watchdogService.beepAlarmAlarm();
	}

	private View getViewFromRootViewById(int viewId) {
		return rootView != null ?
				rootView.findViewById(viewId)
				: null;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if (!hidden) {
			refresh(getView());
		} else {
			saveMapPosition();			
		}
		super.onHiddenChanged(hidden);
	}

	private void refresh(View view) {
		if (view == null) {
			return;
		}
		this.rootView = view;
		
		remoteCarState = getA().getCarState();
		remoteAppState = getA().getAppState();
		
		final Iprbar iprBar = (Iprbar) view.findViewById(R.id.remote_mobile_agent_state_iprbar);
		
		//Gps, signal-strength and battery-pct
		final ImageView stateBlockSignalStrengthImage = (ImageView) view.findViewById(R.id.state_block_ss_image);
		final ImageView stateBlockGpsImage = (ImageView) view.findViewById(R.id.state_block_gps_image);
		final ImageView stateBlockBatteryLevelImage = (ImageView) view.findViewById(R.id.state_block_bl_image);
		
		final ImageView remoteMobileAgentStateImage = (ImageView) view.findViewById(R.id.remote_mobile_agent_state_image);
		remoteMobileAgentStateImage.setVisibility(View.GONE);//Undefined remote agent state
		
		final ControlModsEnum controlMode = getA().getControlMode();
		
		if (controlMode != ControlModsEnum.INTERNET) {
			stateBlockGpsImage.setVisibility(View.INVISIBLE);
		}

		remoteMobileAgentStateImage.setImageDrawable(getResources().getDrawable(R.drawable.car_nolink));
		
		//Show percent in battery charge
		stateBlockBatteryLevelImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mas == null) {
					return;
				}
				if (getActivity() == null) {
					return;
				}
				float batteryPct = Math.round(mas.getBatteryPct());
				String message = getResources().getString(R.string.battery_level_in_percents, batteryPct);
				if (batteryPct <= 0) {
					return;
				}
				//Play low-battery sound
				if (batteryPct < LOW_BATTERY_TRESHOLD && !isCharging(mas) ) {
					beepLowBattery();
				}
				if (batteryPct < 30) { 
					showStatus(message, TextColors.ALARMS);
				} else if (batteryPct < 70) { 
					showStatus(message, TextColors.GRAY);
				} else {
					showStatus(message, TextColors.NORM);
				}
			}
			
		});		
		
		final ImageButton remoteControlButton = (ImageButton) view.findViewById(R.id.remote_control_main);
		remoteControlButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Reset memory for status after user action.
				remoteAgentStatusDescription = null;
				switch(controlMode) {
				case INTERNET:
					firePressMainButtonInBasicMode(remoteControlButton, iprBar, remoteMobileAgentStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage);
					break;
				case PHONE_CALLS:
				case SMS:
					firePressMainButtonInReserveMode(remoteControlButton, iprBar, remoteMobileAgentStateImage, stateBlockSignalStrengthImage, stateBlockGpsImage, stateBlockBatteryLevelImage);
					break;
				}
				timerExpired = new Date(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(2) + TimeUnit.MINUTES.toMillis(1));
				getA().setDate(COUNTDOWN_TIMER_EXPIRED_DATE, timerExpired);
				restartCountdownTimer();
			}
		});
		
		//Make photo buttons
		final ImageButton makePhotoBackCamButton = (ImageButton) view.findViewById(R.id.remote_control_fragment_$make_photo_back$_image_button);
		final ImageButton makePhotoFrontCamButton = (ImageButton) view.findViewById(R.id.remote_control_fragment_$make_photo_front$_image_button);
		makePhotoFrontCamButton.setVisibility(View.GONE);
		//Delay request to remote device (num cameras) for detect visibility
		View.OnClickListener onClickCamButtonListener = new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ImageButton makePhotoButton = (ImageButton) v;
				int cameraId = CameraInfo.CAMERA_FACING_BACK;
				switch(v.getId()) {
				case R.id.remote_control_fragment_$make_photo_back$_image_button:
					cameraId = CameraInfo.CAMERA_FACING_BACK;
					break;
				case R.id.remote_control_fragment_$make_photo_front$_image_button:
					cameraId = CameraInfo.CAMERA_FACING_FRONT;
					break;
				}
				if (makePhotoButton.isSelected()) {
					return;
				}
				makePhotoButton.setSelected(true);
				ControlModsEnum controlMode = getA().getControlMode();
				switch (controlMode) {
			    case INTERNET:
			    	startParsingPreviewImageChunks();
			    	makePhotoCommand(makePhotoButton, cameraId);
			    	break;
			    case PHONE_CALLS:
			    case SMS:
			    	if (!(getActivity() instanceof MainActivity)) {
			    		Log.w(TAG, "Current activity not main and modal message not show");
			    		break;
			    	}
			    	getA().showMessage(getResources().getString(R.string.function_not_available_in_phone_calls_mode), null, null);
					makePhotoButton.setSelected(false);
			    	break;
				}
			}
		};
		makePhotoBackCamButton.setOnClickListener(onClickCamButtonListener);
		makePhotoFrontCamButton.setOnClickListener(onClickCamButtonListener);
		//Change visibility for front-cam button.
		updateFrontCamButtonVisibility(makePhotoFrontCamButton);
		
		//Remote listen button
		final ImageButton remoteListenButton = (ImageButton) view.findViewById(R.id.remote_listen);
		remoteListenButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (remoteListenButton.isSelected()) {
					return;
				}
				remoteListenButton.setSelected(true);
				ControlModsEnum controlMode = getA().getControlMode();
				switch (controlMode) {
			    case INTERNET:
			    	getA().runPhoneTask(new Runnable() {

						@Override
						public void run() {
							final long remoteMobileAgentId = getA().getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
							getA().getUcClient().execCommandAsync(remoteMobileAgentId, UserCommandTypesEnum.RL, null, new RequestCallback() {

								@Override
								public boolean onResponse(Object returnValue,
										boolean isCommunicationError,
										boolean isTimeout) {
									if (isCommunicationError || isTimeout) {
										showStatus(R.string.connection_error, TextColors.GRAY);
									} else if (returnValue == null) {
										showStatus(R.string.bad_phone_number, TextColors.GRAY);
									} else {
										showStatus(R.string.expect_call, TextColors.GRAY);
									}
									safetySelected(remoteListenButton);
									updatePingLight(isCommunicationError, isTimeout);
									return false;
								}

								@Override
								public void onSendRequest(long requestId,
										boolean longTtlRequest,
										boolean isCommunicationError, boolean isTimeout) {
									updatePingLight(isCommunicationError, isTimeout);
									if (isCommunicationError || isTimeout) {
										showStatus(R.string.connection_error, TextColors.GRAY);
									}
								}

							}, false, 1);
						}
			    		
			    	});
			    	break;
			    case PHONE_CALLS:
			    case SMS:
					String phoneNumber = getA().getString(getA().getSelectedDeviceForRemoteControlPhoneNumberPrevKey(), "");
					if (phoneNumber.isEmpty()) {
						getA().makeToastWarning(R.string.no_phone_number_for_remote_agent);
						return;
					}
					String message = getResources().getString(R.string.remote_cmd_remote_listen);
			    	WatchdogService.sendSMSInteractively(getA(), phoneNumber, message);
					remoteListenButton.setSelected(false);
			    	break;
				}
			}
		});
		
		//State block
//		final Logo stateBlockSignalStrengthLogo = (Logo) view.findViewById(R.id.state_block_ss_logo);
//		stateBlockSignalStrengthLogo.setLogo(R.drawable.state_block_ss_1, 53, 35);
//		final Logo stateBlockGpsLogo = (Logo) view.findViewById(R.id.state_block_gps_logo);
//		stateBlockGpsLogo.setLogo(R.drawable.state_block_gps_off, 35, 35);
//		final Logo stateBlockBatteryLevelLogo = (Logo) view.findViewById(R.id.state_block_bl_logo);
//		stateBlockBatteryLevelLogo.setLogo(R.drawable.state_block_bl_2, 46, 26);
		
		//Restore link after unexpected reboot.
		if (controlMode == ControlModsEnum.INTERNET) {
			lastLongttlRequestId = getA().getLong(LAST_LONGTTL_REQUEST_ID, ISid.INVALID_COMPONENT_SESSIONS_ID);
		}
		//Restore listener after unexpected reboot
		restoreRemoteAgentListener();
		
		//Update remote mobile agent status
		updateRemoteAgentState();
		
		final TextView remoteControlExtStatusText = (TextView) view.findViewById(R.id.remote_control_ext_status_text);
		remoteControlExtStatusText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (!isShowCountdownTimer()) {
					return;
				}
				//final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
				try {
				    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=ru.hipdriver.watchdog")));
				} catch (android.content.ActivityNotFoundException anfe) {
				    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=ru.hipdriver.watchdog")));
				}
			}
		});
	}

	private void updateFrontCamButtonVisibility(
			final ImageButton makePhotoFrontCamButton) {
		getA().runPhoneTask(new Runnable() {
			
			@Override
			public void run() {
				ControlModsEnum controlMode = getA().getControlMode();
				switch (controlMode) {
			    case INTERNET:
			    	break;
			    case PHONE_CALLS:
			    case SMS:
			    	return;
				}
				final long remoteMobileAgentId = getA().getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
				Integer numberOfRemoteCameras = getA().getInteger(NUMBER_OF_REMOTE_CAMERAS, remoteMobileAgentId);
				if (numberOfRemoteCameras != null) {
					parseNumberOfRemoteCameras(makePhotoFrontCamButton,
							numberOfRemoteCameras);
					return;
				}
				getA().getUcClient().execCommandAsync(remoteMobileAgentId, UserCommandTypesEnum.GI, null, new RequestCallback() {

					@Override
					public boolean onResponse(Object returnValue,
							boolean isCommunicationError,
							boolean isTimeout) {
						if (!(returnValue instanceof ru.hipdriver.android.app.sslucs.DeviceInfo)) {
							return false;
						}
						ru.hipdriver.android.app.sslucs.DeviceInfo di = (ru.hipdriver.android.app.sslucs.DeviceInfo) returnValue;
						int numberOfRemoteCameras = di.getCamerasCount();
						getA().setInteger(NUMBER_OF_REMOTE_CAMERAS, remoteMobileAgentId, numberOfRemoteCameras);
						parseNumberOfRemoteCameras(makePhotoFrontCamButton,
								numberOfRemoteCameras);
						return false;
					}

					@Override
					public void onSendRequest(long requestId,
							boolean longTtlRequest,
							boolean isCommunicationError, boolean isTimeout) {
					}

				}, true, 6);
			}

		});
	}
	
	private void parseNumberOfRemoteCameras(
			final ImageButton makePhotoFrontCamButton,
			Integer numberOfRemoteCameras) {
		if (numberOfRemoteCameras < 2) {
			return;
		}
		makePhotoFrontCamButton.post(new Runnable() {
			@Override
			public void run() {
				makePhotoFrontCamButton.setVisibility(View.VISIBLE);
			}
		});
		return;
	}

	protected void restartCountdownTimer() {
		TimerTask oldTimerTask = countdownTimerTaskRef.getAndSet(new TimerTask() {

			@Override
			public void run() {
				View view = getView();
				if (view == null) {
					return;
				}
				if (!getUserVisibleHint()) {
					return;
				}
				if (!isShowCountdownTimer()) {
					return;
				}
				//Show warning message about car_alarms
				if (getA().getBoolean(FIRST_CLICK_ON_MAIN_BUTTON, true)) {
					CharSequence message = Html.fromHtml(getResources().getString(R.string.message_after_first_click_on_main_button));
					getA().showMessage(message, null, new ActiveText(R.string.non_tl_version_link_text, "market://details?id=ru.hipdriver.watchdog", "http://play.google.com/store/apps/details?id=ru.hipdriver.watchdog"));

					getA().setBoolean(FIRST_CLICK_ON_MAIN_BUTTON, false);

				}
				
				if (System.currentTimeMillis() > timerExpired.getTime()) {
					return;
				}
				
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						showStatus(R.string.watch, TextColors.NORM);
					}
					
				});
			}
			
		});
		if (oldTimerTask != null) {
			oldTimerTask.cancel();
			getTimer().purge();
		}
		long updateInterval = TimeUnit.MINUTES.toMillis(1);
		getTimer().schedule(countdownTimerTaskRef.get(), updateInterval, updateInterval);
	}

	private void beepLowBattery() {
		HipdriverApplication hipdriverApplication = getA();
		if (hipdriverApplication == null) {
			return;
		}
		WatchdogService watchdogService = hipdriverApplication.getWatchdogService();
		if (watchdogService == null) {
			return;
		}
		watchdogService.beepLowBattery();
	}

	@Override
	public void changeConnectionState(boolean isConnectingOrConnected) {
		updateRemoteAgentState();
	}
	
	boolean parsingPreviewImage;
	ByteBuffer imageData;
	
	void startParsingPreviewImageChunks() {
		if (parsingPreviewImage) {
			return;
		}
		parsingPreviewImage = true;
		imageData = ByteBuffer.allocate(65535);
	}
	void stopParsingPreviewImageChunks() {
		if (!parsingPreviewImage) {
			return;
		}
		byte[] data = new byte[imageData.position()];
		imageData.position(0);
		imageData.get(data, 0, data.length);
		//
		//FileTools.writeIntoFile("/mnt/sdcard/tmp/image.jpeg", data);
		//
		showPreviewImage(data);
		parsingPreviewImage = false;
		imageData.clear();
		imageData = null;
	}
	private void showPreviewImage(byte[] picture) {
		FragmentActivity activity = getActivity();
		if (activity == null) {
			Log.w(TAG, "can-not-show-preview-image[activity is null]");
		}
		Log.d(TAG, "show-image-data");
		showStatus("" + picture.length, TextColors.NORM);
		Intent intent = new Intent(activity, ShowPictureActivity.class);
		intent.putExtra(EXTRA_PICTURE_DATA, picture);
		startActivity(intent);
		
	}

	void parsePreviewImageChunk(byte[] imageChunk) {
		//Check final chunk
		if (imageChunk.length == 0) {
			stopParsingPreviewImageChunks();
			return;
		}
		putImageData(imageChunk);
	}

	private void putImageData(byte[] imageChunk) {
		if (imageChunk == null) {
			return;
		}
		if (imageChunk.length == 0) {
			return;
		}
		if (imageData == null) {
			Log.w(TAG, "can-not-put-image-data[image-data is null]");
			return;
		}
		imageData.put(imageChunk);
	}
	
	private int getHours() {
		long timeRemain = timerExpired.getTime() - System.currentTimeMillis();
		if (timeRemain > 0) {
			return (int) (timeRemain / TimeUnit.HOURS.toMillis(1));
		}
		return 0;
	}
	
	private int getMinutes() {
		long timeRemain = timerExpired.getTime() - System.currentTimeMillis();
		if (timeRemain > 0) {
			return (int) ((timeRemain % TimeUnit.HOURS.toMillis(1)) / TimeUnit.MINUTES.toMillis(1));
		}
		return 0;
	}
	
	protected void updateRemoteMobileAgentSettings() {
		long remoteMobileAgentId = getA().getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
		if (remoteMobileAgentId == IMobileAgent.INVALID_ID) {
			//TODO: Disable iprBar
		} else {
			UITaskFactory.getMap(getA(), new After() {
	
				@Override
				public void success(Object returnValue) {
					Properties properties = (Properties) returnValue;
					Short shortNumber = getShort(properties, IMobileAgentProfile.CONTROL_MODE_ID);
					if (shortNumber == null) {
						return;
					}
					short controlModeIdShort = shortNumber;
					ControlModsEnum currentControlModeForRemoteAgent = DBLinks.unlinkControlMode(controlModeIdShort);
					//Get map provider
					shortNumber = getShort(properties, IMobileAgentProfile.MAP_PROVIDER_ID);
					if (shortNumber == null) {
						return;
					}
					short mapProviderIdShort = shortNumber;
					//Get remote agent version
					String currentVersionForRemoteAgent = properties.getProperty(IMobileAgent.MOBILE_AGENT_VERSION_NAME_KEY);
					//Get is remote agent time limited
					Boolean isTimeLimited = getBoolean(properties, IUserLimit.TIME_LIMITED);
					showCountdownTimer = isTimeLimited(currentVersionForRemoteAgent, isTimeLimited); 
					
					onSwitchToControlMode(currentControlModeForRemoteAgent);
					if (getA().getControlMode() != currentControlModeForRemoteAgent) {
						getA().setControlMode(currentControlModeForRemoteAgent);
						setupConnectorToChatServer(currentControlModeForRemoteAgent);
					}
				}
	
				@Override
				public void failure(int returnCode, String failureReason) {
				}
				
			}, remoteMobileAgentId);
		}
	}

	protected boolean isTimeLimited(
			String versionForRemoteAgent, Boolean isTimeLimited) {
		if (versionForRemoteAgent == null) {
			return TextTools.isTimeLimited(Hardwares.getApplicationVersion(getA()));
		}
		if (isTimeLimited == null) {
			return TextTools.isTimeLimited(Hardwares.getApplicationVersion(getA()));
		}
		if (!TextTools.isTimeLimited(versionForRemoteAgent)) {
			return false;
		}
		if (!isTimeLimited) {
			return false;
		}
		return true;
	}

	protected void onSwitchToControlMode(
			ControlModsEnum controlMode) {
		if (rootView == null) {//It's a first refresh, nothing do
			return;
		}
		if (getActivity() == null) {//No activity - no update
			return;
		}
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				refresh(rootView);
			}
			
		});
	}

	private void setupConnectorToChatServer(ControlModsEnum controlMode) {
		if (controlMode == null) {
			return;
		}
		switch (controlMode) {
		case INTERNET:
			getA().makeUserCommandsSender();
			break;
		case PHONE_CALLS:
		case SMS:
			getA().releaseUserCommandsSender();
			break;
		}
	}
	
	protected Short getShort(Properties properties, String keyName) {
		Object shortNumberObject = properties.get(keyName);
		if (shortNumberObject == null) {
			return null;
		}
		String shortNumberText = String.valueOf(shortNumberObject);
		short shortNumber = -1;
		try {
			shortNumber = Short.parseShort(shortNumberText);
		} catch (NumberFormatException e) {
			return null;
		}
		return shortNumber;
	}
	
	protected Boolean getBoolean(Properties properties, String keyName) {
		Object booleanValueObject = properties.get(keyName);
		if (booleanValueObject == null) {
			return null;
		}
		String booleanValueText = String.valueOf(booleanValueObject);
		boolean booleanValue = false;
		try {
			booleanValue = Boolean.parseBoolean(booleanValueText);
		} catch (NumberFormatException e) {
			return null;
		}
		return booleanValue;
	}

	private boolean isShowCountdownTimer() {
		if (!showCountdownTimer) {
			return false; 
		}
		if (remoteAppState != AppStatesEnum.CAC) {
			return false;
		}
		if (remoteCarState != CarStatesEnum.U) {
			return false;
		}
		if (System.currentTimeMillis() > timerExpired.getTime()) {
			return false;
		}
		return true;
	}

	private void makePhotoCommand(
			final ImageButton makePhotoButton, final int cameraId) {
		getA().runPhoneTask(new Runnable() {

			@Override
			public void run() {
				final long remoteMobileAgentId = getA().getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
				getA().getUcClient().execCommandAsync(remoteMobileAgentId, UserCommandTypesEnum.MP, NumericTools.intToBytes(cameraId), new RequestCallback() {

					@Override
					public boolean onResponse(Object returnValue,
							boolean isCommunicationError,
							boolean isTimeout) {
						if (returnValue instanceof byte[]) {
							parsePreviewImageChunk((byte[]) returnValue);
							return false;
						}
						if (isCommunicationError || isTimeout) {
							showStatus(R.string.connection_error, TextColors.GRAY);
						} else if (returnValue == null) {
							showStatus(R.string.bad_email, TextColors.GRAY);
						} else {
							showStatus(R.string.photo_sended, TextColors.GRAY);
						}
						safetySelected(makePhotoButton);
						updatePingLight(isCommunicationError, isTimeout);
						return false;
					}

					@Override
					public void onSendRequest(long requestId,
							boolean longTtlRequest,
							boolean isCommunicationError, boolean isTimeout) {
						updatePingLight(isCommunicationError, isTimeout);
						if (isCommunicationError || isTimeout) {
							showStatus(R.string.connection_error, TextColors.GRAY);
						}
					}

				}, true, 6);
			}
			
		});
	}
	
}
