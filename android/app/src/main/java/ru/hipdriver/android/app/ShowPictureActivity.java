package ru.hipdriver.android.app;

import java.io.ByteArrayInputStream;

import ru.hipdriver.android.util.TextTools;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import ru.hipdriver.android.app.R;

public class ShowPictureActivity extends HipdriverActivity {

	protected static final String TAG = TextTools
			.makeTag(ShowPictureActivity.class);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_picture_screen);
		Intent intent = getIntent();
		final byte[] pictureData = intent.getByteArrayExtra(RemoteControlFragment.EXTRA_PICTURE_DATA);
		if (pictureData == null) {
			return;
		}
		//BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(pictureData)); //decodeByteArray(pictureData, 0, pictureData.length, options);
        ImageView previewImage = (ImageView) findViewById(R.id.preview_image);
        previewImage.setImageBitmap(bitmap);
        
        OnClickListener onClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		};
		previewImage.setOnClickListener(onClickListener);
        previewImage.invalidate();
        
        View previewBackground = findViewById(R.id.preview_background);
        previewBackground.setOnClickListener(onClickListener);
	}

}
