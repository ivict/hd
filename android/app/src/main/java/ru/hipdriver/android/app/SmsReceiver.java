package ru.hipdriver.android.app;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.support.CarStatesEnum;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsReceiver extends HipdriverBroadcastReceiver {
	
	private static final String TAG = TextTools.makeTag(SmsReceiver.class);

	@Override
	public void onReceive(Context context, Intent intent) {
		if (context == null) {
			return;
		}
		if (intent == null) {
			return;
		}
		
		// Retrieves a map of extended data from the intent.
		final Bundle bundle = intent.getExtras();
		if (bundle == null) {
			return;
		}
		
		final Object[] pdusObj = (Object[]) bundle.get("pdus");
		
		if (pdusObj == null) {
			return;
		}
		
		final HipdriverApplication hipdriverApplication = getA(context);
		
		if (hipdriverApplication == null) {
			return;
		}
		
        //Check sign-up
        if (!hipdriverApplication.isSignup()) {
        	return;
        }
		
		hipdriverApplication.runPhoneTask(new Runnable() {

			@Override
			public void run() {
				final SmsMessage[] messages = new SmsMessage[pdusObj.length];
		        for (int i = 0; i < pdusObj.length; i++) {
		            SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
		            messages[i] = currentMessage;
		        }
		        
				String phoneNumber = hipdriverApplication.getString(MainActivity.PHONE_NUMBER_EDITOR, "");
				String extractedPhoneNumber = TextTools
						.extractLastSixNumbersFromPhoneNumber(phoneNumber);
		        
		        for (SmsMessage currentMessage : messages) {
		            String smsPhoneNumber = currentMessage.getDisplayOriginatingAddress();
		            String extractedSmsPhoneNumber = TextTools.extractLastSixNumbersFromPhoneNumber(smsPhoneNumber);

		            String message = currentMessage.getDisplayMessageBody();
		            String firstNotEmptyLineInMessage = TextTools.getFirstNotEmptyLineInMessage(message);
		            
		            if (extractedPhoneNumber.equals(extractedSmsPhoneNumber)) {
		            	processSms(hipdriverApplication, phoneNumber, firstNotEmptyLineInMessage);
		            	return;
		            }
		            if (firstNotEmptyLineInMessage == null) {
		            	continue;
		            }
		        	String extractedSmsBodyPhoneNumber = TextTools
		        			.extractLastSixNumbersFromPhoneNumber(firstNotEmptyLineInMessage);
		            if (extractedPhoneNumber.equals(extractedSmsBodyPhoneNumber)) {
		              	processSms(hipdriverApplication, phoneNumber, firstNotEmptyLineInMessage);
		               	return;
		            }
		        }		
			}
			
		});
	}

	private void processSms(HipdriverApplication hipdriverApplication, String phoneNumber, String firstNotEmptyLineInMessage) {
		if (firstNotEmptyLineInMessage == null) {
			makeCalForListenMic(hipdriverApplication, phoneNumber);
		}
		if (firstNotEmptyLineInMessage.isEmpty()) {
			makeCalForListenMic(hipdriverApplication, phoneNumber);
		}
		//Extract command
		char cmd = firstNotEmptyLineInMessage.charAt(0);
		final IUserCommandsExecutor userCommandsExecutor = hipdriverApplication.getUserCommandsExecutor();
		if (userCommandsExecutor == null) {
			hipdriverApplication.switchControlModeIfChanged();
			return;
		}
		switch (cmd) {
		case '*':
		case '1':
			userCommandsExecutor.signin();
			//Check billing key
			if (hipdriverApplication.getDate(HipdriverApplication.EXPIRED_DATE).before(new Date())) {
				return;
			}
			userCommandsExecutor.switchOnWatch();
			break;
		case '#':
		case '0':
			hipdriverApplication.setCarState(CarStatesEnum.U);
			hipdriverApplication.setFirstDetectionCarState(CarStatesEnum.U);
			Log.d(TAG, "switch-off-watch[sms-command-exec]");				
			userCommandsExecutor.switchOffWatch();			
			break;
		default:
			makeCalForListenMic(hipdriverApplication, phoneNumber);			
		}
		hipdriverApplication.switchControlModeIfChanged();
	}
	
	private void makeCalForListenMic(HipdriverApplication hipdriverApplication, String phoneNumber) {
		hipdriverApplication.waitForCellNetworkConnection(TimeUnit.MINUTES.toMillis(3));
		WatchdogService.makeOutboundCall(hipdriverApplication, phoneNumber, 0);
	}

}
