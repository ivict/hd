package ru.hipdriver.android.app.sslucs;

import java.util.HashMap;
import java.util.Map;

public class Invariants {

	private Invariants() { }

	final static String KEY_STORE = "key-store";
	final static String KEY_STORE_HTL = "key-store-htl";
	// TODO: make more complex scheme, for example get password from server
	final static String VERIFY_PASSWORD = "Xpdj8JDtLf96";
	final static String LOCALHOST_ONE_DAY_CERT = "localhost-one-day-certificat";

	static final String STUN_SERVER1 = "kernel.hipdriver.me";
	static final int STUN_SERVER1_PORT = 3478;
	static final String STUN_SERVER2 = "stun.hipdriver.ru";
	static final int STUN_SERVER2_PORT = 5766;
	
	static final Map<String, Integer> STUN_SERVERS;
	static {
		STUN_SERVERS = new HashMap<String, Integer>();
		STUN_SERVERS.put(STUN_SERVER1, STUN_SERVER1_PORT);
		STUN_SERVERS.put(STUN_SERVER2, STUN_SERVER2_PORT);
	}

	static final String BC = "BC";// org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME;
	
	static final String PING_REQUEST = "PING";
	static final String PING_ANSWER = "PONG";
	
}
