package ru.hipdriver.android.app;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ru.hipdriver.android.util.SizedDataSource;
import ru.hipdriver.i.IMobileAgent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;

import ru.hipdriver.android.app.R;


/**
 * Activity for getting photos and thumbnails
 * @author valentina
 *
 */
public class PhotoActivity extends HipdriverActivity {
	static final int REQUEST_IMAGE_CAPTURE = 1;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		dispatchTakePictureIntent();
	}
	
	private void dispatchTakePictureIntent() {
	    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
	        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
	    }
	}		

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
	        Bundle extras = data.getExtras();
	        Bitmap imageBitmap = (Bitmap) extras.get("data");
	        
			final String email = getA().getString(IMobileAgent.ALERT_EMAIL_KEY, "");
			String mobileAgentName = getA().getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
			String header = getA().getResources().getString(R.string.email_message_subj_photo, mobileAgentName);
			String body = getA().getResources().getString(R.string.email_message_body_photo);
			int messageIdentity = UITaskFactory.sendEmail(getA(), email, header, body, null, 1);
			UITaskFactory.sendAtachsToEmail(getA(), messageIdentity, 0, makeImageSource(imageBitmap, "photo1.png", 64, 64));
	    }
		finish();
	}

	private SizedDataSource makeImageSource(final Bitmap imageBitmap, final String name, final int maxHeight, final int maxWidth) {
		return new SizedDataSource() {

			@Override
			public String getContentType() {
				return null;
			}

			@Override
			public InputStream getInputStream() throws IOException {
				Float width  = new Float(imageBitmap.getWidth());
				Float height = new Float(imageBitmap.getHeight());
				Float ratio = width/height;
				Bitmap scaledBitmap = Bitmap.createScaledBitmap(imageBitmap, (int)(maxHeight*ratio), maxHeight, false);
				//int padding = (maxWidth - scaledBitmap.getWidth())/2;
				
				//imageView.setPadding(padding, 0, padding, 0);
				//imageView.setImageBitmap(imageBitmap);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();  
				scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
				byte[] byteArray = baos.toByteArray();
				return new ByteArrayInputStream(byteArray);
			}

			@Override
			public String getName() {
				return name;
			}

			@Override
			public OutputStream getOutputStream() throws IOException {
				throw new IllegalStateException("Input only datasource!");
			}

			@Override
			public int getInputStreamSize() {
				//TODO: fix real size
				return 0;
			}
			
		};
	}

}
