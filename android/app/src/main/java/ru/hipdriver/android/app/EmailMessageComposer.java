package ru.hipdriver.android.app;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.j.EmailMessage;
import ru.hipdriver.j.EmailMessageAtach;
import ru.hipdriver.j.EmailMessageAtachFragment;

import android.util.Log;

/**
 * Composer for e-mail
 * @author valentina
 *
 */
public class EmailMessageComposer {
	
	private static final String TAG = TextTools.makeTag(EmailMessageComposer.class);
	
	private final String from;
	private final String to;
	private final String subject;
	private final String body;
	private final ObjectMapper om;
	private final int identity;
	private final int atachsCount;

	public EmailMessageComposer(int identity, String from, String to, String subject,
			String body, int atachsCount) {
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.body = body;
		this.identity = identity;
		this.atachsCount = atachsCount;
		this.om = new ObjectMapper();
	}

	public byte[] convertToJson() {
		try {
			EmailMessage emailMessage = new EmailMessage();
			emailMessage.setFrom(from);
			emailMessage.setTo(to);
			emailMessage.setSubject(subject);
			emailMessage.setBody(body);
			emailMessage.setIdentity(identity);
			emailMessage.setAtachsCount(atachsCount);
			
			String message = om.writeValueAsString(emailMessage);
			return message.getBytes("UTF8");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return null;
	}

}

class EmailMessageAtachComposer {
	
	private static final String TAG = TextTools.makeTag(EmailMessageAtachComposer.class);
	
	private final int identity;
	private final int messageIdentity;
	private final int atachNumber;
	private final String contentType;
	private final String name;
	private final int fragmentsCount;
	private final ObjectMapper om;

	public EmailMessageAtachComposer(int identity, int messageIdentity,
			int atachNumber, String contentType, String name,
			int fragmentsCount) {
		this.identity = identity;
		this.messageIdentity = messageIdentity;
		this.atachNumber = atachNumber;
		this.contentType = contentType;
		this.name = name;
		this.fragmentsCount = fragmentsCount;
		this.om = new ObjectMapper();
	}

	public byte[] convertToJson() {
		try {
			EmailMessageAtach emailMessageAtach = new EmailMessageAtach();
			emailMessageAtach.setIdentity(identity);
			emailMessageAtach.setMessageIdentity(messageIdentity);
			emailMessageAtach.setAtachNumber(atachNumber);
			emailMessageAtach.setContentType(contentType);
			emailMessageAtach.setName(name);
			emailMessageAtach.setFragmentsCount(fragmentsCount);
			
			String message = om.writeValueAsString(emailMessageAtach);
			return message.getBytes("UTF8");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return null;
	}

}

class EmailMessageAtachFragmentComposer {
	
	private static final String TAG = TextTools.makeTag(EmailMessageAtachFragmentComposer.class);
	
	private final int identity;
	private final int messageAtachIdentity;
	private final int fragmentNumber;
	private final byte[] content;
	private final ObjectMapper om;

	public EmailMessageAtachFragmentComposer(int identity, int messageAtachIdentity, int fragmentNumber, byte[] content) {
		this.identity = identity;
		this.messageAtachIdentity = messageAtachIdentity;
		this.fragmentNumber = fragmentNumber;
		this.content = content;
		this.om = new ObjectMapper();
	}

	public byte[] convertToJson() {
		try {
			EmailMessageAtachFragment emailMessageAtachFragment = new EmailMessageAtachFragment();
			emailMessageAtachFragment.setIdentity(identity);
			emailMessageAtachFragment.setMessageAtachIdentity(messageAtachIdentity);
			emailMessageAtachFragment.setFragmentNumber(fragmentNumber);
			emailMessageAtachFragment.setContent(content);
			
			String message = om.writeValueAsString(emailMessageAtachFragment);
			return message.getBytes("UTF8");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return null;
	}

}

