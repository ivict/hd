package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.support.ControlModsEnum;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import ru.hipdriver.android.app.R;

public class ControlModsEnumControl implements OnClickListener {
	
	private static final String TAG = TextTools.makeTag(ControlModsEnumControl.class);
	private final RadioButton internetControlModeRadioButton;
	private final RadioButton phoneCallsControlModeRadioButton;
	
	private final HipdriverApplication hipdriverApplication;

	public ControlModsEnumControl(HipdriverApplication hipdriverApplication, View view) {
		this.hipdriverApplication = hipdriverApplication; 
		
		internetControlModeRadioButton = (RadioButton) view.findViewById(R.id.internet_control_mode_radiobutton);
		phoneCallsControlModeRadioButton = (RadioButton) view.findViewById(R.id.phone_calls_control_mode_radiobutton);

		phoneCallsControlModeRadioButton.setOnClickListener(this);
		internetControlModeRadioButton.setOnClickListener(this);		
		
		//Toggle current mode
	    afterSwitchToControlMode(getA().getControlMode());
	}

	protected HipdriverApplication getA() {
		return hipdriverApplication;
	}

	@Override
	public void onClick(View view) {
		int viewId = view.getId();
		if (viewId != R.id.internet_control_mode_radiobutton &&
				viewId != R.id.phone_calls_control_mode_radiobutton) {
			return;
		}
		ControlModsEnum prevControlMode = getA().getControlMode();
	    // Check which radio button was clicked
	    switch(viewId) {
	    case R.id.internet_control_mode_radiobutton:
	    	getA().setControlMode(ControlModsEnum.INTERNET);
	    	break;
	    case R.id.phone_calls_control_mode_radiobutton:
	    	getA().setControlMode(ControlModsEnum.PHONE_CALLS);
	    	break;
	    }
	    boolean isControlModeChanged = getA().getControlMode() != prevControlMode;
	    if (isControlModeChanged) {
	    	getA().synchronizeCurrentControlModeWithServer();
	    }
	    afterSwitchToControlMode(getA().getControlMode());
	}
	
	protected void afterSwitchToControlMode(ControlModsEnum targetControlMode) {
		if (targetControlMode == null) {
			Log.w(TAG, "null-control-mode-but-result-ok");
			return;
		}
		if (internetControlModeRadioButton == null) {
			Log.w(TAG, String.format("can-not-find-view[%s]", R.id.internet_control_mode_radiobutton));
			return;
		}
		switch (targetControlMode) {
		case INTERNET:
			//resetFocus();

			getA().setControlMode(ControlModsEnum.INTERNET);

			phoneCallsControlModeRadioButton.post(new Runnable() {

				@Override
				public void run() {
					phoneCallsControlModeRadioButton.setChecked(false);
					internetControlModeRadioButton.toggle();
				}
				
			});
			break;
		case PHONE_CALLS:
		case SMS:
			//resetFocus();

			getA().setControlMode(ControlModsEnum.PHONE_CALLS);

			phoneCallsControlModeRadioButton.post(new Runnable() {

				@Override
				public void run() {
					internetControlModeRadioButton.setChecked(false);
					phoneCallsControlModeRadioButton.toggle();
				}
			});
			break;
		}
	}

}
