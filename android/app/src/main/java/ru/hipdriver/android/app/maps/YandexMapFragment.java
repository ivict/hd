package ru.hipdriver.android.app.maps;

import java.lang.ref.WeakReference;

import ru.hipdriver.android.app.R;
import ru.hipdriver.android.util.TextTools;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;

public class YandexMapFragment extends Fragment {
	
	private static final String TAG = TextTools.makeTag(YandexMapFragment.class);
	private GoogleMapOptions googleMapOptions;
	
	private MapView mapView;
	
	private static WeakReference<View> rootViewRef = new WeakReference<View>(null);
	
	static {
		Log.v(TAG, MapView.class.getSimpleName() + " - loaded");
	}
	
	public YandexMapFragment() {
		super();
	}
	
	public GoogleMapOptions getGoogleMapOptions() {
		return googleMapOptions;
	}

	public void setGoogleMapOptions(GoogleMapOptions googleMapOptions) {
		this.googleMapOptions = googleMapOptions;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return getRootView(inflater, container);
	}
		
	private View getRootView(LayoutInflater inflater, ViewGroup container) {
		View view = rootViewRef.get();
		if (view != null) {
			((ViewGroup) view.getParent()).removeView(view);
			//Update references
			mapView = (MapView) view.findViewById(R.id.yandex_map);
			return view;
		}
		view = inflater.inflate(R.layout.yandex_map_fragment, null, false);
		mapView = (MapView) view.findViewById(R.id.yandex_map);
		mapView.setSoundEffectsEnabled(false);
		MapController mapController = mapView.getMapController();
		CameraPosition camPos = getCamPos(view);
		mapController.setZoomCurrent(camPos.zoom);
		GeoPoint latlng = new GeoPoint(camPos.target.latitude, camPos.target.longitude); 
		mapController.setPositionNoAnimationTo(latlng);
		OverlayManager overlayManager = mapController.getOverlayManager();
		overlayManager.getMyLocation().setEnabled(false);
		overlayManager.getMyLocation().setVisible(false);
		
		rootViewRef = new WeakReference<View>(view);
		return view;
	}

	private CameraPosition getCamPos(View view) {
		if (googleMapOptions != null) {
			return googleMapOptions.getCamera();
		}
		return GoogleMapTools.restorePosition(view.getContext());
	}
	
	public MapView getMap() {
		if (mapView != null) {
			return mapView;
		}
		final View view = getView();
		if (view == null) {
			return null;
		}
		return (MapView) view.findViewById(R.id.yandex_map);
	}

}
