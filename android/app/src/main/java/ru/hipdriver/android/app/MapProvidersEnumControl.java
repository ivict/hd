package ru.hipdriver.android.app;

import ru.hipdriver.android.app.maps.GoogleMapTools;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.i.support.MapProvidersEnum;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

public class MapProvidersEnumControl implements OnClickListener {
	
	private final RadioButton googleMapsButton;
	private final RadioButton yandexMapsButton;
	
	private final HipdriverApplication hipdriverApplication;

	public MapProvidersEnumControl(HipdriverApplication hipdriverApplication, View view) {
		this.hipdriverApplication = hipdriverApplication; 
		
		googleMapsButton = (RadioButton) view.findViewById(R.id.google_maps_radiobutton);
		googleMapsButton.setText(Html.fromHtml(getA().getResources().getString(R.string.google_maps)));
        boolean googleMapsNotAvailable = !GoogleMapTools.isGoogleMapsAvailable(hipdriverApplication);
        if (googleMapsNotAvailable) {
            googleMapsButton.setVisibility(View.GONE);
            googleMapsButton.setVisibility(View.GONE);
        }
		yandexMapsButton = (RadioButton) view.findViewById(R.id.yandex_maps_radiobutton);
		yandexMapsButton.setText(Html.fromHtml(getA().getResources().getString(R.string.yandex_maps)));
		
		//Toggle current mode
		MapProvidersEnum mapProvider = DBLinks.unlinkMapProvider((short) getA().getInt(IMobileAgentProfile.MAP_PROVIDER_ID));
		switch (mapProvider) {
	    case GOOGLE:
			yandexMapsButton.setChecked(false);
			googleMapsButton.toggle();
	    	break;
	    case YANDEX:
			googleMapsButton.setChecked(false);
			yandexMapsButton.toggle();
	    	break;
		}
		
		yandexMapsButton.setOnClickListener(this);
		googleMapsButton.setOnClickListener(this);		
	}

	protected HipdriverApplication getA() {
		return hipdriverApplication;
	}

	@Override
	public void onClick(View view) {
	    // Check which radio button was clicked
	    switch(view.getId()) {
	    case R.id.google_maps_radiobutton:
			//resetFocus();

			getA().setInt(IMobileAgentProfile.MAP_PROVIDER_ID, DBLinks.link(MapProvidersEnum.GOOGLE));

			yandexMapsButton.setChecked(false);
			googleMapsButton.toggle();
			
	    	break;
	    case R.id.yandex_maps_radiobutton:
			//resetFocus();

			getA().setInt(IMobileAgentProfile.MAP_PROVIDER_ID, DBLinks.link(MapProvidersEnum.YANDEX));

			googleMapsButton.setChecked(false);
			yandexMapsButton.toggle();
			
	    	break;
	    }
		
	}

}
