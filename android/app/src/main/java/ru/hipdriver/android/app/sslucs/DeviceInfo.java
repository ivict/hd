package ru.hipdriver.android.app.sslucs;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceInfo {

	@JsonProperty(value = "cc")
	public int camerasCount;

	public int getCamerasCount() {
		return camerasCount;
	}

	public void setCamerasCount(int camerasCount) {
		this.camerasCount = camerasCount;
	}
	
}
