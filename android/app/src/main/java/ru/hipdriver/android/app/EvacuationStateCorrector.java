package ru.hipdriver.android.app;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import android.util.Log;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;

import static ru.hipdriver.android.app.Invariants.TAG_GEO;

public class EvacuationStateCorrector extends HipdriverTimerTask {
	
	private static final String TAG = TextTools.makeTag(EvacuationStateCorrector.class);
	
	private final ILocation location;
	private final Date alertTime;
	
	private String debugInfo = "";

	public EvacuationStateCorrector(HipdriverApplication applicationContext, Timer timer, ILocation location, Date alertTime) {
		super(applicationContext, timer);
		this.location = location;
		this.alertTime = alertTime;
	}

	@Override
	public void runIfNotPaused() {
		if (getA().getAppState() != AppStatesEnum.CAC) {
			cancel();
		}
		if (getA().getFirstDetectionCarState() == CarStatesEnum.U) {
			Log.w(TAG, "wrong-time-to-start-evacuation-state-corrector");
		}
		//Not evacuation or car-stolen
		if (getA().getFirstDetectionCarState() != CarStatesEnum.CE &&
				getA().getFirstDetectionCarState() != CarStatesEnum.CS) {
			return;
		}
		
		WatchdogService watchdogService = getA().getWatchdogService();
		List<? extends ICarLocation> track = watchdogService.getLastTenLocations();
		
		if (track.isEmpty()) {
			Log.w(TAG_GEO, "no-points-in-alert-track");
			restartEvacuationStateCorrector();
			return;
		}

		ICarLocation currentLocation = track.get(track.size() - 1);
		
		long delay = System.currentTimeMillis() - alertTime.getTime();
		float averageVelocity = getAverageNotZeroVelocity(track);
		
		//0.6f ~ 2.5 km / hour
		if (averageVelocity > 0 && track.size() > 2 && delay < TimeUnit.MINUTES.toMillis(WatchdogService.TIME_TRESHOLD_FOR_SPLIT_STATE_CORRECTORS_IN_MINUTES)) {
			Log.d(TAG_GEO, String.format("average-velocity [%s], acc1[%s], acc2[%s]", averageVelocity, location.getAcc(), currentLocation.getAcc()));
			if (averageVelocity <= 0.6f) {
				Log.d(TAG_GEO, "after-velocity-measure-correction-car-evacuation");
				getA().setCarState(CarStatesEnum.CE);
				watchdogService.updateAlertState("by-average-velocity-from-gps [" + averageVelocity + "]");
				UITaskFactory.sendAlertEvent(getA(), null, TimeUnit.MINUTES.toMillis(10));
				return;
			}
			Log.d(TAG_GEO, "after-velocity-measure-correction-car-stolen");
			getA().setCarState(CarStatesEnum.CS);
			watchdogService.updateAlertState("by-average-velocity-from-gps [" + averageVelocity + "]");
			UITaskFactory.sendAlertEvent(getA(), null, TimeUnit.MINUTES.toMillis(10));
			return;
		}
		
		float timeoutInSeconds = (float) delay / 1000f;
		if (timeoutInSeconds <= 0f) {
			Log.w(TAG_GEO, "time-measure-troubles");
			restartEvacuationStateCorrector();
			return;
		}
		float moveDistance = getMoveDistanceInMeters(location, currentLocation);
		if (moveDistance == 0f) {
			Log.w(TAG_GEO, "zero-move-distance");
			restartEvacuationStateCorrector();
			return;
		}
		float velocity = (moveDistance * 1000f) / (3600f * timeoutInSeconds);
		Log.d(TAG_GEO, String.format("direct-velocity [%s], moveDistance[%s], acc1[%s], acc2[%s]", velocity, moveDistance, location.getAcc(), currentLocation.getAcc()));
		if (velocity <= 2.5f) {
			Log.d(TAG_GEO, "after-distance-measure-correction-car-evacuation");
			getA().setCarState(CarStatesEnum.CE);
			watchdogService.updateAlertState("by-first-move-velocity [" + velocity + "]");
			UITaskFactory.sendAlertEvent(getA(), null, TimeUnit.MINUTES.toMillis(10));
			return;
		}
		Log.d(TAG_GEO, "after-distance-measure-correction-car-stolen");
		getA().setCarState(CarStatesEnum.CS);
		watchdogService.updateAlertState("by-first-move-velocity [" + velocity + "]");
		UITaskFactory.sendAlertEvent(getA(), null, TimeUnit.MINUTES.toMillis(10));
	}

	private void restartEvacuationStateCorrector() {
		EvacuationStateCorrector evacuationStateCorrector = new EvacuationStateCorrector(getA(), timer, location, alertTime); 
		try {
			long delay = TimeUnit.SECONDS.toMillis(15);
			timer.schedule(evacuationStateCorrector, delay);
			timer.purge();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	private float getAverageNotZeroVelocity(List<? extends ICarLocation> track) {
		if (track.isEmpty()) {
			return 0f;
		}
		int zeroCount = 0;
		float velTotalNontZero = 0f;
		for (ICarLocation pos : track) {
			float velocity = pos.getVelocity();
			if (velocity > 0) {
				velTotalNontZero += velocity;
			} else {
				zeroCount++;
			}
			
		}
		float countOfNotZeroValues = (float) (track.size() - zeroCount);
		if (countOfNotZeroValues <= 0f) {
			return 0f;
		}
		return velTotalNontZero / countOfNotZeroValues;
	}

	private float getMoveDistanceInMeters(ILocation location1, ILocation location2) {
		int lat1 = location1.getLat();
		int lon1 = location1.getLon();
		int lat2 = location2.getLat();
		int lon2 = location2.getLon();
	    float distanceInMeters = lat1 != ILocation.UNKNOWN_LATITUDE ? Locations.getDistance(lat1, lon1, lat2, lon2) : 0;
		return distanceInMeters;
	}

}
