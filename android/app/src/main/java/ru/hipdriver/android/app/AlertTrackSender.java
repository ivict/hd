package ru.hipdriver.android.app;

import static ru.hipdriver.android.app.Invariants.TAG_GEO;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.android.app.R;

import android.util.Log;

public class AlertTrackSender extends HipdriverTimerTask {
	
	private static final String TAG = TextTools.makeTag(AlertTrackSender.class);
	
	private final int alertCid;
	
	private boolean stateChecked; 
	
	private String debugInfo = "+";

	private HttpClient httpClient;

	public AlertTrackSender(HipdriverApplication applicationContext, Timer timer) {
		super(applicationContext, timer);
		WatchdogService watchdogService = applicationContext.getWatchdogService();
		if (watchdogService == null) {
			alertCid = ILocation.UNKNOWN_CELL_ID;
		} else {
			alertCid = watchdogService.getCid();
		}
		
	}

	@Override
	public void runIfNotPaused() {
		if (getA().getAppState() != AppStatesEnum.CAC) {
			cancel();
		}
		final WatchdogService watchdogService = getA().getWatchdogService();
		
		//Car state correction
		if (watchdogService.getDurationFromLastAlertInMillis() > TimeUnit.MINUTES.toMillis(WatchdogService.TIME_TRESHOLD_FOR_SPLIT_STATE_CORRECTORS_IN_MINUTES)) {
			correctionAppState(watchdogService.getLastTenLocations(), watchdogService);
		}
		
		final List<? extends ICarLocation> track = watchdogService.getAlertTrack();
		if (track.isEmpty()) {
			return;
		}
		//Suppress mixing Gps and Gsm
		if (!watchdogService.isGpsAvailable) {
			UITaskFactory.sendGpsEvent(getA(), null);
			updateLocations(track);
		} else {
			UITaskFactory.sendGsmEvent(getA(), null);
		}
		
		int maxPointsPerPacket = watchdogService.getMaxPointsPerPacket();
		List<? extends ICarLocation> compressedTrack = Locations.compressTrack(track, maxPointsPerPacket);
		
		Log.d(TAG, "send-alert-track");
		if (getA().isDebugMode()) {
			StringBuilder logRecord = new StringBuilder();
			for (ICarLocation loc : compressedTrack) {
				//state-id,time,lat,lon,alt,acc,mcc,mnc,lac,cid
				logRecord.append(String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", loc.getCarStateId(), TextTools.format("yyyy-MM-dd HH:mm:ss", loc.getTime()), loc.getLat(), loc.getLon(), loc.getAlt(), loc.getAcc(), loc.getMcc(), loc.getMnc(), loc.getLac(), loc.getCid()));
			}
			String logName = "send-alert-track.log";
			//getA().makeDebugInfoHeaderIfNotHeadered(logName, "state-id,time,lat,lon,alt,acc,mcc,mnc,lac,cid\n");
			getA().writeDebugInfo(logName, logRecord.toString());
		}
		
		UITaskFactory.sendAlertTrackEvent(getA(), compressedTrack, new After() {

			@Override
			public void success(Object returnValue) {
				watchdogService.clearAlertTrack(track);
			}

			@Override
			public void failure(int returnCode, String failureReason) {
				watchdogService.clearAlertTrack(track);
			}
			
		});
	}

	private void correctionAppState(List<? extends ICarLocation> track, WatchdogService watchdogService) {
		final boolean hasMove;
		if (track.isEmpty()) {
			//Wait for GPS
			if (watchdogService.getDurationFromLastAlertInMillis() < TimeUnit.MINUTES.toMillis(WatchdogService.TIME_TRESHOLD_FOR_SPLIT_STATE_CORRECTORS_IN_MINUTES + 3)) {
				return;
			}
			if (alertCid == ILocation.UNKNOWN_CELL_ID) {
				return;
			}
			int currentCid = watchdogService.getCid();
			if (currentCid == ILocation.UNKNOWN_CELL_ID) {
				return;
			}
			
			if (getA().isDebugMode()) {
				debugInfo = String.format("cid1 [%s], cid2 [%s]", alertCid, currentCid);
				Log.d(TAG, debugInfo);
				writeDebugInfoIntoLog(debugInfo);
			}
			hasMove = currentCid != alertCid;
		} else {
			ICarLocation location = track.get(track.size() - 1);
			ICarLocation initialPosition = watchdogService.getInitialPosition();
			if (initialPosition == null) {
				initialPosition = watchdogService.getFirstLocationAfterAlert();
			}
			hasMove = hasMove(initialPosition, location);
		}
		
		
		if (hasMove && (getA().getCarState() == CarStatesEnum.CP || getA().getCarState() == CarStatesEnum.WS)) {
			Log.d(TAG_GEO, "correct-car-state-on-car-stolen");
			getA().setCarState(CarStatesEnum.CS);
			watchdogService.updateAlertState(debugInfo);
			for (ICarLocation pos : track) {
				pos.setCarStateId(DBLinks.link(CarStatesEnum.CS));
			}
			UITaskFactory.sendAlertEvent(getA(), null, TimeUnit.MINUTES.toMillis(10));
			//Single update
			if (!stateChecked) {
				checkStateAndSendNotice();
				stateChecked = true;
			}
			return;
		}
		if (!hasMove && (getA().getCarState() == CarStatesEnum.CE || getA().getCarState() == CarStatesEnum.CS)) {
			Log.d(TAG_GEO, "correct-car-state-on-car-poke");
			getA().setCarState(CarStatesEnum.CP);
			watchdogService.updateAlertState(debugInfo);
			for (ICarLocation pos : track) {
				pos.setCarStateId(DBLinks.link(CarStatesEnum.CS));
			}
			UITaskFactory.sendAlertEvent(getA(), null, TimeUnit.MINUTES.toMillis(10));
			//Single update
			if (!stateChecked) {
				checkStateAndSendNotice();
				stateChecked = true;
			}
			return;
		}
		//Single update
		if (!stateChecked) {
			checkStateAndSendNotice();
			stateChecked = true;
		}
	}

	private void checkStateAndSendNotice() {
		final StringBuilder smsMessageBody = new StringBuilder();
		String alertType = "";
		switch (getA().getCarState()) {
		case CE:
			alertType = getA().getResources().getString(R.string.car_evacuate);
			break;
		case WS:
			alertType = getA().getResources().getString(R.string.steal_wheels);
			break;
		case CP:
			alertType = getA().getResources().getString(R.string.car_poke);
			break;
		case CS:
			alertType = getA().getResources().getString(R.string.car_stolen);
			break;
		}
		smsMessageBody.append(alertType);
		String phoneNumber = getA().getString(MainActivity.PHONE_NUMBER_EDITOR, "");
		String smsMessage = smsMessageBody.toString();
		if (getA().getBoolean(IMobileAgentProfile.SEND_SMS_AFTER_EVENT_CLASS_DETECTION)
				&& !phoneNumber.isEmpty()) {
			WatchdogService.sendSMS(phoneNumber, smsMessage);
		}
		final String email = getA().getString(IMobileAgent.ALERT_EMAIL_KEY, "");
		if (getA().getBoolean(IMobileAgentProfile.SEND_EMAIL_AFTER_EVENT_CLASS_DETECTION)
				&& !email.isEmpty()) {
			String mobileAgentName = getA().getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
			String header = getA().getResources().getString(R.string.email_message_subj_alert, mobileAgentName);
			UITaskFactory.sendEmail(getA(), email, header, smsMessage, null, 0);
//			Uri uri = Uri.parse("mailto:" + email);
//			Intent sendEMailActivity = new Intent(Intent.ACTION_SENDTO, uri);
//			sendEMailActivity.putExtra(Intent.EXTRA_SUBJECT,
//					smsMessageBody.toString());
//			sendEMailActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//					| Intent.FLAG_ACTIVITY_NEW_TASK);
//			getA().startActivity(sendEMailActivity);
//			Log.d(TAG, "sended e-mail mailto:" + email);
		}
	}

	private boolean hasMove(ICarLocation location1, ICarLocation location2) {
		//Move by cids very irregular
//		if (location1.getCid() != location2.getCid() && location1.getCid() != ILocation.UNKNOWN_CELL_ID && location2.getCid() != ILocation.UNKNOWN_CELL_ID) {
//			if (getA().isDebugMode()) {
//				debugInfo = "move-by-cids [" + location1.getCid() + " => " + location2.getCid() + "]"; 
//			}
//			return true;
//		}
		int lat1 = location1.getLat();
		int lon1 = location1.getLon();
		int lat2 = location2.getLat();
		int lon2 = location2.getLon();
		//Accuracy is radius around coordinates which point is located with probability 68%
		int acc1 = location1.getAcc();
		int acc2 = location2.getAcc();
		//TODO: 300 meters by one direction
	    float distanceInMeters = lat1 != ILocation.UNKNOWN_LATITUDE ? Locations.getDistance(lat1, lon1, lat2, lon2) : 0;
	    float accuracy1InMeters = acc1 / 100f;
	    float accuracy2InMeters = acc2 / 100f;
	    float minDistanceWith32PercentError = accuracy1InMeters + accuracy2InMeters;
	    float maxDistanceForMoveDetection = 300f;
	    if (acc1 != ILocation.UNKNOWN_ACCURACY
	    		&& acc2 != ILocation.UNKNOWN_ACCURACY
	    		&& acc1 > 0
	    		&& acc2 > 0) {
	    	maxDistanceForMoveDetection = minDistanceWith32PercentError;
	    }
		if (distanceInMeters > maxDistanceForMoveDetection) {
			if (getA().isDebugMode()) {
				debugInfo = String.format("move-by-distance [%s], acc1 [%s], acc2 [%s]", distanceInMeters, acc1, acc2);
				Log.d(TAG, debugInfo);
				writeDebugInfoIntoLog(debugInfo);
			}
			return true;
		}
		if (getA().isDebugMode()) {
			debugInfo = String.format("stop-by-distance [%s], acc1 [%s], acc2 [%s]", distanceInMeters, acc1, acc2); 
			Log.d(TAG, debugInfo);
			writeDebugInfoIntoLog(debugInfo);
		}
		return false;
	}
	
	private void writeDebugInfoIntoLog(String debugInfo) {
		if (!getA().isDebugMode()) {
		}
		CarStatesEnum carState = getA().getCarState();
		CarStatesEnum firstDetectionCarState = getA().getFirstDetectionCarState();
		String logName = "alert-states.log";
		getA().makeDebugInfoHeaderIfNotHeadered(logName, "time,first-detection-car-state,state,debug-info\n");
		getA().writeDebugInfo(logName, "%s,%s,%s,%s<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()), firstDetectionCarState, carState, debugInfo);
	}

	private void updateLocations(List<? extends ICarLocation> track) {
		//Update coordinates from Yandex
		for (ICarLocation location : track) {
			int lat = location.getLat();
			int lon = location.getLon();
			if (lat != ILocation.UNKNOWN_LATITUDE && lon != ILocation.UNKNOWN_LONGITUDE) {
				continue;
			}
			int mcc = location.getMcc();
			int mnc = location.getMnc();
			int lac = location.getLac();
			int cid = location.getCid();
			ServicesConnector.safetyUpdateGeoCoordinates(getA(), getHttpClient(), location, mcc, mnc, lac, cid);
		}
	}

	private HttpClient getHttpClient() {
		if (httpClient != null) {
			return httpClient;
		}
		httpClient = new DefaultHttpClient();
		return httpClient;
	}
}
