package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.support.AppFacesEnum;
import ru.hipdriver.i.support.AppStatesEnum;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ScreenStateReceiver extends HipdriverBroadcastReceiver {

	private static final String TAG = TextTools.makeTag(ScreenStateReceiver.class);
	
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (action == null) {
			return;
		}
		
		final HipdriverApplication hipdriverApplication = getA(context);
		
		if (hipdriverApplication.getAppFace() == AppFacesEnum.REMOTE_CONTROL) {
			return;
		}
		
		//Not receiving intents if paused
		if (hipdriverApplication.isPaused()) {
			return;
		}
		
		//Application not active
		if (hipdriverApplication.getAppState() == AppStatesEnum.ASO) {
			return;
		}
		//Beacon mode is active
		if (hipdriverApplication.getAppState() == AppStatesEnum.BMA) {
			return;
		}
		
        if (!action.equals(Intent.ACTION_SCREEN_OFF)) {
        	return;
        }
        Log.d(TAG,"action-screen-off");
        // Unregisters the listener and registers it again.
		WatchdogService watchdogService = hipdriverApplication.getWatchdogService();
		if (watchdogService == null) {
			return;
		}
		watchdogService.wakeLockHack();
	}

}

