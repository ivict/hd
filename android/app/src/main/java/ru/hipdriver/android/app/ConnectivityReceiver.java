package ru.hipdriver.android.app;

import android.content.Context;
import android.content.Intent;

public class ConnectivityReceiver extends HipdriverBroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (context == null) {
			return;
		}
		HipdriverApplication hipdriverApplication = getA(context);
		if (hipdriverApplication == null) {
			return;
		}
		if (intent == null) {
			return;
		}
		
		String action = intent.getAction();
		if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
			onChangeConnectivity(hipdriverApplication, intent);
		}
		
		
	}

	private void onChangeConnectivity(
			HipdriverApplication hipdriverApplication, Intent intent) {
		boolean hasConnection = Hardwares.isNetworkConnectedOrConnecting(hipdriverApplication);
	    if( hasConnection) {
	    	hipdriverApplication.setHasConnection(true);
	    } else {
	    	hipdriverApplication.setHasConnection(false);
	    }
	}

}
