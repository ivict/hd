package ru.hipdriver.android.app;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import android.content.Intent;
import android.util.Log;

//@see http://stackoverflow.com/questions/5533881/toggle-airplane-mode-in-android
//For 4.2+ @see http://android.dm.id.lv/Jelly_Bean_4.x_Airplane_Mode_Helper#Notes_for_developers
public class AirplaneMode {
	
	private static final String TAG = TextTools.makeTag(AirplaneMode.class);

	private final HipdriverApplication hipdriverApplication;
	

	public AirplaneMode(HipdriverApplication hipdriverApplication) {
		this.hipdriverApplication = hipdriverApplication;
	}

	public void switchOn() {
		if (!isActivated()) {
			return;
		}
		if (isAirplaneModeOn()) {
			return;
		}
		try {
			switchOnInternalAsync();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	
	private void switchOnInternalAsync() throws InterruptedException,
			ExecutionException, TimeoutException {
		//Suppress switch-on if application not in watchdog or beacon mode
		if (hipdriverApplication.getAppState() == AppStatesEnum.ASO) {
			return;
		}
		//Suppress switch-on if car in alert state
		if (hipdriverApplication.getAppState() == AppStatesEnum.CAC
				&& hipdriverApplication.getCarState() != CarStatesEnum.U) {
			return;
		}
		//Always be run in networking tasks queue
		hipdriverApplication.runNetworkTask(new Runnable() {

			@Override
			public void run() {
				//// read the airplane mode setting
				//boolean isEnabled = isEnabled();
				//// check if already set
				//if (isEnabled) {
				//	return;
				//}
				switchOnInternal();
			}
			
		}, 10, TimeUnit.MINUTES);
	}

	private boolean isAirplaneModeOn() {
		////Cached value
		//return getA().getInt(Hardwares.SETTINGS_AIRPLANE_MODE_ON, 0) != 0;
		return Hardwares.getSettingsInt(hipdriverApplication.getContentResolver(),
					Hardwares.SETTINGS_AIRPLANE_MODE_ON, 0) != 0;
	}

	public void switchOff(boolean async) {
		if (!isActivated()) {
			return;
		}
		
		if (!isAirplaneModeOn()) {
			return;
		}

		switchOffInternal();

		if (!async) {
			runSwitchOffAsTask();
		} else {
			Thread cmd = new Thread(new Runnable() {

				@Override
				public void run() {
					runSwitchOffAsTask();
				}
				
			});
			cmd.start();
		}
		
	}

	protected void runSwitchOffAsTask() {
		try {
			//Synchronization with switch on airplane mode task.
			hipdriverApplication.runNetworkTask(new Runnable() {
	
				@Override
				public void run() {
					if (!isAirplaneModeOn()) {
						return;
					}
					switchOffInternal();
				}
				
			}, 10, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		} catch (ExecutionException e) {
			Log.wtf(TAG, e);
		} catch (TimeoutException e) {
			Log.wtf(TAG, e);
		}
	}

	protected void switchOffInternal() {
		if (!isActivated()) {
			return;
		}
		
		if (!isAirplaneModeOn()) {
			return;
		}
		try {
			// toggle airplane mode
			Hardwares.putSettingsInt(hipdriverApplication.getContentResolver(),
					Hardwares.SETTINGS_AIRPLANE_MODE_ON, 0);
			getA().setInt(Hardwares.SETTINGS_AIRPLANE_MODE_ON, 0);
	
			// Post an intent to reload
			safetySendBroadcastMessageChangeAirplaneMode(false);
			Log.d(TAG, "airplane-mode-off");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	protected void safetySendBroadcastMessageChangeAirplaneMode(boolean mode) {
		try {
			Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
			intent.putExtra("state", mode);
			hipdriverApplication.sendBroadcast(intent);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	
	public HipdriverApplication getA() {
		return hipdriverApplication;
	}

	public static boolean isStable() {
		return android.os.Build.VERSION.SDK_INT < 0x11;		
	}
	
	private boolean isActivated() {
		return getA().getBoolean(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE);
	}

	protected void switchOnInternal() {
		if (!isActivated()) {
			return;
		}
		
		if (isAirplaneModeOn()) {
			return;
		}
		try {
			Hardwares.putSettingsInt(hipdriverApplication.getContentResolver(),
					Hardwares.SETTINGS_AIRPLANE_MODE_ON, 1);
			getA().setInt(Hardwares.SETTINGS_AIRPLANE_MODE_ON, 1);
	
			// Post an intent to reload
			safetySendBroadcastMessageChangeAirplaneMode(true);
			Log.d(TAG, "airplane-mode-on");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

}
