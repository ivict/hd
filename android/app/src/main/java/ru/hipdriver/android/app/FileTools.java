package ru.hipdriver.android.app;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.regex.Pattern;

import ru.hipdriver.android.util.TextTools;
import android.os.StatFs;
import android.util.Log;

public class FileTools {
	
	private static final String TAG = TextTools.makeTag(FileTools.class);
	
	private final HipdriverApplication hipdriverApplication;
	
	public FileTools(HipdriverApplication hipdriverApplication) {
		this.hipdriverApplication = hipdriverApplication;
	}
	
	public HipdriverApplication getA() {
		return this.hipdriverApplication;
	}
	
	public void safetySaveBufferIntoFile(byte[] jsonData, String fileName) {
		String folderName = getA().getString(WatchdogService.WATCHDOG_SERVICE_SAVE_INTO_FOLDER_KEY, "");
		if (folderName.isEmpty()) {
			return;
		}
		File folder = new File(folderName);
		if (!folder.exists()) {
			return;
		}
		StatFs statFs = new StatFs (folderName);
		int freeBytes = getFreeBytes(statFs);
		if (jsonData.length > freeBytes ) {
			return;
		}
		try {
			FileOutputStream out = new FileOutputStream(new File(folder, fileName));
			out.write(jsonData);
			out.flush();
			out.close();
		} catch (IOException e) {
			Log.wtf(TAG, e);
		}
	}

	private int getFreeBytes(StatFs statFs) {
		return statFs.getAvailableBlocks() * statFs.getBlockSize();
	}
	
	public static String getLastLogEvents(String regExp, int maxCharsCount) {
		StringBuilder text = new StringBuilder();
		try	{
			
			Pattern linePattern = Pattern.compile(regExp);
			Process proc = Runtime.getRuntime().exec("logcat -d ");// + LOG_TAG + ":* *:S");
			BufferedReader stdOutReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			BufferedReader stdErrReader = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
			String line;
			while ( (line = stdOutReader.readLine()) != null ) {
				if ( linePattern.matcher(line).find() ) {
					text.append(line).append('\n');
				}
				if (text.length() > maxCharsCount) {
					break;
				}
			}
			while ( (line = stdErrReader.readLine()) != null ) {
				if ( linePattern.matcher(line).find() ) {
					text.append(line).append('\n');
				}
				if (text.length() > maxCharsCount) {
					break;
				}
			}
			proc.waitFor();
			stdOutReader.close();
			stdErrReader.close();
		}
		catch (IOException e) {
			text.append(TextTools.printIntoString(e));
		}
		catch (Throwable th) {
			Log.wtf(TAG, th);
			text.append(TextTools.printIntoString(th));
		}
		return text.toString();
	}

	public static boolean writeIntoFile(String fileName, byte[] info) {
		File file = new File(fileName);
		file.getParentFile().mkdirs();
		try {
			writeInfo(info, file);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return false;
		}
		return true;
	}

	private static void writeInfo(byte[] info, File file)
			throws FileNotFoundException, IOException {
		OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
		try {
			final int pageSize =  4096;
			byte[] page = new byte[pageSize];
			int pagesCount = info.length / pageSize;
			int finalPageSize = info.length % pageSize;
			for (int i = 0; i < pagesCount; i++) {
				System.arraycopy(info, i * pageSize, page, 0, pageSize);
				out.write(page);
				out.flush();
			}
			if (finalPageSize > 0) {
				System.arraycopy(info, pagesCount * pageSize, page, 0, finalPageSize);
				out.write(page, 0, finalPageSize);
			}
			out.flush();
		} finally {
			out.close();
		}
	}
	
	
}
