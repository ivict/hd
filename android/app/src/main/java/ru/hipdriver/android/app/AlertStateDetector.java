package ru.hipdriver.android.app;

import static ru.hipdriver.android.app.NumericTools.ffn;
import static ru.hipdriver.android.app.NumericTools.norma;
import static ru.hipdriver.android.app.NumericTools.num;
import static ru.hipdriver.android.app.NumericTools.num100;

import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.d.Sensors;
import ru.hipdriver.d.Sensors.MinMaxSqValue;
import ru.hipdriver.d.Sensors.MinMaxValue;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.j.ext.SensorData;
import ru.hipdriver.android.app.R;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.FloatMath;
import android.util.Log;

public class AlertStateDetector extends HipdriverTimerTask {
	
	public static final String TAG = TextTools.makeTag(AlertStateDetector.class);

	public static final String MAX_ACC_KEY = "max-acc";
	public static final String MAX_JERK_KEY = "max-jerk";
	public static final String AVG_PC_KEY = "avg-pc";
	public static final String CRITICAL_ANGLE_KEY = "critical-angle";
	
	public static final float DEFAULT_CRITICAL_ANGLE = 2f;
	public static long DEFAULT_TIMEOUT_FOR_NEXT_MEASURE = 120000L; 
	public static long DEFAULT_TIMEOUT_FOR_REDUCE_GAP_MILLIS = 5000L;
	

	// For correct measuring use more then 10 points
	public static final int TOO_SMALL_DATA_TRESHOLD = 15;

	public static final String AUTOMATIC_DETECTION = "automatic-detection";

	public static final String LIMIT_CRITICAL_ANGLE_KEY = "limit-critical-angle";

	public static final String LIMIT_MAX_ACC_KEY = "limit-max-acc";

	//Me
	private float[] position = new float[3];
	private float[] velocity = new float[3];
	//private float[] acceleration = new float[3];
	
	private String status = "";
	private float maxJerkValue;
	//Huawei accmax: 21.44
	//Thor accmax: 25.85
	private float maxMaxAccelerationValue;
	private float maxAbsAccelerationValue;
	private float maxVelocityValue;
	private float maxShiftValue;
	
	//private final float sensorResolution;
	//private final float measureRateInSeconds;
	private float maxJerk;
	private float maxAcc;
	private int avgPc;//Average point count
	
	final private float[] minAngle;
	final private float[] maxAngle;
	final private float criticalAngle;
	
	boolean nextInitialMeasure = true;
	float initialAngleX;
	float initialAngleY;
	float angleX;
	float angleY;
	float prevAngleX;
	float prevAngleY;
	
	boolean preAlertMode;
	
	private long time0;
	
	float avgAngleX;
	float avgAngleY;
	
	private boolean firstRun = true;
	
	private long dontStartBeforeTime;

	private float limitChangeOfAngle;
	private float limitAbsAccelerationValue;
	private boolean automaticLimitsDetection;
    private transient long timestamp;

//	Thread callThread = new Thread(new Runnable() {
//
//		@Override
//		public void run() {
//			getA().waitForGsmConnection(TimeUnit.MINUTES.toMillis(10));
//			String phoneNumber = getA().getString("phone_number_editor", "");
//			if (!WatchdogService.makeOutboundCall(getA(), phoneNumber, TimeUnit.SECONDS.toMillis(20)) ) {
//				getA().debugStatus("Звонок сорвался!");
//			}
//		}
//		
//	});
	
	public AlertStateDetector(HipdriverApplication applicationContext, Timer timer, float sensorResolution, float measureRateInSeconds, long dontStartBeforeTime) {
		super(applicationContext, timer);
		//this.sensorResolution = sensorResolution;
		//this.measureRateInSeconds = measureRateInSeconds;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getA());
		maxAcc = preferences.getFloat(MAX_ACC_KEY, IMobileAgentProfile.DEFAULT_ASD_MAX_ACCELERATION);
		maxJerk = preferences.getFloat(MAX_JERK_KEY, IMobileAgentProfile.DEFAULT_ASD_MAX_JERK);
		avgPc =  preferences.getInt(AVG_PC_KEY, 4);
		criticalAngle =  preferences.getFloat(CRITICAL_ANGLE_KEY, IMobileAgentProfile.DEFAULT_ASD_MAX_ANGLE);
		minAngle = new float[3];
		maxAngle = new float[3];
		Arrays.fill(minAngle, Float.MAX_VALUE);
		Arrays.fill(maxAngle, Float.MIN_VALUE);
		time0 = System.currentTimeMillis();
		
		this.dontStartBeforeTime = dontStartBeforeTime;
		initialState();
		
	}

	protected void initialState() {
		maxAbsAccelerationValue = 0;
		maxMaxAccelerationValue = 0;
		nextInitialMeasure = true;
		automaticLimitsDetection = getA().getBoolean(AlertStateDetector.AUTOMATIC_DETECTION, false); 
		if (automaticLimitsDetection) {
			limitChangeOfAngle = getA().getFloat(AlertStateDetector.LIMIT_CRITICAL_ANGLE_KEY, 0f);
			limitAbsAccelerationValue = getA().getFloat(AlertStateDetector.LIMIT_MAX_ACC_KEY, 0f);
		}
        timestamp = System.currentTimeMillis();

	}
	
//	//Сбрасываем накопленные значения, поскольку не знаем длительность паузы.
//	@Override
//	protected void onPause() {
//		initialState();
//		firstRun = true;
//	}
	
	//Работаем до состояния тревоги,
	//После детекции тревоги убираем задачу с таймера и запускаем новую, для определения класса события
	@Override
	public void runIfNotPaused() {
		if (!isCanRun()) {
			return;
		}
		
		//Check application switch off
		if (getA().getAppState() == AppStatesEnum.ASO) {
			cancel();
			return;
		}
		
		final WatchdogService watchdogService = getA().getWatchdogService();
		
		if (firstRun) {
			Log.d(TAG, "start-alert-detection [carState = " + getA().getCarState() + "]");
			getA().setAppState(AppStatesEnum.CAC);
			watchdogService.sensorEventListener.switchOffCalibration();
			watchdogService.accelerometerData.setCountOfValues(0);
			watchdogService.fireChangeMobileAgentState();
			firstRun = false;
			return;
		}
		
		// Prevent errors on too small data
        int countOfValues = watchdogService.accelerometerData.getCountOfValues();
        if (TOO_SMALL_DATA_TRESHOLD > countOfValues) {
			initialState();
			return;
		}
		
		if (dontStartBeforeTime > System.currentTimeMillis()) {
			initialState();
			return;
		}
		
		boolean debugMode = getA().isDebugMode();
		float averageJerk = getAverageJerk();
		if ( debugMode ) {
			getA().debugStatus("jerk: " + ffn(averageJerk, 5) + status);
		}
		
		if ( averageJerk > maxJerk ) {
			String alertReason = "Тревога J!";
			getA().debugStatus(alertReason);
			setAlertStateMakeCallAndStartReasonDetection(watchdogService, alertReason, averageJerk, -1);
			return;
		}
        //TODO: Fix algorithm of measure average acceleration and remove additional condition
		if ( maxAbsAccelerationValue > maxAcc && (System.currentTimeMillis() - timestamp) > TimeUnit.SECONDS.toMillis(30)) {//watchdogService.accelerometerData.getCountOfValues();
			String alertReason = "Тревога A!";
			getA().debugStatus(alertReason);
			setAlertStateMakeCallAndStartReasonDetection(watchdogService, alertReason, averageJerk, -1);
			return;
		}
		float maxChangeOfAngle = Math.max(Math.abs(((angleX + prevAngleX) / 2f) - initialAngleX),
				Math.abs(((angleY + prevAngleY) / 2f) - initialAngleY));
		if ( maxChangeOfAngle > criticalAngle ) {//watchdogService.accelerometerData.getCountOfValues();
			String alertReason = "Тревога У!";
			getA().debugStatus(alertReason);
			setAlertStateMakeCallAndStartReasonDetection(watchdogService, alertReason, averageJerk, maxChangeOfAngle);
			return;
		}
		
		//Update limits if atomatic limits detection
		if (automaticLimitsDetection) {
			updateLimits(maxChangeOfAngle, maxAbsAccelerationValue);
		}

		if (System.currentTimeMillis() - time0 > DEFAULT_TIMEOUT_FOR_NEXT_MEASURE) {
			nextInitialMeasure = true;
			time0 = System.currentTimeMillis();
		}
		
	}


	private void updateLimits(float maxChangeOfAngle,
			float maxAbsAccelerationValue) {
		if (limitChangeOfAngle < maxChangeOfAngle) {
			limitChangeOfAngle = maxChangeOfAngle;
			getA().setFloat(LIMIT_CRITICAL_ANGLE_KEY, limitChangeOfAngle);
		}
		if (limitAbsAccelerationValue < maxAbsAccelerationValue) {
			limitAbsAccelerationValue = maxAbsAccelerationValue;
			getA().setFloat(LIMIT_MAX_ACC_KEY, limitAbsAccelerationValue);
		}
	}

	public void setAlertStateMakeCallAndStartReasonDetection(
			final WatchdogService watchdogService, String alertReason, float averageJerk, float maxChangeOfAngle) {
		getA().setCarState(CarStatesEnum.A);
		watchdogService.setLastAlertTimeInNs(System.nanoTime());
		watchdogService.setLastAlertTime(new Date(System.currentTimeMillis()));

		AirplaneMode airplaneMode = new AirplaneMode(getA());
		airplaneMode.switchOff(false);

        Log.i(TAG, alertReason + " aj: [" + averageJerk + "], maav: [" + maxAbsAccelerationValue + "], mca: [" + maxChangeOfAngle + "]");
		
		Runnable alertCallCommand = new Runnable() {

			@Override
			public void run() {
				Log.d(TAG, "call-making");
				int alertCallsCount = getA().getInt(IMobileAgentProfile.ALERT_CALLS_COUNT);				
				if (alertCallsCount <= 0) {
					Log.d(TAG, String.format("calls-count[%s]-nothing-do", alertCallsCount));
					return;
				}
				getA().waitForCellNetworkConnection(TimeUnit.MINUTES.toMillis(10));
				if (getA().getBoolean(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE)) {
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) { }
				}
				String phoneNumber = getA().getString("phone_number_editor", "");//getA().isInService();
				int alertCallDurationInSecond = getA().getInt(IMobileAgentProfile.ALERT_CALLS_DURATION_IN_SECONDS);
				if (!WatchdogService.makeOutboundCall(getA(), phoneNumber, TimeUnit.SECONDS.toMillis(alertCallDurationInSecond)) ) {
					getA().debugStatus("Звонок сорвался!");
				}
				alertCallsCount--;
				int alertCallsIntervalInSeconds = getA().getInt(IMobileAgentProfile.ALERT_CALLS_INTERVAL_IN_SECONDS);
				while (alertCallsCount > 0 && !Thread.interrupted()) {
					try {
						Thread.sleep(TimeUnit.SECONDS.toMillis(alertCallsIntervalInSeconds));
					} catch (InterruptedException e) {
						return;
					}
					if (!WatchdogService.makeOutboundCall(getA(), phoneNumber, TimeUnit.SECONDS.toMillis(alertCallDurationInSecond)) ) {
						getA().debugStatus("Звонок сорвался!");
					}
					alertCallsCount--;
				}
			}
			
		};
		
		getA().watchdogAlert(R.string.alert, false);
		
		final String email = getA().getString(IMobileAgent.ALERT_EMAIL_KEY, "");
		boolean sendPhotoAfterAlert = getA().getBoolean(IMobileAgentProfile.SEND_PHOTO_AFTER_ALERT);
		int countOfPhotos = getA().getInt(IMobileAgentProfile.COUNT_OF_PHOTOS);
		int intervalBetweenPhotosInSeconds = getA().getInt(IMobileAgentProfile.INTERVAL_BETWEEN_PHOTOS_IN_SECONDS);
		boolean sendPhotos = sendPhotoAfterAlert
				&& !email.isEmpty()
				&& countOfPhotos > 0;
		boolean makePhotoBeforeOutgoingCall = getA().getBoolean(IMobileAgentProfile.WAIT_UNTIL_THE_ALERT_CALL);
		
		//MAKE OUTGOING CALL HAS MORE PRIORITY THAN PHOTOS
		if (!(makePhotoBeforeOutgoingCall && sendPhotos)) {
			watchdogService.runAlertCallTask(alertCallCommand);
		}
		
		try {
			watchdogService.fireChangeMobileAgentState();
			if (getA().getBoolean(IMobileAgentProfile.ENABLE_DEBUG_MODE)) {
				DebugInfoSender debugInfoSender = new DebugInfoSender(getA(), timer);
				//TODO: Check timer state
				Log.d(TAG, "make-debug-info-sending-task");
				timer.schedule(debugInfoSender, DebugInfoSender.DELAY_IN_MILLIS);
			}
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		try {
			EventClassDetector eventClassDetector = new EventClassDetector(getA(), timer, alertReason);
			long detectionTimeoutInMillis = EventClassDetector.DEFAULT_BIG_TIMEOUT_MILLIS;//getA().getInt(EventClassDetector.BIG_TIMEOUT_KEY, 10) * 1000;
			//TODO: Check timer state
			Log.d(TAG, "make-event-class-detecting-task");
			timer.schedule(eventClassDetector, detectionTimeoutInMillis);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		//Take alert info into server, start new alert-track
		try {
			UITaskFactory.sendAlertEvent(getA(), null, TimeUnit.DAYS.toMillis(1));
			//TODO: below code for test, md
			if (sendPhotos) {
				WatchdogService.makePhotos(getA(), countOfPhotos, intervalBetweenPhotosInSeconds);
			}
			
			//MAKE PHOTOS HAS MORE PRIORITY THAN OUTGOING CALL
			if (makePhotoBeforeOutgoingCall && sendPhotos) {
				watchdogService.runAlertCallTask(alertCallCommand);
			}
			
			//else if (getA().getBoolean(IMobileAgentProfile.SEND_EMAIL_AFTER_EVENT_CLASS_DETECTION)
			//		&& !email.isEmpty()) {
			//	String mobileAgentName = getA().getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
			//	UITaskFactory.sendEmail(getA(), email, "Тревога [" + mobileAgentName + "]", "Тревога, тревога, волк унес зайчат!", null, 0);
			//}

		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		//Start GPS tracking
		try {
			watchdogService.startAlertTrackSending();
			watchdogService.startEvacuationStateCorrector();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}

		watchdogService.beepA();
		
		AnalyticsAdapter.trackInAlertMode(alertReason);
		
		try {
			cancel();
			timer.purge();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	
	private boolean isCanRun() {
		WatchdogService watchdogService = getA().getWatchdogService();
		if (watchdogService == null) {
			return false;
		}
		
		SensorData accelerometerData = watchdogService.accelerometerData;
		if (accelerometerData == null) {
			return false;
		}
		return true;//accelerometerData.getCountOfValues() > 1; 
	}


	private long lastMesasureTime = -1;
	private float[] oldPosition = new float[3];
	private float[] oldVelocity = new float[3];
	// Средний рывок за последние 1.5 s
	public float getAverageJerk() {
		WatchdogService watchdogService = getA().getWatchdogService();
		
		SensorData accelerometerData = watchdogService.accelerometerData;
		//AccSensorListener sensorEventListener = watchdogService.sensorEventListener;
		
		if(lastMesasureTime < 0) {
			lastMesasureTime = System.currentTimeMillis();
		}
		
		long timeInNanos = System.nanoTime();
		float deltaTimeInSecs = 0.5f;
		Sensors.AverageValue jvalue = Sensors.averageValue(accelerometerData,
				timeInNanos, 1500000000L);
		Sensors.AverageValue iposition = Sensors.valueI3(accelerometerData,
		timeInNanos, 500000000L);//==deltaTimeInSecs
		
		float[] jerk = new float[3];
		if (jvalue == null) {
			System.arraycopy(accelerometerData.a, 0, jerk, 0, 3);
		} else {
			jerk[0] = num(jvalue.x);
			jerk[1] = num(jvalue.y);
			jerk[2] = num(jvalue.z);
		}

		deltaTimeInSecs = (System.currentTimeMillis() - lastMesasureTime) / 1000f;
		if (iposition != null) {
			position[0] = oldPosition[0] + iposition.x;// + velocity[0] * deltaTimeInSecs;//velocity[0] * deltaTimeInSecs + (acceleration[0] / 2f) * deltaTimeInSecs * deltaTimeInSecs;
			position[1] = oldPosition[1] + iposition.y;// + velocity[1] * deltaTimeInSecs;//velocity[1] * deltaTimeInSecs + (acceleration[1] / 2f) * deltaTimeInSecs * deltaTimeInSecs;
			position[2] = oldPosition[2] + iposition.z;// + velocity[2] * deltaTimeInSecs;//velocity[2] * deltaTimeInSecs + (acceleration[2] / 2f) * deltaTimeInSecs * deltaTimeInSecs;
		}
		if( deltaTimeInSecs > 1.5f ) {
			oldPosition[0] = position[0];
			oldPosition[1] = position[1];
			oldPosition[2] = position[2];
			//TODO: correction of low velocity
			oldVelocity[0] = velocity[0];
			oldVelocity[1] = velocity[1];
			oldVelocity[2] = velocity[2];
			lastMesasureTime = System.currentTimeMillis();
		}

		float absoluteJerkValue = NumericTools.norma(jerk);
		
		if (maxJerkValue < absoluteJerkValue) {
			maxJerkValue = absoluteJerkValue;
		}
		float velocityValue = norma(velocity);
		if (maxVelocityValue < velocityValue) {
			maxVelocityValue = velocityValue;
		}
		float shiftValue = norma(position);
		if (maxShiftValue < shiftValue) {
			maxShiftValue = shiftValue;
		}
//		MinMaxValue minMaxAcc = Sensors.minMaxValueLPF(accelerometerData, timeInNanos, 500000000L, 0.001f, 1);
		MinMaxSqValue minMaxAccelerationValue = Sensors.minMaxSqabsValueWithAveragingByNPointsLPF(accelerometerData,
				1, timeInNanos, 500000000L, 2, 0.001f);
		if ( minMaxAccelerationValue != null ) {
			maxAbsAccelerationValue = FloatMath.sqrt(minMaxAccelerationValue.maxValue);
//			float value = 0;
//			for (int i = 0; i < 3; i++) {
//				float delta = minMaxAcc.maxValue[i] - minMaxAcc.minValue[i];
//				value += delta * delta;
//			}
//			maxAbsAccelerationValue = FloatMath.sqrt(value);
		}
		if (maxMaxAccelerationValue < maxAbsAccelerationValue) {
			maxMaxAccelerationValue = maxAbsAccelerationValue;
		}
		MinMaxValue minMaxAngles = Sensors.minMaxValueAverageByNPointsLPF(accelerometerData, 2, timeInNanos, 1000000000L, 0.001f, 4);
		if (minMaxAngles == null) {
			return absoluteJerkValue;
		}
		for (int i = 0; i < 3; i++) {
			minAngle[i] = Math.min(minMaxAngles.minValue[i], minAngle[i]);
			maxAngle[i] = Math.max(minMaxAngles.maxValue[i], maxAngle[i]);
		}
		
		if (nextInitialMeasure) {
			initialAngleX = num100(90 - minMaxAngles.avgValue[0]);
			initialAngleY = num100(90 - minMaxAngles.avgValue[1]);
			nextInitialMeasure = false;
			prevAngleX = initialAngleX;
			prevAngleY = initialAngleY;
		}
		angleX = num100(90 - minMaxAngles.avgValue[0]);
		angleY = num100(90 - minMaxAngles.avgValue[1]);
		
		//Calculate average angle
		if (preAlertMode) {
			avgAngleX += angleX;
			avgAngleY += angleY;
		}
		
		if (getA().isDebugMode()) {
			//float[] angle = accelerometerData.ANGLES;
			//MinMaxSqValue minMaxJerkValue = watchdogService.getMinMaxJerkValue();
			status =" trs: " + num100(maxJerk) +
					//"\ns: " + num100(shiftValue) + " cm pc: " + (minMaxAccelerationValue != null ? minMaxAccelerationValue.count : 0) + " <" + num100(maxShiftValue) +
					"\nx: " + ffn((prevAngleX + angleX) / 2f, 7) + ", y: " + ffn((prevAngleY + angleY) / 2f, 7) +
					"\na: " + ffn(maxAbsAccelerationValue, 5) + " < " + ffn(maxMaxAccelerationValue, 5);
					//"\n: " + num100(maxAngle[0]) + "," + num100(maxAngle[1]) + "," + num100(maxAngle[2]);
			//status += "\nmax: " + num100(maxJerkValue) + " cm/s^3";
			//status += "\n" + num100(velocityValue) + " cm/s max: " + num100(maxVelocityValue);
		}
		prevAngleX = angleX;
		prevAngleY = angleY;
		return absoluteJerkValue;
	}

}
