package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import ru.hipdriver.android.app.R;

public abstract class FaceSelectCollectionHipdriverFragment extends HipdriverFragment {
	
	private static final String TAG = TextTools.makeTag(FaceSelectCollectionHipdriverFragment.class);

	protected FaceSelectCollectionPagerAdapter getFaceSelectCollectionPagerAdapter() {
		Activity activity = getActivity();
		if (activity == null) {
			Log.w(TAG, "get-face-select-collection-pager-adapter-impossible[activity-is-null]");
			return null;
		}
		ViewPager pager = (ViewPager) activity.findViewById(R.id.face_select_pager);
		if (pager == null) {
			Log.w(TAG, "get-face-select-collection-pager-adapter-impossible[pager-not-found]");
			return null;
		}
		PagerAdapter adapter = pager.getAdapter();
		if (!(adapter instanceof FaceSelectCollectionPagerAdapter)) {
			Log.w(TAG, "get-face-select-collection-pager-adapter-impossible[adapter-not-cast]");
			return null;
		}
		return (FaceSelectCollectionPagerAdapter) adapter; 		
	}
	
	
}
