package ru.hipdriver.android.app;

import android.content.*;
import android.content.res.TypedArray;
import android.widget.*;
import android.util.*;
import ru.hipdriver.android.app.R;

public class StatusBar extends LinearLayout {

	private final static String TAG = StatusBar.class.getSimpleName();

	protected float preferredLayoutWidth;
	protected float preferredLayoutHeight;

	public StatusBar(Context context) {
		super(context);
	}
	
	public StatusBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray ta = context.obtainStyledAttributes(attrs,
				R.styleable.LimitedExtensible);

		final int indexCount = ta.getIndexCount();
		for (int i = 0; i < indexCount; ++i) {
			int attr = ta.getIndex(i);
			switch (attr) {
			case R.styleable.LimitedExtensible_prefLayouthWidth:
				preferredLayoutWidth = ta.getDimension(attr, 0);
				break;
			case R.styleable.LimitedExtensible_prefLayouthHeight:
				preferredLayoutHeight = ta.getDimension(attr, 0);
				break;
			}
		}
		ta.recycle();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
		int parentWidthMode = MeasureSpec.getMode(widthMeasureSpec);

		int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
		int parentHeightMode = MeasureSpec.getMode(heightMeasureSpec);

		DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
		int maxWidth = displayMetrics.widthPixels;
		int maxHeight = displayMetrics.heightPixels;
		int prefWidth = (int) Pixels.dipToPixels(getContext(), preferredLayoutWidth);
		int prefHeight = (int) Pixels.dipToPixels(getContext(), preferredLayoutHeight);

		if ( prefWidth > 0 && parentWidth > 0 &&
			prefWidth >= getSuggestedMinimumWidth() &&
			(prefWidth < parentWidth || prefWidth < maxWidth) ) {
			//Override parent settings
			parentWidth = prefWidth + (getPaddingLeft() + getPaddingRight()) / 2;
		}
		if ( prefHeight > 0 && parentHeight > 0 &&
			prefHeight >= getSuggestedMinimumHeight() &&
			(prefHeight < parentHeight || prefHeight < maxHeight) ) {
			//Override parent settings
			parentHeight = prefHeight + (getPaddingTop() + getPaddingBottom()) /2;
		}
		super.onMeasure(MeasureSpec.makeMeasureSpec(parentWidth, parentWidthMode),
						MeasureSpec.makeMeasureSpec(parentHeight, parentHeightMode));
	}
}
