package ru.hipdriver.android.app.sslucs;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.android.app.HipdriverApplication;
import ru.hipdriver.android.i.RequestCallback;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.ISid;
import ru.hipdriver.util.ChatMsgParser;
import android.content.Context;
import android.util.Log;

class ChatConnectionWithClientRoleTask extends ChatConnectionTask {
	
	private static final String TAG = TextTools
			.makeTag(ChatConnectionWithClientRoleTask.class);
	//By default implementing policy: one answer key - one instance
	//Answer key is class or enum.
	public static final int MAX_LONG_TTL_ANSWERS_COUNT = 2;

	//disjunctive keys
	private static class LongTtlKey {
		private final long requestId;
		private final Object answerKey;
		public LongTtlKey(long requestId, Object answerKey) {
			super();
			this.requestId = requestId;
			this.answerKey = answerKey;
		}
		public long getRequestId() {
			return requestId;
		}
		public Object getAnswerKey() {
			return answerKey;
		}
		//Define as disjunction keys
		@Override
		public int hashCode() {
			return 0;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			LongTtlKey other = (LongTtlKey) obj;
			if (requestId == other.requestId) {
				return true;
			}
			if (answerKey == null && other.answerKey == null) {
				return true;
			}
			if (answerKey != null && answerKey.equals(other.answerKey)) {
				return true;
			}
			return false;
		}
		@Override
		public String toString() {
			return "LongTtlKey [requestId=" + requestId + ", answerKey="
					+ answerKey + "]";
		}
	}
	
	//Keys unique and access-time-separated, exclude timeout task
	private Map<Long, WaitingForReply> answers = new ConcurrentHashMap<Long, WaitingForReply>();
	private Map<LongTtlKey, WaitingForReply> longTtlAnswers = new HashMap<LongTtlKey, WaitingForReply>(MAX_LONG_TTL_ANSWERS_COUNT);
	private Timer removeAnswersTimer = new Timer("timer: remove-answers-by-timeout");

	private int requestsCount;

	public ChatConnectionWithClientRoleTask(Context context, long mobileAgentId, InetAddress chatServerAddress, int port) {
		super(context, mobileAgentId, chatServerAddress, port);
	}

	public long sendRequest(long mobileAgentIdTo, Object object, Class<?>[] returnValueTypes, RequestCallback requestCallback, long timeout, TimeUnit unit, boolean longTtl, int delayedPacketFactor, Object key) throws JsonGenerationException, JsonMappingException, IOException {
		if (!isRunning()) {
			throw new IllegalStateException("Connection not establishement");
		}
		if (!isConnected()) {
			return ISid.INVALID_COMPONENT_SESSIONS_ID;
		}
		//TODO: remove old answers (with timeout > ...)
		long requestId = getNextRequestId();
		ObjectMapper om = getObjectMapper();
		String body = om.writeValueAsString(object);
		WaitingForReply answerFuture = new WaitingForReply(returnValueTypes, requestCallback, timeout, unit, longTtl, delayedPacketFactor, key);
		answers.put(requestId, answerFuture);
		createTaskRemoveAfterTimeout(requestId, answerFuture.getTimeout(), answerFuture.getUnit());
		sendMessage(mobileAgentIdTo, requestId, body);
		return requestId;
	}
	
	private void createTaskRemoveAfterTimeout(final long requestId, final long timeout, final TimeUnit unit) {
		if (timeout <= 0) {
			Log.w(TAG, "illegal-timeout-value");
			return;
		}
		removeAnswersTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				WaitingForReply answer = answers.get(requestId);
				if (answer == null) {
					return;
				}
				if (answer.isAnswered()) {
					return;
				}
				//Check long ttl answer
				if (answer.isLongTtl()) {
					updateLongTtlAnswers(requestId);
				} else if (answer.isWaitForDelayedPacket()) {
					createTaskRemoveAfterTimeout(requestId, unit.toMillis(timeout) * (answer.getDelayedPacketFactor() - 1), TimeUnit.MILLISECONDS);
				} else {
					answers.remove(requestId);
				}
				RequestCallback requestCallback = answer.getRequestCallback();
				if (requestCallback == null) {
					return;
				}
				requestCallback.onResponse(null, false, true);
			}
			
		}, unit.toMillis(timeout));
		//Purge every 10 additions
		if (requestsCount++ % 10 == 0) {
			removeAnswersTimer.purge();
		}
	}

	public Object waitForAnswer(long requestId) throws InterruptedException {
		WaitingForReply answer = answers.get(requestId);
		if (answer == null) {
			Log.w(TAG, "request-id-not-found");
			return null;
		}
		TimeUnit unit = answer.getUnit();
		long timeout = answer.getTimeout();
		long timer = System.currentTimeMillis() + unit.toMillis(timeout);
		while (timer >= System.currentTimeMillis()) {
			answer = answers.get(requestId);
			if (answer == null) {
				return null;
			}
			if (answer.isAnswered()) {
				return answer.getValue();
			}
			Thread.sleep(100);
		}
		return answer.getValue();
	}

	protected void parseAnswer(String msg) {
		setFirstMessageReceived(true);
		Log.d(TAG, "receive-mesage-with-length :" + (msg != null ? msg.getBytes().length : 0) + "bytes");
		String body = ChatMsgParser.getBody(msg);
		if (body == null) {
			//Special messages or wrong messages
			if (msg == null) {
				Log.w(TAG, "null-msg-can-not-be-parsed");
				return;
			}
			if (msg.startsWith(NO_DEST_CH)) {
				//TODO: retry send after some time;
				//LAST REQUEST FAILED!!!!
				long requestId = ChatMsgParser.getComponentSessionsId(msg);
				WaitingForReply answer = answers.remove(requestId);
				if (answer == null) {
					//no-listeners-for-request[0] normal answer after registration
					Log.d(TAG, String.format("no-listeners-for-request[%s]", requestId));
					return;
				}
				answer.setAnswered(true);
				if (answer.getRequestCallback() == null) {
					return;
				}
				answer.getRequestCallback().onResponse(null, true, false);
			}
			return;
		}
		try {
			long requestId = ChatMsgParser.getComponentSessionsId(msg);
			WaitingForReply answer = updateLongTtlAnswers(requestId);
			if (answer == null) {
				Log.w(TAG, "response-without-request-or-too-later");
				return;
			}
			Class<?>[] returnValueTypes = answer.getReturnValueTypes();
			final Object returnValue;
			if (returnValueTypes != null) {
				returnValue = getFirstSuccessOrThrowException(body, returnValueTypes);
			} else {
				returnValue = body;
			}
			answer.setValue(returnValue);
			answer.setAnswered(true);
			if (answer.getRequestCallback() == null) {
				return;
			}
			answer.getRequestCallback().onResponse(returnValue, false, false);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	private Object getFirstSuccessOrThrowException(String body,
			Class<?>[] returnValueTypes) {
		ObjectMapper om = getObjectMapper();
		for (Class<?> returnValueType : returnValueTypes) {
			try {
				return om.readValue(body, returnValueType);
			} catch (Throwable th) { }
		}
		throw new IllegalStateException("Not parseable!");
	}

	private WaitingForReply updateLongTtlAnswers(long requestId) {
		WaitingForReply answer = answers.remove(requestId);
		if (answer == null) {
			return longTtlAnswers.get(new LongTtlKey(requestId, null));
		}
		//Move to long time to live answers
		if (answer.isLongTtl()) {
			//Check overload case
			if (longTtlAnswers.size() > MAX_LONG_TTL_ANSWERS_COUNT) {
				for (int i = longTtlAnswers.size(); i > MAX_LONG_TTL_ANSWERS_COUNT; i--) {
					LongTtlKey key = longTtlAnswers.keySet().iterator().next();
					longTtlAnswers.remove(key);
				}
			}
			LongTtlKey longTtlKey = new LongTtlKey(requestId, answer.getKey());
			//Hack, TODO: make data structure like HashMap but with disjunction keys
			longTtlAnswers.remove(longTtlKey);
			longTtlAnswers.put(longTtlKey, answer);
		}
		return answer;
	}

	@Override
	protected SimpleChannelInboundHandler<String> makeChatClientHandler() {
		return new ChatClientHandlerForClientTask(this);
	}

	@Override
	protected void onStop() {
		removeAnswersTimer.cancel();
	}

	public void restoreLongttlRequestAnswer(long requestId, Class<?>[] returnValueTypes,
			RequestCallback requestCallback, long timeout, TimeUnit unit, Object key) {
		//Delayed packet factor is arbitrary choice
		int delayedPacketFactor = 6;
		WaitingForReply answerFuture = new WaitingForReply(returnValueTypes, requestCallback, timeout, unit, true, delayedPacketFactor, key);
		answers.put(requestId, answerFuture);
		updateLongTtlAnswers(requestId);
	}

	@Override
	protected boolean checkStopCondition() {
		//TODO: remove this hack after fix reason of issue, it's only workaround
		if (context instanceof HipdriverApplication) {
			switch(((HipdriverApplication) context).getControlMode()) {
				case INTERNET:
					return false;
				case PHONE_CALLS:
				case SMS:
					Log.w(TAG, "check-stop-condition[true]");
					return true;
			}
		}
		return false;
	}

}

class ChatClientHandlerForClientTask extends ChatClientHandler<ChatConnectionWithClientRoleTask> {

	public ChatClientHandlerForClientTask(
			ChatConnectionWithClientRoleTask chatConnectionTask) {
		super(chatConnectionTask);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg)
			throws Exception {
		chatConnectionTask.parseAnswer(msg);
	}

}

class WaitingForReply {
	private boolean answered;
	private final Class<?>[] returnValueTypes;
	private final long time = System.currentTimeMillis(); 
	private final AtomicReference<Object> valueRef = new AtomicReference<Object>();
	private final RequestCallback requestCallback;
	private final long timeout;
	private final TimeUnit unit;
	private final boolean longTtl;
	private final int delayedPacketFactor;
	private final Object key;
	
	public WaitingForReply(Class<?>[] returnValueTypes, RequestCallback requestCallback, long timeout, TimeUnit unit, boolean longTtl, int delayedPacketFactor, Object key) {
		this.requestCallback = requestCallback;
		this.returnValueTypes = returnValueTypes;
		this.timeout = timeout;
		this.unit = unit;
		this.longTtl = longTtl;
		this.delayedPacketFactor = delayedPacketFactor;
		this.key = key;
	}
	public boolean isWaitForDelayedPacket() {
		long timeoutInMillis = unit.toMillis(timeout);
		return ( time + timeoutInMillis * delayedPacketFactor ) < System.currentTimeMillis(); 
	}
	public boolean isAnswered() {
		return answered;
	}
	public void setAnswered(boolean answered) {
		this.answered = answered;
	}
	public Object getValue() {
		return valueRef.get();
	}
	public void setValue(Object value) {
		this.valueRef.set( value );
	}
	public long getTime() {
		return time;
	}
	public RequestCallback getRequestCallback() {
		return requestCallback;
	}
	public Class<?>[] getReturnValueTypes() {
		return returnValueTypes;
	}
	public long getTimeout() {
		return timeout;
	}
	public TimeUnit getUnit() {
		return unit;
	}
	public boolean isLongTtl() {
		return longTtl;
	}
	public int getDelayedPacketFactor() {
		return delayedPacketFactor;
	}
	public Object getKey() {
		return key;
	}
	
}