package ru.hipdriver.android.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.i.EditorCallback;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.ControlModsEnum;
import ru.hipdriver.i.support.DBLinks;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class RemoteControlSettingsFragment extends FaceSelectCollectionHipdriverFragment {

	private static final String TAG = TextTools.makeTag(RemoteControlSettingsFragment.class);
	
	private EditorCallback onePickEditorCallback;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View  view = inflater.inflate(R.layout.remote_control_settings_fragment, container, false);
		refresh(view);
		if (onePickEditorCallback != null) {
			onePickEditorCallback = null;
		}
		return view;
	}

	private void refresh(View view) {
		if (view == null) {
			Log.w(TAG, "view-is-null[refresh]");
			return;
		}
		final Activity activity = getActivity();
		final HipdriverSpinner chooseDeviceSpiner = (HipdriverSpinner) view.findViewById(R.id.select_device_button);
		chooseDeviceSpiner.setSuppressOnItemSelected(true);
		final View selectDeviceRow = view.findViewById(R.id.select_device_row);
		
		
		final ArrayList<DeviceInfo> devices = new ArrayList<DeviceInfo>();
		addDefaultDeviceInfo(devices);
		DeviceAdapter adapter = new DeviceAdapter(activity, R.layout.my_spinner, devices);
		//adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		UITaskFactory.getMymas(getA(), new After() {
			@Override
			public void success(Object returnValue) {
				if (returnValue == null) {
					return;
				}
				devices.clear();
				long selfAgentId = getA().getLong(ServicesConnector.MOBILE_AGENT_ID_KEY, 0L);
				@SuppressWarnings("unchecked")
				Map<Long, String> masMap = (Map<Long, String>) returnValue;
				getA().setInt(HipdriverApplication.MOBILE_AGENT_COUNT, masMap.size());
				for (Entry<Long, String> e : masMap.entrySet()) {
					long mobileAgentId = e.getKey();
					//Filter for self device id
					if (mobileAgentId == selfAgentId) {
						continue;
					}
					String name = e.getValue();
					devices.add(new DeviceInfo(name, mobileAgentId));
				}
				final int countOfDevices = devices.size();
				if (countOfDevices > 0) {
					final int idx = getA().getInt(MainActivity.SELECTED_DEVICE, -1);
					if (idx < countOfDevices  && idx >= 0) {
						chooseDeviceSpiner.setSelection(idx);
					} else {
						chooseDeviceSpiner.setSelection(0);
					}
					chooseDeviceSpiner.refreshDrawableState();
					chooseDeviceSpiner.invalidate();
					chooseDeviceSpiner.setEnabled(true);
					chooseDeviceSpiner.postDelayed(new Runnable() {

						@Override
						public void run() {
							chooseDeviceSpiner.setSuppressOnItemSelected(false);
							//No selected device
							if (idx == -1 && countOfDevices == 1) {
								//Select device
								chooseDeviceSpiner.setSelection(0);
							}
							if (idx == -1 && countOfDevices > 1) {
								//Select device
								chooseDeviceSpiner.performClick();
							}
						}
						
					}, TimeUnit.SECONDS.toMillis(1));
				} else {
					getA().makeToastWarning(R.string.enable_more_then_one_device);
					addDefaultDeviceInfo(devices);
				}
			}
		});
		//TODO: refresh (make http request) after drop-down list
		chooseDeviceSpiner.setAdapter(adapter);
		OnItemSelectedListener onItemSelectedListener = new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if (chooseDeviceSpiner.isSuppressOnItemSelected()) {
					return;
				}
				//Prevent non user input
				if (!chooseDeviceSpiner.isEnabled()) {
					return;
				}
				//long prevId = getA().getLong(SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
				getA().setInt(MainActivity.SELECTED_DEVICE, position);
				if (position < 0) {
					return;
				}
				if (position >= devices.size()) {
					return;
				}
				DeviceInfo deviceInfo = devices.get(position);
				long mobileAgentId = deviceInfo.getMobileAgentId();
				getA().setLong(MainActivity.SELECTED_DEVICE_ID, mobileAgentId);
				String deviceName = deviceInfo.getDeviceName();
				getA().setString(MainActivity.SELECTED_DEVICE_NAME, deviceName);
				//Show phone number editor after user-choice
				//if (prevId != mobileAgentId) {
					showPhoneEditor(deviceName);
				//}
				checkControlModeForRemoteMobileAgent();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) { }
		};
		chooseDeviceSpiner.setOnItemSelectedListener(onItemSelectedListener);
		chooseDeviceSpiner.setOnItemSelectedEvenIfUnchangedListener(onItemSelectedListener);
		chooseDeviceSpiner.setEnabled(false);
		
		final RadioButton basicControlModeButton = (RadioButton) view.findViewById(R.id.internet_control_mode_radiobutton);
		basicControlModeButton.setText(Html.fromHtml(getResources().getString(R.string.internet_control_mode)));
		final RadioButton reserveControlModeButton = (RadioButton) view.findViewById(R.id.phone_calls_control_mode_radiobutton);
		reserveControlModeButton.setText(Html.fromHtml(getResources().getString(R.string.phone_calls_control_mode)));

		OnClickListener controlModeOnClickListener = new OnClickListener() {
			@Override
			public void onClick(View view) {
				int viewId = view.getId();
				if (viewId != R.id.internet_control_mode_radiobutton &&
						viewId != R.id.phone_calls_control_mode_radiobutton) {
					return;
				}
			    // Check which radio button was clicked
				Intent intent = new Intent(activity, SwitchControlModeActivity.class);
			    switch(viewId) {
			    case R.id.internet_control_mode_radiobutton:
					intent.putExtra(MainActivity.EXTRA_SWITCH_TO_CONTROL_MODE, ControlModsEnum.INTERNET);
			    	break;
			    case R.id.phone_calls_control_mode_radiobutton:
					intent.putExtra(MainActivity.EXTRA_SWITCH_TO_CONTROL_MODE, ControlModsEnum.PHONE_CALLS);
			    	break;
			    }
			    ((MainActivity) activity).dispatchResult(MainActivity.SWITCH_TO_CONTROL_MODE_REQUEST, RemoteControlSettingsFragment.this);
				startActivityForResult(intent, MainActivity.SWITCH_TO_CONTROL_MODE_REQUEST);
			}
		};
		reserveControlModeButton.setOnClickListener(controlModeOnClickListener);
		basicControlModeButton.setOnClickListener(controlModeOnClickListener);		
		//Toggle current mode
		checkControlModeForRemoteMobileAgent();
		
		//Install map providers enum behavior 
		new MapProvidersEnumControl(getA(), view);
	}
	
	protected void addDefaultDeviceInfo(final ArrayList<DeviceInfo> devices) {
		if (getActivity() == null) {
			return;
		}
		String selectDevice = getString(R.string.select_device);
		String selectedDeviceName = getA().getString(MainActivity.SELECTED_DEVICE_NAME, selectDevice);//TODO: "select device" instead "-"
		long selectedDeviceId = getA().getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
		devices.add(new DeviceInfo(selectedDeviceName, selectedDeviceId));//Without this line drop 
	}
	
	/**
	 * Show dialog for path for accelerometer track.
	 * 
	 * @param uiContext
	 *            Activity context.
	 */
	public void showPhoneEditor(String deviceName) {
		final FragmentActivity activity = getActivity();
		if (activity == null) {
			Log.w(TAG, "can-not-show-phone-editor[activity is null]");
			return;
		}
		final AlertDialog.Builder phoneNumberDialog = new AlertDialog.Builder(
				activity);
		final String prefKeyName = getA().getSelectedDeviceForRemoteControlPhoneNumberPrevKey();

		String phoneNumber = getA().getString(prefKeyName, "");

		TextView titleTextView = new TextView(activity);
		titleTextView.setText(Html.fromHtml(getResources().getString(R.string.show_phone_editor_$title$_text_view, deviceName)));
		titleTextView.setTextAppearance(activity,
                android.R.style.TextAppearance_Large);
		titleTextView.setHint(R.string.phone_number_editor_hint);
		titleTextView.setEms(10);
		titleTextView.setPadding(10, 0, 10, 0);
		// Setting Dialog Title
		phoneNumberDialog
				.setCustomTitle(titleTextView);

		// Setting Dialog Views
		LinearLayout linearLayout = (LinearLayout) activity.getLayoutInflater().inflate(R.layout.phone_number_editor, null);
		if (linearLayout == null) {
			Log.wtf(TAG, "null-layout");
			return;
		}
		final EditText phoneNumberEditor = (EditText) linearLayout.findViewById(R.id.dialog_phone_number_editor);
		phoneNumberEditor.setText(phoneNumber);
		phoneNumberEditor.setInputType(InputType.TYPE_CLASS_PHONE);
		phoneNumberEditor.setTextColor(getResources().getColor(R.color.phone_number_editor_text_color));
		phoneNumberEditor.setKeepScreenOn(true);
		phoneNumberEditor.setSingleLine(true);
		phoneNumberEditor.addTextChangedListener(new TextWatcher() {

			public void beforeTextChanged(CharSequence text, int start,
					int count, int after) {
			}

			public void onTextChanged(CharSequence text, int start, int before,
					int count) {
			}

			public void afterTextChanged(Editable text) {
				getA().setString(prefKeyName, text.toString());
			}

		});
		
		Button phoneBookButton = (Button) linearLayout.findViewById(R.id.dialog_phone_number_button);
		phoneBookButton.setText("...");
		phoneBookButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,  
			    		ContactsContract.Contacts.CONTENT_URI);
			    setOnePickEditorCallback(new EditorCallback() {

					@Override
					public void onActivityResult(int resultCode,
							Intent data) {
					    if (resultCode != Activity.RESULT_OK) {
						       //activity result error actions
					    }
			            Cursor cursor = null;  
			            String phoneNumber = "";
			            List<String> allNumbers = new ArrayList<String>();
			            int phoneIdx = 0;
			            try {  
			                Uri result = data.getData();  
			                String id = result.getLastPathSegment();  
			                cursor = activity.getContentResolver().query(Phone.CONTENT_URI, null, Phone.CONTACT_ID + "=?", new String[] { id }, null);  
			                phoneIdx = cursor.getColumnIndex(Phone.DATA);
			                if (cursor.moveToFirst()) {
			                    while (cursor.isAfterLast() == false) {
			                        phoneNumber = cursor.getString(phoneIdx);
			                        allNumbers.add(phoneNumber);
			                        cursor.moveToNext();
			                    }
			                } else {
			                    //no results actions
			                }  
			            } catch (Exception e) {  
			               //error actions
			            } finally {  
			                if (cursor != null) {  
			                    cursor.close();
			                }
			                phoneNumberEditor.setText(phoneNumber);
			                if (phoneNumber.length() == 0) {  
			                    //no numbers found actions  
			                }  
			            }  
					}
			    	
			    });
			    ((MainActivity) activity).dispatchResult(MainActivity.CONTACT_PICKER_REQUEST, RemoteControlSettingsFragment.this);
			    activity.startActivityForResult(contactPickerIntent, MainActivity.CONTACT_PICKER_REQUEST);
			    
			}
		});

		phoneNumberDialog.setView(linearLayout);
		
		// On pressing Settings button
		phoneNumberDialog.setPositiveButton(
				R.string.phone_number_dialog_positive_button,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						//TODO: OK event supported
					}
				});
		
		// Showing Alert Message
		AlertDialog alertDialog = phoneNumberDialog.show();
		Button positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
		positiveButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22f);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode) {
		case MainActivity.CONTACT_PICKER_REQUEST:
			afterContactPick(resultCode, data);
			break;
		case MainActivity.SWITCH_TO_CONTROL_MODE_REQUEST:
			afterSwitchToControlMode(resultCode, data);
			break;
		}
	}
	
	private void afterContactPick(int resultCode, Intent data) {
		if (onePickEditorCallback == null) {
			return;
		}
		onePickEditorCallback.onActivityResult(resultCode, data);
	}
	
	protected void setOnePickEditorCallback(EditorCallback editorCallback) {
		this.onePickEditorCallback = editorCallback;
	}
	
	protected void checkControlModeForRemoteMobileAgent() {
		long remoteMobileAgentId = getA().getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
		if (remoteMobileAgentId == IMobileAgent.INVALID_ID) {
			ControlModsEnum controlMode = getA().getControlMode();
			afterSwitchToControlMode(controlMode);
		} else {
			UITaskFactory.getMap(getA(), new After() {
	
				@Override
				public void success(Object returnValue) {
					Properties properties = (Properties) returnValue;
					Object controlModeId = properties.get(IMobileAgentProfile.CONTROL_MODE_ID);
					if (controlModeId == null) {
						ControlModsEnum controlMode = getA().getControlMode();
						afterSwitchToControlMode(controlMode);
						return;
					}
					String controlModeIdText = String.valueOf(controlModeId);
					short controlModeIdShort = -1;
					try {
						controlModeIdShort = Short.parseShort(controlModeIdText);
					} catch (NumberFormatException e) {
						ControlModsEnum controlMode = getA().getControlMode();
						afterSwitchToControlMode(controlMode);
						return;
					}
					ControlModsEnum currentControlModeForRemoteAgent = DBLinks.unlinkControlMode(controlModeIdShort);
					afterSwitchToControlMode(currentControlModeForRemoteAgent);
				}
	
				@Override
				public void failure(int returnCode, String failureReason) {
					ControlModsEnum controlMode = getA().getControlMode();
					afterSwitchToControlMode(controlMode);
				}
				
			}, remoteMobileAgentId);
		}
	}
	
	private void afterSwitchToControlMode(int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			//Rollback changes
			afterSwitchToControlMode(getA().getControlMode());
			return;
		}
		ControlModsEnum targetControlMode = (ControlModsEnum) data.getSerializableExtra(MainActivity.EXTRA_SWITCH_TO_CONTROL_MODE);
		afterSwitchToControlMode(targetControlMode);
		setupConnectorToChatServer(targetControlMode);
	}

	private void setupConnectorToChatServer(ControlModsEnum controlMode) {
		if (controlMode == null) {
			return;
		}
		switch (controlMode) {
		case INTERNET:
			getA().makeUserCommandsSender();
			break;
		case PHONE_CALLS:
		case SMS:
			getA().releaseUserCommandsSender();
			break;
		}
	}

	protected void afterSwitchToControlMode(ControlModsEnum targetControlMode) {
		View view = getView();
		if (view == null) {
			Log.w(TAG, "view-is-null[afterSwitchToControlMode]");
			return;
		}
		if (targetControlMode == null) {
			Log.w(TAG, "null-control-mode-but-result-ok");
			return;
		}
		final RadioButton basicControlModeButton = (RadioButton) view.findViewById(R.id.internet_control_mode_radiobutton);
		final RadioButton reserveControlModeButton = (RadioButton) view.findViewById(R.id.phone_calls_control_mode_radiobutton);
		if (basicControlModeButton == null) {
			Log.w(TAG, String.format("can-not-find-view[%s]", R.id.internet_control_mode_radiobutton));
			return;
		}
		switch (targetControlMode) {
		case INTERNET:
			//resetFocus();

			getA().setControlMode(ControlModsEnum.INTERNET);

			reserveControlModeButton.post(new Runnable() {

				@Override
				public void run() {
					reserveControlModeButton.setChecked(false);
					basicControlModeButton.toggle();
				}
				
			});
			break;
		case PHONE_CALLS:
		case SMS:
			//resetFocus();

			getA().setControlMode(ControlModsEnum.PHONE_CALLS);

			reserveControlModeButton.post(new Runnable() {

				@Override
				public void run() {
					basicControlModeButton.setChecked(false);
					reserveControlModeButton.toggle();
				}
			});
			break;
		}
	}

}
