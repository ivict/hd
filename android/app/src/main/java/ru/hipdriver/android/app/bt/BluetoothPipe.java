package ru.hipdriver.android.app.bt;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ru.hipdriver.android.util.TextTools;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class BluetoothPipe extends Thread {
    private static final String TAG = TextTools.makeTag(BluetoothPipe.class);
	private final BluetoothSocket bluetoothSocket;
    private final InputStream in;
    private final OutputStream out;
    
    private final BluetoothDataHandler dataHandler;
 
    public BluetoothPipe(BluetoothSocket socket, BluetoothDataHandler dataHandler) {
        bluetoothSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
 
        // Get the input and output streams, using temp objects because
        // member streams are final
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
        	Log.wtf(TAG, e);
        }
 
        in = tmpIn;
        out = tmpOut;
        
        this.dataHandler = dataHandler;
    }
 
    public void run() {
    	int pixtureSize = 160 * 120 * 2;
        byte[] buffer = new byte[pixtureSize];  // buffer store for the stream
        int bytes; // bytes returned from read()
 
        // Keep listening to the InputStream until an exception occurs
        while (!isInterrupted()) {
            try {
                // Read from the InputStream
                bytes = in.read(buffer);
                // Handling the obtained bytes
                if (dataHandler == null) {
                	Log.d(TAG, "handler-is-null");
                	break;
                }
                dataHandler.handle(bluetoothSocket.getRemoteDevice(), bytes, -1, buffer);
            } catch (IOException e) {
                if (dataHandler == null) {
                	Log.d(TAG, "handler-is-null");
                	Log.wtf(TAG, e);
                	return;
                }
                dataHandler.error(e);
            }
        }
    }
 
    /* Call this from the main activity to send data to the remote device */
    public void write(byte[] bytes) {
        try {
            out.write(bytes);
        } catch (IOException e) {
            if (dataHandler == null) {
            	Log.d(TAG, "handler-is-null");
            	Log.wtf(TAG, e);
            	return;
            }
            dataHandler.error(e);
        }
    }
 
    /* Call this from the main activity to shutdown the connection */
    public void cancel() {
        try {
            bluetoothSocket.close();
        } catch (IOException e) {
        	Log.wtf(TAG, e);
        }
    }
}