package ru.hipdriver.android.app.sslucs;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
//All fields are required for multi-class response resolving
//@see ChatConnectionWithClientRoleTask#parseAnswer
//i.e. ignoreUnknown = false is critical for correct parsing
@JsonIgnoreProperties(ignoreUnknown = false)
public class MobileAgentStateDescription {

	@JsonProperty(value = "d")
	public String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		
}
