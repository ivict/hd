package ru.hipdriver.android.app;

import java.io.Serializable;

import android.content.Context;
import android.text.Html;

public class ActiveText implements Serializable {
	
	/**
	 * Unique ID 
	 */
	private static final long serialVersionUID = 1L;
	
	private final int resourceId;
	
	private final String uri1;
	private final String uri2;
	
	private transient CharSequence text;

	public ActiveText(int resourceId, String uri1, String uri2) {
		this.resourceId = resourceId;
		this.uri1 = uri1;
		this.uri2 = uri2;
	}

	public CharSequence getText(Context context) {
		if (text == null) {
			text = Html.fromHtml(context.getString(resourceId));
		}
		return text;
	}

	public String getUri1() {
		return uri1;
	}

	public String getUri2() {
		return uri2;
	}

}
