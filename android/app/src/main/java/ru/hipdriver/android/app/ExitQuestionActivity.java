package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.support.AppFacesEnum;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;

public class ExitQuestionActivity extends HipdriverActivity {

	protected static final String TAG = TextTools
			.makeTag(ExitQuestionActivity.class);
	protected static final String STOP_APPLICATION_USER_REQUEST = "stop_application";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exit_question_screen);
		
		View exitQuestionCancelButton = findViewById(R.id.exit_question_screen_$cancel$_button);
		exitQuestionCancelButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				fireClickCancel();
			}
		});
		
		View exitQuestionLogoutButton = findViewById(R.id.exit_question_screen_$logout$_button);
		exitQuestionLogoutButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getA().setInt(ServicesConnector.USER_ID_KEY, 0);
                getA().setLong(ServicesConnector.MOBILE_AGENT_ID_KEY, 0L);
                getA().setString(MainActivity.PASSWORD_EDITOR, "");
                getA().setBoolean(ServicesConnector.FIRST_SIGN_UP_KEY, true);
                //Suppress run GoogleMap and YandexMap threads
                getA().setAppFace(AppFacesEnum.CAR_ALARMS);
                //TODO: Clear secure key
                getA().onTerminate();
                safetySleep(100);
                //@see http://stackoverflow.com/questions/5100728/how-to-force-stop-my-android-application-programatically
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
		//exitQuestionLogoutButton.setVisibility(View.GONE);
		
		View exitQuestionStopButton = findViewById(R.id.exit_question_screen_$stop$_button);
		exitQuestionStopButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getA().setBoolean(STOP_APPLICATION_USER_REQUEST, true);
				getA().onTerminate();
				safetySleep(100);
				//@see http://stackoverflow.com/questions/5100728/how-to-force-stop-my-android-application-programatically
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
	}
	
    protected void safetySleep(long millis) {
    	try {
    		Thread.sleep(millis);
    	} catch (InterruptedException e) {
    		Log.wtf(TAG, e);
    	}
	}

	@Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
       	this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
    }
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {     

        if(keyCode != KeyEvent.KEYCODE_BACK)
        {
        	return super.onKeyDown(keyCode, event);
        }
        fireClickCancel();
        return true;
    }

	private void fireClickCancel() {
		finish();
	}

}
