package ru.hipdriver.android.app.sslucs;

import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.android.util.UCExecutor;

public class UcServerHandler {

	private static final String TAG = TextTools.makeTag(UcServerHandler.class);

	final private UCExecutor executor;
	final private ObjectMapper om;
	final private int userId;
	final private long mobileAgentId;

	public UcServerHandler(UCExecutor executor, int userId, long mobileAgentId) {
		this.executor = executor;
		this.om = new ObjectMapper();
		this.userId = userId;
		this.mobileAgentId = mobileAgentId;
	}

//	@Override
//	public void channelRead0(final ChannelHandlerContext ctx, Object msg)
//			throws Exception {
//		// TODO: Fast parsing on wrong message first level of DDoS atack guard.
//		ByteBuf buff = (ByteBuf) msg;
//		byte[] message = (byte[]) buff.array();
//		UserCommand userCommand = om.readValue(message, UserCommand.class);
//		if (userId != userCommand.getUserId()) {
//			ctx.close();
//			return;
//		}
//		if (mobileAgentId != userCommand.getMobileAgentId()) {
//			ctx.close();
//			return;
//		}
//		short userCommandTypeId = userCommand.getUserCommandTypeId();
//		UserCommandTypesEnum userCommandType = UserCommandTypesEnum
//				.valueOfId(userCommandTypeId);
//		byte[] serializedArgs = userCommand.getArgs();
//		final StandardResponse result;
//		if (serializedArgs == null || serializedArgs.length == 0) {
//			result = executor.exec(userCommandType);
//		} else {
//			ObjectInputStream in = new ObjectInputStream(
//					new ByteArrayInputStream(serializedArgs));
//			Object[] args = (Object[]) in.readObject();
//			result = executor.exec(userCommandType, args);
//		}
//		byte[] response = om.writeValueAsBytes(result);
//
//		final ChannelFuture f = ctx.writeAndFlush(Unpooled
//				.wrappedBuffer(response));
//		f.addListener(new ChannelFutureListener() {
//			@Override
//			public void operationComplete(ChannelFuture future) {
//				assert f == future;
//				ctx.close();
//			}
//		});
//	}
//
//	@Override
//	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
//		// Close connection
//		Log.wtf(TAG, cause);
//		ctx.close();
//	}
//
//	@Override
//	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
//		ctx.flush();
//	}

}