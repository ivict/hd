package ru.hipdriver.android.app;

public enum NetworkErrors {
	NO_ERRORS, NULL_RESPONSE;

	private static NetworkErrors[] cache; 
	
	public static NetworkErrors find(int returnCode) {
		if (returnCode < 0) {
			return null;
		}
		if (returnCode > 1) {
			return null;
		}
		if (cache == null) {
			cache = NetworkErrors.values();
		}
		return cache[returnCode];
	} 
}
