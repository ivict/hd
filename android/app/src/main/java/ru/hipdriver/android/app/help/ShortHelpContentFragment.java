package ru.hipdriver.android.app.help;

import ru.hipdriver.android.app.R;
import ru.hipdriver.android.util.TextTools;
import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class ShortHelpContentFragment extends ShortHelpHipdriverFragment {

	private static final String TAG = TextTools.makeTag(ShortHelpContentFragment.class);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.short_help_content_fragment,
				container, false);
		refresh(view);
		return view;
	}

	private void refresh(final View view) {
		if (view == null) {
			return;
		}
		
		View shortHelpExitButton = view.findViewById(R.id.short_help_exit_button);
		shortHelpExitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ShortHelpCollectionPagerAdapter shortHelpCollectionPagerAdapter = getShortHelpCollectionPagerAdapter();
				if (shortHelpCollectionPagerAdapter == null) {
					Log.w(TAG, "short-help-collection-pager-adapter-is-null");
					return;
				}
				shortHelpCollectionPagerAdapter.show(0);
			}
			
		});
	}
	
	public void updateContent(int stringResourceId) {
		Activity activity = getActivity();
		if (activity == null) {
			Log.w(TAG, "can-not-update-content[activity is null]");
			return;
		}
		View view = getView();
		if (view == null) {
			Log.w(TAG, "can-not-update-content[view is null]");
			return;
		}
		TextView contentView = (TextView) view.findViewById(R.id.short_help_content);
		contentView.setText(Html.fromHtml(getResources().getString(stringResourceId)));
	}

}
