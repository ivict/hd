package ru.hipdriver.android.app;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;

import dalvik.system.DexClassLoader;
import ru.hipdriver.android.util.LinuxConstants;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.j.Location;
import ru.hipdriver.j.MobileAgentWithState;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.InstrumentationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.util.Log;

public class Hardwares {
	
	public static final int BUILD_VERSION_CODES_JELLY_BEAN_MR1 = 17;
	public static final String SCREEN_BRIGHTNESS = "screen_brightness";
	public static final String SETTINGS_AIRPLANE_MODE_ON = "airplane_mode_on";

	private static final byte[] NO_BYTES = new byte[0];
	private static final String TAG = TextTools.makeTag(Hardwares.class);
	
	private static Long cachedImei; 
	
	private static ObjectMapper cachedObjectMapper;

    public static class ExecResult {
		private int exitCode;
		private String stdOut;
		private String stdErr;
		
		public ExecResult(int exitCode, String stdOut) {
			this.exitCode = exitCode;
			this.stdOut = stdOut;
		}

		public ExecResult(int exitCode, String stdOut, String stdErr) {
			this.exitCode = exitCode;
			this.stdOut = stdOut;
			this.stdOut = stdErr;
		}
		
		public int getExitCode() {
			return exitCode;
		}
		public void setExitCode(int exitCode) {
			this.exitCode = exitCode;
		}
		public String getStdOut() {
			return stdOut;
		}
		public void setStdOut(String stdOut) {
			this.stdOut = stdOut;
		}
		public String getStdErr() {
			return stdErr;
		}
		public void setStdErr(String stdErr) {
			this.stdErr = stdErr;
		}
		
	}

	private Hardwares() {
	}

	public static long getImei(Context context) {
		TelephonyManager mngr = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		String imei = mngr.getDeviceId();
		if (imei == null) {// Not a phone
			return -1;
		}
		StringBuilder number = new StringBuilder();
		for (int i = 0; i < imei.length(); i++) {
			char ch = imei.charAt(i);
			if (Character.isDigit(ch)) {
				number.append(ch);
			}
		}
		return Long.valueOf(imei);
	}

	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isNetworkAvailable(Context context) {
		return isOnline(context);
	}
	
	/**
	 * Get network operator name.<br>
	 * For GSM network return result in form <pre>MCC_MNC</pre><br>
	 * For CDMA network return result in form <pre>networkId_systemId</pre><br>
	 * If result is empty string cell network unavailable.
	 * @param context executing context.
	 * @return return empty string if cell network not available.
	 */
	public static String getDefaultCellNetworkOperator(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		if (telephonyManager == null) {
			return "";
		}
		switch(telephonyManager.getPhoneType()) {
		case TelephonyManager.PHONE_TYPE_NONE:
			return "";
		case TelephonyManager.PHONE_TYPE_CDMA:
			CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) telephonyManager.getCellLocation();
			if (cdmaCellLocation == null) {
				return "";
			}
			if (cdmaCellLocation.getNetworkId() == -1) {
				return "";
			}
			StringBuilder cdmaNetworkName = new StringBuilder();
			cdmaNetworkName.append(cdmaCellLocation.getNetworkId());
			cdmaNetworkName.append('_').append(cdmaCellLocation.getSystemId());
			return cdmaNetworkName.toString();
		case TelephonyManager.PHONE_TYPE_GSM:
			String networkOperatorName = telephonyManager.getNetworkOperator();
			if (networkOperatorName == null) {
				return "";
			}
			if (networkOperatorName.isEmpty()) {
				return "";
			}
			return networkOperatorName;
		}
		return "";
	}

	public static byte[] getStatus(HipdriverApplication context, AppStatesEnum appState,
			CarStatesEnum carState, int gsmBitErrorRate, int gsmSignalStrength,
			int lat, int lon, int alt, int acc, int mcc, int mnc, int lac,
			int cid) {
		Object maws = getState(context, appState, carState, gsmBitErrorRate,
				gsmSignalStrength, lat, lon, alt, acc, mcc, mnc, lac, cid);
		return safetySerialize(maws);
	}

	public static IMobileAgentState getState(HipdriverApplication context,
			AppStatesEnum appState, CarStatesEnum carState,
			int gsmBitErrorRate, int gsmSignalStrength, int lat, int lon,
			int alt, int acc, int mcc, int mnc, int lac, int cid) {
		MobileAgentWithState maws = new MobileAgentWithState();
		// TODO: Update app state id before sending or use sign-in process
		short appStateId = DBLinks.link(appState);
		maws.setAppStateId(appStateId);
		short carStateId = DBLinks.link(carState);
		maws.setCarStateId(carStateId);
		maws.setGsmBitErrorRate(gsmBitErrorRate);
		//Zero value reserving for no-connection stage in Web UI.
		if (gsmSignalStrength > 0) {
			maws.setGsmSignalStrength(gsmSignalStrength);
		} else {
			maws.setGsmSignalStrength(1);
		}
		updateBatteryInfo(context, maws);
		Location lastLocation = new Location();
		lastLocation.setLat(lat);
		lastLocation.setLon(lon);
		lastLocation.setAlt(alt);
		lastLocation.setAcc(acc);
		lastLocation.setMcc(mcc);
		lastLocation.setMnc(mnc);
		lastLocation.setLac(lac);
		lastLocation.setCid(cid);
		lastLocation.setTime(new Date());
		lastLocation.setCarStateId(carStateId);
		maws.setLastLocation(lastLocation);
		if (lat != ILocation.UNKNOWN_LATITUDE
				&& lon != ILocation.UNKNOWN_LONGITUDE) {
			return maws;
		}
		HttpClient httpClient = new DefaultHttpClient();
		ServicesConnector.safetyUpdateGeoCoordinates(context, httpClient, lastLocation,
				mcc, mnc, lac, cid);
		return maws;
	}

	private static byte[] safetySerialize(Object obj) {
		try {
			ObjectMapper om = getObjectMapper();
			String message = om.writeValueAsString(obj);
			return message.getBytes("UTF8");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return null;
	}

	private static ObjectMapper getObjectMapper() {
		if (cachedObjectMapper == null) {
			 makeObjectMapper();
		}
		return cachedObjectMapper;
	}

	private static synchronized void makeObjectMapper() {
		if (cachedObjectMapper != null) {
			return;
		}
		cachedObjectMapper = new ObjectMapper();
	}

	/**
	 * Based on solution from
	 * http://stackoverflow.com/questions/15746709/get-battery
	 * -level-only-once-using-android-sdk
	 */
	public static void updateBatteryInfo(Context context,
			IMobileAgentState mobileAgentState) {
		Intent batteryIntent = context.registerReceiver(null, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));
		int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

		int batteryStatus = batteryIntent.getIntExtra(
				BatteryManager.EXTRA_STATUS, -1);
		mobileAgentState.setBatteryStatus(batteryStatus);

		// Error checking that probably isn't needed but I added just in case.
		if (level == -1 || scale == -1) {
			return;
		}

		float batteryPct = ((float) level / (float) scale) * 100.0f;
		mobileAgentState.setBatteryPct(batteryPct);
	}
	
	public static boolean isPowerCableUnplugged(Context context) {
		Intent batteryIntent = context.registerReceiver(null, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));
		//int batteryStatus = batteryIntent.getIntExtra(
		//		BatteryManager.EXTRA_STATUS, -1);
		int plugged = batteryIntent.getIntExtra(
				BatteryManager.EXTRA_PLUGGED, -1);
		
		// Error checking that probably isn't needed but I added just in case.
		if (plugged == -1) {
			return false;
		}

		return plugged == 0;
	}
	
	public static float getPowerBatteryPct(Context context) {
		Intent batteryIntent = context.registerReceiver(null, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));
		int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

		// Error checking that probably isn't needed but I added just in case.
		if (level == -1 || scale == -1) {
			return 0f;
		}

		float batteryPct = ((float) level / (float) scale) * 100.0f;
		return batteryPct;
	}
	

	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}
	
	public static boolean isRooted() {
	    return findBinary("su");
	}

	public static boolean findBinary(String binaryName) {
	    boolean found = false;
	    if (!found) {
	        String[] places = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/",
	                "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
	        for (String where : places) {
	            if ( new File( where + binaryName ).exists() ) {
	                found = true;
	                break;
	            }
	        }
	    }
	    return found;
	}

	public static File fullPathForBinary(String binaryName) {
	    File fullPathForBinary = null;
        String[] places = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/",
                "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
        for (String where : places) {
            File path = new File( where + binaryName );
			if ( path.exists() ) {
                fullPathForBinary = path; 
                break;
            }
        }
	    return fullPathForBinary;
	}
	
	public static boolean powerOff() {
		String[] args = {"-p"};
		return su("reboot", args);
	}

	protected static boolean su(String cmd) {
		return su(cmd, null);
	}
	
	protected static boolean su(String cmd, String[] args) {
		File fullPathToSU = fullPathForBinary("su");
		if (fullPathToSU == null) {
			return false;
		}
		File fullPathToReboot = fullPathForBinary(cmd);
		if (fullPathToReboot == null) {
			return false;
		}
		Process su;		
		OutputStreamWriter osw = null;		
		StringBuilder command = new StringBuilder(fullPathToReboot.getAbsolutePath());
		appendToIfNotNull(command, args);
		Log.d(TAG, command.toString());
		
		try {
			su = Runtime.getRuntime().exec(fullPathToSU.getAbsolutePath());
			osw = new OutputStreamWriter(su.getOutputStream());
            osw.write(command.toString());
            osw.flush();
		} catch (IOException e) {
			Log.wtf(TAG, e);
			return false;
		} finally {
			closeWriterIfNotNull(osw);
		}
		
		try {
			su.waitFor();
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		}
		if (su.exitValue() == 0) {
			return true;
		}
		InputStream stdErr = su.getErrorStream();
		StringBuilder err = new StringBuilder();
		copy(new InputStreamReader(stdErr), err);
		Log.w(TAG, err.toString());
		InputStream stdOut = su.getInputStream();
		StringBuilder out = new StringBuilder();
		copy(new InputStreamReader(stdOut), out);
		Log.d(TAG, out.toString());
		return false;
	}

	protected static ExecResult suo(String cmd, String[] args) {
		File fullPathToSU = fullPathForBinary("su");
		if (fullPathToSU == null) {
			return null;
		}
		File fullPathToReboot = fullPathForBinary(cmd);
		if (fullPathToReboot == null) {
			return null;
		}
		Process su;		
		OutputStreamWriter osw = null;		
		StringBuilder command = new StringBuilder(fullPathToReboot.getAbsolutePath());
		appendToIfNotNull(command, args);
		Log.d(TAG, command.toString());
		
		try {
			su = Runtime.getRuntime().exec(fullPathToSU.getAbsolutePath());
			osw = new OutputStreamWriter(su.getOutputStream());
            osw.write(command.toString());
            osw.flush();
		} catch (IOException e) {
			Log.wtf(TAG, e);
			return null;
		} finally {
			closeWriterIfNotNull(osw);
		}
		
		try {
			su.waitFor();
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		}
		InputStream stdOut = su.getInputStream();
		StringBuilder out = new StringBuilder();
		copy(new InputStreamReader(stdOut), out);
		int exitValue = su.exitValue();
		if (exitValue == 0) {
			return new ExecResult(exitValue, out.toString());
		}
		out.append('\n');
		InputStream stdErr = su.getErrorStream();
		StringBuilder err = new StringBuilder();
		copy(new InputStreamReader(stdErr), err);
		return new ExecResult(exitValue, out.toString(), err.toString());
	}

	//@see apache common tools org.apache.commons.io.IOUtils#copyLarge
	public static long copy(Reader input, StringBuilder output) {
		char[] buffer = new char[4096];
        long count = 0;
        int n = 0;
        try {
	        while (-1 != (n = input.read(buffer))) {
	            output.append(buffer, 0, n);
	            count += n;
	        }
        } catch (IOException e) {
        	Log.wtf(TAG, e);
        }
        return count;		
	}

	private static void appendToIfNotNull(StringBuilder command, String[] args) {
		if (args == null) {
			return;
		}
		for (String arg : args) {
			command.append(' ');
			command.append(arg);
		}
	}

	private static void closeWriterIfNotNull(Writer writer) {
		if (writer == null) {
			return;
		}
        try {
            writer.close();
        } catch (IOException e) {
            Log.wtf(TAG, e);                    
        }
	}

	public static boolean checkSUAccess() {
		return su("ls");
	}
	
	//Workaround, use only internal command for Android ioctl from toolbox
	public static byte[] ioctl(String device, boolean openReadOnly, int cmd, byte[] data) {
		String[] args;
		if (data == null) {
			args = makeArgs(device, openReadOnly, cmd);
		} else {
			args = makeArgs(device, openReadOnly, cmd, data);
		}
		ExecResult execResult = suo("ioctl", args);
		if (execResult == null) {
			return null;
		}
		if (execResult.exitCode != 0) {
			return null;
		}
		String out = execResult.getStdOut();
		//Parse return buf:
		String[] outParts = out.split("return buf:");
		if (outParts.length < 2) {
			return NO_BYTES;
		}
		if (outParts.length > 2) {
			Log.w(TAG, "Multiply result?");
		}
		String[] bytes = outParts[1].trim().split("\\s+");
		byte[] outBuf = new byte[bytes.length];
		int i = 0;
		for (String hexByte : bytes) {
			outBuf[i++] = Byte.parseByte(hexByte, 16);
		}
		return outBuf;
	}
	
	private static String[] makeArgs(String device, boolean openReadOnly,
			int cmd, byte[] data) {
		if (data == null) {
			throw new IllegalArgumentException();
		}
		int argid = 0;
		String[] args;
		if (openReadOnly) {
			args = new String[data.length + 7];
			args[argid++] = "-r";
		} else {
			args = new String[data.length + 6];
		}
		args[argid++] = "-l";
		args[argid++] = Integer.toString(data.length);
		args[argid++] = "-a";
		args[argid++] = "1";
		args[argid++] = device;
		args[argid++] = Integer.toString(cmd);
		for (byte b : data) {
			args[argid++] = Byte.toString(b);
		}
		return args;
	}

	private static String[] makeArgs(String device, boolean openReadOnly,
			int cmd) {
		int argid = 0;
		String[] args;
		if (openReadOnly) {
			args = new String[3];
			args[argid++] = "-r";
		} else {
			args = new String[2];
		}
		args[argid++] = device;
		args[argid++] = Integer.toString(cmd);
		return args;
	}
	
	public static class RtcTime {
        int sec;
        int min;
        int hour;
        int mday;
        int mon;
        int year;
        int wday;
        int yday;
        int isdst;
        
		public RtcTime(byte[] rtcTime) {
			if (rtcTime.length < LinuxConstants.SIZEOF_RTC_TIME) {
				throw new IllegalStateException("Byte sequence too short.");
			}
			ByteBuffer binTime = ByteBuffer.wrap(rtcTime);
			binTime.order(ByteOrder.LITTLE_ENDIAN);
			this.sec = binTime.getInt();
			this.min = binTime.getInt();
			this.hour = binTime.getInt();
			this.mday = binTime.getInt();
			this.mon = binTime.getInt();
			this.year = binTime.getInt();
			this.wday = binTime.getInt();
			this.yday = binTime.getInt();
			this.isdst = binTime.getInt();
		}
        
		public byte[] toByteArray() {
			ByteBuffer binTime = ByteBuffer.allocate(LinuxConstants.SIZEOF_RTC_TIME);
			binTime.order(ByteOrder.LITTLE_ENDIAN);
			binTime.putInt(sec);
			binTime.putInt(min);
			binTime.putInt(hour);
			binTime.putInt(mday);
			binTime.putInt(mon);
			binTime.putInt(year);
			binTime.putInt(wday);
			binTime.putInt(yday);
			binTime.putInt(isdst);
			return binTime.array();
		}

		public void addSeconds(int timeInSeconds) {
			int restTime = timeInSeconds;
			sec += restTime;
			restTime = sec / 60;			
			if (sec >= 60) { 
				sec %= 60;
			}
			min += restTime;
			restTime = min / 60;
			if (min >= 60) {
				min %= 60;
			}
			hour += restTime;
			restTime = hour / 24;
			if (hour >= 24) {
				hour %= 24;
			}
			//TODO: setup day of month, month and year
			if (restTime > 0) {
				mday += 1;//Set for next day
			}
		}
	};

	public static boolean rtcWakeUp(int timeInSeconds) {
		try {
			//Read rd time
			byte[] tm0 = new byte[LinuxConstants.SIZEOF_RTC_TIME]; 
			byte[] tm1 = ioctl("/dev/rtc0", true, LinuxConstants.RTC_RD_TIME, tm0);
			if (tm1 == null) {
				return false;
			}
			RtcTime rtcTime = new RtcTime(tm1);

			boolean runOneOrMoreCommands = false;
			
			rtcTime.addSeconds(timeInSeconds);
			Collection<Integer> validCommands = getValidIECommands("/dev/rtc0");
			if (validCommands.isEmpty()) {
				Log.w(TAG, "No rtc commands supported by rtc device: /dev/rtc0");
			}
			if (validCommands.contains(LinuxConstants.RTC_PIE_ON)) {
				ioctl("/dev/rtc0", false, LinuxConstants.RTC_ALM_SET, rtcTime.toByteArray());
				ioctl("/dev/rtc0", false, LinuxConstants.RTC_PIE_ON, null);
				runOneOrMoreCommands = true;
			}
			if (validCommands.contains(LinuxConstants.RTC_WIE_ON)) {
				ioctl("/dev/rtc0", false, LinuxConstants.RTC_ALM_SET, rtcTime.toByteArray());
				ioctl("/dev/rtc0", false, LinuxConstants.RTC_WIE_ON, null);
				runOneOrMoreCommands = true;
			}
			if (validCommands.contains(LinuxConstants.RTC_UIE_ON)) {
				ioctl("/dev/rtc0", false, LinuxConstants.RTC_ALM_SET, rtcTime.toByteArray());
				ioctl("/dev/rtc0", false, LinuxConstants.RTC_UIE_ON, null);
				runOneOrMoreCommands = true;
			}
			if (validCommands.contains(LinuxConstants.RTC_AIE_ON)) {
				ioctl("/dev/rtc0", false, LinuxConstants.RTC_ALM_SET, rtcTime.toByteArray());
				ioctl("/dev/rtc0", false, LinuxConstants.RTC_AIE_ON, null);
				runOneOrMoreCommands = true;
			}
			
			if (!runOneOrMoreCommands) {
				return false;
			}
			
			ExecResult str = suo("cat", new String[] {"/proc/driver/rtc"});
			if (str == null) {
				return false;
			}
			Log.d(TAG, str.getStdOut());
			String strStdOut = str.getStdOut();
			if (strStdOut == null) {
				return false;
			}
			String[] fields = strStdOut.split("\n");
			for (String field : fields) {
				if (field.contains("alarm_IRQ")) {
					return field.contains("yes") || field.contains("on");
				}
			}
			return false;
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return false;
		}
	}

	private static Collection<Integer> getValidIECommands(String device) {
		Collection<Integer> commands = new HashSet<Integer>();
		if (ioctl(device, false, LinuxConstants.RTC_AIE_OFF, null) != null) {
			commands.add(LinuxConstants.RTC_AIE_OFF);
			commands.add(LinuxConstants.RTC_AIE_ON);
		}
		if (ioctl(device, false, LinuxConstants.RTC_UIE_OFF, null) != null) {
			commands.add(LinuxConstants.RTC_UIE_OFF);
			commands.add(LinuxConstants.RTC_UIE_ON);
		}
		if (ioctl(device, false, LinuxConstants.RTC_WIE_OFF, null) != null) {
			commands.add(LinuxConstants.RTC_WIE_OFF);
			commands.add(LinuxConstants.RTC_WIE_ON);
		}
		if (ioctl(device, false, LinuxConstants.RTC_PIE_OFF, null) != null) {
			commands.add(LinuxConstants.RTC_PIE_OFF);
			commands.add(LinuxConstants.RTC_PIE_ON);
		}
		return commands;
	}

	public static boolean isNetworkConnectedOrConnecting(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		return networkInfo != null && networkInfo.isConnectedOrConnecting();
	}
	
	public static boolean isServiceExists(Context context, String serviceName) {
		if (serviceName == null) {
			throw new IllegalArgumentException();
		}
		PackageManager packageManager = context.getPackageManager();
		List<PackageInfo> pkgs = packageManager.getInstalledPackages(PackageManager.GET_SERVICES);
		for(PackageInfo pkg : pkgs) {
			ServiceInfo[] services = pkg.services;
			if (services == null) {
				continue;
			}
			for (ServiceInfo service : services) {
				if (service.name == null) {
					continue;
				}
				Log.d(TAG, service.name);
				if (service.name.endsWith(serviceName)) {
					return true;
				}
				
			}
		}
		return false;
	}
	
	public static int getSettingsInt(ContentResolver contentResolver, String name, int defaultValue) {
		try {
			return unsafetyGetSettingsInt(contentResolver, name, defaultValue);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return defaultValue;
		}
	}

	private static int unsafetyGetSettingsInt(ContentResolver contentResolver,
			String name, int defaultValue) {
		final Class settingsClass;
		try {
			if (Build.VERSION.SDK_INT < BUILD_VERSION_CODES_JELLY_BEAN_MR1) {
				settingsClass = Thread.currentThread().getContextClassLoader().loadClass("android.provider.Settings$System");
	        } else {
				settingsClass = Thread.currentThread().getContextClassLoader().loadClass("android.provider.Settings$Global");
	        }
			Method m = settingsClass.getDeclaredMethod("getInt", ContentResolver.class, String.class, int.class);
			int value = ((Number) m.invoke(null, contentResolver, name, 0)).intValue();
			return value;
        } catch (ClassNotFoundException e) {
        	return defaultValue;
        } catch (SecurityException e) {
        	return defaultValue;
		} catch (NoSuchMethodException e) {
        	return defaultValue;
		} catch (IllegalArgumentException e) {
        	return defaultValue;
		} catch (IllegalAccessException e) {
        	return defaultValue;
		} catch (InvocationTargetException e) {
        	return defaultValue;
		}
	}

	public static void putSettingsInt(ContentResolver contentResolver, String name, int value) {
		try {
			unsafetyPutSettingsInt(contentResolver, name, value);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	private static void unsafetyPutSettingsInt(ContentResolver contentResolver,
			String name, int value) {
		final Class settingsClass;
		try {
			if (Build.VERSION.SDK_INT < BUILD_VERSION_CODES_JELLY_BEAN_MR1) {
				settingsClass = Thread.currentThread().getContextClassLoader().loadClass("android.provider.Settings$System");
	        } else {
				settingsClass = Thread.currentThread().getContextClassLoader().loadClass("android.provider.Settings$Global");
	        }
			Method m = settingsClass.getDeclaredMethod("putInt", ContentResolver.class, String.class, int.class);
			m.invoke(null, contentResolver, name, value);
			return;
        } catch (ClassNotFoundException e) {
        	return;
        } catch (SecurityException e) {
        	return;
		} catch (NoSuchMethodException e) {
        	return;
		} catch (IllegalArgumentException e) {
        	return;
		} catch (IllegalAccessException e) {
        	return;
		} catch (InvocationTargetException e) {
        	return;
		}
	}

	public static PackageInfo getPackageInfo(Context context) {
		try {
			PackageManager packageManager = context.getPackageManager();
			String packageName = context.getPackageName();
			PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
			return packageInfo;
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return null;
		}
	}
	
	public static String getApplicationVersion(HipdriverApplication application) {
		PackageInfo packageInfo = getPackageInfo(application);
		if (packageInfo == null) {
			return "";
		}
		String versionName = packageInfo.versionName;
		if (versionName == null) {
			return "";
		}
		return versionName;
	}
	
	public static boolean isCameraEnabled(Context context) {
		PackageManager pm = context.getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA); 
	}
	
	public static int makeIdentity(Context context) {
		long l0 = System.currentTimeMillis();
		long l1 = getCachedImei(context);
		return new Long(l0 ^ l1).hashCode(); 
	}

	private static long getCachedImei(Context context) {
		if (cachedImei != null) {
			return cachedImei;
		}
		cachedImei = getImei(context);
		return cachedImei;
	}

	public static String getAndroidInfo() {
		StringBuilder text = new StringBuilder();
		text.append("BOARD: ").append(android.os.Build.BOARD).append('\n');
		text.append("BOOTLOADER: ").append(android.os.Build.BOOTLOADER).append('\n');
		text.append("BRAND: ").append(android.os.Build.BRAND).append('\n');
		text.append("CPU_ABI: ").append(android.os.Build.CPU_ABI).append('\n');
		text.append("CPU_ABI2: ").append(android.os.Build.CPU_ABI2).append('\n');
		text.append("DEVICE: ").append(android.os.Build.DEVICE).append('\n');
		text.append("DISPLAY: ").append(android.os.Build.DISPLAY).append('\n');
		text.append("FINGERPRINT: ").append(android.os.Build.FINGERPRINT).append('\n');
		text.append("HARDWARE: ").append(android.os.Build.HARDWARE).append('\n');
		text.append("HOST: ").append(android.os.Build.HOST).append('\n');
		text.append("ID: ").append(android.os.Build.ID).append('\n');
		return text.toString();
	}

    /**
     * TODO: improve performance.
     * @param context
     * @param applicationPackage
     * @param className
     * @return
     */
    public static boolean isClassAvailable(Context context, String applicationPackage, String className) {
        if (context == null) {
            return false;
        }
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return false;
        }
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(applicationPackage, 0);
            if (applicationInfo == null) {
                return false;
            }
            if (!applicationInfo.enabled) {
                return false;
            }
            String sourceDir = applicationInfo.sourceDir;
            if (sourceDir == null) {
                return false;
            }

            File dexOutputDir = context.getDir("dex", 0);
            DexClassLoader classLoader = new DexClassLoader(sourceDir, dexOutputDir.getAbsolutePath(), null, Thread.currentThread().getContextClassLoader());
            Class<?> cla$$ = classLoader.loadClass(className);
            return cla$$ != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

}
