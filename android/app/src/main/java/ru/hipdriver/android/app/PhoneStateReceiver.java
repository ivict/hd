package ru.hipdriver.android.app;

import java.util.Date;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.AppFacesEnum;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.android.app.R;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class PhoneStateReceiver extends HipdriverBroadcastReceiver {
	// Context context = null;
	private static final String TAG = TextTools.makeTag(PhoneStateReceiver.class);
	
	public static final long TIMEOUT_IN_MILLIS_BEFORE_HANGUP_ON_ACTIVATE_ALARM = 5000;

	// private ITelephony telephonyService;

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (action == null) {
			return;
		}
		
		final HipdriverApplication hipdriverApplication = getA(context);
		////Not receiving intents if paused
		//if (hipdriverApplication.isPaused()) {
		//	return;
		//}
		        
        if (hipdriverApplication.getUserCommandsExecutor() == null) {
        	Log.w(TAG, "null user commands executor???");
        	return;
        }
        
        //Check sign-up
        if (!hipdriverApplication.isSignup()) {
        	return;
        }
        
        //Check sign-up
        if (hipdriverApplication.getAppFace() != AppFacesEnum.CAR_ALARMS) {
        	return;
        }
        
		Bundle bundle = intent.getExtras();
		if (bundle == null) {
			return;
		}
		final String incomingNumber = bundle.getString("incoming_number");
		String state = bundle.getString("state");
		if (state == null) {
			return;
		}
		if (!state.equals("RINGING")) {
			return;
		}
		if (incomingNumber == null) {
			return;
		}
		
		hipdriverApplication.runPhoneTask(new Runnable() {

			@Override
			public void run() {
				final IUserCommandsExecutor userCommandsExecutor = hipdriverApplication.getUserCommandsExecutor();
				String phoneNumber = hipdriverApplication.getString(MainActivity.PHONE_NUMBER_EDITOR, "");
				String extractedPhoneNumber = TextTools.extractLastSixNumbersFromPhoneNumber(phoneNumber);
				final String lastSixNumbersInIncomingNumber = TextTools.extractLastSixNumbersFromPhoneNumber(incomingNumber);
				if (!lastSixNumbersInIncomingNumber.equals(
						extractedPhoneNumber)) {
			        //Check request server if unknown number
			        if (!hipdriverApplication.getBoolean(IMobileAgentProfile.REQUEST_SERVER_IF_UNKNOWN_NUMBER)) {
			        	return;
			        }
			        
			        Log.d(TAG, "try-to-request-server-for-phone-number");
			        
					//Request to server if number is changed
					UITaskFactory.signInAsync(hipdriverApplication, new After() {

						@Override
						public void success(Object returnValue) {
							String phoneNumber = hipdriverApplication.getString(MainActivity.PHONE_NUMBER_EDITOR, "");
							String extractedPhoneNumber = TextTools
									.extractLastSixNumbersFromPhoneNumber(phoneNumber);
							//Check if number changed from web
							if (lastSixNumbersInIncomingNumber.equals(
									extractedPhoneNumber)) {
								safetyExecCommand(hipdriverApplication,
										userCommandsExecutor);
							}
							//Update agent name
							String mobileAgentName = hipdriverApplication.getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
							userCommandsExecutor.safetySetTextOutput(R.id.mobile_agent_name_text, mobileAgentName, false, true, false);
						}
						
					});
					//Not a same number, but emulate same behavior (count of bells).
					if (hipdriverApplication.getAppState() != AppStatesEnum.CAC) {
						try {
							Thread.sleep(TIMEOUT_IN_MILLIS_BEFORE_HANGUP_ON_ACTIVATE_ALARM);
						} catch (InterruptedException e) {
							Log.wtf(TAG, e);
						}
					}
					WatchdogService.hangUp(hipdriverApplication);
					return;
				}
				//Same number
				safetyExecCommand(hipdriverApplication, userCommandsExecutor);
				WatchdogService.hangUp(hipdriverApplication);
			}
			
		});
		
	}

	protected void executeCommand(HipdriverApplication hipdriverApplication,
			IUserCommandsExecutor userCommandsExecutor) {
		//Request current control mode
		hipdriverApplication.switchControlModeIfChanged();
		if (hipdriverApplication.getAppState() != AppStatesEnum.ASO ) {
			hipdriverApplication.setCarState(CarStatesEnum.U);
			hipdriverApplication.setFirstDetectionCarState(CarStatesEnum.U);
			Log.d(TAG, "switch-off-watch[phone-call-incoming]");				
			userCommandsExecutor.switchOffWatch();
		} else {
			//Request new profile settings
			userCommandsExecutor.signin();
			//Check billing key
			if (hipdriverApplication.getDate(HipdriverApplication.EXPIRED_DATE).before(new Date())) {
				return;
			}
			userCommandsExecutor.switchOnWatch();
			sleepBeforeHangup();
		}
	}

	protected void sleepBeforeHangup() {
		try {
			Thread.sleep(TIMEOUT_IN_MILLIS_BEFORE_HANGUP_ON_ACTIVATE_ALARM);
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		}
	}

	protected void safetyExecCommand(
			HipdriverApplication hipdriverApplication,
			IUserCommandsExecutor userCommandsExecutor) {
		try {
			executeCommand(hipdriverApplication, userCommandsExecutor);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

}

