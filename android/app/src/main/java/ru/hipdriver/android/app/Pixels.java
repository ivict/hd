package ru.hipdriver.android.app;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class Pixels {
	
	private Pixels() { }
	
	/**
	 * Big thanks for Dmytro Danylyk
	 * @see http://stackoverflow.com/questions/8399184/convert-dip-to-px-in-android
	 * @param context
	 * @param dipValue
	 * @return
	 */
	public static float dipToPixels(Context context, float dipValue) {
	    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
	    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
	}	

}
