package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.AppFacesEnum;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.ControlModsEnum;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import ru.hipdriver.android.app.R;

public class PowerEventsReceiver extends HipdriverBroadcastReceiver {
	
	public static final String TAG = TextTools.makeTag(PowerEventsReceiver.class); 
	
	@Override
	public void onReceive(Context context, Intent intent) {
		HipdriverApplication hipdriverApplication = getA(context);
		
        //Check sign-up
        if (!hipdriverApplication.isSignup()) {
        	return;
        }

		Log.d(TAG, "action:" + intent != null ? intent.getAction(): "");
        
		////Not receiving intents if paused
		//if (hipdriverApplication.isPaused()) {
		//	return;
		//}
		
		//Empty data case
		String action = intent.getAction();
		if (action == null) {
			return;
		}
		
		//Issue from Google Play
		Intent watchdogServiceIntent = new Intent(hipdriverApplication, WatchdogService.class);
		this.peekService(context, watchdogServiceIntent);
		WatchdogService watchdogService = hipdriverApplication.getWatchdogService();
		//TODO: refactoring to send intent into service
		if (watchdogService != null) {
			sendStateEvent(hipdriverApplication, watchdogService);
		}
		
		//Sending network info only if app-face is car-alarms
		if (hipdriverApplication.getAppFace() != AppFacesEnum.CAR_ALARMS) {
			return;
		}
		
        boolean isPowerDisconnected = action.contains("ACTION_POWER_DISCONNECTED");
        //Handle un-plug power cord
		if (isPowerDisconnected) {
        	hipdriverApplication.handleUnplugPowerCable();
        }

		boolean isPowerConnected = action.contains("ACTION_POWER_CONNECTED");

		//Warning if possible NPE
		if (watchdogService == null) {
			Log.w(TAG, "null-reference-on-watchdog-service");
		}
		
		if (hipdriverApplication.getAppState() != AppStatesEnum.ASO &&
				isPowerConnected && watchdogService != null &&
				hipdriverApplication.getBoolean(IMobileAgentProfile.ALARM_ON_POWER_CABLE_CONNECT)) {
			watchdogService.setAlertStateMakeCallAndStartReasonDetection("Тревога CC!");
		}

		if (hipdriverApplication.getAppState() != AppStatesEnum.ASO &&
				isPowerDisconnected && watchdogService != null &&
				hipdriverApplication.getBoolean(IMobileAgentProfile.ALARM_ON_POWER_CABLE_DISCONNECT)) {
			watchdogService.setAlertStateMakeCallAndStartReasonDetection("Тревога CD!");
		}

		//Update info in async mode
		//TODO: refactoring to send intent into service
		if (hipdriverApplication.getControlMode() == ControlModsEnum.INTERNET
				&& (isPowerConnected || isPowerDisconnected)
				&& watchdogService != null) {
			watchdogService.fireChangeMobileAgentState();
			return;
		}
		
		//Email events only if application in on state
		if (hipdriverApplication.getAppState() == AppStatesEnum.ASO) {
			return;
		}
		
		//Check if user suppress sending e-mail
		if (!hipdriverApplication.getBoolean(IMobileAgentProfile.SEND_EMAIL_ON_POWER_EVENT)) {
			return;
		}
		
		//If low battery send e-mail if application in AppStateEnum.CAC state
		if (isPowerConnected) {
			sendEmailOnExtPowerСonnect(hipdriverApplication);
			return;
		}
		if (isPowerDisconnected) {
			sendEmailOnExtPowerDisconnect(hipdriverApplication);
			return;
		}
		if (action.contains("ACTION_BATTERY_OKAY")) {
			sendEmailOnBatteryFull(hipdriverApplication);
			return;
		}
		//low level redefined @see HipdriverApplication#handleBatteryPct
		
	}

	public static void sendStateEvent(HipdriverApplication hipdriverApplication,
			WatchdogService watchdogService) {
		//Sending network info only if app-face is car-alarms
		if (hipdriverApplication.getAppFace() != AppFacesEnum.CAR_ALARMS) {
			return;
		}
		
		if (watchdogService.isGpsAvailable()) {
			UITaskFactory.sendGpsEvent(hipdriverApplication, null);
		} else {
			UITaskFactory.sendGsmEvent(hipdriverApplication, null);
		}
	}

	public static void sendEmailOnLowBattery(HipdriverApplication hipdriverApplication, float batteryPct) {
		//Sending network info only if app-face is car-alarms
		if (hipdriverApplication.getAppFace() != AppFacesEnum.CAR_ALARMS) {
			return;
		}
		if (!hipdriverApplication.getBoolean(IMobileAgentProfile.SEND_EMAIL_ON_POWER_EVENT)) {
			return;
		}
		final String email = hipdriverApplication.getString(IMobileAgent.ALERT_EMAIL_KEY, "");
		if (email.isEmpty()) {
			return;
		}
		String mobileAgentName = hipdriverApplication.getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
		String subj = hipdriverApplication.getResources().getString(R.string.email_message_subj_ext_power, mobileAgentName);
		String body = hipdriverApplication.getResources().getString(R.string.email_message_body_ext_power_low_battery, String.valueOf(NumericTools.num100(batteryPct)));
		sendEmail(hipdriverApplication, email, subj, body + "\n");
	}
	
	public void sendEmailOnBatteryFull(HipdriverApplication hipdriverApplication) {
		//Sending network info only if app-face is car-alarms
		if (hipdriverApplication.getAppFace() != AppFacesEnum.CAR_ALARMS) {
			return;
		}
		if (!hipdriverApplication.getBoolean(IMobileAgentProfile.SEND_EMAIL_ON_POWER_EVENT)) {
			return;
		}
		final String email = hipdriverApplication.getString(IMobileAgent.ALERT_EMAIL_KEY, "");
		if (email.isEmpty()) {
			return;
		}
		String mobileAgentName = hipdriverApplication.getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");

		String subj = hipdriverApplication.getResources().getString(R.string.email_message_subj_ext_power, mobileAgentName);
		String body = hipdriverApplication.getResources().getString(R.string.email_message_body_ext_power_full);
		sendEmail(hipdriverApplication, email, subj, body + "\n");
		
	}
	
	public void sendEmailOnExtPowerDisconnect(HipdriverApplication hipdriverApplication) {
		//Sending network info only if app-face is car-alarms
		if (hipdriverApplication.getAppFace() != AppFacesEnum.CAR_ALARMS) {
			return;
		}
		if (!hipdriverApplication.getBoolean(IMobileAgentProfile.SEND_EMAIL_ON_POWER_EVENT)) {
			return;
		}
		final String email = hipdriverApplication.getString(IMobileAgent.ALERT_EMAIL_KEY, "");
		if (email.isEmpty()) {
			return;
		}
		String mobileAgentName = hipdriverApplication.getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");

		String subj = hipdriverApplication.getResources().getString(R.string.email_message_subj_ext_power, mobileAgentName);
		String body = hipdriverApplication.getResources().getString(R.string.email_message_body_ext_power_off);
		sendEmail(hipdriverApplication, email, subj, body + "\n");
	}
	
	public void sendEmailOnExtPowerСonnect(HipdriverApplication hipdriverApplication) {
		//Sending network info only if app-face is car-alarms
		if (hipdriverApplication.getAppFace() != AppFacesEnum.CAR_ALARMS) {
			return;
		}
		if (!hipdriverApplication.getBoolean(IMobileAgentProfile.SEND_EMAIL_ON_POWER_EVENT)) {
			return;
		}
		final String email = hipdriverApplication.getString(IMobileAgent.ALERT_EMAIL_KEY, "");
		if (email.isEmpty()) {
			return;
		}
		String mobileAgentName = hipdriverApplication.getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
		
		String subj = hipdriverApplication.getResources().getString(R.string.email_message_subj_ext_power, mobileAgentName);
		String body = hipdriverApplication.getResources().getString(R.string.email_message_body_ext_power_on);
		sendEmail(hipdriverApplication, email, subj, body + "\n");
	}

	private static void sendEmail(HipdriverApplication hipdriverApplication,
			final String email, String subject, String body) {
		UITaskFactory.sendEmail(hipdriverApplication, email, subject, body, null, 0);
	}
	

}
