package ru.hipdriver.android.app;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
/**
 * Very good pattern, 
 * @see http://stackoverflow.com/questions/12560590/android-app-restarts-upon-crash-force-close
 * for details 
 */
public class RuntimeExceptionHandler implements UncaughtExceptionHandler {
    protected final Context context;
    protected final Class<? extends Activity> activityClass;
    
    public static final String EXTRA_UNCAUGHT_EXCEPTION = "uncaughtException";
    public static final String EXTRA_STACK_TRACE = "stacktrace";

    public RuntimeExceptionHandler(Context context, Class<? extends Activity> activityClass) {
        this.context = context;
        this.activityClass = activityClass;
    }

    public void uncaughtException(Thread thread, Throwable exception) {

        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);// You can use LogCat too
        Intent intent = new Intent(context, activityClass);
        String s = stackTrace.toString();
        //you can use this String to know what caused the exception and in which Activity
        intent.putExtra(EXTRA_UNCAUGHT_EXCEPTION,
                exception.getLocalizedMessage());
        intent.putExtra(EXTRA_STACK_TRACE, s);
        updateIntent(intent);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        //for restarting the Activity
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

	protected void updateIntent(Intent intent) {
	}
}
