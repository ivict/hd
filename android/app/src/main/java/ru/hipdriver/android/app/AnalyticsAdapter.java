package ru.hipdriver.android.app;

import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.util.TextTools;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

public class AnalyticsAdapter {
	
	private static final String TAG = TextTools.makeTag(AnalyticsAdapter.class);
	
	private static Tracker tracker;
	private static GoogleAnalytics instance;
	private static HipdriverApplication a;
	
	private AnalyticsAdapter() { }
	
	public static void init(HipdriverApplication hipdriverApplication) {
		try {
			if (tracker != null) {
				return;
			}
			a = hipdriverApplication; 
		    instance = GoogleAnalytics.getInstance(hipdriverApplication);
			tracker = instance.getTracker("UA-47575460-1");
			//Warning not use App-level Opt Out! It's blocked messages.
			//instance.getLogger().setLogLevel(LogLevel.VERBOSE);
			
		    //SET some variables, that will be send to GoogleAnalytics:
			PackageManager packageManager = hipdriverApplication.getPackageManager();
			String packageName = hipdriverApplication.getPackageName();
			PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
		    tracker.set(Fields.APP_NAME, packageName);
		    tracker.set(Fields.APP_VERSION, packageInfo.versionName);
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, null).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}

	}
	
	public static void close() {
		try {
			if (instance == null) {
				return;
			}
		    instance.closeTracker("UA-47575460-1");
		    instance = null;
		    tracker = null;
		} catch (Throwable th) {
			Log.wtf(TAG,  th);
		}
	}
	
	public static void trackSwitchOff(long watchTimeInMillis) {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, null).build());
			tracker.send(MapBuilder.createEvent("alarm-service-ux", "switch-off-alarm", "Выключено", watchTimeInMillis).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	public static void trackSwitchOffByTimer() {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, null).build());
			tracker.send(MapBuilder.createEvent("alarm-service-ux", "switch-off-by-timer", "Выключено по таймеру", TimeUnit.HOURS.toMillis(2)).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	public static void trackSwitchOffByAccelerometerHovered() {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, null).build());
			tracker.send(MapBuilder.createEvent("alarm-service-ux", "switch-off-by-accelerometer", "Завис акселерометер", TimeUnit.MINUTES.toMillis(2)).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	public static void trackSwitchOffByUnknownReason(int stringResourceId) {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, null).build());
			tracker.send(MapBuilder.createEvent("alarm-service-ux", "switch-off-by-unknown-reason", String.valueOf(stringResourceId), 0L).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	public static void trackInWatchMode() {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, "Сторожу").build());
			tracker.send(MapBuilder.createEvent("alarm-service-ux", "switch-on-alarm", "Включено", null).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	public static void trackInShowHelp() {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, "Краткая инструкция").build());
			tracker.send(MapBuilder.createEvent("alarm-service-ux", "show-short-help", "Помощь", null).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	public static void trackInRemoteControlMode() {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, "Пульт").build());
			tracker.send(MapBuilder.createEvent("alarm-service-ux", "remote-control", "Пульт", null).build());
			if (a != null) {
				tracker.send(MapBuilder.createEvent("alarm-service-ux", "remote-control-mode", a.getControlMode().toString(), null).build());
			}
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	public static void trackInBeaconMode() {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, "Маяк").build());
			tracker.send(MapBuilder.createEvent("alarm-service-ux", "switch-on-alarm", "Включено", null).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	public static void trackInAlertMode(String alertReason) {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, "Тревога").set("alert-reason", alertReason).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	public static void trackException(String message) {
		try {
			if (tracker == null) {
				return;
			}
			tracker.send(MapBuilder.createException(message, false).build());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

}
