package ru.hipdriver.android.app;

import static ru.hipdriver.android.app.NumericTools.num100;

import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Locale;

import android.util.FloatMath;

public class NumericTools {
	
	public static float g = 980.665f;//Gravitation acceleration cm / s^2 SGU
	
	private NumericTools() { }
	
	public static float norma(float[] xyz) {
		return (float) Math.sqrt(sq(xyz[0]) + sq(xyz[1]) + sq(xyz[2]));
	}

	public static float normasq(float[] xyz) {
		return (float) sq(xyz[0]) + sq(xyz[1]) + sq(xyz[2]);
	}

	public static float norma(float x, float y, float z) {
		return (float) Math.sqrt(sq(x) + sq(y) + sq(z));
	}

	public static float[] normNum100(float[] sample) {
		float[] nmz = new float[sample.length];
		for (int i = 0; i < nmz.length; i++) {
			nmz[i] = num100(sample[i]);
		}
		return nmz;
	}

	public static float num(float value) {
		return value;// value < 0 ? FloatMath.ceil(value / 3f) * 3f : FloatMath
		// .floor(value / 3f) * 3f;
	}

	public static float num100(float value) {
		return value < 0 ? FloatMath.ceil(value * 100f) / 100f : FloatMath
				.floor(value * 100f) / 100f;
	}
	
	private static final DecimalFormat dfmt = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.ENGLISH));
	/**
	 * Add n leading spaces before number representation.
	 * Number of digits after decimal point fixed and equals by 2.
	 * @param value
	 * @param n count of leading spaces.
	 * @return
	 */
	public static String ffn(float value, int n) {
		String formatedNumber = dfmt.format(num100(value));
		int len = formatedNumber.length();
		if (len >= n) {
			return formatedNumber;
		}
		char[] leadingSpaces = new char[n - len];
		Arrays.fill(leadingSpaces, ' ');
		StringBuilder text = new StringBuilder(n);
		text.append(leadingSpaces);
		text.append(formatedNumber);
		return text.toString();
	}

	public static float num10(float value) {
		return value < 0 ? FloatMath.ceil(value * 10f) / 10f : FloatMath
				.floor(value * 10f) / 10f;
	}

	public static float sq(float value) {
		return value * value;
	}

	public static float[] zeroF3() {
		float[] result = {0f, 0f, 0f};
		return result;
	}
	
	public static float scp(float[] vector1, float[] vector2) {
		if (vector1.length != vector2.length) {
			throw new IllegalStateException("Available only for equals by length vectors.");
		}
		float scalarProduction = 0;
		for (int i = 0; i < vector1.length; i++) {
			scalarProduction += vector1[i] * vector2[i];
		}
		return scalarProduction;
	}

	public static short bytesToShort(byte[] args) {
		if (args == null) {
			throw new NumberFormatException();
		}
		if (args.length < 2) {
			throw new NumberFormatException();
		}
		ByteBuffer bb = ByteBuffer.wrap(args);
		return bb.getShort();
	}

	public static byte[] shortToBytes(short s) {
		byte[] bytes = new byte[2];
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		bb.putShort(s);
		return bytes;
	}

	public static int bytesToInt(byte[] args) {
		if (args == null) {
			throw new NumberFormatException();
		}
		if (args.length < 4) {
			throw new NumberFormatException();
		}
		ByteBuffer bb = ByteBuffer.wrap(args);
		return bb.getInt();
	}
	
	public static byte[] intToBytes(int i) {
		byte[] bytes = new byte[4];
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		bb.putInt(i);
		return bytes;
	}

}
