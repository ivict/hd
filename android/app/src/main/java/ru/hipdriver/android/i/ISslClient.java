package ru.hipdriver.android.i;

import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.app.sslucs.ShortMobileAgentState;
import ru.hipdriver.i.support.UserCommandTypesEnum;

public interface ISslClient {
	
	boolean ping(long mobileAgentIdTo);
	
	/**
	 * TODO: throw Timeout exception
	 * Synchronous request to remote SslServer.
	 * @param mobileAgentIdTo mobile agent identity.
	 * @param request to remote server.
	 * @param responseClass answer parsed with this type.
	 * @param timeout timeout for request, after timeout function return null.
	 * @param unit units for timeout.
	 * @return response from remote server or null after timeout.
	 */
	Object get(long mobileAgentIdTo, Object request, Class<?> responseClass, long timeout, TimeUnit unit);
	
	ShortMobileAgentState execCommand(long mobileAgentIdTo, UserCommandTypesEnum commandType, byte[] args);
	
	/**
	 * Asynchronous request to remote SslServer.
	 * @param mobileAgentIdTo mobile agent identity.
	 * @param request to remote server.
	 * @param responseClasses answer parsed with this type.
	 * @param requestCallback
	 * @param timeout timeout for request, after timeout function return null.
	 * @param unit units for timeout.
	 * @param longTtl special parameter for don't removing callback in answers, i.e. callback call after sequential responses from SslServer.
	 * @param delayedPacketFactor delayed packet factor technique, say to SslClient, don't remove callback after timeout, but remove after timeout * delayedPacketFactor. In same time, callback function after timeout call with parameters isTimeout == true.
	 * @param key for long ttl requests (like type of user commands for example)
	 */
	void getAsync(long mobileAgentIdTo, Object request, Class<?>[] responseClasses, RequestCallback requestCallback, long timeout, TimeUnit unit, boolean longTtl, int delayedPacketFactor, Object key);

	void execCommandAsync(long mobileAgentIdTo, UserCommandTypesEnum commandType, byte[] args, RequestCallback requestCallback, boolean longTtl, int delayedPacketFactor);
	
	void restoreLongttlRequestListener(long componentSessionsId, RequestCallback requestCallback);
	
	void release();

	void reconnect();
	
}
