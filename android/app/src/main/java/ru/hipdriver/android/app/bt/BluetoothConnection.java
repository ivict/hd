package ru.hipdriver.android.app.bt;

import android.bluetooth.BluetoothDevice;

public interface BluetoothConnection {

	boolean open(BluetoothDataHandler dataHandler);
	
	void close();
	
	BluetoothPipe getPipe(BluetoothDevice device);
	
}
