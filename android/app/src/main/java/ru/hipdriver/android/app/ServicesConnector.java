package ru.hipdriver.android.app;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.i.ISid;
import ru.hipdriver.i.ISignInMaResponse;
import ru.hipdriver.i.IUserLimit;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.i.support.EventClassesEnum;
import ru.hipdriver.i.support.MapProvidersEnum;
import ru.hipdriver.i.support.ServicesEnum;
import ru.hipdriver.j.Event;
import ru.hipdriver.j.EventWithAtach;
import ru.hipdriver.j.Location;
import ru.hipdriver.j.MobileAgentWithState;
import ru.hipdriver.j.SKey;
import ru.hipdriver.j.Sid;
import ru.hipdriver.j.SignInMaRequest;
import ru.hipdriver.j.SignInMaResponse;
import ru.hipdriver.j.SignUpMa;
import ru.hipdriver.util.IoTools;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class ServicesConnector {

	private static final byte[] ZERO_BYTES_COUNT = new byte[0];
	public static final String MOBILE_AGENT_ID_KEY = "mobileAgentId";
	public static final String USER_ID_KEY = "userId";
	private static final String TAG = TextTools.makeTag(ServicesConnector.class);
	//private static final String SERVER_URL = "http://192.168.1.103";
	//private static final String SERVER_URL = "http://192.168.2.103";
	//private static final String SERVER_URL = "http://192.168.1.101";
	//private static final String SERVER_URL = "http://10.0.0.21";
	private static final String SERVER_HOST_NAME = "kernel.hipdriver.me";
	private static final String WEB_HOST_NAME = "hipdriver.me";//"qpdb.me/hipdriver";
	
	private static final String YANDEX_GSM_REQUEST_URL_TEMPLATE = "http://mobile.maps.yandex.net/cellid_location/?&countrycode=%d&operatorid=%d&lac=%d&cellid=%d";
	private static final String GOOGLE_GSM_REQUEST_URL = "http://www.google.com/glm/mmap";
	
	public static final String FIRST_SIGN_UP_KEY = "first-sign-up";
	
	/**
	 * Session identity.
	 */
	private ISid sid;
	
	//private int userId;
	//private long mobileAgentId;
	//private byte[] publicServerRsaKey;
	
	private HipdriverApplication context;
	private static ObjectMapper cachedOm;
	private static HttpClient cachedHc;
	private static Object monitorOm = new Object();
	private static Object monitorHc = new Object();
	private static Object monitorLc2 = new Object();
	private static Object monitorLc3 = new Object();
	
	public ServicesConnector(HipdriverApplication context) {
		this.context = context;
		if (context == null) {//No initialization
			return;
		}
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		//Load session identity
		final long a = preferences.getLong("a", 0);
		sid = new ISid() {

			@Override
			public long getA() {
				return a;
			}

			@Override
			public void setA(long a) {
				// TODO Auto-generated method stub
			}
			
		};
		
		//Load user identity
		//userId = preferences.getInt(USER_ID_KEY, 0);
		//mobileAgentId = preferences.getLong(MOBILE_AGENT_ID_KEY, 0);
		//String key = preferences.getString("publicServerRsaKey", "");
		//if ( !key.isEmpty() ) {
		//	publicServerRsaKey = Base64.decode(key, Base64.DEFAULT);
		//}
		
	}
	
	public int signIn() throws Throwable {
		int userId = getA().getInt(USER_ID_KEY, 0);
		long mobileAgentId = getA().getLong(MOBILE_AGENT_ID_KEY, 0L);
		HttpClient httpClient = getHttpClient();
		SignInMaRequest sima = new SignInMaRequest();
		sima.setUserId(userId);
		sima.setMobileAgentId(mobileAgentId);
		Map<String, Object> properties = getProperties();
		sima.setProperties(properties);
		return sign(httpClient, ServicesEnum.KSSI.PATH, sima, false);
	}

	private Map<String, Object> getProperties() {
		Map<String, Object> properties = new HashMap<String, Object>();
		PackageInfo packageInfo;
		if ((packageInfo = Hardwares.getPackageInfo(context)) != null) {
			properties.put(IMobileAgent.MOBILE_AGENT_VERSION_CODE_KEY, packageInfo.versionCode);
			properties.put(IMobileAgent.MOBILE_AGENT_VERSION_NAME_KEY, packageInfo.versionName);
		}
		//Collect only changed properties 
		if (getA().getMemoryVersion(IMobileAgent.ALERT_PHONE_NUMBER_KEY) > getA().getMemoryVersion()) {
		    Object alertPhoneNumber = getA().getString(MainActivity.PHONE_NUMBER_EDITOR, "");
		    properties.put(IMobileAgent.ALERT_PHONE_NUMBER_KEY, alertPhoneNumber);	    
		}

		if (getA().getMemoryVersion(IMobileAgent.MOBILE_AGENT_NAME_KEY) > getA().getMemoryVersion()) {
			Object mobileAgentName = getA().getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, ""); 
			properties.put(IMobileAgent.MOBILE_AGENT_NAME_KEY, mobileAgentName);
		}
		
		if (getA().getMemoryVersion(IMobileAgent.ALERT_EMAIL_KEY) > getA().getMemoryVersion()) {
			Object alertEMail = getA().getString(IMobileAgent.ALERT_EMAIL_KEY, ""); 
			properties.put(IMobileAgent.ALERT_EMAIL_KEY, alertEMail);
		}
		
		if (getA().getMemoryVersion(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE) > getA().getMemoryVersion()) {
			Object enableEnergySavingMode = getA().getBoolean(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE); 
			properties.put(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE, enableEnergySavingMode);
		}
		
		if (getA().getMemoryVersion(AlertStateDetector.MAX_ACC_KEY) > getA().getMemoryVersion()) {
			Object asdMaxAcceleration = getA().getFloat(AlertStateDetector.MAX_ACC_KEY); 
			properties.put(IMobileAgentProfile.ASD_MAX_ACCELERATION, asdMaxAcceleration);
		}
		if (getA().getMemoryVersion(AlertStateDetector.CRITICAL_ANGLE_KEY) > getA().getMemoryVersion()) {
			Object asdMaxAngle = getA().getFloat(AlertStateDetector.CRITICAL_ANGLE_KEY); 
			properties.put(IMobileAgentProfile.ASD_MAX_ANGLE, asdMaxAngle);
		}
		if (getA().getMemoryVersion(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS) > getA().getMemoryVersion()) {
			Object wssdInSeconds = getA().getInt(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS); 
			properties.put(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS, wssdInSeconds);
		}
		
		return properties;
	}

	public int signUp(String userName, byte[] passwordHash,
			String mobileAgentName, long imei, IMobileAgentState mobileAgentState) throws Throwable {
		// Reset current sid
		sid = null;
		boolean firstSignup = getA().getBoolean(FIRST_SIGN_UP_KEY);
		HttpClient httpClient = getHttpClient();
		SignUpMa sima = new SignUpMa();
		sima.setName(userName);
		sima.setPasswordHash(passwordHash);
		sima.setMobileAgentName(mobileAgentName);
		sima.setImei(imei);
		sima.setFirstSignup(firstSignup);
		safetyUpdateVersionInfo(sima);
		sima.setAppStateId(mobileAgentState.getAppStateId());
		sima.setBatteryPct(mobileAgentState.getBatteryPct());
		sima.setBatteryStatus(mobileAgentState.getBatteryStatus());
		sima.setCarStateId(mobileAgentState.getCarStateId());
		sima.setGsmBitErrorRate(mobileAgentState.getGsmBitErrorRate());
		sima.setGsmSignalStrength(mobileAgentState.getGsmSignalStrength());
		ILocation lastLocation = mobileAgentState.getLastLocation();
		if (isValidLocation(lastLocation)) {
			if (lastLocation.getLat() == 0 && lastLocation.getLon() == 0) {
				int mcc = lastLocation.getMcc();
				int mnc = lastLocation.getMnc();
				int lac = lastLocation.getLac();
				int cid = lastLocation.getCid();
				safetyUpdateGeoCoordinates(context, httpClient, lastLocation, mcc, mnc, lac, cid);
			}
			
			sima.setLastLocation(new Location());
			sima.setLastLocation(lastLocation);
		}
		Map<String, Object> properties = getProperties();
		sima.setProperties(properties);
		return sign(httpClient, ServicesEnum.KSSU.PATH, sima, true);
	}

	//TODO: refactoring (split by two methods with general code-base).
	protected int sign(HttpClient httpClient, String service,
			Object sima, boolean up) throws Throwable {
		ObjectMapper om = getObjectMapper();
		String postMessage = om.writeValueAsString(sima);
		HttpPost request = HttpRequestFactory.newJsonPost(SERVER_HOST_NAME, service, 9, 15, sid, postMessage);
		HttpResponse response = httpClient.execute(request);
		HttpEntity entity = response.getEntity();
		if (entity == null) {
			return NetworkErrors.NULL_RESPONSE.ordinal();
		}
		String responseMessage = getString(entity.getContent());
		if ( isHtml(entity) ) {
			String errorMessage = parseFirstH1(responseMessage);
			throw new RuntimeException(errorMessage);
		}
		final ISignInMaResponse signInMaResponse;
		if (up) {
			SKey sKey = om.readValue(responseMessage, SKey.class);
			if (sKey == null) {
				return NetworkErrors.NULL_RESPONSE.ordinal();
			}
			persistUserIdentityAndUpdateMobileAgentCount(sKey.getUserId(), sKey.getMobileAgentId(), sKey.getMobileAgentCount(), sKey.getValue());
			persistOldSettings(sKey.currentSid);
			signInMaResponse = sKey.getCurrentSignInMaResponse();
		} else {
			signInMaResponse = om.readValue(responseMessage, SignInMaResponse.class);
			if (signInMaResponse == null) {
				return NetworkErrors.NULL_RESPONSE.ordinal();
			}
			
		}
		persistSettings(signInMaResponse);
		//switchControlModeIfChanged(signInMaResponse);
		if (!up) {//Prevent override user settings on low speed network.
			getA().increaseMemoryVersion();
		}
		return NetworkErrors.NO_ERRORS.ordinal();
	}

	private ObjectMapper getObjectMapper() {
		if (cachedOm != null) {
			return cachedOm;
		}
		synchronized (monitorOm) {
			if (cachedOm != null) {
				return cachedOm;
			}
			cachedOm = new ObjectMapper();
		}
		return cachedOm;
	}
	
	private HttpClient getHttpClient() {
		if (cachedHc != null) {
			return cachedHc;
		}
		synchronized (monitorHc) {
			if (cachedHc != null) {
				return cachedHc;
			}
			cachedHc = new DefaultHttpClient();
		}
		return cachedHc;
	}

	private boolean isValidLocation(ILocation lastLocation) {
		if (lastLocation == null) {
			return false;
		}
		if (lastLocation.getLac() == ILocation.UNKNOWN_LOCATION_AREA_CODE &&
				lastLocation.getCid() == ILocation.UNKNOWN_CELL_ID &&
				lastLocation.getLat() == ILocation.UNKNOWN_LATITUDE &&
				lastLocation.getLon() == ILocation.UNKNOWN_LONGITUDE) {
			return false;
		}
		return true;
	}

	private void safetyUpdateVersionInfo(SignUpMa sima) {
		try {
			PackageManager packageManager = context.getPackageManager();
			String packageName = context.getPackageName();
			PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
			sima.setVersionCode(packageInfo.versionCode);
			sima.setVersionName(packageInfo.versionName);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	private void persistOldSettings(Sid currentSid) {
		if (currentSid == null) {
			return;
		}
		persistSessionIdentity(currentSid.getA());
	}

	private void persistSessionIdentity(final long a) {
		this.sid = new ISid() {

			@Override
			public long getA() {
				return a;
			}

			@Override
			public void setA(long a) {
				// TODO Auto-generated method stub
			}
			
		};
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = preferences.edit();
		editor.putLong("a", a);
	    editor.commit();
	}

	private void persistSettings(ISignInMaResponse signInMaResponse) {
		persistSessionIdentity(signInMaResponse.getA());
		
		int delayPushAlertEventInSeconds = signInMaResponse.getDelayPushAlertEventInSeconds();
		int delayWhereAmIRequestInSeconds = signInMaResponse.getDelayWhereAmIRequestInSeconds();
		boolean enableDebugMode = signInMaResponse.isEnableDebugMode();
		boolean enableEnergySavingMode = signInMaResponse.isEnableEnergySavingMode();
		int wssdInSeconds = signInMaResponse.getWatchdogServiceStartDelayInSeconds();
		
		float asdMaxJerk = signInMaResponse.getAsdMaxJerk();
		float asdMaxAcceleration = signInMaResponse.getAsdMaxAcceleration();
		float asdMaxAngle = signInMaResponse.getAsdMaxAngle();
		
		boolean timeLimited = signInMaResponse.isTimeLimited();
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = preferences.edit();
	    //Update Alert Geo Track settings
		editor.putInt(IMobileAgentProfile.DELAY_PUSH_ALERT_EVENT_IN_SECONDS, delayPushAlertEventInSeconds);
		editor.putInt(IMobileAgentProfile.DELAY_WHERE_AM_I_REQUEST_IN_SECONDS, delayWhereAmIRequestInSeconds);
		//Update debug info sender property 
		editor.putBoolean(IMobileAgentProfile.ENABLE_DEBUG_MODE, enableDebugMode);
		//Update enable energy saving mode
//TODO:		editor.putBoolean(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE, enableEnergySavingMode);
	    //Update ASD settings
		//100.0 minimal value for JERK because no parameter for UI
		editor.putFloat(AlertStateDetector.MAX_JERK_KEY, asdMaxJerk < 100.0f ? 100.0f : asdMaxJerk);
		editor.putFloat(AlertStateDetector.MAX_ACC_KEY, asdMaxAcceleration);
		editor.putFloat(AlertStateDetector.CRITICAL_ANGLE_KEY, asdMaxAngle);
//TODO:		editor.putInt(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS, wssdInSeconds);
		editor.putBoolean(IUserLimit.TIME_LIMITED, timeLimited);
	    editor.commit();
	    
	    //Change application properties
	    Map<String, Object> properties = signInMaResponse.getProperties();
	    if (properties == null) {
	    	return;
	    }
	    Object alertPhoneNumber = properties.get(IMobileAgent.ALERT_PHONE_NUMBER_KEY);
	    if (alertPhoneNumber != null) {
			getA().setString(MainActivity.PHONE_NUMBER_EDITOR, String.valueOf(alertPhoneNumber));
	    }
	    Object mobileAgentName = properties.get(IMobileAgent.MOBILE_AGENT_NAME_KEY);
	    if (mobileAgentName != null) {
			getA().setString(MainActivity.MOBILE_AGENT_NAME_EDITOR, String.valueOf(mobileAgentName));
	    }
	    Object alertEMail = properties.get(IMobileAgent.ALERT_EMAIL_KEY);
	    if (alertEMail != null) {
			getA().setString(IMobileAgent.ALERT_EMAIL_KEY, String.valueOf(alertEMail));
	    }
	    Object incommingMessage = properties.get(IMobileAgent.INCOMING_MESSAGE_KEY);
	    if (incommingMessage != null) {
			getA().setString(IMobileAgent.INCOMING_MESSAGE_KEY, String.valueOf(incommingMessage));
	    }
	    Object userKeyExpiredInMinutes = properties.get(IMobileAgent.USER_KEY_EXPIRED_IN_MINUTES_KEY);
	    if (userKeyExpiredInMinutes != null && userKeyExpiredInMinutes instanceof Number) {
	    	long userKeyExpiredInMillis = ((Number) userKeyExpiredInMinutes).longValue() * 60 * 1000;
	    	Date expiredDate = new Date(System.currentTimeMillis() + userKeyExpiredInMillis);
			getA().setDate(HipdriverApplication.EXPIRED_DATE, expiredDate);
	    }
	}

	private HipdriverApplication getA() {
		return context;
	}

	private void persistUserIdentityAndUpdateMobileAgentCount(int userId, long mobileAgentId,
			int mobileAgentCount, byte[] publicServerRsaKey) {
		//this.userId = userId;
		//this.mobileAgentId = mobileAgentId;
		//this.publicServerRsaKey = publicServerRsaKey;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = preferences.edit();
		if ( publicServerRsaKey != null && publicServerRsaKey.length > 0 ) {
			editor.putString("publicServerRsaKey", Base64.encodeToString(publicServerRsaKey, Base64.DEFAULT));
		}
		editor.putInt(USER_ID_KEY, userId);
		editor.putLong(MOBILE_AGENT_ID_KEY, mobileAgentId);
		//Update mobile agent count
		editor.putInt(HipdriverApplication.MOBILE_AGENT_COUNT, mobileAgentCount);
	    editor.commit();
	}

	private static String getString(InputStream inputStream) {
		StringBuilder text = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = reader.readLine();
			while ( line != null ) {
				text.append(line);
				line = reader.readLine();
				if ( line != null ) {
					text.append('\n');
				}
			}
			return text.toString();
		} catch (IOException e) {
			Log.wtf(TAG, e);
		}
		return null;
	}
	
	private static final Pattern H1 = Pattern.compile("<h1>(.+)</h1>", Pattern.CASE_INSENSITIVE);
	private String parseFirstH1(String errorReportHtml) {
		Matcher matcher = H1.matcher(errorReportHtml);
		if ( matcher.find() ) {
			return matcher.group(1);
		}
		return "";
	}

	private static final Pattern TEXT_HTML = Pattern.compile("^text/html.*");
	private boolean isHtml(HttpEntity entity) {
		if (entity == null) {
			return false;
		}
		Header contentType = entity.getContentType();
		if (contentType == null) {
			return false;
		}
		String value = contentType.getValue();
		return TEXT_HTML.matcher(value).matches();
	}
	
	public void sendMessage(EventClassesEnum eventClass, Date time) throws Throwable {
		if (sid == null ) {
			throw new RuntimeException("Отсутствует безопасное соединение");
		}
		HttpClient httpclient = getHttpClient();
		Event event = new Event();
		//TODO: Update event classes id before sending or use sign-in process
		event.setEventClassId(DBLinks.link(eventClass));
		event.setTime(time);
		ObjectMapper om = getObjectMapper();
		String postMessage = om.writeValueAsString(event);
		
		HttpPost request = HttpRequestFactory.newJsonPost(SERVER_HOST_NAME, ServicesEnum.KSEP.PATH, 3, 30, sid, postMessage);
		HttpResponse response = httpclient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseMessage = getString(entity.getContent());
		if ( isHtml(entity) ) {
			String errorMessage = parseFirstH1(responseMessage);
			throw new RuntimeException(errorMessage);
		}
	}
	
	public void sendMessageA(EventClassesEnum eventClass, String description, byte[] content, Date time, int connectionTimeoutInSecs, int socketTimeoutInSecs) throws Throwable {
		if (sid == null ) {
			throw new RuntimeException("Отсутствует безопасное соединение");
		}
		HttpClient httpclient = getHttpClient();
		EventWithAtach event = new EventWithAtach();
		//TODO: Update event classes id before sending or use sign-in process
		event.setEventClassId(DBLinks.link(eventClass));
		event.setDescription(description);
		event.setContent(content);
		//TODO: add check sum
		event.setTime(time);
		ObjectMapper om = getObjectMapper();
		String postMessage = om.writeValueAsString(event);
		HttpPost request = HttpRequestFactory.newJsonPost(SERVER_HOST_NAME, ServicesEnum.KSEPA.PATH, connectionTimeoutInSecs, socketTimeoutInSecs, sid, postMessage);
		HttpResponse response = httpclient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseMessage = getString(entity.getContent());
		if ( isHtml(entity) ) {
			String errorMessage = parseFirstH1(responseMessage);
			throw new RuntimeException(errorMessage);
		}
	}

	public void sendGpsMessage(EventClassesEnum eventClass, int lat, int lon, int alt, int acc, Date time) throws Throwable {
		if (sid == null ) {
			throw new RuntimeException("Отсутствует безопасное соединение");
		}
		HttpClient httpclient = getHttpClient();
		Event event = new Event();
		//TODO: Update event classes id before sending or use sign-in process
		event.setEventClassId(DBLinks.link(eventClass));
		event.setTime(time);
		event.setLat(lat);
		event.setLon(lon);
		event.setAlt(alt);
		event.setAcc(acc);
		Locations.packGpsLocation(event);
		event.setTime(null);//Move to packed buffer
		ObjectMapper om = getObjectMapper();
		String postMessage = om.writeValueAsString(event);
		
		HttpPost request = HttpRequestFactory.newJsonPost(SERVER_HOST_NAME, ServicesEnum.KSEP.PATH, 3, 30, sid, postMessage);
		HttpResponse response = httpclient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseMessage = getString(entity.getContent());
		if ( isHtml(entity) ) {
			String errorMessage = parseFirstH1(responseMessage);
			throw new RuntimeException(errorMessage);
		}
	}

	public void sendGpsMessageA(EventClassesEnum eventClass, String description, byte[] content, int lat, int lon, int alt, int acc, Date time) throws Throwable {
		if (sid == null ) {
			throw new RuntimeException("Отсутствует безопасное соединение");
		}
		HttpClient httpclient = getHttpClient();
		EventWithAtach event = new EventWithAtach();
		//TODO: Update event classes id before sending or use sign-in process
		event.setEventClassId(DBLinks.link(eventClass));
		event.setTime(time);
		event.setLat(lat);
		event.setLon(lon);
		event.setAlt(alt);
		event.setAcc(acc);
		event.setDescription(description);
		event.setContent(content);
		Locations.packGpsLocation(event);
		event.setTime(null);//Move to packed buffer
		ObjectMapper om = getObjectMapper();
		String postMessage = om.writeValueAsString(event);
		HttpPost request = HttpRequestFactory.newJsonPost(SERVER_HOST_NAME, ServicesEnum.KSEPA.PATH, 3, 30, sid, postMessage);
		HttpResponse response = httpclient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseMessage = getString(entity.getContent());
		if ( isHtml(entity) ) {
			String errorMessage = parseFirstH1(responseMessage);
			throw new RuntimeException(errorMessage);
		}
	}
	
	private static class MccMncLacCidRequestCacheEntry {
		int mcc;
		int mnc;
		int lac;
		int cid;
		
		int lat;
		int lon;
		int acc;
		
		public boolean equalsByKeys(int mcc, int mnc, int lac, int cid) {
			if (this.mcc != mcc ) {
				return false;
			}
			if (this.mnc != mnc ) {
				return false;
			}
			if (this.lac != lac ) {
				return false;
			}
			if (this.cid != cid ) {
				return false;
			}
			return true;
		}
		
		public void update(int mcc, int mnc, int lac, int cid, int lat, int lon, int acc) {
			this.mcc = mcc;
			this.mnc = mnc;
			this.lac = lac;
			this.cid = cid;
			this.lat = lat;
			this.lon = lon;
			this.acc = acc;
		}
	}
	
	private static AtomicInteger cacheEntryIndex = new AtomicInteger(1);
	private static MccMncLacCidRequestCacheEntry requestsCache1 = new MccMncLacCidRequestCacheEntry();
	private static MccMncLacCidRequestCacheEntry requestsCache2 = new MccMncLacCidRequestCacheEntry();
	private static MccMncLacCidRequestCacheEntry requestsCache3 = new MccMncLacCidRequestCacheEntry();
	private static MccMncLacCidRequestCacheEntry requestsCache4 = new MccMncLacCidRequestCacheEntry();
	
	private static void updateGpsCoordinatesByGsm(HipdriverApplication hipdriverApplication, HttpClient httpClient, ILocation location, int mcc, int mnc, int lac, int cid) throws ClientProtocolException, IOException, SAXException, ParserConfigurationException, InterruptedException {
        boolean isErrorInCache1 = requestsCache1.lat == 0 && requestsCache1.lon == 0;
		if (requestsCache1.equalsByKeys(mcc, mnc, lac, cid) && !isErrorInCache1) {
			//Cached request
			location.setLat(requestsCache1.lat);
			location.setLon(requestsCache1.lon);
			location.setAcc(requestsCache1.acc);
			return;
		}
        boolean isErrorInCache2 = requestsCache2.lat == 0 && requestsCache2.lon == 0;
		if (requestsCache2.equalsByKeys(mcc, mnc, lac, cid) && !isErrorInCache2) {
			//Cached request
			location.setLat(requestsCache2.lat);
			location.setLon(requestsCache2.lon);
			location.setAcc(requestsCache2.acc);
			return;
		}
        boolean isErrorInCache3 = requestsCache3.lat == 0 && requestsCache3.lon == 0;
		if (requestsCache3.equalsByKeys(mcc, mnc, lac, cid) && !isErrorInCache3) {
			//Cached request
			location.setLat(requestsCache3.lat);
			location.setLon(requestsCache3.lon);
			location.setAcc(requestsCache3.acc);
			return;
		}
        boolean isErrorInCache4 = requestsCache4.lat == 0 && requestsCache4.lon == 0;
		if (requestsCache4.equalsByKeys(mcc, mnc, lac, cid) && !isErrorInCache4) {
			//Cached request
			location.setLat(requestsCache4.lat);
			location.setLon(requestsCache4.lon);
			location.setAcc(requestsCache4.acc);
			return;
		}
		MccMncLacCidRequestCacheEntry requestsCache = requestsCache1;
		int index = cacheEntryIndex.getAndIncrement();
		if (index > 4) {
			index = 1;
			cacheEntryIndex.set(1);
		}
		switch(index) {
		case 2:
			requestsCache = requestsCache2;
		case 3:
			requestsCache = requestsCache3;
		case 4:
			requestsCache = requestsCache4;
		}
		
		//Request GooglePlay services
		updateCacheFromGoogleRequest2(hipdriverApplication, requestsCache, httpClient, location, mcc, mnc, lac, cid);		
		boolean isErrorInCache = requestsCache.lat == 0 && requestsCache.lon == 0;
		if (!isErrorInCache) {
			Log.d(TAG, "google-play-location-service-request-success");
			return;
		}
		//Request Network-Provider
		updateCacheFromGoogleRequest3(hipdriverApplication, requestsCache, httpClient, location, mcc, mnc, lac, cid);		
		if (!isErrorInCache) {
			Log.d(TAG, "network-provider-location-request-success");
			return;
		}
		
		//Choose geography
		MapProvidersEnum mapProvider = DBLinks.unlinkMapProvider((short) hipdriverApplication.getInt(IMobileAgentProfile.MAP_PROVIDER_ID));
		switch (mapProvider) {
	    case GOOGLE:
	    	updateCacheFromGoogleRequest(requestsCache, httpClient, location, mcc, mnc, lac, cid);
	    	isErrorInCache = requestsCache.lat == 0 && requestsCache.lon == 0;
	    	if (isErrorInCache) {
	    		//Try to update from Yandex
	    		updateCacheFromYandexRequest(requestsCache, httpClient, location, mcc, mnc, lac, cid);	    		
	    	}
	    	break;
	    case YANDEX:
			updateCacheFromYandexRequest(requestsCache, httpClient, location, mcc, mnc, lac, cid);
	    	isErrorInCache = requestsCache.lat == 0 && requestsCache.lon == 0;
	    	if (isErrorInCache) {
	    		//Try to update from Google
	    		updateCacheFromGoogleRequest(requestsCache, httpClient, location, mcc, mnc, lac, cid);	    		
	    	}
	    	break;
		}
	}

	protected static void updateCacheFromYandexRequest(MccMncLacCidRequestCacheEntry requestsCache, HttpClient httpClient,
			ILocation location, int mcc, int mnc, int lac, int cid)
			throws IOException, ClientProtocolException, SAXException,
			ParserConfigurationException {
		String requestUrl = String.format(YANDEX_GSM_REQUEST_URL_TEMPLATE, mcc, mnc, lac, cid);
		HttpGet request = HttpRequestFactory.newGet(requestUrl, 3, 5);
		HttpResponse response = httpClient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseMessage = getString(entity.getContent());
		//responseMessage = responseMessage.replaceFirst("\\<\\?.*\\?\\>", "");
		Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(responseMessage.getBytes()));
		NodeList htmlNode = doc.getElementsByTagName("html");
		if (htmlNode != null && htmlNode.getLength() > 0) {
        	//TODO: Google request
			return;
		}
		NodeList errorNode = doc.getElementsByTagName("error");
		if (errorNode != null && errorNode.getLength() > 0) {
		        requestsCache.update(mcc, mnc, lac, cid, 0, 0, 0);
        	//TODO: Google request
			return;
		}
		Node node = doc.getElementsByTagName("coordinates").item(0);
		NamedNodeMap attrs = node.getAttributes();
		String latitude = attrs.getNamedItem("latitude").getNodeValue();
		String longitude = attrs.getNamedItem("longitude").getNodeValue();
		Double latDouble = Double.valueOf(latitude);
		int lat = (int) (latDouble * location.getFactor());
		Double lonDouble = Double.valueOf(longitude);
		int lon = (int) (lonDouble * location.getFactor());
		location.setLat(lat);
		location.setLon(lon);

		String nlatitude = attrs.getNamedItem("nlatitude").getNodeValue();
		String nlongitude = attrs.getNamedItem("nlongitude").getNodeValue();
		Double nlatDouble = Double.valueOf(nlatitude);
		Double nlonDouble = Double.valueOf(nlongitude);
		int acc = (int) (Math.sqrt((nlatDouble - latDouble) * (nlatDouble - latDouble) + (nlonDouble - lonDouble) * (nlonDouble - lonDouble)) * location.getFactor());
		location.setAcc(acc);
		requestsCache.update(mcc, mnc, lac, cid, lat, lon, acc);
	}
	
	protected static void updateCacheFromGoogleRequest(MccMncLacCidRequestCacheEntry requestsCache, HttpClient httpClient,
			ILocation location, int mcc, int mnc, int lac, int cid)
			throws IOException, ClientProtocolException, SAXException,
			ParserConfigurationException {
		String requestUrl = GOOGLE_GSM_REQUEST_URL;
		//, mcc, mnc, lac, cid);
		HttpPost request = HttpRequestFactory.newPost(requestUrl, 3, 5);
		//String data = "000E00000000000000000000000000001B000000000000000000000003000001CFDB94000018D9000000FA00000002FFFFFFFF00000000";
		//InputStreamEntity inputStreamEntity = new InputStreamEntity();
		//request.setEntity(inputStreamEntity);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		writeData(stream, mcc, mnc, lac, cid);
		request.setEntity(new ByteArrayEntity(stream.toByteArray()));
		request.setHeader("Content-Type", "application/binary");
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		//Not success request - nothing do
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		HttpEntity entity = response.getEntity();
		DataInputStream dataInputStream = new DataInputStream(entity.getContent());
        
        //---interpret the response obtained---
        dataInputStream.readShort();
        dataInputStream.readByte();
        int code = dataInputStream.readInt();
        if (code == 0) {
            double latDouble = (double) dataInputStream.readInt() / 1000000D;
            double lonDouble = (double) dataInputStream.readInt() / 1000000D;
            int i1 = dataInputStream.readInt();
            int i2 = dataInputStream.readInt();
            String str = dataInputStream.readUTF();
    		int lat = (int) (latDouble * location.getFactor());
    		int lon = (int) (lonDouble * location.getFactor());
    		location.setLat(lat);
    		location.setLon(lon);
    		int acc = i1 < 10000 ? i1 * 100: 1000 * 100;//Average by one kilometer
    		location.setAcc(acc);
    		requestsCache.update(mcc, mnc, lac, cid, lat, lon, acc);
        }
        else
        {
        	//TODO: Yandex request
        	Log.w(TAG, String.format("false-resolving-of-cell-location[mcc=%s, mnc=%s, lac=%s, cid=%s]", mcc, mnc, lac, cid));
	        requestsCache.update(mcc, mnc, lac, cid, 0, 0, 0);
        }
	}
	
	protected static void updateCacheFromGoogleRequest2(Context context, final MccMncLacCidRequestCacheEntry requestsCache, HttpClient httpClient,
			final ILocation location, final int mcc, final int mnc, final int lac, final int cid)
			throws IOException, ClientProtocolException, SAXException,
			ParserConfigurationException, InterruptedException {
		if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) != ConnectionResult.SUCCESS) {
	        requestsCache.update(mcc, mnc, lac, cid, 0, 0, 0);
	        Log.d(TAG, "google-play-services-are-unavailables");
	        return;
		}
		
		final LocationManager locationManager = (LocationManager) context
			    .getSystemService(Context.LOCATION_SERVICE);
		if (!locationManager
			    .isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
	        requestsCache.update(mcc, mnc, lac, cid, 0, 0, 0);
	        Log.d(TAG, "google-play-services-not-requested[network-provider-not-enabled]");
	        return;
		}
		
		long timer0 = System.currentTimeMillis();
		final AtomicBoolean requestComplete = new AtomicBoolean(false);//Only for reference
		
		final LocationListener locationListener = new LocationListener() {

			@Override
			public void onLocationChanged(android.location.Location arg0) {
				Log.d(TAG, "location-changed-for-google-play-service-listener");
				double latDouble = arg0.getLatitude();
				double lonDouble = arg0.getLongitude();
	    		int lat = (int) (latDouble * location.getFactor());
	    		int lon = (int) (lonDouble * location.getFactor());
	    		location.setLat(lat);
	    		location.setLon(lon);
	    		int acc = (int) (arg0.getAccuracy() * 100);
	    		location.setAcc(acc);
	    		requestsCache.update(mcc, mnc, lac, cid, lat, lon, acc);
	    		synchronized(monitorLc2) {
	    			requestComplete.set(true);
	    			monitorLc2.notify();
	    		}
			}
			
		};
		
		final AtomicReference<LocationClient> locationClientRef = new AtomicReference<LocationClient>();
		locationClientRef.set(new LocationClient(context, new GooglePlayServicesClient.ConnectionCallbacks() {

			@Override
			public void onConnected(Bundle arg0) {
				try {
					// TODO Auto-generated method stub
					Log.d(TAG, "google-services-location-client-connected");
					
					LocationClient locationClient = locationClientRef.get();
					
					//Log.d(TAG, String.valueOf(locationClient.getLastLocation()));
					
					LocationRequest locationRequest = LocationRequest.create();
					//locationRequest.setExpirationDuration(millis)
					locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
					locationRequest.setSmallestDisplacement(1);				
					locationRequest.setInterval(5000);
					locationRequest.setFastestInterval(1000);
					locationRequest.setNumUpdates(1);
					
					locationClient.requestLocationUpdates(locationRequest, locationListener);
				} catch (Throwable th) {
					Log.wtf(TAG, th);
		    		synchronized(monitorLc2) {
		    			requestComplete.set(true);
		    			monitorLc2.notify();
		    		}
				}
				
			}

			@Override
			public void onDisconnected() {
				// TODO Auto-generated method stub
				Log.d(TAG, "google-services-location-client-disconnected");
			}
			
		}, new GooglePlayServicesClient.OnConnectionFailedListener() {

			@Override
			public void onConnectionFailed(ConnectionResult arg0) {
				Log.d(TAG, "google-services-location-client-connection-failed");
	    		synchronized(monitorLc2) {
	    			requestComplete.set(true);
	    			monitorLc2.notify();
	    		}
			}
			
		}));
		
        requestsCache.update(mcc, mnc, lac, cid, 0, 0, 0);
		locationClientRef.get().connect();

		try {
			synchronized(monitorLc2) {
				monitorLc2.wait(TimeUnit.SECONDS.toMillis(15));
			}
		} finally {
			Log.d(TAG, String.format("request-time[%sms]", System.currentTimeMillis() - timer0));
			LocationClient locationClient = locationClientRef.get();
			if (locationClient.isConnected()) {
				locationClientRef.get().removeLocationUpdates(locationListener);
				locationClientRef.get().disconnect();
			}
		}
//        if (code == 0) {
//            double latDouble = (double) dataInputStream.readInt() / 1000000D;
//            double lonDouble = (double) dataInputStream.readInt() / 1000000D;
//            int i1 = dataInputStream.readInt();
//            int i2 = dataInputStream.readInt();
//            String str = dataInputStream.readUTF();
//    		int lat = (int) (latDouble * location.getFactor());
//    		int lon = (int) (lonDouble * location.getFactor());
//    		location.setLat(lat);
//    		location.setLon(lon);
//    		int acc = i1 < 10000 ? i1 * 100: 1000 * 100;//Average by one kilometer
//    		location.setAcc(acc);
//    		requestsCache.update(mcc, mnc, lac, cid, lat, lon, acc);
//        }
//        else
//        {
//        	//TODO: Yandex request
//        	Log.w(TAG, String.format("false-resolving-of-cell-location[mcc=%s, mnc=%s, lac=%s, cid=%s]", mcc, mnc, lac, cid));
//	        requestsCache.update(mcc, mnc, lac, cid, 0, 0, 0);
//        }
	}
	
	protected static void updateCacheFromGoogleRequest3(Context context, final MccMncLacCidRequestCacheEntry requestsCache, HttpClient httpClient,
			final ILocation location, final int mcc, final int mnc, final int lac, final int cid)
			throws IOException, ClientProtocolException, SAXException,
			ParserConfigurationException, InterruptedException {
		long timer0 = System.currentTimeMillis();
		final LocationManager locationManager = (LocationManager) context
			    .getSystemService(Context.LOCATION_SERVICE);
		if (!locationManager
			    .isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
	        requestsCache.update(mcc, mnc, lac, cid, 0, 0, 0);
	        Log.d(TAG, "network-provider-not-enabled");
	        return;
		}
		final android.location.LocationListener locationListener = new android.location.LocationListener() {
		    @Override
		    public void onStatusChanged(String provider, int status, Bundle extras) {
		    }
		    @Override
		    public void onProviderEnabled(String provider) {
		    }
		    @Override
		    public void onProviderDisabled(String provider) {
		    }
		    @Override
		    public void onLocationChanged(final android.location.Location arg0) {
				Log.d(TAG, "location-changed-for-network-provider-location-listener");
				double latDouble = arg0.getLatitude();
				double lonDouble = arg0.getLongitude();
	    		int lat = (int) (latDouble * location.getFactor());
	    		int lon = (int) (lonDouble * location.getFactor());
	    		location.setLat(lat);
	    		location.setLon(lon);
	    		int acc = (int) (arg0.getAccuracy() * 100);
	    		location.setAcc(acc);
	    		requestsCache.update(mcc, mnc, lac, cid, lat, lon, acc);
	    		synchronized(monitorLc3) {
	    			monitorLc3.notify();
	    		}
		    }
		};
        requestsCache.update(mcc, mnc, lac, cid, 0, 0, 0);
		
		new Handler(context.getMainLooper()).post(new Runnable() {

			@Override
			public void run() {
				locationManager.requestLocationUpdates(
					    LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
			}
			
		});
		try {
			synchronized(monitorLc3) {
				monitorLc3.wait(TimeUnit.SECONDS.toMillis(10));
			}
		} finally {
			Log.d(TAG, String.format("request-time[%sms]", System.currentTimeMillis() - timer0));
			locationManager.removeUpdates(locationListener);
		}
	}
	
	
	//@see http://www.devx.com/wireless/Article/40524/0/page/2
	private static void writeData(OutputStream out, int mcc, int mnc, int lac, int cid)
			throws IOException {
		
		DataOutputStream dataOutputStream = new DataOutputStream(out);
		dataOutputStream.writeShort(21);
		dataOutputStream.writeLong(0);
		dataOutputStream.writeUTF("en");
		dataOutputStream.writeUTF("Android");
		dataOutputStream.writeUTF("1.0");
		dataOutputStream.writeUTF("Web");
		dataOutputStream.writeByte(27);
		dataOutputStream.writeInt(0);
		dataOutputStream.writeInt(0);
		dataOutputStream.writeInt(3);
		dataOutputStream.writeUTF("");

		dataOutputStream.writeInt(cid);
		dataOutputStream.writeInt(lac);

		dataOutputStream.writeInt(mcc);
		dataOutputStream.writeInt(mnc);
		dataOutputStream.writeInt(0);
		dataOutputStream.writeInt(0);
		dataOutputStream.flush();
	}

	public static boolean safetyUpdateGeoCoordinates(HipdriverApplication hipdriverApplication, HttpClient httpClient, ILocation location, int mcc, int mnc, int lac, int cid) {
		if (ILocation.Validator.isUnknownGsmLocation(mcc, mnc, lac, cid)) {
			return false;
		}
		try {
			updateGpsCoordinatesByGsm(hipdriverApplication, httpClient, location, mcc, mnc, lac, cid);
			return true;
		} catch (IOException e) {
			return retryGpsCoordinatesRequest(hipdriverApplication, httpClient, location, mcc, mnc,
					lac, cid);
		} catch (SAXException sex) {
			return retryGpsCoordinatesRequest(hipdriverApplication, httpClient, location, mcc, mnc,
					lac, cid);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return false;
		}
	}

	protected static boolean retryGpsCoordinatesRequest(HipdriverApplication hipdriverApplication, HttpClient httpClient,
			ILocation location, int mcc, int mnc, int lac, int cid) {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException ex) {
			return false;
		}
		try {
			updateGpsCoordinatesByGsm(hipdriverApplication, httpClient, location, mcc, mnc, lac, cid);
			return true;
		} catch (Throwable thex) {
			Log.wtf(TAG, thex);
		}
		return false;
	}

	public void sendGsmMessage(EventClassesEnum eventClass, int mcc, int mnc, int lac, int cid, Date time) throws Throwable {
		if (sid == null ) {
			throw new RuntimeException("Отсутствует безопасное соединение");
		}
		HttpClient httpClient = getHttpClient();
		Event event = new Event();
		//TODO: Update event classes id before sending or use sign-in process
		event.setEventClassId(DBLinks.link(eventClass));
		event.setTime(time);
		event.setMcc(mcc);
		event.setMnc(mnc);
		event.setLac(lac);
		event.setCid(cid);
		Locations.packGsmLocation(event);
		if (safetyUpdateGeoCoordinates(context, httpClient, event, mcc, mnc, lac, cid)) {
			Locations.packGpsLocation(event);
		}
		event.setTime(null);//Move to packed buffer
		ObjectMapper om = getObjectMapper();
		String postMessage = om.writeValueAsString(event);
		HttpPost request = HttpRequestFactory.newJsonPost(SERVER_HOST_NAME, ServicesEnum.KSEP.PATH, 3, 5, sid, postMessage);
		HttpResponse response = httpClient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseMessage = getString(entity.getContent());
		if ( isHtml(entity) ) {
			String errorMessage = parseFirstH1(responseMessage);
			throw new RuntimeException(errorMessage);
		}
	}

	public void sendGsmMessageA(EventClassesEnum eventClass, String description, byte[] content, int mcc, int mnc, int lac, int cid, Date time) throws Throwable {
		if (sid == null ) {
			throw new RuntimeException("Отсутствует безопасное соединение");
		}
		HttpClient httpClient = getHttpClient();
		EventWithAtach event = new EventWithAtach();
		//TODO: Update event classes id before sending or use sign-in process
		event.setEventClassId(DBLinks.link(eventClass));
		event.setTime(time);
		event.setMcc(mcc);
		event.setMnc(mnc);
		event.setLac(lac);
		event.setCid(cid);
		event.setDescription(description);
		event.setContent(content);
		Locations.packGsmLocation(event);
		if (safetyUpdateGeoCoordinates(context, httpClient, event, mcc, mnc, lac, cid)) {
			Locations.packGpsLocation(event);
		}
		event.setTime(null);//Move to packed buffer
		ObjectMapper om = getObjectMapper();
		String postMessage = om.writeValueAsString(event);

		HttpPost request = HttpRequestFactory.newJsonPost(SERVER_HOST_NAME, ServicesEnum.KSEPA.PATH, 3, 5, sid, postMessage);
		HttpResponse response = httpClient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseMessage = getString(entity.getContent());
		if ( isHtml(entity) ) {
			String errorMessage = parseFirstH1(responseMessage);
			throw new RuntimeException(errorMessage);
		}
	}

	public void sendLocationCorrectionMessage(EventClassesEnum eventClass, int lat, int lon, int alt, int acc, int mcc, int mnc, int lac, int cid, Date time) throws Throwable {
		if (sid == null ) {
			throw new RuntimeException("Отсутствует безопасное соединение");
		}
		HttpClient httpclient = getHttpClient();
		Event event = new Event();
		//TODO: Update event classes id before sending or use sign-in process
		event.setEventClassId(DBLinks.link(eventClass));
		event.setTime(time);
		event.setLat(lat);
		event.setLon(lon);
		event.setAlt(alt);
		event.setAcc(acc);
		Locations.packGpsLocation(event);
		event.setMcc(mcc);
		event.setMnc(mnc);
		event.setLac(lac);
		event.setCid(cid);
		Locations.packGsmLocation(event);
		event.setTime(null);//Move to packed buffer
		ObjectMapper om = getObjectMapper();
		String postMessage = om.writeValueAsString(event);
		HttpPost request = HttpRequestFactory.newJsonPost(SERVER_HOST_NAME, ServicesEnum.KSEP.PATH, 3, 5, sid, postMessage);
		HttpResponse response = httpclient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseMessage = getString(entity.getContent());
		if ( isHtml(entity) ) {
			String errorMessage = parseFirstH1(responseMessage);
			throw new RuntimeException(errorMessage);
		}
	}

	public int ping() throws ClientProtocolException, IOException {
		long mobileAgentId = getA().getLong(MOBILE_AGENT_ID_KEY, 0L);
		HttpClient httpClient = getHttpClient();
		byte[] value = makeMid(mobileAgentId);
		String string = new String(value);
		StringEntity stringEntity = new StringEntity(string);
		HttpPut request = HttpRequestFactory.newPut(SERVER_HOST_NAME, ServicesEnum.KPP.PATH, 3, 5);
		request.setEntity(stringEntity);
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		return 0;
	}

	public int putPubkey(byte[] pubkey) throws ClientProtocolException, IOException {
		long mobileAgentId = getA().getLong(MOBILE_AGENT_ID_KEY, 0L);
		HttpClient httpClient = getHttpClient();
		long mid = mobileAgentId ^ ISid.SALTMA;
		StringBuilder text = new StringBuilder();
		text.append(Long.toString(mid)).append('\n');
		String pubkeyText = Base64.encodeToString(pubkey, Base64.DEFAULT);
		text.append(pubkeyText);
		StringEntity stringEntity = new StringEntity(text.toString());
		HttpPut request = HttpRequestFactory.newPut(SERVER_HOST_NAME, ServicesEnum.KPPKU.PATH, 3, 5);
		request.setEntity(stringEntity);
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		if (returnCode < 200 || returnCode >= 300) {
			return returnCode;
		}
		return 0;
	}

	public int getMymas(Map<Long, String> mymas) throws ClientProtocolException, IOException {
		long mobileAgentId = getA().getLong(MOBILE_AGENT_ID_KEY, 0L);
		HttpClient httpClient = getHttpClient();
		String servicePath = ServicesEnum.KPM.PATH;
		HttpPost request = makePostRequest(servicePath, new String(makeMid(mobileAgentId)));
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		//Not success request - nothing do
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IoTools.pipe(response.getEntity().getContent(), out, 1024);
		String text = new String(out.toByteArray());
		//TODO: check format and throw if strange format
		String[] mas = text.split("\n");
		int i = 0;
		for (String ma : mas) {
			if (ma.trim().isEmpty()) {
				continue;
			}
			int idx = ma.indexOf('\t');
			final long mid;
			final String name;
			if (idx > -1) {
				String longB64E = ma.substring(0, idx);
				byte[] bb = Base64.decode(longB64E.getBytes(), Base64.DEFAULT);
				if (bb.length < 8) {
					continue;
					//mid = idx * i; 
				}
				ByteBuffer bbuff = ByteBuffer.wrap(bb);
				mid = bbuff.getLong() ^ ISid.SALTSS;
				if (mid <= 0) {
					continue;
				}
				name = ma.substring(idx + 1);
			} else {
				mid = idx * i; 
				name = ma;
			}
			mymas.put(mid, name);
			i++;
		}
		return 0;
	}
	
	private HttpPost makePostRequest(String servicePath, String... args) throws UnsupportedEncodingException {
		return makePostRequest(servicePath, 3, 5, args);
	}
	
	private HttpPost makePostRequest(String servicePath, int connectionTimeoutInSeconds, int socketTimeoutInSeconds, String... args)
			throws UnsupportedEncodingException {
		StringBuilder text = new StringBuilder();
		for (String arg : args) {
			//TODO: check line separator for all arguments
			if (arg == null) {
				text.append('\n');
			} else if (arg.indexOf('\n') == -1) {
				text.append(arg).append('\n');
			} else {
				text.append(arg);
			}
		}
		StringEntity stringEntity = new StringEntity(text.toString());
		HttpPost request = HttpRequestFactory.newPost(SERVER_HOST_NAME, servicePath, connectionTimeoutInSeconds, connectionTimeoutInSeconds);
		request.setEntity(stringEntity);
		return request;
	}
	
	protected byte[] makeMid(long mobileAgentId) {
		if (mobileAgentId <= 0) {
			return ZERO_BYTES_COUNT;
		}
		ByteBuffer bb = ByteBuffer.wrap(new byte[8]); 
		//TODO: MORE STRONG SECURITY
		long mid = mobileAgentId ^ ISid.SALTMA;
		bb.position(0);
		bb.putLong(mid);
		byte[] value =  Base64.encode(bb.array(), Base64.DEFAULT);
		return value;
	}
	
	public int getPubkey(long mobileAgentId, ByteBuffer holder) throws ClientProtocolException, IOException {
		HttpClient httpClient = getHttpClient();
		String servicePath = ServicesEnum.KPPK.PATH;
		HttpPost request = makePostRequest(servicePath, new String(makeMid(mobileAgentId)));
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		//Not success request - nothing do
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IoTools.pipe(response.getEntity().getContent(), out);
		holder.put(out.toByteArray());
		return 0;
	}

	public int getHost(long mobileAgentId, String localStreamDescriptor, long targetMobileAgentId, AtomicReference<String> remoteStreamDescriptionRef) throws ClientProtocolException, IOException {
		HttpClient httpClient = getHttpClient();
		String servicePath = ServicesEnum.KPH.PATH;
		HttpPost request = makePostRequest(servicePath, new String(makeMid(mobileAgentId)),
				new String(makeMid(targetMobileAgentId)), localStreamDescriptor);
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		//Not success request - nothing do
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IoTools.pipe(response.getEntity().getContent(), out);
		remoteStreamDescriptionRef.set(new String(out.toByteArray()));
		return 0;
	}
	
	public int getMobileAgentState(long mobileAgentId, AtomicReference<MobileAgentWithState> mobileAgentWithStateRef) throws IllegalStateException, IOException {
		HttpClient httpClient = getHttpClient();
		String servicePath = ServicesEnum.KPMAS.PATH;
		HttpPost request = makePostRequest(servicePath, new String(makeMid(mobileAgentId)));
		
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		//Not success request - nothing do
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IoTools.pipe(response.getEntity().getContent(), out);
		ObjectMapper om = getObjectMapper();
		byte[] byteArray = out.toByteArray();
		if (byteArray.length == 0) {
			return returnCode;
		}
		MobileAgentWithState mas = om.readValue(byteArray, MobileAgentWithState.class);
		mobileAgentWithStateRef.set(mas);
		return 0;
		
	}

	public int getMobileAgentProfile(long mobileAgentId,
			AtomicReference<Properties> propertiesRef) throws ClientProtocolException, IOException {
		HttpClient httpClient = getHttpClient();
		String servicePath = ServicesEnum.KPMAP.PATH;
		HttpPost request = makePostRequest(servicePath, new String(makeMid(mobileAgentId)));
		
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		//Not success request - nothing do
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		Properties properties = new Properties();
		properties.load(response.getEntity().getContent());
		propertiesRef.set(properties);
		return 0;
	}

	public int changeMobileAgentProfile(long mobileAgentId,
			Properties changedProperties, AtomicBoolean result) throws ClientProtocolException, IOException {
		HttpClient httpClient = getHttpClient();
		String servicePath = ServicesEnum.KPEMAP.PATH;
		ByteArrayOutputStream arg = new ByteArrayOutputStream();
		changedProperties.save(arg, "");
		HttpPost request = makePostRequest(servicePath, new String(makeMid(mobileAgentId)),
				new String(arg.toByteArray()));
		
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		//Not success request - nothing do
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			IoTools.pipe(response.getEntity().getContent(), out);
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] byteArray = out.toByteArray();
		String responseText = new String(byteArray).trim();
		result.set(Boolean.parseBoolean(responseText));
		return 0;
	}

	public int checkEmail(String email, AtomicBoolean result) throws ClientProtocolException, IOException {
		HttpClient httpClient = getHttpClient();
		String servicePath = ServicesEnum.KCHEML.PATH;
		HttpPost request = makePostRequest(servicePath, email);
		
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		//Not exists
		if (returnCode == 400) {
			result.set(false);
			return 0;
		}
		//Not success request - nothing do
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		result.set(true);
		return 0;
	}
	
	public int register(String user, String email, String password, AtomicBoolean result) throws ClientProtocolException, IOException, InterruptedException {
		//Not cached-request
		HttpClient httpClient = new DefaultHttpClient();
		String service = "/user/register";
		//GET REQUEST
		HttpGet initialRequest = HttpRequestFactory.newGet(WEB_HOST_NAME, service, 30, 50);
		Locale current = getA().getResources().getConfiguration().locale;
		initialRequest.addHeader("Accept-Language", current.getLanguage());
		HttpResponse initialResponse = httpClient.execute(initialRequest);
		String initialResponseMessage = getString(initialResponse.getEntity().getContent());
		//Log.d(TAG, responseMessage);
		org.jsoup.nodes.Document doc = Jsoup.parse(initialResponseMessage);
		Elements elements = doc.select("input[type=hidden]");
		Map<String, String> inputs = new HashMap<String, String>();
        for (Iterator<Element> iterator = elements.iterator(); iterator.hasNext();) {
            Element element = iterator.next();
            inputs.put(element.attr("name"), element.attr("value"));
        }
        
        //Check valid of form:
        if (inputs.get("honeypot_time") == null) {
        	throw new IllegalStateException("Invalid registration form");
        }
        
		HttpPost request = HttpRequestFactory.newPost(WEB_HOST_NAME, service, 30, 50);
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		entity.addPart("name", new StringBody(user));
		entity.addPart("mail", new StringBody(email));
		entity.addPart("pass[pass1]", new StringBody(password));
		entity.addPart("pass[pass2]", new StringBody(password));
		entity.addPart("honeypot_time", new StringBody(inputs.get("honeypot_time")));
		entity.addPart("timezone", new StringBody(inputs.get("timezone")));
		entity.addPart("form_id", new StringBody(inputs.get("form_id")));
		entity.addPart("form_build_id", new StringBody(inputs.get("form_build_id")));
		request.setEntity(entity);
		request.addHeader("Accept-Language", current.getLanguage());
		
		Thread.sleep(TimeUnit.SECONDS.toMillis(10));
		
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		if (returnCode < 200 || returnCode >= 300) {
			result.set(false);
			return returnCode;
		}
		
		String message = null;	
		try {
			//String typeValue = response.getEntity().getContentType().getValue();
			//result.set(typeValue.startsWith("text/html"));
			//For success search logged-in class in body tag
			message = getString(response.getEntity().getContent());
			org.jsoup.nodes.Document responseDoc = Jsoup.parse(message);
			Elements bodys = responseDoc.select("body");
			for (Iterator<Element> iterator = bodys.iterator(); iterator.hasNext();) {
	            Element element = iterator.next();
	            String classes = element.attr("class");
	            if (!classes.contains("not-logged-in")) {
	            	result.set(true);
	            	break;
	            };
	        }
			
//			Map<String, String> inputs2 = new HashMap<String, String>();
//			Elements elements2 = responseDoc.select("input[type=hidden]");
//	        for (Iterator<Element> iterator = elements2.iterator(); iterator.hasNext();) {
//	            Element element = iterator.next();
//	            inputs2.put(element.attr("name"), element.attr("value"));
//	        }		
			
			
//			System.out.println(message);
		} catch (Throwable th) {
			result.set(false);
		}
		return 0;
	}
	
	public int getEncodedPasswordHashForEmail(String email, AtomicReference<String> result) throws ClientProtocolException, IOException {
		HttpClient httpClient = getHttpClient();
		String servicePath = ServicesEnum.KHASH.PATH;
		HttpPost request = makePostRequest(servicePath, email);
		
		HttpResponse response = httpClient.execute(request);
		StatusLine statusLine = response.getStatusLine();
		int returnCode = statusLine.getStatusCode();
		//Not exists
		if (returnCode == 400) {
			result.set(null);
			return 0;
		}
		//Not success request - nothing do
		if (returnCode < 200 || returnCode >= 300) {
			throw new IllegalStateException("Failed response with code: " + returnCode);
		}
		HttpEntity entity = response.getEntity();
		String responseMessage = getString(entity.getContent());		
		result.set(responseMessage);
		return 0;
	}
	
}
