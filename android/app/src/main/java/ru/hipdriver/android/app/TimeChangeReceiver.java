package ru.hipdriver.android.app;

import java.util.Date;

import android.content.Context;
import android.content.Intent;

public class TimeChangeReceiver extends HipdriverBroadcastReceiver {
	
	private long lastTime;
	private long ticksCount;
	
	@Override
	public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action == null) {
        	return;
        }
        //Save time before change
        if (action.equals(Intent.ACTION_TIME_TICK)) {
        	lastTime = System.currentTimeMillis();
        	ticksCount++;
        	//Every hour update hours to live
        	if (ticksCount % 60 == 0) {
        		updateHTL(getA(context));
        	}
        	return;
        }
        
        long shiftInMillis = System.currentTimeMillis() - lastTime; 
        
		HipdriverApplication hipdriverApplication = getA(context);
        if (action.equals(Intent.ACTION_TIME_CHANGED) ||
            action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
    		Date expiredDate = hipdriverApplication.getDate(HipdriverApplication.EXPIRED_DATE);
        	Date shiftedDate = new Date(expiredDate.getTime() + shiftInMillis);
        	hipdriverApplication.setDate(HipdriverApplication.EXPIRED_DATE, shiftedDate);
        }
	}

	private void updateHTL(HipdriverApplication hipdriverApplication) {
		if (hipdriverApplication == null) {
			return;
		}
		hipdriverApplication.increaseHtl();
	}

}
