package ru.hipdriver.android.app.help;

import java.lang.reflect.Field;

import ru.hipdriver.android.app.HipdriverActivity;
import ru.hipdriver.android.app.R;
import ru.hipdriver.android.util.TextTools;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;

public class ShortHelpActivity extends HipdriverActivity {

	protected static final String TAG = TextTools
			.makeTag(ShortHelpActivity.class);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.short_help_screen);
		
		//Add pages
		ViewPager viewPager = (ViewPager) findViewById(R.id.short_help_pager);
		ShortHelpCollectionPagerAdapter faceSelectCollectionPagerAdapter = new ShortHelpCollectionPagerAdapter(getA(), getSupportFragmentManager(), viewPager);
		viewPager.setAdapter(faceSelectCollectionPagerAdapter);
		
		PagerTabStrip pagerTabStrip = (PagerTabStrip) findViewById(R.id.short_help_pager_tab_strip);
		//pagerTabStrip.setTabIndicatorColorResource(R.color.semi_transparrent);
		//TODO: refactoring
		pagerTabStrip.setTabIndicatorColorResource(R.color.short_help_link_background_gradient_start);
		//SETUP spacing between tabs
		try {
			Field mMinTextSpacing = pagerTabStrip.getClass().getDeclaredField("mMinTextSpacing");
			mMinTextSpacing.setAccessible(true);
			mMinTextSpacing.set(pagerTabStrip, 0);
			pagerTabStrip.setTextSpacing(0);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		//pagerTabStrip.setDrawFullUnderline(true);
		//pagerTabStrip.setPadding(0, 5, 0, 5);
		
	}
	
	protected ShortHelpCollectionPagerAdapter getShortHelpCollectionPagerAdapter() {
		ViewPager pager = (ViewPager) findViewById(R.id.short_help_pager);
		if (pager == null) {
			Log.w(TAG, "get-short-help-collection-pager-adapter-impossible[pager-not-found]");
			return null;
		}
		PagerAdapter adapter = pager.getAdapter();
		if (!(adapter instanceof ShortHelpCollectionPagerAdapter)) {
			Log.w(TAG, "get-short-help-collection-pager-adapter-impossible[adapter-not-cast]");
			return null;
		}
		return (ShortHelpCollectionPagerAdapter) adapter; 		
	}
	
	
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
       	this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
    }
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {     

        if(keyCode != KeyEvent.KEYCODE_BACK)
        {
        	return super.onKeyDown(keyCode, event);
        }
        
		ShortHelpCollectionPagerAdapter adapter = getShortHelpCollectionPagerAdapter();
		if (adapter == null) {
        	return super.onKeyDown(keyCode, event);
		}
		if ( adapter.showPrevPage() < 0) {
        	return super.onKeyDown(keyCode, event);
		}
        return true;
    }

}
