package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;

import ru.hipdriver.android.app.R;

public class NotificationsCarAlarmsSettingsFragment extends FaceSelectCollectionHipdriverFragment {

	private static final String TAG = TextTools.makeTag(NotificationsCarAlarmsSettingsFragment.class);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.notifications_car_alarms_settings_fragment,
				container, false);
		refresh(view);
		return view;
	}

	private void refresh(final View view) {
		if (view == null) {
			return;
		}
		FragmentActivity activity = getActivity();
		
		final HintedCheckBox ssaecdHintedCheckBox = (HintedCheckBox) view.findViewById(R.id.send_sms_after_event_class_detection_checkbox);
		ssaecdHintedCheckBox.setChecked(getA().getBoolean(IMobileAgentProfile.SEND_SMS_AFTER_EVENT_CLASS_DETECTION));
		ssaecdHintedCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getA().setBoolean(IMobileAgentProfile.SEND_SMS_AFTER_EVENT_CLASS_DETECTION, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.SEND_SMS_AFTER_EVENT_CLASS_DETECTION);
			}
		});
		
		EditText emailEditor = (EditText) view.findViewById(R.id.email_editor);
		String email = getA().getString(IMobileAgent.ALERT_EMAIL_KEY, "");
		emailEditor.setText(email);
		emailEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.email_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		emailEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String email = s.toString();
    			getA().setString(IMobileAgent.ALERT_EMAIL_KEY, email);
    			getA().setMemoryVersion(IMobileAgent.ALERT_EMAIL_KEY);
			}
			
		});
		
		final HintedCheckBox seaecdHintedCheckBox = (HintedCheckBox) view.findViewById(R.id.send_email_after_event_class_detection_checkbox);
		seaecdHintedCheckBox.setChecked(getA().getBoolean(IMobileAgentProfile.SEND_EMAIL_AFTER_EVENT_CLASS_DETECTION));
		seaecdHintedCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getA().setBoolean(IMobileAgentProfile.SEND_EMAIL_AFTER_EVENT_CLASS_DETECTION, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.SEND_EMAIL_AFTER_EVENT_CLASS_DETECTION);
			}
		});
		
//		scrollView.scrollTo(0, 0);
//		scrollView.setOnScrollByY(new HipdriverScrollView.OnScrollByY() {
//			@Override
//			public void scrollByY(int scrollY) {
//				saveScrollPosition(R.layout.car_alarms_settings_affecting_the_balance_of_payments_screen, scrollY);//Current screen
//			}
//		});
//		scrollView.postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				scrollView.scrollTo(0, scrollY);
//			}
//		}, 200);
		
		EditText alertCallsCountEditor = (EditText) view.findViewById(R.id.alert_calls_count_editor);
		alertCallsCountEditor.setText(String.valueOf(getA().getInt(IMobileAgentProfile.ALERT_CALLS_COUNT)));
		alertCallsCountEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.alert_calls_count_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		alertCallsCountEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String alertCallsCountText = s.toString();
    			int alertCallsCount = TextTools.safetyGetInt(alertCallsCountText, IMobileAgentProfile.DEFAULT_ALERT_CALLS_COUNT);
    			getA().setInt(IMobileAgentProfile.ALERT_CALLS_COUNT, alertCallsCount);
    			getA().setMemoryVersion(IMobileAgentProfile.ALERT_CALLS_COUNT);
			}
			
		});
		
		EditText alertCallsIntervalEditor = (EditText) view.findViewById(R.id.alert_calls_interval_editor);
		alertCallsIntervalEditor.setText(String.valueOf(getA().getInt(IMobileAgentProfile.ALERT_CALLS_INTERVAL_IN_SECONDS)));
		alertCallsIntervalEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.alert_calls_interval_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		alertCallsIntervalEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String alertCallsIntervalText = s.toString();
    			int alertCallsInterval = TextTools.safetyGetInt(alertCallsIntervalText, IMobileAgentProfile.DEFAULT_ALERT_CALLS_INTERVAL_IN_SECONDS);
    			getA().setInt(IMobileAgentProfile.ALERT_CALLS_INTERVAL_IN_SECONDS, alertCallsInterval);
    			getA().setMemoryVersion(IMobileAgentProfile.ALERT_CALLS_INTERVAL_IN_SECONDS);
			}
			
		});
		
		EditText alertCallsDurationEditor = (EditText) view.findViewById(R.id.alert_calls_duration_editor);
		alertCallsDurationEditor.setText(String.valueOf(getA().getInt(IMobileAgentProfile.ALERT_CALLS_DURATION_IN_SECONDS)));
		alertCallsDurationEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.alert_calls_duration_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		alertCallsDurationEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String alertCallsDurationText = s.toString();
    			int alertCallsDuration = TextTools.safetyGetInt(alertCallsDurationText, IMobileAgentProfile.DEFAULT_ALERT_CALLS_DURATION_IN_SECONDS);
    			getA().setInt(IMobileAgentProfile.ALERT_CALLS_DURATION_IN_SECONDS, alertCallsDuration);
    			getA().setMemoryVersion(IMobileAgentProfile.ALERT_CALLS_DURATION_IN_SECONDS);
			}
			
		});
		
		HintedCheckBox spaaCheckbox = (HintedCheckBox) view.findViewById(R.id.send_photo_after_alert_checkbox);
		OnCheckedChangeListener spaaOnCheckedListener = new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				final ViewGroup viewGroup1 = (ViewGroup) view.findViewById(R.id.send_photo_after_alert_checkbox1);
				final ViewGroup viewGroup2 = (ViewGroup) view.findViewById(R.id.send_photo_after_alert_checkbox2);
				final ViewGroup viewGroup3 = (ViewGroup) view.findViewById(R.id.send_photo_after_alert_checkbox3);
				if (isChecked) {
					enableAllChildsInViewGroup(viewGroup1);
					enableAllChildsInViewGroup(viewGroup2);
					enableAllChildsInViewGroup(viewGroup3);
					
				} else {
					disableAllChildsInViewGroup(viewGroup1);
					disableAllChildsInViewGroup(viewGroup2);
					disableAllChildsInViewGroup(viewGroup3);
					CheckBox waitUntilTheAlertCall = (CheckBox) view.findViewById(R.id.wait_until_the_alert_call_checkbox);
					waitUntilTheAlertCall.setChecked(true);
				}
				getA().setBoolean(IMobileAgentProfile.SEND_PHOTO_AFTER_ALERT, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.SEND_PHOTO_AFTER_ALERT);

			}
			
		};
		spaaCheckbox.setOnCheckedChangeListener(spaaOnCheckedListener);
		boolean isSpaaCheckBoxChecked = getA().getBoolean(IMobileAgentProfile.SEND_PHOTO_AFTER_ALERT);
		spaaCheckbox.setChecked(isSpaaCheckBoxChecked);
		spaaCheckbox.setEnabled(Hardwares.isCameraEnabled(activity));
		spaaOnCheckedListener.onCheckedChanged(spaaCheckbox, isSpaaCheckBoxChecked);
		Spanned spaaText = Html.fromHtml(getResources().getString(R.string.send_photo_after_alert));
		spaaCheckbox.setText(spaaText);
		
		HintedCheckBox mpbocCheckbox = (HintedCheckBox) view.findViewById(R.id.wait_until_the_alert_call_checkbox);
		OnCheckedChangeListener mpbocOnCheckedListener = new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getA().setBoolean(IMobileAgentProfile.WAIT_UNTIL_THE_ALERT_CALL, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.WAIT_UNTIL_THE_ALERT_CALL);

			}
			
		};
		mpbocCheckbox.setOnCheckedChangeListener(mpbocOnCheckedListener);
		boolean isMpbocCheckBoxChecked = getA().getBoolean(IMobileAgentProfile.WAIT_UNTIL_THE_ALERT_CALL);
		mpbocCheckbox.setChecked(isMpbocCheckBoxChecked);
		mpbocOnCheckedListener.onCheckedChanged(mpbocCheckbox, isMpbocCheckBoxChecked);
		
		EditText countOfPhotosEditor = (EditText) view.findViewById(R.id.count_of_photos_editor);
		countOfPhotosEditor.setText(String.valueOf(getA().getInt(IMobileAgentProfile.COUNT_OF_PHOTOS)));
		countOfPhotosEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.count_of_photos_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		countOfPhotosEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String countOfPhotosText = s.toString();
    			int countOfPhotos = TextTools.safetyGetInt(countOfPhotosText, IMobileAgentProfile.DEFAULT_COUNT_OF_PHOTOS);
    			getA().setInt(IMobileAgentProfile.COUNT_OF_PHOTOS, countOfPhotos);
    			getA().setMemoryVersion(IMobileAgentProfile.COUNT_OF_PHOTOS);
			}
			
		});

		EditText intervalBetweenPhotosInSecondsEditor = (EditText) view.findViewById(R.id.interval_between_photos_in_seconds_editor);
		intervalBetweenPhotosInSecondsEditor.setText(String.valueOf(getA().getInt(IMobileAgentProfile.INTERVAL_BETWEEN_PHOTOS_IN_SECONDS)));
		intervalBetweenPhotosInSecondsEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.interval_between_photos_in_seconds_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		intervalBetweenPhotosInSecondsEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String intervalBetweenPhotosInSecondsText = s.toString();
    			int intervalBetweenPhotosInSeconds = TextTools.safetyGetInt(intervalBetweenPhotosInSecondsText, IMobileAgentProfile.DEFAULT_INTERVAL_BETWEEN_PHOTOS_IN_SECONDS);
    			getA().setInt(IMobileAgentProfile.INTERVAL_BETWEEN_PHOTOS_IN_SECONDS, intervalBetweenPhotosInSeconds);
    			getA().setMemoryVersion(IMobileAgentProfile.INTERVAL_BETWEEN_PHOTOS_IN_SECONDS);
			}
			
		});
		
		final HintedCheckBox seopeHintedCheckBox = (HintedCheckBox) view.findViewById(R.id.send_email_on_power_event_checkbox);
		seopeHintedCheckBox.setChecked(getA().getBoolean(IMobileAgentProfile.SEND_EMAIL_ON_POWER_EVENT));
		seopeHintedCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					getA().showMessage(R.string.send_email_on_power_event_hint2);
				}
				getA().setBoolean(IMobileAgentProfile.SEND_EMAIL_ON_POWER_EVENT, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.SEND_EMAIL_ON_POWER_EVENT);
			}
		});
		
		final HintedCheckBox seososoHintedCheckBox = (HintedCheckBox) view.findViewById(R.id.send_email_on_switch_on_switch_off_checkbox);
		seososoHintedCheckBox.setChecked(getA().getBoolean(IMobileAgentProfile.SEND_EMAIL_ON_SWITCH_ON_SWITCH_OFF));
		seososoHintedCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getA().setBoolean(IMobileAgentProfile.SEND_EMAIL_ON_SWITCH_ON_SWITCH_OFF, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.SEND_EMAIL_ON_SWITCH_ON_SWITCH_OFF);
			}
		});
		
		//final ScrollView scrollView = (ScrollView) view.findViewById(R.id.notification_car_alarms_settings_scroll_view);
		//scrollView.scrollTo(0, 0);
		
	}

}
