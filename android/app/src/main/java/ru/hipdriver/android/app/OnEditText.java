package ru.hipdriver.android.app;

import android.text.TextWatcher;

abstract class OnTextEdit implements TextWatcher {
	
	protected int start;
	protected int count;

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		//Log.d(TAG, String.format("before text [%s] changed start[%s], count[%s], after[%s]", s, start, count, after));
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before,
			int count) {
		this.start = start;
		this.count = count;
		//Log.d(TAG, String.format("text [%s] changed start[%s], before[%s], count[%s]", s, start, before, count));
	}
	
	protected boolean isManualChanged(CharSequence s) {
		return start != 0 || s.length() != count || count == 1;
	}
	
}
