package ru.hipdriver.android.app;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import ru.hipdriver.android.util.SizedDataSource;
import ru.hipdriver.android.util.StreamTools;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.i.support.EventClassesEnum;
import ru.hipdriver.j.MobileAgentWithState;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

abstract class UITask extends AsyncTask<After, Void, Void> {

	public static final String TAG = TextTools.makeTag(UITask.class);
	
	protected final HipdriverApplication context;
	protected final long timeoutInMillis;
	protected final String name;
	protected IDebugMessageConsumer dmsgConsumer;
	protected String failureReason;
	private After after;
	protected int returnCode;
	protected Object returnValue;
	//private long timer;
	
	
	public UITask(HipdriverApplication context, String name, long timeoutInMillis) {
		super();
		this.context = context;
		this.name = name;
		this.timeoutInMillis = timeoutInMillis;
	}

	@Override
	protected Void doInBackground(After... arg0) {
		try {
			Log.d(TAG, name);
			safetyPreexec();
			if (context.isDebugMode()) {
				String logName = "ui-tasks.log";
				context.makeDebugInfoHeaderIfNotHeadered(logName, "time,status,simple-class-name,name\n");
				context.writeDebugInfo(logName, "%s,%s,%s,%s\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()), "s", this.getClass().getSimpleName(), name);
			}
			//Wait for executing network task
			context.runNetworkTask(new Runnable() {
	
				@Override
				public void run() {
					if (context.isInterrupted()) {
						returnCode = -1;
						failureReason = "Application execution interrupted";
						return;
					}
					
					//timer = System.currentTimeMillis() + timeoutInMillis;
					long evaluationOfTimeForMultiTryExec1 = System.currentTimeMillis();
					//First shot and success
					if (firstExec()) {
						return;
					}
					
					evaluationOfTimeForMultiTryExec1 = System.currentTimeMillis() - evaluationOfTimeForMultiTryExec1;
					//If Internet Connection not available wait for context#isHasConnection 10 minutes
					long timeForRunMultiTryExec199 = TimeUnit.SECONDS.toMillis(3 * 199) + evaluationOfTimeForMultiTryExec1 * 199;
					long minimalTimeoutInMillis = timeoutInMillis - timeForRunMultiTryExec199 - evaluationOfTimeForMultiTryExec1;
					if (minimalTimeoutInMillis > 0) {
						context.waitForInternetConnection(minimalTimeoutInMillis);
					}
					//Push of network, TODO: try reconnecting
					multiTryExec(199);
				}
				
			}, timeoutInMillis, TimeUnit.MILLISECONDS);
			if (context.isDebugMode()) {
				String logName = "ui-tasks.log";
				context.makeDebugInfoHeaderIfNotHeadered(logName, "time,status,simple-class-name,name\n");
				context.writeDebugInfo(logName, "%s,%s,%s,%s\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()), "f", this.getClass().getSimpleName(), name);
			}
		} catch (Throwable th) {
			returnCode = -1;
			String thMessage = th.getLocalizedMessage();
			failureReason = thMessage != null ? thMessage : "E";
			Log.e(TAG, thMessage, th);
		}
		return null;
	}

	protected boolean firstExec() {
		try {
			exec();
			return true;
		} catch (Throwable th) {
			String thMessage = th.getLocalizedMessage();
			Log.e(TAG, thMessage, th);
			return false;
		}
	}

	private void safetyPreexec() {
		try {
			preExec();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	
	protected boolean multiTryExec(int count) {
		for (int i = 0; i < count; i++) {
			if (context != null && !Hardwares.isOnline(context)) {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					failureReason = "Interrupted";
					Log.wtf(TAG, e);
					return false;
				}
				continue;
			}
			
			try {
				exec();
				return true;
			} catch (IOException e) {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException ex) {
					failureReason = "Interrupted";
					return false;
				}
				continue;
			} catch (Throwable th) {
				returnCode = -1;
				String thMessage = th.getLocalizedMessage();
				failureReason = thMessage != null ? thMessage : "E";
				Log.e(TAG, thMessage, th);
				return false;
			}
		}
		
		
		if (context != null && !Hardwares.isOnline(context)) {
			failureReason = "Нет Internet соединения";
		}
		return false;
	}

	@Override
	protected void onPostExecute(Void result) {
		if ( after == null ) {
			return;
		}
		if (failureReason != null || returnCode != 0) {
			debugStatus(failureReason);
			after.failure(returnCode, failureReason);
		} else {
			after.success(returnValue);
		}
	}

	protected void debugStatus(String debugMessage) {
		if (dmsgConsumer == null) {
			return;
		}
		dmsgConsumer.debugStatus(debugMessage);
	}

	public void setAfter(After after) {
		this.after = after;
		if (after == null) {
			return;
		}
		dmsgConsumer = after.getDebugMessageConsumer();
	}

//	private Void safetyExec() {
//		try {
//			exec();
//		} catch (Throwable th) {
//			String thMessage = th.getLocalizedMessage();
//			failureReason = thMessage != null ? thMessage : "E";
//			Log.e(TAG, thMessage, th);
//		}
//		return null;
//	}

	/**
	 * Method executed in network tasks queue
	 * @throws Throwable
	 */
	protected abstract void exec() throws Throwable;
	/**
	 * Method executed before {@link #exec()} and in background mode (as {@link AsyncTask#execute(Object...)})
	 * @throws Throwable
	 */
	protected abstract void preExec() throws Throwable;

}

abstract class SimpleTask extends UITask {

	public SimpleTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void preExec() throws Throwable { }

}

class PingTask extends SimpleTask {

	public PingTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		returnCode = sc.ping();
	}

}

abstract class EmailTask extends SimpleTask {
	
	String email;

	public EmailTask(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

}

class CheckEmailTask extends EmailTask {

	public CheckEmailTask(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		AtomicBoolean result = new AtomicBoolean();
		returnCode = sc.checkEmail(email, result);
		returnValue = result.get();
	}

}

class EncodedPasswordHashForEmailTask extends EmailTask {

	public EncodedPasswordHashForEmailTask(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		AtomicReference<String> result = new AtomicReference<String>();
		returnCode = sc.getEncodedPasswordHashForEmail(email, result);
		returnValue = result.get();
	}

}

class RegisterNewEmailTask extends EmailTask {

	String user;
	String password;

	public RegisterNewEmailTask(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		AtomicBoolean result = new AtomicBoolean();
		returnCode = sc.register(user, email, password, result);
		returnValue = result.get();
	}

}

abstract class FreezeStateUITask extends UITask {
	protected AppStatesEnum appState;
	protected CarStatesEnum carState;
	
	public FreezeStateUITask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

}

class SignUpTask extends FreezeStateUITask {

	String userName;
	byte[] passwordHash;
	String mobileAgentName;
	public IMobileAgentState mobileAgentState;

	public SignUpTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
	@Override
	protected void onPreExecute() {
		debugStatus("Ожидаю соединения");
	}

	@Override
	protected void preExec() throws Throwable {
		//Make and packing mobile-agent state
		UITaskFactory.setupTask(context, this, appState, carState, true);
	}
	
	
	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		long imei = 0;
		if (context != null) {
			imei = Hardwares.getImei(context);
		}
		if (mobileAgentName == null) {
			mobileAgentName = "imei:" + imei;
		}
		returnCode = sc.signUp(userName, passwordHash, mobileAgentName, imei, mobileAgentState);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (failureReason != null || returnCode != 0) {
			return;
		}
		debugStatus("Ответ получен");
	}

}

class SignInTask extends SimpleTask {

	public SignInTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		returnCode = sc.signIn();
	}

}

class PubkeyTask extends SimpleTask {
	
	byte[] pubkey;

	public PubkeyTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		returnCode = sc.putPubkey(pubkey);
	}

}

abstract class ByMobileAgentIdTask extends SimpleTask {

	long mobileAgentId;
	
	public ByMobileAgentIdTask(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

}

class GetMymasTask extends ByMobileAgentIdTask {
	
	public GetMymasTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		Map<Long, String> mymas = new HashMap<Long, String>();
		returnCode = sc.getMymas(mymas);
		returnValue = mymas;
	}

}

class GetPubkeyTask extends ByMobileAgentIdTask {
	
	public GetPubkeyTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		ByteBuffer holder = ByteBuffer.allocate(4096);
		returnCode = sc.getPubkey(mobileAgentId, holder);
		returnValue = holder;
	}

}

class GetHostTask extends ByMobileAgentIdTask {
	
	String localStreamDescriptor;
	long targetMobileAgentId;	
	
	public GetHostTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		AtomicReference<String> remoteStreamDescriptorRef = new AtomicReference<String>();
		returnCode = sc.getHost(mobileAgentId, localStreamDescriptor, targetMobileAgentId, remoteStreamDescriptorRef);
		returnValue = remoteStreamDescriptorRef.get();
	}

}

class GetMasTask extends ByMobileAgentIdTask {
	
	public GetMasTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		AtomicReference<MobileAgentWithState> masRef = new AtomicReference<MobileAgentWithState>();
		returnCode = sc.getMobileAgentState(mobileAgentId, masRef);
		returnValue = masRef.get();
	}

}

class GetMapTask extends ByMobileAgentIdTask {
	
	public GetMapTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		AtomicReference<Properties> propertiesRef = new AtomicReference<Properties>();
		returnCode = sc.getMobileAgentProfile(mobileAgentId, propertiesRef);
		returnValue = propertiesRef.get();
	}

}

class SendEmapTask extends ByMobileAgentIdTask {
	
	Properties changedProperties;
	
	public SendEmapTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		AtomicBoolean result = new AtomicBoolean();
		returnCode = sc.changeMobileAgentProfile(mobileAgentId, changedProperties, result);
		returnValue = result.get();
	}

}

class SendEventTask extends FreezeStateUITask {

	EventClassesEnum eventClass;
	Date time;
	int lat = ILocation.UNKNOWN_LATITUDE;
	int lon = ILocation.UNKNOWN_LONGITUDE;
	int alt = ILocation.UNKNOWN_ALTITUDE;
	int acc = ILocation.UNKNOWN_ACCURACY;
	int mcc = ILocation.UNKNOWN_MOBILE_COUNTRY_CODE;
	int mnc = ILocation.UNKNOWN_MOBILE_NETWORK_CODE;
	int lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
	int cid = ILocation.UNKNOWN_CELL_ID;
	
	public SendEventTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void onPreExecute() {
		debugStatus("Отсылаю сообщение");
	}
	
	@Override
	protected void preExec() throws Throwable {
		//Make and packing mobile-agent state
		UITaskFactory.setupTask(context, this, appState, carState, false);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		sc.sendMessage(eventClass, time);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (failureReason != null) {
			return;
		}
		debugStatus("Сообщение отправил");
	}

}

abstract class SendEmailObjectEventTask extends UITask {
	//Email object identity
	int identity;
	//Serialized json-object
	byte[] content;

	public SendEmailObjectEventTask(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		sc.sendMessageA(EventClassesEnum.SEE, "send-email", content, new Date(), 30, 50);
	}

}

class SendEmailMessageEventTask extends SendEmailObjectEventTask {

	String from;
	String to;
	String subject;
	String body;
	int atachsCount;

	public SendEmailMessageEventTask(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void preExec() throws Throwable {
		//Make and packing email-message
		content = new EmailMessageComposer(identity, from, to, subject, body, atachsCount).convertToJson();
		//TODO: set to null from, to, subject and body
	}
	
}

class SendEmailMessageAtachEventTask extends SendEmailObjectEventTask {

	int messageIdentity;
	int atachNumber;
	String contentType;
	String atachName;
	int fragmentsCount;

	public SendEmailMessageAtachEventTask(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void preExec() throws Throwable {
		//Make and packing email-message
		content = new EmailMessageAtachComposer(identity, messageIdentity, atachNumber, contentType, atachName, fragmentsCount).convertToJson();
		//TODO: set to null from, to, subject and body
	}
	
}

class SendEmailMessageAtachFragmentEventTask extends SendEmailObjectEventTask {

	int messageAtachIdentity;
	int fragmentNumber;
	byte[] fragmentContent;	

	public SendEmailMessageAtachFragmentEventTask(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	protected void preExec() throws Throwable {
		//Make and packing email-message
		content = new EmailMessageAtachFragmentComposer(identity, messageAtachIdentity, fragmentNumber, fragmentContent).convertToJson();
		//TODO: set to null from, to, subject and body
	}
	
}

class SendEventWithAtachTask extends SendEventTask {

	String description;
	byte[] content;
	
	public SendEventWithAtachTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
	@Override
	protected void preExec() throws Throwable {
		//Make and packing mobile-agent state
		UITaskFactory.setupTask(context, this, appState, carState, content == null);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		//Dispatch
		if (eventClass == EventClassesEnum.SATE && isGPSCoords()) {
			sc.sendGpsMessageA(eventClass, description, content, lat, lon, alt, acc, time);
		} else if (eventClass == EventClassesEnum.SATE && !isGPSCoords()) {
			sc.sendGsmMessageA(eventClass, description, content, mcc, mnc, lac, cid, time);
		} else if (eventClass == EventClassesEnum.MASGSLE) {
			sc.sendGsmMessageA(eventClass, description, content, mcc, mnc, lac, cid, time);
		} else if (eventClass == EventClassesEnum.MASGPLE) {
			sc.sendGpsMessageA(eventClass, description, content, lat, lon, alt, acc, time);
		} else {
			sc.sendMessageA(eventClass, description, content, time, 3, 30);
		}
	}

	private boolean isGPSCoords() {
		return lat == ILocation.UNKNOWN_LATITUDE && lon == ILocation.UNKNOWN_LONGITUDE;
	}

}

class SendLocationCorrectionEventTask extends SendEventTask {

	public SendLocationCorrectionEventTask(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
	@Override
	protected void preExec() throws Throwable {
		//Make and packing mobile-agent state
		UITaskFactory.setupTask(context, this, appState, carState, false);
	}

	@Override
	protected void exec() throws Throwable {
		ServicesConnector sc = new ServicesConnector(context);
		sc.sendLocationCorrectionMessage(eventClass, lat, lon, alt, acc, mcc,
				mnc, lac, cid, time);
	}

}

abstract class TaskMaker {
	protected final HipdriverApplication context;
	protected final String name;
	protected final long timeoutInMillis;
	
	/**
	 * Constructor for task maker.
	 * @param context application
	 * @param name task-name
	 * @param timeoutInMillis if 0 then no timeout
	 */
	public TaskMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super();
		this.context = context;
		this.name = name;
		this.timeoutInMillis = timeoutInMillis;
	}

	public abstract UITask make();
}

class PingMaker extends TaskMaker {

	public PingMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		PingTask task = new PingTask(context, name, timeoutInMillis);
		return task;
	}

}

abstract class FreezeStateTaskMaker extends TaskMaker {
	protected AppStatesEnum appState;
	protected CarStatesEnum carState;

	public FreezeStateTaskMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
}

abstract class SendEmailObjectTaskMaker extends TaskMaker {

	protected int identity;

	public SendEmailObjectTaskMaker(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
}

class SendEmailMessageTaskMaker extends SendEmailObjectTaskMaker {
	protected String from;
	protected String to;
	protected String subject;
	protected String body;
	protected int atachsCount;

	public SendEmailMessageTaskMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		SendEmailMessageEventTask task = new SendEmailMessageEventTask(context, name, timeoutInMillis);
		task.from = from;
		task.to = to;
		task.subject = subject;
		task.body = body;
		task.identity = identity;
		task.atachsCount = atachsCount;
		return task;
	}
}

class SendEmailMessageAtachTaskMaker extends SendEmailObjectTaskMaker {
	protected int messageIdentity;
	protected int atachNumber;
	protected String contentType;
	protected String atachName;
	protected int fragmentsCount;

	public SendEmailMessageAtachTaskMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		SendEmailMessageAtachEventTask task = new SendEmailMessageAtachEventTask(context, name, timeoutInMillis);
		task.messageIdentity = messageIdentity;
		task.identity = identity;
		task.atachNumber = atachNumber;
		task.contentType = contentType;
		task.atachName = atachName;
		task.fragmentsCount = fragmentsCount;
		return task;
	}
}

class SendEmailMessageAtachFragmentTaskMaker extends SendEmailObjectTaskMaker {

	protected int messageAtachIdentity;
	protected int fragmentNumber;
	protected byte[] fragmentContent;		

	public SendEmailMessageAtachFragmentTaskMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		SendEmailMessageAtachFragmentEventTask task = new SendEmailMessageAtachFragmentEventTask(context, name, timeoutInMillis);
		task.identity = identity;
		task.messageAtachIdentity = messageAtachIdentity;
		task.fragmentNumber = fragmentNumber;
		task.fragmentContent = fragmentContent;			
		return task;
	}
}

class SignUpMaker extends FreezeStateTaskMaker {
	String userName;
	byte[] passwordHash;
	String mobileAgentName;

	public SignUpMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
	@Override
	public UITask make() {
		SignUpTask task = new SignUpTask(context, name, timeoutInMillis);
		task.userName = userName;
		task.passwordHash = passwordHash;
		task.mobileAgentName = mobileAgentName;
		task.appState = appState;
		task.carState = carState;
		return task;
	}

}

class SignInMaker extends TaskMaker {

	public SignInMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		SignInTask task = new SignInTask(context, name, timeoutInMillis);
		return task;
	}

}

class PubkeyMaker extends TaskMaker {
	
	byte[] pubkey;

	public PubkeyMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		PubkeyTask task = new PubkeyTask(context, name, timeoutInMillis);
		task.pubkey = pubkey;
		return task;
	}

}

abstract class EmailTaskMaker extends TaskMaker{

	String email;
	
	public EmailTaskMaker(HipdriverApplication context, String name,
			long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
}

class CheckEmailMaker extends EmailTaskMaker {
	
	public CheckEmailMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		CheckEmailTask task = new CheckEmailTask(context, name, timeoutInMillis);
		task.email = email;
		return task;
	}

}

class EncodedPasswordHashForEmailMaker extends EmailTaskMaker {
	
	public EncodedPasswordHashForEmailMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		EncodedPasswordHashForEmailTask task = new EncodedPasswordHashForEmailTask(context, name, timeoutInMillis);
		task.email = email;
		return task;
	}

}

class RegisterNewEmailMaker extends EmailTaskMaker {
	
	String username;
	String password;

	public RegisterNewEmailMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		RegisterNewEmailTask task = new RegisterNewEmailTask(context, name, timeoutInMillis);
		task.email = email;
		task.user = username;
		task.password = password;
		return task;
	}

}

abstract class ByMobileAgentIdMaker extends TaskMaker {
	
	long mobileAgentId;

	public ByMobileAgentIdMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

}

class GetPubkeyMaker extends ByMobileAgentIdMaker {
	
	public GetPubkeyMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		GetPubkeyTask task = new GetPubkeyTask(context, name, timeoutInMillis);
		task.mobileAgentId = mobileAgentId;
		return task;
	}

}

class GetMymasMaker extends ByMobileAgentIdMaker {
	
	public GetMymasMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		GetMymasTask task = new GetMymasTask(context, name, timeoutInMillis);
		task.mobileAgentId = mobileAgentId;
		return task;
	}

}

class GetHostMaker extends ByMobileAgentIdMaker {
	
	String localStreamDescriptor;
	long targetMobileAgentId;	
	
	public GetHostMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		GetHostTask task = new GetHostTask(context, name, timeoutInMillis);
		task.mobileAgentId = mobileAgentId;
		task.localStreamDescriptor = localStreamDescriptor;
		task.targetMobileAgentId = targetMobileAgentId;
		return task;
	}

}

class GetMasMaker extends ByMobileAgentIdMaker {
	
	public GetMasMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		GetMasTask task = new GetMasTask(context, name, timeoutInMillis);
		task.mobileAgentId = mobileAgentId;
		return task;
	}

}

class GetMapMaker extends ByMobileAgentIdMaker {
	
	public GetMapMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		GetMapTask task = new GetMapTask(context, name, timeoutInMillis);
		task.mobileAgentId = mobileAgentId;
		return task;
	}

}

class SendEmapMaker extends ByMobileAgentIdMaker {
	
	Properties changedProperties;	
	
	public SendEmapMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}

	@Override
	public UITask make() {
		SendEmapTask task = new SendEmapTask(context, name, timeoutInMillis);
		task.mobileAgentId = mobileAgentId;
		task.changedProperties = changedProperties;
		return task;
	}

}

class SendEventMaker extends FreezeStateTaskMaker {

	EventClassesEnum eventClass;
	Date time;
	String description;
	byte[] content;

	public SendEventMaker(HipdriverApplication context, String name, long timeoutInMillis) {
		super(context, name, timeoutInMillis);
	}
	
	@Override
	public UITask make() {
		final SendEventTask task;
		switch (eventClass) {
		case MASCLE:
			task = new SendLocationCorrectionEventTask(context, name, timeoutInMillis);
			break;
		case SEE:
			throw new IllegalStateException("This maker can't create send-email-task");
		case MASGSLE:
		case MASGPLE:
		case STDE:
		case UPSOFBIAOMA:
		case UPSONBIAOMA:
		case SAE:
		case SATE:
			task = new SendEventWithAtachTask(context, name, timeoutInMillis);
			SendEventWithAtachTask atask = (SendEventWithAtachTask) task;
			atask.description = description;
			atask.content = content;
			break;
		default:
			task = new SendEventTask(context, name, timeoutInMillis);
		}
		task.eventClass = eventClass;
		task.time = time;
		task.appState = appState;
		task.carState = carState;
		return task;
	}

}

public class UITaskFactory {
	
	private static final String TAG = TextTools.makeTag(UITaskFactory.class);
	
	static {
		//Pre-load classes for preventing class-not-found exceptions @see issue #114
		Log.v(TAG, SendEventWithAtachTask.class.getSimpleName() + " - loaded");
		Log.v(TAG, SendLocationCorrectionEventTask.class.getSimpleName() + " - loaded");
		Log.v(TAG, SignUpTask.class.getSimpleName() + " - loaded");
		Log.v(TAG, SendEmailMessageAtachEventTask.class.getSimpleName() + " - loaded");
		Log.v(TAG, SendEmailMessageAtachFragmentEventTask.class.getSimpleName() + " - loaded");
		Log.v(TAG, SendEmailMessageEventTask.class.getSimpleName() + " - loaded");
		Log.v(TAG, SignInTask.class.getSimpleName() + " - loaded");
		Log.v(TAG, PingTask.class.getSimpleName() + " - loaded");
	}
	
	private UITaskFactory() {
	}

	/**
	 * This method can be run only from Main Thread.
	 * @param context
	 * @param userName
	 * @param passwordHash
	 * @param mobileAgentName
	 * @param after
	 * @param timeoutInMillis
	 * @return
	 */
	public static UITask signUp(HipdriverApplication context, String userName,
			byte[] passwordHash, String mobileAgentName, After after, long timeoutInMillis) {
		SignUpMaker maker = new SignUpMaker(context, "signup", timeoutInMillis);
		maker.userName = userName;
		maker.passwordHash = passwordHash;
		maker.mobileAgentName = mobileAgentName;
		maker.appState = AppStatesEnum.ASO;
		maker.carState = CarStatesEnum.U;
		
		UITask task = maker.make();
		task.setAfter(after);
		task.execute();
		return task;
	}

	/**
	 * This method can be run only from Main Thread.
	 * @param context
	 * @param after
	 * @return
	 */
	public static UITask signIn(HipdriverApplication context, After after) {
		SignInMaker maker = new SignInMaker(context, "signin", TimeUnit.MINUTES.toMillis(10));
		UITask task = maker.make();
		task.setAfter(after);
		task.execute();
		return task;
	}

	public static void signInAsync(HipdriverApplication context, After after) {
		SignInMaker maker = new SignInMaker(context, "async-signin", TimeUnit.MINUTES.toMillis(10));
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void sendWatchOnEvent(HipdriverApplication context, After after) {
		SendEventMaker maker = new SendEventMaker(context, "send-watch-on-event", TimeUnit.MINUTES.toMillis(10));
		maker.eventClass = EventClassesEnum.UPSONBIAOMA;
		maker.appState = AppStatesEnum.CAC;
		maker.carState = CarStatesEnum.U;
		
		//setupTask(context, maker, AppStatesEnum.CAC, CarStatesEnum.U, true);

		makeAndExecuteOnMainThread(context, after, maker);		
	}
	
	public static void sendBeaconOnEvent(HipdriverApplication context, After after) {
		SendEventMaker maker = new SendEventMaker(context, "send-beacon-on-event", TimeUnit.MINUTES.toMillis(10));
		maker.eventClass = EventClassesEnum.UPSONBIAOMA;
		maker.appState = AppStatesEnum.BMA;
		maker.carState = CarStatesEnum.U;
		
		//setupMaker(context, maker, AppStatesEnum.BMA, context.getCarState(), true);

		makeAndExecuteOnMainThread(context, after, maker);		
	}
	
	public static void sendGpsEvent(HipdriverApplication context, After after) {
		SendEventMaker maker = new SendEventMaker(context, "send-gps-event", TimeUnit.MINUTES.toMillis(10));
		maker.eventClass = EventClassesEnum.MASGPLE;
		maker.appState = context.getAppState();
		maker.carState = context.getCarState();

		//setupTask(context, maker, context.getAppState(), context.getCarState(), true);
		
		makeAndExecuteOnMainThread(context, after, maker);		
		Log.d(TAG, "send- " + maker.appState.toString() + "\t" + maker.carState.toString());
	}
	
	public static void sendGsmEvent(HipdriverApplication context, After after) {
		SendEventMaker maker = new SendEventMaker(context, "send-gsm-event", TimeUnit.MINUTES.toMillis(10));
		maker.eventClass = EventClassesEnum.MASGSLE;
		maker.appState = context.getAppState();
		maker.carState = context.getCarState();

		//setupTask(context, maker, context.getAppState(), context.getCarState(), true);
		
		makeAndExecuteOnMainThread(context, after, maker);		
		Log.d(TAG, "send- " + maker.appState.toString() + "\t" + maker.carState.toString());
	}
	

	public static void sendCorrectionLocationEvent(HipdriverApplication context, After after) {
		SendEventMaker maker = new SendEventMaker(context, "send-correction-location-event", TimeUnit.MINUTES.toMillis(10));
		maker.eventClass = EventClassesEnum.MASCLE;
		maker.appState = context.getAppState();
		maker.carState = context.getCarState();
		
		//setupTask(context, maker, context.getAppState(), context.getCarState(), false);
		
		makeAndExecuteOnMainThread(context, after, maker);		
		Log.d(TAG, "send- " + maker.appState.toString() + "\t" + maker.carState.toString());
	}

	public static void sendWatchOffEvent(HipdriverApplication context, After after) {
		SendEventMaker maker = new SendEventMaker(context, "send-watch-off-event", TimeUnit.MINUTES.toMillis(10));
		maker.eventClass = EventClassesEnum.UPSOFBIAOMA;
		maker.appState = AppStatesEnum.ASO;
		maker.carState = CarStatesEnum.U;
		
		//setupTask(context, maker, AppStatesEnum.CASO, CarStatesEnum.U, true);
		
		makeAndExecuteOnMainThread(context, after, maker);		
		Log.d(TAG, "send- " + maker.appState.toString() + "\t" + maker.carState.toString());
	}

	public static void sendAlertEvent(HipdriverApplication context, After after, long timeoutInMillis) {
		SendEventMaker maker = new SendEventMaker(context, "send-alert-event", timeoutInMillis);
		maker.eventClass = EventClassesEnum.SAE;
		maker.appState = context.getAppState();
		maker.carState = context.getCarState();

		//setupTask(context, maker, context.getAppState(), context.getCarState(), true);		
		
		makeAndExecuteOnMainThread(context, after, maker);		
		Log.d(TAG, "send- " + maker.appState.toString() + "\t" + maker.carState.toString());
	}
	//TODO: move to execute stage
	protected static void setupTask(HipdriverApplication context,
			UITask task, AppStatesEnum appState, CarStatesEnum carState, boolean withStateAsAtach) {
		WatchdogService watchdogService = context.getWatchdogService();
		int gsmBitErrorRate = IMobileAgentState.UNKNOWN_GSM_BIT_ERROR_RATE;
		int gsmSignalStrength = IMobileAgentState.UNKNOWN_GSM_SIGNAL_STRENGTH;
		int lat = ILocation.UNKNOWN_LATITUDE;
		int lon = ILocation.UNKNOWN_LONGITUDE;
		int alt = ILocation.UNKNOWN_ALTITUDE;
		int acc = ILocation.UNKNOWN_ACCURACY;
		int mcc = ILocation.UNKNOWN_MOBILE_COUNTRY_CODE;
		int mnc = ILocation.UNKNOWN_MOBILE_NETWORK_CODE;
		int lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
		int cid = ILocation.UNKNOWN_CELL_ID;
		if (watchdogService != null) {
			gsmBitErrorRate = watchdogService.getGsmBitErrorRate();
			gsmSignalStrength = watchdogService.getGsmSignalStrength();
			lat = watchdogService.getLat();
			lon = watchdogService.getLon();
			alt = watchdogService.getAlt();
			acc = watchdogService.getAcc();
			mcc = watchdogService.getMcc();
			mnc = watchdogService.getMnc();
			lac = watchdogService.getLac();
			cid = watchdogService.getCid();
		} else {
			Log.w(TAG, "poor-mobile-agent-state");
		}
		//TODO: refactoring: remove instanceof
		if (task instanceof SendEventTask) {
			SendEventTask sendEventTask = (SendEventTask) task;
			sendEventTask.time = new Date();
			sendEventTask.lat = lat;
			sendEventTask.lon = lon;
			sendEventTask.alt = alt;
			sendEventTask.acc = acc;
			sendEventTask.mcc = mcc;
			sendEventTask.mnc = mnc;
			sendEventTask.lac = lac;
			sendEventTask.cid = cid;
			if (withStateAsAtach && task instanceof SendEventWithAtachTask) {
				if (appState == null || carState == null) {
					Log.w(TAG, "State generation issue, appState or carState is null");
					return;
				}
				SendEventWithAtachTask sendEventTaskWithAtach = (SendEventWithAtachTask) sendEventTask;
				sendEventTaskWithAtach.content = Hardwares.getStatus(context, appState, carState, gsmBitErrorRate, gsmSignalStrength, lat, lon, alt, acc, mcc, mnc, lac, cid);
			}
		} else if (task instanceof SignUpTask) {
			SignUpTask signupTask = (SignUpTask) task;
			
			signupTask.mobileAgentState = Hardwares.getState(context, appState, carState, gsmBitErrorRate, gsmSignalStrength, lat, lon, alt, acc, mcc, mnc, lac, cid);
		}
	}

	public static void sendAlertTrackEvent(HipdriverApplication context, List<? extends ICarLocation> track, After after) {
		SendEventMaker maker = new SendEventMaker(context, "send-alert-tracking-event", TimeUnit.MINUTES.toMillis(10));
		maker.eventClass = EventClassesEnum.SATE;
		maker.time = new Date();
		maker.content = Locations.makeTrack(track);

		makeAndExecuteOnMainThread(context, after, maker);		
		Log.d(TAG, "send- " + context.getAppState().toString() + "\t" + context.getCarState().toString());
	}

	public static void sendSensorTrackingEvent(HipdriverApplication context, String description, byte[] packedData, After after) {
		SendEventMaker maker = new SendEventMaker(context, "send-sensor-tracking-event", TimeUnit.MINUTES.toMillis(10));
		maker.eventClass = EventClassesEnum.STDE;
		maker.time = new Date();
		maker.description = description;
		maker.content = packedData;
		
		makeAndExecuteOnMainThread(context, after, maker);		
	}

	//@see http://stackoverflow.com/questions/11123621/running-code-in-main-thread-from-another-thread
	private static void makeAndExecuteOnMainThread(
			HipdriverApplication context, final After after, final TaskMaker maker) {
		Handler mainHandler = new Handler(context.getMainLooper());
		Runnable myRunnable = new Runnable() {
			@Override
			public void run() {
				UITask task = maker.make();
				task.setAfter(after);
                try {
                    task.execute();
                } catch (Throwable th) {
                    //Protect overflow queue issue #210
                    Log.wtf(TAG, th);
                    if (after != null) {
                        after.failure(-1, th.getLocalizedMessage());
                    }
                }
			}
		};
		mainHandler.post(myRunnable);
	}

	public static int sendEmail(final HipdriverApplication context, String to, String subject, String body, After after, int atachsCount) {
		SendEmailMessageTaskMaker maker = new SendEmailMessageTaskMaker(context, "send-email-event", TimeUnit.MINUTES.toMillis(10));
		maker.to = to;
		maker.subject = subject;
		maker.body = body;
		final int messageIdentity = Hardwares.makeIdentity(context);
		maker.identity = messageIdentity;
		//int atachsCount = atachSources.length;
		maker.atachsCount = atachsCount;
		makeAndExecuteOnMainThread(context, after, maker);
		Log.d(TAG, "send-email-event");
		return messageIdentity;
	}
	
	public static void sendAtachsToEmail(final HipdriverApplication context, final int messageIdentity, final int startAtachNumber, final SizedDataSource... atachSources) {
		//For sending atachs create thread.
		Thread atachsSendingThread = new Thread(new Runnable() {

			@Override
			public void run() {
				int atachNumber = startAtachNumber;
				for (SizedDataSource atachSource : atachSources) {
					try {
						sendEmailMessageAtach(context, messageIdentity,
								atachNumber, atachSource);
					} catch (IOException e) {
						Log.wtf(TAG, e);
					} catch (InterruptedException ex) {
						Log.w(TAG, "send-email-message-interrupted");
						return;
					}
					
					atachNumber++;
				}
			}
			
		}, "AtachsSendingThread");
		atachsSendingThread.start();
	}
	
	private static final int SIZE_OF_FRAGMENT = 65535;

	private static void sendEmailMessageAtach(
			final HipdriverApplication context, final int messageIdentity,
			int atachNumber, SizedDataSource atachSource) throws IOException, InterruptedException {
		final int atachIdentity = Hardwares.makeIdentity(context);
		
		
		//Read stream
		//Write every 65535 bytes to fragment
		//Send fragment
		//Wait 500 millisecond after task executed
		
		final AtomicBoolean fail = new AtomicBoolean(false);
		final AtomicBoolean complete = new AtomicBoolean(false);

		SendEmailMessageAtachTaskMaker maker = new SendEmailMessageAtachTaskMaker(context, "send-email-atach-event", TimeUnit.MINUTES.toMillis(10));
		maker.atachName = atachSource.getName();
		maker.atachNumber = atachNumber;
		maker.contentType = atachSource.getContentType();
		maker.identity = atachIdentity;
		maker.messageIdentity = messageIdentity;
		maker.fragmentsCount = atachSource.getInputStreamSize() / SIZE_OF_FRAGMENT + (atachSource.getInputStreamSize() % SIZE_OF_FRAGMENT > 0 ? 1 : 0);
		makeAndExecuteOnMainThread(context, new After() {
			
			@Override
			public void success(Object returnValue) {
				complete.set(true);
				fail.set(false);
			}

			@Override
			public void failure(int returnCode, String failureReason) {
				complete.set(true);
				fail.set(true);
			}
			
		}, maker);
		//Synchronize sequence of sending
		while (!complete.get()) {
			Thread.sleep(200);
		}

		int fragmentsCount = 0;
		InputStream atachInputStream = atachSource.getInputStream();
		try{
			////TODO: send  fragments for atach
			byte[] buffer;
			while ((buffer = StreamTools.readNextBufferFromStream(atachInputStream, SIZE_OF_FRAGMENT)) != null) {
				int fragmentIdentity = Hardwares.makeIdentity(context);
				SendEmailMessageAtachFragmentTaskMaker fragmentMaker = new SendEmailMessageAtachFragmentTaskMaker(context, "send-email-atach-fragment-event", TimeUnit.MINUTES.toMillis(10));
				fragmentMaker.messageAtachIdentity = atachIdentity;
				fragmentMaker.fragmentNumber = fragmentsCount++;
				fragmentMaker.fragmentContent = buffer;
				fragmentMaker.identity = fragmentIdentity;
				
				complete.set(false);
				fail.set(false);
				
				makeAndExecuteOnMainThread(context, new After() {
	
					@Override
					public void success(Object returnValue) {
						complete.set(true);
						fail.set(false);
					}
	
					@Override
					public void failure(int returnCode, String failureReason) {
						complete.set(true);
						fail.set(true);
					}
					
				}, fragmentMaker);
				while (!complete.get()) {
					Thread.sleep(200);
				}
				if (fail.get()) {
					return;
				}
			}
		} finally {
			atachInputStream.close();
		}
		
		//task.get(20000, TimeUnit.MILLISECONDS)
		
	}
	
	public static void sendPing(HipdriverApplication context, After after) {
		PingMaker maker = new PingMaker(context, "send-ping", TimeUnit.SECONDS.toMillis(10));
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void sendPubkey(HipdriverApplication context, After after, byte[] pubkey) {
		PubkeyMaker maker = new PubkeyMaker(context, "send-pubkey", TimeUnit.MINUTES.toMillis(30));
		maker.pubkey = pubkey;
		makeAndExecuteOnMainThread(context, after, maker);
	}
	
	public static void getMymas(HipdriverApplication context, After after) {
		GetMymasMaker maker = new GetMymasMaker(context, "get-mymas", TimeUnit.MINUTES.toMillis(10));
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void getPubkey(HipdriverApplication context, After after, long mobileAgentId) {
		GetPubkeyMaker maker = new GetPubkeyMaker(context, "get-pubkey", TimeUnit.MINUTES.toMillis(10));
		maker.mobileAgentId = mobileAgentId;
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void getHost(HipdriverApplication context, After after, long mobileAgentId, String localStreamDescriptor, long targetMobileAgentId) {
		GetHostMaker maker = new GetHostMaker(context, "get-host", TimeUnit.MINUTES.toMillis(10));
		maker.mobileAgentId = mobileAgentId;
		maker.localStreamDescriptor = localStreamDescriptor;
		maker.targetMobileAgentId = targetMobileAgentId;	
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void getMas(HipdriverApplication context, After after, long mobileAgentId) {
		GetMasMaker maker = new GetMasMaker(context, "get-mas", TimeUnit.MINUTES.toMillis(10));
		maker.mobileAgentId = mobileAgentId;
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void getMap(HipdriverApplication context, After after, long mobileAgentId) {
		GetMapMaker maker = new GetMapMaker(context, "get-map", TimeUnit.MINUTES.toMillis(10));
		maker.mobileAgentId = mobileAgentId;
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void sendEmap(HipdriverApplication context, After after, long mobileAgentId, Properties changedProperties) {
		SendEmapMaker maker = new SendEmapMaker(context, "send-emap", TimeUnit.MINUTES.toMillis(10));
		maker.mobileAgentId = mobileAgentId;
		maker.changedProperties = changedProperties;
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void checkEmail(HipdriverApplication context, After after, String email) {
		CheckEmailMaker maker = new CheckEmailMaker(context, "check-email", TimeUnit.SECONDS.toMillis(30));
		maker.email = email;
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void encodedPasswordHashForEmail(HipdriverApplication context, After after, String email) {
		EncodedPasswordHashForEmailMaker maker = new EncodedPasswordHashForEmailMaker(context, "checking-email", TimeUnit.SECONDS.toMillis(30));
		maker.email = email;
		makeAndExecuteOnMainThread(context, after, maker);
	}

	public static void registerNewEmail(HipdriverApplication context, After after,
			String username, String email, String password) {
		RegisterNewEmailMaker maker = new RegisterNewEmailMaker(context, "register-email", TimeUnit.SECONDS.toMillis(60));
		maker.email = email;
		maker.username = username;
		maker.password = password;
		makeAndExecuteOnMainThread(context, after, maker);
	}

}
