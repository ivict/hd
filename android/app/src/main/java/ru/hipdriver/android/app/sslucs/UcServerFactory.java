package ru.hipdriver.android.app.sslucs;

import static ru.hipdriver.android.app.sslucs.Invariants.KEY_STORE;
import static ru.hipdriver.android.app.sslucs.Invariants.KEY_STORE_HTL;
import static ru.hipdriver.android.app.sslucs.Invariants.LOCALHOST_ONE_DAY_CERT;
import static ru.hipdriver.android.app.sslucs.Invariants.VERIFY_PASSWORD;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import ru.hipdriver.android.app.HipdriverApplication;
import ru.hipdriver.android.app.ServicesConnector;
import ru.hipdriver.android.app.UITaskFactory;
import ru.hipdriver.android.i.ISslServer;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.android.util.UCExecutor;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IUser;
import android.util.Base64;
import android.util.Log;
//import org.bouncycastle.asn1.x509.BasicConstraints;
//import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
//import org.bouncycastle.asn1.x509.GeneralName;
//import org.bouncycastle.asn1.x509.GeneralNames;
//import org.bouncycastle.asn1.x509.KeyPurposeId;
//import org.bouncycastle.asn1.x509.KeyUsage;
//import org.bouncycastle.asn1.x509.X509Extensions;
//import org.bouncycastle.x509.X509V3CertificateGenerator;

public class UcServerFactory {

	private final static String TAG = TextTools.makeTag(UcServerFactory.class);

	public static synchronized ISslServer createServer(
			HipdriverApplication hipdriverApplication) throws Throwable {
		// Prevent NPE
		if (hipdriverApplication == null) {
			Log.w(TAG, "possible NPE");
			return null;
		}
		// Check sign-up
		if (!hipdriverApplication.isSignup()) {
			return null;
		}

		int userId = hipdriverApplication.getInt(ServicesConnector.USER_ID_KEY,
				IUser.INVALID_ID);
		long mobileAgentId = hipdriverApplication.getLong(
				ServicesConnector.MOBILE_AGENT_ID_KEY, IMobileAgent.INVALID_ID);
		// Key store resolution
		KeyStore keyStore = loadKeyStore(hipdriverApplication);
		// TODO: uncomment below code after X509 sert issue resolving
		// if (keyStore == null) {
		// Log.wtf(TAG, "key-store-is-empty");
		// return null;
		// }
		SSLContext serverContext = null;// SslContextFactory.getServerContext(keyStore,
		// VERIFY_PASSWORD);

		// TODO: get user-name and password for user account.
		ISslServer server = new SslServer(hipdriverApplication, mobileAgentId);

		Log.d(TAG, "ssl-uc-server-initialized");
		return server;
	}

	private static KeyStore loadKeyStore(
			HipdriverApplication hipdriverApplication)
			throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException, InvalidKeyException,
			NoSuchProviderException, SecurityException, SignatureException {
		String storage = hipdriverApplication.getString(KEY_STORE, "");
		if (storage.isEmpty()) {
			return null;// getNewKeyStore(hipdriverApplication);
		}
		// Check date and generate new key after
		int creationTime = hipdriverApplication.getInt(KEY_STORE_HTL, 0);
		if (hipdriverApplication.getHtl() - creationTime > 24) {
			return null;// getNewKeyStore(hipdriverApplication);
		}

		KeyStore keyStore = KeyStore.getInstance("BKS");
		byte[] keyContainer = Base64.decode(storage.getBytes(), Base64.DEFAULT);
		keyStore.load(new ByteArrayInputStream(keyContainer),
				VERIFY_PASSWORD.toCharArray());
		return keyStore;
	}

	private static KeyStore getNewKeyStore(
			HipdriverApplication hipdriverApplication)
			throws NoSuchAlgorithmException, CertificateException,
			CertificateEncodingException, KeyStoreException, IOException,
			InvalidKeyException, NoSuchProviderException, SecurityException,
			SignatureException {
		long creationTime;
		creationTime = System.currentTimeMillis();
		// Make new key pair, store in preferences and send to server.
		// @see
		// http://stackoverflow.com/questions/22154458/openssl-analogue-for-java
		KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
		gen.initialize(2048);
		KeyPair keyPair = gen.generateKeyPair();
		// Send public key to application server
		PublicKey pubkey = keyPair.getPublic();
		UITaskFactory.sendPubkey(hipdriverApplication, null,
				pubkey.getEncoded());
		// Generate x509 certificate
		X509Certificate certificate = generateCertificate(keyPair, new Date(
				creationTime),
				new Date(creationTime + TimeUnit.DAYS.toMillis(1)));
		KeyStore keyStore = KeyStore.getInstance("BKS");
		keyStore.load(null, null);
		keyStore.setCertificateEntry(LOCALHOST_ONE_DAY_CERT, certificate);
		// Update preferences and timestamp
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		keyStore.store(out, VERIFY_PASSWORD.toCharArray());
		String text = new String(Base64.encode(out.toByteArray(),
				Base64.DEFAULT));
		hipdriverApplication.setString(KEY_STORE, text);
		hipdriverApplication.setInt(KEY_STORE_HTL,
				hipdriverApplication.getHtl());
		return keyStore;
	}

	private static X509Certificate generateCertificate(KeyPair keyPair,
			Date dateNotBefore, Date dateNotAfter) throws CertificateException,
			InvalidKeyException, NoSuchProviderException, SecurityException,
			SignatureException {
		// Security.addProvider(new
		// org.bouncycastle.jce.provider.BouncyCastleProvider());
		//
		// X509V3CertificateGenerator certGen = new
		// X509V3CertificateGenerator();
		//
		// certGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));
		// certGen.setIssuerDN(new X500Principal("CN=One Day Certificate"));
		// certGen.setNotBefore(dateNotBefore);
		// certGen.setNotAfter(dateNotAfter);
		// certGen.setSubjectDN(new X500Principal("CN=UC Certificate"));
		// certGen.setPublicKey(keyPair.getPublic());
		// certGen.setSignatureAlgorithm("SHA256WithRSAEncryption");
		//
		// //certGen.addExtension(X509Extensions.BasicConstraints, true, new
		// BasicConstraints(false));
		// //certGen.addExtension(X509Extensions.KeyUsage, true, new
		// KeyUsage(KeyUsage.digitalSignature
		// // | KeyUsage.keyEncipherment));
		// //certGen.addExtension(X509Extensions.ExtendedKeyUsage, true, new
		// ExtendedKeyUsage(
		// // KeyPurposeId.id_kp_serverAuth));
		//
		// //certGen.addExtension(X509Extensions.SubjectAlternativeName, false,
		// new GeneralNames(
		// // new GeneralName(GeneralName.rfc822Name, "admin@hipdriver.ru")));
		//
		// return certGen.generateX509Certificate(keyPair.getPrivate(), "BC");
		return null;
	}
}
