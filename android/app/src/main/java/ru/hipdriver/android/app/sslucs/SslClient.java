package ru.hipdriver.android.app.sslucs;

import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.app.Hardwares;
import ru.hipdriver.android.app.HipdriverApplication;
import ru.hipdriver.android.i.ISslClient;
import ru.hipdriver.android.i.RequestCallback;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.ISid;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.i.support.UserCommandTypesEnum;
import android.util.Log;

public class SslClient extends ChatConnectorWithClientRole implements ISslClient {
	
	private static final String TAG = TextTools.makeTag(SslClient.class);
	
	public SslClient(HipdriverApplication hipdriverApplication, long mobileAgentId) throws UnknownHostException {
		super(hipdriverApplication, mobileAgentId, Invariants.STUN_SERVER2, Invariants.STUN_SERVER2_PORT);
	}

	@Override
	public boolean ping(long mobileAgentIdTo) {
		sendRequest(mobileAgentIdTo, Invariants.PING_REQUEST, null, null, 10, TimeUnit.SECONDS, false, 1, null);
		Object answer = waitForAnswer(3, TimeUnit.SECONDS);
		return answer != null;
	}

	@Override
	public void release() {
		super.release();
	}

	@Override
	public Object get(long mobileAgentIdTo, Object request, Class<?> responseClass,
			long timeout, TimeUnit unit) {
		waitForConnection(timeout, unit);
		Class<?>[] responseClasses = {responseClass};
		sendRequest(mobileAgentIdTo, request, responseClasses, null, timeout, unit, false, 1, null);
		Object answer = waitForAnswer(timeout, unit);
		return answer;
	}

	private void waitForConnection(long timeout, TimeUnit unit) {
		long timer = System.currentTimeMillis() + unit.toMillis(timeout);
		//Wait for connection state
		while (!isConnected() && timer >= System.currentTimeMillis()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Log.wtf(TAG, e);
				return;
			}
		}
	}

	@Override
	public ShortMobileAgentState execCommand(long mobileAgentIdTo, UserCommandTypesEnum commandType, byte[] args) {
		if (commandType == null) {
			throw new IllegalArgumentException();
		}
		ShortUserCommand request = new ShortUserCommand();
		request.setUserCommandTypeId(DBLinks.link(commandType));
		request.setArgs(args);
		Object response = get(mobileAgentIdTo, request, ShortMobileAgentState.class, 30, TimeUnit.SECONDS);
		if (response instanceof ShortMobileAgentState) {
			return (ShortMobileAgentState) response;
		}
		return null;
	}
	
	public HipdriverApplication getA() {
		return (HipdriverApplication) getContext();
	}

	@Override
	public void getAsync(final long mobileAgentIdTo, final Object request, final Class<?>[] responseClasses,
			final RequestCallback requestCallback, final long timeout, final TimeUnit unit, final boolean longTtlRequest, final int delayedPacketFactor, final Object key) {
		getConnectorTasksExecutorService().execute(new Runnable() {

			@Override
			public void run() {
				if (!Hardwares.isNetworkConnectedOrConnecting(getA())) {
					if (requestCallback != null) {
						requestCallback.onSendRequest(ISid.INVALID_COMPONENT_SESSIONS_ID, longTtlRequest, true, false);
					}
					return;
				};
				waitForConnection(timeout, unit);
				if (!isConnected() && requestCallback != null) {
					requestCallback.onSendRequest(ISid.INVALID_COMPONENT_SESSIONS_ID, longTtlRequest, false, true);
					return;
				}
				long requestId = sendRequest(mobileAgentIdTo, request, responseClasses, requestCallback, timeout, unit, longTtlRequest, delayedPacketFactor, key);
				if (requestCallback != null) {
					requestCallback.onSendRequest(requestId, longTtlRequest, false, false);
				}
			}
			
		});
		Thread.yield();
	}

	@Override
	public void execCommandAsync(long mobileAgentIdTo,
			UserCommandTypesEnum commandType, byte[] args, RequestCallback requestCallback, boolean longTtl, int delayedPacketFactor) {
		if (commandType == null) {
			throw new IllegalArgumentException();
		}
		ShortUserCommand request = new ShortUserCommand();
		request.setUserCommandTypeId(DBLinks.link(commandType));
		request.setArgs(args);
		Class<?>[] responseClasses = {ShortMobileAgentState.class, MobileAgentStateDescription.class, byte[].class, DeviceInfo.class};
		getAsync(mobileAgentIdTo, request, responseClasses, requestCallback, 30, TimeUnit.SECONDS, longTtl, delayedPacketFactor, commandType);
	}

	@Override
	public void restoreLongttlRequestListener(final long componentSessionsId,
			final RequestCallback requestCallback) {
		getConnectorTasksExecutorService().execute(new Runnable() {
			
			@Override
			public void run() {
				waitForConnection(30, TimeUnit.SECONDS);
				if (chatConnectionTask == null) {
					return;
				}
				Class<?>[] responseClasses = {ShortMobileAgentState.class, MobileAgentStateDescription.class};
				chatConnectionTask.restoreLongttlRequestAnswer(componentSessionsId, responseClasses, requestCallback, 30, TimeUnit.SECONDS, UserCommandTypesEnum.ACA);
			}
		});
	}

}




