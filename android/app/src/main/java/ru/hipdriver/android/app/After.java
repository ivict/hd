package ru.hipdriver.android.app;

//Token
public abstract class After {
	
	private IDebugMessageConsumer debugMessageConsumer;
	
	public After() { }
	public After(IDebugMessageConsumer debugMessageConsumer) {
		this.debugMessageConsumer = debugMessageConsumer;
	}
	
	public void success(Object returnValue) { } 
	public void failure(int returnCode, String failureReason) { }
	
	public IDebugMessageConsumer getDebugMessageConsumer() {
		return debugMessageConsumer;
	}

	public void setDebugMessageConsumer(IDebugMessageConsumer debugMessageConsumer) {
		this.debugMessageConsumer = debugMessageConsumer;
	}
	
}
