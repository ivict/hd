package ru.hipdriver.android.app;

import java.util.Date;
import java.util.Timer;

public class DebugInfoSender extends SingleRunHipdriverTimerTask {
	
	public static final String LAST_DEBUG_INFO_FILE_NAME = "di.last.json";

	public DebugInfoSender(HipdriverApplication applicationContext, Timer timer) {
		super(applicationContext, timer);
	}

	public static final long DELAY_IN_MILLIS = 60000;

	@Override
	public void runIfNotPaused() {
		WatchdogService watchdogService = getA().getWatchdogService();
		//Time back equals to DELAY_IN_MILLIS + 
		long timeBackInNanos = (DELAY_IN_MILLIS + AlertStateDetector.DEFAULT_TIMEOUT_FOR_NEXT_MEASURE) * 1000000L;
		byte[] jsonData = watchdogService.getAccelerometerTrack(System.nanoTime(), timeBackInNanos);
		String description = "Debug info sended at " + new Date();
		new FileTools(getA()).safetySaveBufferIntoFile(jsonData, LAST_DEBUG_INFO_FILE_NAME);
		int tryCountLimit = 7;
		final boolean[] canSendRef = {true};
		final boolean[] successDeliveryRef = {false};
		while(tryCountLimit-- > 0 && !successDeliveryRef[0]) {
			if (canSendRef[0]) {
				canSendRef[0] = false;
				UITaskFactory.sendSensorTrackingEvent(getA(), description, jsonData, new After() {
					
					@Override
					public void failure(int returnCode, String failureReason) {
						canSendRef[0] = true;
					}
	
					@Override
					public void success(Object returnValue) {
						successDeliveryRef[0] = true;
					}
				
				});
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				break;
			}
		}
	}

}
