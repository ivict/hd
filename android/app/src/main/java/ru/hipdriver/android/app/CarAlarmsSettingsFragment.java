package ru.hipdriver.android.app;

import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.app.BasicCarAlarmsSettingsFragment.OnEditPhoneNumber;
import ru.hipdriver.android.util.TextTools;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import ru.hipdriver.android.app.R;

public class CarAlarmsSettingsFragment extends FaceSelectCollectionHipdriverFragment {

	private static final String TAG = TextTools.makeTag(CarAlarmsSettingsFragment.class);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.car_alarms_settings_fragment,
				container, false);
		refresh(view);
		return view;
	}

	private void refresh(final View view) {
		if (view == null) {
			return;
		}
		
		final Button basicCarAlarmsSettingsButton = (Button) view.findViewById(R.id.basic_car_alarms_settings_button);
		//TODO: check invalid phone number
		if (getA().getString(MainActivity.PHONE_NUMBER_EDITOR, "").trim().length() <= 2) {
			notifyButton(basicCarAlarmsSettingsButton, R.string.basic_car_alarms_settings);
		}
		basicCarAlarmsSettingsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = getFaceSelectCollectionPagerAdapter();
				if (faceSelectCollectionPagerAdapter == null) {
					Log.w(TAG, "face-select-collection-pager-adapter-is-null");
					return;
				}
				BasicCarAlarmsSettingsFragment fragment = new BasicCarAlarmsSettingsFragment();
				fragment.setOnEditPhoneNumber(new OnEditPhoneNumber() {

					@Override
					public void onChangeText(final String text) {
						if (text == null) {
							return;
						}
						basicCarAlarmsSettingsButton.postDelayed(new Runnable() {

							@Override
							public void run() {
								//TODO: check invalid phone number
								if (text.trim().length() > 2) {
									clearNotifyForButton(basicCarAlarmsSettingsButton, R.string.basic_car_alarms_settings);
								} else {
									notifyButton(basicCarAlarmsSettingsButton, R.string.basic_car_alarms_settings);
								}
								basicCarAlarmsSettingsButton.invalidate();
							}
							
						}, TimeUnit.SECONDS.toMillis(1));
					}
					
				});
				faceSelectCollectionPagerAdapter.show(2, fragment, R.string.basic_car_alarms_settings);
			}
			
		});
		
		View sensitivityCarAlarmsSettingsButton = view.findViewById(R.id.sensitivity_car_alarms_settings_button);
		sensitivityCarAlarmsSettingsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = getFaceSelectCollectionPagerAdapter();
				if (faceSelectCollectionPagerAdapter == null) {
					Log.w(TAG, "face-select-collection-pager-adapter-is-null");
					return;
				}
				faceSelectCollectionPagerAdapter.show(2, new SensitivityCarAlarmsSettingsFragment(), R.string.sensitivity_car_alarms_settings);
			}
			
		});
		
		View notificationCarAlarmsSettingsButton = view.findViewById(R.id.notification_car_alarms_settings_button);
		notificationCarAlarmsSettingsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = getFaceSelectCollectionPagerAdapter();
				if (faceSelectCollectionPagerAdapter == null) {
					Log.w(TAG, "face-select-collection-pager-adapter-is-null");
					return;
				}
				faceSelectCollectionPagerAdapter.show(2, new NotificationsCarAlarmsSettingsFragment(), R.string.notification_car_alarms_settings);
			}
			
		});

		View powersavingsCarAlarmsSettingsButton = view.findViewById(R.id.powersavings_car_alarms_settings_button);
		powersavingsCarAlarmsSettingsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = getFaceSelectCollectionPagerAdapter();
				if (faceSelectCollectionPagerAdapter == null) {
					Log.w(TAG, "face-select-collection-pager-adapter-is-null");
					return;
				}
				faceSelectCollectionPagerAdapter.show(2, new PowersavingsCarAlarmsSettingsFragment(), R.string.powersavings_car_alarms_settings);
			}
			
		});
		
		View otherCarAlarmsSettingsButton = view.findViewById(R.id.other_car_alarms_settings_button);
		otherCarAlarmsSettingsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = getFaceSelectCollectionPagerAdapter();
				if (faceSelectCollectionPagerAdapter == null) {
					Log.w(TAG, "face-select-collection-pager-adapter-is-null");
					return;
				}
				faceSelectCollectionPagerAdapter.show(2, new OtherCarAlarmsSettingsFragment(), R.string.other_car_alarms_settings);
			}
			
		});
	}

	private void notifyButton(Button button,
			int stringResourceId) {
		CharSequence text = Html.fromHtml(getString(stringResourceId) + " (<font color='#f17602'>!</font>)");
		button.setText(text);
	}
	
	private void clearNotifyForButton(Button button,
			int stringResourceId) {
		CharSequence text = Html.fromHtml(getString(stringResourceId));
		button.setText(text);
	}

}
