package ru.hipdriver.android.app;

import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.app.util.CtxWrapper;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.AppFacesEnum;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Autostarter extends HipdriverBroadcastReceiver {
	
	private final static String TAG = Autostarter.class.getName();

	@Override
	public void onReceive(Context context, Intent intent) {
		//Check if watchdog service active
		Log.d(TAG, "run-autostarter");
		HipdriverApplication hipdriverApplication = getA(context);
		
		//Check auto-start before first signup
	    boolean firstSignup = hipdriverApplication.getBoolean(ServicesConnector.FIRST_SIGN_UP_KEY);
	    if (firstSignup) {
	    	exit(hipdriverApplication);
	    	return;
	    }
	    
		//TL Version START -->
		//if (hipdriverApplication.isChargeableInstalled()) {
	    //	exit(hipdriverApplication);
	    //	return;
		//}
		//<-- TL Version END		    
	    
	    //Check user-option "not start after reboot"
		if ( !hipdriverApplication.getBoolean(IMobileAgentProfile.RUN_AFTER_REBOOT) ) {
			exit(hipdriverApplication); 
			return;
		}
		hipdriverApplication.setBoolean(MainActivity.NOTIFICATION_NOT_DELIVERED, true);			
		//Check app-face autostart only if app-face is car-alarms
		if ( hipdriverApplication.getAppFace() != AppFacesEnum.CAR_ALARMS) {
			exit(hipdriverApplication); 
			return;
		}
		
		AppStatesEnum prevLifeAppState;
		CarStatesEnum prevLifeCarState;
		CarStatesEnum prevLifeFirstDetectionCarState;
		if (hipdriverApplication.hasCheckPoint()) {
			Log.d(TAG, "detect-uncleared-checkpoint");

			prevLifeAppState = hipdriverApplication.getStoredAppState(AppStatesEnum.CAC);
			prevLifeCarState = hipdriverApplication.getStoredCarState(CarStatesEnum.U);
			prevLifeFirstDetectionCarState = hipdriverApplication.getStoredFirstDetectionCarState(CarStatesEnum.U);
		} else {
			prevLifeAppState = AppStatesEnum.ASO;
			prevLifeCarState = CarStatesEnum.U;
			prevLifeFirstDetectionCarState = CarStatesEnum.U;
		}
		
		//From user action memory
		boolean prevLifeDebugMode = hipdriverApplication.getBoolean(HipdriverApplication.EXTRA_DEBUG_MODE);
		
		Intent intentStartMainService = new Intent(context, MainService.class);
		intentStartMainService.putExtra(HipdriverApplication.EXTRA_APPLICATION_STATE, prevLifeAppState);
		intentStartMainService.putExtra(HipdriverApplication.EXTRA_CAR_STATE, prevLifeCarState);
		intentStartMainService.putExtra(HipdriverApplication.EXTRA_FIRST_DETECTION_CAR_STATE, prevLifeFirstDetectionCarState);
		intentStartMainService.putExtra(HipdriverApplication.EXTRA_PAUSED, false);
		intentStartMainService.putExtra(HipdriverApplication.EXTRA_DEBUG_MODE, prevLifeDebugMode);
		context.startService(intentStartMainService);
	}

	protected void exit(HipdriverApplication hipdriverApplication) {
		hipdriverApplication.interrupt();
		try {
			safetySleep(2);
			hipdriverApplication.onTerminate();
			safetySleep(3);
		} finally {
			hipdriverApplication.clearInterruptFlag();
		}
	}

	protected void safetySleep(int seconds) {
		try {
			Thread.sleep(TimeUnit.SECONDS.toMillis(seconds));
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		}
	}

}
