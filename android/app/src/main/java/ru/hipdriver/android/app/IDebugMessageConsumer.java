package ru.hipdriver.android.app;

/**
 * Интерфейс для обработки отладочных сообщений.
 * @author ivict
 *
 */
public interface IDebugMessageConsumer {
	
	public void debugStatus(String debugMessage);

	public void onSwitchOnDebugMode();
	
	public void onSwitchOffDebugMode();
	
}
