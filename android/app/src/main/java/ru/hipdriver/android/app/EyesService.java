package ru.hipdriver.android.app;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.validator.routines.EmailValidator;

import ru.hipdriver.android.app.sslucs.MobileAgentStateDescription;
import ru.hipdriver.android.i.ISslServer;
import ru.hipdriver.android.util.StreamTools;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgent;

import android.app.Activity;
import ru.hipdriver.i.support.ControlModsEnum;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.FloatMath;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

//@see http://androideasylessons.blogspot.ru/2012/09/turn-off-cameraWrapper-shutter-sound.html
//TODO: ultra-light-weight-mode http://stackoverflow.com/questions/5626795/how-to-get-a-bitmap-from-a-raw-image
public class EyesService extends HipdriverService {
	private static final String TAG = TextTools.makeTag(EyesService.class);

	private static final String ACCELEROMETER_WORK_WITHOUT_SCREEN_ON = "$accelerometer-work-without-screen-on$";
    public static final String CAMERA_FOR_MOTION_DETECTION = "$camera-for-motion-detection$";

    // a variable to control the cameraWrapper
    private static ReentrantLock serviceLock = new ReentrantLock(false);
	private static Map<Integer, CameraWrapper> cameraWrappers;
    static {
        cameraWrappers = new HashMap<Integer, CameraWrapper>(Camera.getNumberOfCameras());
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            cameraWrappers.put(i, null);
        }
    }

    protected static CameraWrapper getCamera(EyesService service, int cameraId) {
        serviceLock.lock();
        try {
            CameraWrapper cameraWrapper = cameraWrappers.get(cameraId);
            if (cameraWrapper == null) {
                cameraWrapper = new CameraWrapper(service, cameraId);
                cameraWrappers.put(cameraId, cameraWrapper);
            }
            if (cameraWrapper.eyesService == service) {
                return cameraWrapper;
            }
            cameraWrapper.release();
            cameraWrapper = new CameraWrapper(service, cameraId);
            cameraWrappers.put(cameraId, cameraWrapper);
            return cameraWrapper;
        } finally {
            serviceLock.unlock();
        }
    }

    protected static CameraWrapper getWrapper(Camera camera) {
        serviceLock.lock();
        try {
            for (Map.Entry<Integer, CameraWrapper> e : cameraWrappers.entrySet()) {
                CameraWrapper cameraWrapper = e.getValue();
                if (cameraWrapper == null) {
                    continue;
                }
                if (cameraWrapper.camera == camera) {
                    return cameraWrapper;
                }
            }
            return null;
        } finally {
            serviceLock.unlock();
        }
    }

    int countOfPhotos;

	int notCapturedPhotos;

	long doNextPhotoThroughMillis;

	int messageIdentity;

	float[] ANGLES = new float[3];
	
	private long remoteControlMobileAgentId;
	private long requestId;

    private static class CameraWrapper {
        private EyesService eyesService;
        private Camera camera;
        // a surface holder
        private SurfaceHolder surfaceHolder;
        private boolean preview;
        private boolean connected;
        // the cameraWrapper parameters
        private Parameters parameters;
        private PreviewCallback previewCallback;
        private final int cameraId;
        private SurfaceView surfaceView;

        private Timer timer;


        /** Main lock guarding all access */
        final ReentrantLock lock;

//        private AVConnection connection;
//        private OnBabyAlertListener babyAlertListener;
        private PreviewCallback cachedPreviewCallback;
//        private OnBabyAlertListener lastNotNullBabyAlertListener;
        public boolean motionDetection;

        private CameraWrapper(EyesService eyesService, int cameraId) {
            this.eyesService = eyesService;
            this.cameraId = cameraId;
            this.lock = new ReentrantLock(false);
        }

        public void setPreviewDisplay(SurfaceHolder holder) {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                this.surfaceHolder = holder;
                camera.setPreviewDisplay(holder);
            } catch (Throwable th) {
                Log.wtf(TAG, th);
            } finally {
                lock.unlock();
            }
        }

        public void startPreview() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                if (preview) {
                    Log.d(TAG, "already-previewing[" + cameraId + "]");
                    return;
                }
                camera.startPreview();
                preview = true;
                Log.d(TAG, "preview-started[" + cameraId + "]");
            } finally {
                lock.unlock();
            }
        }

        public void takePicture(Camera.ShutterCallback shutter, Camera.PictureCallback raw,
                                Camera.PictureCallback postview, Camera.PictureCallback jpeg) {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                Log.d(TAG, "take-picture[" + cameraId + "]");
                camera.takePicture(shutter, raw, postview, jpeg);
            } finally {
                lock.unlock();
            }
        }

        public void open() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                if (connected) {
                    return;
                }
                camera = Camera.open(cameraId);
            } catch (Throwable th) {
                Log.wtf(TAG, th);
            } finally {
                lock.unlock();
            }
        }

        public boolean reconnect() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                camera.reconnect();
                connected = true;
                return true;
            } catch (Throwable th) {
                Log.wtf(TAG, th);
                return false;
            } finally {
                lock.unlock();
            }
        }

        public void lock() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                camera.lock();
            } finally {
                lock.unlock();
            }
        }

        public Parameters getParameters() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                return camera.getParameters();
            } finally {
                lock.unlock();
            }
        }

        public void setParameters(Parameters parameters) {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                this.parameters = parameters;
                if (!preview) {
                    camera.setParameters(parameters);
                    return;
                }
                camera.stopPreview();
                camera.setParameters(parameters);
                //Restore callback and surface holder
                if (previewCallback != null) {
                    camera.setPreviewCallback(previewCallback);
                }
                if (surfaceHolder != null) {
                    camera.setPreviewDisplay(surfaceHolder);
                }
                camera.startPreview();
            } catch (Throwable th) {
                Log.wtf(TAG, th);
            } finally {
                lock.unlock();
            }
        }

        public void release() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                if (!connected) {
                    return;
                }
                setToNullAllCalbacks();
                safetyStopPreview();
                camera.release();
                connected = false;
            } catch (Throwable th) {
                Log.wtf(TAG, th);
            } finally {
                motionDetection = false;
                lock.unlock();
            }
        }

        private void safetyStopPreview() {
            try {
                camera.stopPreview();
            } catch (Throwable th) {
                Log.wtf(TAG, th);
            }
        }

        private void setToNullAllCalbacks() {
            try {
                camera.setPreviewCallback(null);
                camera.setPreviewCallbackWithBuffer(null);
                camera.setPreviewDisplay(null);
            } catch (Throwable th) {
                Log.wtf(TAG, th);
            }
        }

        public void stopPreview() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                if (!preview) {
                    return;
                }
                camera.stopPreview();
                preview = false;
                Log.d(TAG, "preview-stopped");
            } catch (Throwable th) {
                Log.wtf(TAG, th);
            } finally {
                lock.unlock();
            }
        }

        public void setPreviewCallback(PreviewCallback previewCallback) {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                this.previewCallback = previewCallback;
                camera.setPreviewCallback(previewCallback);
            } catch (Throwable th) {
                Log.wtf(TAG, th);
            } finally {
                lock.unlock();
            }
        }

        public boolean isPreview() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                return preview;
            } finally {
                lock.unlock();
            }
        }

//        public boolean equalsByAvConnection(AVConnection connection) {
//            final ReentrantLock lock = this.lock;
//            lock.lock();
//            try {
//                return this.connection == connection;
//            } finally {
//                lock.unlock();
//            }
//        }
//
//        public boolean equalsByOnBabyAlertListener(OnBabyAlertListener onBabyAlertListener) {
//            final ReentrantLock lock = this.lock;
//            lock.lock();
//            try {
//                return this.babyAlertListener == onBabyAlertListener;
//            } finally {
//                lock.unlock();
//            }
//        }

        public PreviewCallback getCachedPreviewCallback() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                return cachedPreviewCallback;
            } finally {
                lock.unlock();
            }
        }

        public void setCachedPreviewCallback(PreviewCallback cachedPreviewCallback) {//, OnBabyAlertListener babyAlertListener, AVConnection connection) {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                this.cachedPreviewCallback = cachedPreviewCallback;
//                this.babyAlertListener = babyAlertListener;
//                this.connection = connection;
//                if (babyAlertListener != null) {
//                    this.lastNotNullBabyAlertListener = babyAlertListener;
//                }
            } finally {
                lock.unlock();
            }
        }

        public int getCameraId() {
            final ReentrantLock lock = this.lock;
            lock.lock();
            try {
                return cameraId;
            } finally {
                lock.unlock();
            }
        }

//        public OnBabyAlertListener getBabyAlertListener() {
//            final ReentrantLock lock = this.lock;
//            lock.lock();
//            try {
//                return babyAlertListener;
//            } finally {
//                lock.unlock();
//            }
//        }
//
//        public AVConnection getConnection() {
//            final ReentrantLock lock = this.lock;
//            lock.lock();
//            try {
//                return connection;
//            } finally {
//                lock.unlock();
//            }
//        }

        public Timer getTimer() {
            return timer;
        }

        public void setTimer(Timer timer) {
            this.timer = timer;
        }

        public Timer resetTimer() {
            if (timer != null) {
                timer.cancel();
            }
            timer = new Timer();
            return timer;
        }

        public void stopTimer() {
            if ( timer == null) {
                return;
            }
            timer.cancel();
            timer = null;
        }
    }

    //
    //============================================================================

    /** Called when the activity is first created. */

	public class EyesBinder extends Binder {
		EyesService getService() {
			return EyesService.this;
		}

	}

	// Binder given to clients
	private final IBinder eyesBinder = new EyesBinder();

	private int volume;
	
	private AtomicReference<Runnable> stopMotionDetectionTaskRef = new AtomicReference<Runnable>();

	@Override
	public IBinder onBind(Intent arg0) {
		return eyesBinder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		getA().setEyesService(this);
	}

    private boolean isStopped(int cameraId) {
        CameraWrapper cameraWrapper = cameraWrappers.get(cameraId);
        if (cameraWrapper == null) {
            return true;
        }
        return cameraWrapper.getTimer() == null;
    }

	public void start(int countOfPhotos, int intervalBetweenPhotosInSeconds, final int cameraId) {
        CameraWrapper cameraWrapper = getCamera(this, cameraId);
        Timer timer = cameraWrapper.resetTimer();

		this.countOfPhotos = countOfPhotos;
		notCapturedPhotos = countOfPhotos;
		doNextPhotoThroughMillis = TimeUnit.SECONDS
				.toMillis(intervalBetweenPhotosInSeconds);

		final Handler mainHandler = new Handler(getMainLooper());
		Runnable capturePhoto = new Runnable() {
			@Override
			public void run() {
				if (!safetyTakePicture(new EmailSendingCallback(cameraId), cameraId)) {
					return;
				}
				// Send body of email
				if (EyesService.this.countOfPhotos == notCapturedPhotos) {
					final String email = getA().getString(
							IMobileAgent.ALERT_EMAIL_KEY, "");
					String mobileAgentName = getA().getString(
							MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
					String subj = getA().getResources().getString(R.string.email_message_subj_photo, mobileAgentName);
					String body = getA().getResources().getString(R.string.email_message_body_photo);
					messageIdentity = UITaskFactory.sendEmail(getA(), email,
							subj,
							body, null,
							EyesService.this.countOfPhotos);
				}

				notCapturedPhotos--;
				// NEW PHOTO CONDITION
				if (notCapturedPhotos > 0) {
					mainHandler.postDelayed(this, doNextPhotoThroughMillis);
				}
			}
		};
		mainHandler.post(capturePhoto);

		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				// Try to run and stop accelerometer if not running
				runAndStopAccelerometerIfNotRunning(getA().getWatchdogService());
			}

		}, 0);

		long timeout = countOfPhotos * TimeUnit.MINUTES.toMillis(1)
				+ doNextPhotoThroughMillis * (countOfPhotos - 1);
		// Stop service after timeout
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				stop(cameraId);
			}

		}, TimeUnit.MINUTES.toMillis(timeout));
	}

	// @see
	// https://github.com/commonsguy/cw-advandroid/blob/master/Camera/Picture/src/com/commonsware/android/picture/PictureDemo.java

	private boolean takePicture(final Camera.PictureCallback pictureCallback, final int cameraId) {
		final SurfaceView surfaceView = prepareCam(cameraId);
		if (surfaceView == null) {
			Log.wtf(TAG, "surface-view-is-null");
			return false;
		}
        Log.d(TAG, "cam-prepared[" + cameraId + "]");
		
		Runnable takePhotoTask = new Runnable() {

			@Override
			public void run() {
				try {
                    final CameraWrapper cameraWrapper = cameraWrappers.get(cameraId);
					final SurfaceHolder holder = surfaceView.getHolder();
					final Callback callback = new Callback() {

						@Override
						public void surfaceCreated(SurfaceHolder holder) {
							holder.removeCallback(this);//Remove old callback
							try {
								cameraWrapper.setPreviewDisplay(holder);
								cameraWrapper.startPreview();

								// @see
								// http://stackoverflow.com/questions/14476791/android-mute-cameraWrapper-shutter-sound
								AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
								audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
								cameraWrapper.takePicture(null, null, null, pictureCallback);
			
								final Handler handler = new Handler();
								handler.postDelayed(new Runnable() {
			
									@Override
									public void run() {
										AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
										audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
									}
									
								}, TimeUnit.SECONDS.toMillis(5));
							} catch (Throwable e) {
								Log.wtf(TAG, e);
								//Send message to remote-control
								if (getA().getControlMode() != ControlModsEnum.INTERNET) {
									return;
								}
								if (Camera.getNumberOfCameras() > 0) {
									sendResponse(getRemoteControlMobileAgentId(), getRequestId(), R.string.can_not_make_photo1);
								} else {
									sendResponse(getRemoteControlMobileAgentId(), getRequestId(), R.string.can_not_make_photo2);
								}
							}						
							
						}

						@Override
						public void surfaceChanged(SurfaceHolder holder,
								int format, int width, int height) {
							holder.removeCallback(this);//Remove old callback
						}

						@Override
						public void surfaceDestroyed(SurfaceHolder holder) {
							holder.removeCallback(this);//Remove old callback
						}
						
					};
					holder.addCallback(callback);
					if (getA().getMainActivity() == null) {//If activity not set try to make-photo in not visual mode (worked not for all Android devices).
                        Log.d(TAG, "surface-created-direct-call[cam: " + cameraId + "]");
						callback.surfaceCreated(holder);
					} else {
                        surfaceView.post(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "refresh-surface-view[cam: " + cameraId + "]");
                                surfaceView.setVisibility(View.VISIBLE);
                                surfaceView.invalidate();
                                callback.surfaceCreated(holder);
                            }
                        });
                    }
				} catch (Throwable e) {
					Log.wtf(TAG, e);
					//Send message to remote-control
					if (getA().getControlMode() != ControlModsEnum.INTERNET) {
						return;
					}
					if (Camera.getNumberOfCameras() > 0) {
						sendResponse(getRemoteControlMobileAgentId(), getRequestId(), R.string.can_not_make_photo1);
					} else {
						sendResponse(getRemoteControlMobileAgentId(), getRequestId(), R.string.can_not_make_photo2);
					}
				} finally {
                    Log.d(TAG, "take-photo-task[" + cameraId + "]");
                }
            }
			
		};
		if (getA().getMainActivity() != null) {
			getA().getMainActivity().runOnUiThread(takePhotoTask);
		} else {
			takePhotoTask.run();
		}
		return true;
	}
	
	protected boolean safetyTakePicture(Camera.PictureCallback pictureCallback, int cameraId) {
		try {
			return takePicture(pictureCallback, cameraId);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return false;
		}
	}

    protected SurfaceView prepareCam(int cameraId) {
        CameraWrapper cameraWrapper = getCamera(this, cameraId);
        if (cameraWrapper != null) {
            cameraWrapper.release();
        }
        cameraWrapper.open();
        if (!cameraWrapper.reconnect()) {
            return null;
        }
        //cameraWrapper.lock();

        final SurfaceView surfaceView = getSurfaceView(cameraId);
        if (surfaceView == null) {
            return null;
        }
        Parameters parameters = cameraWrapper.getParameters();
        // List<Camera.Size> previewSizes =
        // parameters.getSupportedPreviewSizes();
        //parameters.getSupportedPreviewFormats();
        // parameters.setPreviewFormat(ImageFormat.RGB_565);
        // set cameraWrapper parameters
        //parameters.setPreviewFormat(ImageFormat.JPEG);
        cameraWrapper.setParameters(parameters);
        return surfaceView;
    }

    public void stop() {
        for (Map.Entry<Integer, CameraWrapper> e : cameraWrappers.entrySet()) {
            int cameraId = e.getKey();
            if (isStopped(cameraId)) {
                return;
            }
            CameraWrapper cameraWrapper = e.getValue();//getCamera(this, cameraId);
            cameraWrapper.stopTimer();
            cameraWrapper.release();
        }
    }

    protected void stop(int cameraId) {
        if (isStopped(cameraId)) {
            return;
        }
        CameraWrapper cameraWrapper = getCamera(this, cameraId);
        cameraWrapper.stopTimer();
        cameraWrapper.release();
    }

    // Camera.PictureCallback bitmapPictureCallbak = new
	// Camera.PictureCallback() {
	//
	// public void onPictureTaken(byte[] data, Camera cameraWrapper) {
	// Bitmap imageBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
	// final String email = getA().getString(IMobileAgent.ALERT_EMAIL_KEY, "");
	// String mobileAgentName =
	// getA().getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
	// UITaskFactory.sendEmail(getA(), email, "Картинка [" + mobileAgentName +
	// "]", "Фото с места событий!", null, makeImageSource(imageBitmap,
	// "photo1.png", 64, 64));
	// audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
	// }
	// };

	abstract class JpegPictureCallback implements Camera.PictureCallback {

        protected final int cameraId;

        public JpegPictureCallback(int cameraId) {
            this.cameraId = cameraId;
        }

        public void onPictureTaken(byte[] data, Camera camera) {
            final CameraWrapper cameraWrapper = getCamera(EyesService.this, cameraId);
            final SurfaceView surfaceView = cameraWrapper.surfaceView;
			Runnable setInvisibleTask = new Runnable() {

				@Override
				public void run() {
					if (surfaceView == null) {
						return;
					}
					surfaceView.setVisibility(View.INVISIBLE);
					if (cameraWrapper != null) {
                        cameraWrapper.release();
                    }
				}
				
			};
			if (getA().getMainActivity() != null) {
				getA().getMainActivity().runOnUiThread(setInvisibleTask);
			} else {
				setInvisibleTask.run();
			}
			
			final byte[] jpegImageData = data;
			// final float horizontalViewAngle =
			// cameraWrapper.getParameters().getHorizontalViewAngle();
			getA().runPhoneTask(new Runnable() {

				@Override
				public void run() {
					Bitmap bitmap = BitmapFactory.decodeByteArray(
							jpegImageData, 0, jpegImageData.length);
					Matrix mat = new Matrix();
					float rotationAngle = getRotationAngle(cameraId);
					// Log.d(TAG, "horizontalViewAngle " + horizontalViewAngle);
					mat.postRotate(rotationAngle);
					Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
							bitmap.getWidth(), bitmap.getHeight(), mat, true);
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					rotatedBitmap
							.compress(Bitmap.CompressFormat.JPEG, 63, baos);
					byte[] rotatedImageData = baos.toByteArray();

					processImageData(rotatedImageData);
				}

                private float getRotationAngle(int cameraId) {
                    if (cameraId == CameraInfo.CAMERA_FACING_FRONT) {
                        return getRotationAngleFront();
                    }
                    return getRotationAngleBack();
                }

                private float getRotationAngleFront() {
                    float[] angles = Arrays.copyOf(ANGLES, 3);
                    // Log.d(TAG, "angleX " + angles[0]);
                    // Log.d(TAG, "angleY " + angles[1]);
                    float angleX = angles[0];
                    float angleY = angles[1];
                    int factorX = getFactor(angleX);
                    int factorY = getFactor(angleY);

                    // Phone in horizontal plane
                    if (factorX == 0 && factorY == 0) {
                        return 2 * 90f;
                    }
                    if (factorX == 0 && factorY == 1) {
                        return 3 * 90f;
                    }
                    if (factorX == 0 && factorY == -1) {
                        return 90f;
                    }
                    if (factorX == -1 && factorY == 0) {
                        return 0f;
                    }
                    return 0f;
                }

                private float getRotationAngleBack() {
                    float[] angles = Arrays.copyOf(ANGLES, 3);
                    // Log.d(TAG, "angleX " + angles[0]);
                    // Log.d(TAG, "angleY " + angles[1]);
                    float angleX = angles[0];
                    float angleY = angles[1];
                    int factorX = getFactor(angleX);
                    int factorY = getFactor(angleY);

                    // Phone in horizontal plane
                    if (factorX == 0 && factorY == 0) {
                        return 0f;
                    }
                    if (factorX == 0 && factorY == 1) {
                        return 90f;
                    }
                    if (factorX == 0 && factorY == -1) {
                        return 3 * 90f;
                    }
                    if (factorX == -1 && factorY == 0) {
                        return 2 * 90f;
                    }
                    return 0f;
                }

                private int getFactor(float angle) {
					if (angle < 45f) {
						return 1;
					}
					if (angle < (90f + 45f)) {
						return 0;
					}
					if (angle < (180f)) {
						return -1;
					}
					// throw new IllegalStateException("Unknown case");
					Log.w(TAG, "Unknown case " + angle
							+ " more then 180 degree");
					return 0;
				}

			});
		}

		protected abstract void processImageData(byte[] rotatedImageData);
	}

	class PreviewImageSendingCallback extends JpegPictureCallback {

		private final long remoteControlMobileAgentId;
		private final long requestId;

		public PreviewImageSendingCallback(long remoteControlMobileAgentId,
				long requestId, int cameraId) {
            super(cameraId);
			this.requestId = requestId;
			this.remoteControlMobileAgentId = remoteControlMobileAgentId;
		}

		protected void processImageData(final byte[] rotatedImageData) {
			try {
				// FileTools.writeIntoFile("/mnt/sdcard/tmp/image-out.jpeg",
				// rotatedImageData);
				getA().waitForUcServerMaking(TimeUnit.SECONDS.toMillis(10));
				ISslServer sslServer = getA().getUcServer();
				if (sslServer == null) {
					Log.w(TAG, "can-not-send-preview[ssl-server is null]");
					return;
				}

				// ByteBuffer imageData = ByteBuffer.allocate(65535);
				// TODO: send through different server then command chat server

				int chunkSize = Invariants.DATA_CHUNK_SIZE;
				byte[] chunk = new byte[chunkSize];
				int chunksCount = rotatedImageData.length / chunkSize;
				int finalChunkSize = rotatedImageData.length % chunkSize;
				for (int i = 0; i < chunksCount; i++) {
					System.arraycopy(rotatedImageData, i * chunkSize, chunk, 0,
							chunkSize);
					sslServer.sendResponse(remoteControlMobileAgentId,
							requestId, chunk);
				}
				if (finalChunkSize > 0) {
					byte[] finalChunk = new byte[finalChunkSize];
					System.arraycopy(rotatedImageData, chunksCount * chunkSize,
							finalChunk, 0, finalChunkSize);
					sslServer.sendResponse(remoteControlMobileAgentId,
							requestId, finalChunk);
				}
				//
				//
				//
				// int chunkSize = Invariants.DATA_CHUNK_SIZE;
				// byte[] chunk = new byte[chunkSize];
				// byte[] finalChunk = new byte[rotatedImageData.length %
				// chunkSize];
				// for (int i = 0, srcPos = 0; i < rotatedImageData.length /
				// chunkSize; i++) {
				// System.arraycopy(rotatedImageData, srcPos, chunk, 0,
				// chunk.length);
				// sslServer.sendResponse(remoteControlMobileAgentId, requestId,
				// chunk);
				// imageData.put(transport(chunk));
				// srcPos += chunk.length;
				// }
				// //Fill final chunk if his length more then zero
				// if (finalChunk.length > 0) {
				// System.arraycopy(rotatedImageData, rotatedImageData.length -
				// finalChunk.length - 1, finalChunk, 0, finalChunk.length);
				// sslServer.sendResponse(remoteControlMobileAgentId, requestId,
				// finalChunk);
				// imageData.put(transport(finalChunk));
				// }
				// Send EOF
				sslServer.sendResponse(remoteControlMobileAgentId, requestId,
						new byte[0]);

				// byte[] data = new byte[imageData.position()];
				// imageData.position(0);
				// imageData.get(data, 0, data.length);
				// //
				// FileTools.writeIntoFile("/mnt/sdcard/tmp/image-in.jpeg",
				// data);
				// //
			} finally {
				final String email = getA().getString(
						IMobileAgent.ALERT_EMAIL_KEY, "");
				boolean badEmail = !EmailValidator.getInstance().isValid(email);
				if (getA().getControlMode() == ControlModsEnum.INTERNET) {
					// Send answer take photo success
					if (badEmail) {// || "")
						sendResponse(remoteControlMobileAgentId, requestId, R.string.bad_email);
					} else {
						sendResponse(remoteControlMobileAgentId, requestId, R.string.photo_sended);
					}
				}
				
				//Send photo to e-mail
				if (badEmail) {
					return;
				}
				getA().runPhoneTask(new Runnable() {
					
					@Override
					public void run() {
						String mobileAgentName = getA().getString(
								MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
						String subj = getA().getResources().getString(R.string.email_message_subj_photo, mobileAgentName);
						String body = getA().getResources().getString(R.string.email_message_body_photo);
						messageIdentity = UITaskFactory.sendEmail(getA(), email,
								subj,
								body, null,
								1);
						UITaskFactory.sendAtachsToEmail(
								getA(),
								messageIdentity,
								0,
								StreamTools.makeDataSource(rotatedImageData, "photo1.jpg", "image/jpeg"));
					}
				});


			}

		}

		// ObjectMapper om = new ObjectMapper();
		// private byte[] transport(byte[] chunk) {
		// try {
		// //String packet = om.writeValueAsString(chunk);
		// //byte[] packetData = om.readValue(packet, byte[].class);
		// byte[] packetData = new byte[chunk.length];
		// System.arraycopy(chunk, 0, packetData, 0, chunk.length);
		// return packetData;
		// } catch (Throwable th) {
		// Log.wtf(TAG, th);
		// throw new RuntimeException(th);
		// }
		// }

	}

	class EmailSendingCallback extends JpegPictureCallback {

        public EmailSendingCallback(int cameraId) {
            super(cameraId);
        }

        protected void processImageData(byte[] rotatedImageData) {
			int imageNumber = countOfPhotos - notCapturedPhotos;
			UITaskFactory.sendAtachsToEmail(
					getA(),
					messageIdentity,
					imageNumber - 1,
					StreamTools.makeDataSource(rotatedImageData, "photo"
							+ imageNumber + ".jpg", "image/jpeg"));
		}

	}

	private void runAndStopAccelerometerIfNotRunning(
			final WatchdogService watchdogService) {
		if (!watchdogService.isStopped()) {
			System.arraycopy(watchdogService.accelerometerData.ANGLES, 0, ANGLES, 0, 3);
			return;
		}
		
		final SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		final List<Sensor> sensors = sensorManager
				.getSensorList(Sensor.TYPE_ACCELEROMETER);

		if (sensors.isEmpty()) {
			Log.d(TAG, "No accelerometer sensors");
			return;
		}

		if (sensors.size() > 1) {
			Log.w(TAG, "Many accelerometer sensors");
		}
		Sensor sensor = sensors.get(0);
		
		final AtomicBoolean noValues = new AtomicBoolean(true);
		
		final SensorEventListener listener = new SensorEventListener() {
			
			@Override
			public void onSensorChanged(SensorEvent event) {
				float[] m = event.values;
				float g = FloatMath.sqrt(m[0] * m[0] + m[1] * m[1] + m[2] * m[2]);
				//Угол скалярного произведения вектора гравитации на единичный вектор оси x
				ANGLES[0] = (float) Math.acos(m[0] / g) * 180f / (float) Math.PI;
				ANGLES[1] = (float) Math.acos(m[1] / g) * 180f / (float) Math.PI;
				ANGLES[2] = (float) Math.acos(m[2] / g) * 180f / (float) Math.PI;
				noValues.set(false);
			}

			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {
				// TODO Auto-generated method stub
				
			}
		};
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		WakeLock userActivityWakeLock = pm.newWakeLock(
				PowerManager.SCREEN_DIM_WAKE_LOCK
				| PowerManager.ACQUIRE_CAUSES_WAKEUP,
		"MakingPhotoActivity");
		
		sensorManager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_FASTEST);
		

		try {
			userActivityWakeLock.acquire();
			long timerValue = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(3);
			while(noValues.get() && timerValue > System.currentTimeMillis()) {
				Thread.sleep(100L);
			}
		} catch (InterruptedException ex) {
			Log.wtf(TAG, ex);
		} finally {
			userActivityWakeLock.release();
			sensorManager.unregisterListener(listener);
		}
		if (noValues.get()) {
			Log.w(TAG, "silent-on-measure-photo-orientation");
		}
	}

	public void preview(final int cameraId) {
        CameraWrapper cameraWrapper = getCamera(this, cameraId);
        Timer timer = cameraWrapper.resetTimer();

		final Handler mainHandler = new Handler(getMainLooper());
		Runnable capturePreview = new Runnable() {
			@Override
			public void run() {
                Log.d(TAG, "capture-preview");
				if (!safetyTakePicture(new PreviewImageSendingCallback(
						getRemoteControlMobileAgentId(), getRequestId(), cameraId), cameraId)) {
                    Log.d(TAG, "capture-preview-failed");
					return;
				}
			}
		};
		mainHandler.post(capturePreview);
		
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				// Try to run and stop accelerometer if not running
				runAndStopAccelerometerIfNotRunning(getA().getWatchdogService());
			}

		}, 0);
	}

	public void startMotionDetection(final int cameraId) {//, final OnBabyAlertListener onBabyAlertListener, final AVConnection connection) {//TODO: remove parameter activity
        final CameraWrapper cameraWrapper = getCamera(this, cameraId);
        cameraWrapper.resetTimer();

        cameraWrapper.motionDetection = true;
		final Handler mainHandler = new Handler(getMainLooper());
		Runnable startMotionDetection = new Runnable() {
			@Override
			public void run() {
                System.gc();
                final SurfaceView surfaceView = prepareCam(cameraId);
                if (surfaceView == null) {
                    Log.wtf(TAG, "surface-view-is-null");
                    return;
                }

                final SurfaceHolder holder = surfaceView.getHolder();
                final Callback callback = new Callback() {

                    @Override
                    public void surfaceCreated(SurfaceHolder holder) {
                        try {
                            motionDetectionAndSendVideoStream(cameraWrapper);//, connection, onBabyAlertListener);
                        } catch (Throwable e) {
                            Log.wtf(TAG, e);
                        }
                    }

                    @Override
                    public void surfaceChanged(SurfaceHolder holder,
                                               int format, int width, int height) {
                        if (cameraWrapper != null) {
                            cameraWrapper.stopPreview();
                        }
                        this.surfaceCreated(holder);
                    }

                    @Override
                    public void surfaceDestroyed(SurfaceHolder holder) {
                        holder.removeCallback(this);//Remove old callback
                    }

                };
                holder.addCallback(callback);
                surfaceView.setVisibility(View.VISIBLE);
                surfaceView.invalidate();
                callback.surfaceCreated(cameraWrapper.surfaceHolder);

                // @see
                // http://stackoverflow.com/questions/14476791/android-mute-cameraWrapper-shutter-sound
                AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
                //cameraWrapper.takePicture(null, null, null, pictureCallback);

                //TODO: stop motion detection if task not null
                stopMotionDetectionTaskRef.set(new Runnable() {

                    @Override
                    public void run() {
                        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
                        stop(cameraId);
                    }

                });
			}
		};
		mainHandler.post(startMotionDetection);
	}

    private synchronized void motionDetectionAndSendVideoStream(CameraWrapper cameraWrapper) {//, final AVConnection connection, final OnBabyAlertListener onBabyAlertListener) {
        if (cameraWrapper == null) {
            Log.w(TAG, "can-not-motion-detection-and-send-video-stream[camera-wrapper is null]");
            return;
        }
        if (cameraWrapper.isPreview()) {
            cameraWrapper.stopPreview();
        }
        PreviewCallback previewCallback = getPreviewCallback(cameraWrapper);//, connection, onBabyAlertListener);
        cameraWrapper.setPreviewCallback(previewCallback);

        cameraWrapper.surfaceView.setVisibility(View.VISIBLE);
        cameraWrapper.setPreviewDisplay(cameraWrapper.surfaceHolder);
        cameraWrapper.startPreview();
    }

    private PreviewCallback getPreviewCallback(CameraWrapper cameraWrapper) {//, final AVConnection connection, final OnBabyAlertListener onBabyAlertListener) {
        if (cameraWrapper.getCachedPreviewCallback() != null) {//cameraWrapper.equalsByAvConnection(connection) && cameraWrapper.equalsByOnBabyAlertListener(onBabyAlertListener)) {
            return cameraWrapper.getCachedPreviewCallback();
        }
        final int previewFormat = cameraWrapper.getParameters().getPreviewFormat();
        final Size previewSize = cameraWrapper.getParameters().getPreviewSize();
        final int height = previewSize.height / 4;
        final int width = previewSize.width / 4;
        final int size = width * height;
        //Motion container
        final byte[] pE = new byte[size];
        //Frames container
        final byte[] p0 = new byte[size];
        final byte[] p1 = new byte[size];
        final byte[] p2 = new byte[size];
        final byte[] p3 = new byte[size];
        final byte[] p4 = new byte[size];
        final long timer0 = System.currentTimeMillis();
        final AtomicInteger prevSum = new AtomicInteger(0);
//					final Random random = new Random();
//					final byte[][] whiteNoise = new byte[previewSize.width][previewSize.height];
//					for (int i = 0; i < previewSize.width; i++) {
//						random.nextBytes(whiteNoise[i]);
//					}
//					final int[] lines = new int[previewSize.height];
        cameraWrapper.setCachedPreviewCallback(new PreviewCallback() {

            @Override
            public void onPreviewFrame(final byte[] data, Camera camera) {
                if (previewFormat != ImageFormat.NV21) {
                    return;
                }
                final long timer = System.currentTimeMillis();

                //Store 2 second difference
                System.arraycopy(p3, 0, p4, 0, size);
                System.arraycopy(p2, 0, p3, 0, size);


                //Move prev to history
                System.arraycopy(p1, 0, p2, 0, size);
                //Move actual to prev
                System.arraycopy(p0, 0, p1, 0, size);

                int sum = 0;

                int pA0;
                int pB0;
                int pC0;
                int pD0;
                int pEE;
                int prevPEE = 0;
                int j = 0;
                for (int h = 0; h < previewSize.height; h += 4) {
                    for (int w = 0; w < previewSize.width; w += 4) {
                        int i = h * previewSize.width + w;
                        byte a = data[i];
                        //Update actual
                        p0[j] = a;

                        ////Diff view
                        //p = p0[j] & 0xFF;
                        //pixelsA[j] = 0xff000000 | p<<16 | p<<8 | p;

                        pA0 = Math.abs(p0[j] & 0xFF - p1[j] & 0xFF);
                        pB0 = Math.abs(p0[j] & 0xFF - p2[j] & 0xFF);
                        pC0 = Math.abs(p0[j] & 0xFF - p3[j] & 0xFF);
                        pD0 = Math.abs(p0[j] & 0xFF - p4[j] & 0xFF);
                        if (pA0 < 25) {
                            pEE = 0;
                        } else if (pB0 < 35) {
                            pEE = 0;
                        } else if (pC0 < 45) {
                            pEE = 0;
                        } else if (pD0 < 55) {
                            pEE = 0;
                        } else {
                            pEE = 100;
                        }
                        if (prevPEE == pEE) {
                            //pE = (pA == pB && pB == pC && pC == pD) ? 0 : 100;//Math.min(pB, pA);
                            //pixelsB[j] = 0xff000000 | pE<<16 | pE<<8 | pE;
                            pE[j] = (byte) pEE;

                            sum += pEE;
                        }

                        prevPEE = pEE;

                        j++;
                    }
                }

                //Send to remote device
//                if (connection != null) {
//                    connection.appendDataAndSendWithDebugInfo(data, pE, previewSize);
//                }

                //Log.d(TAG, String.format("[%s] on-preview-frame[%sx%s][%sms] sum[%s]", TextTools.timeSince(timer0), width, height, System.currentTimeMillis() - timer, sum));
                //Log.d(TAG, String.format("line1[%s],line2[%s]", lines[1], uniformLines[1]));
                //Bitmap bm = Bitmap.createBitmap(pixels, width, height, Bitmap.Config.ARGB_8888);
                //saveIntoFile("/HWUserData/tmp", "test.png", bm);

                //Detect motion
                if ((System.currentTimeMillis() - timer0) < TimeUnit.SECONDS.toMillis(11)) {
                    prevSum.set(sum);
                    return;
                }
                //Detect motion
                int DELTA = Math.abs(sum - prevSum.get());
                float k = DELTA / (float) prevSum.get();
//                if (prevSum.get() > 0 &&
//                        (
//                                (k > 1f && prevSum.get() > 10000)
//                                        || DELTA > 50000
//                        ) && onBabyAlertListener != null) {
//                    Log.d(TAG, String.format("sum %s, prev-sum %s", sum, prevSum.get()));
//                    onBabyAlertListener.onAlert(BabyBlockEventsEnum.MOTION, k);
//                }

                prevSum.set(sum);
            }

            private boolean checkInLattice(int offset, int width) {
                int i = offset % width;//w
                int j = offset / width;//h
                return i % 4 == 0 && j % 4 == 0;
            }
        });//, onBabyAlertListener, connection);
        return cameraWrapper.getCachedPreviewCallback();
    }

    public void stopMotionDetection() {
		Runnable runnable = stopMotionDetectionTaskRef.getAndSet(null);
        if (runnable == null) {
            return;
        }
		final Handler mainHandler = new Handler(getMainLooper());
		mainHandler.post(runnable);
	}
	
	private void saveIntoFile(String folder, String fileName, Bitmap image) {
		try {
			OutputStream fOut = null;
			File file = new File(folder, fileName);
			file.createNewFile();
			fOut = new FileOutputStream(file);
	
			// 100 means no compression, the lower you go, the stronger the compression
			image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
			fOut.flush();
			fOut.close();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	protected void sendResponse(long remoteControlMobileAgentId, long requestId, int resourceId) {
		ISslServer sslServer = getA().getUcServer();
		if (sslServer == null) {
			return;
		}
		MobileAgentStateDescription mobileAgentStateDescription = new MobileAgentStateDescription();
		mobileAgentStateDescription
				.setDescription(getResources().getString(
						resourceId));
		sslServer.sendResponse(remoteControlMobileAgentId,
				requestId, mobileAgentStateDescription);
	}

	public long getRemoteControlMobileAgentId() {
		return remoteControlMobileAgentId;
	}

	public void setRemoteControlMobileAgentId(long remoteControlMobileAgentId) {
		this.remoteControlMobileAgentId = remoteControlMobileAgentId;
	}

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	@Override
	protected void finalize() throws Throwable {
        safetyReleaseAllCams();
		super.finalize();
	}

	public static void safetyReleaseAllCams() {
        for (Map.Entry<Integer, CameraWrapper> e : cameraWrappers.entrySet()) {
            CameraWrapper cameraWrapper = e.getValue();
            if (cameraWrapper == null) {
                continue;
            }
            cameraWrapper.release();
        }
	}

	@Override
	public void onDestroy() {
        safetyReleaseAllCams();
		super.onDestroy();
	}

	protected SurfaceView getSurfaceView(int cameraId) {
        serviceLock.lock();
        try {
            CameraWrapper cameraWrapper = getCamera(this, cameraId);
            if (cameraWrapper.surfaceView != null) {
                return cameraWrapper.surfaceView;
            }
            Activity mainActivity = getA().getMainActivity();
            if (mainActivity == null) {
                cameraWrapper.surfaceView = new SurfaceView(getApplicationContext());
            } else if (cameraId == CameraInfo.CAMERA_FACING_BACK) {
                cameraWrapper.surfaceView = (SurfaceView) mainActivity.findViewById(R.id.main_screen_$back_cam$_surface_view);
            } else  {
                cameraWrapper.surfaceView = (SurfaceView) mainActivity.findViewById(R.id.main_screen_$front_cam$_surface_view);
            }
            // Get a surface
            cameraWrapper.surfaceHolder = cameraWrapper.surfaceView.getHolder();
            // tells Android that this surface will have its data
            // constantly
            // replaced
            cameraWrapper.surfaceHolder
                    .setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            //surfaceHolder.setSizeFromLayout();
            //surfaceHolder.addCallback(this);
            return cameraWrapper.surfaceView;
        } finally {
            serviceLock.unlock();
        }
    }

	// private SizedDataSource makeImageSource(final Bitmap imageBitmap, final
	// String name, final int maxHeight, final int maxWidth) {
	// return new SizedDataSource() {
	//
	// @Override
	// public String getContentType() {
	// return "image/png";
	// }
	//
	// @Override
	// public InputStream getInputStream() throws IOException {
	// Float width = new Float(imageBitmap.getWidth());
	// Float height = new Float(imageBitmap.getHeight());
	// Float ratio = width/height;
	// Bitmap scaledBitmap = Bitmap.createScaledBitmap(imageBitmap,
	// (int)(maxHeight*ratio), maxHeight, false);
	// //int padding = (maxWidth - scaledBitmap.getWidth())/2;
	//
	// //imageView.setPadding(padding, 0, padding, 0);
	// //imageView.setImageBitmap(imageBitmap);
	//
	// ByteArrayOutputStream baos = new ByteArrayOutputStream();
	// scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
	// byte[] byteArray = baos.toByteArray();
	// return new ByteArrayInputStream(byteArray);
	// }
	//
	// @Override
	// public String getName() {
	// return name;
	// }
	//
	// @Override
	// public OutputStream getOutputStream() throws IOException {
	// throw new IllegalStateException("Input only datasource!");
	// }
	//
	// @Override
	// public int getInputStreamSize() {
	// // TODO Auto-generated method stub
	// return 0;
	// }
	//
	// };
	// }

    public Parameters getParameters(int cameraId) {
        CameraWrapper cameraWrapper = getCamera(this, cameraId);
        return cameraWrapper.getParameters();
    }

    public void applyNewParameters(int cameraId, Parameters parameters) {
        CameraWrapper cameraWrapper = getCamera(this, cameraId);
        try {
            cameraWrapper.setParameters(parameters);
        } catch (Throwable th) {
            Log.wtf(TAG, th);
        }
        //TODO: release cameraWrapper and restart preview
    }

    public void switchCamera() {
        try {
            switchToNextCamera();
        } catch (Throwable th) {
            Log.wtf(TAG, th);
        }
    }

    public int getMDCameraId() {
        for (Map.Entry<Integer, CameraWrapper> e : cameraWrappers.entrySet()) {
            CameraWrapper cameraWrapper = e.getValue();
            if (cameraWrapper.motionDetection) {
                return cameraWrapper.cameraId;
            }
        }
        return -1;
    }

    public void restartMotionDetection(int cameraId) {//, OnBabyAlertListener onBabyAlertListener) {
        CameraWrapper cameraWrapper = getCamera(this, cameraId);
        startMotionDetection(cameraId);//, onBabyAlertListener, cameraWrapper.getConnection());
    }

    protected int getCameraIdForMotionDetection() {
        return getA().getInt(EyesService.CAMERA_FOR_MOTION_DETECTION, Camera.CameraInfo.CAMERA_FACING_BACK);
    }

    public void switchToNextCamera() throws IOException {
        if (cameraWrappers.size() < 2 ) {
            //Nothing do...
            return;
        }
        //TODO: stop camera and switch to next camera
        int mdId = getCameraIdForMotionDetection();
        int cameraId = mdId + 1;
        if (cameraId > cameraWrappers.size() - 1) {
            cameraId = CameraInfo.CAMERA_FACING_BACK;
        }
        CameraWrapper cameraWrapperOld = getCamera(this, mdId);
        cameraWrapperOld.release();
        startMotionDetection(cameraId);//, cameraWrapperOld.lastNotNullBabyAlertListener, cameraWrapperOld.connection);
    }



}
