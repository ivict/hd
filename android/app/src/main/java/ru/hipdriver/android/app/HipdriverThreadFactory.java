package ru.hipdriver.android.app;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class HipdriverThreadFactory implements ThreadFactory {
	
	private final String poolName;
	private AtomicInteger threadIndex;
	
	private static final int SINGLE_THREAD = -1;
	
	public HipdriverThreadFactory(String poolName, boolean isSingleThreadModel) {
		this.poolName = poolName;
		if (isSingleThreadModel) {
			this.threadIndex = new AtomicInteger(SINGLE_THREAD);
		} else {
			this.threadIndex = new AtomicInteger(0);
		}
	}

	@Override
	public Thread newThread(Runnable r) {
		if (threadIndex.get() == SINGLE_THREAD) {
			return new Thread(r, poolName);
		}
		return new Thread(r, poolName + "-" + threadIndex.getAndIncrement());
	}
	
	public static HipdriverThreadFactory newInstance(String poolName) {
		return new HipdriverThreadFactory(poolName, false);
	}

	public static HipdriverThreadFactory newSingleThreadInstance(String threadName) {
		return new HipdriverThreadFactory(threadName, true);
	}

}
