package ru.hipdriver.android.app;

import android.app.Activity;
import android.content.Intent;

public class HipdriverRuntimeExceptionHandler extends RuntimeExceptionHandler {

	public HipdriverRuntimeExceptionHandler(HipdriverApplication context,
			Class<? extends Activity> activityClass) {
		super(context, activityClass);
	}

	@Override
	protected void updateIntent(Intent intent) {
		super.updateIntent(intent);
		intent.putExtra(HipdriverApplication.EXTRA_APPLICATION_STATE, getA().getAppState());
		intent.putExtra(HipdriverApplication.EXTRA_CAR_STATE, getA().getCarState());
		intent.putExtra(HipdriverApplication.EXTRA_FIRST_DETECTION_CAR_STATE, getA().getFirstDetectionCarState());
		intent.putExtra(HipdriverApplication.EXTRA_PAUSED, getA().isPaused());
		intent.putExtra(HipdriverApplication.EXTRA_DEBUG_MODE, getA().isDebugMode());
	}
	
	public HipdriverApplication getA() {
		return (HipdriverApplication) context;
	}

}
