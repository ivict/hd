package ru.hipdriver.android.app;

import static ru.hipdriver.android.app.Invariants.TAG_GEO;
import static ru.hipdriver.android.app.NumericTools.sq;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.android.app.sslucs.MobileAgentStateListener;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.android.util.UCExecutor;
import ru.hipdriver.d.Sensors;
import ru.hipdriver.d.Sensors.MinMaxSqValue;
import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.i.IUserLimit;
import ru.hipdriver.i.support.AppFacesEnum;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.j.EventWithAtach;
import ru.hipdriver.j.ext.SensorData;
import ru.hipdriver.j.support.SensorDataPusher;
import ru.hipdriver.android.app.R;
import ru.hipdriver.util.NumTools;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.Camera.CameraInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.FloatMath;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.internal.telephony.ITelephony;

public class WatchdogService extends HipdriverService {

	public static final String TAG = TextTools.makeTag(WatchdogService.class);

	public static final String WATCHDOG_SERVICE_CHECK_POINT = "watchdog-service-check-point";
	public static final String WATCHDOG_SERVICE_SAVE_INTO_FOLDER_KEY = "save_into_folder_editor_text";
	public static final String DISABLE_SCREEN_ON_SENSOR_SILENT = "disable_screen_on_sensor_silent";
	public static final String RINGER_MODE = "ringer_mode";
	public static final String PREV_GPS_ENABLED = "prev_gps_enabled";
	
	public static final int TIME_TRESHOLD_FOR_SPLIT_STATE_CORRECTORS_IN_MINUTES = 2;

	public class WhatchdogBinder extends Binder {
		WatchdogService getService() {
			return WatchdogService.this;
		}

	}
	
	abstract class WatchdogLocationListener implements LocationListener, GpsStatus.Listener {
		int lat = ILocation.UNKNOWN_LATITUDE;
		int lon = ILocation.UNKNOWN_LONGITUDE;
		int acc = ILocation.UNKNOWN_ACCURACY;
		int alt = ILocation.UNKNOWN_ALTITUDE;
		float vel = 0f;
		long time;
		int gpsStatus;

		@Override
		public void onLocationChanged(Location location) {
			lat = Locations.integerLatitude(location.getLatitude());
			lon = Locations.integerLongitude(location.getLongitude());
			//Accuracy in centimeters
			if (location.hasAccuracy()) {
				acc = (int) Math.round(location.getAccuracy() * 100f);
			}
			//Altitude in centimeters
			if (location.hasAltitude()) {
				alt = (int) Math.round(location.getAltitude() * 100f);
			}
			//Velocity in meter per second
			if (location.hasSpeed()) {
				vel = location.getSpeed();
			} else {
				vel = 0f;
			}
			//UTC time in milliseconds
			time = location.getTime();
			updateLocation(lat, lon, alt, acc, vel, time);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			System.out.println();
			// TODO Auto-generated method stub
		}

		@Override
		public void onProviderEnabled(String provider) {
			System.out.println();
			// TODO Auto-generated method stub
		}

		@Override
		public void onProviderDisabled(String provider) {
			System.out.println();
			// TODO Auto-generated method stub
		}

		@Override
		public void onGpsStatusChanged(int event) {
			gpsStatus = event;
		}
		
	}

	class SingleUpdateWatchdogLocationListener extends WatchdogLocationListener {
		
		private int prevAcc = 30000;

		@Override
		public void onLocationChanged(Location location) {
			super.onLocationChanged(location);
			//Synchronization with alert track listener if application in car alarms mode
			if (getA().getAppState() == AppStatesEnum.CAC && getA().getCarState() != CarStatesEnum.U) {
				stopGPSUpdateSingle();
				return;
			}
			//Check accuracy
			if (location.getAccuracy() > 300.0) {
				return;
			}
			if (criticalData != null && criticalData.initialPosition != null && acc > criticalData.initialPosition.acc) {
				return;
			}
			//Accuracy no more then 50 meters
			if (acc < 5000 && criticalData.cancelSingleUpdateLocationListenerThread == null) {
				//Wait one minute for more accuracy
				criticalData.cancelSingleUpdateLocationListenerThread = new Thread("CancelSingleUpdateLocationListenerThread") {
					@Override
					public void run() {
						try {
							try {
								Thread.sleep(TimeUnit.SECONDS.toMillis(30));
							} catch (InterruptedException e) {
								return;
							}
							if (Thread.interrupted()) {
								return;
							}
							stopGPSUpdateSingle();
						} finally {
							criticalData.cancelSingleUpdateLocationListenerThread = null;
						}
					}
				};
				criticalData.cancelSingleUpdateLocationListenerThread.start();
			}
			short carStateId = DBLinks.link(getA().getCarState());
			criticalData.initialPosition = Locations.makeLocation(lat, lon, alt, acc, mcc, mnc, lac, cid, time, carStateId, vel);
			if (prevAcc > acc) {
				UITaskFactory.sendCorrectionLocationEvent(getA(), null);
				fireChangeMobileAgentState();
			}
			prevAcc = acc;
			long delayInMillis = System.currentTimeMillis() - timerSingleUpdateLocationStart;
			Log.d(TAG_GEO, String.format("stop-loc = [%s,%s,%s,%s][%s]", lat, lon, acc, vel, delayInMillis));
		}

	}

	class AlertTrackLocationListener extends WatchdogLocationListener {

		@Override
		public void onLocationChanged(Location location) {
			super.onLocationChanged(location);
			if (criticalData.getLocationTask == null) {
				Log.d(TAG_GEO, "NPE if access getLocationTask");
				return;
			}
			criticalData.getLocationTask.pushLocation(lat, lon, alt, acc, mcc, mnc, lac, cid, vel, System.currentTimeMillis());
			//resetLocation();
		}

	}

	class AccSensorListener extends SensorDataPusher implements SensorEventListener {
		public AccSensorListener(SensorData sensorData,
				SensorData calibrationData) {
			super(sensorData, calibrationData);
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			//Not use event.timestamp Megafon Phone issue
			pushValues(System.nanoTime(), event.values);
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			Log.w(TAG, "Accuracy of sensor changed");
		}

		public void getGravity(float[] result) {
			System.arraycopy(gravity, 0, result, 0, 3);
		}

		public void setSensorTid(int sensorTid) {
			super.sensorTid = sensorTid; 
		}

		public int getSensorTid() {
			return sensorTid;
		}

		@Override
		protected int myTid() {
			return android.os.Process.myTid();
		}

		@Override
		protected float sqrt(float value) {
			return FloatMath.sqrt(value);
		}
		
	}

	static class GetLocation extends HipdriverTimerTask {
		private final int maxCacheSize;
		private final List<ru.hipdriver.j.Location> track;
		private final BlockingQueue<ru.hipdriver.j.Location> lastTenLocations;
		private ru.hipdriver.j.Location firstLocation;
		private long minTimeInMillisForStopDetection;
		private boolean firstPoint = true;
		
		private int prevLat = ILocation.UNKNOWN_LATITUDE;
		private int prevLon = ILocation.UNKNOWN_LONGITUDE;
		private long prevTime;
		private float velocity;
		
		private int accelerometerDataSize;
		
		private Writer writer;
		
		private final WatchdogService ws;
		private long fireMobileAgentStateTime;
		
		public GetLocation(WatchdogService ws, int maxCacheSize, HipdriverApplication applicationContext, Timer timer, long minTimeInMillisForStopDetection) {
			super(applicationContext, timer);
			this.maxCacheSize = maxCacheSize;
			this.track = new ArrayList<ru.hipdriver.j.Location>(maxCacheSize);
			this.minTimeInMillisForStopDetection = minTimeInMillisForStopDetection;
			this.lastTenLocations = new ArrayBlockingQueue<ru.hipdriver.j.Location>(10);
			this.ws = ws;
		}

		@Override
		public void runIfNotPaused() {
			//Only GSM coords update with regularity
			try {
				ws.updateCellLocation();
			} catch (Throwable th) {
				Log.wtf(TAG, th);
			}
			//Update GSM locations only if GPS not available
			if (!ws.isGpsAvailable) {
				ws.criticalData.getLocationTask.pushLocation(ws.lat, ws.lon, ws.alt, ws.acc, ws.mcc, ws.mnc, ws.lac, ws.cid, ws.vel, System.currentTimeMillis());
				//resetLocation();
			}
		}

		protected void pushLocation(int lat, int lon, int alt, int acc, int mcc,
				int mnc, int lac, int cid, float vel, long currentTimeMillis) {
			logInDebugMode("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", currentTimeMillis, lat, lon, alt, acc, mcc, mnc, lac, cid, vel);
			if ((lat == ILocation.UNKNOWN_LATITUDE || lon == ILocation.UNKNOWN_LONGITUDE) &&
					(lac == ILocation.UNKNOWN_LOCATION_AREA_CODE || cid == ILocation.UNKNOWN_CELL_ID)) {
				logInDebugMode(",zero-lat-or-lon\n");
				return;
			}
			//Supress rough coords for GPS
			if (lat != ILocation.UNKNOWN_LATITUDE && lon != ILocation.UNKNOWN_LONGITUDE && acc > 30000f) {
				logInDebugMode(",poor-acc\n");
			    return;
			}

			short carStateId = DBLinks.link(getA().getCarState());			
			ru.hipdriver.j.Location currentLocation = Locations.makeLocation(lat, lon, alt, acc, mcc, mnc, lac, cid, currentTimeMillis, carStateId, vel);
            try {
                lastTenLocations.add(currentLocation);
            } catch (Throwable th) {
                Log.wtf(TAG, th);
                //Queue is full
                return;
            }
			//First point store
			if (firstLocation == null) {
				firstLocation = currentLocation;
			}
			
			float maxAbsAccelerationValue = 0f;
			//Suppress push if car stop
			if (ws.accelerometerData != null) {
				long nanoTime = System.nanoTime();
				long minTimeInNs = minTimeInMillisForStopDetection * 1000000L;
				MinMaxSqValue minMaxAccelerationValue = Sensors.minMaxSqabsValueWithAveragingByNPointsLPF(ws.accelerometerData,
						1, nanoTime, minTimeInNs, 2, 0.001f);
				if ( minMaxAccelerationValue != null ) {
					maxAbsAccelerationValue = FloatMath.sqrt(minMaxAccelerationValue.maxValue);
				}
				boolean isSilent = accelerometerDataSize == ws.accelerometerData.countOfValues;
				accelerometerDataSize = ws.accelerometerData.countOfValues;
				logInDebugMode(",%s", String.valueOf(minMaxAccelerationValue));
				//Car stop detection
				if (!firstPoint && !isSilent && minMaxAccelerationValue != null && maxAbsAccelerationValue < 5f) {
					logInDebugMode(",too-more-stops\n");
					return;
				}
			} else {
				logInDebugMode(",");
			}
			
		    long time = System.currentTimeMillis();
		    float distanceInMeters = 0;
			//Calculate velocity
			if (lat != ILocation.UNKNOWN_LATITUDE && lon != ILocation.UNKNOWN_LONGITUDE) {
			    distanceInMeters = prevLat != ILocation.UNKNOWN_LATITUDE ? Locations.getDistance(prevLat, prevLon, lat, lon) : 0;
			    float timeInSeconds = (time - prevTime) / 1000f;
			    ///Log.d(TAG, String.format("d[%s]m,t[%s]s", distanceInMeters, timeInSeconds));
			    velocity = timeInSeconds > 0 ? distanceInMeters / timeInSeconds : 0;
			}
			//Suppress high velocity more then 150 kilometer per hour in same direction
			if (!firstPoint && velocity > 42f) {
				logInDebugMode(",high-velocity[%s] d[%s]\n", velocity, distanceInMeters);
			    //return;
			}
			//Update normal position
			//Suppress high restriction for points not included in current track
		    prevLat = lat;
		    prevLon = lon;
		    prevTime = time;
		    
		    if (firstPoint) {
				long delayInMillis = System.currentTimeMillis() - ws.timerAlertTrackUpdateLocationStart;
				Log.d(TAG_GEO, String.format("loc = [%s,%s,%s,%s][%s]", lat, lon, acc, vel, delayInMillis));
		    }

			firstPoint = false;
			
			logInDebugMode("\n=> velocity[%s], acc[%s], lat[%s], lon[%s]\n", velocity, acc, lat, lon);
			
			if (fireMobileAgentStateTime + TimeUnit.SECONDS.toMillis(5) < System.currentTimeMillis()) {
				ws.fireChangeMobileAgentState();
				fireMobileAgentStateTime = System.currentTimeMillis();
			}
			
			if (track.isEmpty()) {
				track.add(currentLocation);
				return;
			}
			ru.hipdriver.j.Location lastLocation = (ru.hipdriver.j.Location) track.get(track.size() - 1);
			if (equalsByFields(lastLocation, currentLocation) ) {
				return;
			}
			//Prevent overflow
			if (track.size() >= maxCacheSize) {
				return;
			}
			track.add(currentLocation);
		}

		protected void logInDebugMode(String format, Object... args) {
			if (getA().isDebugMode()) {
				writer = getWriter();
				if (writer == null) {
					return;
				}
				try {
					final String logRecord;
					if (args.length == 0) {
						logRecord = format;
					} else {
						logRecord = String.format(format, args);
					}
					writer.append(logRecord);
				} catch (IOException e) { }
			}
		}

		private Writer getWriter() {
			if (writer != null) {
				return writer;
			}
			final String prefKeyName = WATCHDOG_SERVICE_SAVE_INTO_FOLDER_KEY;
			String folderName = getA().getString(prefKeyName, "");
			File folder = new File(folderName);
			if (!folder.exists()) {
				return null;
			}
			if(!folder.isDirectory()) {
				return null;
			}
			try {
				File log = new File(folder, "geo.gps.log");
				writer = new BufferedWriter(new FileWriter(log, false));
				writer.append("time,lat,lon,alt,acc,mcc,mnc,lac,cid,vel,a,filtered-reason\n");
				return writer;
			} catch (IOException e) {
				Log.wtf(TAG, e);
				return null;
			}
		}

		private boolean equalsByFields(ru.hipdriver.j.Location loc1,
				ru.hipdriver.j.Location loc2) {
			return loc1.lat == loc2.lat && loc1.lon == loc2.lon && loc1.alt == loc2.alt &&
					loc1.acc == loc2.acc && loc1.mcc == loc2.mcc && loc1.mnc == loc2.mnc &&
					loc1.lac == loc2.lac &&	loc1.cid == loc2.cid;
		}
		
		public void stop() {
			if (writer != null) {
				try {
					writer.flush();
					writer.close();
				} catch (IOException e) {
					Log.wtf(TAG, e);
				}
				writer = null;
			}
		}
		
	};
	
//	//@see http://stackoverflow.com/questions/9766002/making-a-list-of-outgoing-calls-one-by-one-automatically
//	private class PhoneCallListener extends PhoneStateListener {
//
//		private boolean isPhoneCalling = false;
//
//		@Override
//		public void onCallStateChanged(int state, String incomingNumber) {
//
//			if (TelephonyManager.CALL_STATE_RINGING == state) {
//				// phone ringing
//				Log.d(TAG, "RINGING, number: " + incomingNumber);
//			}
//
//			if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
//				// active
//				Log.d(TAG, "OFFHOOK");
//
//				isPhoneCalling = true;
//			}
//
//			if (TelephonyManager.CALL_STATE_IDLE == state) {
//				// run when class initial and phone call ended, need detect flag
//				// from CALL_STATE_OFFHOOK
//				Log.d(TAG, "IDLE");
//				//
//				// if (isPhoneCalling) {
//				//
//				// Log.i(TAG, "CALL...");
//				//
//				// // restart app
//				// Intent i = getBaseContext().getPackageManager()
//				// .getLaunchIntentForPackage(
//				// getBaseContext().getPackageName());
//				// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				// // startActivity(i);
//				// call(num);
//				// isPhoneCalling = false;
//				// }
//
//			}
//		}
//
//		public boolean isPhoneCalling() {
//			return isPhoneCalling;
//		}
//
//		public void setPhoneCalling(boolean isPhoneCalling) {
//			this.isPhoneCalling = isPhoneCalling;
//		}
//		
//	}
	
	static class WatchdogServiceCritical {
		ru.hipdriver.j.Location initialPosition;
		Timer timer;
		Timer alertTimer;
		Timer calibrationTimer;
		GetLocation getLocationTask;
		//Last alert time (System.nanoTime())
		long lastAlertTimeInNs = System.nanoTime();//TODO: init from stored value
		Date alertTime = new Date();//TODO: init from stored value
		PowerManager.WakeLock userActivityWakeLock;
		int screenBrightnessLevel = -1;
		GenerateUserActivityThread generateUserActivityThread;
		AtomicBoolean generateUserActivityThreadStopped = new AtomicBoolean(true);
		Timer countdownTimer;

//		//It's a user variable state before start
//		int ringerMode;
//		
//		//It's a user variable state before start
//		boolean prevGpsEnabled;

		HandlerThread alertTrackLocationListenerThread;
		HandlerThread singleUpdateLocationListenerThread;
		Thread cancelSingleUpdateLocationListenerThread;
		AlertStateDetector alertStateDetector;
	}	
	
//	class UpdateLocationListenerThread extends HandlerThread {
//
//		private boolean stopped; 
//		
//		@Override
//		public void run() {
//			Thread.currentThread().setName("UpdateLocationListenerHandler");
//			Looper.prepare();
//			preparing();
//			stopped = false;
//			Looper.loop();
//		}
//		
//		public void preparing() {
//		}
//		
//		public void quit() {
//			Looper myLooper = Looper.myLooper();
//			if (myLooper == null) {
//				return;
//			}
//			if (myLooper == Looper.getMainLooper()) {
//				return;
//			}
//			if (stopped) {
//				return;
//			}
//			myLooper.quit();
//			stopped = true;
//		}
//	}
	
	private static WatchdogServiceCritical criticalData = new WatchdogServiceCritical();
	
 	private ExecutorService watchdogTasksExecutorService = Executors.newSingleThreadExecutor(HipdriverThreadFactory.newSingleThreadInstance("watchdog-tasks-executor"));
	
	private boolean mIsBound = false;
	private BeepService beepService;
	private ServiceConnection beepConnection =new ServiceConnection(){

		public void onServiceConnected(ComponentName name, IBinder binder) {
			beepService = ((BeepService.ServiceBinder) binder).getService();
		}

		public void onServiceDisconnected(ComponentName name) {
			beepService = null;
		}

	};

	void doBindBeepService(){
			bindService(new Intent(this,BeepService.class),
				beepConnection,Context.BIND_AUTO_CREATE);
		mIsBound = true;
	}

	void doUnbindBeepService()
	{
		if(mIsBound)
		{
			unbindService(beepConnection);
	     		mIsBound = false;
		}
	}

	// Binder given to clients
	private final IBinder whatchdogBinder = new WhatchdogBinder();

	Sensor sensor;

	// flag for GPS availability
	boolean isGpsAvailable;

	// flag for network status
	boolean isNetworkEnabled;

	// flag for roaming state
	boolean isNetworkRoaming;

	//Latitude * 1000000
	int lat = ILocation.UNKNOWN_LATITUDE;
	//Longitude * 1000000
	int lon = ILocation.UNKNOWN_LONGITUDE;
	//Accuracy in centimeters
	int acc = ILocation.UNKNOWN_ACCURACY;
	//Altitude in centimeters
	int alt = ILocation.UNKNOWN_ALTITUDE;
	float vel;
	//Time in milliseconds
	long time;
	int mcc = ILocation.UNKNOWN_MOBILE_COUNTRY_CODE;
	int mnc = ILocation.UNKNOWN_MOBILE_NETWORK_CODE;
	int lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
	int cid = ILocation.UNKNOWN_CELL_ID;

	int gsmSignalStrength = IMobileAgentState.UNKNOWN_GSM_SIGNAL_STRENGTH;
	int gsmBitErrorRate = IMobileAgentState.UNKNOWN_GSM_BIT_ERROR_RATE;

	// // The minimum distance to change Updates in meters
	// private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10
	// meters
	//
	// // The minimum time between updates in milliseconds
	// private static final long MIN_TIME_BW_UPDATES = 100 * 60 * 1; // 1 minute

	protected LocationManager locationManager;
	protected TelephonyManager telephonyManager;
	protected SensorManager sensorManager;

	boolean notCalibrated;// Calibration gravity direction
	boolean notRated;// Measure rate defining

	// Tracking data container
	// Max reaction for Human is 40 ms, then take 20 ms interval
	// Max measure time 15 мин.
	// 15 * 50 * 60 = 45000, byte array size 45000 * 20 = 879 KB
	SensorData accelerometerData = Sensors.makeForNMeasures(45000, true);
	// Max calibration rate 1ms, calibration timeout 3s
	SensorData calibrationData = Sensors.makeForNMeasures(3000);

	AccSensorListener sensorEventListener = new AccSensorListener(accelerometerData, calibrationData);

	protected MinMaxSqValue minMaxJerkValue;

	//private PhoneCallListener phoneCallListener;
	
	private float calibrationTime;
	private float measureRateInSeconds;
	private float sensorResolution;

	private long timerSingleUpdateLocationStart;
	private long timerAlertTrackUpdateLocationStart;

	private long ttlForWatchInMillis;

	private int delayStartInSeconds;
	
	WatchdogLocationListener singleUpdateLocationListener = new SingleUpdateWatchdogLocationListener();
	WatchdogLocationListener alertTrackLocationListener = new AlertTrackLocationListener(){};
	
	private Future<?> alertCallTask;

	private Future<?> restartTask;
	
	Thread setupSoundModeThread;
	
	private boolean gpsOn;
	
	@Override
	public IBinder onBind(Intent intent) {
		return whatchdogBinder;
	}

	public void start(long delayInMillis) {
		Log.d(TAG, "starting-watchdog-service");
		if (getA().getCarState() == CarStatesEnum.A) {
			return;
		}
		delayStartInSeconds = (int) TimeUnit.MILLISECONDS.toSeconds(delayInMillis);
		resetLocation();
		safetySetWakeLock();

		if (getA().isDebugMode()) {
			String logName = "ws-restart-stat.log";
			getA().makeDebugInfoHeaderIfNotHeadered(logName, "time,delay-in-millis\n");
			getA().writeDebugInfo(logName, "%s,%s\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()),delayInMillis);
		}
		
		try {
			startMeasure();
			
			startUpdateLocation();
			
			setupSoundModeThread = new Thread(new Runnable() {

				@Override
				public void run() {
					AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
					int ringerMode = getA().getInt(RINGER_MODE, audioManager.getRingerMode()) ;
					if (ringerMode == AudioManager.RINGER_MODE_NORMAL && beepService != null) {
						beepService.beepCac();
					}
					try {
						Thread.sleep(1000);
						// Vibrate for 500 milliseconds, prevent sleeping accelerometer on some models
						vibroSlow();
					} catch (InterruptedException e) {
						return;
					}
					audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
				}
				
			});
			setupSoundModeThread.start();
			Thread.yield();
			
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	private Timer getTimer() {
		if (criticalData.timer != null) {
			return criticalData.timer;
		}
		criticalData.timer = new Timer("GeneralWatchdogServiceTimer");
		return criticalData.timer;
	}

	protected void startUpdateLocation() {
		getTimer();
		
		boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (!isGpsEnabled && isGpsAvailable) {
			turnGPSOn();
		}
			
		isNetworkEnabled = locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		registrationSingleUpdateLocationListener(10, TimeUnit.MINUTES);

		if (locationManager == null) {
			initPhone();
		}
		updateLocation();
	}
	
	private void safetySetWakeLock() {
		try {
			getA().setWakeLock();
		} catch (Throwable th) {
			Log.wtf(TAG,  th);
		}
	}

	private void safetyReleaseWakeLock() {
		try {
			getA().releaseWakeLock();
		} catch (Throwable th) {
			Log.wtf(TAG,  th);
		}
	}

	public void startSwitchOffTimer(long delayInMillis) {
		if (criticalData.timer == null) {
			return;
		}
		ttlForWatchInMillis = delayInMillis;
		final long stopTimer = System.currentTimeMillis() + delayInMillis; 
		//Switch off task
		criticalData.timer.schedule(new TimerTask() {
			@Override
			public void run() {
				switchOffWithStatus(R.string.switched_off_by_timer);				
			}
		}, delayInMillis);
		//Update indicator task
		long minute = TimeUnit.MINUTES.toMillis(1);
		criticalData.timer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (getA().getAppState() == AppStatesEnum.ASO) {
					cancel();
					return;
				}
				long restInMinutes = TimeUnit.MILLISECONDS.toMinutes(stopTimer - System.currentTimeMillis());
				if (restInMinutes < 0) {
					cancel();
					return;
				}
				displayRestOfTime(restInMinutes);
				ttlForWatchInMillis = restInMinutes;
			}
		}, minute, minute);
	}

	private void turnGPSOn() {
		try {
		    Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
		    intent.putExtra("enabled", true);
		    getA().sendBroadcast(intent);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}

		try {
		    String provider = Settings.Secure.getString(getA().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		    if(!provider.contains("gps")){ //if gps is disabled
		        final Intent poke = new Intent();
		        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
		        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
		        poke.setData(Uri.parse("3")); 
		        getA().sendBroadcast(poke);
			}
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}	

	private void turnGPSOff() {
		try {
		    Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
		    intent.putExtra("enabled", false);
		    getA().sendBroadcast(intent);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		
		try {
		    String provider = Settings.Secure.getString(getA().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		    if(provider.contains("gps")){ //if gps is enabled
		        final Intent poke = new Intent();
		        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
		        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
		        poke.setData(Uri.parse("3")); 
		        getA().sendBroadcast(poke);
		    }
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	
	protected void resetLocation() {
		lat = ILocation.UNKNOWN_LATITUDE;
		lon = ILocation.UNKNOWN_LONGITUDE;
		alt = ILocation.UNKNOWN_ALTITUDE;
		acc = ILocation.UNKNOWN_ACCURACY;
		mcc = ILocation.UNKNOWN_MOBILE_COUNTRY_CODE;
		mnc = ILocation.UNKNOWN_MOBILE_NETWORK_CODE;
		lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
		cid = ILocation.UNKNOWN_CELL_ID;
		vel = 0f;
	}

	private void registrationSingleUpdateLocationListener(long timeOut, TimeUnit timeUnit) {
		if (criticalData.cancelSingleUpdateLocationListenerThread != null) {
			criticalData.cancelSingleUpdateLocationListenerThread.interrupt();
			criticalData.cancelSingleUpdateLocationListenerThread = null;
		}
		stopGPSUpdateSingle();
		criticalData.singleUpdateLocationListenerThread = new HandlerThread("SingleUpdateLocationListenerThread") {
			protected void onLooperPrepared() {
				WatchdogLocationListener locationListener = singleUpdateLocationListener;
				setLocationListener(locationListener, 0);
			}
		};
		//Reget initial position
		criticalData.initialPosition = null;
		criticalData.singleUpdateLocationListenerThread.start();
		long delay = TimeUnit.MILLISECONDS.convert(timeOut, timeUnit);
		getTimer().schedule(new TimerTask() {

			@Override
			public void run() {
				stopGPSUpdateSingle();
			}
			
		}, delay);
		timerSingleUpdateLocationStart = System.currentTimeMillis();		
	}

	private void registrationAlertTrackLocationListener(final long minTimeInMillis) {
		stopGPSUpdateAlert();
		criticalData.alertTrackLocationListenerThread = new HandlerThread("AlertTrackLocationListenerThread") {
			public void onLooperPrepared() {
				WatchdogLocationListener locationListener = alertTrackLocationListener;
				setLocationListener(locationListener, minTimeInMillis);
			}
		};
		criticalData.alertTrackLocationListenerThread.start();
		timerAlertTrackUpdateLocationStart = System.currentTimeMillis();
		Log.d(TAG, "registration-alert-track-location-listener-complete");
	}

	private void setLocationListener(WatchdogLocationListener locationListener, long minTimeInMillis) {
		if (isGpsAvailable) {
			LocationManager locationManager = getLocationManager();
			//Initialize call
			locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			//Register listener
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, minTimeInMillis, 0, locationListener);
			//locationManager.addGpsStatusListener(locationListener);
		}
//		if (isNetworkEnabled) {
//			locationManager.requestLocationUpdates(
//					LocationManager.NETWORK_PROVIDER, minTimeInMillis, 0,
//					locationListener);
//		}
	}

	private LocationManager getLocationManager() {
		if (locationManager != null) {
			return locationManager;
		}
		synchronized(this) {
			if (locationManager != null) {
				return locationManager;
			}
			locationManager = (LocationManager) this
					.getSystemService(Context.LOCATION_SERVICE);
		}
		return locationManager;
	}

	private void initPhone() {
		telephonyManager = getTelephonyManager();
		isNetworkRoaming = telephonyManager.isNetworkRoaming();
		//phoneCallListener = new PhoneCallListener();
		//telephonyManager.listen(phoneCallListener,
		//		PhoneStateListener.LISTEN_CALL_STATE);
		LocationManager locationManager = getLocationManager();
		isGpsAvailable = locationManager
				.getProvider(LocationManager.GPS_PROVIDER) != null;
	}

	private void startMeasure() {
		Log.d(TAG, "starting-measure");
		if (getA().getCarState() == CarStatesEnum.A) {
			return;
		}
		SensorData accelerometerData = getAccelerometerData();
		accelerometerData.setMaximumRange(sensor.getMaximumRange());
		accelerometerData.setMinDelay(sensor.getMinDelay());
		accelerometerData.setName(sensor.getName());
		accelerometerData.setPower(sensor.getPower());
		accelerometerData.setResolution(sensor.getResolution());
		accelerometerData.setType(sensor.getType());
		accelerometerData.setVendor(sensor.getVendor());
		accelerometerData.setVersion(sensor.getVersion());
		accelerometerData.setCountOfValues(0);

		// Setup debug mode for accelerometer data
		// Set to true write velocity and coordinates (J1 and J2 integrals).
		accelerometerData.preparingMode = true;// getA().isDebugMode();

		if (criticalData.calibrationTimer == null) {
			criticalData.calibrationTimer = new Timer("CallibrationTimer");
		}
		if (criticalData.countdownTimer == null) {
			criticalData.countdownTimer = new Timer("CountdownTimer");
		}
		//final long t0 = 1;//4500;
		// Human take off phone between 1.5s - 4.5s
		notRated = true;
		notCalibrated = true;
		// Setup alert timer
		resetAlertTimer();
		runWatchdogTask(new Runnable() {

			@Override
			public void run() {
				registrationSensorListener();
			}
			
		});

		final long calibrationStart = System.currentTimeMillis();
		
		calibrationData.setCountOfValues(0);
		calibrationData.preparingMode = false;
		sensorEventListener.switchOnCalibration();
		final TimerTask calibrationTask = new TimerTask() {// START MEASURE
					@Override
					public void run() {
						Log.d(TAG, "start-calibration");
						sensorEventListener.setBaseTime(System
								.currentTimeMillis());
						float maxValue = 0;
						float averageSqabs = 0;
						// SGU from SI translation
						sensorResolution = sensor.getResolution() * 100f;
						MinMaxSqValue minMaxValues = new MinMaxSqValue(0,
								Float.MAX_VALUE, 0);
						try {
							// float resolution = sensor.getResolution() * _100;
							int loopsLimit = 2; 
							do {
								long nanoTime = System.nanoTime();
								MinMaxSqValue realMinMaxValue = Sensors
										.minMaxSqabsValue(calibrationData,
												nanoTime, 3000000000L);
								averageSqabs = Sensors.averageSqabsValue(
										calibrationData, nanoTime, 3000000000L);
								if (averageSqabs > 0) {
									averageSqabs = FloatMath.sqrt(averageSqabs);
								}
								if (realMinMaxValue != null) {
									minMaxValues = new MinMaxSqValue(0, FloatMath
											.sqrt(realMinMaxValue.maxValue), 0);
								}

								if (maxValue < minMaxValues.maxValue) {
									maxValue = minMaxValues.maxValue;
								}
								Thread.sleep(500);
								// Check switch off user action
								if (getA().getAppState() == AppStatesEnum.ASO) {
									return;
								}
							} while ((minMaxValues.maxValue - minMaxValues.minValue) >
								2 * Math.E * averageSqabs && loopsLimit-- < 0);
							// || maxAbsoluteAccelerationValue > 10 *
							// sensorResolution);
							WatchdogService.this.minMaxJerkValue = minMaxValues;
							calibrationTime = (System.currentTimeMillis() - calibrationStart) / 1000f;
							measureRateInSeconds = calibrationTime / (float) calibrationData.getCountOfValues();
							
							long timeOffset = System.currentTimeMillis() - calibrationStart;
							long delayStartInMillis = delayStartInSeconds * 1000 - timeOffset;
							
							//Check application swith off
							if (getA().getAppState() == AppStatesEnum.ASO) {
								return;
							}
							
							startAlertDetection(delayStartInMillis > 0 ? delayStartInMillis : 0, sensorEventListener.getSensorTid());
							// Measure correction
							// sensorEventListener.resetJ1J2J1N();
							destroyCalibrationTimer();
						} catch (Throwable th) {
							Log.wtf(TAG, th);
						}
					}

				};
		
		//Setup sensor thread priority (MTS phone issue).
		//criticalData.calibrationTimer.schedule(new TimerTask() {
		runWatchdogTask(new TimerTask() {

			@Override
			public void run() {
				try {
					final int maxCount = 2 * 60;//One minute
					int count = 0;
					while (sensorEventListener.getSensorTid() == -1) {
						
						//Check application swith off
						if (getA().getAppState() == AppStatesEnum.ASO) {
							return;
						}
						
						Thread.sleep(500);
						//Two minutes and no response
						if (count++ > 2 * maxCount) {
							switchOffWithStatus(R.string.accelerometer_hovered);
							return;
						}
						//One minute and no response (hard link between screen-on and accelerometer)
						if (count == maxCount) {
							boolean isChecked = getA().getBoolean(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT);
							if (isChecked) {
								Log.d(TAG, "User disable screen on silent sensor, state as is");
								continue;
							}
							switchOnScreen();
						}
						if (count == 60) {
							// Vibrate, prevent sleeping accelerometer on some models
							vibroSlow();					
						}
					}
					//Update measure thread priority
					android.os.Process.setThreadPriority(sensorEventListener.getSensorTid(), -20);
					//criticalData.calibrationTimer.schedule(calibrationTask, t0);// + 3500);
				} catch (InterruptedException e) { }
			}
		});
		runWatchdogTask(calibrationTask);
		
		final TimerTask countdownTimerTask = new TimerTask() {
			final long time0 = calibrationStart;
			
			@Override
			public void run() {
				if (getA().getAppState() == AppStatesEnum.ASO) {
					cancel();
					return;	
				}
				//if (getA().getCarState() != CarStatesEnum.U) {
				//	cancel();
				//	return;
				//}
				long time1 = System.currentTimeMillis();
				int timerValue = delayStartInSeconds - (int) TimeUnit.MILLISECONDS.toSeconds(time1 - time0);
				if (timerValue < 1) {
					getA().watchdogAlert(R.string.watch, false);
					new AirplaneMode(getA()).switchOn();
					cancel();
					return;
				}
				//%02d%n
				getA().watchdogAlert(String.format("%s:%02d%n", timerValue / 60, timerValue % 60), false);
			}
			
		};
		criticalData.countdownTimer.schedule(countdownTimerTask, 1, 1000);
		// //Update position and velocity for debugging
		// alertTimer.schedule(new TimerTask() {
		//
		// @Override
		// public void run() {
		// boolean debugMode = getA().isDebugMode();
		// if ( debugMode ) {
		// String averageShift = getAverageShift();
		// getA().debugStatus(String.valueOf(averageShift));
		// }
		// }
		//
		// }, t0 + 4000, 1000);

		// AlertStateDetector alertStateDetector = new
		// AlertStateDetector(getA(), alertTimer);
		// alertTimer.schedule(alertStateDetector, t0 + 5000, 500);

	}
	
	public synchronized SensorData getAccelerometerData() {
		if (accelerometerData != null) {
			return accelerometerData;
		}
		accelerometerData = Sensors.makeForNMeasures(45000, true);
		return accelerometerData;
	};

	protected void switchOffWithStatus(final int watchdogStatusResourceId) {
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				IUserCommandsExecutor userCommandExecutor = getA().getUserCommandsExecutor();
				if (userCommandExecutor == null) {
					Log.d(TAG, String.format("can-not-switch-off-with-status[%s][user-command-executor is null]", watchdogStatusResourceId));
					return;
				}
				Log.d(TAG, "switch-off-watch[with-status]");				
				userCommandExecutor.switchOffWatch();
				Log.d(TAG, String.format("switched-off-with-status[%s]", watchdogStatusResourceId));
				getA().watchdogAlert(watchdogStatusResourceId, false);
				String description = getResources().getString(watchdogStatusResourceId);
				fireChangeStateDescription(description);
				//Send analytics
				sendSwitchOffWithStatusAnalytics(watchdogStatusResourceId);
			}
		});
		thread.start();
	}
	
	protected void sendSwitchOffWithStatusAnalytics(int stringResourceId) {
		switch(stringResourceId) {
		case R.string.accelerometer_hovered:
			AnalyticsAdapter.trackSwitchOffByAccelerometerHovered();
			break;
		case R.string.switched_off_by_timer:
			AnalyticsAdapter.trackSwitchOffByTimer();
			break;
		default:
			AnalyticsAdapter.trackSwitchOffByUnknownReason(stringResourceId);
		}
	}

	private void startAlertDetection(long delayInMillis, final int sensorTid) {
		if (isStopped()) {
			return;
		}
		long dontStartBeforeTime = System.currentTimeMillis() + delayInMillis;
		criticalData.alertStateDetector = new AlertStateDetector(
				getA(), criticalData.alertTimer, sensorResolution,
				measureRateInSeconds, dontStartBeforeTime);
		long realDelay = delayInMillis - 3500;//Ten measures
		if (realDelay < 1) {//Correction
			realDelay = 1;
		}

		if (isStopped()) {
			return;
		}
		//Change timer priority after sensor thread
		criticalData.alertTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				Thread.currentThread().setPriority(Thread.MAX_PRIORITY - 1);
				int tid = android.os.Process.myTid();
				android.os.Process.setThreadPriority(tid, -19);
			}
			
		}, 0);

		if (isStopped()) {
			return;
		}
		criticalData.alertTimer.schedule(criticalData.alertStateDetector, realDelay, 500);
		//Track accelerometer activity
		if (getA().isDebugMode()) {
			long delay = TimeUnit.MINUTES.toMillis(10);
			final String logName = "sensor-stat.log";
			getA().makeDebugInfoHeaderIfNotHeadered(logName, "time,pid,tid,priority,sensor-tid,sensor-priority,values-count\n");
			criticalData.alertTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					String time = TextTools.format("yyyy-MM-dd HH:mm:ss", new Date());
					int pid = android.os.Process.myPid();
					int tid = android.os.Process.myTid();
					int priority = android.os.Process.getThreadPriority(tid);
					int sensorPriority = Integer.MAX_VALUE;
					if (sensorTid != -1) {
						sensorPriority = android.os.Process.getThreadPriority(sensorTid);
					}
					int valuesCount = -1;
					if (accelerometerData != null) {
						valuesCount = accelerometerData.getCountOfValues();
					}
					getA().writeDebugInfo(logName, "%s,%s,%s,%s,%s,%s,%s<br>\n", time, pid, tid, priority, sensorTid, sensorPriority, valuesCount);
				}
				
			}, 0, delay);
			
		}
	}

	private void resetAlertTimer() {
		if (criticalData.alertTimer != null) {
			Log.d(TAG, "reset-alert-timer");
			criticalData.alertTimer.cancel();
		}
		criticalData.alertTimer = new Timer("AlertTimer");
	}

	private void initSensors() {
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		final List<Sensor> sensors = sensorManager
				.getSensorList(Sensor.TYPE_ACCELEROMETER);

		if (sensors.isEmpty()) {
			Log.d(TAG, "No accelerometer sensors");
			return;
		}

		if (sensors.size() > 1) {
			Log.w(TAG, "Many accelerometer sensors");
		}
		sensor = sensors.get(0);
	}

	/**
	 * Show dialog for path for accelerometer track.
	 * 
	 * @param uiContext
	 *            Activity context.
	 */
	public void showSaveAccerometerTrack(Context uiContext) {
		final AlertDialog.Builder saveDialog = new AlertDialog.Builder(
				uiContext);
		final String prefKeyName = WATCHDOG_SERVICE_SAVE_INTO_FOLDER_KEY;

		String fileName = getA().getString(prefKeyName, "");

		// Setting Dialog Title
		saveDialog
				.setTitle(fileName.isEmpty() ? R.string.save_into_file_dialog_title_u
						: R.string.save_into_file_dialog_title);

		// Setting Dialog Views
		LinearLayout linearLayout = new LinearLayout(uiContext);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		linearLayout.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));
		linearLayout.setPadding(10, 0, 10, 0);

		final AutoCompleteTextView folderNameEditor = new AutoCompleteTextView(
				uiContext);
		folderNameEditor.setTextAppearance(uiContext,
				android.R.style.TextAppearance_Large);
		folderNameEditor.setHint(R.string.save_into_folder_editor_hint);
		folderNameEditor.setText(fileName);
		folderNameEditor.setEms(10);
		folderNameEditor.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
		folderNameEditor.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));
		folderNameEditor.setTextColor(getResources().getColor(R.color.save_into_file_editor_text_color));
		folderNameEditor.setKeepScreenOn(true);
		folderNameEditor.setSingleLine(true);
		final FolderContentAdapter suggestionAdapter = new FolderContentAdapter(
				uiContext, R.layout.file_row);
		suggestionAdapter.setFilter(new FileFilter() {

			@Override
			public boolean accept(File file) {
				return file.isDirectory();
			}

		});
		folderNameEditor.addTextChangedListener(new TextWatcher() {

			public void beforeTextChanged(CharSequence text, int start,
					int count, int after) {
			}

			public void onTextChanged(CharSequence text, int start, int before,
					int count) {
			}

			public void afterTextChanged(Editable text) {
				File folder = new File(text.toString());
				if (!folder.exists()) {
					return;
				}
				if (!folder.isDirectory()) {
					return;
				}
				suggestionAdapter.setFolder(folder);
				// Memory for user input
				getA().setString(prefKeyName, folder.getAbsolutePath());
			}

		});

		folderNameEditor.setAdapter(suggestionAdapter);

		linearLayout.addView(folderNameEditor);

		saveDialog.setView(linearLayout);

		// On pressing Settings button
		saveDialog.setPositiveButton(
				R.string.save_into_folder_dialog_positive_button,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						File folder = new File(folderNameEditor.getText()
								.toString());
						saveAccelerometerTrackIntoFolder(folder);
					}
				});

		// on pressing cancel button
		saveDialog.setNegativeButton(
				R.string.save_into_folder_dialog_negative_button,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		saveDialog.show();
	}

	/**
	 * Show dialog for debug settings.
	 * 
	 * @param uiContext
	 *            Activity context.
	 */
	public void showDebugSettingsDialog(Context uiContext) {
		final AlertDialog.Builder debugSettingsDialog = new AlertDialog.Builder(
				uiContext);
		final String prefKeyMaxAcc = AlertStateDetector.MAX_ACC_KEY;
		final String prefKeyCriticalAngle = AlertStateDetector.CRITICAL_ANGLE_KEY;

		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(getA());
		String maxAcc = String
				.valueOf(preferences.getFloat(prefKeyMaxAcc, 25f));
		String criticalAngle = String.valueOf(preferences.getFloat(prefKeyCriticalAngle, AlertStateDetector.DEFAULT_CRITICAL_ANGLE));

		// Setting Dialog Title
		debugSettingsDialog.setTitle(R.string.debug_settings_dialog_title);

		// Setting Dialog Views
		LinearLayout linearLayout = new LinearLayout(uiContext);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		linearLayout.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));
		linearLayout.setPadding(10, 0, 10, 0);

		// LinearLayout linearLayout1 = new LinearLayout(uiContext);
		// linearLayout1.setOrientation(LinearLayout.HORIZONTAL);
		// linearLayout1.setLayoutParams(new ViewGroup.LayoutParams(
		// ViewGroup.LayoutParams.FILL_PARENT,
		// ViewGroup.LayoutParams.WRAP_CONTENT));
		// linearLayout1.setPadding(0, 0, 0, 0);

		// LinearLayout linearLayout2 = new LinearLayout(uiContext);
		// linearLayout2.setOrientation(LinearLayout.HORIZONTAL);
		// linearLayout2.setLayoutParams(new ViewGroup.LayoutParams(
		// ViewGroup.LayoutParams.FILL_PARENT,
		// ViewGroup.LayoutParams.WRAP_CONTENT));
		// linearLayout2.setPadding(0, 0, 0, 0);

		final EditText maxAccEditor = new EditText(uiContext);
		maxAccEditor.setTextAppearance(uiContext,
				android.R.style.TextAppearance_Large);
		maxAccEditor.setText(maxAcc);
		maxAccEditor.setEms(10);
		maxAccEditor.setInputType(InputType.TYPE_CLASS_NUMBER);
		maxAccEditor.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));
		maxAccEditor.setHint(R.string.maxacc_hint);
		maxAccEditor.setTextColor(getResources().getColor(R.color.debug_settings_editors_text_color));
		maxAccEditor.setKeepScreenOn(true);
		maxAccEditor.setSingleLine(true);
		final TextView maxAccEditorHint = new TextView(uiContext);
		maxAccEditorHint.setTextAppearance(uiContext,
				android.R.style.TextAppearance_Large);
		maxAccEditorHint.setText(R.string.maxacc_hint);
		maxAccEditorHint.setEms(10);
		maxAccEditorHint.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));

		// linearLayout2.addView(maxAccEditor);
		// linearLayout2.addView(maxAccEditorHint);
		// linearLayout.addView(linearLayout2);
		linearLayout.addView(maxAccEditorHint);
		linearLayout.addView(maxAccEditor);
		
		final EditText criticalAngleEditor = new EditText(uiContext);
		criticalAngleEditor.setTextAppearance(uiContext,
				android.R.style.TextAppearance_Large);
		criticalAngleEditor.setText(criticalAngle);
		criticalAngleEditor.setEms(10);
		criticalAngleEditor.setInputType(InputType.TYPE_CLASS_NUMBER);
		criticalAngleEditor.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));
		criticalAngleEditor.setHint(R.string.asd_angle_hint);
		criticalAngleEditor.setTextColor(getResources().getColor(R.color.debug_settings_editors_text_color));
		criticalAngleEditor.setKeepScreenOn(true);
		criticalAngleEditor.setSingleLine(true);
		final TextView criticalAngleEditorHint = new TextView(uiContext);
		criticalAngleEditorHint.setTextAppearance(uiContext,
				android.R.style.TextAppearance_Large);
		criticalAngleEditorHint.setText(R.string.asd_angle_hint);
		criticalAngleEditorHint.setEms(10);
		criticalAngleEditorHint.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));

		// linearLayout2.addView(maxAccEditor);
		// linearLayout2.addView(maxAccEditorHint);
		// linearLayout.addView(linearLayout2);
		linearLayout.addView(criticalAngleEditorHint);
		linearLayout.addView(criticalAngleEditor);

		debugSettingsDialog.setView(linearLayout);

		// On pressing Settings button
		debugSettingsDialog.setPositiveButton(
				R.string.debug_settings_dialog_positive_button,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						try {
							float maxAcc = Float.valueOf(maxAccEditor.getText()
									.toString());
							getA().setFloat(prefKeyMaxAcc, maxAcc);
							float criticalAngle = Float.valueOf(criticalAngleEditor.getText()
									.toString());
							getA().setFloat(prefKeyCriticalAngle, criticalAngle);
						} catch (Throwable th) {
							Log.wtf(TAG, th);
						}
					}
				});

		// on pressing cancel button
		debugSettingsDialog.setNegativeButton(
				R.string.debug_settings_dialog_negative_button,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		debugSettingsDialog.show();
	}

	public boolean saveAccelerometerTrackIntoFolder(File folder) {
		if (folder == null) {
			throw new IllegalArgumentException();
		}
		if (!folder.isDirectory()) {
			getA().debugStatus("Папка отключена,\nтрек не сохранен");
			return false;
		}
		try {
			ObjectMapper om = new ObjectMapper();
			EventWithAtach event12 = new EventWithAtach();
			event12.setEventClassId((short) 12);
			event12.setDescription("Stored track at " + new Date());
			byte[] accelerometerTrack = getAccelerometerTrack();
			event12.setContent(accelerometerTrack);

			SimpleDateFormat dateTimeFormat = new SimpleDateFormat(
					"yyyyMMddHHmmss");
			String fileName = "event12-" + dateTimeFormat.format(new Date())
					+ ".json";

			FileOutputStream fos = new FileOutputStream(new File(folder,
					fileName));

			BufferedOutputStream bos = new BufferedOutputStream(fos);
			om.writeValue(bos, event12);

			getA().debugStatus("Трек сохранен");

			return true;
		} catch (Throwable th) {
			getA().debugStatus(th.getLocalizedMessage());
			Log.wtf(TAG, th);
			return false;
		}
	}

	public boolean isStopped() {
		return criticalData.alertTimer == null;
	}

	public boolean stop() {
		Log.d(TAG, "stoping-watchdog-service[" + getTrace() + "]");
//		if (getA().getCarState() == CarStatesEnum.A) {
//			return;
//		}
		if (getA().getAppState() == AppStatesEnum.BMA) {
			Log.w(TAG, "please-switch-off-beacon-mode");
			return false;
		}
		safetyReleaseWakeLock();
		if (setupSoundModeThread != null) {
			setupSoundModeThread.interrupt();
			setupSoundModeThread = null;
		}
		
		if (criticalData.timer != null) {
			criticalData.timer.cancel();
			criticalData.timer = null;
		}
		if (criticalData.alertTimer != null) {
			criticalData.alertTimer.cancel();
			criticalData.alertTimer = null;
		}
		
		if (criticalData.countdownTimer != null) {
			criticalData.countdownTimer.cancel();
			criticalData.countdownTimer = null;
		}
		
		if (criticalData.calibrationTimer != null) {
			criticalData.calibrationTimer.cancel();
			criticalData.calibrationTimer = null;
		}
		
		if (criticalData.getLocationTask != null) {
			criticalData.getLocationTask.stop();
			criticalData.getLocationTask.cancel();
		}
		
		destroyCalibrationTimer();
		
		stopUpdateLocation();

		if (criticalData.generateUserActivityThread != null) {
			criticalData.generateUserActivityThread.interrupt();
			criticalData.generateUserActivityThread = null;
		}
		
		stopAccelerometer();

		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int ringerMode = getA().getInt(RINGER_MODE, audioManager.getRingerMode());
		audioManager.setRingerMode(ringerMode);
		
		if (beepService != null && ringerMode == AudioManager.RINGER_MODE_NORMAL) {
			beepService.beepCaso();
		}
		
		if (alertCallTask != null) {
			alertCallTask.cancel(true);
		}
		
		EyesService eyesService = getA().getEyesService();
		if (eyesService != null) {
			eyesService.stop();
		}
		
		restoreScreen();
		
		AirplaneMode airplaneMode = new AirplaneMode(getA());
		airplaneMode.switchOff(false);
		
		//Break another activity if not in watchdog mode
		if (getA().getAppState() != AppStatesEnum.CAC) {
			return false;
		}
		
		//Update maxAbsAcceleration and maxChangeForAngle
		//If car-state is unknown and time for watching more then ten minutes
		if (getA().getCarState() == CarStatesEnum.U 
				&& criticalData.alertStateDetector != null
				&& getA().getBoolean(AlertStateDetector.AUTOMATIC_DETECTION, false)) {
			getA().setFloat(AlertStateDetector.CRITICAL_ANGLE_KEY, NumTools.round(getA().getFloat(AlertStateDetector.LIMIT_CRITICAL_ANGLE_KEY, 0f), 3));
			getA().setMemoryVersion(AlertStateDetector.CRITICAL_ANGLE_KEY);
			getA().setFloat(AlertStateDetector.MAX_ACC_KEY, NumTools.round(getA().getFloat(AlertStateDetector.LIMIT_MAX_ACC_KEY, 0f), 3));
			getA().setMemoryVersion(AlertStateDetector.MAX_ACC_KEY);
			
			getA().setBoolean(AlertStateDetector.AUTOMATIC_DETECTION, false);
		}
		
		return true;

	}

	private String getTrace() {
		Thread thread = Thread.currentThread();
		StringBuilder text = new StringBuilder(new Date().toString());
		text.append(':');
		try {
			StackTraceElement[] stackTrace = thread.getStackTrace();
			text.append(TextTools.withoutPackage(stackTrace[4].getClassName()));
			text.append('#').append(stackTrace[4].getMethodName());
			text.append("<=");
			text.append(TextTools.withoutPackage(stackTrace[5].getClassName()));
			text.append('#').append(stackTrace[5].getMethodName());
			text.append("<=");
			text.append(TextTools.withoutPackage(stackTrace[6].getClassName()));
			text.append('#').append(stackTrace[6].getMethodName());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return text.toString();
	}

	protected void stopAccelerometer() {
		if (sensorManager != null) {
			sensorManager.unregisterListener(sensorEventListener);
		}
	}

	protected void stopUpdateLocation() {
		try {
			stopGPSUpdateSingle();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		try {
			stopGPSUpdateAlert();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		boolean prevGpsEnabled = getA().getBoolean(PREV_GPS_ENABLED, locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));		
		if (!prevGpsEnabled) {
			turnGPSOff();
		}
	}

	private void stopGPSUpdateAlert() {
		LocationManager locationManager = getLocationManager();
		locationManager.removeUpdates(alertTrackLocationListener);
		locationManager.removeGpsStatusListener(alertTrackLocationListener);
		if (criticalData.alertTrackLocationListenerThread != null) {
			try {
				criticalData.alertTrackLocationListenerThread.quit();
			} catch (Throwable th) {
				return;
			}
		}
	}

	private void stopGPSUpdateSingle() {
		LocationManager locationManager = getLocationManager();
		locationManager.removeUpdates(singleUpdateLocationListener);
		locationManager.removeGpsStatusListener(singleUpdateLocationListener);
		if (criticalData.singleUpdateLocationListenerThread != null) {
			try {
				criticalData.singleUpdateLocationListenerThread.quit();
			} catch (Throwable th) {
				return;
			}
		}
	}

	private void destroyCalibrationTimer() {
		if (criticalData.calibrationTimer != null) {
			criticalData.calibrationTimer.cancel();
			criticalData.calibrationTimer = null;
		}
		if (sensorEventListener != null) {
			sensorEventListener.setSensorTid(-1);
		}
	}
	
	protected void updateLocation(int lat, int lon, int alt, int acc, float vel, long time) {
		this.lat = lat;
		this.lon = lon;
		this.alt = alt;
		this.acc = acc;
		this.vel = vel;
		this.time = time;
	}

	public void updateLocation() {
		try {
			//resetLocation();
			
			updateCellLocation();

			//TODO: IP LOCATION DATABASE REQUEST OR CALL GOOGLE-API FOR WIFI COORDS and so on
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	public void updateCellLocation() {
		if (telephonyManager == null) {
			Log.w(TAG, "can-not-update-cell-location[telephony-manager is null]");
			return;
		}
		if (!getA().isCellNetworkAvailable()) {
			Log.d(TAG, "can-not-update-cell-location[cell-network-unavailable]");
			return;
		}
		try {
			CellLocation cellLocation = telephonyManager
					.getCellLocation();
			if (cellLocation == null) {
				return;
			}
			//CDMA phone
			if (CdmaCellLocation.class.equals(cellLocation.getClass())) {
				updateCdmaCellLocation((CdmaCellLocation) cellLocation);
			}
			//GSM phone
			if (GsmCellLocation.class.equals(cellLocation.getClass())) {
				updateGsmCellLocation((GsmCellLocation) cellLocation);
			}
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	private void updateCdmaCellLocation(CdmaCellLocation cellLocation) {
		time = System.currentTimeMillis();
		lat = cellLocation.getBaseStationLatitude();
		//Uknown state correction
		if (lat == Integer.MAX_VALUE) {
			lat = ILocation.UNKNOWN_LATITUDE;
		}
		lon = cellLocation.getBaseStationLongitude();
		//Uknown state correction
		if (lon == Integer.MAX_VALUE) {
			lon = ILocation.UNKNOWN_LONGITUDE;
		}
		cid = cellLocation.getBaseStationId();
		//Uknown state correction
		if (cid == -1) {
			cid = ILocation.UNKNOWN_CELL_ID;
		}
		mnc = cellLocation.getNetworkId();
		//Uknown state correction
		if (mnc == -1) {
			mnc = ILocation.UNKNOWN_MOBILE_NETWORK_CODE;
		}
		mcc = cellLocation.getSystemId();
		//Uknown state correction
		if (mcc == -1) {
			mcc = ILocation.UNKNOWN_MOBILE_COUNTRY_CODE;
		}
		//It's a cdma location
		lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
	}

	private void updateGsmCellLocation(GsmCellLocation gsmCellLocation) {
		time = System.currentTimeMillis();
		cid = gsmCellLocation.getCid();
		lac = gsmCellLocation.getLac();
		String networkOperator = telephonyManager.getNetworkOperator();
		if (networkOperator != null) {
			mcc = Integer.parseInt(networkOperator.substring(0, 3));
			mnc = Integer.parseInt(networkOperator.substring(3));
		}
	}

//	private void updateCoordinates(Location location, String providerKind) {
//		lat = Locations.integerLatitude(location.getLatitude());
//		lon = Locations.integerLongitude(location.getLongitude());
//		if (location.hasAccuracy()) {
//			acc = (int) Math.round(location.getAccuracy() * 100f);
//		}
//		if (location.hasAltitude()) {
//			alt = (int) Math.round(location.getAltitude() * 100f);
//		}
//		time = location.getTime();
//		if (location.hasSpeed()) {
//			vel = location.getSpeed();
//		} else {
//			vel = 0f;
//		}
//		// Context context = getApplicationContext();
//		// int duration = Toast.LENGTH_SHORT;
//		// Toast toast = Toast.makeText(context, providerKind
//		// + " lastknown " + latitude + "\n" + longitude,
//		// duration);
//		// toast.show();
//	}

	public boolean isNetworkEnabled() {
		return isNetworkEnabled;
	}

	public boolean isNetworkRoaming() {
		return isNetworkRoaming;
	}

	public int getLat() {
		return lat;
	}

	public int getLon() {
		return lon;
	}

	public int getAcc() {
		return acc;
	}

	public int getAlt() {
		return alt;
	}

	public long getTime() {
		return time;
	}

	public int getMcc() {
		return mcc;
	}

	public int getMnc() {
		return mnc;
	}

	public int getLac() {
		return lac;
	}

	public int getCid() {
		return cid;
	}

	public float getVel() {
		return vel;
	}

	public int getGsmSignalStrength() {
		return gsmSignalStrength;
	}

	public int getGsmBitErrorRate() {
		return gsmBitErrorRate;
	}

	public byte[] getAccelerometerTrack() {
		if (accelerometerData == null) {
			return null;
		}
		try {
			ObjectMapper om = new ObjectMapper();
			// Shrink byte buffer
			SensorData sensorData;
			if (accelerometerData.preparingMode) {
				sensorData = Sensors.shrinkDataInDebugMode(accelerometerData);
			} else {
				// //TODO: remove below code
				// int countOfValues = accelerometerData.getCountOfValues();
				// double[] ddobechi = new double[countOfValues];
				// double[] rdobechi = new double[countOfValues];
				// byte[] data = accelerometerData.data;
				// long time0 = -1;
				// long time1;
				// long timeStep = 20000000000L;
				// int j = 0;
				// for ( int i = 0; i < countOfValues; i++ ) {
				// ByteBuffer value = Sensors.readBuffer(data, i);
				// float x = value.getFloat();
				// value.position(3 * 4);
				// time1 = value.getLong();
				// if (time0 < 0) {
				// time0 = time1;
				// }
				// ddobechi[i] = x;
				// rdobechi[i] = x;
				// j++;
				// boolean lastIndex = i == countOfValues - 1;
				// if ((time1 - time0) > timeStep || lastIndex) {//Every time
				// step
				// int n = j - 1;
				// int offset = i - n;
				// double[] dbuff = new double[n];
				// double[] rbuff = new double[n];
				// System.arraycopy(ddobechi, offset, dbuff, 0, n);
				// System.arraycopy(rdobechi, offset, rbuff, 0, n);
				// daub d = new daub();
				// d.invDaubTrans(dbuff);
				// d.daubTrans(rbuff);
				// d.invDaubTrans(rbuff);
				// d.daubTrans(rbuff);
				// d.invDaubTrans(rbuff);
				// System.arraycopy(dbuff, 0, ddobechi, offset, n);
				// System.arraycopy(rbuff, 0, rdobechi, offset, n);
				// time0 = time1;
				// j = 0;
				// }
				//
				// }
				// ByteBuffer buff = ByteBuffer.wrap(data);
				// for ( int i = 0; i < countOfValues; i++ ) {
				// buff.position(i * (4 * 5));
				// buff.getFloat();
				// buff.putFloat((float) ddobechi[i]);
				// buff.putFloat((float) rdobechi[i]);
				// }

				sensorData = Sensors.shrinkData(accelerometerData);
			}

			String data = om.writeValueAsString(sensorData);
			return data.getBytes("UTF8");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return null;
	}
	
	public byte[] getAccelerometerTrack(long timeInNanos, long timeBackInNanos) {
		if (accelerometerData == null) {
			return null;
		}
		try {
			ObjectMapper om = new ObjectMapper();
			// Shrink byte buffer
			SensorData sensorData;
			if (accelerometerData.preparingMode) {
				sensorData = Sensors.shrinkDataInDebugMode(accelerometerData, timeInNanos, timeBackInNanos);
			} else {
				sensorData = Sensors.shrinkData(accelerometerData, timeInNanos, timeBackInNanos);
			}

			sensorData.alertTimeInNs = getLastAlertTimeInNs();
			String data = om.writeValueAsString(sensorData);
			return data.getBytes("UTF8");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return null;
	}

	public float getAverageAccelerationForLastSecond() {
		long timeInMillis = System.currentTimeMillis();
		Sensors.AverageValue avalue = Sensors.averageValue(accelerometerData,
				timeInMillis, 500);
		if (avalue == null) {
			return -1;
		}

		float absoluteValue = (float) Math.sqrt(sq(avalue.x) + sq(avalue.y)
				+ sq(avalue.z));
		return absoluteValue;
	}

	// ######################################################

	@Override
	public void onStart(Intent intent, int startid) {
		Log.d(TAG, "watchdog-service-init");
		if (getA().getAppFace() != AppFacesEnum.CAR_ALARMS) {
			Log.d(TAG, "app-face-not-car-alarms-nothing-do");
			return;
		}
		if (!isStopped()) {
			Log.d(TAG, "watchdog-service-always-be-started");
			return;
		}
		if (getA().getAppState() == AppStatesEnum.CAC || getA().getAppState() == AppStatesEnum.U) {//Restart service
			Log.d(TAG, "restarting-watchdog-service");
			CarStatesEnum carState = getA().getCarState();
			if (carState == CarStatesEnum.U) {
				//Only service restarted (critical data restored)
				if (intent == null) {
					//if (stop()) {
					//	getA().setAppState(AppStatesEnum.ASO);
					//	getA().watchdogAlert(R.string.car_alarms_switched_off, false);
					//}
				}
				getA().setAppState(AppStatesEnum.U);
				//not use user timeout
				start(0);
				//TL Version START -->
				//if (getA().getBoolean(IUserLimit.TIME_LIMITED)) {
				//	long delayInMillis = TimeUnit.HOURS.toMillis(2);
				//	startSwitchOffTimer(delayInMillis);
				//}
				//<-- TL Version END
				getA().resume();
				getA().watchdogAlert(R.string.watch, false);
			} else {
				resumeAfterAlertState(carState);
			}
		}
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		getA().setWatchdogService(this);
		//Restore critical data
		WatchdogServiceCritical prevLifeWatchdogServiceCritical = getA().getWatchdogServiceCritical();
		if (prevLifeWatchdogServiceCritical != null) {
			this.criticalData = prevLifeWatchdogServiceCritical;
		}
		startUCExecutor();
		if (sensorManager != null) {
			Log.d(TAG, "watchdog-service-always-be-initialized");
		}
		initSensors();
		initPhone();
		//Start beep service
		Intent beep = new Intent();
		beep.setClass(this, BeepService.class);
		bindService(beep, beepConnection, BIND_AUTO_CREATE);
		//Add listener for GPS activate/passivate
		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		lm.addGpsStatusListener(new android.location.GpsStatus.Listener()
		{
		    public void onGpsStatusChanged(int event)
		    {
		        switch(event)
		        {
		        case GpsStatus.GPS_EVENT_STARTED:
		            gpsOn = true;
		            fireChangeMobileAgentState();
		            break;
		        case GpsStatus.GPS_EVENT_STOPPED:
		            gpsOn = false;
		            fireChangeMobileAgentState();
		            break;
		        }
		    }
		});		
	}

	private void startUCExecutor() {
		try {
			// Get SKey from preferences
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(getA());
			// Load user identity
			int userId = preferences.getInt("userId", 0);
			long mobileAgentId = preferences.getLong("mobileAgentId", 0);
			if (userId == 0 || mobileAgentId == 0) {
				return;
			}
			//WatchdogServiceExecutor executor = new WatchdogServiceExecutor();
			//UCServerFactory.createServer(userId, mobileAgentId, executor, 1975);
			getA().setWebserviceAvailable(false);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		// if ( getA().getCarState() != CarStatesEnum.A ) {
		// getA().setWatchdogService(null);
		// }
		unbindService(beepConnection);
		super.onDestroy();
	}

	// ######################################################

	private float[] position = new float[3];
	private float[] positionConst = new float[3];
	private float[] velocity = new float[3];
	private float[] velocityConst = new float[3];

	int resetErrCount;
	float[] err = new float[3];

	private MobileAgentStateListener mobileAgentStateListener;


	public float[] getPositionAndResetToZero(float[] vel/* variable paramater */) {
		float[] result = new float[3];
		System.arraycopy(position, 0, result, 0, 3);
		System.arraycopy(velocity, 0, vel, 0, 3);
		sensorEventListener.resetJ1J2J1N();
		Arrays.fill(velocityConst, 0);// TODO: check abs value
		System.arraycopy(velocityConst, 0, velocity, 0, 3);
		Arrays.fill(positionConst, 0);// TODO: check abs value
		System.arraycopy(positionConst, 0, position, 0, 3);

		return result;
	}

	public float[] getVelocityAndResetToZero() {
		float[] result = new float[3];
		float[] dV2N = new float[3];
		float[] dVN = new float[3];
		System.arraycopy(accelerometerData.J1, 0, dV2N, 0, 3);
		System.arraycopy(accelerometerData.J12N, 0, dVN, 0, 3);
		// Richardson formula: (2^p * dV2N - dVN) / (2^p - 1)
		// Where p = 4 for Simpson method
		result[0] = ((16f * dV2N[0] - dVN[0]) / (16f - 1f));
		result[1] = ((16f * dV2N[1] - dVN[1]) / (16f - 1f));
		result[2] = ((16f * dV2N[2] - dVN[2]) / (16f - 1f));
		sensorEventListener.resetJ1J2J1N();

		return result;
	}

	public void resetMeasure() {
	}
	
	public static boolean makePhotos(final HipdriverApplication hipdriverApplication, final int countOfPhotos, final int intervalBetweenPhotosInSeconds) {
		Log.d(TAG, "make-photos");
		try {
//			Intent intent = new Intent(hipdriverApplication.getBaseContext(), PhotoActivity.class);
//			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//					| Intent.FLAG_ACTIVITY_NEW_TASK);
//			//intent.setData(Uri.parse("tel://" + pn));
//			try {
//				Thread.sleep(500);
//			} catch (InterruptedException ex) {	}
//			hipdriverApplication.startActivity(intent);
//			Log.d(TAG, "make-photos-active");
			if (hipdriverApplication.startEyesServiceAndWait(TimeUnit.MINUTES.toMillis(1))) {
				//Synchronize with alert call
				hipdriverApplication.runPhoneTask(new Runnable() {

					@Override
					public void run() {
						EyesService eyesService = hipdriverApplication.getEyesService();
						eyesService.start(countOfPhotos, intervalBetweenPhotosInSeconds, CameraInfo.CAMERA_FACING_BACK);
					}
					
				});
				if (android.hardware.Camera.getNumberOfCameras() < 2) {
					return true;
				}
				long delayInMillis = countOfPhotos > 0 ? countOfPhotos * TimeUnit.SECONDS.toMillis(intervalBetweenPhotosInSeconds) : 0
						+ TimeUnit.SECONDS.toMillis(10);//Time for initialization
				hipdriverApplication.schedulePhoneTask(new Runnable() {

					@Override
					public void run() {
						EyesService eyesService = hipdriverApplication.getEyesService();
						eyesService.start(countOfPhotos, intervalBetweenPhotosInSeconds, CameraInfo.CAMERA_FACING_FRONT);
					}
					
				}, delayInMillis);
				return true;
			}
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return false;
	}

	public static boolean makePreview(final HipdriverApplication hipdriverApplication, final long remoteControlMobileAgentId, final long requestId, final int cameraId) {
		Log.d(TAG, "make-preview");
		try {
			if (hipdriverApplication.startEyesServiceAndWait(TimeUnit.MINUTES.toMillis(1))) {
				//Synchronize with alert call
				hipdriverApplication.runPhoneTask(new Runnable() {

					@Override
					public void run() {
						EyesService eyesService = hipdriverApplication.getEyesService();
						eyesService.setRequestId(requestId);
						eyesService.setRemoteControlMobileAgentId(remoteControlMobileAgentId);
						eyesService.preview(cameraId);
					}
					
				});
				return true;
			}
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return false;
	}

	public static boolean makeOutboundCall(HipdriverApplication hipdriverApplication, String phoneNumber, long hangUpInMillis) {
		Log.d(TAG, "make-out-bound-call-start");
		try {
			String pn = TextTools.extractPhoneNumber(phoneNumber);
			if (pn.trim().isEmpty()) {
				Log.w(TAG, "can-not-make-outbound-call[empty-phone-number]");
				return false;
			}

			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setData(Uri.parse("tel://" + pn));
			try {
				Thread.sleep(500);
			} catch (InterruptedException ex) {	}
			hipdriverApplication.startActivity(intent);
			Log.d(TAG, "make-out-bound-call-active");

			if (hangUpInMillis == 0) {
				return true;
			}
			//Thread.sleep(hangUpInMillis);
			//Log.d(TAG, "make-out-bound-call-hangup");
			//hangUp(hipdriverApplication);

			return true;
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return false;
	}

	public static boolean sendSMSInteractively(HipdriverApplication hipdriverApplication, String phoneNumber, String message) {
		Log.d(TAG, "send-sms-interactively");
		try {

			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setData(Uri.parse("sms:" + phoneNumber));
			intent.putExtra("sms_body", message);
			hipdriverApplication.startActivity(intent);
			return true;
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return false;
	}

	public static boolean sendSMS(String phoneNumber, String message) {
		Log.d(TAG, "send-sms");
		try {
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(phoneNumber, null, message, null, null);
			return true;
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		return false;
	}

	public synchronized TelephonyManager getTelephonyManager() {
		if ( telephonyManager == null) {
			telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		}
		return telephonyManager;
	}

	public static void hangUp(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		try {
			//Log.v(TAG, "Hang up phone...");
			Log.d(TAG, "hang-up-phone");
			String className = telephonyManager.getClass().getName();
			endCall(telephonyManager, className);
		} catch (Throwable th) {
			if (!(th instanceof NoSuchMethodException)) {
				Log.wtf(TAG, th);
				return;
			}
			//Try to direct call of class
			try {
				endCall(telephonyManager, "android.telephony.TelephonyManager");
			} catch (Throwable th2) {
				Log.wtf(TAG, th2);
			}
		}
	}

	private static void endCall(TelephonyManager telephonyManager,
			String className) throws ClassNotFoundException,
			NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, RemoteException {
		Class c = Class.forName(className);
		Method m = c.getDeclaredMethod("getITelephony");
		m.setAccessible(true);
		ITelephony telephonyService = (ITelephony) m
				.invoke(telephonyManager);
		telephonyService.endCall();
	}

//	public static boolean isActiveCall(Context context) {//Not worked!
//		Method getFgState = null;
//		Object cm = null;
//		try {
//			Log.v(TAG, "Is active call...");
//			Class cmDesc = Class.forName("com.android.internal.telephony.CallManager");
//			Method getCM = cmDesc.getMethod("getInstance");
//			getFgState = cmDesc.getMethod("getActiveFgCallState");
//			cm = getCM.invoke(null);
//			Object state = getFgState.invoke(cm);
//			if (state.toString().equals("IDLE")) {
//				return false;
//			} else if (state.toString().equals("ACTIVE")) {
//				return true;
//			}
//		} catch (Throwable th) {
//			Log.wtf(TAG, th);
//		}
//		return false;
//	}

	public float[] getGravity() {
		float[] result = new float[3];
		if (sensorEventListener != null) {
			sensorEventListener.getGravity(result);
		}
		return result;
	}

	public MinMaxSqValue getMinMaxJerkValue() {
		return minMaxJerkValue;
	}

	void registrationSensorListener() {
		if (sensorManager == null) {
			Log.w(TAG,
					"Sensor manager is null, cancel registration sensor event listener.");
			return;
		}
		if (sensorEventListener == null) {
			Log.w(TAG,
					"Sensor event listener is null, cancel registration sensor event listener.");
			return;
		}
		if (sensor == null) {
			Log.w(TAG,
					"Sensor is null, cancel registration sensor event listener.");
			return;
		}
		sensorManager.unregisterListener(sensorEventListener, sensor);
		//Don't reseting data for event-class-detector
		if (getA().getCarState() != CarStatesEnum.A) {
			resetAccelerometerData();
		}
		int sensorDelay;
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
			sensorDelay = SensorManager.SENSOR_DELAY_NORMAL;
		} else {
			sensorDelay = 100000;//microseconds
		}
		
	 	//String brand = android.os.Build.BRAND;
	 	//String buildHardware = android.os.Build.HARDWARE;
	 	//String buildId = android.os.Build.ID;
	 	//String manufactur = android.os.Build.MANUFACTURER;
		//if ( "mt6575".equals(buildHardware) && "JRO03C".equals(buildId)) {//MTS device
		//	sensorDelay = SensorManager.SENSOR_DELAY_FASTEST;
		//}
		sensorManager.registerListener(sensorEventListener, sensor,
				sensorDelay);
		//Hack for MTS phone
		setupHighPriorityForSensorThread();
	}

	protected void resetAccelerometerData() {
		accelerometerData.setCountOfValues(0);
		sensorEventListener.reset();
		sensorEventListener.resetJ1J2J1N();
	}

	private void setupHighPriorityForSensorThread() {
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
		//Search sensor thread
		for (Thread thread : threadSet) {
			if (thread.getName().contains("SensorThread")) {
				ThreadGroup threadGroup = thread.getThreadGroup();
				int maxPriority = threadGroup != null ? threadGroup.getMaxPriority() : Thread.MAX_PRIORITY;
				thread.setPriority(maxPriority);
			}
		}
	}

	// Cool workaround @see
	// http://mylifewithandroid.blogspot.ru/2010/04/monitoring-sensors-in-background.html
	public void wakeLockHack() {
		if (!criticalData.generateUserActivityThreadStopped.compareAndSet(true, false)) {
			Log.d(TAG, "thread-generate-user-activity-always-be-running");
			return;
		}
		criticalData.generateUserActivityThread = new GenerateUserActivityThread(this);
		criticalData.generateUserActivityThread.start();
		Thread.yield();
	}

	static class GenerateUserActivityThread extends Thread {
		private final String TAG = TextTools.makeTag(GenerateUserActivityThread.class);
		
		private final WatchdogService ws;

		public GenerateUserActivityThread(WatchdogService ws) {
			setName("GenerateUserActivity");
			this.ws = ws;
		}

		public void run() {
			try {
				unsafetyRun();
			} finally {
				ws.getA().resume();
				ws.criticalData.generateUserActivityThreadStopped.set(true);
			}
		}

		protected void unsafetyRun() {
			//Check Car Alarm active mode
			if (ws.getA().getAppState() != AppStatesEnum.CAC
					&& ws.getA().getAppState() != AppStatesEnum.U) {
				return;
			}
			//Wait for change unknow state
			//It's double check application state actual for phones
			//with hard link between screen and accelerometer
			try {
				AppStatesEnum appState = ws.getA().waitForChangeUnknownState();
				if (appState != AppStatesEnum.CAC) {
					return;
				}
			} catch (InterruptedException e) {
				return;
			}
			//Check interruption
			if (isInterrupted()) {
				return;
			}
			if (!ws.getA().hasWakeLock()) {
				//Try to resolve issue #29
				ws.getA().releaseWakeLock();
				ws.getA().setWakeLock();
			}
			
			if (ws.accelerometerData == null) {
				Log.w(TAG, "Uninitialized accelerometer data, no sence to continue.");
				return;
			}
			Log.d(TAG, "Waiting 11 sec for reregistering sensor event listener ...");
			Future<?> future = ws.runWatchdogTask(new Runnable() {

				@Override
				public void run() {
					ws.registrationSensorListener();
				}
				
			});
			
			int eventsCount0 = 0;
			try {
				future.get();
				eventsCount0 = ws.accelerometerData.getCountOfValues();
				Thread.yield();
				// Timeout for check after 11 second:
				if (ws.getA().getCarState() != CarStatesEnum.A) {
					long rest = 11000L;
					Thread.sleep(150L);
					rest -= 150L;
					// Vibrate for 50 milliseconds, prevent sleeping accelerometer on some models
					ws.vibroSlow();
					rest -= 600L;
					//vibroSlow reseting eventsCount to zero.
					eventsCount0 = 0;
					ws.waitForAccelerometerData(ws.accelerometerData, rest, eventsCount0);
					// End timeout
				} else {
					ws.waitForAccelerometerData(ws.accelerometerData, 11000L, eventsCount0);
				}
			} catch (InterruptedException ex) {
				Log.wtf(TAG, ex);
				return;
			} catch (ExecutionException e) {
				Log.wtf(TAG, e);
			}
			//Check interruption
			if (isInterrupted()) {
				return;
			}

			int eventsCount1 = ws.accelerometerData.getCountOfValues();
			if (eventsCount1 > eventsCount0) {
				//// Reset vibration effect
				//if (ws.getA().getCarState() != CarStatesEnum.A) {
				//.eventsCount0	ws.resetAccelerometerData();
				//}
				ws.getA().resume(3000L);
				Log.d(TAG,
						"Sensor listener reregistering success, cool hack is work.");
				return;
			}

			boolean isChecked = ws.getA().getBoolean(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT);
			if (isChecked) {
				//// Reset vibration effect
				//if (ws.getA().getCarState() != CarStatesEnum.A) {
				//	ws.resetAccelerometerData();
				//}
				ws.getA().resume(3000L);
				Log.d(TAG, "User disable screen on silent sensor, state as is");
				return;
			}
			Log.d(TAG, "User activity generation thread real started");

			//Check interruption
			if (isInterrupted()) {
				return;
			}
			ws.switchOnScreen();
			Log.d(TAG, "User activity generation thread exiting");
		}

	}
	
	protected void restoreScreen() {
		if (criticalData.userActivityWakeLock != null) {
			try {
				Log.d(TAG, "User activity wake lock released");
				criticalData.userActivityWakeLock.release();
				criticalData.userActivityWakeLock = null;
			} catch (Throwable th) {
				Log.wtf(TAG, th);
			}
		}
		restoreScreenBrightnessSettings();
	}
	
	protected void switchOnScreen() {
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);

		setToMinimalBrightness();
		
		criticalData.userActivityWakeLock = pm.newWakeLock(
				PowerManager.SCREEN_DIM_WAKE_LOCK
						| PowerManager.ACQUIRE_CAUSES_WAKEUP,
				"GenerateUserActivity");
		criticalData.userActivityWakeLock.acquire();
		getA().resume(3000L);
	}
	
	protected void waitForAccelerometerData(SensorData accelerometerData,
			long timeoutInMillis, int eventsCount0) throws InterruptedException {
		long timeInFuture = System.currentTimeMillis() + timeoutInMillis;
		while( accelerometerData.getCountOfValues() <= eventsCount0 ) {
			Thread.sleep(TimeUnit.SECONDS.toMillis(1));
			if (System.currentTimeMillis() > timeInFuture) {
				break;
			}
		}
	}

	private void setToMinimalBrightness() {
		Hardwares.putSettingsInt(getContentResolver(),
				    Hardwares.SCREEN_BRIGHTNESS, 1);
	}
	
	private void restoreScreenBrightnessSettings() {
		//Check if not stored
		if (criticalData.screenBrightnessLevel < 0) {
			return;
		}
		if (!getA().getBoolean(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT)) {
			Hardwares.putSettingsInt(getContentResolver(),
					Hardwares.SCREEN_BRIGHTNESS, criticalData.screenBrightnessLevel);
		}
	}

//	public void startAlertTracking(long delayPushAlertEventInMillis, long delayLocationRequestInMillis) {
//		Looper.prepare();
//		setLocationListener(locationListener);
//		
//		if (timer == null) {
//			timer = new Timer("GetLocationTimer");
//		}
//		int maxEventsBeforeClean = (int) (delayPushAlertEventInMillis / delayLocationRequestInMillis);
//		
//		GetLocation getLocation = getLocationTask.get();
//		if (getLocation != null) {
//			getLocation.cancel();
//		}
//		getLocation = new GetLocation(maxEventsBeforeClean, getA(), timer);
//		getLocationTask.set(getLocation);
//		try {
//			timer.schedule(getLocation, delayLocationRequestInMillis, delayLocationRequestInMillis);
//			timer.purge();
//		} catch (Throwable th) {
//			Log.wtf(TAG, th);
//		}
//		
//		myLooper = Looper.myLooper();
//		//Looper.loop();
//		myLooper.quit();
//	}
	
	public List<? extends ICarLocation> getAlertTrack() {
		GetLocation getLocation = criticalData.getLocationTask;
		if (getLocation == null) {
			return Collections.emptyList();
		}
		List<? extends ICarLocation> track = new ArrayList<ru.hipdriver.j.Location>(getLocation.track);
		return track;
	}

	public List<? extends ICarLocation> getLastTenLocations() {
		GetLocation getLocation = criticalData.getLocationTask;
		if (getLocation == null) {
			return Collections.emptyList();
		}
        int count = getLocation.lastTenLocations.size();
		List<ru.hipdriver.j.Location> lastTenLocations = new ArrayList<ru.hipdriver.j.Location>(count);
        ru.hipdriver.j.Location location = null;
        while ((location = getLocation.lastTenLocations.poll()) != null) {
            lastTenLocations.add(location);
        }
		return lastTenLocations;
	}
	
	public ICarLocation getFirstLocationAfterAlert() {
		GetLocation getLocation = criticalData.getLocationTask;
		if (getLocation == null) {
			return null;
		}
		return getLocation.firstLocation;
	}

	public ICarLocation getInitialPosition() {
		return criticalData.initialPosition;
	}

	public void clearAlertTrack(List<? extends ILocation> sendedTrack) {
		GetLocation getLocation = criticalData.getLocationTask;
		if (getLocation == null) {
			return ;
		}
		getLocation.track.removeAll(sendedTrack);
	}

	public GetLocation makeGetLocation(int maxEventsBeforeClean,
			HipdriverApplication a, Timer timer, long minTimeInMillisForStopDetection, long minTimeInMillisForLocationRequest) {
		GetLocation prevTask = criticalData.getLocationTask;
		if (prevTask != null) {
			prevTask.stop();
			prevTask.cancel();
		}
		//Listen GPS receiver as frequently as possible.
		registrationAlertTrackLocationListener(minTimeInMillisForLocationRequest);
		GetLocation getLocation = new GetLocation(this, maxEventsBeforeClean, getA(), timer, minTimeInMillisForStopDetection);
		criticalData.getLocationTask = getLocation;
		return getLocation;
	}

	public void setLastAlertTimeInNs(long nanoTime) {
		criticalData.lastAlertTimeInNs = nanoTime;
	}
	
	public long getLastAlertTimeInNs() {
		return criticalData.lastAlertTimeInNs;
	}

	public void beepA() {
		if (beepService == null) {
			return;
		}
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int ringerMode = getA().getInt(RINGER_MODE, audioManager.getRingerMode());
		if (ringerMode == AudioManager.RINGER_MODE_NORMAL) {
			beepService.beepA();
		}
	}
	public void beepCac() {
		if (beepService == null) {
			return;
		}
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int ringerMode = getA().getInt(RINGER_MODE, audioManager.getRingerMode());
		if (ringerMode == AudioManager.RINGER_MODE_NORMAL) {
			beepService.beepCac();
		}
	}
	public void beepCaso() {
		if (beepService == null) {
			return;
		}
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int ringerMode = getA().getInt(RINGER_MODE, audioManager.getRingerMode());
		if (ringerMode == AudioManager.RINGER_MODE_NORMAL) {
			beepService.beepCaso();
		}
	}
	public void beepLowBattery() {
		if (beepService == null) {
			return;
		}
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int ringerMode = getA().getInt(RINGER_MODE, audioManager.getRingerMode());
		if (ringerMode == AudioManager.RINGER_MODE_NORMAL) {
			beepService.beepLowBattery();
		}
	}
	public ILocation getLocation() {
		return Locations.readonlyLocation(lat, lon, alt, acc, mcc, mnc, lac, cid);
	}
	
	public void updateAlertState(String debugInfo) {
		updateAlertState();
		if (getA().isDebugMode()) {
			CarStatesEnum carState = getA().getCarState();
			CarStatesEnum firstDetectionCarState = getA().getFirstDetectionCarState();
			String logName = "alert-states.log";
			getA().makeDebugInfoHeaderIfNotHeadered(logName, "time,first-detection-car-state,state,debug-info\n");
			getA().writeDebugInfo(logName, "%s,%s,%s,%s<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()), firstDetectionCarState, carState, debugInfo);
		}
	}
	
	public void beepAlarmAlarm() {
		if (beepService == null) {
			return;
		}
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int ringerMode = getA().getInt(RINGER_MODE, audioManager.getRingerMode());
		if (ringerMode == AudioManager.RINGER_MODE_NORMAL) {
			beepService.beepAlarmAlarm();
		}
	}
	
	public void stopAlarmAlarm() {
		if (beepService == null) {
			return;
		}
		beepService.pauseAlarmAlarm();
	}
	
	public void beep(int resId, boolean looping) {
		if (beepService == null) {
			return;
		}
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int ringerMode = getA().getInt(RINGER_MODE, audioManager.getRingerMode());
		if (ringerMode == AudioManager.RINGER_MODE_NORMAL) {
			beepService.beep(resId, looping);
		}
	}

	public void stop(int resId) {
		if (beepService == null) {
			return;
		}
		beepService.stop(resId);
	}

	public void updateAlertState() {
		switch(getA().getCarState()) {
		case CE:
			getA().watchdogAlert(R.string.car_evacuate, false);
			break;
		case WS:
			getA().watchdogAlert(R.string.steal_wheels, false);
			if (getA().getBoolean(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_STEAL_WHEELS)) {
				runRestartTask();
			}
			break;
		case CP:
			getA().watchdogAlert(R.string.car_poke, false);
			if (getA().getBoolean(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_CAR_POKE)) {
				runRestartTask();
			}
			break;
		case CS:
			getA().watchdogAlert(R.string.car_stolen, false);
			break;
		}
		fireChangeMobileAgentState();
	}

	protected void displayRestOfTime(long restInMinutes) {
		IUserCommandsExecutor userCommandExecutor = getA().getUserCommandsExecutor();
		if (userCommandExecutor != null) {
			int restInHours = (int)restInMinutes / 60;
			int restInMins = (int)restInMinutes % 60;
			String timerText = getString(R.string.counterdown_timer, restInHours, restInMins);
			userCommandExecutor.safetySetTextOutput(R.id.timer_text, timerText, false, false, false);
		}
	}

	private void vibro(long delay) {
		Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(delay);
	}
	
	protected void vibroSlow() throws InterruptedException {
		//Pause measure
		getA().pause();
		vibro(50);
		Thread.sleep(500);
		vibro(50);
		//Reset measure...
		if (accelerometerData != null) {
			accelerometerData.reset();
		}
		if (criticalData.alertStateDetector != null) {
			criticalData.alertStateDetector.initialState();
		}
		getA().resume();
	}
	
	protected void vibroSlowSingle() throws InterruptedException {
		//Pause measure
		getA().pause();
		vibro(50);
		//Reset measure...
		if (accelerometerData != null) {
			accelerometerData.reset();
		}
		if (criticalData.alertStateDetector != null) {
			criticalData.alertStateDetector.initialState();
		}
		getA().resume();
	}

	public long getRestTTLForWatchInMillis() {
		return ttlForWatchInMillis;
	}

	public void runAlertCallTask(Runnable command) {
		Future<?> task = getA().runAlertTask(command);
		if (alertCallTask != null) {
			alertCallTask.cancel(true);
		}
		alertCallTask = task;
	}

	public void runRestartTask() {
		Future<?> task = getA().runPhoneTask(new Runnable() {

			@Override
			public void run() {
				//TODO: why this check?
				if (Thread.interrupted()) {
					return;
				}
				if (getA().getAppState() != AppStatesEnum.CAC) {
					return;
				}
				if (getA().getCarState() != CarStatesEnum.CP
						&& getA().getCarState() != CarStatesEnum.WS) {
					return;
				}
				
//				IUserCommandsExecutor usersCommandsExecutor = getA().getUserCommandsExecutor();
//				if (usersCommandsExecutor == null) {
//					Log.wtf(TAG, "userCommandsExecutor is null!");
//					return;
//				}
//				usersCommandsExecutor.switchOffWatch();
				Log.d(TAG, "run-restart-task");
				if(stop()) {
					getA().setAppState(AppStatesEnum.U);
					getA().watchdogAlert(R.string.car_alarms_switched_off, false);
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Log.wtf(TAG, e);
					return;
				}
				getA().setAppState(AppStatesEnum.U);
				getA().setCarState(CarStatesEnum.U);
				//usersCommandsExecutor.switchOnWatch();
				start(0);
			}
			
		}); 
		if (restartTask != null) {
			restartTask.cancel(true);
		}
		restartTask = task;
	}

	public boolean isGpsAvailable() {
		return isGpsAvailable;
	}

	public void resumeAfterAlertState(CarStatesEnum carState) {
		Log.d(TAG, "resume-after-alert-state");
		if (carState == CarStatesEnum.U) {
			return;
		}
		if (getA().isDebugMode()) {
			String logName = "ws-restart-stat.log";
			getA().makeDebugInfoHeaderIfNotHeadered(logName, "time,delay-in-millis\n");
			getA().writeDebugInfo(logName, "%s,%s<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()),"resume-after-alert-state");
		}
		
		//Reset previous timers;
		resetAlertTimer();				
		if (criticalData.getLocationTask != null) {
			criticalData.getLocationTask.stop();
			criticalData.getLocationTask.cancel();
		}
		registrationSensorListener();
		//Run only geolocation activity
		try {
			if (carState == CarStatesEnum.A) {
				//Take alert info into server, start new alert-track
				try {
					UITaskFactory.sendAlertEvent(getA(), null, TimeUnit.DAYS.toMillis(1));
				} catch (Throwable th) {
					Log.wtf(TAG, th);
				}
				try {
					EventClassDetector eventClassDetector = new EventClassDetector(getA(), criticalData.alertTimer, "Тревога U!");
					long detectionTimeoutInMillis = EventClassDetector.DEFAULT_BIG_TIMEOUT_MILLIS;//getA().getInt(EventClassDetector.BIG_TIMEOUT_KEY, 10) * 1000;
					//TODO: Check timer state
					Log.d(TAG, "make-event-class-detecting-task");
					criticalData.alertTimer.schedule(eventClassDetector, detectionTimeoutInMillis);
				} catch (Throwable th) {
					Log.wtf(TAG, th);
				}
				
			}
			startAlertTrackSending();
			startEvacuationStateCorrector();
			
			AnalyticsAdapter.trackInAlertMode("Restart Alert");
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		
	}
	
	protected int getMaxPointsPerPacket() {
		int delayWhereAmIRequestInSeconds = getDelayWhereAmIRequestInSeconds();
		int delayPushAlertEventInSeconds = getDelayPushAlertEventInSeconds();
		
		int maxPointsPerPacket = delayPushAlertEventInSeconds / delayWhereAmIRequestInSeconds;
		if (maxPointsPerPacket <= 0) {
			maxPointsPerPacket = 6;
		}
		return maxPointsPerPacket;
	}
	
	protected int getDelayPushAlertEventInSeconds() {
		int delayPushAlertEventInSeconds = getA().safetyGetInt(IMobileAgentProfile.DELAY_PUSH_ALERT_EVENT_IN_SECONDS, IMobileAgentProfile.DEFAULT_DELAY_PUSH_ALERT_EVENT_IN_SECONDS);
		if (delayPushAlertEventInSeconds <= 0) {
			delayPushAlertEventInSeconds = IMobileAgentProfile.DEFAULT_DELAY_PUSH_ALERT_EVENT_IN_SECONDS;
		}
		return delayPushAlertEventInSeconds;
	}

	protected int getDelayWhereAmIRequestInSeconds() {
		int delayWhereAmIRequestInSeconds = getA().safetyGetInt(IMobileAgentProfile.DELAY_WHERE_AM_I_REQUEST_IN_SECONDS, IMobileAgentProfile.DEFAULT_DELAY_WHERE_AM_I_REQUEST_IN_SECONDS);
		if (delayWhereAmIRequestInSeconds <= 0) {
			delayWhereAmIRequestInSeconds = IMobileAgentProfile.DEFAULT_DELAY_WHERE_AM_I_REQUEST_IN_SECONDS;
		}
		return delayWhereAmIRequestInSeconds;
	}
	
	protected void startAlertTrackSending() {
		Log.d(TAG, "start-alert-track-sending");
		int delayPushAlertEventInSeconds = getDelayPushAlertEventInSeconds();
		int delayWhereAmIRequestInSeconds = getDelayWhereAmIRequestInSeconds();

		long delayNotGpsLocationRequestInMillis = TimeUnit.SECONDS.toMillis(delayPushAlertEventInSeconds);
		AlertTrackSender alertTrackSender = new AlertTrackSender(getA(), criticalData.alertTimer);
		getA().makeDebugInfoHeaderIfNotHeadered("send-alert-track.log", "state-id,time,lat,lon,alt,acc,mcc,mnc,lac,cid\n");
		long minTimeInMillisForStopDetection = TimeUnit.SECONDS.toMillis(delayWhereAmIRequestInSeconds); 
		int maxPointsPerPacket = getMaxPointsPerPacket();
		//20 = 10 minutes timeout for alert-track-sending task / MobileAgentProfile.DEFAULT_DELAY_WHERE_AM_I_REQUEST_IN_SECONDS; 
		GetLocation getLocation = makeGetLocation(20 * 50 * maxPointsPerPacket, getA(), criticalData.alertTimer, minTimeInMillisForStopDetection, 0);

		long delayPushAlertEventInMillis = TimeUnit.SECONDS.toMillis(delayPushAlertEventInSeconds);
		try {
			criticalData.alertTimer.schedule(alertTrackSender, 1001, delayPushAlertEventInMillis / 2);
			criticalData.alertTimer.schedule(getLocation, 1, delayNotGpsLocationRequestInMillis / 2);
			criticalData.alertTimer.purge();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	public void setLastAlertTime(Date alertTime) {
		this.criticalData.alertTime = alertTime;
	}
	
	public long getLastAlertTimeInMillis() {
		if (criticalData.alertTime != null) {
			return criticalData.alertTime.getTime();
		}
		return 0;
	}
	
	protected void startEvacuationStateCorrector() {
		WatchdogService watchdogService = getA().getWatchdogService();
		ILocation location = watchdogService.getLocation();
		EvacuationStateCorrector evacuationStateCorrector = new EvacuationStateCorrector(getA(), criticalData.alertTimer, location, criticalData.alertTime); 
		try {
			long delay = TimeUnit.SECONDS.toMillis(80);
			criticalData.alertTimer.schedule(evacuationStateCorrector, delay);
			criticalData.alertTimer.purge();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	public WatchdogServiceCritical getCriticalData() {
		return criticalData;
	}

	public long getDurationFromLastAlertInMillis() {
		return System.currentTimeMillis() - getLastAlertTimeInMillis();
	}
	
	public Future<?> runWatchdogTask(Runnable command) {
		return watchdogTasksExecutorService.submit(command);
	}

	public void storeUserVariables() {
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		getA().setBoolean(PREV_GPS_ENABLED, locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		getA().setInt(RINGER_MODE, audioManager.getRingerMode());
		if (!getA().getBoolean(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT)) {
			criticalData.screenBrightnessLevel = Hardwares.getSettingsInt(getContentResolver(),
				    Hardwares.SCREEN_BRIGHTNESS, -1);
		}
	}

	public void setRemoteListener(
			MobileAgentStateListener mobileAgentStateListener) {
		this.mobileAgentStateListener = mobileAgentStateListener;
	}
	
	protected void fireChangeMobileAgentState() {
		if (mobileAgentStateListener == null) {
			return;
		}
		this.runWatchdogTask(new Runnable() {

			@Override
			public void run() {
				mobileAgentStateListener.onChangeMobileAgentState();
			}
			
		});
		
	}

	protected void fireChangeStateDescription(String description) {
		if (mobileAgentStateListener == null) {
			return;
		}
		mobileAgentStateListener.onChangeStateDescription(description);
	}
	
	public boolean isGpsOn() {
		return gpsOn;
	}

	public void setAlertStateMakeCallAndStartReasonDetection(String alertReason) {
		if (criticalData.alertStateDetector == null) {
			Log.w(TAG, String.format("no-alert-state-detector-on[%s]", alertReason));
			return;
		}
		criticalData.alertStateDetector.setAlertStateMakeCallAndStartReasonDetection(this, alertReason, -1, -1);
	}
	
}

