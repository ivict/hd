package ru.hipdriver.android.app.sslucs;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.j.Location;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
//All fields are required for multi-class response resolving
//@see ChatConnectionWithClientRoleTask#parseAnswer
//i.e. ignoreUnknown = false is critical for correct parsing
@JsonIgnoreProperties(ignoreUnknown = false)
public class ShortMobileAgentState {

	@JsonProperty(value = "asid")
	public short appStateId;

	@JsonProperty(value = "lat")
	public int lat = ILocation.UNKNOWN_LATITUDE;

	@JsonProperty(value = "lon")
	public int lon = ILocation.UNKNOWN_LONGITUDE;
	
	@JsonProperty(value = "alt")
	public int alt = ILocation.UNKNOWN_ALTITUDE;
	
	@JsonProperty(value = "acc")
	public int acc = ILocation.UNKNOWN_ACCURACY;
	
	//Mobile Country Code
	@JsonProperty(value = "mcc")
	public int mcc = ILocation.UNKNOWN_MOBILE_COUNTRY_CODE;
	
	//Mobile Network Code
	@JsonProperty(value = "mnc")
	public int mnc = ILocation.UNKNOWN_MOBILE_NETWORK_CODE;
	
	//Location Area Code
	@JsonProperty(value = "lac")
	public int lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
	
	//Cell Id
	@JsonProperty(value = "cid")
	public int cid = ILocation.UNKNOWN_CELL_ID;
	
	//Timestamp
	@JsonProperty(value = "time")
	public Date time;
	
	//Car state id
	@JsonProperty(value = "csid")
	public short carStateId;
	
	//Gsm signal strength
	@JsonProperty(value = "gsmss")
	public int gsmSignalStrength = IMobileAgentState.UNKNOWN_GSM_SIGNAL_STRENGTH;
	
	//Battery percents
	@JsonProperty(value = "bpct")
	public float batteryPct;
	
	//Battery status
	@JsonProperty(value = "bst")
	public int batteryStatus;
	
	//Is gps on
	@JsonProperty(value = "gpson")
	public boolean gpsOn;
	
	//Invariant id for message on front panel
	@JsonProperty(value = "iid")
	public int iid;

	public short getAppStateId() {
		return appStateId;
	}

	public void setAppStateId(short appStateId) {
		this.appStateId = appStateId;
	}

	public Location getLastLocation() {
		Location location = new Location();
		location.setTime(getTime());
		location.setLat(getLat());
		location.setLon(getLon());
		location.setAcc(getAcc());
		location.setAlt(getAlt());
		location.setMcc(getMcc());
		location.setMnc(getMnc());
		location.setLac(getLac());
		location.setCid(getCid());
		location.setCarStateId(getCarStateId());
		return location;
	}

	public void setLastLocation(Location lastLocation) {
		setTime(lastLocation.getTime());
		setLat(lastLocation.getLat());
		setLon(lastLocation.getLon());
		setAcc(lastLocation.getAcc());
		setAlt(lastLocation.getAlt());
		setMcc(lastLocation.getMcc());
		setMnc(lastLocation.getMnc());
		setLac(lastLocation.getLac());
		setCid(lastLocation.getCid());
		setCarStateId(lastLocation.getCarStateId());
	}

	public void setLastLocation(ShortLocation lastLocation) {
		setTime(lastLocation.getTime());
		setLat(lastLocation.getLat());
		setLon(lastLocation.getLon());
		setAcc(lastLocation.getAcc());
		setAlt(lastLocation.getAlt());
		setMcc(lastLocation.getMcc());
		setMnc(lastLocation.getMnc());
		setLac(lastLocation.getLac());
		setCid(lastLocation.getCid());
		setCarStateId(lastLocation.getCarStateId());
	}

	public int getLat() {
		return lat;
	}

	public void setLat(int lat) {
		this.lat = lat;
	}

	public int getLon() {
		return lon;
	}

	public void setLon(int lon) {
		this.lon = lon;
	}

	public int getAlt() {
		return alt;
	}

	public void setAlt(int alt) {
		this.alt = alt;
	}

	public int getAcc() {
		return acc;
	}

	public void setAcc(int acc) {
		this.acc = acc;
	}

	public int getMcc() {
		return mcc;
	}

	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	public int getMnc() {
		return mnc;
	}

	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	public int getLac() {
		return lac;
	}

	public void setLac(int lac) {
		this.lac = lac;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public short getCarStateId() {
		return carStateId;
	}

	public void setCarStateId(short carStateId) {
		this.carStateId = carStateId;
	}

	public int getGsmSignalStrength() {
		return gsmSignalStrength;
	}

	public void setGsmSignalStrength(int gsmSignalStrength) {
		this.gsmSignalStrength = gsmSignalStrength;
	}

	public float getBatteryPct() {
		return batteryPct;
	}

	public void setBatteryPct(float batteryPct) {
		this.batteryPct = batteryPct;
	}
	
	public int getBatteryStatus() {
		return batteryStatus;
	}

	public void setBatteryStatus(int batteryStatus) {
		this.batteryStatus = batteryStatus;
	}

	public boolean isGpsOn() {
		return gpsOn;
	}

	public void setGpsOn(boolean gpsOn) {
		this.gpsOn = gpsOn;
	}

	/**
	 * Invariant identity for exchange resource-id between two mobile applications.
	 * @return iid @see CCRI for details
	 */
	public int getIid() {
		return iid;
	}

	/**
	 * Set invariant identity for exchange resource-if between two mobile applications.
	 * @param iid @see CCRI for details
	 */
	public void setIid(int iid) {
		this.iid = iid;
	}
		
}
