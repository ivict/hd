package ru.hipdriver.android.app.help;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.hipdriver.android.app.HipdriverApplication;
import ru.hipdriver.android.app.R;
import ru.hipdriver.i.support.AppFacesEnum;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

public class ShortHelpCollectionPagerAdapter extends FragmentStatePagerAdapter {

	private final HipdriverApplication hipdriverApplication;
	private final List<Fragment> fragments;
	private final Map<Fragment, CharSequence> titles;
	private final Map<Fragment, Integer> indexes;
	private final ViewPager viewPager;

	public ShortHelpCollectionPagerAdapter(
			HipdriverApplication hipdriverApplication,
			FragmentManager fragmentManager, ViewPager viewPager) {
		super(fragmentManager);
		this.hipdriverApplication = hipdriverApplication;
		this.fragments = new ArrayList<Fragment>();
		this.titles = new HashMap<Fragment, CharSequence>();
		this.indexes = new HashMap<Fragment, Integer>();
		this.viewPager = viewPager;
	}

	@Override
	public Fragment getItem(int position) {
		initFragmentsListIfListIsEmpty();
		return fragments.get(position);
	}

	private void initFragmentsListIfListIsEmpty() {
		if (!fragments.isEmpty()) {
			return;
		}
		ShortHelpMainFragment fragment0 = new ShortHelpMainFragment();
		putFragment(fragment0, 0, R.string.short_help_reference);
		//Fragment fragment1 = createSettingsFragment(hipdriverApplication
		//		.getAppFace());
		//putFragment(fragment1, 1,
		//		getPageTitle(hipdriverApplication.getAppFace()));
	}

	@Override
	public int getCount() {
		initFragmentsListIfListIsEmpty();
		return fragments.size();
	}

	private int getPageTitle(AppFacesEnum appFace) {
		switch (appFace) {
		case CAR_ALARMS:
			return R.string.select_car_alarms_settings_page;
		case REMOTE_CONTROL:
			return R.string.select_remote_control_settings_page;
		}
		return -1;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		initFragmentsListIfListIsEmpty();
		CharSequence title;
		if (position == 0) {
			title = hipdriverApplication.getResources().getString(
					R.string.short_help_reference);
		} else {
			Fragment fragment = fragments.get(position);
			title = titles.get(fragment);
		}
		return (position > 0 ? " " : "") + title + (position < getCount() - 1 ? " >" : "");
	}


	@Override
	public int getItemPosition(Object object) {
		Integer index = indexes.get(object);
		if (index == null) {
			return PagerAdapter.POSITION_NONE;
		}
		return PagerAdapter.POSITION_UNCHANGED;
	}

	public void onChangeAppFace(AppFacesEnum currentAppFace) {
		initFragmentsListIfListIsEmpty();
		Fragment fragment0 = fragments.get(0);
		fragments.clear();
		titles.clear();
		indexes.clear();
		putFragment(fragment0, 0, R.string.select_work_mode_page);
		//Fragment fragment1 = createSettingsFragment(currentAppFace);
		//putFragment(fragment1, 1, getPageTitle(currentAppFace));
		notifyDataSetChanged();
	}

	protected void putFragment(Fragment fragment, int position,
			int titleResourceId) {
		if (fragments.size() < position) {
			throw new IllegalStateException(
					"Can't insert null values, append only possible.");
		}
		if (fragments.size() == position) {
			fragments.add(fragment);
		} else {
			Fragment replacedFragment = fragments.get(position);
			titles.remove(replacedFragment);
			indexes.remove(replacedFragment);

			fragments.set(position, fragment);
		}
		titles.put(fragment,
				hipdriverApplication.getResources().getString(titleResourceId));
		indexes.put(fragment, position);
	}

	public void show(final int position, Fragment fragment, int titleResourceId, int contentResourceId) {
		if (fragments.size() < position) {
			throw new IllegalStateException(
					"Can't insert null values, append only possible.");
		}
		// Append case
		if (fragments.size() == position) {
			putFragment(fragment, position, titleResourceId);
			notifyDataSetChanged();
			setPage(position);
			updateContent(fragment, contentResourceId);
			return;
		}

		Fragment existFragmentOnSamePosition = fragments.get(position);
		// If same classes then always be equals
		if (existFragmentOnSamePosition.getClass().equals(fragment.getClass())) {
			// TODO: change title only
			setPage(position);
			titles.put(existFragmentOnSamePosition,
					hipdriverApplication.getResources().getString(titleResourceId));			
			updateContent(existFragmentOnSamePosition, contentResourceId);
			notifyDataSetChanged();
			return;
		}
		putFragment(fragment, position, titleResourceId);
		notifyDataSetChanged();
		setPage(position);
		updateContent(fragment, contentResourceId);
	}

	private void updateContent(Fragment fragment, int contentResourceId) {
		if (!fragment.getClass().equals(ShortHelpContentFragment.class)) {
			return;
		}
		ShortHelpContentFragment shortHelpContentFragment = (ShortHelpContentFragment) fragment;
		shortHelpContentFragment.updateContent(contentResourceId);
	}

	public void show(final int position) {
		setPage(position);
	}

	private void setPage(final int position) {
		int fragmentsCount = fragments.size();
		if (fragmentsCount <= position || fragmentsCount < 0) {
			throw new IllegalStateException(
					"Invalid index value, position is out of range.");
		}
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				viewPager.setCurrentItem(position, true);
			}
		}, 250);
	}

	public boolean isFirstPageSelected() {
		return viewPager.getCurrentItem() == 0;
	}

	public int showPrevPage() {
		int position = viewPager.getCurrentItem();
		if (position > 0) {
			show(position - 1);
			return position;
		}
		return position - 1;
	}

}
