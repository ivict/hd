package ru.hipdriver.android.app;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import ru.hipdriver.android.app.R;

public class DeviceAdapter extends ArrayAdapter<DeviceInfo> {
	private Activity context;
	ArrayList<DeviceInfo> data = null;

	public DeviceAdapter(Activity context, int resource,
			ArrayList<DeviceInfo> data) {
		super(context, resource, data);
		this.context = context;
		this.data = data;
	}

	// Ordinary view in Spinner, we use android.R.layout.simple_spinner_item	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		return super.getView(position, convertView, parent);
	}

	// This view starts when we click the spinner.
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.devices_spinner_layout, parent, false);
		}

		DeviceInfo item = data.get(position);

		if (item != null) { // Parse the data from each object and set it.
			//ImageView myFlag = (ImageView) row.findViewById(R.id.imageIcon);
			TextView myDevice = (TextView) row.findViewById(R.id.device_name);
			//if (myFlag != null) {
			//	myFlag.setBackgroundDrawable(getResources().getDrawable(
			//			item.getDeviceFlag()));
			//}
			if (myDevice != null)
				myDevice.setText(item.getDeviceName());

		}

		return row;
	}
}
