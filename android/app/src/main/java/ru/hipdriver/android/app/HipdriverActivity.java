package ru.hipdriver.android.app;

//import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import ru.hipdriver.android.util.TextTools;

public abstract class HipdriverActivity extends FragmentActivity {

    private static final String TAG = TextTools.makeTag(HipdriverActivity.class);

    public HipdriverApplication getA() {
		return (HipdriverApplication) this.getApplicationContext();
	}

    protected<T> T uiComponent(int resourceId) {
        try {
            return (T) findViewById(resourceId);
        } catch (ClassCastException e) {
            Log.wtf(TAG, e);
            throw e;
        }
    }

    //For close keyboard
    public void closeKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);
    }

}
