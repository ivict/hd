package ru.hipdriver.android.app;

import java.util.Locale;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class SettingsTools {

	private SettingsTools() { }
	
	//@see http://stackoverflow.com/questions/2900023/change-language-programatically-in-android
	public static void setLocale(Context context, String locale) {
		Resources resources = context.getResources();
	    // Change locale settings in the app.
	    DisplayMetrics displayMetrics = resources.getDisplayMetrics();
	    Configuration configuration = resources.getConfiguration();
	    configuration.locale = new Locale(locale.trim().toLowerCase());
	    resources.updateConfiguration(configuration, displayMetrics);		
	}
	
}
