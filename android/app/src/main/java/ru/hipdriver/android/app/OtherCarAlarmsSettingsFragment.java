package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgentProfile;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import ru.hipdriver.android.app.R;

public class OtherCarAlarmsSettingsFragment extends FaceSelectCollectionHipdriverFragment {

	private static final String TAG = TextTools.makeTag(OtherCarAlarmsSettingsFragment.class);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.other_car_alarms_settings_fragment,
				container, false);
		refresh(view);
		return view;
	}

	private void refresh(final View view) {
		if (view == null) {
			return;
		}
		FragmentActivity activity = getActivity();
		
		final HintedCheckBox carAutoAlarmsAfterCarPoke = (HintedCheckBox) view.findViewById(R.id.car_auto_alarms_after_car_poke_checkbox);
		carAutoAlarmsAfterCarPoke.setChecked(getA().getBoolean(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_CAR_POKE));
		carAutoAlarmsAfterCarPoke.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getA().setBoolean(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_CAR_POKE, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_CAR_POKE);
			}
			
		});
		
		final HintedCheckBox carAutoAlarmsAfterStealWheels = (HintedCheckBox) view.findViewById(R.id.car_auto_alarms_after_steal_wheels_checkbox);
		carAutoAlarmsAfterStealWheels.setChecked(getA().getBoolean(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_STEAL_WHEELS));
		carAutoAlarmsAfterStealWheels.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getA().setBoolean(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_STEAL_WHEELS, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_STEAL_WHEELS);
			}
			
		});
		
		//Power cable notifications
		
		HintedCheckBox aopccCheckbox = (HintedCheckBox) view.findViewById(R.id.alarm_on_power_cable_connect);
		OnCheckedChangeListener aopccOnCheckedListener = new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getA().setBoolean(IMobileAgentProfile.ALARM_ON_POWER_CABLE_CONNECT, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.ALARM_ON_POWER_CABLE_CONNECT);

			}
			
		};
		aopccCheckbox.setOnCheckedChangeListener(aopccOnCheckedListener);
		boolean isAopccCheckBoxChecked = getA().getBoolean(IMobileAgentProfile.ALARM_ON_POWER_CABLE_CONNECT);
		aopccCheckbox.setChecked(isAopccCheckBoxChecked);
		aopccOnCheckedListener.onCheckedChanged(aopccCheckbox, isAopccCheckBoxChecked);

		HintedCheckBox aopcdCheckbox = (HintedCheckBox) view.findViewById(R.id.alarm_on_power_cable_disconnect);
		OnCheckedChangeListener aopcdOnCheckedListener = new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getA().setBoolean(IMobileAgentProfile.ALARM_ON_POWER_CABLE_DISCONNECT, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.ALARM_ON_POWER_CABLE_DISCONNECT);

			}
			
		};
		aopcdCheckbox.setOnCheckedChangeListener(aopcdOnCheckedListener);
		boolean isAopcdCheckBoxChecked = getA().getBoolean(IMobileAgentProfile.ALARM_ON_POWER_CABLE_DISCONNECT);
		aopcdCheckbox.setChecked(isAopcdCheckBoxChecked);
		aopcdOnCheckedListener.onCheckedChanged(aopcdCheckbox, isAopcdCheckBoxChecked);
		
		//Special and rare parameters
		
		final HintedCheckBox disableScreenOnSensorSilent = (HintedCheckBox) view.findViewById(R.id.disable_screen_on_sensor_silent_checkbox);
		disableScreenOnSensorSilent.setChecked(getA().getBoolean(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT));
		disableScreenOnSensorSilent.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) { 
					//getA().makeToastWarning(R.string.disable_screen_on_sensor_silent_hint1);
					getA().showMessage(R.string.disable_screen_on_sensor_silent_hint2);
				}
				
				getA().setBoolean(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT, isChecked);
    			getA().clearShowCount(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT);    			
			}
			
		});
		
		final HintedCheckBox runAfterRebootCheckbox = (HintedCheckBox) view.findViewById(R.id.run_after_reboot_checkbox);
		runAfterRebootCheckbox.setChecked(getA().getBoolean(IMobileAgentProfile.RUN_AFTER_REBOOT));
		runAfterRebootCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (!isChecked) { 
					//getA().makeToastWarning(R.string.disable_screen_on_sensor_silent_hint1);
					getA().showMessage(R.string.run_after_reboot_hint2);
				}
				
				getA().setBoolean(IMobileAgentProfile.RUN_AFTER_REBOOT, isChecked);
    			//getA().clearShowCount(IMobileAgentProfile.RUN_AFTER_REBOOT);    			
			}
			
		});
		
		CheckBox rsiunCheckbox = (CheckBox) view.findViewById(R.id.request_server_if_unknown_number_checkbox);
		rsiunCheckbox.setChecked(getA().getBoolean(IMobileAgentProfile.REQUEST_SERVER_IF_UNKNOWN_NUMBER));
		rsiunCheckbox.setText(Html.fromHtml(getResources().getString(R.string.request_server_if_unknown_number)));
		rsiunCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) { 
					getA().showMessage(R.string.request_server_if_unknown_number_hint2);
				}
				
				getA().setBoolean(IMobileAgentProfile.REQUEST_SERVER_IF_UNKNOWN_NUMBER, isChecked);
    			getA().setMemoryVersion(IMobileAgentProfile.REQUEST_SERVER_IF_UNKNOWN_NUMBER);
			}
			
		});
		
		CheckBox loCheckbox = (CheckBox) view.findViewById(R.id.landscape_orientation_checkbox);
		loCheckbox.setChecked(getA().getInt(MainActivity.SCREEN_ORIENTATION) == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		loCheckbox.setText(Html.fromHtml(getResources().getString(R.string.landscape_orientation)));
		loCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					getA().setInt(MainActivity.SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
				} else {
					getA().setInt(MainActivity.SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
				}
			}
			
		});
		
//		final HipdriverScrollView scrollView = (HipdriverScrollView) findViewById(R.id.scroll_view);
//		scrollView.setOnScrollByY(new HipdriverScrollView.OnScrollByY() {
//			@Override
//			public void scrollByY(int scrollY) {
//				saveScrollPosition(R.layout.car_alarms_additional_behaviour_settings_screen, scrollY);//Current screen
//			}
//		});
//		scrollView.postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				scrollView.scrollTo(0, scrollY);
//			}
//		}, 200);
		
		CheckBox slasotdCheckbox = (CheckBox) view.findViewById(R.id.send_log_after_switch_off_to_developers_checkbox);
		OnCheckedChangeListener slasotdOnCheckedListener = new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getA().setBoolean(HipdriverApplication.SEND_LOG_AFTER_SWITCH_OFF_TO_DEVELOPERS, isChecked);
    			getA().setMemoryVersion(HipdriverApplication.SEND_LOG_AFTER_SWITCH_OFF_TO_DEVELOPERS);

			}
			
		};
		slasotdCheckbox.setOnCheckedChangeListener(slasotdOnCheckedListener);
		boolean isSlasotdCheckBoxChecked = getA().getBoolean(HipdriverApplication.SEND_LOG_AFTER_SWITCH_OFF_TO_DEVELOPERS);
		slasotdCheckbox.setChecked(isSlasotdCheckBoxChecked);

	}

}
