package ru.hipdriver.android.app;

import java.util.Timer;
import java.util.TimerTask;

import ru.hipdriver.android.util.TextTools;

import android.util.Log;

public abstract class SingleRunHipdriverTimerTask extends TimerTask {
	
	private static final String TAG = TextTools.makeTag(SingleRunHipdriverTimerTask.class);
	
	private final HipdriverApplication applicationContext;
	protected final Timer timer;
	
	public SingleRunHipdriverTimerTask(HipdriverApplication applicationContext,
			Timer timer) {
		this.applicationContext = applicationContext;
		this.timer = timer;
	}
	
	public HipdriverApplication getA() {
		return applicationContext;
	}

	@Override
	final public void run() {
		while (getA().isPaused()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Log.d(TAG, "single-run-timer-task-interrupted");
				return;
			}
		}
		runIfNotPaused();
	}
	
	protected abstract void runIfNotPaused(); 

}
