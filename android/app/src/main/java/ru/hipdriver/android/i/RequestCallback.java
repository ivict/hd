package ru.hipdriver.android.i;

/**
 * 
 * Interface for callback of request to ISslServer
 *
 */
public interface RequestCallback {

	/**
	 * On response reaction.
	 * @param returnValue returned value or null if no-result 
	 * @param isCommunicationError true if network communication fail
	 * @param isTimeout true if request fail by timeout
	 * @return true if callback continue listen for series of responses, false if need to stop listening
	 */
	boolean onResponse(Object returnValue, boolean isCommunicationError, boolean isTimeout); 
	
	/**
	 * On making request. 
	 * @param requestId
	 * @param longTtlRequest TODO
	 * @param isCommunicationError true if network communication fail
	 * @param isTimeout true if request fail by timeout
	 */
	void onSendRequest(long requestId, boolean longTtlRequest, boolean isCommunicationError, boolean isTimeout);
	
}
