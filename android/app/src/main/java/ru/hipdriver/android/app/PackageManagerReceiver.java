package ru.hipdriver.android.app;

import java.util.List;

import ru.hipdriver.android.util.TextTools;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;

//@see beutiful solution from http://stackoverflow.com/questions/18692571/how-it-works-warning-that-app-is-going-to-be-uninstalled
public class PackageManagerReceiver extends HipdriverBroadcastReceiver {

	private static final String TAG = TextTools.makeTag(PackageManagerReceiver.class);

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "package: RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
		// fetching package names from extras
		String[] packageNames = intent
				.getStringArrayExtra("android.intent.extra.PACKAGES");

		HipdriverApplication hipdriverApplication = getA(context);
		Log.d(TAG, "package: "+ hipdriverApplication.getPackageName());

		if (packageNames != null) {
			for (String packageName : packageNames) {
				if (packageName != null
						&& packageName.equals(hipdriverApplication.getPackageName())) {
					// User has selected our application under the Manage Apps
					// settings
					// now initiating background thread to watch for activity
					new ListenActivities(hipdriverApplication).start();

				}
			}
		}
	}

}

class ListenActivities extends Thread {

	private static final String TAG = TextTools.makeTag(ListenActivities.class);

	boolean exit = false;
	ActivityManager am = null;
	HipdriverApplication context = null;

	public ListenActivities(HipdriverApplication con) {
		context = con;
		am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
	}

	public void run() {

		Looper.prepare();

		while (!exit) {

			// get the info from the currently running task
			List<ActivityManager.RunningTaskInfo> taskInfo = am
					.getRunningTasks(MAX_PRIORITY);

			String activityName = taskInfo.get(0).topActivity.getClassName();

			// TODO: Add support install another version of application event

			Log.d("topActivity", "CURRENT Activity ::" + activityName);

			if (activityName
					.equals("com.android.packageinstaller.UninstallerActivity")) {
				// User has clicked on the Uninstall button under the Manage
				// Apps settings
				Log.d(TAG, "send-watch-off-event");
				// Send switch-off event after uninstall
				UITaskFactory.sendWatchOffEvent(context, null);
				exit = true;
			} else if (activityName
					.equals("com.android.settings.ManageApplications")) {
				// back button was pressed and the user has been taken back to
				// Manage Applications window
				// we should close the activity monitoring now
				exit = true;
			}
		}
		Looper.loop();
	}
}