package ru.hipdriver.android.app;

import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;

public class CellularNetworkStateListener extends PhoneStateListener {

	protected final HipdriverApplication hipdriverApplication;

	public CellularNetworkStateListener(HipdriverApplication hipdriverApplication) {
		super();
		this.hipdriverApplication = hipdriverApplication;
	}

	public HipdriverApplication getA() {
		return hipdriverApplication;
	}

	@Override
	public void onCellLocationChanged(CellLocation location) {
		super.onCellLocationChanged(location);
		WatchdogService watchdogService = getA().getWatchdogService();
		if ( watchdogService == null ) {
			return;
		}
		watchdogService.updateCellLocation();
	}

	@Override
	public void onSignalStrengthsChanged(SignalStrength signalStrength) {
		super.onSignalStrengthsChanged(signalStrength);
		
        //Check sign-up
        if (!getA().isSignup()) {
        	return;
        }
		WatchdogService watchdogService = getA().getWatchdogService();
		if ( watchdogService == null ) {
			return;
		}
		int oldRssi = watchdogService.gsmSignalStrength; 
		int newRssi = signalStrength.getGsmSignalStrength();
		int ber = signalStrength.getGsmBitErrorRate();
		watchdogService.gsmBitErrorRate = ber;
		watchdogService.gsmSignalStrength = newRssi;
		//Generate event after low strength detection
		if (oldRssi > 1 && newRssi <= 1) {
			if (watchdogService.isGpsAvailable()) {
				UITaskFactory.sendGpsEvent(getA(), null);
			} else {
				UITaskFactory.sendGsmEvent(getA(), null);
			}
		}
	}

	@Override
	public void onServiceStateChanged(ServiceState serviceState) {
		getA().setCellNetworkAvailable(serviceState.getState() == ServiceState.STATE_IN_SERVICE);
	}
	
}
