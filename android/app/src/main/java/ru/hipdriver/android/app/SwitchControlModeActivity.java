package ru.hipdriver.android.app;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.app.sslucs.MobileAgentStateDescription;
import ru.hipdriver.android.i.ISslClient;
import ru.hipdriver.android.i.RequestCallback;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.ControlModsEnum;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.i.support.UserCommandTypesEnum;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class SwitchControlModeActivity extends HipdriverActivity {

	protected static final String TAG = TextTools
			.makeTag(SwitchControlModeActivity.class);
	private static final String DO_NOT_SHOW_NOTIFICATION_MESSAGE = "scma_do_not_show_notification_message";
	private Thread switchToControlModeThread;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				setContentView(R.layout.switch_control_mode_screen);

				// Get the control mode from the intent
				Intent intent = getIntent();
				final ControlModsEnum targetControlMode = (ControlModsEnum) intent
						.getSerializableExtra(MainActivity.EXTRA_SWITCH_TO_CONTROL_MODE);
				if (targetControlMode == null) {
					getA().makeToastWarning(
							R.string.unknown_control_mode);
					cancelSwitch(IMobileAgent.INVALID_ID);
				}
				switchToControlModeThread = new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							switchControlModeTo(targetControlMode);
						} catch (Throwable e) {
							Log.wtf(TAG, e);
							getA().makeToastWarning(
									R.string.control_mode_switch_error);
							cancelSwitch(IMobileAgent.INVALID_ID);
						}
					}

				}, String.format("swith-control-mode-to[%s]", targetControlMode));
				switchToControlModeThread.start();
			}
			
		});
	}

	protected void switchControlModeTo(final ControlModsEnum targetControlMode)
			throws InterruptedException {
		final long remoteMobileAgentId = getA().getLong(
				MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
		if (remoteMobileAgentId == IMobileAgent.INVALID_ID) {
			cancelSwitch(IMobileAgent.INVALID_ID);
			return;
		}
		if (targetControlMode == getA().getControlMode()) {
			cancelSwitch(remoteMobileAgentId);
			return;
		}
		switch (targetControlMode) {
		case INTERNET:
			// TODO: Show modal message?
			// Update settings on site //edit-mobile-agent-profile from request
			// to server
			// Do call for remote phone.
			// On remote mobile agent switch on SslServer
			// On local mobile agent switch on SslClient
			matchingPhase(remoteMobileAgentId, targetControlMode);
			break;
		case PHONE_CALLS:
		case SMS:
			// Send request with chat server
			// After success edit, switch
			// In remote mobile agent switch off SslServer.
			// In local mobile agent switch off SslClient.

			if (!getA().isCellNetworkAvailable()) {
				getA().makeToastWarning(
						R.string.no_cell_network_available_but_we_try_to_switch_remote_mobile_agent_into_phone_call_control_mode);
			}

			ISslClient ucClient = getA().getUcClient();
			if (ucClient == null) {
				getA().makeUserCommandsSender();
			}
			ucClient = getA().getUcClient();
			byte[] args = NumericTools.shortToBytes(DBLinks
					.link(ControlModsEnum.PHONE_CALLS));
			ucClient.execCommandAsync(remoteMobileAgentId,
					UserCommandTypesEnum.ES, args, new RequestCallback() {

						@Override
						public boolean onResponse(Object returnValue,
								boolean isCommunicationError, boolean isTimeout) {
							if (isCommunicationError || isTimeout) {
								// Request current mode and switch to internet
								// mode
								matchingPhase(remoteMobileAgentId,
										targetControlMode);
								
								return false;
							}
							if (returnValue instanceof MobileAgentStateDescription) {
								MobileAgentStateDescription mobileAgentStateDescription = (MobileAgentStateDescription) returnValue;
								ControlModsEnum currentControlModeOnRemoteAgent = ControlModsEnum
										.valueOfGlobalKey(mobileAgentStateDescription
												.getDescription());
								if (currentControlModeOnRemoteAgent == targetControlMode) {
									switchSuccess(targetControlMode);
									return false;
								} else {
									getA().makeToastWarning(
											R.string.remote_mobile_agent_can_not_connected_to_cell_network);
								}
							} else {
								getA().makeToastWarning(
										R.string.protocol_error);
							}
							cancelSwitch(remoteMobileAgentId);
							return false;
						}

						@Override
						public void onSendRequest(long requestId,
								boolean longTtlRequest,
								boolean isCommunicationError, boolean isTimeout) {
							if (isCommunicationError || isTimeout) {
								getA().makeToastWarning(
										R.string.internet_connection_not_stable);
								cancelSwitch(remoteMobileAgentId);
								return;
							}

						}

					}, false, 1);
			break;
		}
	}

	protected void matchingPhase(final long remoteMobileAgentId,
			final ControlModsEnum targetControlMode) {
		UITaskFactory.getMap(getA(), new After() {
			public void success(Object returnValue) {
				if (!(returnValue instanceof Properties)) {
					getA().makeToastWarning(
							R.string.control_mode_switch_error_from_server_side);
					cancelSwitch(remoteMobileAgentId);
					return;
				}
				Properties properties = (Properties) returnValue;
				Object controlModeIdObject = properties
						.get(IMobileAgentProfile.CONTROL_MODE_ID);
				if (controlModeIdObject == null) {
					//TODO: add comment for user
					cancelSwitch(remoteMobileAgentId);
				}
				String controlModeIdText = String.valueOf(controlModeIdObject);
				ControlModsEnum currentControlModeForRemoteAgent = null;
				try {
					currentControlModeForRemoteAgent = DBLinks
							.unlinkControlMode(Short.parseShort(controlModeIdText));
				} catch (NumberFormatException e) {
					Log.wtf(TAG, e);
					//TODO: add comment for user
					cancelSwitch(remoteMobileAgentId);
				}
				if (currentControlModeForRemoteAgent == targetControlMode) {
					switchSuccess(targetControlMode);
					return;
				}

				final String phoneNumber = getA()
						.getString(
								getA().getSelectedDeviceForRemoteControlPhoneNumberPrevKey(),
								"");

				if (phoneNumber.isEmpty()) {
					getA().makeToastWarning(
							R.string.no_phone_number_for_remote_agent);
					cancelSwitch(remoteMobileAgentId);
					return;
				}

				// TODO: refactoring and always create new properties
				properties.setProperty(IMobileAgentProfile.CONTROL_MODE_ID,
						String.valueOf(DBLinks.link(targetControlMode)));
				// properties.setProperty(IMobileAgent.ALERT_PHONE_NUMBER_KEY,
				// alertPhoneNumber);
				UITaskFactory.sendEmap(getA(), new After() {

					public void success(Object returnValue) {
						if (returnValue == null) {
							getA().makeToastWarning(
									R.string.control_mode_switch_error_from_server_side);
							cancelSwitch(remoteMobileAgentId);
							return;
						}
						boolean isBooleanType = Boolean.class
								.equals(returnValue.getClass())
								|| boolean.class.equals(returnValue.getClass());
						if (isBooleanType && (Boolean) returnValue) {
							requestForSwitchControlModeThroughCall(targetControlMode,
									phoneNumber, remoteMobileAgentId);
							return;
						}
						if (boolean.class.equals(returnValue)
								&& (Boolean) returnValue) {
							switchSuccess(targetControlMode);
							return;
						}
						cancelSwitch(remoteMobileAgentId);
					}

					public void failure(int returnCode, String failureReason) {
						getA().makeToastWarning(
								R.string.internet_connection_not_stable);
						cancelSwitch(remoteMobileAgentId);
					}

				}, remoteMobileAgentId, properties);
			}

			public void failure(int returnCode, String failureReason) {
				getA().makeToastWarning(
						R.string.internet_connection_not_stable);
				cancelSwitch(remoteMobileAgentId);
			}
		}, remoteMobileAgentId);
	}

	@Override
	protected void onDestroy() {
		if (switchToControlModeThread != null) {
			switchToControlModeThread.interrupt();
		}
		super.onDestroy();
	}

	protected void cancelSwitch(long remoteMobileAgentId) {
		if (remoteMobileAgentId != IMobileAgent.INVALID_ID) {
			Properties properties = new Properties();		
			// TODO: refactoring and always create new properties
			properties.setProperty(IMobileAgentProfile.CONTROL_MODE_ID,
					String.valueOf(DBLinks.link(getA().getControlMode())));
			// properties.setProperty(IMobileAgent.ALERT_PHONE_NUMBER_KEY,
			// alertPhoneNumber);
			UITaskFactory.sendEmap(getA(), null, remoteMobileAgentId, properties);
		}

		Intent returnIntent = new Intent();
		setResult(RESULT_CANCELED, returnIntent);
		finish();
	}

	protected void switchSuccess(final ControlModsEnum targetControlMode) {
		//Make notification if control mode is internet
		if (targetControlMode == ControlModsEnum.INTERNET && !getA().getBoolean(DO_NOT_SHOW_NOTIFICATION_MESSAGE, false)) {
			CharSequence title = Html.fromHtml(getResources().getString(R.string.internet_control_mode_notification_title));
			CharSequence text = Html.fromHtml(getResources().getString(R.string.internet_control_mode_notification_text));
			getA().makeNotificationForMainActivity(R.drawable.pictogram, R.drawable.pictogram, title, text, DO_NOT_SHOW_NOTIFICATION_MESSAGE);
		}
		
		Intent returnIntent = new Intent();
		returnIntent.putExtra(MainActivity.EXTRA_SWITCH_TO_CONTROL_MODE,
				targetControlMode);
		setResult(RESULT_OK, returnIntent);
		finish();
	}

	protected void requestForSwitchControlModeThroughCall(
			final ControlModsEnum targetControlMode,
			final String phoneNumber,
			final long remoteMobileAgentId) {
		final Context uiContext = this;
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				final AlertDialog.Builder modalAlert = new AlertDialog.Builder(uiContext);

				// Setting Dialog Title
				modalAlert.setTitle(R.string.modal_alert_title);

				// Setting Dialog Views
				ScrollView scrollView = new ScrollView(uiContext);
				scrollView.setFillViewport(true);
				
				LinearLayout linearLayout = new LinearLayout(uiContext);
				linearLayout.setOrientation(LinearLayout.VERTICAL);
				linearLayout.setLayoutParams(new ViewGroup.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT));
				linearLayout.setPadding(10, 0, 10, 0);

				final TextView alertView = new TextView(uiContext);
				alertView.setTextAppearance(uiContext,
						android.R.style.TextAppearance_Large);
				Spanned text;
				switch (targetControlMode) {
				case INTERNET:
					text = Html.fromHtml(uiContext.getString(R.string.request_for_switch_to_internet_control_mode_through_call));
					alertView.setText(text);
					break;
				case PHONE_CALLS:
				case SMS:
					text = Html.fromHtml(uiContext.getString(R.string.request_for_switch_to_phone_call_control_mode_through_call));
					alertView.setText(text);
					break;
				}
				alertView.setEms(10);
				
				linearLayout.addView(alertView);
				scrollView.addView(linearLayout);
				
				modalAlert.setView(scrollView);

				// On pressing Settings button
				modalAlert.setPositiveButton(
						R.string.modal_alert_positive_button,
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, int which) {
								runOnUiThread(new Runnable() {

									@Override
									public void run() {
										dialog.dismiss();
										getA().runPhoneTask(new Runnable() {

											@Override
											public void run() {
												WatchdogService.makeOutboundCall(getA(),
														phoneNumber, TimeUnit.SECONDS.toMillis(10));
												//TODO: check version from site
												switchSuccess(targetControlMode);
											}
											
										});
									}
									
								});
							}
						});

				// On pressing Settings button
				modalAlert.setNegativeButton(
						R.string.modal_alert_negative_button,
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, int which) {
								runOnUiThread(new Runnable() {

									@Override
									public void run() {
										dialog.dismiss();
										cancelSwitch(remoteMobileAgentId);										
									}
									
								});
							}
						});
				try {
					// Showing Alert Message
					modalAlert.show();
				} catch (Throwable th) {
					Log.wtf(TAG, th);
					safetyCancelSwitch(remoteMobileAgentId);
				}
			}
			
		});
		
		
	}

	private void safetyCancelSwitch(final long remoteMobileAgentId) {
		try {
			cancelSwitch(remoteMobileAgentId);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
}
