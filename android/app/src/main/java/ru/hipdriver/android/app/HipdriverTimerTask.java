package ru.hipdriver.android.app;

import java.util.Timer;
import java.util.TimerTask;

public abstract class HipdriverTimerTask extends TimerTask {
	
	private final HipdriverApplication applicationContext;
	protected final Timer timer;
	
	public HipdriverTimerTask(HipdriverApplication applicationContext,
			Timer timer) {
		this.applicationContext = applicationContext;
		this.timer = timer;
	}
	
	public HipdriverApplication getA() {
		return applicationContext;
	}

	@Override
	final public void run() {
		if (getA().isPaused()) {
			onPause();
			return;
		}
		runIfNotPaused();
	}
	
	protected abstract void runIfNotPaused(); 

	protected void onPause() { }; 

}
