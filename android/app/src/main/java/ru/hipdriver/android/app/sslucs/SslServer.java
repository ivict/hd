package ru.hipdriver.android.app.sslucs;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.app.CCRI;
import ru.hipdriver.android.app.Hardwares;
import ru.hipdriver.android.app.HipdriverApplication;
import ru.hipdriver.android.app.MainActivity;
import ru.hipdriver.android.app.NumericTools;
import ru.hipdriver.android.app.WatchdogService;
import ru.hipdriver.android.i.ISslServer;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.i.ISid;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.i.support.ControlModsEnum;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.i.support.UserCommandTypesEnum;
import ru.hipdriver.j.Location;
import ru.hipdriver.j.MobileAgentWithState;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.util.Log;

import ru.hipdriver.android.app.R;

public class SslServer extends ChatConnector<ChatConnectionWithServerRoleTask> implements
		ISslServer, UserCommandExecutor {

	private static final String TAG = TextTools.makeTag(SslServer.class);
	
	public static final String REQUEST_ID = "requestId";
	public static final String REMOTE_CONTROL_MOBILE_AGENT_ID = "remoteControlMobileAgentId";

	public SslServer(HipdriverApplication hipdriverApplication,
			long mobileAgentId) throws UnknownHostException {
		super(hipdriverApplication, mobileAgentId, Invariants.STUN_SERVER2,
				Invariants.STUN_SERVER2_PORT);
	}

	@Override
	public void onDnsAddressResolving() {
		long requestId = getA().getLong(REQUEST_ID, ISid.INVALID_COMPONENT_SESSIONS_ID);
		if (requestId == ISid.INVALID_COMPONENT_SESSIONS_ID) {
			return;
		}
		long remoteControlMobileAgentId = getA().getLong(REMOTE_CONTROL_MOBILE_AGENT_ID, IMobileAgent.INVALID_ID);
		if (remoteControlMobileAgentId == IMobileAgent.INVALID_ID) {
			return;
		}
		setupRemoteListener(requestId, remoteControlMobileAgentId);
	}

	@Override
	public void shutdown() {
		release();
	}

	@Override
	public Object exec(UserCommandTypesEnum userCommandType, byte[] args,
			final long requestId, final long remoteControlMobileAgentId) {
		if (userCommandType == null) {
			throw new IllegalStateException("Unknown command type!");
		}
		AppStatesEnum appState = getA().getAppState();
		CarStatesEnum carState = getA().getCarState();
		switch (userCommandType) {
		case ACA:
			getA().getUserCommandsExecutor().switchOnWatch();
			//Setup remote listener
			setupRemoteListener(requestId, remoteControlMobileAgentId);
			appState = AppStatesEnum.CAC;
			//Persistence of connection, for restore after restart
			getA().setLong(REQUEST_ID, requestId);
			getA().setLong(REMOTE_CONTROL_MOBILE_AGENT_ID, remoteControlMobileAgentId);
			break;
		case SOCA:
			Log.d(TAG, "switch-off-watch[chat-command-exec]");
			getA().getUserCommandsExecutor().switchOffWatch();
			appState = AppStatesEnum.ASO;
			break;
		case WAY:
			break;
		case RL:
			final String phoneNumber = getA().getString(MainActivity.PHONE_NUMBER_EDITOR, "");
			if (phoneNumber == "") {
				return null;
			}
			if (phoneNumber.trim().length() <= 1) {
				return null;
			}
			getA().runPhoneTask(new Runnable() {

				@Override
				public void run() {
					try {
						Thread.sleep(TimeUnit.SECONDS.toMillis(1));
					} catch (InterruptedException e){
						Log.wtf(TAG, e);
						return;
					}
					getA().waitForCellNetworkConnection(TimeUnit.SECONDS.toMillis(30));
					WatchdogService.makeOutboundCall(getA(), phoneNumber, 0);
				}
				
			});
			break;
		case MP:
			int cameraId = getCameraId(args);
			WatchdogService.makePreview(getA(), remoteControlMobileAgentId, requestId, cameraId);
			return ChatConnectionWithServerRoleTask.NO_SEND_VALUE;
		case ES:
			ControlModsEnum targetControlMode = getTargetControlMode(args);
			if (targetControlMode == null) {
				return null;
			}
			getA().setControlMode(targetControlMode);
			getA().makeInternetConnectorIfCellNetworkUnavailableOrControlModeIsInternet(targetControlMode);
			ControlModsEnum realControlMode = getA().getControlMode();
			switch (realControlMode) {
			case INTERNET:
				break;
			case PHONE_CALLS:
			case SMS:
				getA().shutdownRemoteUserCommandsExecutor();
				break;
			}
			//TODO: refactoring for return DBLinks.link(realControlMode) code.
			MobileAgentStateDescription code = new MobileAgentStateDescription();
			code.setDescription(realControlMode.getGlobalKey());
			return code;
		case GI:
			int cc = Camera.getNumberOfCameras();
			DeviceInfo deviceInfo = new DeviceInfo();
			deviceInfo.setCamerasCount(cc);
			return deviceInfo;
		}

		return getMobileAgentStateForRemoteControl(appState, carState);
	}

	private ControlModsEnum getTargetControlMode(byte[] args) {
		short controlModeId = IMobileAgentProfile.DEFAULT_CONTROL_MODE_ID;
		try {
			controlModeId = NumericTools.bytesToShort(args);
		} catch (NumberFormatException e) {
			Log.wtf(TAG, e);
			return null;
		}
		ControlModsEnum controlMode = DBLinks.unlinkControlMode(controlModeId);
		return controlMode;
	}

	private int getCameraId(byte[] args) {
		int cameraId = CameraInfo.CAMERA_FACING_BACK;
		try {
			cameraId = NumericTools.bytesToInt(args);
		} catch (NumberFormatException e) {
			Log.wtf(TAG, e);
		}
		return cameraId;
	}

	private void setupRemoteListener(final long requestId,
			final long remoteControlMobileAgentId) {
		long timerExpired = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5);
		WatchdogService watchdogService = getA().getWatchdogService();
		while( watchdogService == null && timerExpired >= System.currentTimeMillis()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Log.wtf(TAG, e);
				return;
			}
			watchdogService = getA().getWatchdogService();
		}
		
		if (watchdogService == null)  {
			Log.w(TAG, "can-not-setup-remote-listener[timeout]");
			return;
		}
		
		watchdogService.setRemoteListener(new MobileAgentStateListener() {

			@Override
			public void onChangeMobileAgentState() {
				if (chatConnectionTask == null) {
					return;
				}
				getConnectorTasksExecutorService().execute(new Runnable() {

					@Override
					public void run() {
						try {
							Object object = getMobileAgentStateForRemoteControl(getA().getAppState(), getA().getCarState());
							chatConnectionTask.sendResponse(remoteControlMobileAgentId, requestId, object);
						} catch (Throwable th) {
							Log.wtf(TAG, th);
						}
					}
					
				});
			}

			@Override
			public void onChangeStateDescription(final String description) {
				if (chatConnectionTask == null) {
					return;
				}
				getConnectorTasksExecutorService().execute(new Runnable() {

					@Override
					public void run() {
						try {
							Object object = getMobileAgentStateDescription(description);
							chatConnectionTask.sendResponse(remoteControlMobileAgentId, requestId, object);
						} catch (Throwable th) {
							Log.wtf(TAG, th);
						}
					}
				});

			}
			
		});
	}

	protected Object getMobileAgentStateForRemoteControl(
			AppStatesEnum appState, CarStatesEnum carState) {
		WatchdogService watchdogService = getA().getWatchdogService();
		boolean gpsOn = false;
		int gsmBitErrorRate = IMobileAgentState.UNKNOWN_GSM_BIT_ERROR_RATE;
		int gsmSignalStrength = IMobileAgentState.UNKNOWN_GSM_SIGNAL_STRENGTH;
		int lat = ILocation.UNKNOWN_LATITUDE;
		int lon = ILocation.UNKNOWN_LONGITUDE;
		int alt = ILocation.UNKNOWN_ALTITUDE;
		int acc = ILocation.UNKNOWN_ACCURACY;
		int mcc = ILocation.UNKNOWN_MOBILE_COUNTRY_CODE;
		int mnc = ILocation.UNKNOWN_MOBILE_NETWORK_CODE;
		int lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
		int cid = ILocation.UNKNOWN_CELL_ID;
		if (watchdogService != null) {
			gsmBitErrorRate = watchdogService.getGsmBitErrorRate();
			gsmSignalStrength = watchdogService.getGsmSignalStrength();
			watchdogService.updateCellLocation();
			lat = watchdogService.getLat();
			lon = watchdogService.getLon();
			alt = watchdogService.getAlt();
			acc = watchdogService.getAcc();
			mcc = watchdogService.getMcc();
			mnc = watchdogService.getMnc();
			lac = watchdogService.getLac();
			cid = watchdogService.getCid();
			gpsOn = watchdogService.isGpsOn();
		} else {
			Log.w(TAG, "poor-mobile-agent-state");
		}

		MobileAgentWithState state = (MobileAgentWithState) Hardwares.getState(
				getA(), appState, carState, gsmBitErrorRate, gsmSignalStrength,
				lat, lon, alt, acc, mcc, mnc, lac, cid);
		// TODO: resolve small aperture of mobile Megafone network issue.
		ShortMobileAgentState shortState = new ShortMobileAgentState();
		shortState.setAppStateId(state.getAppStateId());
		shortState.setLastLocation((Location) state.getLastLocation());
		shortState.setBatteryPct(state.getBatteryPct());
		shortState.setBatteryStatus(state.getBatteryStatus());
		shortState.setGsmSignalStrength(state.getGsmSignalStrength());
		shortState.setGpsOn(gpsOn);
		shortState.setIid(getA().getInt(MainActivity.LAST_WATCHDOG_STATUS_IID, CCRI.getInvariantId(R.string.three_points)));
		return shortState;
	}
	
	protected Object getMobileAgentStateDescription(String description) {
		MobileAgentStateDescription mobileAgentStateDescription = new MobileAgentStateDescription();
		mobileAgentStateDescription.setDescription(description);
		return mobileAgentStateDescription;
	}

	public HipdriverApplication getA() {
		return (HipdriverApplication) getContext();
	}

	@Override
	protected ChatConnectionWithServerRoleTask createChatConnectionTask(
			InetAddress chatServerAddress, int chatServerPort) {
		return new ChatConnectionWithServerRoleTask(getContext(), getMobileAgentId(), chatServerAddress, chatServerPort, this);
	}

	@Override
	public void sendResponse(long mobileAgentIdTo, long requestId, Object object) {
		if (chatConnectionTask == null) {
			Log.w(TAG, "can-not-send-response[chat-connection-task is null]");
			return;
		}
		try {
			chatConnectionTask.sendResponse(mobileAgentIdTo, requestId, object);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

}
