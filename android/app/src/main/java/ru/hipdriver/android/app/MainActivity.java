package ru.hipdriver.android.app;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.validator.routines.EmailValidator;
import org.codehaus.jackson.Base64Variants;
import org.codehaus.jackson.JsonParseException;

import ru.hipdriver.android.app.help.ShortHelpActivity;
import ru.hipdriver.android.i.AppFaceListener;
import ru.hipdriver.android.util.PasswordGenerator;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.support.AppFacesEnum;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.i.support.ControlModsEnum;
import ru.hipdriver.util.EncDecTools;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends HipdriverActivity implements IDebugMessageConsumer, IUserCommandsExecutor {

    private static final int MAX_COUNT_OF_CHARACTERS_IN_HEADER = 9;

    private static final String TAG = TextTools.makeTag(MainActivity.class);

    public static final String STACK_TRACE_FILE_NAME = "e.last.txt";

    public static final String PHONE_NUMBER_EDITOR = "phone_number_editor";
    public static final String MOBILE_AGENT_NAME_EDITOR = "mobile_agent_name_editor";
    public static final String USER_NAME_EDITOR = "user_name_editor";
    public static final String PASSWORD_EDITOR = "password_editor";
    public static final String ENERGY_SAVING_MODE_CHECKBOX = "energy_saving_mode_checkbox";
    public static final String LAST_WATCHDOG_STATUS = "last_watchdog_status";
    public static final String LAST_WATCHDOG_STATUS_IID = "last_watchdog_status_iid";
    public static final String FIRST_START_KEY = "first_start_key";

    protected static final String SENSITIVITY_MODE = "sensitivity_mode";

    protected static final String SELECTED_DEVICE = "selected_device";
    public static final String SELECTED_DEVICE_ID = "selected_device_id";
    protected static final String SELECTED_DEVICE_NAME = "selected_device_name";
    protected static final String SELECTED_DEVICE_PHONE_NUMBER = "selected_device_phone_number";
    protected static final String SELECTED_DEVICE_LAT = "selected_device_lat";
    protected static final String SELECTED_DEVICE_LON = "selected_device_lat";
    protected static final String SELECTED_DEVICE_ACC = "selected_device_acc";

    protected static final String CONTROL_MODE = "control_mode";

    protected static final int CONTACT_PICKER_REQUEST = 43133;

    public static final String SIGNUP_INTENT = "ru.hipdriver.action.SIGNUP";
    public static final String SETTINGS_INTENT = "ru.hipdriver.action.SETTINGS";

    protected static final String EXTRA_SWITCH_TO_CONTROL_MODE = "extra_switch_to_control_mode";

    protected static final int SWITCH_TO_CONTROL_MODE_REQUEST = 28571;

    private static final long TIME_TO_READ_TEXT_ON_FIRST_SCREEN = TimeUnit.SECONDS.toMillis(11);

    public static final String IS_AUTO_SIGN_IN_UP_USER_BREAK = "is_auto_sign_in_up_user_break";

    public static final String NOTIFICATION_NOT_DELIVERED = "notification_not_delivered";
    private static final String DO_NOT_SHOW_NOTIFICATION_MESSAGE_REMOTE_CONTROL = "rcf_do_not_show_notification_message";
    private static final String DO_NOT_SHOW_NOTIFICATION_MESSAGE_CAR_ALARMS = "caf_do_not_show_notification_message";

    public static final String SCREEN_ORIENTATION = "screen_orientation";

    private HandlerThread signupThread;
    private UITask signupTask;

    //private Map<Integer, Integer> scrollPositions = new HashMap<Integer, Integer>();

    private RemoteControlFragment remoteControlFragment;
    private Fragment mapFragment;

    private HipdriverBroadcastReceiver signupReceiver;
    private HipdriverBroadcastReceiver settingsReceiver;

    private AppFaceListener appFaceListener;

    private Timer timer;

    private interface OnCompleteRequest {
        void requestComplete(Boolean isRegistered);
    }

    private class FirstStartInfo {
        String email;
        Boolean isRegistered;
        boolean isGoogleAccount;
        OnCompleteRequest onCompleteRequest;
        boolean isUserBreak = getA().getBoolean(IS_AUTO_SIGN_IN_UP_USER_BREAK, false);

        String encodedPasswordHash;

        public boolean findSameEmail(Account[] accounts) {
            if (accounts == null) {
                return false;
            }
            if (accounts.length == 0) {
                return false;
            }
            for (Account account : accounts) {
                if (account.name.equalsIgnoreCase(email)) {
                    return true;
                }
                ;
            }
            return false;
        }

    }

    private FirstStartInfo firstStartInfo;

    private Map<Integer, Fragment> resultConsumers = new HashMap<Integer, Fragment>(3);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getA().setMainActivity(this);

        //Stop application if it user requirement
        if (getA().getBoolean(ExitQuestionActivity.STOP_APPLICATION_USER_REQUEST, false)) {
            getA().setBoolean(ExitQuestionActivity.STOP_APPLICATION_USER_REQUEST, false);
            //Sane check application face
            if (getA().getAppFace() == AppFacesEnum.REMOTE_CONTROL) {
                //Savety terminate all services
                getA().onTerminate();
                //Wait for termination
                //TODO: place for said bay-bay for dear user
                try {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(1));
                } catch (InterruptedException e) {
                }
                //Exit
                finish();
                return;
            }
        }

        //Splash screen or/and prepare first screen
        splashScreenAndPrepareFirstScreen();

        //TL Version START -->
        //if (getA().isChargeableInstalled()) {
        //	setContentView(R.layout.err_screen);
        //	TextView errTitleTextView = (TextView) findViewById(R.id.err_title_text);
        //	errTitleTextView.setText(Html.fromHtml(getString(R.string.dear_friend)));
        //	TextView errTextView1 = (TextView) findViewById(R.id.err_text1);
        //	errTextView1.setText(Html.fromHtml(getString(R.string.please_remove_uncharged_app1)));
        //	TextView errTextView2 = (TextView) findViewById(R.id.err_text2);
        //	errTextView2.setText(Html.fromHtml(getString(R.string.please_remove_uncharged_app2)));
        //	return;
        //}
        //<-- TL Version END

        setContentView(R.layout.main_screen);

        //Setup surface for cam preview
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.main_screen_$back_cam$_surface_view);
        SurfaceHolder holder = surfaceView.getHolder();
        holder.setFormat(PixelFormat.RGBA_8888);
        holder.setFixedSize(640, 480);
        surfaceView.setVisibility(View.GONE);
        //

        setupFaceSelector();

        getA().setDebugMessageConsumer(this);
        getA().setUserCommandsExecutor(this);

        showStatusBar();
        setupDebugElements();
        skipAutoFocus();

        //getA().setWakeLock();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Intent intent = getIntent();
        //Additional salt
        String message = intent.getStringExtra(RuntimeExceptionHandler.EXTRA_UNCAUGHT_EXCEPTION);
        if (message != null) {
            Log.d(TAG, "has-uncaught-exception-in-previous-life-cycle");
            String stackTrace = intent.getStringExtra(RuntimeExceptionHandler.EXTRA_STACK_TRACE);
            //Send e-mail to developer
            int userId = getA().getInt(ServicesConnector.USER_ID_KEY, 0);
            long mobileAgentId = getA().getLong(ServicesConnector.MOBILE_AGENT_ID_KEY, 0);
            String applicationVersion = Hardwares.getApplicationVersion(getA());
            StringBuilder bodyText = new StringBuilder(Hardwares.getAndroidInfo());
            bodyText.append('\n').append('\n').append(message).append('\n').append('\n').append(stackTrace);
            String subject = String.format("EXCEPTION FROM [%s][%s] %s", userId, mobileAgentId, applicationVersion);
            UITaskFactory.sendEmail(getA(), HipdriverApplication.TECH_SUPPORT_EMAIL, subject, bodyText.toString(), null, 0);
            //Send analytics
            AnalyticsAdapter.trackException(stackTrace);
            Log.d(TAG, "store-error-into-local-folder-if-it-possible");
            new FileTools(getA()).safetySaveBufferIntoFile(stackTrace.getBytes(), STACK_TRACE_FILE_NAME);
            debugStatus(message);
        }
        getA().restoreApplicationState(intent);

        //Setup settings receiver
        activateSettingsReceiverIfSignupComplete();

        initSurfacesForCameras();
    }

    private void initSurfacesForCameras() {
        if (Camera.getNumberOfCameras() > 0) {
            //Setup surface for back-cam preview
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.main_screen_$back_cam$_surface_view);
            SurfaceHolder holder = surfaceView.getHolder();
            holder.setFormat(PixelFormat.RGBA_8888);
            holder.setFixedSize(640, 480);
            surfaceView.setVisibility(View.GONE);
            //
        }
        if (Camera.getNumberOfCameras() > 1) {
            //Setup surface for back-cam preview
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.main_screen_$front_cam$_surface_view);
            SurfaceHolder holder = surfaceView.getHolder();
            holder.setFormat(PixelFormat.RGBA_8888);
            holder.setFixedSize(640, 480);
            surfaceView.setVisibility(View.GONE);
            //
        }
    }

    protected void splashScreenAndPrepareFirstScreen() {
        boolean firstSignup = getA().getBoolean(ServicesConnector.FIRST_SIGN_UP_KEY);
        if (firstSignup) {
            showSplashScreen();

            //TODO: start request for check e-mail
            updateFirstStartInfo();
            UITaskFactory.checkEmail(getA(), new After() {

                @Override
                public void success(Object returnValue) {
                    if (!isBoolean(returnValue)) {
                        return;
                    }
                    try {
                        if (!((Boolean) returnValue)) {
                            firstStartInfo.isRegistered = false;
                            return;
                        }

                        firstStartInfo.isRegistered = true;
                    } finally {
                        if (firstStartInfo.onCompleteRequest != null) {
                            firstStartInfo.onCompleteRequest.requestComplete((Boolean) returnValue);
                        }
                    }
                }

            }, firstStartInfo.email);
        } else if (getA().getAppFace() == AppFacesEnum.REMOTE_CONTROL && !getA().isNotFirstStartOfMainActivity()) {
            showSplashScreen();
            getA().setNotFirstStartOfMainActivity(true);
        }
    }

    private void activateSettingsReceiverIfSignupComplete() {
        //Nothing do if application not to be authorized
        if (!getA().isSignup()) {
            return;
        }
        if (settingsReceiver != null) {
            return;
        }
        synchronized (this) {
            if (settingsReceiver != null) {
                return;
            }
            settingsReceiver = new HipdriverBroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent == null) {
                        return;
                    }
                    String action = intent.getAction();
                    if (!MainActivity.SETTINGS_INTENT.equals(action)) {
                        return;
                    }

                    Bundle extras = intent.getExtras();
                    if (extras == null) {
                        return;
                    }
                    for (String key : extras.keySet()) {
                        if (HipdriverApplication.APP_FACE.equals(key)) {
                            String appFaceText = extras.getString(key);
                            final AppFacesEnum appFace = AppFacesEnum.parse(appFaceText);
                            if (appFace == null) {
                                return;
                            }
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    MainActivity.this.getA().setAppFace(appFace);
                                    showAppFace();
                                }
                            });
                            return;
                        }
                    }
                }
            };
            this.registerReceiver(settingsReceiver, new IntentFilter(MainActivity.SETTINGS_INTENT));
        }
    }

    private boolean isBoolean(Object object) {
        if (object == null) {
            return false;
        }
        if (!object.getClass().equals(boolean.class) &&
                !object.getClass().equals(Boolean.class)) {
            return false;
        }
        return true;
    }

    protected void setupFaceSelector() {
        boolean firstSignup = getA().getBoolean(ServicesConnector.FIRST_SIGN_UP_KEY);
        if (firstSignup) {
            return;
        }
        final ImageButton faceSelectButton = (ImageButton) findViewById(R.id.face_select_button);
        if (faceSelectButton == null) {
            return;
        }
        if (faceSelectButton.getVisibility() == View.VISIBLE) {
            return;
        }
        faceSelectButton.setVisibility(View.VISIBLE);
        //faceSelectButton.setOnTouchListener(new View.OnTouchListener() {
        //
        //	@Override
        //	public boolean onTouch(View v, MotionEvent event) {
        //		return true;
        //	}
        //});

        faceSelectButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                fireClickTopRightButton(faceSelectButton);
            }
        });

    }

    protected FaceSelectCollectionPagerAdapter getFaceSelectCollectionPagerAdapter(
            View view) {
        if (!(view instanceof ViewPager)) {
            Log.w(TAG, "can-not-get-face-select-collection-pager-adapter[view is null or not view-pager]");
            return null;
        }
        ViewPager viewPager = (ViewPager) view;
        PagerAdapter adapter = viewPager.getAdapter();
        if (!(adapter instanceof FaceSelectCollectionPagerAdapter)) {
            Log.w(TAG, "can-not-get-face-select-collection-pager-adapter[class cast issue]");
            return null;
        }
        return (FaceSelectCollectionPagerAdapter) adapter;
    }

    protected void fireOnExitFace() {
        if (appFaceListener == null) {
            return;
        }
        appFaceListener.onExitFace();
    }

    protected void showFaceSelect() {
        if (findViewById(R.id.face_select_pager) != null) {
            return;
        }
        Log.d(TAG, "show-face-select-screen");
        // Layout initialization
        ViewGroup mainBody = (ViewGroup) findViewById(R.id.main_body);
        mainBody.removeAllViews();
        View faceSelectScreen = View.inflate(mainBody.getContext(),
                R.layout.face_select_screen, mainBody);
        faceSelectScreen.setVisibility(View.VISIBLE);
        // signUpScreen.postInvalidate();

        changePictogramBehavior();

        //Add pages
        ViewPager viewPager = (ViewPager) findViewById(R.id.face_select_pager);
        FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = new FaceSelectCollectionPagerAdapter(getA(), getSupportFragmentManager(), viewPager);
        viewPager.setAdapter(faceSelectCollectionPagerAdapter);

        getA().setBoolean(ServicesConnector.FIRST_SIGN_UP_KEY, false);
        setupFaceSelector();

        PagerTabStrip pagerTabStrip = (PagerTabStrip) findViewById(R.id.face_select_pager_tab_strip);
        //pagerTabStrip.setTabIndicatorColorResource(R.color.semi_transparrent);
        //TODO: refactoring
        pagerTabStrip.setTabIndicatorColorResource(R.color.pager_tab_strip_background);
        //SETUP spacing between tabs
        try {
            Field mMinTextSpacing = pagerTabStrip.getClass().getDeclaredField("mMinTextSpacing");
            mMinTextSpacing.setAccessible(true);
            mMinTextSpacing.set(pagerTabStrip, 0);
            pagerTabStrip.setTextSpacing(0);
        } catch (Throwable th) {
            Log.wtf(TAG, th);
        }
        //pagerTabStrip.setDrawFullUnderline(true);
        //pagerTabStrip.setPadding(0, 5, 0, 5);
    }


    private void setupDebugElements() {
        OnClickListener onClickListener = new OnClickListener() {
            private int clickCount;
            long lastClick;

            @Override
            public void onClick(View arg0) {
                //Reset long memory
                if (System.currentTimeMillis() - lastClick > 2000) {
                    clickCount = 0;
                }
                if (clickCount > 0 && clickCount % 7 == 0) {
                    getA().switchDebugMode();
                }
                clickCount++;
                lastClick = System.currentTimeMillis();
            }

        };
        //View view1 = findViewById(R.id.main_status);
        //view1.setOnClickListener(onClickListener);
        View view2 = findViewById(R.id.status_bar_text);
        if (view2 != null) {
            view2.setOnClickListener(onClickListener);
        }
    }

    private void skipAutoFocus() {
        View skipAutoFocus = findViewById(R.id.skip_auto_focus);
        if (skipAutoFocus == null) {
            return;
        }
        skipAutoFocus.setFocusableInTouchMode(true);
        skipAutoFocus.requestFocus();
        //Old phones
        //final InputMethodManager inputMethodManager = (InputMethodManager) context
        //        .getSystemService(Context.INPUT_METHOD_SERVICE);
        //inputMethodManager.showSoftInput(edittext, InputMethodManager.SHOW_IMPLICIT);

    }

    private void showSignupScreen() {
        if (findViewById(R.id.signup_button) != null) {
            return;
        }
        Log.d(TAG, "show-signup-screen");
        //Layout initialization
        ViewGroup mainBody = (ViewGroup) findViewById(R.id.main_body);
        mainBody.removeAllViews();
        View signUpScreen = View.inflate(mainBody.getContext(), R.layout.signup_screen, mainBody);
        signUpScreen.setVisibility(View.VISIBLE);
        //signUpScreen.postInvalidate();

        //Setup behavior
        Button signupButton = (Button) findViewById(R.id.signup_button);
        signupButton.setText(Html.fromHtml(getResources().getString(R.string.signup)));
        signupButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View p1) {
                debugStatus("");
                final String userName = readTextFrom(R.id.user_name_editor);
                final String password = readTextFrom(R.id.password_editor);
                if (isUserRegistered()) {
                    getA().setAppFace(AppFacesEnum.REMOTE_CONTROL);
                } else {
                    getA().setAppFace(AppFacesEnum.CAR_ALARMS);
                }
                signup(userName, password.getBytes());
            }

        });

        setTextTo(R.id.user_name_editor, USER_NAME_EDITOR);
        EditText userNameEditor = (EditText) findViewById(R.id.user_name_editor);
        userNameEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (v.getId() == R.id.user_name_editor && !hasFocus) {
                    hideSoftKeyboard(v);
                }
            }
        });
        userNameEditor.addTextChangedListener(new OnTextEdit() {

            @Override
            public void afterTextChanged(Editable s) {
                String mobileAgentName = s.toString();
                getA().setString(USER_NAME_EDITOR, mobileAgentName);
            }

        });

        setTextTo(R.id.password_editor, PASSWORD_EDITOR);
        EditText passwordEditor = (EditText) findViewById(R.id.password_editor);
        passwordEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (v.getId() == R.id.password_editor && !hasFocus) {
                    hideSoftKeyboard(v);
                }
                if (v.getId() == R.id.password_editor && hasFocus) {
                    showSoftKeyboard(v);
                }
            }
        });
        passwordEditor.addTextChangedListener(new OnTextEdit() {

            @Override
            public void afterTextChanged(Editable s) {
                String mobileAgentName = s.toString();
                getA().setString(PASSWORD_EDITOR, mobileAgentName);
            }

        });

        /**
         setTextTo(R.id.mobile_agent_name_editor, MOBILE_AGENT_NAME_EDITOR);
         EditText mobileAgentNameEditor = (EditText) findViewById(R.id.mobile_agent_name_editor);
         mobileAgentNameEditor.addTextChangedListener(new OnTextEdit() {

        @Override public void afterTextChanged(Editable s) {
        String mobileAgentName = s.toString();
        getA().setString(MOBILE_AGENT_NAME_EDITOR, mobileAgentName);
        getA().setMemoryVersion(IMobileAgent.MOBILE_AGENT_NAME_KEY);
        }

        });

         setTextTo(R.id.phone_number_editor, PHONE_NUMBER_EDITOR);
         EditText phoneNumberEditor = (EditText) findViewById(R.id.phone_number_editor);
         phoneNumberEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {

         public void onFocusChange(View v, boolean hasFocus){
         if(v.getId() == R.id.phone_number_editor && !hasFocus) {
         hideSoftKeyboard(v);
         //get current accelerometer track and send message
         //debugStatus("");
         }
         }
         });
         phoneNumberEditor.addTextChangedListener(new OnTextEdit() {

        @Override public void afterTextChanged(Editable s) {
        String phoneNumber = s.toString();
        getA().setString(PHONE_NUMBER_EDITOR, phoneNumber);
        getA().setMemoryVersion(IMobileAgent.ALERT_PHONE_NUMBER_KEY);
        }

        });
         **/
        TextView slogan1 = (TextView) findViewById(R.id.slogan1);
        Spanned text1 = Html.fromHtml(getResources().getString(R.string.slogan1));
        slogan1.setText(text1);
        slogan1.invalidate();
        TextView slogan2 = (TextView) findViewById(R.id.slogan2);
        Spanned text2 = Html.fromHtml(getResources().getString(R.string.slogan2));
        slogan2.setText(text2);
        slogan2.invalidate();

        //Hide debug status
        View statusBarFrame = (View) findViewById(R.id.status_bar_frame);
        if (statusBarFrame != null) {
            statusBarFrame.setVisibility(View.INVISIBLE);
        }

        //Disable settings button (top-right-button)
        final ImageButton faceSelectButton = (ImageButton) findViewById(R.id.face_select_button);
        faceSelectButton.setVisibility(View.GONE);

//		showFirstMessage();						
    }

    protected boolean isUserRegistered() {
        if (firstStartInfo == null) {
            return false;
        }
        if (firstStartInfo.isRegistered == null) {
            return false;
        }
        return firstStartInfo.isRegistered;
    }

    private void showFirstScreen() {
        if (findViewById(R.id.show_signinup_screen_button) != null) {
            return;
        }
        Log.d(TAG, "show-first-screen");
        //Layout initialization
        final ViewGroup mainBody = (ViewGroup) findViewById(R.id.main_body);
        mainBody.removeAllViews();
        View firstScreen = View.inflate(mainBody.getContext(), R.layout.first_screen, mainBody);
        firstScreen.setVisibility(View.VISIBLE);
        //signUpScreen.postInvalidate();

        //Update only if null, i.e. sane check
        if (firstStartInfo == null) {
            updateFirstStartInfo();
        }

        //Setup behavior
        final TextView emailHintText = (TextView) findViewById(R.id.email_hint_text);
        final EditText emailEditor = (EditText) findViewById(R.id.email_editor);

        final Iprbar autoSigninupIprbar = (Iprbar) findViewById(R.id.auto_signinup_iprbar);

        final View yesNoButtonsBlock = findViewById(R.id.yes_no_buttons_block);
        final Button noAnotherEmailButton = (Button) findViewById(R.id.no_another_email_button);
        String noAnotherEmailText = getResources().getString(R.string.no_another_email);
        noAnotherEmailButton.setText(Html.fromHtml(noAnotherEmailText));
        final Button yesThisEmailButton = (Button) findViewById(R.id.yes_this_email_button);
        String yesThisEmailText = getResources().getString(R.string.yes_this_email);
        yesThisEmailButton.setText(Html.fromHtml(yesThisEmailText));

        final View signInUpButtonsBlock = findViewById(R.id.sign_in_up_buttons_block);

        final Button showSigninupScreenButton = (Button) findViewById(R.id.show_signinup_screen_button);
        String showSignupScreenText = getResources().getString(R.string.show_signup_screen);
        showSigninupScreenButton.setText(Html.fromHtml(showSignupScreenText));

        if (firstStartInfo.isUserBreak) {
            yesNoButtonsBlock.setVisibility(View.GONE);
            signInUpButtonsBlock.setVisibility(View.VISIBLE);
            emailEditor.setEnabled(true);
            String text = getA().getString(USER_NAME_EDITOR, firstStartInfo.email);
            emailEditor.setText(text);
        } else {
            emailEditor.setEnabled(false);
            String text = getA().getString(USER_NAME_EDITOR, firstStartInfo.email);
            emailEditor.setText(text);
        }

        showSigninupScreenButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View p1) {
                debugStatus("");

                if (firstStartInfo == null) {
                    showSignupScreen();
                    return;
                }

                if (!firstStartInfo.isGoogleAccount && firstStartInfo.isRegistered != null && firstStartInfo.isRegistered) {
                    getA().setString(USER_NAME_EDITOR, firstStartInfo.email);
                    showSignupScreen();
                    return;
                }

                autoSigninup(mainBody, emailEditor,
                        signInUpButtonsBlock, autoSigninupIprbar,
                        yesNoButtonsBlock, true);
            }

        });

        noAnotherEmailButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Kill thread for auto-sign-in/up

                //User action state for first form
                //Default state for first form
                yesNoButtonsBlock.setVisibility(View.GONE);
                signInUpButtonsBlock.setVisibility(View.VISIBLE);
                emailEditor.setEnabled(true);
                String text = getA().getString(USER_NAME_EDITOR, firstStartInfo.email);
                emailEditor.setText(text);
                firstStartInfo.isUserBreak = true;
                getA().setBoolean(IS_AUTO_SIGN_IN_UP_USER_BREAK, true);
            }
        });

        OnCompleteRequest onCompleteRequest = new OnCompleteRequest() {

            @Override
            public void requestComplete(final Boolean isRegistered) {
                //Request fail nothing do
                if (isRegistered == null) {
                    return;
                }
                emailHintText.post(new Runnable() {
                    @Override
                    public void run() {
                        updateUIStateOnFirstScreen(emailHintText, emailEditor,
                                signInUpButtonsBlock, yesNoButtonsBlock,
                                isRegistered);
                    }
                });
            }
        };
        firstStartInfo.onCompleteRequest = onCompleteRequest;

        yesThisEmailButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (firstStartInfo.isUserBreak) {
                    return;
                }
                autoSigninup(mainBody, emailEditor,
                        signInUpButtonsBlock, autoSigninupIprbar,
                        yesNoButtonsBlock, false);
            }

        });

        //If too-late start in handle mode
        if (firstStartInfo.isRegistered != null) {
            onCompleteRequest.requestComplete(firstStartInfo.isRegistered);
        }

        //E-mail editor events
        emailEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (v.getId() == R.id.email_editor && !hasFocus) {
                    hideSoftKeyboard(v);
                }
            }
        });
        emailEditor.addTextChangedListener(new OnTextEdit() {

            @Override
            public void afterTextChanged(Editable s) {
                String email = s.toString();
                //TODO: find instead match
                if (!email.equalsIgnoreCase(firstStartInfo.email) &&
                        EmailValidator.getInstance().isValid(email)) {
                    //TODO: cache requests
                    UITaskFactory.checkEmail(getA(), new After() {

                        @Override
                        public void success(Object returnValue) {
                            if (!isBoolean(returnValue)) {
                                return;
                            }
                            boolean isRegistered = (Boolean) returnValue;
                            firstStartInfo.isRegistered = isRegistered;
                            //Get google accounts
                            AccountManager accountManager = AccountManager.get(MainActivity.this);
                            Account[] accounts = accountManager.getAccountsByType("com.google");
                            firstStartInfo.isGoogleAccount = firstStartInfo.findSameEmail(accounts);
                            updateUIStateOnFirstScreen(emailHintText, null,
                                    signInUpButtonsBlock, yesNoButtonsBlock,
                                    isRegistered);
                        }

                    }, email);
                }
                firstStartInfo.email = email;
                getA().setString(USER_NAME_EDITOR, firstStartInfo.email);
            }

        });

        //Start timer for auto-authorization or auto-registration
        if (firstStartInfo.isGoogleAccount) {
            getTimer().schedule(new TimerTask() {

                @Override
                public void run() {
                    if (firstStartInfo.isUserBreak) {
                        return;
                    }
                    autoSigninup(mainBody, emailEditor,
                            signInUpButtonsBlock, autoSigninupIprbar,
                            yesNoButtonsBlock, false);
                }

            }, TIME_TO_READ_TEXT_ON_FIRST_SCREEN);

        }

        //Disable settings button (top-right-button)
        final ImageButton faceSelectButton = (ImageButton) findViewById(R.id.face_select_button);
        faceSelectButton.setVisibility(View.GONE);

        //Activate signup receiver if receiver not activated
        //Signup receiver use for auto-authorization after install of application
        activateSignupReceiverIfNotActive();
    }

    private void updateFirstStartInfo() {
        if (firstStartInfo == null) {
            firstStartInfo = new FirstStartInfo();
        }
        if (firstStartInfo.email != null && Patterns.EMAIL_ADDRESS.matcher(firstStartInfo.email).find()) {
            return;
        }
        //Get google accounts
        AccountManager accountManager = AccountManager.get(this);
        Account[] accounts = accountManager.getAccountsByType("com.google");
        firstStartInfo.isGoogleAccount = accounts.length > 0;
        String email1 = getA().getString(USER_NAME_EDITOR, "");
        String email2 = chooseEmail(accounts);
        //TODO: use find instead matches
        if (Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {
            firstStartInfo.email = email1;
            firstStartInfo.isGoogleAccount = firstStartInfo.findSameEmail(accounts);
            return;
        }
        if (email2 == null) {
            firstStartInfo.email = "";
            firstStartInfo.isGoogleAccount = false;
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email2).find()) {
            firstStartInfo.email = "";
            firstStartInfo.isGoogleAccount = false;
            return;
        }
        firstStartInfo.email = email2;
        firstStartInfo.isGoogleAccount = true;
    }

    private void updateUIStateOnFirstScreen(
            final TextView emailHintText,
            final EditText emailEditor,
            final View signInUpButtonsBlock,
            final View yesNoButtonsBlock,
            final Boolean isRegistered) {
        String showSignupScreenText;
        if (isRegistered) {
            showSignupScreenText = getResources().getString(R.string.show_signin_screen);
            if (firstStartInfo.isGoogleAccount) {
                emailHintText.setText(getResources().getString(R.string.email_hint_case_authomatic_authorization));
            } else {
                emailHintText.setText(getResources().getString(R.string.email_hint_case_break_authomatic_authorization));
            }
        } else {
            showSignupScreenText = getResources().getString(R.string.show_signup_screen);
            if (firstStartInfo.isGoogleAccount) {
                emailHintText.setText(getResources().getString(R.string.email_hint_case_authomatic_registration));
            } else {
                emailHintText.setText(getResources().getString(R.string.email_hint_case_break_authomatic_registration));
            }
        }
        final Button showSigninupScreenButton = (Button) findViewById(R.id.show_signinup_screen_button);
        showSigninupScreenButton.setText(showSignupScreenText);

        if (emailEditor == null) {
            return;
        }

        if (firstStartInfo.isGoogleAccount) {
            //Default state for first form with google account
            emailEditor.setEnabled(false);
            yesNoButtonsBlock.setVisibility(View.VISIBLE);
            signInUpButtonsBlock.setVisibility(View.GONE);
        } else {
            //Default state for first form without google account
            emailEditor.setEnabled(true);
            yesNoButtonsBlock.setVisibility(View.GONE);
            signInUpButtonsBlock.setVisibility(View.VISIBLE);
        }
        if (firstStartInfo.isUserBreak) {
            emailEditor.setEnabled(true);
            yesNoButtonsBlock.setVisibility(View.GONE);
            signInUpButtonsBlock.setVisibility(View.VISIBLE);
            return;
        }
        emailEditor.setText(firstStartInfo.email);
    }

    protected void autoSigninup(final ViewGroup mainBody,
                                final EditText emailEditor,
                                final View signInUpButtonsBlock,
                                final Iprbar autoSigninupIprbar,
                                final View yesNoButtonsBlock,
                                final boolean ignoreUserBreak) {
        final String email = firstStartInfo.email;

        if (!ignoreUserBreak && firstStartInfo.isUserBreak) {
            return;
        }

        mainBody.post(new Runnable() {

            @Override
            public void run() {
                if (!ignoreUserBreak && firstStartInfo.isUserBreak) {
                    return;
                }

                yesNoButtonsBlock.setVisibility(View.GONE);
                signInUpButtonsBlock.setVisibility(View.GONE);
                emailEditor.setEnabled(false);
                emailEditor.setText(email);
                autoSigninupIprbar.setVisibility(View.VISIBLE);
            }

        });

        UITaskFactory.encodedPasswordHashForEmail(getA(), new After() {

            @Override
            public void success(Object returnValue) {
                if (!ignoreUserBreak && firstStartInfo.isUserBreak) {
                    failback(mainBody, emailEditor,
                            signInUpButtonsBlock,
                            autoSigninupIprbar,
                            yesNoButtonsBlock);
                    return;
                }

                if (!firstStartInfo.isGoogleAccount && returnValue != null) {
                    failback(mainBody, emailEditor,
                            signInUpButtonsBlock,
                            autoSigninupIprbar,
                            yesNoButtonsBlock);
                    return;
                }

                //Registration case
                if (returnValue == null) {
                    //Auto registration
                    //autoregister(email);
                    String username = TextTools.extractUserName(email);
                    final String password = new PasswordGenerator().next(7);
                    UITaskFactory.registerNewEmail(getA(), new After() {

                        @Override
                        public void success(Object returnValue) {
                            if (!ignoreUserBreak && firstStartInfo.isUserBreak) {
                                failback(mainBody, emailEditor,
                                        signInUpButtonsBlock,
                                        autoSigninupIprbar,
                                        yesNoButtonsBlock);
                                return;
                            }
                            if (returnValue == null) {
                                failback(mainBody, emailEditor,
                                        signInUpButtonsBlock,
                                        autoSigninupIprbar,
                                        signInUpButtonsBlock);
                                return;
                            }
                            if (!isBoolean(returnValue) || !((Boolean) returnValue)) {
                                failback(mainBody, emailEditor,
                                        signInUpButtonsBlock,
                                        autoSigninupIprbar,
                                        yesNoButtonsBlock);
                                return;
                            }
                            //Success registration
                            getA().setString(PASSWORD_EDITOR, password);
                            showSignupScreen();
                            signup(email, password.getBytes());

                            String message = getResources().getString(R.string.auto_registration_success_message, email);
                            getA().showMessage(message, null, null);
                        }

                        @Override
                        public void failure(int returnCode, String failureReason) {
                            if (!ignoreUserBreak && firstStartInfo.isUserBreak) {
                                return;
                            }
                            failback(mainBody, emailEditor,
                                    signInUpButtonsBlock,
                                    autoSigninupIprbar,
                                    yesNoButtonsBlock);
                        }

                    }, username, email, password);
                } else {
                    String encodedPasswordHash = String.valueOf(returnValue);
                    byte[] encodedHash;
                    try {
                        encodedHash = EncDecTools.getBinaryValue(Base64Variants.MIME_NO_LINEFEEDS, encodedPasswordHash);
                        byte[] drupalPasswordHash = EncDecTools.decodePasswordHash(encodedHash);
                        showSignupScreen();
                        signup(email, drupalPasswordHash);
                    } catch (JsonParseException e) {
                        Log.w(TAG, e);
                        failback(mainBody, emailEditor,
                                signInUpButtonsBlock,
                                autoSigninupIprbar,
                                yesNoButtonsBlock);
                    }
                }
            }

            protected void failback(final ViewGroup mainBody,
                                    final EditText emailEditor,
                                    final View signInUpButtonsBlock,
                                    final Iprbar autoSigninupIprbar,
                                    final View yesNoButtonsBlock) {
                mainBody.post(new Runnable() {

                    @Override
                    public void run() {
                        yesNoButtonsBlock.setVisibility(View.GONE);
                        signInUpButtonsBlock.setVisibility(View.VISIBLE);
                        emailEditor.setEnabled(true);
                        autoSigninupIprbar.setVisibility(View.GONE);
                    }

                });
            }

            @Override
            public void failure(int returnCode, String failureReason) {
                failback(mainBody, emailEditor,
                        signInUpButtonsBlock,
                        autoSigninupIprbar,
                        yesNoButtonsBlock);
            }

        }, email);
        //Get hash for e-mail
        //If hash == null
        //Run registration
        //If hash != null
        //Run authorization
    }

    private String chooseEmail(Account[] accounts) {
        if (accounts.length == 0) {
            return "";
        }
        if (accounts.length == 1) {
            return accounts[0].name;
        }
        //TODO: first human-named e-mail
        return accounts[accounts.length - 1].name;
    }

    private void activateSignupReceiverIfNotActive() {
        //Nothing do if application always be authorized
        if (getA().isSignup()) {
            return;
        }
        if (signupReceiver != null) {
            return;
        }
        synchronized (this) {
            if (signupReceiver != null) {
                return;
            }
            signupReceiver = new HipdriverBroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent == null) {
                        return;
                    }
                    String action = intent.getAction();
                    if (!MainActivity.SIGNUP_INTENT.equals(action)) {
                        return;
                    }
                    final String userName = (String) intent.getExtras().get(USER_NAME_EDITOR);
                    final String password = (String) intent.getExtras().get(PASSWORD_EDITOR);
                    if (userName == null || userName.isEmpty()) {
                        Log.d(TAG, "no-user-name");
                        return;
                    }
                    if (password == null || password.isEmpty()) {
                        Log.d(TAG, "no-password");
                        return;
                    }
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            showSignupScreen();
                            EditText userNameEditor = (EditText) findViewById(R.id.user_name_editor);
                            EditText passwordEditor = (EditText) findViewById(R.id.password_editor);
                            Button signupButton = (Button) findViewById(R.id.signup_button);

                            userNameEditor.setText(userName);
                            passwordEditor.setText(password);
                            signupButton.performClick();
                        }
                    });
                }
            };
            this.registerReceiver(signupReceiver, new IntentFilter(MainActivity.SIGNUP_INTENT));
        }
    }

    protected void disableAllChildsInViewGroup(ViewGroup viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(false);
        }
    }

    protected void enableAllChildsInViewGroup(ViewGroup viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(true);
        }
    }

    protected String readTextFrom(int editorId) {
        EditText editText = (EditText) findViewById(editorId);
        if (editText == null) {
            throw new IllegalStateException("Resource collected or not created.");
        }
        return editText.getText().toString();
    }

    protected void setTextTo(int editorId, String preferencesKey) {
        EditText editText = (EditText) findViewById(editorId);
        editText.setText(getA().getString(preferencesKey, ""));
    }

    protected void setTextTo(int editorId, int stringId, Object... args) {
        TextView textView = (TextView) findViewById(editorId);
        String textSeed = getResources().getString(stringId);
        StringBuilder text = new StringBuilder(textSeed);
        for (Object arg : args) {
            text.append(arg);
        }
        textView.setText(text.toString());
    }

    private void showCarAlarmsScreen() {
        View checkView = findViewById(R.id.switch_on_button);
        if (checkView != null) {
            invalidateSwitcherState(checkView);
            return;
        }
        Log.d(TAG, "show-car-alarms-screen");
        //Layout initialization
        ViewGroup mainBody = (ViewGroup) findViewById(R.id.main_body);
        mainBody.removeAllViews();
        View watchdogScreen;
        if (getA().getInt(SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_NOSENSOR) == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            watchdogScreen = View.inflate(mainBody.getContext(), R.layout.car_alarms_screen_landscape, mainBody);
        } else {
            watchdogScreen = View.inflate(mainBody.getContext(), R.layout.car_alarms_screen, mainBody);
        }

        watchdogScreen.setVisibility(View.VISIBLE);

        //Setup behavior
        restorePictogramBehavior();

        invalidateSwitcherState(mainBody);

        final RadioButton switchOnButton = (RadioButton) findViewById(R.id.switch_on_button);
        switchOnButton.setText(Html.fromHtml(getResources().getString(R.string.switch_on)));
        final RadioButton switchOffButton = (RadioButton) findViewById(R.id.switch_off_button);
        switchOffButton.setText(Html.fromHtml(getResources().getString(R.string.switch_off)));
        updateCarAlarmScreensState();
        OnClickListener onClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                // Is the button now checked?
                //boolean checked = ((RadioButton) view).isChecked();
                //if (checked) {
                //	return;
                //}

                // Check which radio button was clicked
                switch (view.getId()) {
                    case R.id.switch_on_button:
                        resetFocus();
                        getA().switchControlModeIfChanged();

                        signin();
                        if (getA().getDate(HipdriverApplication.EXPIRED_DATE).before(new Date())) {
                            getA().showMessage(getResources().getString(R.string.expired_user_key), null, null);
                            break;
                        }
                        switchOnWatch();
                        break;
                    case R.id.switch_off_button:
                        resetFocus();
                        getA().setCarState(CarStatesEnum.U);
                        getA().setFirstDetectionCarState(CarStatesEnum.U);
                        getA().switchControlModeIfChanged();
                        switchOffWatch();
                        break;
                }
                updateCarAlarmScreensState();
            }
        };
        switchOnButton.setOnClickListener(onClickListener);
        switchOffButton.setOnClickListener(onClickListener);

        String mobileAgentName = getA().getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
        setTextOutput(R.id.mobile_agent_name_text, mobileAgentName, false, true, false);
        EditText mobileAgentNameEditor = (EditText) findViewById(R.id.mobile_agent_name_text);
        mobileAgentNameEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (v.getId() == R.id.mobile_agent_name_text && !hasFocus) {
                    hideSoftKeyboard(v);
                    //get current accelerometer track and send message
                    //debugStatus("");
                }
            }
        });
        mobileAgentNameEditor.addTextChangedListener(new OnTextEdit() {

            @Override
            public void afterTextChanged(Editable s) {
                if (!isManualChanged(s)) {
                    return;
                }
                String mobileAgentName = s.toString();
                getA().setString(MOBILE_AGENT_NAME_EDITOR, mobileAgentName);
                getA().setMemoryVersion(IMobileAgent.MOBILE_AGENT_NAME_KEY);
            }

        });

        //watchdogStatus(getResources().getString(R.string.not_watch), false);
        final Logo appStateLogo = (Logo) findViewById(R.id.app_state_logo);
        if (getA().getAppState() == AppStatesEnum.ASO) {
            appStateLogo.setLogo(R.drawable.unlock, 48, 60);
            appStateLogo.invalidate();
        } else {
            appStateLogo.setLogo(R.drawable.lock, 46, 60);
            appStateLogo.invalidate();
        }

        final TextView shortHelpLinkText = (TextView) findViewById(R.id.short_help_link_text);
        shortHelpLinkText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "show-short-help");
                AnalyticsAdapter.trackInShowHelp();
                Intent intent = new Intent(MainActivity.this, ShortHelpActivity.class);
                startActivity(intent);
            }

        });

    }

    private void restorePictogramBehavior() {
        final ImageButton pictogramButton = uiComponent(R.id.main_screen_$pictogram$_button);
        pictogramButton.setImageResource(R.drawable.pictogram);
        pictogramButton.setVisibility(View.VISIBLE);
        pictogramButton.setOnClickListener(null);
    }

    public void updateCarAlarmScreensState() {
        final RadioButton switchOnButton = (RadioButton) findViewById(R.id.switch_on_button);
        final RadioButton switchOffButton = (RadioButton) findViewById(R.id.switch_off_button);
        switchOnButton.post(new Runnable() {

            @Override
            public void run() {
                switchOnButton.setText(Html.fromHtml(getResources().getString(R.string.switch_on)));
                switchOffButton.setText(Html.fromHtml(getResources().getString(R.string.switch_off)));
                if (getA().getAppState() == AppStatesEnum.ASO) {
                    switchOffButton.toggle();
                    switchOnButton.setChecked(false);
                } else {
                    switchOnButton.toggle();
                    switchOffButton.setChecked(false);
                }
            }
        });
    }

    private void showRemoteControlScreen() {
        if (findViewById(R.id.remote_control_main) != null) {
            return;
        }
        Log.d(TAG, "show-remote-control-screen");
        //Layout initialization
        ViewGroup mainBody = (ViewGroup) findViewById(R.id.main_body);
        if (mainBody == null) {
            return;
        }
        mainBody.removeAllViews();
        View remoteControlScreen = getLayoutInflater().inflate(R.layout.remote_control_screen, mainBody);
        remoteControlScreen.setVisibility(View.VISIBLE);

        final RemoteControlFragment remoteControlFragment = getRemoteControlFragment();
        remoteControlFragment.setMapPositioned(false);
        Fragment mapFragment = getMapFragment();
        final FragmentManager fragmentManager = getSupportFragmentManager();
        //Detect cached state
        if (fragmentManager.findFragmentByTag("map_fragment") != null) {
            FragmentTransaction changeContainerTransaction = fragmentManager.beginTransaction();
            changeContainerTransaction.remove(remoteControlFragment);
            changeContainerTransaction.remove(mapFragment);
            changeContainerTransaction.commit();
            fragmentManager.executePendingTransactions();
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.map_fragment_placeholder, mapFragment, "map_fragment");
        fragmentTransaction.replace(R.id.remote_control_fragment_placeholder, remoteControlFragment, "rc_fragment");
        fragmentTransaction.commit();
        View mapFragmentPlaceholder = findViewById(R.id.map_fragment_placeholder);
        mapFragmentPlaceholder.setVisibility(View.VISIBLE);

        //Setup behavior
        //showRemoteAgentLocation(getStoredLastLocation());
        String selectDevice = getString(R.string.select_device);
        String deviceName = getA().getString(SELECTED_DEVICE_NAME, selectDevice);
        final TextView remoteMobileAgentNameTextView = (TextView) findViewById(R.id.remote_mobile_agent_name_text);
        remoteMobileAgentNameTextView.setText(TextTools.restrictLine(deviceName, MAX_COUNT_OF_CHARACTERS_IN_HEADER, "..."));
        remoteMobileAgentNameTextView.setVisibility(View.VISIBLE);
        remoteMobileAgentNameTextView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showFaceSelect();
                ViewPager viewPager = (ViewPager) findViewById(R.id.face_select_pager);
                //Get face select collection pager ada
                FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = (FaceSelectCollectionPagerAdapter) viewPager.getAdapter();
                faceSelectCollectionPagerAdapter.show(1);
            }

        });

        final View whereAreYouButton = findViewById(R.id.where_are_you);
        whereAreYouButton.setVisibility(View.VISIBLE);
        whereAreYouButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                remoteControlFragment.updateRemoteAgentState();
            }
        });
        changePictogramBehavior();

        final View newLogoTextView = findViewById(R.id.application_title);
        newLogoTextView.setVisibility(View.GONE);

        final View internetStatusButton = findViewById(R.id.link_status_button);
        internetStatusButton.setVisibility(View.VISIBLE);
        final Header headerBackground = (Header) findViewById(R.id.header_background);
        headerBackground.setRemoteControlAppFace();

        setAppFaceListener(new AppFaceListener() {

            @Override
            public void onExitFace() {
                if (findViewById(R.id.remote_control_main) != null) {
                    return;
                }
                headerBackground.setDefault();
                remoteMobileAgentNameTextView.setVisibility(View.GONE);
                internetStatusButton.setVisibility(View.GONE);
                whereAreYouButton.setVisibility(View.GONE);
                //New logo always be visible
                //pictogramButton.setVisibility(View.GONE);
                newLogoTextView.setVisibility(View.VISIBLE);
            }

        });

        AnalyticsAdapter.trackInRemoteControlMode();
    }

    private void changePictogramBehavior() {
        final ImageButton pictogramButton = uiComponent(R.id.main_screen_$pictogram$_button);
        pictogramButton.setImageResource(R.drawable.pictogram_exit);
        pictogramButton.setVisibility(View.VISIBLE);
        //Setup exit behavior
        pictogramButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Now exit available only for remote-control
                //if (getA().getAppFace() == AppFacesEnum.REMOTE_CONTROL) {
                //    return;
                //}
                Log.d(TAG, "show-exit-question");
                Intent intent = new Intent(MainActivity.this, ExitQuestionActivity.class);
                startActivity(intent);
            }
        });
    }

    protected void setAppFaceListener(AppFaceListener appFaceListener) {
        this.appFaceListener = appFaceListener;
    }

    private Fragment getMapFragment() {
        if (this.mapFragment != null && this.mapFragment.getClass().equals(RemoteControlFragment.getMapFragmentClass(getA()))) {
            return mapFragment;
        }
        mapFragment = RemoteControlFragment.newMapFragment(getA());
        return mapFragment;
    }

    private RemoteControlFragment getRemoteControlFragment() {
        if (remoteControlFragment != null) {
            return remoteControlFragment;
        }
        remoteControlFragment = new RemoteControlFragment();
        return remoteControlFragment;
    }

    private void resetFocus() {
        final View releaseFocusView = findViewById(R.id.skip_auto_focus);
        if (releaseFocusView == null) {
            return;
        }
        releaseFocusView.post(new Runnable() {

            @Override
            public void run() {
                releaseFocusView.clearFocus();
                releaseFocusView.requestFocus();
            }

        });
    }

    @Override
    public void debugStatus(final String debugMessage) {
        if (getA().isDebugMode()) {
            safetySetTextOutput(R.id.status_bar_text, debugMessage, false, false, false);
            //statusBar.setText(debugMessage);
        }
    }

    private void networkStatus(final String message) {
        safetySetTextOutput(R.id.network_status_text, message, false, false, false);
    }

    @Override
    public void onSwitchOnDebugMode() {
        final View sendLogButton = findViewById(R.id.send_log_button);
        if (sendLogButton == null) {
            return;
        }
        final View statusBarText = findViewById(R.id.status_bar_text);
        statusBarText.post(new Runnable() {

            @Override
            public void run() {
                sendLogButton.setVisibility(View.VISIBLE);
                statusBarText.setPadding(sendLogButton.getWidth(), 0, 0, 0);
            }

        });
    }

    @Override
    public void onSwitchOffDebugMode() {
        final View sendLogButton = findViewById(R.id.send_log_button);
        final View statusBarText = findViewById(R.id.status_bar_text);
        statusBarText.post(new Runnable() {

            @Override
            public void run() {
                sendLogButton.setVisibility(View.INVISIBLE);
                statusBarText.setPadding(0, 0, 0, 0);
                statusBarText.invalidate();
            }

        });

    }

    public void safetySetTextOutput(int textViewId, final CharSequence text, final boolean append, final boolean bold, final boolean italic) {
        final TextView textView = (TextView) findViewById(textViewId);
        if (textView == null) {
            return;
        }
        textView.post(new Runnable() {

            @Override
            public void run() {
                textOutput(text, append, bold, italic, textView);
            }

        });
    }

    public void setTextOutput(int textViewId, final String text, final boolean append, final boolean bold, final boolean italic) {
        final TextView textView = (TextView) findViewById(textViewId);
        if (textView == null) {
            return;
        }
        textOutput(text, append, bold, italic, textView);
    }


    protected void hideSoftKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    protected void showSoftKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInputFromInputMethod(v.getWindowToken(), 0);
    }

    @Override
    public Object switchOffWatch() {
        debugStatus("");

        final Logo appStateLogo = (Logo) findViewById(R.id.app_state_logo);
        //Check if form is visible
        if (appStateLogo == null) {
            Log.d(TAG, "switch-off-watch[push-button]");
            return getA().getOffscreenExecutor().switchOffWatch();
        }
        appStateLogo.post(new Runnable() {
            @Override
            public void run() {
                appStateLogo.setLogo(R.drawable.unlock, 48, 60);
                appStateLogo.invalidate();
            }
        });
        hidePleaseChargeUsSettings();
        Object returnValue = getA().getOffscreenExecutor().switchOffWatch();
        getA().watchdogAlert(R.string.car_alarms_switched_off, false);
        return returnValue;
    }

    @Override
    public void signin() {
        getA().getOffscreenExecutor().signin();
    }

    @Override
    public Object switchOnWatch() {
        Object returnValue = null;
//		safetySetTextOutput(R.id.whatchdog_status_text, getResources().getString(R.string.watch));
//		EditText whatHappenedEditor = (EditText) findViewById(R.id.what_happened_editor);
//		whatHappenedEditor.setVisibility(View.GONE);
//		whatHappenedEditor.setText("");
        if (getA().getCarState() == CarStatesEnum.A) {
            return returnValue;
        }
        WatchdogService watchdogService = getA().getWatchdogService();
        if (watchdogService == null) {
            return returnValue;
        }
        if (getA().getAppState() != AppStatesEnum.ASO) {
            return returnValue;
        }
        getA().watchdogAlert(R.string.three_points, false);
        debugStatus("");
        getA().watchdogAlert("", false);

        final Logo appStateLogo = (Logo) findViewById(R.id.app_state_logo);
        //Check if form is visible
        if (appStateLogo == null) {
            getA().getOffscreenExecutor().switchOnWatch();
            return returnValue;
        }

        appStateLogo.post(new Runnable() {
            @Override
            public void run() {
                appStateLogo.setLogo(R.drawable.lock, 46, 60);
                appStateLogo.invalidate();
            }
        });
        String phoneNumber = getA().getString(MainActivity.PHONE_NUMBER_EDITOR, "");
        if (phoneNumber.isEmpty()) {
            makeToastWarning(R.string.empty_phone_number_text);
        }

        getA().getOffscreenExecutor().switchOnWatch();
        //Display not modal message
        //TL Version START -->
        //if (getA().getBoolean(IUserLimit.TIME_LIMITED)) {
        //	int delayStartInSeconds = getA().getInt(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS);
        //	long delayInMillis = TimeUnit.HOURS.toMillis(2) + TimeUnit.SECONDS.toMillis(delayStartInSeconds);
        //	showChargeUsSettings(delayInMillis);
        //}
        //<-- TL Version END
        return returnValue;
    }


    @Override
    public void switchOnBeaconMode() {
        //Don't touch car state
        //Don't run in intermediate state
        if (getA().getCarState() == CarStatesEnum.A) {
            return;
        }
        if (getA().getAppState() == AppStatesEnum.U) {
            return;
        }
        WatchdogService watchdogService = getA().getWatchdogService();
        if (watchdogService == null) {
            return;
        }
        BeaconService beaconService = getA().getBeaconService();
        if (beaconService == null) {
            return;
        }
//		if ( getA().getAppState() == AppStatesEnum.CAC ) {
//			return;
//		}
        getA().watchdogAlert(R.string.beacon_mode, false);
        debugStatus("");

        final Logo appStateLogo = (Logo) findViewById(R.id.app_state_logo);
        appStateLogo.post(new Runnable() {
            @Override
            public void run() {
                appStateLogo.setLogo(R.drawable.lock, 46, 60);
                appStateLogo.invalidate();
            }
        });
        getA().getOffscreenExecutor().switchOnBeaconMode();
    }

    private void showChargeUsSettings(final long delayInMillis) {
        if (findViewById(R.id.non_tl_version_link_text) != null) {
            return;
        }
        //Layout initialization
        final ViewGroup pleaseChargeUsBar = (ViewGroup) findViewById(R.id.charge_us_bar);
        pleaseChargeUsBar.post(new Runnable() {
            @Override
            public void run() {
                pleaseChargeUsBar.removeAllViews();
                int layoutResourceId = R.layout.charge_us_fragment;
                if (getA().getInt(SCREEN_ORIENTATION) == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                    layoutResourceId = R.layout.charge_us_fragment_landscape;
                }
                View pleaseChargeUsFragment = View.inflate(pleaseChargeUsBar.getContext(), layoutResourceId, pleaseChargeUsBar);
                pleaseChargeUsFragment.setVisibility(View.VISIBLE);
                int restInMinutes = (int) TimeUnit.MILLISECONDS.toMinutes(delayInMillis);
                int restInHours = restInMinutes / 60;
                int restInMins = restInMinutes % 60;
                String timerText = getString(R.string.counterdown_timer, restInHours, restInMins);
                safetySetTextOutput(R.id.timer_text, timerText, false, false, false);

                //Link settings
                final TextView nonTlVersionLinkText = (TextView) findViewById(R.id.non_tl_version_link_text);
                nonTlVersionLinkText.setText(Html.fromHtml(getString(R.string.non_tl_version_link_text)));
                if (layoutResourceId == R.layout.charge_us_fragment_landscape) {
                    nonTlVersionLinkText.setText(Html.fromHtml(getString(R.string.non_tl_version_link_text2)));
                }
                nonTlVersionLinkText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=ru.hipdriver.watchdog")));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=ru.hipdriver.watchdog")));
                        }
                    }
                });
            }
        });
    }

    private void hidePleaseChargeUsSettings() {
        if (findViewById(R.id.non_tl_version_link_text) == null) {
            return;
        }
        //Layout initialization
        final ViewGroup pleaseChargeUsBar = (ViewGroup) findViewById(R.id.charge_us_bar);
        pleaseChargeUsBar.post(new Runnable() {
            @Override
            public void run() {
                pleaseChargeUsBar.setVisibility(View.GONE);
                pleaseChargeUsBar.removeAllViews();
            }
        });
    }

    private void showStatusBar() {
        if (findViewById(R.id.status_bar_text) != null) {
            return;
        }
        ViewGroup mainStatus = (ViewGroup) findViewById(R.id.main_status_bar);
        if (mainStatus == null) {
            return;
        }
        View statusBar = View.inflate(mainStatus.getContext(), R.layout.status_bar, mainStatus);
        statusBar.setVisibility(View.VISIBLE);

        //final TextView statusBarText = (TextView) findViewById(R.id.status_bar_text);

        final Button sendLogButton = (Button) findViewById(R.id.send_log_button);
        sendLogButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!getA().isDebugMode()) {
                    return;
                }
                WatchdogService watchdogService = getA().getWatchdogService();
                if (watchdogService == null) {
                    return;
                }
                watchdogService.showDebugSettingsDialog(MainActivity.this);
//				
//				EditText whatHappenedEditor = (EditText) findViewById(R.id.what_happened_editor);
//				whatHappenedEditor.setVisibility(View.VISIBLE);
//				sendLogButton.setVisibility(View.INVISIBLE);
//				statusBarText.setVisibility(View.INVISIBLE);
            }

        });

        sendLogButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View p1) {
                if (!getA().isDebugMode()) {
                    return false;
                }
                WatchdogService watchdogService = getA().getWatchdogService();
                if (watchdogService == null) {
                    return false;
                }
                debugStatus("");
                if (getA().isDebugMode()) {
                    watchdogService.showSaveAccerometerTrack(MainActivity.this);
                }
                return true;
            }

        });

//		//Setup what happened
//		final EditText whatHappenedEditor = (EditText) findViewById(R.id.what_happened_editor);
//		whatHappenedEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//			
//		    public void onFocusChange(View v, boolean hasFocus){
//		        if(v.getId() == R.id.what_happened_editor && !hasFocus) {
//		            hideSoftKeyboard(v);
//		            
//		            if ( statusBarText.getVisibility() != View.VISIBLE ) {//Only one call for send
//		            	sendTrackingEvent();
//		            }
//		            
//		    		whatHappenedEditor.setVisibility(View.INVISIBLE);
//		    		sendLogButton.setVisibility(View.VISIBLE);
//		    		statusBarText.setVisibility(View.VISIBLE);
//		        }
//		    }
//
//		});
//		whatHappenedEditor.setOnEditorActionListener(new EditText.OnEditorActionListener() {
//
//	        @Override
//	        public boolean onEditorAction(TextView v, int actionId,
//	                KeyEvent event) {
//	            if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
//		            hideSoftKeyboard(v);
//		            return true;
//	            }
//	            return false;
//	        }
//	    });


    }

//	private void showModalIncommingMessage() {
//		String incommingMessage = getA().getString(IMobileAgent.INCOMING_MESSAGE_KEY, "");
//		if (incommingMessage.isEmpty()) {
//			return;
//		}
//		//Clear incomming message
//		getA().setString(IMobileAgent.INCOMING_MESSAGE_KEY, "");
//		
//		showModalMessage(incommingMessage);
//	}

//	private void sendTrackingEvent() {
//		WatchdogService watchdogService = getA().getWatchdogService();
//		if ( watchdogService == null ) {
//			return;
//		}
// 		//get current accelerometer track and send message
//		debugStatus("");
//		String description = readTextFrom(R.id.what_happened_editor);
//		byte[] jsonData = watchdogService.getAccelerometerTrack();
//		UITaskFactory.sendSensorTrackingEvent(getA(), description, jsonData, 
//				new After(MainActivity.this) {	});
//	}

    @Override
    protected void onStart() {
        Log.d(TAG, "start-main-activity");
        super.onStart();

        //TL Version START -->
        //if (getA().isChargeableInstalled()) {
        //	return;
        //}
        //<-- TL Version END

        getA().resume();

        //Create service request
        startService(new Intent(this, WatchdogService.class));

        boolean firstSignup = getA().getBoolean(ServicesConnector.FIRST_SIGN_UP_KEY);
        if (!firstSignup) {
            showAppFace();
            //Restore debug mode
            if (getA().isDebugMode()) {
                getA().switchOnDebugMode();
                Log.d(TAG, "restore-debug-mode");
            }

            if (getA().getAppFace() == AppFacesEnum.CAR_ALARMS) {
                restoreCarAlarmsState();
            }

        } else {
            showFirstScreen();
        }
        setupAutofocus();

        changeScreenOrientation();
    }

    public void changeScreenOrientation() {
        if ((getA().getInt(SCREEN_ORIENTATION) == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
                && getA().getAppFace() == AppFacesEnum.CAR_ALARMS) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            //@see http://stackoverflow.com/questions/3611457/android-temporarily-disable-orientation-changes-in-an-activity
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }
    }

    private void restoreCarAlarmsState() {
        WatchdogService watchdogService = getA().getWatchdogService();
        if (getA().hasCheckPoint() && watchdogService != null && !watchdogService.isStopped()) {
            Logo appStateLogo = (Logo) findViewById(R.id.app_state_logo);
            appStateLogo.setLogo(R.drawable.lock, 46, 60);
            appStateLogo.invalidate();
        }
        getA().runUserCommandOnResume();

        TextView watchdogStatusText = (TextView) findViewById(R.id.car_alarms_status_text);
        if (watchdogStatusText != null && getA().getAppState() != AppStatesEnum.ASO) {
            CharSequence text = getA().getString(LAST_WATCHDOG_STATUS, getResources().getString(R.string.car_alarms_switched_off));
            watchdogStatusText.setText(text);
            if (watchdogService != null && getA().getAppState() == AppStatesEnum.CAC) {
                //TL Version START -->
                //if (getA().getBoolean(IUserLimit.TIME_LIMITED)) {
                //	long delayInMillis = watchdogService.getRestTTLForWatchInMillis();
                //	showChargeUsSettings(delayInMillis);
                //}
                //<-- TL Version END
            }
        } else {
            getA().watchdogAlert(R.string.car_alarms_switched_off, false);
        }
    }

//private void showFirstMessage() {
//    boolean firstStart = getA().getBoolean(FIRST_START_KEY);
//	if (!firstStart) {
//		return;
//	}
//	String infoMessage = getResources().getString(R.string.first_message);
//	showModalMessage(infoMessage);
//	getA().setBoolean(FIRST_START_KEY, false);
//}

    private void setupAutofocus() {
        View main = findViewById(R.id.main);
        main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                View view = findViewById(R.id.skip_auto_focus);
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);
                view.requestFocus();
                return false;
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (settingsReceiver != null) {
            unregisterReceiver(settingsReceiver);
        }
        if (signupReceiver != null) {
            unregisterReceiver(signupReceiver);
        }

        getA().setDebugMessageConsumer(null);
        getA().setUserCommandsExecutor(getA().getOffscreenExecutor());
        //getA().releaseWakeLock();
        //Reset alert messages listener

        //Stop beep service
        Intent music = new Intent();
        music.setClass(this, BeepService.class);
        stopService(music);

        getA().setMainActivity(null);

        super.onDestroy();
    }

    private void invalidateSwitcherState(View view) {
        if (view == null) {
            return;
        }
        view.post(new Runnable() {

            @Override
            public void run() {
                final Button switchOnButton = (Button) findViewById(R.id.switch_on_button);
                final Button switchOffButton = (Button) findViewById(R.id.switch_off_button);
                if (getA().getAppState() == AppStatesEnum.ASO) {
                    switchOffButton.setPressed(true);
                    switchOnButton.setPressed(false);
                } else {
                    switchOnButton.setPressed(true);
                    switchOffButton.setPressed(false);
                }
            }
        });
    }

    private void textOutput(final CharSequence text, final boolean append,
                            final boolean bold, final boolean italic, final TextView textView) {

        CharSequence finalText = append ? append(textView.getText(), text) : text;
        textView.setText(finalText);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        if (bold || italic) {
            SpannableStringBuilder textBuilder = new SpannableStringBuilder(text);
            int style = bold && italic ? android.graphics.Typeface.BOLD_ITALIC : (bold ? android.graphics.Typeface.BOLD : android.graphics.Typeface.ITALIC);
            StyleSpan bss = new StyleSpan(style);
            textBuilder.setSpan(bss, 0, textBuilder.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            textView.setText(textBuilder);
        }
    }

    private CharSequence append(CharSequence charSequence1, CharSequence charSequence2) {
        SpannableStringBuilder text = new SpannableStringBuilder(charSequence1);
        text.append(charSequence2);
        return text;
    }

    private void multiTrySignUp(final String userName, final byte[] password,
                                final String mobileAgentName, final long timeoutInMillis) throws InterruptedException {
        if (signupThread != null) {
            signupThread.quit();
            return;
        }
        if (signupTask != null) {
            return;
        }
        //signupHandler.removeMessages(0);
        final boolean[] refIsConnected = {false};
        final int[] refCount = {0};
        final long timer = System.currentTimeMillis();
        networkStatus(getString(R.string.linking));

        //Check network status
        if (!Hardwares.isOnline(this)) {
            networkStatus(getString(R.string.network_error_no_internet_connection));
            return;
        }

        final ProgressBar networkStatusProgressBar = (ProgressBar) findViewById(R.id.network_status_progress_bar);
        networkStatusProgressBar.setVisibility(View.VISIBLE);
        signupThread = new HandlerThread("SignupThread") {

            protected boolean isTimeout() {
                return (System.currentTimeMillis() - timer) >= timeoutInMillis;
            }

            @Override
            protected void onLooperPrepared() {
                while (!refIsConnected[0] && !isTimeout()) {
                    final UITask[] newTaskRef = new UITask[1];
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            newTaskRef[0] = UITaskFactory.signUp(getA(), userName, password, mobileAgentName,
                                    new After(MainActivity.this) {
                                        @Override
                                        public void failure(int returnCode, String failureReason) {
                                            NetworkErrors error = NetworkErrors.find(returnCode);
                                            if (error == null) {
                                                String text = getResources().getString(R.string.network_error_retry, returnCode);
                                                networkStatus(text + " " + refCount[0]);
                                            } else if (error == NetworkErrors.NULL_RESPONSE) {
                                                hideProgressBar(networkStatusProgressBar);
                                                refIsConnected[0] = true;
                                                String text = getResources().getString(R.string.authority_error);
                                                networkStatus(text);
                                                setVisiblePasswordShower();
                                                if (signupThread != null) {
                                                    signupThread.quit();
                                                }
                                                signupThread = null;
                                                signupTask = null;
                                                return;
                                            } else {
                                                hideProgressBar(networkStatusProgressBar);
                                                refIsConnected[0] = true;
                                                String text = getResources().getString(R.string.network_error_no_internet_connection);
                                                networkStatus(text);
                                                if (signupThread != null) {
                                                    signupThread.quit();
                                                }
                                                signupThread = null;
                                                signupTask = null;
                                                return;
                                            }
                                            refCount[0]++;
                                            if (isTimeout()) {
                                                hideProgressBar(networkStatusProgressBar);
                                                networkStatus(getResources().getString(R.string.authorization));
                                                if (signupThread != null) {
                                                    signupThread.quit();
                                                }
                                                signupThread = null;
                                                signupTask = null;
                                                return;
                                            }
                                        }

                                        @Override
                                        public void success(Object returnValue) {
                                            hideProgressBar(networkStatusProgressBar);
                                            refIsConnected[0] = true;
                                            getA().setString(USER_NAME_EDITOR, userName);
                                            getA().setString(PASSWORD_EDITOR, new String(password));
                                            getA().setAppState(AppStatesEnum.ASO);//U->CASO TRANSITION
                                            networkStatus(getString(R.string.linked));
                                            //showModalIncommingMessage();
                                            prepareChooseFace();
                                            setupFaceSelector();
                                            showFaceSelect();
                                            if (signupThread != null) {
                                                signupThread.quit();
                                            }
                                            signupThread = null;
                                            signupTask = null;
                                            //Unregister signup receiver
                                            unregisterSignupReceiver();
                                            //Setup settings receiver
                                            activateSettingsReceiverIfSignupComplete();
                                        }

                                    }, timeoutInMillis
                            );
                        }
                    });

                    while (newTaskRef[0] == null && !isTimeout()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            if (signupThread != null) {
                                signupThread.quit();
                            }
                            if (signupTask != null) {
                                signupTask.cancel(true);
                            }
                            signupThread = null;
                            signupTask = null;
                            return;
                        }
                    }

                    signupTask = newTaskRef[0];
                    if (signupTask == null) {
                        break;
                    }
                    try {
                        signupTask.get(timeoutInMillis, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException e) {
                        Log.wtf(TAG, e);
                        break;
                    } catch (ExecutionException e) {
                        Log.wtf(TAG, e);
                        break;
                    } catch (CancellationException e) {
                        Log.wtf(TAG, e);
                        break;
                    } catch (TimeoutException e) {
                    }
                }
                if (signupTask != null && signupTask.getStatus() == Status.FINISHED) {
                    hideProgressBar(networkStatusProgressBar);
                    networkStatus(getResources().getString(R.string.authorization));
                    if (signupThread != null) {
                        signupThread.quit();
                    }
                    signupThread = null;
                    signupTask = null;
                    return;
                }
            }

        };
        signupThread.start();
        Thread.yield();
    }

    private void prepareChooseFace() {
        if (getA().getInt(HipdriverApplication.MOBILE_AGENT_COUNT, 0) > 1) {
            getA().setAppFace(AppFacesEnum.REMOTE_CONTROL);
        } else {
            getA().setAppFace(AppFacesEnum.CAR_ALARMS);
        }
    }

    private void setVisiblePasswordShower() {
        View view = findViewById(R.id.password_shower_block);
        if (view == null) {
            return;
        }
        view.setVisibility(View.VISIBLE);
        final EditText passwordEditor = (EditText) findViewById(R.id.password_editor);
        CheckBox checkBox = (CheckBox) findViewById(R.id.password_shower_checkbox);
        checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    passwordEditor.setTransformationMethod(new TransformationMethod() {

                        @Override
                        public CharSequence getTransformation(
                                CharSequence source, View view) {
                            return source;
                        }

                        @Override
                        public void onFocusChanged(View view,
                                                   CharSequence sourceText, boolean focused,
                                                   int direction, Rect previouslyFocusedRect) {
                            // TODO Auto-generated method stub
                        }

                    });
                } else {
                    passwordEditor.setTransformationMethod(new PasswordTransformationMethod());
                }
                passwordEditor.refreshDrawableState();
            }

        });
    }

    private void unregisterSignupReceiver() {
        if (signupReceiver == null) {
            return;
        }
        try {
            unregisterReceiver(signupReceiver);
        } catch (IllegalArgumentException th) {
            Log.w(TAG, "receiver-not-registered");
        } finally {
            signupReceiver = null;
        }
    }

    protected void hideProgressBar(
            final ProgressBar networkStatusProgressBar) {
        networkStatusProgressBar.post(new Runnable() {

            @Override
            public void run() {
                networkStatusProgressBar.setVisibility(View.GONE);
            }

        });
    }

    public void makeToastWarning(final int resourceId) {
        View view = findViewById(R.id.main);
        if (view == null) {
            return;
        }
        view.post(new Runnable() {

            @Override
            public void run() {
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.toast_warning,
                        null);

                TextView warningText = (TextView) layout.findViewById(R.id.warning_text);
                Spanned text = Html.fromHtml(getResources().getString(resourceId));
                warningText.setText(text);
                Toast toast = new Toast(getA());
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }

        });

    }

//	protected int restoreScrollPosition(
//			int layoutId) {
//		Integer scrollY = scrollPositions.get(layoutId);
//		if (scrollY == null) {
//			return 0;
//		}
//		return scrollY;
//	}

//	protected void saveScrollPosition(int layoutId,
//			int scrollY) {
//		scrollPositions.put(layoutId, scrollY);
//	}

    protected void signup(String userName, byte[] passwordHash) {
        //signup
        //showWtachdogScreen() if success
        if (userName.trim().isEmpty() || new String(passwordHash).trim().isEmpty()) {
            networkStatus(getString(R.string.please_input_user_name_and_password));
            return;
        }
        String mobileAgentName = getA().getString(MOBILE_AGENT_NAME_EDITOR, "");
        if (mobileAgentName.isEmpty()) {
            mobileAgentName = Hardwares.getDeviceName();
            getA().setString(MOBILE_AGENT_NAME_EDITOR, mobileAgentName);
        }
        try {

            multiTrySignUp(userName, passwordHash, mobileAgentName, TimeUnit.MINUTES.toMillis(3));

        } catch (InterruptedException e) {
            Log.wtf(TAG, e);
        }
    }

//	/**
//	 * Show html formated message
//	 * @param message html text or plain text
//	 * @param prefKeyForCheckBox preference key, if null check-box not show, otherwise show with message (Don't show this message again)
//	 * result will be store into preference key as boolean.
//	 * @param activeText clickable text
//	 */
//	public void showModalMessage(final CharSequence message, final String prefKeyForCheckBox, final ActiveText activeText) {
//		final Context uiContext = this;
//		runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				final AlertDialog.Builder modalAlert = new AlertDialog.Builder(uiContext);
//
//				// Setting Dialog Title
//				modalAlert.setTitle(R.string.modal_alert_title);
//
//				// Setting Dialog Views
//				ScrollView scrollView = new ScrollView(uiContext);
//				scrollView.setFillViewport(true);
//
//				LinearLayout linearLayout = new LinearLayout(uiContext);
//				linearLayout.setOrientation(LinearLayout.VERTICAL);
//				linearLayout.setLayoutParams(new ViewGroup.LayoutParams(
//						ViewGroup.LayoutParams.FILL_PARENT,
//						ViewGroup.LayoutParams.WRAP_CONTENT));
//				linearLayout.setPadding(10, 0, 10, 0);
//				
//				final TextView alertView = new TextView(uiContext);
//				alertView.setTextAppearance(uiContext,
//						android.R.style.TextAppearance_Large);
//				Spanned text;
//				if (message instanceof Spanned) {
//					text = (Spanned) message;
//				} else {
//					text = Html.fromHtml(String.valueOf(message));
//				}
//				alertView.setText(text);
//				alertView.setEms(10);
//				//alertView.setMovementMethod(LinkMovementMethod.getInstance());
//				linearLayout.addView(alertView);
//				
//				//Add checkbox
//				if (prefKeyForCheckBox != null) {
//					CheckBox checkBox = new CheckBox(uiContext);
//					checkBox.setEms(10);
//					checkBox.setTextAppearance(uiContext,
//							android.R.style.TextAppearance_Large);
//					text = Html.fromHtml(getString(R.string.do_not_show_this_message_again));
//					checkBox.setText(text);
//					
//					checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//
//						@Override
//						public void onCheckedChanged(CompoundButton buttonView,
//								boolean isChecked) {
//							getA().setBoolean(prefKeyForCheckBox, isChecked);
//						}
//						
//					});
//					
//					linearLayout.addView(checkBox);
//				}
//				//Add active text
//				if (activeText != null) {
//					TextView activeTextView = new TextView(uiContext);
//					activeTextView.setEms(10);
//					activeTextView.setTextAppearance(uiContext,
//							android.R.style.TextAppearance_Large);
//					CharSequence activeTextViewText = activeText.getText();
//					activeTextView.setText(activeTextViewText);
//					activeTextView.setOnClickListener(activeText);
//					linearLayout.addView(activeTextView);
//				}
//				
//				scrollView.addView(linearLayout);
//				
//				modalAlert.setView(scrollView);
//
//				// On pressing Settings button
//				modalAlert.setPositiveButton(
//						R.string.modal_alert_positive_button,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int which) {
//							}
//						});
//
//				// Showing Alert Message
//				modalAlert.show();
//			}
//			
//		});
//	}

    protected void showAppFace() {
        getA().makeDefaulValues();//Update default values for application face

        switch (getA().getAppFace()) {
            case CAR_ALARMS:
                getA().releaseUserCommandsSender();
                if (getA().getControlMode() == ControlModsEnum.INTERNET) {
                    getA().makeRemoteUserCommandsExecutor();
                }
                //Release resources
                releaseUiResources();
                showCarAlarmsScreen();
                break;
            case REMOTE_CONTROL:
                getA().shutdownRemoteUserCommandsExecutor();
                if (getA().getControlMode() == ControlModsEnum.INTERNET) {
                    getA().makeUserCommandsSender();
                }
                showRemoteControlScreen();
                break;
        }
        final Runnable makeNotificationTask = new Runnable() {

            @Override
            public void run() {
                //Notification if control mode is Internet
                if (getA().getControlMode() != ControlModsEnum.INTERNET) {
                    return;
                }
                //Check if always be delivered
                if (!getA().getBoolean(NOTIFICATION_NOT_DELIVERED, true)) {
                    return;
                }
                CharSequence title = "";
                CharSequence text = "";
                String doNotShowNotificationPref = null;
                switch (getA().getAppFace()) {
                    case CAR_ALARMS:
                        if (getA().getBoolean(DO_NOT_SHOW_NOTIFICATION_MESSAGE_CAR_ALARMS, false)) {
                            return;
                        }
                        title = Html.fromHtml(getResources().getString(R.string.internet_control_mode_notification_title));
                        text = Html.fromHtml(getResources().getString(R.string.energy_consumtion_suggestion_car_alarms));
                        doNotShowNotificationPref = DO_NOT_SHOW_NOTIFICATION_MESSAGE_CAR_ALARMS;
                        break;
                    case REMOTE_CONTROL:
                        if (getA().getBoolean(DO_NOT_SHOW_NOTIFICATION_MESSAGE_REMOTE_CONTROL, false)) {
                            return;
                        }
                        title = Html.fromHtml(getResources().getString(R.string.internet_control_mode_notification_title));
                        text = Html.fromHtml(getResources().getString(R.string.energy_consumtion_suggestion_remote_control));
                        doNotShowNotificationPref = DO_NOT_SHOW_NOTIFICATION_MESSAGE_REMOTE_CONTROL;
                        break;
                    default:
                        return;
                }
                getA().setBoolean(NOTIFICATION_NOT_DELIVERED, false);
                getA().makeNotificationForMainActivity(R.drawable.pictogram, R.drawable.pictogram, title, text, doNotShowNotificationPref);
            }

        };
        getTimer().schedule(new TimerTask() {

            @Override
            public void run() {
                makeNotificationTask.run();
            }

        }, TimeUnit.SECONDS.toMillis(3));
    }

    private void showSplashScreen() {
        final Context uiContext = this;
        Runnable showSplashTask = new Runnable() {

            @Override
            public void run() {
                final Dialog dlg = new Dialog(uiContext, R.style.Theme_SplashScreen);
                dlg.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
                View view = dlg.getLayoutInflater().inflate(R.layout.splash_screen, null);
                OnClickListener hider = new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        dlg.dismiss();
                    }
                };
                view.setOnClickListener(hider);
                View layout = view.findViewById(R.id.splash_screen_layout);
                layout.setOnClickListener(hider);
                final View imageBackground = view.findViewById(R.id.splash_background_image);
                imageBackground.setOnClickListener(hider);
                View image = view.findViewById(R.id.splash_image);
                image.setOnClickListener(hider);

                final long timeoutInMillis = TimeUnit.SECONDS.toMillis(2);
                final Animation animation = new TranslateAnimation(0, 0, 0, -150);
                animation.setDuration(timeoutInMillis);
                animation.setFillAfter(true);
                imageBackground.startAnimation(animation);
                imageBackground.setVisibility(View.VISIBLE);

                dlg.setContentView(view);
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Thread.sleep(timeoutInMillis);
                        } catch (InterruptedException e) {
                        }
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                //@see discuss http://stackoverflow.com/questions/2745061/java-lang-illegalargumentexception-view-not-attached-to-window-manager
                                try {
                                    dlg.dismiss();
                                } catch (Throwable th) {
                                    Log.wtf(TAG, th);
                                }
                            }

                        });

                    }

                }, "hide-splash-screen-timer").start();
                dlg.show();
            }

        };
        //Detect main thread
        if (Looper.myLooper() == Looper.getMainLooper()) {
            showSplashTask.run();
        } else {
            runOnUiThread(showSplashTask);
        }
    }

    protected void releaseUiResources() {
//		FragmentManager fragmentManager = getSupportFragmentManager();
//		if (fragmentManager == null) {
//			return;
//		}
//		
//		if (fragmentManager.getFragments() == null) {
//			return;
//		}
//		
//		for (Fragment fragment : fragmentManager.getFragments()) {
//			if (fragment == null) {
//				continue;
//			}
//			View view = fragment.getView();
//			if (view == null) {
//				continue;
//			}
//			view.destroyDrawingCache();
//			ViewParent parentView = view.getParent();
//			if (parentView instanceof ViewGroup) {
//				((ViewGroup) parentView).removeView(view);
//			}
//			//Try to stop all threads
//			fragment.onDestroyView();
//			fragment.onDestroy();			
//		}
    }

    @Override
    public void onUserInteraction() {
        //Reset alert notification
        if (getA().isAlarmActive()) {
            NotificationManager notificationManager = (NotificationManager) getA()
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(RemoteControlFragment.LED_NOTIFICATION_ID);
            WatchdogService watchdogService = getA().getWatchdogService();
            if (watchdogService != null) {
                watchdogService.stopAlarmAlarm();
            }
            getA().setAlarmActive(false);
        }
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public Timer getTimer() {
        if (timer != null) {
            return timer;
        }
        synchronized (this) {
            if (timer != null) {
                return timer;
            }
            timer = new Timer();
            return timer;
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isTopScreen()) {//Conflict with yandex-map and google-map fragments 
            this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode != KeyEvent.KEYCODE_BACK) {
            return super.onKeyDown(keyCode, event);
        }

        final ImageButton faceSelectButton = (ImageButton) findViewById(R.id.face_select_button);
        if (faceSelectButton == null) {
            return super.onKeyDown(keyCode, event);
        }
        if (faceSelectButton.getVisibility() != View.VISIBLE) {
            return super.onKeyDown(keyCode, event);
        }
        if (isTopScreen()) {
            return super.onKeyDown(keyCode, event);
        }
        fireClickTopRightButton(faceSelectButton);
        return true;
    }

    private boolean isTopScreen() {
        boolean isTopScreen = findViewById(R.id.face_select_pager) == null;
        return isTopScreen;
    }

    ;

    @Override
    protected void onPause() {
        Log.d(TAG, "on-pause");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "on-resume");
        super.onResume();
    }

    protected void fireClickTopRightButton(final ImageButton faceSelectButton) {
        View viewPager = findViewById(R.id.face_select_pager);
        if (viewPager == null) {
            faceSelectButton.setSelected(true);
            showFaceSelect();
            fireOnExitFace();
        } else {
            faceSelectButton.setSelected(false);
            FaceSelectCollectionPagerAdapter faceSelectCollectionPagerAdapter = getFaceSelectCollectionPagerAdapter(viewPager);
            if (faceSelectCollectionPagerAdapter != null && !faceSelectCollectionPagerAdapter.isFirstPageSelected()) {
                faceSelectCollectionPagerAdapter.showPrevPage();
                return;
            }
            showAppFace();
            if (getA().getAppFace() == AppFacesEnum.CAR_ALARMS) {
                //Restart process (hard free memory for correct working of application in car_alarms face.
                Intent restartIntent = getBaseContext().getPackageManager().getLaunchIntentForPackage(getA().getPackageName());
                restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(restartIntent);
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        }
    }

    public void dispatchResult(int requestCode,
                               Fragment fragment) {
        resultConsumers.put(requestCode, fragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        Fragment fragment = resultConsumers.remove(requestCode);
        if (fragment == null) {
            return;
        }
        fragment.onActivityResult(requestCode, resultCode, data);
    }

}

