package ru.hipdriver.android.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.CheckBox;

public class HintedCheckBox extends CheckBox {

	public interface OnShowHint {

		void onHint(MotionEvent event);

	}
	
	private OnShowHint onShowHint;

	public HintedCheckBox(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public HintedCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public HintedCheckBox(Context context) {
		super(context);
	}

	public void setOnShowHint(OnShowHint onShowHint) {
		this.onShowHint = onShowHint;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN && onShowHint != null) {
			onShowHint.onHint(event);
		}
		return super.onTouchEvent(event);
	}
	
}
