package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgentProfile;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import ru.hipdriver.android.app.R;

public class SensitivityCarAlarmsSettingsFragment extends FaceSelectCollectionHipdriverFragment {

	private static final String TAG = TextTools.makeTag(SensitivityCarAlarmsSettingsFragment.class);
	protected static final String USER_INPUT_MAX_ACC_KEY = "user_input_max_acc_key";
	protected static final String USER_INPUT_CRITICAL_ANGLE_KEY = "user_input_critical_angle_key";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.sensitivity_car_alarms_settings_fragment,
				container, false);
		refresh(view);
		return view;
	}

	private void refresh(View view) {
		if (view == null) {
			return;
		}
		
		final EditText asdAngleEditor = (EditText) view.findViewById(R.id.asd_angle_editor);
		asdAngleEditor.setText(String.valueOf(getA().getFloat(AlertStateDetector.CRITICAL_ANGLE_KEY)));
		asdAngleEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.asd_angle_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		asdAngleEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String asdMaxAngleText = s.toString();
    			float asdMaxAngle = TextTools.safetyGetFloat(asdMaxAngleText, IMobileAgentProfile.DEFAULT_ASD_MAX_ANGLE);
    			getA().setFloat(AlertStateDetector.CRITICAL_ANGLE_KEY, asdMaxAngle);
    			getA().setMemoryVersion(AlertStateDetector.CRITICAL_ANGLE_KEY);
			}
			
		});
		
		final EditText asdAccelerationEditor = (EditText) view.findViewById(R.id.asd_acceleration_editor);
		asdAccelerationEditor.setText(String.valueOf(getA().getFloat(AlertStateDetector.MAX_ACC_KEY)));
		asdAccelerationEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    public void onFocusChange(View v, boolean hasFocus){
		        if(v.getId() == R.id.asd_acceleration_editor && !hasFocus) {
		            hideSoftKeyboard(v);
			        //get current accelerometer track and send message
	    			//debugStatus("");
		        }
		    }
		});
		asdAccelerationEditor.addTextChangedListener(new OnTextEdit() {

			@Override
			public void afterTextChanged(Editable s) {
				if (!isManualChanged(s)) {
					return;
				}
    			String asdMaxAngleText = s.toString();
    			float asdMaxAngle = TextTools.safetyGetFloat(asdMaxAngleText, IMobileAgentProfile.DEFAULT_ASD_MAX_ACCELERATION);
    			getA().setFloat(AlertStateDetector.MAX_ACC_KEY, asdMaxAngle);
    			getA().setMemoryVersion(AlertStateDetector.MAX_ACC_KEY);
			}
			
		});

		int sensitivityMode = getA().getInt(MainActivity.SENSITIVITY_MODE, R.id.user_sensitivity_button);
		//Switch sensitivity settings
		final RadioButton sensitivityMaxButton = (RadioButton) view.findViewById(R.id.max_sensitivity_button);
		sensitivityMaxButton.setText(Html.fromHtml(getResources().getString(R.string.max_sensitivity2)));
		final RadioButton sensitivityNormButton = (RadioButton) view.findViewById(R.id.norm_sensitivity_button);		
		sensitivityNormButton.setText(Html.fromHtml(getResources().getString(R.string.norm_sensitivity2)));
		final RadioButton sensitivityMinButton = (RadioButton) view.findViewById(R.id.min_sensitivity_button);		
		sensitivityMinButton.setText(Html.fromHtml(getResources().getString(R.string.min_sensitivity2)));
		final RadioButton sensitivityUserButton = (RadioButton) view.findViewById(R.id.user_sensitivity_button);		
		sensitivityUserButton.setText(Html.fromHtml(getResources().getString(R.string.user_sensitivity2)));
		
		final Button automaticDetectionButton = (Button) view.findViewById(R.id.automatic_detection_button);
		automaticDetectionButton.setSelected(getA().getBoolean(AlertStateDetector.AUTOMATIC_DETECTION, false));
		asdAngleEditor.setEnabled(!automaticDetectionButton.isSelected());
		asdAccelerationEditor.setEnabled(!automaticDetectionButton.isSelected());
		
		OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View view) {
			    // Is the button now checked?
			    //boolean checked = ((RadioButton) view).isChecked();
			    //if (checked) {
			    //	return;
			    //}
			    
			    // Check which radio button was clicked
			    switch(view.getId()) {
			    case R.id.max_sensitivity_button:
					//resetFocus();
					
	    			getA().setFloat(AlertStateDetector.CRITICAL_ANGLE_KEY, 1.5f);
	    			getA().setMemoryVersion(AlertStateDetector.CRITICAL_ANGLE_KEY);
	    			getA().setFloat(AlertStateDetector.MAX_ACC_KEY, 40f);
	    			getA().setMemoryVersion(AlertStateDetector.MAX_ACC_KEY);
	    			//Update text-editors
					asdAngleEditor.setText("1.5");
					asdAccelerationEditor.setText("40");
					asdAngleEditor.setEnabled(false);
					asdAccelerationEditor.setEnabled(false);
					
					sensitivityMaxButton.toggle();
					sensitivityNormButton.setChecked(false);
					sensitivityMinButton.setChecked(false);
					sensitivityUserButton.setChecked(false);
					automaticDetectionButton.setVisibility(View.GONE);
					
	    			getA().setBoolean(AlertStateDetector.AUTOMATIC_DETECTION, false);
					automaticDetectionButton.setSelected(false);
					
					getA().setInt(MainActivity.SENSITIVITY_MODE, view.getId());
			    	break;
			    case R.id.norm_sensitivity_button:
					//resetFocus();

	    			getA().setFloat(AlertStateDetector.CRITICAL_ANGLE_KEY, 1.7f);
	    			getA().setMemoryVersion(AlertStateDetector.CRITICAL_ANGLE_KEY);
	    			getA().setFloat(AlertStateDetector.MAX_ACC_KEY, 80f);
	    			getA().setMemoryVersion(AlertStateDetector.MAX_ACC_KEY);
	    			//Update text-editors
					asdAngleEditor.setText("1.7");
					asdAccelerationEditor.setText("80");
					asdAngleEditor.setEnabled(false);
					asdAccelerationEditor.setEnabled(false);

					sensitivityMaxButton.setChecked(false);
					sensitivityNormButton.toggle();
					sensitivityMinButton.setChecked(false);
					sensitivityUserButton.setChecked(false);
					automaticDetectionButton.setVisibility(View.GONE);
					
	    			getA().setBoolean(AlertStateDetector.AUTOMATIC_DETECTION, false);
					automaticDetectionButton.setSelected(false);

					getA().setInt(MainActivity.SENSITIVITY_MODE, view.getId());
			    	break;
			    case R.id.min_sensitivity_button:
					//resetFocus();
					
	    			getA().setFloat(AlertStateDetector.CRITICAL_ANGLE_KEY, 1.7f);
	    			getA().setMemoryVersion(AlertStateDetector.CRITICAL_ANGLE_KEY);
	    			getA().setFloat(AlertStateDetector.MAX_ACC_KEY, 340f);
	    			getA().setMemoryVersion(AlertStateDetector.MAX_ACC_KEY);
	    			//Update text-editors
					asdAngleEditor.setText("1.7");
					asdAccelerationEditor.setText("340");
					
					asdAngleEditor.setEnabled(false);
					asdAccelerationEditor.setEnabled(false);

					sensitivityMaxButton.setChecked(false);
					sensitivityNormButton.setChecked(false);
					sensitivityMinButton.toggle();
					sensitivityUserButton.setChecked(false);
					automaticDetectionButton.setVisibility(View.GONE);
					
	    			getA().setBoolean(AlertStateDetector.AUTOMATIC_DETECTION, false);
					automaticDetectionButton.setSelected(false);
					
					getA().setInt(MainActivity.SENSITIVITY_MODE, view.getId());
			    	break;
			    case R.id.user_sensitivity_button:
					//resetFocus();

			    	if (!automaticDetectionButton.isSelected()) {
			    		asdAngleEditor.setEnabled(true);
			    		asdAccelerationEditor.setEnabled(true);
			    	}
					
					sensitivityMaxButton.setChecked(false);
					sensitivityNormButton.setChecked(false);
					sensitivityMinButton.setChecked(false);
					sensitivityUserButton.toggle();
					automaticDetectionButton.setVisibility(View.VISIBLE);

					getA().setInt(MainActivity.SENSITIVITY_MODE, view.getId());
			    	break;
			    }
				
			}
		};
		sensitivityMaxButton.setOnClickListener(onClickListener);
		sensitivityNormButton.setOnClickListener(onClickListener);		
		sensitivityMinButton.setOnClickListener(onClickListener);		
		sensitivityUserButton.setOnClickListener(onClickListener);
		//Toggle current mode
		switch (sensitivityMode) {
	    case R.id.max_sensitivity_button:
	    	onClickListener.onClick(sensitivityMaxButton);
	    	break;
	    case R.id.norm_sensitivity_button:
	    	onClickListener.onClick(sensitivityNormButton);
	    	break;
	    case R.id.min_sensitivity_button:
	    	onClickListener.onClick(sensitivityMinButton);
	    	break;
	    default:
	    	onClickListener.onClick(sensitivityUserButton);
		}
		
		automaticDetectionButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getActivity() == null) {
					return;
				}
				
				if (automaticDetectionButton.isSelected()) {
					automaticDetectionButton.setSelected(false);
	    			getA().setBoolean(AlertStateDetector.AUTOMATIC_DETECTION, false);
	    			//Reset previous user choice
	    			asdAccelerationEditor.setText(String.valueOf(getA().getFloat(USER_INPUT_MAX_ACC_KEY, getA().getFloat(AlertStateDetector.MAX_ACC_KEY))));
	    			asdAngleEditor.setText(String.valueOf(getA().getFloat(USER_INPUT_CRITICAL_ANGLE_KEY, getA().getFloat(AlertStateDetector.CRITICAL_ANGLE_KEY))));
	    			
	    			asdAccelerationEditor.setEnabled(true);
	    			asdAngleEditor.setEnabled(true);
					
				} else {
					automaticDetectionButton.setSelected(true);
					//Save previous mode
					getA().setFloat(USER_INPUT_MAX_ACC_KEY, getA().getFloat(AlertStateDetector.MAX_ACC_KEY));
					getA().setFloat(USER_INPUT_CRITICAL_ANGLE_KEY, getA().getFloat(AlertStateDetector.CRITICAL_ANGLE_KEY));
					//Set maximal bounds for auto-detection:
					//Set real-values
	    			getA().setFloat(AlertStateDetector.CRITICAL_ANGLE_KEY, 2.9f);
	    			getA().setMemoryVersion(AlertStateDetector.CRITICAL_ANGLE_KEY);
	    			getA().setFloat(AlertStateDetector.MAX_ACC_KEY, 800f);
	    			getA().setMemoryVersion(AlertStateDetector.MAX_ACC_KEY);
	    			//Update text-editors
					asdAngleEditor.setText("2.9");
					asdAccelerationEditor.setText("800");
					
					getA().showMessage(R.string.automatic_detection_hint);
	    			getA().setBoolean(AlertStateDetector.AUTOMATIC_DETECTION, true);
	    			//RESET LIMITS
	    			getA().setFloat(AlertStateDetector.LIMIT_CRITICAL_ANGLE_KEY, 0f);
	    			getA().setFloat(AlertStateDetector.LIMIT_MAX_ACC_KEY, 0f);
	    					
	    			asdAccelerationEditor.setEnabled(false);
	    			asdAngleEditor.setEnabled(false);
				}
			}
			
		});
		
	}

}
