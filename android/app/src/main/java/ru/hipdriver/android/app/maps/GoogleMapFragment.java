package ru.hipdriver.android.app.maps;

import java.lang.ref.WeakReference;

import ru.hipdriver.android.app.R;
import ru.hipdriver.android.util.TextTools;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;

public class GoogleMapFragment extends Fragment {

	private static final String TAG = TextTools
			.makeTag(GoogleMapFragment.class);

	private GoogleMap googleMap;

	private MapView mapView;
	
	private static WeakReference<View> rootViewRef = new WeakReference<View>(null);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return getRootView(inflater, container);
	}

	private View getRootView(LayoutInflater inflater, ViewGroup container) {
		View view = rootViewRef.get();
		if (view != null) {
			((ViewGroup) view.getParent()).removeView(view);
			//Update references
			mapView = (MapView) view.findViewById(R.id.google_map);
			googleMap = mapView.getMap();
			return view;
		}
		view = inflater.inflate(R.layout.google_map_fragment, null,
				false);
		mapView = (MapView) view.findViewById(R.id.google_map);
		Bundle arguments = getArguments();
		mapView.onCreate(arguments);
		try {
			MapsInitializer.initialize(getActivity().getApplicationContext());
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return view;
		}
		//mapView.get
		
		googleMap = mapView.getMap();
		if (googleMap != null) {
			googleMap.getUiSettings().setMyLocationButtonEnabled(false);
			googleMap.getUiSettings().setZoomControlsEnabled(true);
			googleMap.getUiSettings().setCompassEnabled(true);
			googleMap.getUiSettings().setAllGesturesEnabled(true);
			GoogleMapOptions googleMapOptions = arguments.getParcelable("MapOptions");
			if (googleMapOptions != null) {
				googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(googleMapOptions.getCamera()));
			}
			Context context = getActivity();
			setupOverlayControlsBehavior(view, context, mapView);
		}
		//container.removeView(view);
		//container.addView(view);
		
		rootViewRef = new WeakReference<View>(view);
		return view;
	}

	private void setupOverlayControlsBehavior(View rootView, Context context, final MapView mapView) {
		if (context == null) {
			return;
		}
		final Button h = (Button) rootView.findViewById(R.id.overlay_control_for_google_map_type_change_button);
		h.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				GoogleMap googleMap = mapView.getMap();
				if (googleMap == null) {
					return;
				}
				int mapType = googleMap.getMapType();
				if (mapType == GoogleMap.MAP_TYPE_HYBRID) {
					googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
					h.setText(Html.fromHtml("<b>H</b>"));
					h.setBackgroundResource(R.drawable.overlay_controls_for_google_map_button);
				} else {
					googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
					h.setText(Html.fromHtml("<b>U</b>"));
					h.setBackgroundResource(R.drawable.overlay_controls_for_google_map_button_light);
				}
			}
		});
		h.setText(Html.fromHtml("<b>H</b>"));
		
		
		Button plus = (Button) rootView.findViewById(R.id.overlay_control_for_google_map_zoom_plus_button);
		plus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				GoogleMap googleMap = mapView.getMap();
				if (googleMap == null) {
					return;
				}
				googleMap.animateCamera(CameraUpdateFactory.zoomIn());
			}
		});
		
		Button minus = (Button) rootView.findViewById(R.id.overlay_control_for_google_map_zoom_minus_button);
		minus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				GoogleMap googleMap = mapView.getMap();
				if (googleMap == null) {
					return;
				}
				googleMap.animateCamera(CameraUpdateFactory.zoomOut());
			}
		});
		
		GoogleMap googleMap = mapView.getMap();
		if (googleMap == null) {
			Log.w(TAG, "can_not_set_on_change_scale_listener[google-map is null]");
			return;
		}
		//Scale event
		googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

		    private float currentZoom = -1;

		    @Override
		    public void onCameraChange(CameraPosition pos) {
		        if (pos.zoom == currentZoom){
		        	return;
		        }
		        //Update scale text
		        
	            currentZoom = pos.zoom;
		    }
		});		
		
		
//		Button button = new Button(context);
//		button.setText("H");
//	    FrameLayout.LayoutParams hParams = new FrameLayout.LayoutParams(
//	            LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//	    button.setGravity(Gravity.LEFT);
//	    //button.setBackgroundResource(R.drawable.map_button);
//		mapView.addView(button, hParams);
	}

	@Override
	public void onPause() {
		super.onPause();
		try {
			if (mapView != null) {
				mapView.onPause();
			}
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	public static GoogleMapFragment newInstance2(
			GoogleMapOptions googleMapOptions) {
		GoogleMapFragment googleMapFragment = new GoogleMapFragment();
		Bundle bundle = new Bundle();
		bundle.putParcelable("MapOptions", googleMapOptions);
		googleMapFragment.setArguments(bundle);
		return googleMapFragment;
	}

	public GoogleMap getMap() {
		if (mapView != null) {
			return mapView.getMap();
		}
		return null;
	}

	@Override
	public void onResume() {
		try {
			if (mapView != null) {
				mapView.onResume();
			}
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		super.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			//mapView.onDestroy();
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		try {
			if (mapView != null) {
				mapView.onLowMemory();
			}
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

}
