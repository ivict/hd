package ru.hipdriver.android.app;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.GZIPOutputStream;

import ru.hipdriver.android.app.WatchdogService.WatchdogServiceCritical;
import ru.hipdriver.android.app.maps.GoogleMapTools;
import ru.hipdriver.android.app.sslucs.UcClientFactory;
import ru.hipdriver.android.app.sslucs.UcServerFactory;
import ru.hipdriver.android.app.util.CtxWrapper;
import ru.hipdriver.android.i.ISslClient;
import ru.hipdriver.android.i.ISslServer;
import ru.hipdriver.android.i.NetworkConnectionListener;
import ru.hipdriver.android.util.StreamTools;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.IUserLimit;
import ru.hipdriver.i.support.AppFacesEnum;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.i.support.ControlModsEnum;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.i.support.MapProvidersEnum;
import ru.hipdriver.i.support.UserCommandTypesEnum;
import android.app.Activity;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Global variables container.
 * 
 * @author ivict
 * 
 */
public class HipdriverApplication extends Application {

	private static final String TAG = TextTools.makeTag(HipdriverApplication.class);

	private static final int FULL_DAY_IN_MILLIS = 24 * 60 * 1000;
	public static final String EXTRA_APPLICATION_STATE = "appState";
	public static final String EXTRA_CAR_STATE = "carState";
	public static final String EXTRA_FIRST_DETECTION_CAR_STATE = "firstDetectionCarState";
	public static final String EXTRA_DEBUG_MODE = "debugMode";
	public static final String EXTRA_PAUSED = "paused";
	public static final String EXPIRED_DATE = "expiredDate";
	public static final String WATCH_ON_TIMER = "watch_on_timer";
	
	public static final int POWER_MEASURE_INTERVAL_IN_MINUTES = 5;
	//public static final int PING_INTERVAL_IN_MINUTES = 45;//32 times in one day
	public static final String CAR_STATE = "car-state";
	public static final String APP_STATE = "app-state";
	public static final String FIRST_DETECTION_CAR_STATE = "first-detection-car-state";
	public static final String APP_FACE = "app_face";
	public static final String MOBILE_AGENT_COUNT = "mobile-agent-count";
	
	public static final String SEND_LOG_AFTER_SWITCH_OFF_TO_DEVELOPERS = "send-log-after-switch-off-to-developers";
	
	public static final String TECH_SUPPORT_EMAIL = "ivict@rambler.ru";

	/**
	 * Global variable car state.
	 */
	private CarStatesEnum carState = CarStatesEnum.U;

	/**
	 * Global variable application state.
	 */
	private AppStatesEnum appState = AppStatesEnum.ASO;
	
	/**
	 * Global variable first detection car state.
	 * First detection after alert without last correction.
	 */
	private CarStatesEnum firstDetectionCarState = CarStatesEnum.U;

	/**
	 * Global variable face of application.
	 */
	private AppFacesEnum appFace = AppFacesEnum.CAR_ALARMS;

	/**
	 * Global variable control mode for mobile agent.
	 */
	private ControlModsEnum controlMode = ControlModsEnum.INTERNET;

	/**
	 * Global variable debugMode.
	 */
	private boolean debugMode;

	private IDebugMessageConsumer debugMessageConsumer;

	private IUserCommandsExecutor userCommandsExecutor;

	private IUserCommandsExecutor remoteUserCommandsExecutor;

	// TODO: refactoring to map
	private WatchdogService watchdogService;
	
	private BeaconService beaconService;
	
	private EyesService eyesService;
	
	private boolean webserviceAvailable;

	private WakeLock wakeLock;

	private BroadcastReceiver screenStateReceiver;
	
	private Map<String, Object> defaultValues;
	
	private AtomicBoolean paused = new AtomicBoolean(false);
	
	private boolean isCellNetworkAvailable;
	
	private boolean hasConnection = true;
	
	private ExecutorService networkTasksExecutorService = Executors.newSingleThreadExecutor(HipdriverThreadFactory.newSingleThreadInstance("network-tasks-executor"));

    private ScheduledExecutorService phoneTasksExecutorService = Executors.newSingleThreadScheduledExecutor(HipdriverThreadFactory.newSingleThreadInstance("phone-tasks-executor"));

    private ExecutorService alertTasksExecutorService = Executors.newSingleThreadExecutor(HipdriverThreadFactory.newSingleThreadInstance("alert-tasks-executor"));

	private Timer mainTimer = new Timer("MainTimer");

	private Map<String, Boolean> debugInfoHeaderMaking;
	
	private String lastWatchdogStatus;

	private int lastWatchdogStatusIid;

	private WatchdogServiceCritical watchdogServiceCritical;
	
	private ISslServer ucServer;
	private Thread makingUcServerThread;
	private ISslClient ucClient;
	private Thread makingUcClientThread;

    private class BatteryHandlerFlags {
		boolean notHandled60Pct;
		boolean notHandled30Pct;
		boolean notHandledLowLevel;
	}
	BatteryHandlerFlags batteryHandlerFlags = new BatteryHandlerFlags();
	
	//For samsung cameras @see http://stackoverflow.com/questions/13546788/camera-takepicture-not-working-on-my-jb-gb-froyo-phones
	private Activity mainActivity;
	
	//static warranty for unique
	private static AtomicInteger gsmReciverSemaphor = new AtomicInteger();
	private static AtomicReference<Thread> shutdownGsmReceiverThreadRef = new AtomicReference<Thread>();

	private AppStatesEnum remoteAppState;

	private boolean isAlarmActive;

	private NetworkConnectionListener networkConnectionListener;
	
	protected PhoneStateListener phoneStateListener;

	private boolean notFirstStartOfMainActivity;

	private boolean interrupted;

	public WatchdogService getWatchdogService() {
		return watchdogService;
	}

	@Deprecated
	private IDebugMessageConsumer getDebugMessageConsumer() {
		return debugMessageConsumer;
	}
	
	public BeaconService getBeaconService() {
		return beaconService;
	}
	
	public void setBeaconService(BeaconService beaconService) {
		this.beaconService = beaconService;
	}

	public void setDebugMessageConsumer(
			IDebugMessageConsumer debugMessageConsumer) {
		this.debugMessageConsumer = debugMessageConsumer;
	}

	public void setWatchdogService(WatchdogService watchdogService) {
		if (this.watchdogService != null) {
			WatchdogService.WatchdogServiceCritical watchdogServiceCritical = this.watchdogService.getCriticalData();
			setWatchdogServiceCritical(watchdogServiceCritical);
		}
		this.watchdogService = watchdogService;
	}

	private void setWatchdogServiceCritical(
			WatchdogServiceCritical watchdogServiceCritical) {
		this.watchdogServiceCritical = watchdogServiceCritical;
	}

	public WatchdogService.WatchdogServiceCritical getWatchdogServiceCritical() {
		return this.watchdogServiceCritical;
	}

	public CarStatesEnum getCarState() {
		return carState;
	}

	public void setCarState(CarStatesEnum carState) {
		this.carState = carState;
		setInt(CAR_STATE, carState.ordinal());
	}
	
	public CarStatesEnum getFirstDetectionCarState() {
		return firstDetectionCarState;
	}

	public void setFirstDetectionCarState(CarStatesEnum carState) {
		this.firstDetectionCarState = carState;
		setInt(FIRST_DETECTION_CAR_STATE, carState.ordinal());
	}

	public AppStatesEnum getAppState() {
		return appState;
	}

	public void setAppState(AppStatesEnum appState) {
		this.appState = appState;
		setInt(APP_STATE, appState.ordinal());
	}

	public AppFacesEnum getAppFace() {
		return appFace;
	}

	public void setAppFace(AppFacesEnum appFace) {
		this.appFace = appFace;
		setInt(APP_FACE, appFace.ordinal());
	}

	public ControlModsEnum getControlMode() {
		return controlMode;
	}

	public void setControlMode(ControlModsEnum controlMode) {
		this.controlMode = controlMode;
		setInt(MainActivity.CONTROL_MODE, controlMode.ordinal());
	}

	public boolean isDebugMode() {
		return debugMode;
	}

	public void switchDebugMode() {
		debugMode = !debugMode;
		if (debugMessageConsumer == null) {
			return;
		}
		if (debugMode) {
			debugMessageConsumer.onSwitchOnDebugMode();
		} else {
			debugMessageConsumer.onSwitchOffDebugMode();
		}
		//User actions memory
		setBoolean(EXTRA_DEBUG_MODE, debugMode);
	}

	public void switchOffDebugMode() {
		debugMode = false;
		if (debugMessageConsumer == null) {
			return;
		}
		debugMessageConsumer.onSwitchOffDebugMode();
	}

	public void switchOnDebugMode() {
		debugMode = true;
		if (debugMessageConsumer == null) {
			return;
		}
		debugMessageConsumer.onSwitchOnDebugMode();
	}

	public void debugStatus(String debugMessage) {
		if (debugMessageConsumer == null) {
			return;
		}
		debugMessageConsumer.debugStatus(debugMessage);
	}

	public void setString(String preferencesKey,
			String preferencesValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = preferences.edit();
		editor.putString(preferencesKey, preferencesValue);
		editor.commit();
	}
	
	public void setInt(String preferencesKey,
			int preferencesValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = preferences.edit();
		editor.putInt(preferencesKey, preferencesValue);
		editor.commit();
	}
	
	/**
	 * Set int value for pref-key and rarity.
	 * 
	 * @param preferredKey key for persist setting
	 * @param rarity info for control if setting is rare
	 * @param value persist value
	 */
	public void setInteger(String preferredKey, Object rarity, int value) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor = preferences.edit();
		editor.putInt(preferredKey, value);
		editor.putInt(preferredKey + "$rarity", rarity.hashCode());
		editor.commit();
	}

	public void setLong(String preferencesKey,
			long preferencesValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = preferences.edit();
		editor.putLong(preferencesKey, preferencesValue);
		editor.commit();
	}

	protected void setFloat(String preferencesKey,
			float preferencesValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = preferences.edit();
		editor.putFloat(preferencesKey, preferencesValue);
		editor.commit();
	}

	protected void setBoolean(String preferencesKey,
			boolean preferencesValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = preferences.edit();
		editor.putBoolean(preferencesKey, preferencesValue);
		editor.commit();
	}

	protected void setDate(String preferencesKey,
			Date preferencesValue) {
		if (preferencesValue == null) {
			throw new IllegalArgumentException("Store only not null values");
		}
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = preferences.edit();
		editor.putLong(preferencesKey, preferencesValue.getTime());
		editor.commit();
	}
	
	protected Date getDate(String preferencedKey, Date defaultValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		long defValue = defaultValue.getTime();
		long time = preferences.getLong(preferencedKey, defValue);
		return new Date(time);
	}
	
	

	protected void setFloatIfNotPresent(String preferencesKey,
			float preferencesValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		if (!preferences.contains(preferencesKey)) {
			Editor editor = preferences.edit();
			editor.putFloat(preferencesKey, preferencesValue);
			editor.commit();
		}
	}

	protected void setIntIfNotPresent(String preferencesKey,
			int preferencesValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		if (!preferences.contains(preferencesKey)) {
			Editor editor = preferences.edit();
			editor.putInt(preferencesKey, preferencesValue);
			editor.commit();
		}
	}
	
	protected boolean getBoolean(String preferencedKey) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		boolean defValue = (Boolean) getDefaultValue(preferencedKey);
		boolean value = preferences.getBoolean(preferencedKey, defValue);
		return value;
	}

	protected boolean getBoolean(String preferencedKey, boolean defaultValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		boolean value = preferences.getBoolean(preferencedKey, defaultValue);
		return value;
	}

	protected Date getDate(String preferencedKey) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		long defValue = (Long) getDefaultValue(preferencedKey);
		long time = preferences.getLong(preferencedKey, defValue);
		return new Date(time);
	}
	
	protected float getFloat(String preferencedKey) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		float defValue = (Float) getDefaultValue(preferencedKey);
		float value = preferences.getFloat(preferencedKey, defValue);
		return value;
	}

	protected float getFloat(String preferencedKey, float defaulValue) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		float value = preferences.getFloat(preferencedKey, defaulValue);
		return value;
	}

	private Object getDefaultValue(String preferencedKey) {
		if (defaultValues == null) {
			synchronized(this) {
				if (defaultValues != null) {
					return defaultValues.get(preferencedKey);
				}
				makeDefaulValues();
			}
		}
		return defaultValues.get(preferencedKey);
	}

	synchronized protected void makeDefaulValues() {
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(IMobileAgentProfile.DELAY_PUSH_ALERT_EVENT_IN_SECONDS, IMobileAgentProfile.DEFAULT_DELAY_PUSH_ALERT_EVENT_IN_SECONDS);
		values.put(IMobileAgentProfile.DELAY_WHERE_AM_I_REQUEST_IN_SECONDS, IMobileAgentProfile.DEFAULT_DELAY_WHERE_AM_I_REQUEST_IN_SECONDS);
		values.put(IMobileAgentProfile.ENABLE_DEBUG_MODE, IMobileAgentProfile.DEFAULT_ENABLE_DEBUG_MODE);
		values.put(WatchdogService.WATCHDOG_SERVICE_CHECK_POINT, 0L);
		values.put(HipdriverApplication.EXTRA_DEBUG_MODE, false);
		values.put(ServicesConnector.FIRST_SIGN_UP_KEY, true);
		values.put(MainActivity.FIRST_START_KEY, true);
		values.put(IMobileAgentProfile.REQUEST_SERVER_IF_UNKNOWN_NUMBER, IMobileAgentProfile.DEFAULT_REQUEST_SERVER_IF_UNKNOWN_NUMBER);
		values.put(IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE, IMobileAgentProfile.DEFAULT_ENABLE_ENERGY_SAVING_MODE);
		values.put(EXPIRED_DATE, System.currentTimeMillis() + 2 * 30 * 24 * 3600 * 1000L);
		values.put(AlertStateDetector.MAX_ACC_KEY, IMobileAgentProfile.DEFAULT_ASD_MAX_ACCELERATION);
		values.put(AlertStateDetector.CRITICAL_ANGLE_KEY, IMobileAgentProfile.DEFAULT_ASD_MAX_ANGLE);
		values.put(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS, IMobileAgentProfile.DEFAULT_WATCHDOG_SERVICE_START_DELAY_IN_SECONDS);
		values.put(IMobileAgentProfile.SWITCH_ON_BEACON_MODE, IMobileAgentProfile.DEFAULT_SWITCH_ON_BEACON_MODE);
		values.put(IMobileAgentProfile.BEACON_INTERVAL_IN_MINUTES, IMobileAgentProfile.DEFAULT_BEACON_INTERVAL_IN_MINUTES);
		values.put(WatchdogService.DISABLE_SCREEN_ON_SENSOR_SILENT, false);
		values.put(IMobileAgentProfile.BEACON_PWR_TRESHOLD_PERCENTS, IMobileAgentProfile.DEFAULT_BEACON_PWR_TRESHOLD_PERCENTS);
		values.put(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_CAR_POKE, IMobileAgentProfile.DEFAULT_CAR_AUTO_ALARMS_AFTER_CAR_POKE);
		values.put(IMobileAgentProfile.CAR_AUTO_ALARMS_AFTER_STEAL_WHEELS, IMobileAgentProfile.DEFAULT_CAR_AUTO_ALARMS_AFTER_STEAL_WHEELS);
		values.put(IMobileAgentProfile.ALERT_CALLS_COUNT, IMobileAgentProfile.DEFAULT_ALERT_CALLS_COUNT);
		values.put(IMobileAgentProfile.ALERT_CALLS_INTERVAL_IN_SECONDS, IMobileAgentProfile.DEFAULT_ALERT_CALLS_INTERVAL_IN_SECONDS);
		values.put(IMobileAgentProfile.ALERT_CALLS_DURATION_IN_SECONDS, IMobileAgentProfile.DEFAULT_ALERT_CALLS_DURATION_IN_SECONDS);
		values.put(IMobileAgentProfile.SEND_SMS_AFTER_EVENT_CLASS_DETECTION, IMobileAgentProfile.DEFAULT_SEND_SMS_AFTER_EVENT_CLASS_DETECTION);
		values.put(IMobileAgentProfile.SEND_EMAIL_AFTER_EVENT_CLASS_DETECTION, IMobileAgentProfile.DEFAULT_SEND_EMAIL_AFTER_EVENT_CLASS_DETECTION);
		values.put(IMobileAgentProfile.SEND_EMAIL_ON_POWER_EVENT, IMobileAgentProfile.DEFAULT_SEND_EMAIL_ON_POWER_EVENT);
		values.put(IMobileAgentProfile.SEND_EMAIL_ON_SWITCH_ON_SWITCH_OFF, IMobileAgentProfile.DEFAULT_SEND_EMAIL_ON_SWITCH_ON_SWITCH_OFF);
		values.put(IMobileAgentProfile.SEND_PHOTO_AFTER_ALERT, IMobileAgentProfile.DEFAULT_SEND_PHOTO_AFTER_ALERT);
		values.put(IMobileAgentProfile.COUNT_OF_PHOTOS, IMobileAgentProfile.DEFAULT_COUNT_OF_PHOTOS);
		values.put(IMobileAgentProfile.INTERVAL_BETWEEN_PHOTOS_IN_SECONDS, IMobileAgentProfile.DEFAULT_INTERVAL_BETWEEN_PHOTOS_IN_SECONDS);
		values.put(IMobileAgentProfile.RUN_AFTER_REBOOT, IMobileAgentProfile.DEFAULT_RUN_AFTER_REBOOT);
		values.put(IMobileAgentProfile.WAIT_UNTIL_THE_ALERT_CALL, IMobileAgentProfile.DEFAULT_WAIT_UNTIL_THE_ALERT_CALL);
		values.put(IUserLimit.TIME_LIMITED, IUserLimit.DEFAULT_TIME_LIMITED);
		values.put(SEND_LOG_AFTER_SWITCH_OFF_TO_DEVELOPERS, false);
		values.put(IMobileAgentProfile.ALARM_ON_POWER_CABLE_CONNECT, IMobileAgentProfile.DEFAULT_ALARM_ON_POWER_CABLE_CONNECT);
		values.put(IMobileAgentProfile.ALARM_ON_POWER_CABLE_DISCONNECT, IMobileAgentProfile.DEFAULT_ALARM_ON_POWER_CABLE_DISCONNECT);
		//TUNE ru_RU LOCALE TODO: update by national providers for all locales
		Locale current = getResources().getConfiguration().locale;
        boolean googleMapsNotAvailable = !GoogleMapTools.isGoogleMapsAvailable(this);
		if (googleMapsNotAvailable
                || ("ru".equals(current.getLanguage()) && getAppFace() == AppFacesEnum.CAR_ALARMS)) {//FOR LAC AND CID REQUESTS
			values.put(IMobileAgentProfile.MAP_PROVIDER_ID, (int) DBLinks.link(MapProvidersEnum.YANDEX));
		} else {
			values.put(IMobileAgentProfile.MAP_PROVIDER_ID, (int) IMobileAgentProfile.DEFAULT_MAP_PROVIDER_ID);
		}
		values.put(MainActivity.SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);		
		
		defaultValues = Collections.unmodifiableMap(values);
	}

	public IUserCommandsExecutor getUserCommandsExecutor() {
		return userCommandsExecutor;
	}

	public void setUserCommandsExecutor(
			IUserCommandsExecutor userCommandsExecutor) {
		this.userCommandsExecutor = userCommandsExecutor;
	}

	public IUserCommandsExecutor getRemoteUserCommandsExecutor() {
		return remoteUserCommandsExecutor;
	}

	public void setRemoteUserCommandsExecutor(
			IUserCommandsExecutor remoteUserCommandsExecutor) {
		this.remoteUserCommandsExecutor = remoteUserCommandsExecutor;
	}

	public boolean isWebserviceAvailable() {
		return webserviceAvailable;
	}

	public void setWebserviceAvailable(boolean webserviceAvailable) {
		this.webserviceAvailable = webserviceAvailable;
	}

	public void releaseWakeLock() {
		if (wakeLock == null) {
			return;
		}
		wakeLock.release();
		wakeLock = null;
		// Intent myAlarm = new Intent(getApplicationContext(),
		// MainBroadcastReceiver.class);
		// //myAlarm.putExtra("project_id",project_id); //put the SAME extras
		// PendingIntent recurringAlarm =
		// PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm,
		// PendingIntent.FLAG_CANCEL_CURRENT);
		// AlarmManager alarms = (AlarmManager)
		// getApplicationContext().getSystemService(Context.ALARM_SERVICE);
		// alarms.cancel(recurringAlarm);
	}

	// Idea here @see
	// http://stackoverflow.com/questions/8713361/keep-a-service-running-even-when-phone-is-asleep/8713795#8713795
	public void setWakeLock() {
		// Intent myAlarm = new Intent(getApplicationContext(),
		// MainBroadcastReceiver.class);
		// //myAlarm.putExtra("project_id", project_id); //Put Extra if needed
		// PendingIntent recurringAlarm =
		// PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm,
		// PendingIntent.FLAG_CANCEL_CURRENT);
		// AlarmManager alarms = (AlarmManager)
		// this.getSystemService(Context.ALARM_SERVICE);
		// Calendar updateTime = Calendar.getInstance();
		// //updateTime.setWhatever(0); //set time to start first occurence of
		// alarm
		// alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
		// updateTime.getTimeInMillis(), AlarmManager.INTERVAL_DAY,
		// recurringAlarm); //you can modify the interval of course
		if (wakeLock != null && wakeLock.isHeld()) {
			return;
		}

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
				"Lock by car-alarm");
		wakeLock.acquire();
	}
	
	public boolean hasWakeLock() {
		return wakeLock != null && wakeLock.isHeld();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
		//TL Version START -->
		//if (isChargeableInstalled()) {
		//	interrupt();
		//	try {
		//		safetySleep(2);
		//		onTerminate();
		//		safetySleep(3);
		//	} finally {
		//		clearInterruptFlag();
		//	}
		//	return;
		//}
		//<-- TL Version END		
		
		startWatchdogService();
		startBeaconService();
	    AnalyticsAdapter.init(this);
	    setUserCommandsExecutor(getOffscreenExecutor());
	    setRemoteUserCommandsExecutor(getDefaultRemoteExecutor());
	    registerScreenStateReceiver();
		lastWatchdogStatus = getString(MainActivity.LAST_WATCHDOG_STATUS, "");
		lastWatchdogStatusIid = getInt(MainActivity.LAST_WATCHDOG_STATUS_IID, CCRI.getInvariantId(R.string.three_points));
		//Power control
		if (Hardwares.isPowerCableUnplugged(this)) {
			handleUnplugPowerCable();
		}
		long powerMeasureIntervalInMillis = TimeUnit.MINUTES.toMillis(POWER_MEASURE_INTERVAL_IN_MINUTES);
	    mainTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				Intent batteryIntent = registerReceiver(null, new IntentFilter(
						Intent.ACTION_BATTERY_CHANGED));
				int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
				int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

				//int batteryStatus = batteryIntent.getIntExtra(
				//		BatteryManager.EXTRA_STATUS, -1);

				// Error checking that probably isn't needed but I added just in case.
				if (level == -1 || scale == -1) {
					return;
				}
				
				float batteryPct = ((float) level / (float) scale) * 100.0f;
				handleBatteryPct(batteryPct);
				
				if (!getBoolean(IMobileAgentProfile.SWITCH_ON_BEACON_MODE)) {
					return;
				}

				float tresholdPwr = getFloat(IMobileAgentProfile.BEACON_PWR_TRESHOLD_PERCENTS);
				CarStatesEnum carState = getCarState();
				if (batteryPct > tresholdPwr) {
					return;
				}
				boolean isFinalAlertState = carState != CarStatesEnum.U && carState != CarStatesEnum.A;
				//TODO: Check logic
				if (!isFinalAlertState) {
					return;
				}
				IUserCommandsExecutor userCommandsExecutor = getUserCommandsExecutor();
				if (userCommandsExecutor == null) {
					return;
				}
				userCommandsExecutor.switchOnBeaconMode();
			}

	    }, powerMeasureIntervalInMillis, powerMeasureIntervalInMillis);
	    
	    Date nextFirstMinuteOfHour = getNextFirstMinuteOfHour();
	    mainTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				if (getControlMode() != ControlModsEnum.INTERNET) {
					return;
				}
				switch (getAppFace()) {
				case CAR_ALARMS:
					ISslServer sslServer = getUcServer();
					if (sslServer == null) {
						return;
					}
					sslServer.reconnect();
					break;
				case REMOTE_CONTROL:
					ISslClient sslClient = getUcClient();
					if (sslClient == null) {
						return;
					}
					sslClient.reconnect();
					break;
				}
			}
	    	
	    }, nextFirstMinuteOfHour, TimeUnit.HOURS.toMillis(1));

	    restorePrevLifeState();
	    //Init phone listener
	    isCellNetworkAvailable = !Hardwares.getDefaultCellNetworkOperator(this).isEmpty();
	    phoneStateListener = registerPhoneStateListener();
	    
	    makeInternetConnectorIfCellNetworkUnavailableOrControlModeIsInternet(getControlMode());
	    
	    //Setup exception handler for main thread
	    Thread.setDefaultUncaughtExceptionHandler(new HipdriverRuntimeExceptionHandler(this,
	            MainActivity.class));
	}
	
	public boolean isChargeableInstalled() {
		return CtxWrapper.make(this).isAppInstalled("ru.hipdriver.watchdog");
	}

	protected void safetySleep(int seconds) {
		try {
			Thread.sleep(TimeUnit.SECONDS.toMillis(seconds));
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		}
	}

	private Date getNextFirstMinuteOfHour() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, 1);
		calendar.set(Calendar.MINUTE, 1);
		return calendar.getTime();
	}

	protected void registerScreenStateReceiver() {
		if (screenStateReceiver != null) {
			return;
		}
		final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		screenStateReceiver = new ScreenStateReceiver();
		registerReceiver(screenStateReceiver, filter);
	}

	protected void unregisterScreenStateReceiver() {
		if (screenStateReceiver == null) {
			return;
		}
		try {
			unregisterReceiver(screenStateReceiver);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return;
		}
		screenStateReceiver = null;
	}

	public void makeInternetConnectorIfCellNetworkUnavailableOrControlModeIsInternet(ControlModsEnum controlMode) {
		if (!isSignup()) {
			return;
		}
		try {
			if (controlMode != ControlModsEnum.INTERNET && isCellNetworkAvailable()) {
				return;
			}
			setControlMode(ControlModsEnum.INTERNET);
	    	switch(getAppFace()) {
			case CAR_ALARMS:
				makeRemoteUserCommandsExecutor();
				break;
			case REMOTE_CONTROL:
				makeUserCommandsSender();
				break;
	    	}
		} finally {
			synchronizeCurrentControlModeWithServer();
		}
	}

	public void synchronizeCurrentControlModeWithServer() {
		if (getAppFace() != AppFacesEnum.CAR_ALARMS) {
			//Synchronize current control mode with server
			long remoteMobileAgentId = getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
			if (remoteMobileAgentId == IMobileAgent.INVALID_ID) {
				Log.w(TAG, "invalid_remote_mobile_agent_id");
				return;
			}
			UITaskFactory.getMap(this, new After() {
				
				@Override
				public void success(Object returnValue) {
					Properties properties = (Properties) returnValue;
					Object controlModeId = properties.get(IMobileAgentProfile.CONTROL_MODE_ID);
					if (controlModeId == null) {
						return;
					}
					String controlModeIdText = String.valueOf(controlModeId);
					short controlModeIdShort = -1;
					try {
						controlModeIdShort = Short.parseShort(controlModeIdText);
					} catch (NumberFormatException e) {
						return;
					}
					ControlModsEnum currentControlModeForRemoteAgent = DBLinks.unlinkControlMode(controlModeIdShort);
					Log.d(TAG, String.format("get-map update-info-control-mode[%s]", currentControlModeForRemoteAgent));
					setControlMode(currentControlModeForRemoteAgent);
				}
			}, remoteMobileAgentId);
			return;
		}
		//Synchronize current control mode with server
		long mobileAgentId = getLong(
				ServicesConnector.MOBILE_AGENT_ID_KEY, IMobileAgent.INVALID_ID);
		if (mobileAgentId == IMobileAgent.INVALID_ID) {
			Log.w(TAG, "invalid_mobile_agent_id");
			return;
		}
		Properties properties = new Properties();
		ControlModsEnum currentControlMode = getControlMode();
		properties.setProperty(IMobileAgentProfile.CONTROL_MODE_ID, String.valueOf(DBLinks.link(currentControlMode)));
		Log.d(TAG, String.format("send-emap update-info-control-mode[%s]", currentControlMode));
		UITaskFactory.sendEmap(this, null, mobileAgentId, properties);
	}

	protected PhoneStateListener registerPhoneStateListener() {
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	    if (telephonyManager == null) {
	    	return null;
	    }
		PhoneStateListener phoneStateListener = new CellularNetworkStateListener(this);
		telephonyManager.listen(phoneStateListener,
				PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
				| PhoneStateListener.LISTEN_SERVICE_STATE
				| PhoneStateListener.LISTEN_CELL_LOCATION);
		return phoneStateListener;
	}

	public synchronized void makeUserCommandsSender() {
		if (ucClient != null) {
			return;
		}
		//Additional synchronization
		if (makingUcClientThread != null) {
			return;
		}
		makingUcClientThread = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					//TODO: change port on fail start
					long mobileAgentId = getLong(ServicesConnector.MOBILE_AGENT_ID_KEY, IMobileAgent.INVALID_ID);
					ucClient = UcClientFactory.createClient(HipdriverApplication.this, mobileAgentId);
				} catch (Throwable th) {
					Log.wtf(TAG, th);
				} finally {
					makingUcClientThread = null;
				}
			}
			
		}, "CreateUCClient");
		makingUcClientThread.start();
	}
	
	public synchronized void releaseUserCommandsSender() {
		if (ucClient == null) {
			return;
		}
		try {
			Log.d(TAG, "release-user-commands-sender");
			ucClient.release();
			ucClient = null;
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	
	public synchronized void makeRemoteUserCommandsExecutor() {
		if (ucServer != null) {
			return;
		}
		//Additional synchronization
		if (makingUcServerThread != null) {
			return;
		}
		makingUcServerThread = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					//TODO: change port on fail start
					ucServer = UcServerFactory.createServer(HipdriverApplication.this);
					Thread.sleep(TimeUnit.SECONDS.toMillis(10));
					WatchdogService watchdogService = getWatchdogService();
					if (watchdogService != null) {
						watchdogService.fireChangeMobileAgentState();
					}
				} catch (Throwable th) {
					Log.wtf(TAG, th);
				} finally {
					makingUcServerThread = null;
				}
			}
			
		}, "CreateUCServer");
		makingUcServerThread.start();
	}
	
	public synchronized void shutdownRemoteUserCommandsExecutor() {
		if (ucServer == null) {
			return;
		}
		try {
			Log.d(TAG, "shutdown-ssl-server");
			ucServer.shutdown();
			ucServer = null;
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	
	private void restorePrevLifeState() {
		if (hasCheckPoint()) {
			Log.d(TAG, "detect-uncleared-checkpoint");

			appState = getStoredAppState(AppStatesEnum.CAC);
			carState = getStoredCarState(CarStatesEnum.U);
			firstDetectionCarState = getStoredFirstDetectionCarState(CarStatesEnum.U);
		} else {
			appState = AppStatesEnum.ASO;
			carState = CarStatesEnum.U;
			firstDetectionCarState = CarStatesEnum.U;
		}
		
		appFace = AppFacesEnum.valueOf(getInt(APP_FACE, AppFacesEnum.CAR_ALARMS.ordinal()));
		if (appFace == null) {
			appFace = AppFacesEnum.CAR_ALARMS;
		}
		
		controlMode = ControlModsEnum.valueOf(getInt(MainActivity.CONTROL_MODE, ControlModsEnum.INTERNET.ordinal()));
		if (controlMode == null) {
			controlMode = ControlModsEnum.INTERNET;
		}
	}

	private void startWatchdogService() {
		if (watchdogService != null) {
			return;
		}
		Intent watchdogServiceIntent = new Intent(this, WatchdogService.class);
		startService(watchdogServiceIntent);
	}
	
	private void stopWatchdogService() {
		if (watchdogService == null) {
			return;
		}
		Log.d(TAG, "stop-watchdog-service");
		watchdogService.stop();
		Intent watchdogServiceIntent = new Intent(this, WatchdogService.class);
		stopService(watchdogServiceIntent);
		watchdogService = null;
	}
	
	public void startWatchdogServiceAndWait(long timeoutMillis) {
		startWatchdogService();
		long timer = System.currentTimeMillis() + timeoutMillis;
		while (watchdogService == null && timer > System.currentTimeMillis()) {
			try {
				Thread.yield();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				return;
			}
		}
	}

	public void startBeaconServiceAndWait(long timeoutMillis) {
		startBeaconService();
		long timer = System.currentTimeMillis() + timeoutMillis;
		while (beaconService == null && timer > System.currentTimeMillis()) {
			try {
				Thread.yield();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				return;
			}
		}
	}

	private void startBeaconService() {
		if (beaconService != null) {
			return;
		}
		Intent beaconServiceIntent = new Intent(this, BeaconService.class);
		startService(beaconServiceIntent);
	}

	private void stopBeaconService() {
		if (beaconService == null) {
			return;
		}
		Log.d(TAG, "stop-beacon-service");
    	beaconService.stop();
		Intent beaconServiceIntent = new Intent(this, BeaconService.class);
		stopService(beaconServiceIntent);
		beaconService = null;
	}

	@Override
	public void onTerminate() {
	    AnalyticsAdapter.close();
	    stopWatchdogService();
	    stopBeaconService();
	    stopEyesService();
	    shutdownRemoteUserCommandsExecutor();
	    releaseUserCommandsSender();
	    pause();
	    unregisterScreenStateReceiver();
	    unregisterPhoneListener(phoneStateListener);
	    
		super.onTerminate();
	}

	private void unregisterPhoneListener(PhoneStateListener phoneStateListener) {
		if (phoneStateListener == null) {
			return;
		}
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	    if (telephonyManager == null) {
	    	return;
	    }
		telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
	}

	/**
	 * Pause main services in application
	 * @return previous value of paused flag
	 */
	public boolean pause() {
		if (isDebugMode()) {
			String logName = "ws-restart-stat.log";
			writeDebugInfo(logName, "%s,pause<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()));
		}
		return paused.getAndSet(true);
	}
	
	/**
	 * Check state of main services in application, paused or not.
	 * @return
	 */
	public boolean isPaused() {
		return paused.get();
	}
	
	/**
	 * Resume main services in application
	 * @return previous value of paused flag
	 */
	public boolean resume(long delayInMillis) {
		try {
			Thread.sleep(delayInMillis);
		} catch (InterruptedException e) { }
		boolean oldValue = paused.getAndSet(false);
		if (isDebugMode()) {
			String logName = "ws-restart-stat.log";
			writeDebugInfo(logName, "%s,resume [%s]<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()), delayInMillis);
		}
		return oldValue;
	}

	/**
	 * Resume main services immediately
	 * @return previous value of paused flag
	 */
	public boolean resume() {
		boolean oldValue = paused.getAndSet(false);
		if (isDebugMode()) {
			String logName = "ws-restart-stat.log";
			writeDebugInfo(logName, "%s,resume<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()));
		}
		return oldValue;
	}

	public String getString(String preferredKey, String defaultValue) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		if (!preferences.contains(preferredKey)) {
			return defaultValue;
		}
		return preferences.getString(preferredKey, defaultValue);
	}

	public int getInt(String preferredKey, int defaultValue) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		return preferences.getInt(preferredKey, defaultValue);
	}

	/**
	 * Get int value for pref-key or null if info is rare.
	 * Rare defined by checksum (hash-code) of rarity object.
	 * @param preferredKey preferences key
	 * @param rarity info for check if info is rare
	 * @return null if key not exist or info is rare
	 */
	public Integer getInteger(String preferredKey, Object rarity) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		//Info not persist
		if (!preferences.contains(preferredKey)) {
			return null;
		}
		//Check persist check-sum
		if (!preferences.contains(preferredKey + "$rarity")) {
			return null;
		}
		int checkSum = rarity.hashCode();
		int persistSum = getInt(preferredKey + "$rarity", 0);
		//Check actuality
		if (persistSum != checkSum) {
			return null;
		}
		return preferences.getInt(preferredKey, 0);
	}

	public int getInt(String preferencedKey) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		int defValue = (Integer) getDefaultValue(preferencedKey);
		int value = preferences.getInt(preferencedKey, defValue);
		return value;
	}
	
	public int safetyGetInt(String preferencedKey, int valueIfFail) {
		try {
			return getInt(preferencedKey);
		} catch (Throwable th) {
			return valueIfFail;
		}
	}

	public long getLong(String preferredKey, long defaultValue) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		return preferences.getLong(preferredKey, defaultValue);
	}

	/**
	 * Restore application after rough and fatal interruption.
	 * @param intent
	 */
	public void restoreApplicationState(Intent intent) {
		if (intent == null) {
			return;
		}
		AppStatesEnum prevAppState = (AppStatesEnum) intent.getSerializableExtra(EXTRA_APPLICATION_STATE);
		if (prevAppState != null) {
			setAppState(prevAppState);
	    	Log.d(TAG, "restore-app-state");
		}
		CarStatesEnum prevCarState = (CarStatesEnum) intent.getSerializableExtra(EXTRA_CAR_STATE);
		if (prevCarState != null) {
			setCarState(prevCarState);
	    	Log.d(TAG, "restore-car-state");
		}
		CarStatesEnum prevFirstDetectionCarState = (CarStatesEnum) intent.getSerializableExtra(EXTRA_FIRST_DETECTION_CAR_STATE);
		if (prevCarState != null) {
			setFirstDetectionCarState(prevFirstDetectionCarState);
	    	Log.d(TAG, "restore-first-detection-car-state");
		}
//		Boolean paused = (Boolean) intent.getSerializableExtra(EXTRA_PAUSED);
//		if (paused != null && paused) {
//			//TODO: check logic
//			pause();
//	    	Log.d(TAG, "restore-paused");
//		}
		Boolean debugMode = (Boolean) intent.getSerializableExtra(EXTRA_DEBUG_MODE);
		if (debugMode != null && debugMode) {
			this.debugMode = debugMode;
	    	Log.d(TAG, "restore-debug-mode [true]");
		}
		
	}

	public void runUserCommandOnResume() {
		IUserCommandsExecutor userCommandsExecutor = this.getUserCommandsExecutor();
		if (userCommandsExecutor == null) {
			return;
		}
//		//Not run resume if car has defined state.
//		if (getCarState() != CarStatesEnum.U) {
//			return;
//		}
		Log.d(TAG, "run-user-command-on-resume [appState = " + appState + "] [carState = " + carState + "]");
		switch(appState) {
		case U:
		case CAC:
			startService("WatchdogService", new Runnable() {
				@Override
				public void run() {
					startWatchdogServiceAndWait(TimeUnit.MINUTES.toMillis(1));
//					if (!watchdogService.isStopped()) {
//						return;
//					}
//					if (carState == CarStatesEnum.U) {
//						watchdogService.start(0);
//					} else {
//						//watchdogService.resumeAfterAlertState(carState);
//					}
				}
			});
			break;
		case BMA:
			startService("BeaconService", new Runnable() {
				@Override
				public void run() {
					startBeaconServiceAndWait(TimeUnit.MINUTES.toMillis(1));
//					if (!beaconService.isStopped()) {
//						return;
//					}
//					beaconService.start(0);
				}
			});
			break;
		case ASO:
			//Not started
			//watchdogService.stop();
			break;
		}
	}

	protected void startService(String serviceName, Runnable command) {
		Thread thread = new Thread(command);
		thread.setName(serviceName + "#start()");
		thread.start();
	}

	public boolean hasCheckPoint() {
		Date currentDate = new Date();
		Date checkpointDate = getDate(WatchdogService.WATCHDOG_SERVICE_CHECK_POINT);
		//Invalid checkpoint detection
		if (currentDate.before(checkpointDate)) {
			return false;
		}
		return currentDate.getTime() - checkpointDate.getTime() < FULL_DAY_IN_MILLIS;
	}

	public void releaseCheckpoint() {
		setDate(WatchdogService.WATCHDOG_SERVICE_CHECK_POINT, new Date(0));
	}

	public void setCheckpoint() {
		setDate(WatchdogService.WATCHDOG_SERVICE_CHECK_POINT, new Date());
	}
	
	//TODO: refactoring (migrate to different structure).
	public int getMemoryVersion() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		return preferences.getInt("memory-version", 0);
	}
	public synchronized void increaseMemoryVersion() {
		setInt("memory-version", getMemoryVersion() + 1);
	}

	public int getMemoryVersion(String preferencesKey) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		return preferences.getInt(preferencesKey + "-version", getMemoryVersion());
	}
	public synchronized void setMemoryVersion(String preferencesKey) {
		setInt(preferencesKey + "-version", getMemoryVersion() + 1);
	}

	protected void setCellNetworkAvailable(boolean isCellNetworkAvailable) {
		this.isCellNetworkAvailable = isCellNetworkAvailable;
	}

	public boolean isHasConnection() {
		return hasConnection;
	}

	protected void setHasConnection(boolean hasConnection) {
		this.hasConnection = hasConnection;
		fireChangeConnectionState(hasConnection);
	}
	
	private void fireChangeConnectionState(boolean hasConnection) {
		if (networkConnectionListener == null) {
			return;
		}
		networkConnectionListener.changeConnectionState(hasConnection);
	}

	public void setNetworkConnectionListener(NetworkConnectionListener networkConnectionListener) {
		this.networkConnectionListener = networkConnectionListener;
	}

	public boolean isCellNetworkAvailable() {
		return isCellNetworkAvailable;
	}

	public IUserCommandsExecutor getOffscreenExecutor() {
		return new IUserCommandsExecutor() {

			@Override
			public Object switchOffWatch() {
				Object returnValue = null;
				if (getAppFace() != AppFacesEnum.CAR_ALARMS) {
					return returnValue;
				}
				if ( getAppState() == AppStatesEnum.ASO ) {
					return returnValue;
				}
				//@see unbind it always be stopping service
				WatchdogService watchdogService = getWatchdogService();
				if (watchdogService != null) {
					if (watchdogService.stop()) {
						setAppState(AppStatesEnum.ASO);
						setCarState(CarStatesEnum.U);
						watchdogAlert(R.string.car_alarms_switched_off, false);
					} else {
						Log.w(TAG, "Watchdog service is running???");
						setAppState(AppStatesEnum.ASO);
						setCarState(CarStatesEnum.U);
					}
					watchdogService.fireChangeMobileAgentState();
				}
				BeaconService beaconService = getBeaconService();
				if (beaconService != null) {
					if (beaconService.stop()) {
						setAppState(AppStatesEnum.ASO);
						setCarState(CarStatesEnum.U);
						watchdogAlert(R.string.car_alarms_switched_off, false);
					} else {
						Log.w(TAG, "Beacon service is running???");
						setAppState(AppStatesEnum.ASO);
						setCarState(CarStatesEnum.U);
					}
				}
				releaseCheckpoint();
				//Sane check
				if (beaconService == null && watchdogService == null) {
					Log.w(TAG, "Sane check running???");
					setAppState(AppStatesEnum.ASO);
					setCarState(CarStatesEnum.U);
				}
				//Airplane mode switch-off automatically before run network task
				UITaskFactory.sendWatchOffEvent(HipdriverApplication.this, null);
				//Send analytics
				AnalyticsAdapter.trackSwitchOff(System.currentTimeMillis() - getLong(WATCH_ON_TIMER, 0L));
				//Synchronization for control mode
				synchronizeCurrentControlModeWithServer();
				
				if (getBoolean(SEND_LOG_AFTER_SWITCH_OFF_TO_DEVELOPERS)) {
					captureLogAndEmailing();
				}
				
				switchOnOffEmailNotification();
				
				try {
					((MainActivity) getMainActivity()).updateCarAlarmScreensState();
				} catch (Throwable th) {
					Log.wtf(TAG, th);
				}
				
				Log.d(TAG, "car-alarm-switched-off");
				return returnValue;
			}

			@Override
			public Object switchOnWatch() {
				Object returnValue = null;
				if (getAppFace() != AppFacesEnum.CAR_ALARMS) {
					return returnValue;
				}
				if (getCarState() == CarStatesEnum.A) {
					return returnValue;
				}
				WatchdogService watchdogService = getWatchdogService();
				if (watchdogService == null) {
					return returnValue;
				}
				if ( getAppState() != AppStatesEnum.ASO ) {
					return returnValue;
				}
				if (!watchdogService.isStopped()) {
					Log.d(TAG, "watchdog-service-always-be-running");
					return returnValue;
				}
				setAppState(AppStatesEnum.U);
				try {
					watchdogService.storeUserVariables();
				} catch (Throwable th) {
					Log.wtf(TAG, th);
				}
				watchdogService.start(TimeUnit.SECONDS.toMillis(getInt(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS)));
				setCheckpoint();
				
				//Switch off airplane mode  before sending network data automatically
				
				UITaskFactory.sendWatchOnEvent(HipdriverApplication.this, null);
				
				//Activate timer for tl version
				//TL Version START -->
				//if (getBoolean(IUserLimit.TIME_LIMITED)) {
				//	int delayStartInSeconds = getInt(IMobileAgentProfile.WATCHDOG_SERVICE_START_DELAY_IN_SECONDS);
				//	long delayInMillis = TimeUnit.HOURS.toMillis(2) + TimeUnit.SECONDS.toMillis(delayStartInSeconds);
				//	watchdogService.startSwitchOffTimer(delayInMillis);
				//}
				//<-- TL Version END
				
				//Send analytics
				AnalyticsAdapter.trackInWatchMode();
				//Synchronization for control mode
				synchronizeCurrentControlModeWithServer();
				
				//TODO: prevent date-time manipulations @see TICK event in TimeChangeReceiver
				setLong(WATCH_ON_TIMER, System.currentTimeMillis());
				
				switchOnOffEmailNotification();
				
				try {
					((MainActivity) getMainActivity()).updateCarAlarmScreensState();
				} catch (Throwable th) {
					Log.wtf(TAG, th);
				}
				
				Log.d(TAG, "car-alarm-switched-on");
				return returnValue;
			}

			@Override
			public void safetySetTextOutput(int textViewResourceId,
					CharSequence text, boolean append, boolean bold, boolean italic) {
				if (getAppFace() != AppFacesEnum.CAR_ALARMS) {
					return;
				}

				IUserCommandsExecutor userCommandsExecutor = getUserCommandsExecutor(); 
				if (userCommandsExecutor == null) {
					return;
				}
				if (userCommandsExecutor == this) {
					return;
				}
				//Display only for UI enabled commands executor
				if (!(userCommandsExecutor instanceof Activity)) {
					return;
				}
				userCommandsExecutor.safetySetTextOutput(textViewResourceId, text, append, bold, italic);
			}

			@Override
			public void signin() {
				//Check if not in main thread
				if (Looper.myLooper() != Looper.getMainLooper()) {
					//Run in async mode in hope then message will be run before switch-on procedure
					//Otherwise UITaskFactory.signIn always be fail.
					UITaskFactory.signInAsync(HipdriverApplication.this, new After() {
						
						@Override
						public void success(Object returnResult) {
							safetySetTextOutput(R.id.mobile_agent_name_text, getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, ""), false, true, false);
						}
						
					});
					return;
				}

				UITask task = UITaskFactory.signIn(HipdriverApplication.this, new After() {
					
					@Override
					public void success(Object returnValue) {
						safetySetTextOutput(R.id.mobile_agent_name_text, getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, ""), false, true, false);
						//Synchronization for control mode
						synchronizeCurrentControlModeWithServer();
					}
					
				});
				try {
					//Check expired, then if expired wait for response from server
					if (getDate(HipdriverApplication.EXPIRED_DATE).before(new Date())) {
						task.get(20000, TimeUnit.MILLISECONDS);
					} else {
						task.get(200, TimeUnit.MILLISECONDS);
					}
				} catch (InterruptedException e) {
					Log.wtf(TAG, e);
				} catch (ExecutionException e) {
					Log.wtf(TAG, e);
				} catch (CancellationException e) {
					Log.wtf(TAG, e);
				} catch (TimeoutException e) {
				}
			}

			@Override
			public void switchOnBeaconMode() {
				if (getAppFace() != AppFacesEnum.CAR_ALARMS) {
					return;
				}
				//Don't touch car state
				//Don't run in intermediate state 
				if (getCarState() == CarStatesEnum.A) {
					return;
				}
				if (getAppState() == AppStatesEnum.U) {
					return;
				}
				WatchdogService watchdogService = getWatchdogService();
				if (watchdogService == null) {
					return;
				}
				BeaconService beaconService = getBeaconService();
				if (beaconService == null) {
					return;
				}
				if (!beaconService.isStopped()) {
					Log.d(TAG, "beacon-service-always-be-running");
					return;
				}
				setAppState(AppStatesEnum.U);
				//Don't start two services in same time
				if (!watchdogService.isStopped()) {
					Log.d(TAG, "stop-watchdog-service-before-start-beacon-service");
					watchdogService.stop();
				}
				beaconService.start(0);
				setCheckpoint();
				setAppState(AppStatesEnum.BMA);

				//Airplane mode switch off automatically before sending networking data
				
				UITaskFactory.sendBeaconOnEvent(HipdriverApplication.this, null);
				
				//Send analytics
				AnalyticsAdapter.trackInBeaconMode();
						
				Log.d(TAG, "beacon-switched-on");
			}
			
		};
	}

	protected void switchOnOffEmailNotification() {
		//Sending network info only if app-face is car-alarms
		if (getAppFace() != AppFacesEnum.CAR_ALARMS) {
			return;
		}
		if (!getBoolean(IMobileAgentProfile.SEND_EMAIL_ON_SWITCH_ON_SWITCH_OFF)) {
			return;
		}
		final String email = getString(IMobileAgent.ALERT_EMAIL_KEY, "");
		if (email.isEmpty()) {
			return;
		}
		String mobileAgentName = getString(MainActivity.MOBILE_AGENT_NAME_EDITOR, "");
		String subj = getResources().getString(R.string.email_message_subj_alarms, mobileAgentName);
		String body = getAppState() == AppStatesEnum.ASO ?
				  getResources().getString(R.string.email_message_body_alarms_off)
				: getResources().getString(R.string.email_message_body_alarms_on);
		UITaskFactory.sendEmail(this, email, subj, body + "\n", null, 0);
	}

	public AppStatesEnum waitForChangeUnknownState() throws InterruptedException {
		while(appState == AppStatesEnum.U) {
			Thread.sleep(100);
		}
		return appState;
	}
	
	/**
	 * Long time toaster show
	 * @param resourceId string id, string can be html formated (don't forget usage quoted like this &amp;lt; instead < in xml file:)
	 */
	public void makeToastWarning(final int resourceId) {
		Handler handler = new Handler(getMainLooper());
		handler.post(new Runnable() {

			@Override
			public void run() {
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.toast_warning, null);

				TextView warningText = (TextView) layout.findViewById(R.id.warning_text);
				Spanned text = Html.fromHtml(getResources().getString(resourceId));
				warningText.setText(text);

				Toast toast = new Toast(getApplicationContext());
				toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			
		});

	}
	
	private void captureLogAndEmailing() {
		mainTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				String atachName = "last.log";
				String contentType = "text/plain";
				String text = FileTools.getLastLogEvents(TextTools.BIG_TAG, 65535);
				byte[] bytes = text.getBytes();
				try {
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					GZIPOutputStream zos = new GZIPOutputStream(bos);
					try {
						zos.write(bytes);
						zos.finish();
						//Replace to compressed object
						bytes = bos.toByteArray();
						atachName = "last.log.gz";
						contentType = "application/x-gzip";
					} finally {
						zos.close();
					}
				} catch (Throwable th) {
					Log.wtf(TAG, th);
				}
				int userId = getInt(ServicesConnector.USER_ID_KEY, 0);
				long mobileAgentId = getLong(ServicesConnector.MOBILE_AGENT_ID_KEY, 0);
				String applicationVersion = Hardwares.getApplicationVersion(HipdriverApplication.this);
				StringBuilder bodyText = new StringBuilder(Hardwares.getAndroidInfo());
				bodyText.append('\n').append("See atach, for log info.");
				String subject = String.format("LOG FROM [%s][%s] %s", userId, mobileAgentId, applicationVersion);
				if (bytes.length == 0) {
					UITaskFactory.sendEmail(HipdriverApplication.this, TECH_SUPPORT_EMAIL, subject, bodyText.toString(), null, 0);
					return;
				}
				int messageId = UITaskFactory.sendEmail(HipdriverApplication.this, TECH_SUPPORT_EMAIL, subject, bodyText.toString(), null, 1);
				UITaskFactory.sendAtachsToEmail(HipdriverApplication.this, messageId, 0, StreamTools.makeDataSource(bytes, atachName, contentType));
			}
			
		}, 1000);
	}
	
	
	public IUserCommandsExecutor getDefaultRemoteExecutor() {
		return new IUserCommandsExecutor() {
			
			@Override
			public Object switchOnWatch() {
				Object returnValue = null;
				if (remoteAppState != AppStatesEnum.ASO) {
					return returnValue;
				}
				ControlModsEnum controlMode = getControlMode(); 
				switch (controlMode) {
			    case INTERNET:
					long targetMobileAgentId = getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
					returnValue = ucClient.execCommand(targetMobileAgentId, UserCommandTypesEnum.ACA, null);
			    	break;
			    case PHONE_CALLS:
			    case SMS:
					String phoneNumber = getString(getSelectedDeviceForRemoteControlPhoneNumberPrevKey(), "");
					if (phoneNumber.isEmpty()) {
						makeToastWarning(R.string.no_phone_number_for_remote_agent);
						return returnValue;
					}
					WatchdogService.makeOutboundCall(HipdriverApplication.this, phoneNumber, TimeUnit.SECONDS.toMillis(10));
			    	break;
				}
				
				remoteAppState = AppStatesEnum.CAC;
				return returnValue;
			}
			
			@Override
			public void switchOnBeaconMode() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public Object switchOffWatch() {
				Object returnValue = null;
				if (remoteAppState == AppStatesEnum.ASO) {
					return returnValue;
				}
				ControlModsEnum controlMode = getControlMode();
				switch (controlMode) {
			    case INTERNET:
					long targetMobileAgentId = getLong(MainActivity.SELECTED_DEVICE_ID, IMobileAgent.INVALID_ID);
					returnValue = ucClient.execCommand(targetMobileAgentId, UserCommandTypesEnum.SOCA, null);
			    	//TODO: implement basic control mode
			    	break;
			    case PHONE_CALLS:
			    case SMS:
					String phoneNumber = getString(getSelectedDeviceForRemoteControlPhoneNumberPrevKey(), "");
					if (phoneNumber.isEmpty()) {
						makeToastWarning(R.string.no_phone_number_for_remote_agent);
						return returnValue;
					}
					WatchdogService.makeOutboundCall(HipdriverApplication.this, phoneNumber, TimeUnit.SECONDS.toMillis(10));
			    	break;
				}
				
				remoteAppState = AppStatesEnum.ASO;
				return returnValue;
			}
			
			@Override
			public void safetySetTextOutput(int textViewResourceId, CharSequence text,
					boolean append, boolean bold, boolean italic) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void signin() {
				// TODO Auto-generated method stub
				
			}
		};
	}
	
	public String getSelectedDeviceForRemoteControlPhoneNumberPrevKey() {
		int selectedDeviceNameHash = getString(MainActivity.SELECTED_DEVICE_NAME, "").hashCode(); 
		final String prefKeyName = MainActivity.SELECTED_DEVICE_PHONE_NUMBER + "_" + selectedDeviceNameHash;
		return prefKeyName;
	}

	public int increaseShowCount(String preferencesKey) {
		int showCount = getShowCount(preferencesKey) + 1;
		setInt(preferencesKey + "-show-count", showCount);
		return showCount;
	}

	public void clearShowCount(String preferencesKey) {
		setInt(preferencesKey + "-show-count", 0);
	}

	public int getShowCount(String preferencesKey) {
		return getInt(preferencesKey + "-show-count", 0);
	}

	//TODO: Connect to GSM network if system in air-plane mode
	//final AirplaneMode airplaneMode = new AirplaneMode(getA());
	public Future<?> runNetworkTask(Runnable command, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		lockGsmReceiver();
		try {
			if (isInterrupted()) {
				Log.d(TAG, "network-task-interrupted");
				return null;
			}
			//invokeAll() blocks until ALL tasks submitted to executor complete @see http://stackoverflow.com/questions/1322147/java-executors-wait-for-task-termination
			Future<?> future = networkTasksExecutorService.submit(command);
			if (future == null) {
				return null;
			}
			future.get(timeout, unit);
			Log.d(TAG, "network-task-complete");
			return future;
		} finally {
			unlockGsmReceiver();
		}
	}
	
	private void unlockGsmReceiver() {
		int count = gsmReciverSemaphor.decrementAndGet();
		if (count > 0) {
			return;
		}
		//Start shutdown resource task
		Thread shutdownGsmReceiverThread = new Thread(new Runnable() {

			@Override
			public void run() {
				if (getCarState() != CarStatesEnum.U) {
					return;
				}
				if (getAppState() == AppStatesEnum.ASO) {
					return;
				}
				try {
					Thread.sleep(TimeUnit.SECONDS.toMillis(31));
				} catch (InterruptedException e) {
					return;
				}
				if (Thread.interrupted()) {
					return;
				}
				new AirplaneMode(HipdriverApplication.this).switchOnInternal();
			}
			
		}, "Shutdown GSM Receiver Thread");
		Thread thread = shutdownGsmReceiverThreadRef.getAndSet(shutdownGsmReceiverThread); 
		if (thread != null && !thread.isInterrupted()) {
			thread.interrupt();
		}
		shutdownGsmReceiverThread.start();
	}

	private void lockGsmReceiver() {
		int count = gsmReciverSemaphor.incrementAndGet();
		if (count > 1) {
			return;
		}
		//Cancel delayed task for shutdown resource
		Thread thread = shutdownGsmReceiverThreadRef.getAndSet(null);
		if (thread != null && !thread.isInterrupted()) {
			thread.interrupt();
		}
		//Start switch-on power task for resource
		new AirplaneMode(this).switchOffInternal();
	}

	//Fix ANR issue, respect for user Serggg1212 from 4pda
	public Future<?> runPhoneTask(final Runnable command) {
		if (isInterrupted()) {
			Log.d(TAG, "phone-task-interrupted");
			return null;
		}
		Log.d(TAG, "submit-phone-task");
		return phoneTasksExecutorService.submit(new Runnable() {

			@Override
			public void run() {
				lockGsmReceiver();
				try {
					command.run();
				} finally {
					unlockGsmReceiver();
				}
				
			}
			
		});
	}

    public Future<?> runAlertTask(final Runnable command) {
        if (isInterrupted()) {
            Log.d(TAG, "alert-task-interrupted");
            return null;
        }
        Log.d(TAG, "submit-alert-task");
        return alertTasksExecutorService.submit(new Runnable() {

            @Override
            public void run() {
                lockGsmReceiver();
                try {
                    command.run();
                } finally {
                    unlockGsmReceiver();
                }

            }

        });
    }


    public ScheduledFuture<?> schedulePhoneTask(final Runnable command, long delayInMillis) {
		if (isInterrupted()) {
			Log.d(TAG, "schedule-phone-task-interrupted");
			return null;
		}
		Log.d(TAG, "schedule-phone-task");
		return phoneTasksExecutorService.schedule(new Runnable() {

			@Override
			public void run() {
				lockGsmReceiver();
				try {
					command.run();
				} finally {
					unlockGsmReceiver();
				}
				
			}
			
		}, delayInMillis, TimeUnit.MILLISECONDS);
	}
	
	public void waitForCellNetworkConnection(long millis) {
		try {
			int count = (int)(millis / 200) + 1;
			while (!isCellNetworkAvailable() && count-- > 0) {
				Thread.sleep(200);
			}
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		}
	}
	
	public void waitForInternetConnection(long millis) {
		try {
			int count = (int)(millis / 200) + 1;
			while (!Hardwares.isOnline(this) && count-- > 0) {
				Thread.sleep(200);
			}
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		}
	}

	public void waitForUcClientMaking(long millis) {
		try {
			int count = (int)(millis / 200) + 1;
			while (ucClient == null && count-- > 0) {
				Thread.sleep(200);
			}
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		}
	}

	public void waitForUcServerMaking(long millis) {
		try {
			int count = (int)(millis / 200) + 1;
			while (ucServer == null && count-- > 0) {
				Thread.sleep(200);
			}
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		}
	}

	public boolean isDebugInfoLogHeadered(String logName) {
		Boolean isHeadered = getDebugInfoHeaderMaking().get(logName);
		return isHeadered != null && isHeadered;
	}
	
	public void writeDebugHeader(String logName, String header) {
		Writer writer = getWriter(logName, false);
		if (writer == null) {
			return;
		}
		getDebugInfoHeaderMaking().put(logName, true);		
		try {
			final String logRecord = header;
			writer.append(logRecord);
			writer.flush();
			writer.close();
		} catch (IOException e) { }
	}
	
	public void writeDebugInfo(String logName, String format, Object... args) {
		Writer writer = getWriter(logName, true);
		if (writer == null) {
			return;
		}
		try {
			final String logRecord;
			if (args.length == 0) {
				logRecord = format;
			} else {
				logRecord = String.format(format, args);
			}
			writer.append(logRecord);
			writer.flush();
			writer.close();
		} catch (IOException e) { }
	}

	private Writer getWriter(String logName, boolean append) {
		final String prefKeyName = WatchdogService.WATCHDOG_SERVICE_SAVE_INTO_FOLDER_KEY;
		String folderName = getString(prefKeyName, "");
		File folder = new File(folderName);
		if (!folder.exists()) {
			return null;
		}
		if(!folder.isDirectory()) {
			return null;
		}
		try {
			File log = new File(folder, logName);
			Writer writer = new BufferedWriter(new FileWriter(log, append));
			return writer;
		} catch (IOException e) {
			Log.wtf(TAG, e);
			return null;
		}
	}
	
	public boolean isLogExist(String logName) {
		final String prefKeyName = WatchdogService.WATCHDOG_SERVICE_SAVE_INTO_FOLDER_KEY;
		String folderName = getString(prefKeyName, "");
		File folder = new File(folderName);
		if (!folder.exists()) {
			return false;
		}
		if(!folder.isDirectory()) {
			return false;
		}
		File log = new File(folder, logName);
		return log.exists();
	}
	
	private Map<String, Boolean> getDebugInfoHeaderMaking() {
		if (debugInfoHeaderMaking != null) {
			return debugInfoHeaderMaking;
		}
		debugInfoHeaderMaking = Collections.synchronizedMap(new HashMap<String, Boolean>());
		return debugInfoHeaderMaking;
	}

	public void makeDebugInfoHeaderIfNotHeadered(String logName, String header) {
		//Don't rewrite info between application restarting
		if (isLogExist(logName)) {
			//TODO: write header if header not exist in file
			return;
		}
		if (isDebugInfoLogHeadered(logName)) {
			return;
		}
		writeDebugHeader(logName, header);
	}

	public void watchdogAlert(int stringResourceId, boolean append) {
		int invariantId = CCRI.getInvariantId(stringResourceId);
		if (invariantId != lastWatchdogStatusIid) { 
			setInt(MainActivity.LAST_WATCHDOG_STATUS_IID, invariantId);
		}
		String status = getResources().getString(stringResourceId);
		watchdogAlert(status, append);
	}
	
	public void watchdogAlert(String status, boolean append) {
		IUserCommandsExecutor userCommandsExecutor = getUserCommandsExecutor(); 
		if (userCommandsExecutor == null) {
			return;
		}
		userCommandsExecutor.safetySetTextOutput(R.id.car_alarms_status_text, status, append, false, false);
		//Store last output
		if (status != null && !status.equals(lastWatchdogStatus)) { 
			setString(MainActivity.LAST_WATCHDOG_STATUS, status);
		}
	}

	public boolean isSignup() {
		int userId = getInt(ServicesConnector.USER_ID_KEY, 0);
        if (userId == 0) {
        	return false;
        }
		long mobileAgentId = getLong(ServicesConnector.MOBILE_AGENT_ID_KEY, 0L);
		if (mobileAgentId == 0L) {
			return false;
		}
		return true;
	}

	public AppStatesEnum getStoredAppState(AppStatesEnum appStateIfNoState) {
		int ordinal = getInt(APP_STATE, -1);
		if (ordinal == -1) {
			return appStateIfNoState;
		}
		AppStatesEnum[] values = AppStatesEnum.values();
		if (ordinal > values.length - 1) {
			return appStateIfNoState;
		}
		return values[ordinal];
	}

	public CarStatesEnum getStoredCarState(CarStatesEnum carStateIfNoState) {
		int ordinal = getInt(CAR_STATE, -1);
		if (ordinal == -1) {
			return carStateIfNoState;
		}
		CarStatesEnum[] values = CarStatesEnum.values();
		if (ordinal > values.length - 1) {
			return carStateIfNoState;
		}
		return values[ordinal];
	}
	
	public CarStatesEnum getStoredFirstDetectionCarState(CarStatesEnum carStateIfNoState) {
		int ordinal = getInt(FIRST_DETECTION_CAR_STATE, -1);
		if (ordinal == -1) {
			return carStateIfNoState;
		}
		CarStatesEnum[] values = CarStatesEnum.values();
		if (ordinal > values.length - 1) {
			return carStateIfNoState;
		}
		return values[ordinal];
	}

	public void setEyesService(EyesService eyesService) {
		this.eyesService = eyesService;
	}

	public EyesService getEyesService() {
		return eyesService;
	}
	
	private void startEyesService() {
		if (eyesService != null) {
			return;
		}
		Intent eyesServiceIntent = new Intent(this, EyesService.class);
		startService(eyesServiceIntent);
	}
	
	private void stopEyesService() {
		if (eyesService == null) {
			return;
		}
		Log.d(TAG, "stop-eyes-service");
		eyesService.stop();
		Intent eyesServiceIntent = new Intent(this, EyesService.class);
		stopService(eyesServiceIntent);
		eyesService = null;
	}
	
	public boolean startEyesServiceAndWait(long timeoutMillis) {
		startEyesService();
		long timer = System.currentTimeMillis() + timeoutMillis;
		while (eyesService == null && timer > System.currentTimeMillis()) {
			try {
				Thread.yield();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				return eyesService != null;
			}
		}
		return eyesService != null;
	}
	
	private void handleBatteryPct(float batteryPct) {
		if (batteryHandlerFlags == null) {
			return;
		}
		if (batteryPct < 60f && batteryHandlerFlags.notHandled60Pct) {
			PowerEventsReceiver.sendStateEvent(this, getWatchdogService());
			batteryHandlerFlags.notHandled60Pct = false;
		}
		if (batteryPct < 30f && batteryHandlerFlags.notHandled30Pct) {
			PowerEventsReceiver.sendStateEvent(this, getWatchdogService());
			PowerEventsReceiver.sendEmailOnLowBattery(this, batteryPct);
			batteryHandlerFlags.notHandled30Pct = false;
		}
		if (batteryPct < 18f && batteryHandlerFlags.notHandledLowLevel) {
			//PowerEventsReceiver.sendEmailOnLowBattery(this, batteryPct);
			PowerEventsReceiver.sendStateEvent(this, getWatchdogService());
			updateRemoteAgentStateIfCarAlarmsInInternetControlMode();
			batteryHandlerFlags.notHandledLowLevel = false;
		}
	}
	
	private void updateRemoteAgentStateIfCarAlarmsInInternetControlMode() {
		if (getAppFace() != AppFacesEnum.CAR_ALARMS) {
			return;
		}
		if (getControlMode() != ControlModsEnum.INTERNET) {
			return;
		}
		WatchdogService watchdogService = getWatchdogService();
		if (watchdogService == null) {
			Log.w(TAG, "can-not-update-remote-agent-state-if-car-alarms-in-internet-control-mode[watchdog-service is null]");
			return;
		}
		watchdogService.fireChangeMobileAgentState();
	}

	public void handleUnplugPowerCable() {
		if (batteryHandlerFlags == null) {
			return;
		}
		batteryHandlerFlags.notHandled60Pct = true;
		batteryHandlerFlags.notHandled30Pct = true;
		batteryHandlerFlags.notHandledLowLevel = true;
	}

	/**
	 * Increase hours to live after installation, without sleep and time of power-off.
	 */
	public synchronized void increaseHtl() {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = preferences.edit();
		int htl = getHtl();
		editor.putInt("htl", htl + 1);
		editor.commit();
	}
	
	/**
	 * Get hours to live.
	 * @return zero value for first hour after installation.
	 */
	public int getHtl() {
		return getInt("htl", 0);
	}

	public synchronized void setRemoteAppState(AppStatesEnum remoteAppState) {
		this.remoteAppState = remoteAppState;
	}

	public AppStatesEnum getRemoteAppState() {
		return remoteAppState;
	}

	public ISslClient getUcClient() {
		return ucClient;
	}

	public ISslServer getUcServer() {
		return ucServer;
	}

	public boolean isAlarmActive() {
		return isAlarmActive;
	}

	public void setAlarmActive(boolean isAlarmActive) {
		this.isAlarmActive = isAlarmActive;
	}

	public boolean isNotFirstStartOfMainActivity() {
		return notFirstStartOfMainActivity;
	}

	public void setNotFirstStartOfMainActivity(boolean notFirstStartOfMainActivity) {
		this.notFirstStartOfMainActivity = notFirstStartOfMainActivity;
	}

	public void interrupt() {
		interrupted = true;
	}

	public boolean isInterrupted() {
		return interrupted;
	}
	
	protected void clearInterruptFlag() {
		interrupted = false;
	}
	
	public void switchControlModeIfChanged() {
		if (getAppFace() != AppFacesEnum.CAR_ALARMS) {
			return;
		}
		long mobileAgentId = getLong(
				ServicesConnector.MOBILE_AGENT_ID_KEY, IMobileAgent.INVALID_ID);
		if (mobileAgentId == IMobileAgent.INVALID_ID) {
			Log.w(TAG, "invalid_mobile_agent_id");
			return;
		}
		
		UITaskFactory.getMap(this, new After() {
			
			@Override
			public void success(Object returnValue) {
				Properties properties = (Properties) returnValue;
				Object controlModeId = properties.get(IMobileAgentProfile.CONTROL_MODE_ID);
				if (controlModeId == null) {
					return;
				}
				String controlModeIdText = String.valueOf(controlModeId);
				short controlModeIdShort = -1;
				try {
					controlModeIdShort = Short.parseShort(controlModeIdText);
				} catch (NumberFormatException e) {
					return;
				}
				ControlModsEnum controlModeFromServer = DBLinks.unlinkControlMode(controlModeIdShort);
				if (controlModeFromServer == null) {
					return;
				}
				if (controlModeFromServer == getControlMode()) {
					Log.d(TAG, "control-mode-not-changed");
					return;
				}
				switch (controlModeFromServer) {
				case INTERNET:
					makeRemoteUserCommandsExecutor();
					break;
				case PHONE_CALLS:
				case SMS:
					shutdownRemoteUserCommandsExecutor();
					break;
				}
				setControlMode(controlModeFromServer);
				Log.d(TAG, String.format("switch-to-control-mode[%s]", controlModeFromServer));
			}
			
		}, mobileAgentId);
	}
	
	public void makeNotificationForMainActivity(int drawableResourceId, int drawableSmallIconId, CharSequence contentTitle, CharSequence contentText, String prefKeyForCheckBox) {
	    long when = System.currentTimeMillis();
	    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	    Intent intent = new Intent(this, ShowMessageActivity.class);
	    // set intent so it does not start a new activity
	    Context context = getMainActivity();
	    if (context == null) {
	    	context = getApplicationContext();
	    }
	    intent.putExtra(ShowMessageActivity.NOTIFICATION_MESSAGE, contentText);
	    //notificationIntent.
	    if (prefKeyForCheckBox != null) {
	    	intent.putExtra(ShowMessageActivity.NOTIFICATION_MESSAGE_PREF_KEY, prefKeyForCheckBox);
	    }
	    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	    Notification notification;
	        //if (Build.VERSION.SDK_INT < 11) {
	            notification = new Notification(drawableResourceId, contentTitle, when);
	            notification.setLatestEventInfo(
	            		context,
	                    contentTitle,
	                    contentText,
	                    pendingIntent);
	        //} else {
	        //    notification = new Notification.Builder(this)
	        //            .setContentTitle("Title")
	        //            .setContentText(
	        //                    "Text").setSmallIcon(drawableSmallIconId)
	        //            .setContentIntent(intent).setWhen(when).setAutoCancel(true)
	        //            .build();
	        //}
	    notification.flags |= Notification.FLAG_AUTO_CANCEL;
	    notification.defaults |= Notification.DEFAULT_SOUND;
	    notificationManager.notify(1, notification);
	}

	/**
	 * Show html formated message
	 * @param message html text or plain text
	 * @param prefKeyForCheckBox preference key, if null check-box not show, otherwise show with message (Don't show this message again)
	 * result will be store into preference key as boolean.
	 * @param activeText clickable text
	 */
    public void showMessage(final CharSequence message, final String prefKeyForCheckBox, final ActiveText activeText) {
	    Intent intent = new Intent(this, ShowMessageActivity.class);
	    // set intent so it does not start a new activity
	    Context context = getMainActivity();
	    if (context == null) {
	    	context = getApplicationContext();
	    }
	    intent.putExtra(ShowMessageActivity.NOTIFICATION_MESSAGE, message);
	    if (prefKeyForCheckBox != null) {
	    	intent.putExtra(ShowMessageActivity.NOTIFICATION_MESSAGE_PREF_KEY, prefKeyForCheckBox);
	    }
	    if (activeText != null) {
	    	intent.putExtra(ShowMessageActivity.NOTIFICATION_ACTIVE_TEXT, activeText);
	    }
        try {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Throwable th) {
            Log.wtf(TAG, th);
        }
	}
    
    //public void showMessage(CharSequence message) {
    //	showMessage(message, null, null);
    //}
    
    public void showMessage(int resourceId) {
    	CharSequence message = Html.fromHtml(getResources().getString(resourceId));
    	showMessage(message, null, null);
    }
    
	

	public Activity getMainActivity() {
		return mainActivity;
	}

	public void setMainActivity(Activity mainActivity) {
		this.mainActivity = mainActivity;
	}

}
