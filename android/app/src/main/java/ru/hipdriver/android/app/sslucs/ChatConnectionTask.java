package ru.hipdriver.android.app.sslucs;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoop;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.net.InetAddress;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.android.app.Hardwares;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.ISid;
import ru.hipdriver.util.ChatMsgComposer;
import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

abstract class ChatConnectionTask implements Runnable {

	protected static final String NO_DEST_CH = "NO_DEST_CH";
	protected static final String INVALID_REQUEST = "INVALID_REQUEST";
	protected static final String ERROR = "ERROR";
	public static final int DEFAULT_APERTURE_OF_NETWORK = 256;//bytes

	private static final String TAG = TextTools
			.makeTag(ChatConnectionTask.class);

	private static final Random random = new Random();

	protected final InetAddress addr;
	protected final int port;
	protected final long mobileAgentId;

	protected long nextRetryDelayInMillis = TimeUnit.SECONDS.toMillis(3);

	private ObjectMapper cachedOm;

	private int tid;
	private static final int NO_TID = -1;

	private Bootstrap bootstrap;

	private Channel currentChannel;
	
	private final String registrationMsg;
	
	private final BlockingQueue<Runnable> tasks;
	
	private final AtomicBoolean queueHasReconnectTask;
	
	protected final Context context;
	
	private boolean firstMessageReceived;

	public ChatConnectionTask(Context context, long mobileAgentId,
			InetAddress chatServerAddress, int port) {
		this.addr = chatServerAddress;
		this.port = port;
		this.mobileAgentId = mobileAgentId;
		//TODO: include registration key
		this.registrationMsg = ChatMsgComposer.makeMessage(mobileAgentId,
				1, ISid.INVALID_COMPONENT_SESSIONS_ID ^ System.currentTimeMillis(), "REGISTRATION-REQUEST");
		this.tasks = new ArrayBlockingQueue<Runnable>(32);
		this.context = context;
		this.queueHasReconnectTask = new AtomicBoolean();
	}

	@Override
	public void run() {
		tid = android.os.Process.myTid();
		EventLoopGroup group = makeEventLoopGroup();
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		WakeLock  wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
				"Lock by chart-server-connection");
		wakeLock.acquire();
		try {
			Log.d(TAG, "start-chat-connector");
			bootstrap = new Bootstrap().group(group)
					.channel(NioSocketChannel.class)
					.handler(new ChatClientInitializer(this));

			increaseThreadPriority(tid); 
			
			connect();
			//SO_KEEPALIVE emulation
			soKeepAliveEmulation(group);
			startTaskRunningLoop();
		} catch (InterruptedException e) {
			Log.wtf(TAG, e);
		} finally {
			tasks.clear();
			group.shutdownGracefully();
			tid = NO_TID;
			wakeLock.release();
			onStop();
		}
	}

	private EventLoopGroup makeEventLoopGroup() {
		int tryCount = 3;
		Throwable lastThrowable = null;
		while (tryCount-- > 0) {
			try {
				lastThrowable = null;
				return new NioEventLoopGroup(1);
			} catch (Throwable th) {
				lastThrowable = th;
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				return null;
			}
		}
		Log.wtf(TAG, lastThrowable);
		throw new RuntimeException(lastThrowable);
	}

	protected void onStop() {
	}

	private void startTaskRunningLoop() throws InterruptedException {
		while(!Thread.interrupted()) {
			//Thread.sleep(TimeUnit.SECONDS.toMillis(1));
			Runnable task;
			while ((task = tasks.take()) != null) {
				if (checkStopCondition()) {
					return;
				}
				if (Thread.interrupted()) {
					return;
				}
				try {
					task.run();
				} catch (Throwable th) {
					Log.wtf(TAG, th);
				}
			}
		}
	}

	abstract protected boolean checkStopCondition();

	private void soKeepAliveEmulation(EventLoopGroup group) {
		group.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				if (currentChannel == null) {
					return;
				}
				if (!currentChannel.isOpen()) {
					return;
				}
				currentChannel.writeAndFlush("\n");
			}
		}, 3, 3, TimeUnit.MINUTES);
	}

	private void increaseThreadPriority(int tid) {
		//Increase thread priority
		int threadPriority = android.os.Process.getThreadPriority(tid);
		android.os.Process.setThreadPriority(tid, threadPriority + 1);
	}

	protected void connect() {
		ChannelFuture connect = bootstrap.connect(addr, port);
		setupReconnectBehavior(connect);
		try {
			currentChannel = connect.channel();
			connect.sync().channel();
		} catch (Throwable th) {
			addReconnectTaskToQueue(currentChannel);
			Log.wtf(TAG, th);
			Log.d(TAG, String.format("next-reconnect-timeout[%sms]", nextRetryDelayInMillis));
		}
	}
	
	public void reconnect() {
		if (currentChannel == null) {
			return;
		}
		reconnect(currentChannel);
	}


	// @see
	// http://stackoverflow.com/questions/19739054/whats-the-best-way-to-reconnect-after-connection-closed-in-netty
	protected void reconnect(final Channel channel) {
		if (channel == null) {
			throw new IllegalArgumentException();
		}
		if (channel.isOpen()) {
			channel.close();
		}
		ChannelFuture connect = bootstrap.connect(addr, port);
		setupReconnectBehavior(connect);
		try {
			currentChannel = connect.channel();
			connect.sync().channel();
		} catch (Throwable th) {
			addReconnectTaskToQueue(channel);
			Log.wtf(TAG, th);
			Log.d(TAG, String.format("next-reconnect-timeout[%sms]", nextRetryDelayInMillis));
		}
	}

	private void setupReconnectBehavior(ChannelFuture connect) {
		connect.addListener(new ChannelFutureListener() {

			@Override
			public void operationComplete(ChannelFuture future)
					throws Exception {
				if (!future.isSuccess()) {
					return;
				}
				currentChannel.writeAndFlush(registrationMsg);
			}
			
		});
		connect.channel().closeFuture().addListener(new ChannelFutureListener() {
			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				if (!isRunning()) {
					Log.d(TAG, "connector-thread-not-running");
					return;
				}
				final Channel channel = future.channel();
				EventLoop eventLoop = channel.eventLoop();
				if (eventLoop == null) {
					Log.d(TAG, "channel-always-be-unregistered");
					return;
				}
				
				addReconnectTaskToQueue(channel);
			}
		});
	}

	protected void sendMessage(long mobileAgentIdTo, long componentSessionsId, String body) {
		if (!isRunning()) {
			throw new IllegalStateException("Connection not establishement");
		}
		String msg = ChatMsgComposer.makeMessage(mobileAgentId,
				mobileAgentIdTo, componentSessionsId, body);
		currentChannel.writeAndFlush(msg);
		Log.d(TAG, "send-mesage-with-length :" + msg.getBytes().length + "bytes");
	}

	public void setNextRetryDelay(long nextRetryDelay, TimeUnit unit) {
		this.nextRetryDelayInMillis = unit.toMillis(nextRetryDelay);
	}

	protected abstract SimpleChannelInboundHandler<String> makeChatClientHandler();

	protected long getNextRequestId() {
		return random.nextLong();
	}

	protected boolean isRunning() {
		return tid != NO_TID;
	}

	protected ObjectMapper getObjectMapper() {
		if (cachedOm != null) {
			return cachedOm;
		}
		synchronized (this) {
			if (cachedOm != null) {
				return cachedOm;
			}
			cachedOm = new ObjectMapper();
			return cachedOm;
		}
	}
	
	protected boolean isConnected() {
		return currentChannel != null && currentChannel.isOpen() && firstMessageReceived;
	}

	protected void addReconnectTaskToQueue(final Channel channel) {
		if (queueHasReconnectTask.get() && tasks.size() > 0) {
			return;
		}
		tasks.add(new Runnable() {

			@Override
			public void run() {
				queueHasReconnectTask.set(false);
				try {
					Thread.sleep(nextRetryDelayInMillis);
					//After first success transfer data through of channel nextRetryDelayInMillis reset to initial value 
					nextRetryDelayInMillis = Math.round((float) nextRetryDelayInMillis * 1.2f);
				} catch (InterruptedException e) {
					return;
				}
				if (currentChannel != null && currentChannel.isOpen()) {
					return;
				}
				if (!Hardwares.isNetworkConnectedOrConnecting(context)) {
					Log.d(TAG, "network-not-active-try-to-reconnect[" + nextRetryDelayInMillis + "]millis");
					addReconnectTaskToQueue(channel);
					return;
				}
				reconnect(channel);
			}
			
		});
		queueHasReconnectTask.set(true);
	}

	public boolean isFirstMessageReceived() {
		return firstMessageReceived;
	}

	public void setFirstMessageReceived(boolean firstMessageReceived) {
		this.firstMessageReceived = firstMessageReceived;
	}

}

class ChatClientInitializer extends ChannelInitializer<SocketChannel> {

	private final ChatConnectionTask chatConnectionTask;

	public ChatClientInitializer(ChatConnectionTask chatConnectionTask) {
		this.chatConnectionTask = chatConnectionTask;
	}

	@Override
	protected void initChannel(SocketChannel arg0) throws Exception {
		ChannelPipeline pipeline = arg0.pipeline();

		pipeline.addLast("framer", new DelimiterBasedFrameDecoder(8192,
				Delimiters.lineDelimiter()));
		pipeline.addLast("decoder", new StringDecoder());
		pipeline.addLast("encoder", new StringEncoder());

		pipeline.addLast("handler", chatConnectionTask.makeChatClientHandler());

	}

}

class DueTimeException extends RuntimeException {

	/**
	 * Unique id for serialization/deserialization procedure
	 */
	private static final long serialVersionUID = 4639481953925748723L;

}

abstract class ChatClientHandler<A extends ChatConnectionTask> extends
		SimpleChannelInboundHandler<String> {

	private static final String TAG = TextTools
			.makeTag(ChatClientHandler.class);

	protected final A chatConnectionTask;

	public ChatClientHandler(A chatConnectionTask) {
		this.chatConnectionTask = chatConnectionTask;
	}

	@Override
	public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
		Log.d(TAG, "channel-unregistered");
		chatConnectionTask.addReconnectTaskToQueue(ctx.channel());
		super.channelUnregistered(ctx);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		Log.d(TAG, "channel-exception-caught");
		Log.wtf(TAG, cause);
		super.exceptionCaught(ctx, cause);
	}

	@Override
	public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
		super.channelRegistered(ctx);
	}

	@Override
	public boolean acceptInboundMessage(Object msg) throws Exception {
		//Reset reconnect timeout after success incomming message processing
		chatConnectionTask.setNextRetryDelay(3, TimeUnit.SECONDS);		
		return super.acceptInboundMessage(msg);
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		//Reset reconnect timeout after success incomming message processing
		chatConnectionTask.setNextRetryDelay(3, TimeUnit.SECONDS);		
		super.channelRead(ctx, msg);
	}
	
//	private void reconnectIfRunning(ChannelHandlerContext ctx) {
//		if (!chatConnectionTask.isRunning()) {
//			return;
//		}
//		EventLoop eventLoop = ctx.channel().eventLoop();
//		final EventLoopGroup group = eventLoop.parent();
//		eventLoop.schedule(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					chatConnectionTask.reconnect(group);
//				} catch (InterruptedException e) {
//					Log.wtf(TAG, e);
//				}
//			}
//		}, chatConnectionTask.nextRetryDelayInMillis, TimeUnit.MILLISECONDS);
//	}
	
	

}
