package ru.hipdriver.android.app;

public class DeviceInfo {

	private final String deviceName;
	private final long mobileAgentId;

	public DeviceInfo(String deviceName, long mobileAgentId) {
		this.deviceName = deviceName;
		this.mobileAgentId = mobileAgentId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public String toString() {
		return deviceName;
	}
}