package ru.hipdriver.android.app.sslucs;

import java.lang.Thread.UncaughtExceptionHandler;
import java.net.InetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.app.Hardwares;
import ru.hipdriver.android.app.HipdriverThreadFactory;
import ru.hipdriver.android.util.TextTools;
import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

abstract class ChatConnector<A extends ChatConnectionTask> {

	protected static final String TAG = TextTools.makeTag(ChatConnector.class);
	
	private final long mobileAgentId;
	private final int chatServerPort;
	private final String chatServerHostName;
	private final Context context;
	
	private Throwable lastConnectionThrowable;
	private Thread connectionThread;
	private Thread dnsConnectionThread;
	
	protected A chatConnectionTask;
	
	private ExecutorService connectorTasksExecutorService;
	
	private class SleepingPolicy {
		
		private final int maxTryCount0 = 10;
		private final long sleepingTimeInMillis0 = TimeUnit.SECONDS.toMillis(10);

		private final long sleepingTimeInMillis1 = TimeUnit.MINUTES.toMillis(10);

		private final int maxTrySeriesCount = 3;
		private final long sleepingTimeInMillis2 = TimeUnit.SECONDS.toMillis(10);
		
		private int tryCount;
		//Suppress first series after zero stage
		private int trySeriesCount = maxTrySeriesCount;
		
		public long nextSleepingTimeInMillis() {
			tryCount++;
			if (tryCount == 1) {
				return 0;
			}
			if (tryCount < maxTryCount0) {
				return sleepingTimeInMillis0;
			}
			if (trySeriesCount < maxTrySeriesCount) {
				trySeriesCount++;
				return sleepingTimeInMillis2;
			}
			trySeriesCount = 0;
			return sleepingTimeInMillis1;
		}
		
		public void reset() {
			tryCount = 0;
			trySeriesCount = maxTrySeriesCount;
		}
		
	}
	private SleepingPolicy sleepingPolicy = new SleepingPolicy();
	

	public ChatConnector(Context context, long mobileAgentId, String chatServerHostName, int chatServerPort) {
		this.mobileAgentId = mobileAgentId;
		this.chatServerPort = chatServerPort;
		this.chatServerHostName = chatServerHostName;
		this.context = context;
		connect();
	}
	
	/**
	 * Start connection thread
	 */
	private synchronized void connect() {
		if (chatConnectionTask != null) {
			return;
		}
		if (dnsConnectionThread != null) {
			return;
		}
		dnsConnectionThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				InetAddress chatServerAddress = null;
				PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				WakeLock  wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
						"Lock by dns-resolving");
				wakeLock.acquire();
				try {
					//DNS name resolving
					while(true) {
						if (Thread.interrupted()) {
							return;
						}
						chatServerAddress = getChatServerAddress();
						if (chatServerAddress != null) {
							break;
						}
						try {
							Thread.sleep(sleepingPolicy.nextSleepingTimeInMillis());
						} catch (InterruptedException e) {
							Log.wtf(TAG, e);
							return;
						}
					}
					Log.d(TAG, "dns-address-resolving");
					onDnsAddressResolving();
				} finally {
					wakeLock.release();
				}
				startConnectionTask(chatServerAddress, new SleepingPolicy());
			}

			private InetAddress getChatServerAddress() {
				if (!Hardwares.isNetworkConnectedOrConnecting(getContext())) {
					return null;
				}
				try {
					return InetAddress.getByName(chatServerHostName);
				} catch (Throwable th) {
					Log.w(TAG, String.format("host[%s]-not-found", chatServerHostName));
					return null;
				}
			}
		}, "dns-connection");
		dnsConnectionThread.start();
		Thread.yield();
	}
	
	private void startConnectionTask(
			final InetAddress chatServerAddress,
			final SleepingPolicy sleepingPolicy) {
		chatConnectionTask = createChatConnectionTask(chatServerAddress, chatServerPort);
		connectionThread = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					long nextSleepingTimeInMillis = sleepingPolicy.nextSleepingTimeInMillis();
					Thread.sleep(nextSleepingTimeInMillis);
				} catch (InterruptedException e) {
					Log.wtf(TAG, e);
					return;
				}
				if (!Hardwares.isNetworkConnectedOrConnecting(getContext())) {
					throw new IllegalStateException("network-not-connected");
				}
				chatConnectionTask.run();
			}
			
		}, "chat-connection[" + chatServerAddress.toString() + "][" + chatServerPort + "]");
		connectionThread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				lastConnectionThrowable = ex;
				Log.wtf(TAG, ex);
				if (!chatConnectionTask.isRunning()) { 
					startConnectionTask(chatServerAddress, sleepingPolicy);
				}
			}
		});
		connectionThread.start();
	}
	
	public void onDnsAddressResolving() {
	}

	protected abstract A createChatConnectionTask(InetAddress chatServerAddress, int chatServerPort);

	public Throwable getLastConnectionThrowable() {
		return lastConnectionThrowable;
	}
	
	public synchronized void release() {
		if (dnsConnectionThread != null && dnsConnectionThread.isAlive()) {
			dnsConnectionThread.interrupt();
		}
		sleepingPolicy.reset();
		
		if (connectorTasksExecutorService != null) {
			connectorTasksExecutorService.shutdown();
			connectorTasksExecutorService = null;
		}
		
		if (connectionThread == null) {
			return;
		}
		if (connectionThread.isAlive()) {
			connectionThread.interrupt();
		}
		chatConnectionTask = null;
		connectionThread = null;
	}

	public Context getContext() {
		return context;
	}
	
	public boolean isConnected() {
		return chatConnectionTask != null && chatConnectionTask.isConnected();
	}

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public int getChatServerPort() {
		return chatServerPort;
	}

	public String getChatServerHostName() {
		return chatServerHostName;
	}

	public ExecutorService getConnectorTasksExecutorService() {
		if (connectorTasksExecutorService == null) {
			initConnectorTasksExecutorService();
		}
		return connectorTasksExecutorService;
	}

	private synchronized void initConnectorTasksExecutorService() {
		if (connectorTasksExecutorService != null) {
			return;
		}
		connectorTasksExecutorService = Executors.newSingleThreadExecutor(HipdriverThreadFactory.newSingleThreadInstance("chat-connector-tasks-executor"));
	}
	
	public void reconnect() {
		if (chatConnectionTask == null) {
			return;
		}
		chatConnectionTask.reconnect();
	}

}
