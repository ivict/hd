package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import ru.hipdriver.android.app.R;

public class ShowMessageActivity extends HipdriverActivity {

	protected static final String TAG = TextTools
			.makeTag(ShowMessageActivity.class);
	
	public static final String NOTIFICATION_MESSAGE = "notification_message";

	public static final String NOTIFICATION_MESSAGE_PREF_KEY = "notification_message_pref_key";
	
	public static final String NOTIFICATION_ACTIVE_TEXT = "notification_active_text";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_message_screen);
	    //Show message if has notification request.
	    Intent intent = getIntent();
	    CharSequence notificationMessage = intent.getCharSequenceExtra(NOTIFICATION_MESSAGE);
	    final String prefKeyForCheckBox = intent.getStringExtra(NOTIFICATION_MESSAGE_PREF_KEY);
	    final ActiveText activeText = (ActiveText) intent.getSerializableExtra(NOTIFICATION_ACTIVE_TEXT);
		if (notificationMessage != null) {
			intent.putExtra(NOTIFICATION_MESSAGE_PREF_KEY, (String) null);
			intent.putExtra(NOTIFICATION_MESSAGE, (CharSequence) null);
			intent.putExtra(NOTIFICATION_ACTIVE_TEXT, (ActiveText) null);
			//showModalMessage(notificationMessage, prefKeyForCheckBox, null);
			TextView messageTextView = (TextView) findViewById(R.id.message_text);
			String title = getResources().getString(R.string.modal_alert_title) + "<br/>";
			SpannableStringBuilder text = new SpannableStringBuilder(Html.fromHtml(title));
			text.append(notificationMessage);
			messageTextView.setText(text);
			
	    } else {
	    	finish();
	    }
		View positiveButton = findViewById(R.id.positive_button);
		positiveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
			
		});
		
		//Add checkbox
		if (prefKeyForCheckBox != null) {
			CheckBox checkBox = (CheckBox) findViewById(R.id.message_readed_checkbox);
			CharSequence text = Html.fromHtml(getString(R.string.do_not_show_this_message_again));
			checkBox.setText(text);
			checkBox.setVisibility(View.VISIBLE);
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					getA().setBoolean(prefKeyForCheckBox, isChecked);
				}
				
			});
		}
		//Add active text
		if (activeText != null) {
			TextView activeTextView = (TextView) findViewById(R.id.active_text);
			CharSequence activeTextViewText = activeText.getText(this);
			activeTextView.setText(activeTextViewText);
			activeTextView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
					try {
					    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(activeText.getUri1())));
					} catch (android.content.ActivityNotFoundException anfe) {
					    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(activeText.getUri2())));
					}
				}
			});
			activeTextView.setVisibility(View.VISIBLE);
		}		
	}

}
