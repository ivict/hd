package ru.hipdriver.android.app;

import ru.hipdriver.android.util.TextTools;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import ru.hipdriver.android.app.R;

public abstract class HipdriverFragment extends Fragment {
	
	private static final String TAG = TextTools.makeTag(HipdriverFragment.class);
	private HipdriverApplication hipdriverApplication;
	
	public HipdriverApplication getA() {
		if (hipdriverApplication != null) {
			return hipdriverApplication;
		}
		Activity activity = getActivity();
		if (activity == null) {
			return null;
		}
		hipdriverApplication = (HipdriverApplication) getActivity().getApplicationContext();
		return hipdriverApplication;
	}
	
	protected void setTextTo(View view, int editorId, final String preferencesKey) {
		if (view == null) {
			return;
		}
		Activity activity = getActivity();
		EditText editText = (EditText) view.findViewById(editorId);
		if (editText == null && activity != null) {
			editText = (EditText) activity.findViewById(editorId);
		}
		final EditText target = editText;
		target.post(new Runnable() {

			@Override
			public void run() {
				Activity activity = getActivity();
				if (activity == null) {
					return;
				}
				target.setText(getA().getString(preferencesKey, ""));
			}
			
		});
	}
	
	protected void setTextTo(View view, int editorId, final int stringId, final Object... args) {
		if (view == null) {
			return;
		}
		Activity activity = getActivity();
		TextView textView = (TextView) view.findViewById(editorId);
		if (textView == null && activity != null) {
			textView = (TextView) activity.findViewById(editorId);
		}
		if (textView == null) {
			return;
		}
		final TextView target = textView;
		textView.post(new Runnable() {

			@Override
			public void run() {
				Activity activity = getActivity();
				if (activity == null) {
					return;
				}
				String textSeed = getResources().getString(stringId);
				StringBuilder text = new StringBuilder(textSeed);
				for (Object arg : args) {
					text.append(arg);
				}
				target.setText(text.toString());
			}
			
		});

	}
	
	protected void hideSoftKeyboard(View v) {
		Activity activity = getActivity();
		if (activity == null) {
			return;
		}
		InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}
	
	protected void disableAllChildsInViewGroup(ViewGroup viewGroup) {
		for (int i = 0; i < viewGroup.getChildCount(); i++) {
			View view = viewGroup.getChildAt(i);
			view.setEnabled(false);
		}
	}

	protected void enableAllChildsInViewGroup(ViewGroup viewGroup) {
		for (int i = 0; i < viewGroup.getChildCount(); i++) {
			View view = viewGroup.getChildAt(i);
			view.setEnabled(true);
		}
	}

	protected <T> T uiComponent(View view, int resourceId) {
		if (view == null) {
			return null;
		}
        try {
            return (T) view.findViewById(resourceId);
        } catch (ClassCastException e) {
            Log.wtf(TAG, e);
            throw e;
        }
	}

}
