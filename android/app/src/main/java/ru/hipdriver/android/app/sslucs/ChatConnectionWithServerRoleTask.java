package ru.hipdriver.android.app.sslucs;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.io.IOException;
import java.net.InetAddress;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import ru.hipdriver.android.app.HipdriverApplication;
import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.ISid;
import ru.hipdriver.i.support.DBLinks;
import ru.hipdriver.i.support.UserCommandTypesEnum;
import ru.hipdriver.util.ChatMsgParser;
import android.content.Context;
import android.util.Log;

class ChatConnectionWithServerRoleTask extends ChatConnectionTask {
	
	private static final String TAG = TextTools
			.makeTag(ChatConnectionWithServerRoleTask.class);
	public static final Object NO_SEND_VALUE = new Object();
	private UserCommandExecutor executor;

	public ChatConnectionWithServerRoleTask(Context context, long mobileAgentId, InetAddress chatServerAddress, int port, UserCommandExecutor executor) {
		super(context, mobileAgentId, chatServerAddress, port);
		this.executor = executor;
	}

	protected void sendResponse(long mobileAgentIdTo, long requestId, Object object) throws JsonGenerationException, JsonMappingException, IOException {
		if (!isRunning()) {
			throw new IllegalStateException("Connection not establishement");
		}
		if (!isConnected()) {
			throw new IllegalStateException("Connection not initialized");
		}
		ObjectMapper om = getObjectMapper();
		String body = om.writeValueAsString(object);
		sendMessage(mobileAgentIdTo, requestId, body);
	}

	protected void handleForRequest(String msg) {
		setFirstMessageReceived(true);
		Log.d(TAG, "receive-mesage-with-length :" + (msg != null ? msg.getBytes().length : 0) + "bytes");
		long requesterMobileAgentId = ChatMsgParser.getMobileAgentIdFrom(msg);
		if (requesterMobileAgentId == IMobileAgent.INVALID_ID) {
			if (msg == null) {
				Log.w(TAG, "null-msg-can-not-be-parsed");
				return;
			}
			//Special messages or wrong messages
			if (msg.startsWith(NO_DEST_CH)) {
				//TODO: retry send after some time;
			}
			return;
		}
		String body = ChatMsgParser.getBody(msg);
		long requestId = ISid.INVALID_COMPONENT_SESSIONS_ID;
		try {
			requestId = ChatMsgParser.getComponentSessionsId(msg);
			if (Invariants.PING_REQUEST.equals(body)) {
				sendResponse(requesterMobileAgentId, requestId, Invariants.PING_ANSWER);
				return;
			}
			if (body == null) {
				sendResponse(requesterMobileAgentId, requestId, ChatConnectionTask.INVALID_REQUEST);
				return;
			}
			if (msg.startsWith(ERROR)) {
				//TODO: handle error in protocol
				return;
			}
			//TODO: Setup request processor and redirect processing
			ObjectMapper om = getObjectMapper();
			ShortUserCommand shortUserCommand = om.readValue(body, ShortUserCommand.class);
			short userCommandTypeId = shortUserCommand.getUserCommandTypeId();
			UserCommandTypesEnum userCommandType = DBLinks.unlinkUserCommandType(userCommandTypeId);
			if (userCommandType == null) {
				sendResponse(requesterMobileAgentId, requestId, ChatConnectionTask.INVALID_REQUEST);
				return;
			}
			if (executor == null) {
				sendError(requesterMobileAgentId, requestId);
				return;
			}
			byte[] args = shortUserCommand.getArgs();
			Object returnValue = executor.exec(userCommandType, args, requestId, requesterMobileAgentId);
			if (returnValue == NO_SEND_VALUE) {
				return;
			}
			sendResponse(requesterMobileAgentId, requestId, returnValue);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			sendError(requesterMobileAgentId, requestId);
		}
	}

	protected void sendError(long requesterMobileAgentId, long requestId) {
		if (requestId == ISid.INVALID_COMPONENT_SESSIONS_ID) {
			return;
		}
		try {
			sendResponse(requesterMobileAgentId, requestId, ChatConnectionTask.ERROR);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}

	@Override
	protected SimpleChannelInboundHandler<String> makeChatClientHandler() {
		return new ChatClientHandlerForServerTask(this);
	}

	@Override
	protected boolean checkStopCondition() {
		//TODO: remove this hack after fix reason of issue, it's only workaround
		if (context instanceof HipdriverApplication) {
			switch(((HipdriverApplication) context).getControlMode()) {
				case INTERNET:
					return false;
				case PHONE_CALLS:
				case SMS:
					Log.w(TAG, "check-stop-condition[true]");
					return true;
			}
		}
		return false;
	}


}

class ChatClientHandlerForServerTask extends ChatClientHandler<ChatConnectionWithServerRoleTask> {

	public ChatClientHandlerForServerTask(
			ChatConnectionWithServerRoleTask chatConnectionTask) {
		super(chatConnectionTask);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg)
			throws Exception {
		chatConnectionTask.handleForRequest(msg);
	}

}
