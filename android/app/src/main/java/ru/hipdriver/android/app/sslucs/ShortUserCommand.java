package ru.hipdriver.android.app.sslucs;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
//All fields are required for multi-class response resolving
//@see ChatConnectionWithClientRoleTask#parseAnswer
//i.e. ignoreUnknown = false is critical for correct parsing
@JsonIgnoreProperties(ignoreUnknown = false)
public class ShortUserCommand {

	@JsonProperty(value = "uctid")
	public short userCommandTypeId;

	@JsonProperty(value = "args")
	public byte[] args;

	public short getUserCommandTypeId() {
		return userCommandTypeId;
	}

	public void setUserCommandTypeId(short userCommandTypeId) {
		this.userCommandTypeId = userCommandTypeId;
	}

	public byte[] getArgs() {
		return args;
	}

	public void setArgs(byte[] args) {
		this.args = args;
	}
	
}
