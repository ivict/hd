package ru.hipdriver.android.app.sslucs;

public interface MobileAgentStateListener {

	void onChangeMobileAgentState();
	
	void onChangeStateDescription(String description);
	
}
