package ru.hipdriver.android.app;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import ru.hipdriver.android.app.R;

//@see http://stackoverflow.com/questions/2991110/android-how-to-stretch-an-image-to-the-screen-width-while-maintaining-aspect-ra
public class Logo extends View {

	private Drawable logo;
	private float initialWidth = 64;
	private float initialHeight = 64;

	public Logo(Context context) {
		super(context);
		//logo = context.getResources().getDrawable(R.drawable.lock);
		//setBackgroundDrawable(logo);
	}

	public Logo(Context context, AttributeSet attrs) {
		super(context, attrs);
		//logo = context.getResources().getDrawable(R.drawable.lock);
		//setBackgroundDrawable(logo);
	}

	public Logo(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		//logo = context.getResources().getDrawable(R.drawable.lock);
		//setBackgroundDrawable(logo);
	}
	
	public Drawable getLogo() {
		return logo;
	}

	public void setLogo(int resourceId, int initialWidth, int initialHeight) {
		this.initialHeight = initialHeight;
		this.initialWidth = initialWidth;
		logo = getContext().getResources().getDrawable(resourceId);
		setBackgroundDrawable(logo);		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		int width = MeasureSpec.getSize(widthMeasureSpec);
//		int height = width * logo.getIntrinsicHeight()
//				/ logo.getIntrinsicWidth();
		
		int width = (int) Pixels.dipToPixels(getContext(), initialWidth);
		int height = (int) Pixels.dipToPixels(getContext(), initialHeight);
		
		
		setMeasuredDimension(width, height);
	}
}
