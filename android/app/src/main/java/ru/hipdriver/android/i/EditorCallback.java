package ru.hipdriver.android.i;

import android.content.Intent;

/**
 *
 * Interface for callback.
 *
 */
public interface EditorCallback {
	
	void onActivityResult(int resultCode, Intent data);

}
