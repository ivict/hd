package ru.hipdriver.android.app.sslucs;

import ru.hipdriver.android.app.HipdriverApplication;
import ru.hipdriver.android.i.ISslClient;

public class UcClientFactory {
	
    /**
     * Monitor object used to wait for remote agent to finish it's job
     */
    static final Object remoteAgentMonitor = new Object();

	private UcClientFactory() {
	}

	public static ISslClient createClient(
			HipdriverApplication hipdriverApplication,
			long mobileAgentId)
			throws Throwable {
		SslClient client = new SslClient(hipdriverApplication, mobileAgentId);
		return client;
	}
}
