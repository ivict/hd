package ru.hipdriver.android.app;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class HipdriverScrollView extends ScrollView {
	
	public interface OnScrollByY {
		
		void scrollByY(int scrollY);
		
	}
	
	private OnScrollByY onScrollByY;

	public HipdriverScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public HipdriverScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public HipdriverScrollView(Context context) {
		super(context);
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		if (onScrollByY != null && t != oldt) {
			onScrollByY.scrollByY(t);
		}
	}

	public OnScrollByY getOnScrollByY() {
		return onScrollByY;
	}

	public void setOnScrollByY(OnScrollByY onScrollByY) {
		this.onScrollByY = onScrollByY;
	}
	
}
