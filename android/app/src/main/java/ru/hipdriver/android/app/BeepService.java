package ru.hipdriver.android.app;

import java.util.HashMap;
import java.util.Map;

import ru.hipdriver.android.util.TextTools;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import ru.hipdriver.android.app.R;

public class BeepService extends Service implements MediaPlayer.OnErrorListener {
	
	public static final String TAG = TextTools.makeTag(BeepService.class);

	private final IBinder binder = new ServiceBinder();
	MediaPlayer player_cac;
	MediaPlayer player_caso;
	MediaPlayer player_a;
	MediaPlayer player_aa;
	MediaPlayer player_low_battery;
	private int length = 0;
	
	private Map<Integer, MediaPlayer> players;
	
	public BeepService() {
		players = new HashMap<Integer, MediaPlayer>(3);
	}

	public class ServiceBinder extends Binder {
		BeepService getService() {
			return BeepService.this;
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return binder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.w(TAG, "try-to-recreate-existing-service");
		if (player_cac != null) {
			return;
		}
		
		player_cac = makePlayer(R.raw.beep_cac, 1f);
		player_caso = makePlayer(R.raw.beep_caso, 1f);
		player_a = makePlayer(R.raw.beep_a, 0.5f);
		player_aa = makePlayer(R.raw.alarm0, 1f);
		player_low_battery = makePlayer(R.raw.low_battery, 1f);
	}

	private MediaPlayer makePlayer(int resId, float volume) {
		try {
			MediaPlayer player = getPlayer(resId);
			if (player == null) {
				return null;
			}
			player.setOnErrorListener(this);
	
			if (player != null) {
				if (player.isLooping()) {
					player.setLooping(false);
				}
				player.setVolume(volume, volume);
			}
	
			return player;
		} catch (Throwable th) {
			Log.wtf(TAG, th);
			return null;
		}
	}
	
	private boolean existsPlayer(int resId) {
		return players.get(resId) != null;
	}

	private MediaPlayer getPlayer(int resId) {
		MediaPlayer player = players.get(resId);
		if (player != null) {
			return player;
		}
		synchronized (this) {
			if ((player = players.get(resId)) != null) {
				return player;
			}
			try {
				player = MediaPlayer.create(this, resId);
			} catch (Throwable th) {
				Log.wtf(TAG, th);
				return null;
			}
			players.put(resId, player);
			return player;
		}
	}

	public void pauseMusic(MediaPlayer player) {
		if (player == null) {
			return;
		}
		if (player.isPlaying()) {
			player.pause();
			length = player.getCurrentPosition();

		}
	}

	public void resumeMusic(MediaPlayer player) {
		if (player == null) {
			return;
		}
		if (player.isPlaying() == false) {
			player.seekTo(length);
			player.start();
		}
	}

	public void stopMusic(MediaPlayer player) {
		if (player == null) {
			return;
		}
		player.stop();
		player.release();
		player = null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		releasePlayer(player_cac);
		releasePlayer(player_caso);
		releasePlayer(player_a);
		releasePlayer(player_aa);
	}

	private void releasePlayer(MediaPlayer player) {
		if (player == null) {
			return;
		}
		
		try {
			player.stop();
			player.release();
		} finally {
			player = null;
		}
	}

	public boolean onError(MediaPlayer mp, int what, int extra) {
		releasePlayer(mp);
		return true;
	}
	
	public void beepCaso() {
		if (player_caso == null) {
			Log.w(TAG, "service-not-initialized");
			return;
		}
		player_caso.seekTo(0);
		player_caso.start();
	}

	public void beepCac() {
		if (player_cac == null) {
			Log.w(TAG, "service-not-initialized");
			return;
		}
		player_cac.seekTo(0);
		player_cac.start();
	}

	public void beepA() {
		if (player_a == null) {
			Log.w(TAG, "service-not-initialized");
			return;
		}
		player_a.seekTo(0);
		player_a.start();
	}

	public void beepAlarmAlarm() {
		if (player_aa == null) {
			Log.w(TAG, "service-not-initialized");
			return;
		}
		player_aa.seekTo(0);
		try {
			player_aa.setLooping(true);
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
		player_aa.start();
	}
	
	public void beepLowBattery() {
		if (player_low_battery == null) {
			Log.w(TAG, "service-not-initialized");
			return;
		}
		player_low_battery.seekTo(0);
		player_low_battery.start();
	}

	public void pauseAlarmAlarm() {
		if (player_aa == null) {
			Log.w(TAG, "service-not-initialized");
			return;
		}
		player_aa.pause();
	}

	public void beep(int resId, boolean looping) {
		MediaPlayer player = makePlayer(resId, 1f);
		if (player == null) {
			Log.w(TAG, "service-not-initialized");
			return;
		}
		player.seekTo(0);
		player.setLooping(looping);
		player.start();
	}

	public void stop(int resId) {
		if (!existsPlayer(resId)) {
			return;
		}
		MediaPlayer player = makePlayer(resId, 1f);
		if (player == null) {
			Log.w(TAG, "service-not-initialized");
			return;
		}
		player.stop();
	}
}