package ru.hipdriver.android.app;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.support.AppStatesEnum;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.i.support.DBLinks;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import ru.hipdriver.android.app.R;

public class BeaconService extends HipdriverService {

	private static final String ACTION_START_BEACON = "ru.hipdriver.START_BEACON";

	public static final String TAG = TextTools.makeTag(BeaconService.class);

	public static final String BEACON_SERVICE_CHECK_POINT = "beacon-service-check-point";
	
	private static final int ANY_NUMBER = 0;

	private static final long GPS_TIMEOUT_IN_MILLIS = TimeUnit.MINUTES.toMillis(10);
	
	public class BeaconBinder extends Binder {
		BeaconService getService() {
			return BeaconService.this;
		}

	}
	
	private class ShutdownPhoneTask extends TimerTask {

		@Override
		public void run() {
			if (getA().getAppState() != AppStatesEnum.BMA) {
				cancel();
				return;
			}
			//if (!getA().getBoolean(IMobileAgentProfile.SWITCH_ON_BEACON_MODE)) {
			//	cancel();
			//	return;
			//}
			//if (location == null) {
			//	cancel();
			//	return;
			//}
			//Check if shutdown in process
			if (!startShutdown.compareAndSet(false, true)) {
				if (getA().isDebugMode()) {
					String logName = "bs-restart-stat.log";
					getA().writeDebugInfo(logName, "%s,,another-shutdown-is-running<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()));
				}
				return;
			}
			int minutes = getA().getInt(IMobileAgentProfile.BEACON_INTERVAL_IN_MINUTES);
			//int seconds = (int) TimeUnit.MINUTES.toSeconds(minutes);
			WatchdogService watchdogService = getA().getWatchdogService();
			if (watchdogService != null) {
				watchdogService.stopUpdateLocation();
			}
//			//Rooted phone and rtc chip has control on wakeup system
//			if (Hardwares.isRooted() && Hardwares.rtcWakeUp(seconds)) {
//				getA().makeToastWarning(R.string.phone_power_switch_off_after_minute);
//				try {
//					Thread.sleep(TimeUnit.MINUTES.toMillis(1));
//				} catch (InterruptedException e) {
//					cancel();
//					return;
//				}
//				//if (!getA().getBoolean(IMobileAgentProfile.SWITCH_ON_BEACON_MODE)) {
//				//	cancel();
//				//	return;
//				//}
//				Hardwares.rtcWakeUp(seconds);
//				Hardwares.powerOff();
//				cancel();
//				return;
//			}
			AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
			Intent beaconServiceIntent = new Intent(getA(), BeaconService.class);
			beaconServiceIntent.setAction(ACTION_START_BEACON);
			long triggerAtTime = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(minutes);
			PendingIntent operation = PendingIntent.getService(getA(), ANY_NUMBER, beaconServiceIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);
			alarmManager.set(AlarmManager.RTC_WAKEUP, triggerAtTime, operation);
			//TODO: Support airplane-mode on rooted phones on Android 4.2+
			final AirplaneMode airplaneMode = new AirplaneMode(getA());
			airplaneMode.switchOn();
			if (getA().isDebugMode()) {
				String logName = "bs-restart-stat.log";
				getA().makeDebugInfoHeaderIfNotHeadered(logName, "time,delay-in-millis,action\n");
				getA().writeDebugInfo(logName, "%s,,shutdown<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()));
			}
		}
		
	}
	
	// Binder given to clients
	private final IBinder beaconBinder = new BeaconBinder();

	private Timer timer;
	
	//private ru.hipdriver.i.ICarLocation location;
	
	private AtomicBoolean startShutdown = new AtomicBoolean(false);
	
	private AtomicBoolean runned = new AtomicBoolean(false);

	@Override
	public IBinder onBind(Intent intent) {
		return beaconBinder;
	}

	public void start(long delayInMillis) {
		//Not start in intermediate state of car
		if (getA().getCarState() == CarStatesEnum.A) {
			return;
		}
		
		resetPendingIntents();
		
		if (runned.get()) {
			if (getA().isDebugMode()) {
				String logName = "bs-restart-stat.log";
				getA().makeDebugInfoHeaderIfNotHeadered(logName, "time,delay-in-millis,action\n");
				getA().writeDebugInfo(logName, "%s,%s,nothing-do-another-thread-running<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()),delayInMillis);
			}
			return;
		}
		
		if (getA().isDebugMode()) {
			String logName = "bs-restart-stat.log";
			getA().makeDebugInfoHeaderIfNotHeadered(logName, "time,delay-in-millis,action\n");
			getA().writeDebugInfo(logName, "%s,%s,start<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()),delayInMillis);
		}

		try {
			getA().watchdogAlert(R.string.beacon_mode, false);
			
			if (this.timer != null) {
				this.timer.cancel();
			}
			final Timer timer = new Timer("ShutdownPhoneTimer");
			this.timer = timer;
			
			getA().setAppState(AppStatesEnum.BMA);
			
			timer.schedule(new TimerTask() {

				@Override
				public void run() {
					runned.set(true);
					getA().startWatchdogServiceAndWait(TimeUnit.SECONDS.toMillis(10));
					WatchdogService watchdogService = getA().getWatchdogService();
					if (watchdogService == null) {
						runned.set(false);
						return;
					}
					
					watchdogService.resetLocation();
					watchdogService.startUpdateLocation();
					ILocation location = watchdogService.getLocation();
					long timeStop = System.currentTimeMillis() + GPS_TIMEOUT_IN_MILLIS;
					while(invalidLocation(location)) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							Log.wtf(TAG, e);
							break;
						}
						location = watchdogService.getLocation();						
						if (timeStop < System.currentTimeMillis()) {
							break;
						}
					}
					long time = System.currentTimeMillis();
					short carStateId = DBLinks.link(getA().getCarState());
					float vel = watchdogService.getVel();
					final ICarLocation currentCarLocation = Locations.makeLocation(location.getLat(), location.getLon(), location.getAlt(), location.getAcc(), location.getMcc(), location.getMnc(), location.getLac(), location.getCid(), time, carStateId, vel);
					//BeaconService.this.location = currentCarLocation;
					//Off airplane mode before send data
					//Automatically before run-network-task
					
					List<ru.hipdriver.i.ICarLocation> singlePointTrack = Collections.singletonList(currentCarLocation);
					//Send alert-track
					UITaskFactory.sendAlertTrackEvent(getA(), singlePointTrack, new After() {

						@Override
						public void success(Object returnValue) {
							if (getA().isDebugMode()) {
								String logName = "bs-restart-stat.log";
								String debugInfo = String.format("sended location lat[%s], lon[%s], acc[%s], carStateId[%s]", currentCarLocation.getLat(), currentCarLocation.getLon(), currentCarLocation.getAcc(), currentCarLocation.getCarStateId()); 
								getA().writeDebugInfo(logName, "%s,,%s<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()), debugInfo);
							}
							new ShutdownPhoneTask().run();
							runned.set(false);
						}

						@Override
						public void failure(int returnCode, String failureReason) {
							if (getA().isDebugMode()) {
								String logName = "bs-restart-stat.log";
								getA().writeDebugInfo(logName, "%s,,send location failed: code[%s], reason[%s]<br>\n", TextTools.format("yyyy-MM-dd HH:mm:ss", new Date()), returnCode, failureReason);
							}
							new ShutdownPhoneTask().run();
							runned.set(false);
						}
						
					});
				}
				
			}, 0);
			//Prevent premature start of shutdown
			startShutdown.set(false);
			//timer.schedule(new ShutdownPhoneTask(), delayInMillis);
			
		} catch (Throwable th) {
			Log.wtf(TAG, th);
		}
	}
	
//	private void devicePowerOff() {
//		if (Hardwares.isRooted() && Hardwares.powerOff()) {
//			return;
//		}
//		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
////		final long ident = Binder.clearCallingIdentity();
////		try {
////			Log.v(TAG, "shutdown ...");
////			Class c = Class.forName(powerManager.getClass().getName());
////			Field f = c.getDeclaredField("mService");
////			f.setAccessible(true);
////			IPowerManager powerService = (IPowerManager) f.get(powerManager);
////			enforceCallingOrSelfPermission(android.Manifest.permission.REBOOT, null);			
////			powerService.shutdown(false, false);
////			return;
////		} catch (Throwable th) {
////			Log.wtf(TAG, th);
////		} finally {
////	        Binder.restoreCallingIdentity(ident);
////	    }
//		AirplaneMode airplaneMode = new AirplaneMode(getA());
//		airplaneMode.switchOn();
//		try {
//			powerManager.goToSleep(SystemClock.uptimeMillis() + 1);
//		} catch (Throwable th) {
//			Log.wtf(TAG, th);
//		}
//	}

	public boolean isStopped() {
		return timer == null;
	}

	public boolean stop() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		//location = null;

		//Reset previous pending intent
		resetPendingIntents();
		
		//Break another activity if not in beacon mode
		if (getA().getAppState() != AppStatesEnum.BMA) {
			return false;
		}
		
		//Switch off GPS
		WatchdogService watchdogService = getA().getWatchdogService();
		if (watchdogService != null) {
			watchdogService.stopUpdateLocation();
		}
		return true;
	}

	private void resetPendingIntents() {
		Intent beaconServiceIntent = new Intent(getA(), BeaconService.class);
		beaconServiceIntent.setAction(ACTION_START_BEACON);
		PendingIntent operation = PendingIntent.getService(getA(), ANY_NUMBER, beaconServiceIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);
		operation.cancel();
	}

	@Override
	public void onStart(Intent intent, int startid) {
		Log.d(TAG, "beacon-service-init");
		if (!isStopped()) {
			Log.d(TAG, "beacon-service-always-be-started");
			return;
		}
		//Service nothing to be restart
		if (intent == null) {
			return;
		}
		if (getA().getAppState() == AppStatesEnum.BMA) {//Restart service
			Log.d(TAG, "restarting-beacon-service");
			start(0);
		}
		
//		String action = intent.getAction();
//		if (ACTION_START_BEACON.equals(action)) {
//			start(0);
//		}
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		getA().setBeaconService(this);
	}

	protected boolean  invalidLocation(ILocation location) {
		if (location == null) {
			return true;
		}
		return location.getLat() == ILocation.UNKNOWN_LATITUDE ||
			location.getLon() == ILocation.UNKNOWN_LONGITUDE;
	}
	
}
