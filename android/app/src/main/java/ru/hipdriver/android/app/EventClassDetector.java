package ru.hipdriver.android.app;

import static ru.hipdriver.android.app.NumericTools.ffn;
import static ru.hipdriver.android.app.NumericTools.norma;
import static ru.hipdriver.android.app.NumericTools.num;
import static ru.hipdriver.android.app.NumericTools.zeroF3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import ru.hipdriver.android.util.TextTools;
import ru.hipdriver.d.Sensors;
import ru.hipdriver.d.Sensors.MinMaxValue;
import ru.hipdriver.d.Sensors.Shift;
import ru.hipdriver.i.support.CarStatesEnum;
import ru.hipdriver.j.ext.SensorData;
import android.util.FloatMath;
import android.util.Log;

public class EventClassDetector extends SingleRunHipdriverTimerTask {
	
	private static final String TAG = TextTools.makeTag(EventClassDetector.class);

	private static final long NANO_RATE = 5000000000L;// 10000000000L;
	public static final long RATE_IN_MILLIS = NANO_RATE / 1000000;
	public static final long _60s = 60000000000L;
	public static final long _10s = 10000000000L;
	public static final long _5s =   5000000000L;
	public static final String BIG_TIMEOUT_KEY = "detection-timeout-in-millis";
	public static final long DEFAULT_BIG_TIMEOUT_MILLIS = 30000L;
	
	// For correct measuring use more then 10 points
	public static final int TOO_SMALL_DATA_TRESHOLD = 40;

	private long detectionTimeoutInMillis;
	
	private String alertReason;

	// private final static EventClassDetector instance = new
	// EventClassDetector();

	public EventClassDetector(HipdriverApplication applicationContext,
			Timer timer, String alertReason) {
		super(applicationContext, timer);
		detectionTimeoutInMillis = DEFAULT_BIG_TIMEOUT_MILLIS;//getA().getInt(BIG_TIMEOUT_KEY, 60) * 1000;
		this.alertReason = alertReason;
	}

	/**
	 * Отображаем статус в заголовке, перед кнопками.
	 */

	@Override
	public void runIfNotPaused() {
		Log.d(TAG, "start-event-detection");
		WatchdogService watchdogService = getA().getWatchdogService();
		SensorData sensorData = watchdogService.accelerometerData;
		// Prevent errors on too small data
		if (TOO_SMALL_DATA_TRESHOLD > sensorData.getCountOfValues()) {
			//Wait for data
			Log.d(TAG, "wait-for-more-data-before-run-event-class-detecting-task");
			EventClassDetector retryDetection = new EventClassDetector(getA(), timer, alertReason); 
			timer.schedule(retryDetection, detectionTimeoutInMillis);
			return;
		}
		
		long detectionTimeoutNs = detectionTimeoutInMillis * 1000000L;
		
		//Точка срабатывания тревоги
		long time0 = watchdogService.getLastAlertTimeInNs();
		long time1 = time0 + detectionTimeoutNs;
		//Разбиваем интервал по пять секунд
		List<Float> list0XDelta = new ArrayList<Float>();
		List<Float> list0YDelta = new ArrayList<Float>();
		for (int i = 0; i < 6; i++) {
			long t = time0 - (5 - i) * _5s;
			//More sensitivity for event
			MinMaxValue minMaxAngles;
			if (i == 6 - 1) {
				minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, t, _5s, 1f, 1);
			} else {
				minMaxAngles = new MinMaxValue(new float[3], new float[3], new float[3], 0);
				minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, t, _5s, 0.2f, 1);
			}
			if (minMaxAngles != null) {
				float xDelta = minMaxAngles.maxValue[0] - minMaxAngles.minValue[0];
				float yDelta = minMaxAngles.maxValue[1] - minMaxAngles.minValue[1];
				list0XDelta.add(xDelta);
				list0YDelta.add(yDelta);
			} else {
				list0XDelta.add(0f);
				list0YDelta.add(0f);
			}
		}
		
		Log.d(TAG, Arrays.toString(list0XDelta.toArray()));
		Log.d(TAG, Arrays.toString(list0YDelta.toArray()));
		
		List<Float> list1XDelta = new ArrayList<Float>();
		List<Float> list1YDelta = new ArrayList<Float>();
		int count = (int) (detectionTimeoutNs / _5s);
		for (int i = 0; i < count; i++) {
			long t = time1 - ((count - 1) - i) * _5s;
			MinMaxValue minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, t, _5s, 0.2f, 1);
			if (minMaxAngles != null) {
				float xDelta = minMaxAngles.maxValue[0] - minMaxAngles.minValue[0];
				float yDelta = minMaxAngles.maxValue[1] - minMaxAngles.minValue[1];
				list1XDelta.add(xDelta);
				list1YDelta.add(yDelta);
			} else {
				list1XDelta.add(0f);
				list1YDelta.add(0f);
			}
			
		}
		
		Log.d(TAG, Arrays.toString(list1XDelta.toArray()));
		Log.d(TAG, Arrays.toString(list1YDelta.toArray()));
		
		//Last and first 5s for car-poke detection
		float xDelta00 = list0XDelta.get(list0XDelta.size() - 1);
		float yDelta00 = list0XDelta.get(list0YDelta.size() - 1);
		float xDelta01 = list0XDelta.get(0);
		float yDelta01 = list0XDelta.get(0);
		boolean isPossibleCarPoke = xDelta00 > 1f || yDelta00 > 1f || xDelta01 > 1f || yDelta01 > 1f;
		
		Sensors.ScalarValue jerkProjection = Sensors.jerkProjectionOnGravityCyclic(sensorData, time1, detectionTimeoutNs + _60s);
		float jpr = jerkProjection != null ? jerkProjection.value : 0;
		
		if (getA().isDebugMode()) {
			StringBuilder text = new StringBuilder();
			text.append("jp:").append(ffn(jpr, 7));
			getA().debugStatus(text.toString());			
		}
		
		int howMachMoreThenOneX0 = 0;
		for (Float delta : list1XDelta) {
			if (delta > 1f) {
				howMachMoreThenOneX0++;
			}
		}
		int howMachMoreThenOneX = howMachMoreThenOneX0;
		for (Float delta : list0XDelta) {
			if (delta > 1f) {
				howMachMoreThenOneX++;
			}
		}
		
		int howMachMoreThenOneY0 = 0;
		for (Float delta : list1YDelta) {
			if (delta > 1f) {
				howMachMoreThenOneY0++;
			}
		}
		int howMachMoreThenOneY = howMachMoreThenOneY0;
		for (Float delta : list0YDelta) {
			if (delta > 1f) {
				howMachMoreThenOneY++;
			}
		}
		
		//High frequency poke detection
		boolean highFrequencyPoke = false;
		if (howMachMoreThenOneX == 0 && howMachMoreThenOneY == 0) {
			List<Float> notNeighbourForEventX = new ArrayList<Float>();
			for (int i = 0; i < list0XDelta.size() - 1; i++) {
				notNeighbourForEventX.add(list0XDelta.get(i));
			}
			for (int i = 1; i < list1XDelta.size(); i++) {
				notNeighbourForEventX.add(list1XDelta.get(i));
			}
			List<Float> notNeighbourForEventY = new ArrayList<Float>();
			for (int i = 0; i < list0YDelta.size() - 1; i++) {
				notNeighbourForEventY.add(list0YDelta.get(i));
			}
			for (int i = 1; i < list1YDelta.size(); i++) {
				notNeighbourForEventY.add(list1YDelta.get(i));
			}
			
			//One of neighbours for alert time more then all others
			if (moreThenAllAtTimes(xDelta00, notNeighbourForEventX, 5)) {
				highFrequencyPoke = true;
			} else if (moreThenAllAtTimes(xDelta01, notNeighbourForEventX, 5)) {
				highFrequencyPoke = true;
			} else if (moreThenAllAtTimes(yDelta00, notNeighbourForEventY, 5)) {
				highFrequencyPoke = true;
			} else if (moreThenAllAtTimes(yDelta01, notNeighbourForEventY, 5)) {
				highFrequencyPoke = true;
			}
		}
		
		//Later perturbation detection
		boolean laterPerturbations = false;
		for (int i = 1; i < list1XDelta.size(); i++) {
			if (list1XDelta.get(i) > 1f) {
				laterPerturbations = true;
				break;
			}
		}
		for (int i = 1; i < list1YDelta.size(); i++) {
			if (list1YDelta.get(i) > 1f) {
				laterPerturbations = true;
				break;
			}
		}
		
		//STOLEN WHEELS
		if (howMachMoreThenOneX == 0 && howMachMoreThenOneY == 0 && !highFrequencyPoke) {
			getA().setCarState(CarStatesEnum.WS);
			watchdogService.updateAlertState();
			getA().debugStatus(alertReason + " Колеса!");
			sendAlertEvent();
			return;
		}
		
		//CAR-POKE
		if (howMachMoreThenOneX <= 2 && howMachMoreThenOneY <= 2 && isPossibleCarPoke && (!laterPerturbations || highFrequencyPoke)) {
			getA().setCarState(CarStatesEnum.CP);
			watchdogService.updateAlertState();
			getA().debugStatus(alertReason + " Удар!");
			sendAlertEvent();
			
			return;
		}
		
		Sensors.Value stdevAcc = Sensors.standardDeviationCyclic(sensorData, 1, time1,  detectionTimeoutNs);
		if (stdevAcc == null) {
			return;
		}
		float stdevAccZ = stdevAcc.value[2];
		float stdevAccXY = FloatMath.sqrt(stdevAcc.value[0] * stdevAcc.value[0] + stdevAcc.value[1] * stdevAcc.value[1]);

		//CHECK EVACUATION
		if (stdevAccZ > stdevAccXY || jpr < 0) {
			getA().setCarState(CarStatesEnum.CE);
			watchdogService.updateAlertState();
			getA().debugStatus(alertReason + " Эвакуируют!");
			sendAlertEvent();
			return;
		}
		
		
		getA().setCarState(CarStatesEnum.CS);
		watchdogService.updateAlertState();
		getA().debugStatus(alertReason + " Угоняют!");
		sendAlertEvent();
	}

	private boolean moreThenAllAtTimes(float value,
			List<Float> otherValues, int times) {
		for (Float otherValue : otherValues) {
			if (value < times * otherValue) {
				return false;
			}
		}
		return true;
	}

	private void sendAlertEvent() {
		//Store first detection result
		getA().setFirstDetectionCarState(getA().getCarState());
		UITaskFactory.sendAlertEvent(getA(), null, TimeUnit.MINUTES.toMillis(10));
		cancel();
	}

//	private boolean checkBigTimeout() {
//		return System.currentTimeMillis() >= timeThresholdMillis;
//	}

	// Изменение скорости по среднему ускорению за 10 секунд
	public float[] getVelocityShiftByAverageAcceleration() {
		WatchdogService watchdogService = getA().getWatchdogService();

		SensorData accelerometerData = watchdogService.accelerometerData;

		long timeInMillis = System.nanoTime();
		Sensors.AverageValue avalue = Sensors.averageValue(accelerometerData,
				timeInMillis, NANO_RATE);
		if (avalue == null) {
			return zeroF3();
		}
		// TODO: test J1 average value?

		float[] acc = new float[3];
		acc[0] = num(avalue.x);
		acc[1] = num(avalue.y);
		acc[2] = num(avalue.z);

		float mr = 10f;

		float[] velocity = new float[3];
		velocity[0] = acc[0] * mr;
		velocity[1] = acc[1] * mr;
		velocity[2] = acc[2] * mr;

		return velocity;
	}

	// Средняя скорость за 10 секунд
	public float[] getAverageVelocity() {
		WatchdogService watchdogService = getA().getWatchdogService();

		SensorData accelerometerData = watchdogService.accelerometerData;

		long timeInNanos = System.nanoTime();
		Sensors.AverageValue avalue = Sensors.averageValueI1(accelerometerData,
				timeInNanos, NANO_RATE);
		if (avalue == null) {
			return zeroF3();
		}
		// TODO: test J1 average value?
		float[] velocity = new float[3];
		velocity[0] = avalue.x;
		velocity[1] = avalue.y;
		velocity[2] = avalue.z;

		return velocity;
	}

	// Смещение за 10 секунд
	public Shift getShift(float[] gravity) {
		WatchdogService watchdogService = getA().getWatchdogService();
		SensorData accelerometerData = watchdogService.accelerometerData;
		long timeInNanos = System.nanoTime();
		float[] e1 = new float[3];
		float g = norma(gravity);
		System.arraycopy(gravity, 0, e1, 0, 3);
		e1[0] /= g;
		e1[1] /= g;
		e1[2] /= g;
		float[] e2 = new float[3];
		float[] e3 = new float[3];
		resolveBasis(e1, e2, e3);// scp(e1, e2);
		// e1[2] = 1f;
		// e2[0] = 1f;
		// e3[1] = 1f;

		Shift shift = Sensors.calculateShift(accelerometerData, e1,
				timeInNanos, NANO_RATE, e2, e3);
		if (shift == null) {
			return new Shift(0, 0, 0, 0, 0, 0);
		}
		return shift;
	}

	/**
	 * Resolve system of equations Measured vector def: m0 = a0 + g Jerk def: m1
	 * - m0, where m1 measured vector m1 = a1 + g Gravitation constant def:
	 * gConst 980.665 cm / s^2
	 * 
	 * g[0] * j[0] + g[1] * j[1] + g[2] * j[2] + a0[0] * j[0] + a0[1] * j[1] +
	 * a0[2] * j[2] == m0[0] * j[0] + m0[1] * j[1] + m0[2] * j[2] g[2] * j[1] -
	 * g[1] * j[2] + a0[2] * j[1] - a0[1] * j[2] == m0[2] * j[1] - m0[1] * j[2]
	 * g[0] * j[2] - g[2] * j[0] + a0[0] * j[2] - a0[2] * j[0] == m0[0] * j[2] -
	 * m0[2] * j[0] g[1] * j[2] - g[0] * j[1] + a0[1] * j[2] - a0[0] * j[1] ==
	 * m0[1] * j[2] - m0[0] * j[1]
	 * 
	 * g[0] * m0[0] + g[1] * m0[1] + g[2] * m0[2] - a0[0] * g[0] - a0[1] * g[1]
	 * - a0[2] * g[2] == gConst * gConst
	 */

	// Resolve system of equations:
	// e1 is known vector
	// e1[0] * e2[0] + e1[1] * e2[1] + e1[2] * e2[2] == 0
	// e1[0] * e3[0] + e1[1] * e3[1] + e1[2] * e3[2] == 0
	// e2[0] * e3[0] + e2[1] * e3[1] + e2[2] * e3[2] == 0
	// e2[0] * e3[0] + e2[1] * e3[1] == 0
	// e2[1] * e3[1] + e2[2] * e3[2] == 0
	// e2[0] * e3[0] + e2[2] * e3[2] == 0
	// TODO: see N dimensional case
	private void resolveBasis(float[] e1, float[] e2, float[] e3) {
		// Находим e2 как векторное произведение e1 на ось z (0, 0, 1)
		// (a[z]b[y] - a[y]b[z], a[x]b[z] - a[z]b[x], a[y]b[z] - a[x]b[y])
		float[] axis = new float[3];
		// Выбираем ось с наименьшим скалярным произведением (уменьшение ошибок
		// округления).
		float minPr = Math.min(Math.min(Math.abs(e1[0]), Math.abs(e1[1])),
				Math.abs(e1[2]));
		if (minPr == Math.abs(e1[0])) {
			axis[0] = 1f;
		} else if (minPr == Math.abs(e1[1])) {
			axis[1] = 1f;
		} else {
			axis[2] = 1f;
		}
		rot(e1, axis, e2);
		rot(e1, e2, e3);
	}

	private void rot(float[] a, float[] b, float[] r) {
		r[0] = a[2] * b[1] - a[1] * b[2];
		r[1] = a[0] * b[2] - a[2] * b[0];
		r[2] = a[1] * b[2] - a[0] * b[1];
	}

	public float[] getGravity() {
		WatchdogService watchdogService = getA().getWatchdogService();
		float[] gravity = watchdogService.getGravity();
		return gravity;
	}

}
