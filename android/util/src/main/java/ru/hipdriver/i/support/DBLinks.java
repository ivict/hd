/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i.support;

public class DBLinks {

	private DBLinks() { }

	public static short link(AppStatesEnum appState) {
		return (short) (appState.ordinal() + 7);
	}
	
	public static AppStatesEnum unlinkAppState(short appStateId) {
		AppStatesEnum[] values = AppStatesEnum.values();
		int idx = appStateId - 7;
		if (idx < 0) {
			return null;
		}
		if (idx >= values.length) {
			return null;
		}
		return values[idx];
	}
	
	public static short link(CarStatesEnum carState) {
		return (short) (carState.ordinal() + 7);
	}

	public static CarStatesEnum unlinkCarState(short carStateId) {
		CarStatesEnum[] values = CarStatesEnum.values();
		int idx = carStateId - 7;
		if (idx < 0) {
			return null;
		}
		if (idx >= values.length) {
			return null;
		}
		return values[idx];
	}
	

	public static short link(EventClassesEnum eventClass) {
		return (short) (eventClass.ordinal()+ 7);
	}
	
	public static short link(ControlModsEnum controlMode) {
		return (short) (controlMode.ordinal() + 7);
	}

	public static ControlModsEnum unlinkControlMode(short controlModeId) {
		ControlModsEnum[] values = ControlModsEnum.values();
		int idx = controlModeId - 7;
		if (idx < 0) {
			return null;
		}
		if (idx >= values.length) {
			return null;
		}
		return values[idx];
	}
	
	public static short link(UserCommandTypesEnum controlMode) {
		//3 Get from database
		return (short) (controlMode.ordinal() + 3);
	}

	public static UserCommandTypesEnum unlinkUserCommandType(short controlModeId) {
		//3 Get from database
		UserCommandTypesEnum[] values = UserCommandTypesEnum.values();
		int idx = controlModeId - 3;
		if (idx < 0) {
			return null;
		}
		if (idx >= values.length) {
			return null;
		}
		return values[idx];
	}
	
	public static short link(MapProvidersEnum mapProvider) {
		//0 Get from database
		return (short) (mapProvider.ordinal() + 0);
	}

	public static MapProvidersEnum unlinkMapProvider(short mapProviderId) {
		//0 Get from database
		MapProvidersEnum[] values = MapProvidersEnum.values();
		int idx = mapProviderId - 0;
		if (idx < 0) {
			return null;
		}
		if (idx >= values.length) {
			return null;
		}
		return values[idx];
	}
	
}
