/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i.support;

import java.util.HashMap;
import java.util.Map;
/**
 * <b>Перечисление типов команд пользователей</b>, самый критический момент
 * синхронизация с таблицей user_command_types в базе данных.
 * Основная проблема - обмен этой информацией с мобильным агентом.
 * Исполнитель команд в настоящий момент опирается на код, дабы не 
 * переусложнять логику, используем настоящий подход.
 * Кроме этого есть соображение стабильности работы сервера приложений,
 * очевидно, что перезагружать его из за добавления новой команды
 * пользователя невероятное попустительство.
 * @see ru.hipdriver.kernel.db.db.changelog-*.xml for synchronization
 * @author ivict
 */
public enum UserCommandTypesEnum {
	/**
	 * activate-car-alarm
	 */
	ACA("activate-car-alarm"),
	/**
	 * switch-off-car-alarm
	 */
	SOCA("switch-off-car-alarm"),
	/**
	 * where-are-you
	 */
	WAY("where-are-you"),
	/**
	 * make-photo
	 */
	MP("make-photo"),
	/**
	 * remote-listen
	 */
	RL("remote-listen"),
	/**
	 * edit-settings
	 */
	ES("edit-settings"),
	/**
	 * get-info
	 */
	GI("get-info");

	//Index
	private final static Map<String, UserCommandTypesEnum> globalKeys = 
			new HashMap<String, UserCommandTypesEnum>();//TODO: size of enum as param
	//Update index
	static {
		for ( UserCommandTypesEnum e : UserCommandTypesEnum.values() ) {
			globalKeys.put(e.globalKey, e);
		}
	}

	private final String globalKey;
	private UserCommandTypesEnum(String globalKey) {
		this.globalKey = globalKey;
	}

	public String getGlobalKey() {
		return globalKey;
	}

	public static UserCommandTypesEnum valueOfGlobalKey(String globalKey) {
		return globalKeys.get(globalKey);
	}

}
