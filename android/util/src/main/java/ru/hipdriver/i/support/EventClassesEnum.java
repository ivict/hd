/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i.support;

import java.util.HashMap;
import java.util.Map;
/**
 * <b>Перечисление классов событий</b>, самый критический момент
 * синхронизация с таблицей event_classes в базе данных.
 * Основная проблема - обмен этой информацией с мобильным агентом.
 * Детектор событий в настоящий момент опирается на код, дабы не 
 * переусложнять логику, используем настоящий подход.
 * В случае наличия свободного времени, попробуйте решить задачу, путем переноса фокуса из базы в файл ресурсов. В случае "идеального" решения, данный файл может генерится на этапе сборки, по файлу ресурсов.
 * @see ru.hipdriver.kernel.db.db.changelog-*.xml for synchronization
 * @author ivict
 */
public enum EventClassesEnum {
	/**
	 * user-push-switch-on-button-in-application-on-mobile-agent
	 */
	UPSONBIAOMA("user-push-switch-on-button-in-application-on-mobile-agent"),
	/**
	 * user-push-switch-off-button-in-application-on-mobile-agent
	 */
	UPSOFBIAOMA("user-push-switch-off-button-in-application-on-mobile-agent"),
	/**
	 * mobile-agent-send-correction-location-event
	 */
	MASCLE("mobile-agent-send-correction-location-event"),
	/**
	 * mobile-agent-send-gsm-location-event
	 */
	MASGSLE("mobile-agent-send-gsm-location-event"),
	/**
	 * mobile-agent-send-gps-location-event
	 */
	MASGPLE("mobile-agent-send-gps-location-event"),
	/**
	 * sensor-tracking-data-event
	 */
	STDE("sensor-tracking-data-event"),
	/**
	 * send-alert-track-event
	 */
	SATE("send-alert-track-event"),
	/**
	 * send-alert-event
	 */
	SAE("send-alert-event"),
	/**
	 * sign-up-mobile-agent
	 */
	SUMA("sign-up-mobile-agent"),
	/**
	 * send-email-event
	 */
	SEE("send-email-event")
	;

	//Index
	private final static Map<String, EventClassesEnum> globalKeys = 
		new HashMap<String, EventClassesEnum>();//TODO: size of enum as param
	//Update index
	static {
		for ( EventClassesEnum e : EventClassesEnum.values() ) {
			globalKeys.put(e.globalKey, e);
		}
	}

	private final String globalKey;
	private EventClassesEnum(String globalKey) {
		this.globalKey = globalKey;
	}

	public String getGlobalKey() {
		return globalKey;
	}

	public static EventClassesEnum valueOfGlobalKey(String globalKey) {
		return globalKeys.get(globalKey);
	}

}
