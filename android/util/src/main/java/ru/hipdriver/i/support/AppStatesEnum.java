/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i.support;

import java.util.HashMap;
import java.util.Map;
/**
 * <b>Перечисление состояний приложения</b>, самый критический момент
 * синхронизация с таблицей app_states в базе данных.
 * Основная проблема - обмен этой информацией с мобильным агентом.
 * Детектор событий в настоящий момент опирается на код, дабы не 
 * переусложнять логику, используем настоящий подход.
 * В случае наличия свободного времени, попробуйте решить задачу, путем переноса фокуса из базы в файл ресурсов. В случае "идеального" решения, данный файл может генерится на этапе сборки, по файлу ресурсов.
 * @see ru.hipdriver.kernel.db.db.changelog-*.xml for synchronization
 * @author ivict
 */
public enum AppStatesEnum {
	/**
	 * unknown, intermediate state between car-alarm-switched-off and car-alarm-activated  
	 */
	U("unknown"),
	/**
	 * car-alarm-activated
	 */
	CAC("car-alarm-activated"),
	/**
	 * application switched off (car-alarm-switched-off)
	 */
	ASO("car-alarm-switched-off"),
	/**
	 * beacon-mode-activated
	 */
	BMA("beacon-mode-activated")
	;

	//Index
	private final static Map<String, AppStatesEnum> globalKeys = 
		new HashMap<String, AppStatesEnum>();//TODO: size of enum as param
	//Update index
	static {
		for ( AppStatesEnum e : AppStatesEnum.values() ) {
			globalKeys.put(e.globalKey, e);
		}
	}

	private final String globalKey;
	private AppStatesEnum(String globalKey) {
		this.globalKey = globalKey;
	}

	public String getGlobalKey() {
		return globalKey;
	}

	public static AppStatesEnum valueOfGlobalKey(String globalKey) {
		return globalKeys.get(globalKey);
	}

}
