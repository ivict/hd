/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i.support;

import java.util.HashMap;
import java.util.Map;

/**
 * <b>Перечисление сервисов</b>, доступных для мобильного агента.
 * @author ivict
 *  
 */
public enum ServicesEnum {
	/**
	 * /kernel/services/events/push
	 */
	KSEP("/kernel/services/events/push"),
	/**
	 * /kernel/services/events/pusha
	 */
	KSEPA("/kernel/services/events/pusha"),
	/**
	 * /kernel/services/sign/up
	 */
	KSSU("/kernel/services/sign/up"),
	/**
	 * /kernel/services/sign/in
	 */
	KSSI("/kernel/services/sign/in"),
	/**
	 * /kernel/pub/ping;
	 */
	KPP("/kernel/pub/ping"),
	/**
	 * /kernel/pub/pubkey;
	 */
	KPPK("/kernel/pub/pubkey"),
	/**
	 * /kernel/pub/pubkey-update;
	 */
	KPPKU("/kernel/pub/pubkey-update"),
	/**
	 * /kernel/pub/mymas;
	 */
	KPM("/kernel/pub/mymas"),
	/**
	 * /kernel/pub/host;
	 */
	KPH("/kernel/pub/host"),
	/**
	 * /kernel/pub/mas;
	 */
	KPMAS("/kernel/pub/mas"),
	/**
	 * /kernel/pub/mas;
	 */
	KPMAP("/kernel/pub/map"),
	/**
	 * /kernel/pub/mas;
	 */
	KPEMAP("/kernel/pub/emap"),
	/**
	 * /kernel/pub/cheml;
	 */
	KCHEML("/kernel/pub/cheml"),
	/**
	 * /kernel/pub/hash;
	 */
	KHASH("/kernel/pub/hash");
	
	//Index
	private final static Map<String, ServicesEnum> paths = 
		new HashMap<String, ServicesEnum>();//TODO: size of enum as param
	//Update index
	static {
		for ( ServicesEnum e : ServicesEnum.values() ) {
			paths.put(e.PATH, e);
		}
	}

	public final String PATH;
	private ServicesEnum(String globalKey) {
		this.PATH = globalKey;
	}

	public static ServicesEnum valueOfPath(String path) {
		return paths.get(path);
	}

}