/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.i.support;

import java.util.HashMap;
import java.util.Map;
/**
 * <b>Перечисление состояний машины</b>, самый критический момент
 * синхронизация с таблицей car_states в базе данных.
 * Основная проблема - обмен этой информацией с мобильным агентом.
 * Детектор событий в настоящий момент опирается на код, дабы не 
 * переусложнять логику, используем настоящий подход.
 * В случае наличия свободного времени, попробуйте решить задачу, путем переноса фокуса из базы в файл ресурсов. В случае "идеального" решения, данный файл может генерится на этапе сборки, по файлу ресурсов.
 * @see ru.hipdriver.kernel.db.db.changelog-*.xml for synchronization
 * @author ivict
 */
public enum CarStatesEnum {
	/**
	 * unknown
	 */
	U("unknown"),
	/**
	 * alarm
	 */
	A("alarm"),
	/**
	 * car-stolen
	 */
	CS("car-stolen"),
	/**
	 * steal-wheels
	 */
	WS("steal-wheels"),
	/**
	 * car-evacuate
	 */
	CE("car-evacuate"),
	/**
	 * car-poke
	 */
	JAM("jamming"),
	/**
	 * car-poke
	 */
	CP("car-poke")
	;

	//Index
	private final static Map<String, CarStatesEnum> globalKeys = 
		new HashMap<String, CarStatesEnum>();//TODO: size of enum as param
	//Update index
	static {
		for ( CarStatesEnum e : CarStatesEnum.values() ) {
			globalKeys.put(e.globalKey, e);
		}
	}

	private final String globalKey;
	private CarStatesEnum(String globalKey) {
		this.globalKey = globalKey;
	}

	public String getGlobalKey() {
		return globalKey;
	}

	public static CarStatesEnum valueOfGlobalKey(String globalKey) {
		return globalKeys.get(globalKey);
	}

}
