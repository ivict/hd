package ru.hipdriver.i.support;

public enum AppFacesEnum {
	/**
	 * car-alarms
	 */
	CAR_ALARMS,
	/**
	 * remote-control
	 */
	REMOTE_CONTROL;

	public static AppFacesEnum parse(String appFaceText) {
		if (appFaceText == null) {
			return null;
		}
		try {
			return AppFacesEnum.valueOf(appFaceText.toUpperCase());
		} catch (Throwable th) {
			return null;
		}
	}
	
	public static AppFacesEnum valueOf(int ordinal) {
		AppFacesEnum[] values = AppFacesEnum.values();
		if (ordinal < 0) {
			return null;
		}
		if (ordinal >= values.length) {
			return null;
		}
		return values[ordinal];
	}	
}
