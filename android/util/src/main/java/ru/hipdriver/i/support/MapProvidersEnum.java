package ru.hipdriver.i.support;

import java.util.HashMap;
import java.util.Map;

/**
 * <b>Перечисление провайдеров географии</b>, самый критический момент
 * синхронизация с таблицей map_providers в базе данных.
 * Основная проблема - обмен этой информацией с мобильным агентом.
 * Детектор событий в настоящий момент опирается на код, дабы не 
 * переусложнять логику, используем настоящий подход.
 * В случае наличия свободного времени, попробуйте решить задачу, путем переноса фокуса из базы в файл ресурсов. В случае "идеального" решения, данный файл может генерится на этапе сборки, по файлу ресурсов.
 * @see ru.hipdriver.kernel.db.db.changelog-*.xml for synchronization
 * @author ivict
 */
public enum MapProvidersEnum {
	/**
	 * google, geography based by google info  
	 */
	GOOGLE("google"),
	/**
	 * yandex, geography based by yandex info  
	 */
	YANDEX("yandex");

	//Index
	private final static Map<String, MapProvidersEnum> globalKeys = 
		new HashMap<String, MapProvidersEnum>();//TODO: size of enum as param
	//Update index
	static {
		for ( MapProvidersEnum e : MapProvidersEnum.values() ) {
			globalKeys.put(e.globalKey, e);
		}
	}

	private final String globalKey;
	private MapProvidersEnum(String globalKey) {
		this.globalKey = globalKey;
	}

	public String getGlobalKey() {
		return globalKey;
	}

	public static MapProvidersEnum valueOfGlobalKey(String globalKey) {
		return globalKeys.get(globalKey);
	}
	
	public static MapProvidersEnum valueOf(int ordinal) {
		MapProvidersEnum[] values = MapProvidersEnum.values();
		if (ordinal < 0) {
			return null;
		}
		if (ordinal >= values.length) {
			return null;
		}
		return values[ordinal];
	}	
	
}
