package ru.hipdriver.i.support;

import java.util.HashMap;
import java.util.Map;

/**
 * <b>Перечисление режимов управления</b>, самый критический момент
 * синхронизация с таблицей control_modes в базе данных.
 * Основная проблема - обмен этой информацией с мобильным агентом.
 * Детектор событий в настоящий момент опирается на код, дабы не 
 * переусложнять логику, используем настоящий подход.
 * В случае наличия свободного времени, попробуйте решить задачу, путем переноса фокуса из базы в файл ресурсов. В случае "идеального" решения, данный файл может генерится на этапе сборки, по файлу ресурсов.
 * @see ru.hipdriver.kernel.db.db.changelog-*.xml for synchronization
 * @author ivict
 */
public enum ControlModsEnum {
	/**
	 * internet, communication through chat server  
	 */
	INTERNET("internet"),
	/**
	 * phone-calls, communication through mobile network operator (usual calls) 
	 */
	PHONE_CALLS("phone-calls"),
	/**
	 * sms, communication through mobile network operator (use not interactive sms)
	 */
	SMS("sms");

	//Index
	private final static Map<String, ControlModsEnum> globalKeys = 
		new HashMap<String, ControlModsEnum>();//TODO: size of enum as param
	//Update index
	static {
		for ( ControlModsEnum e : ControlModsEnum.values() ) {
			globalKeys.put(e.globalKey, e);
		}
	}

	private final String globalKey;
	private ControlModsEnum(String globalKey) {
		this.globalKey = globalKey;
	}

	public String getGlobalKey() {
		return globalKey;
	}

	public static ControlModsEnum valueOfGlobalKey(String globalKey) {
		return globalKeys.get(globalKey);
	}
	
	public static ControlModsEnum valueOf(int ordinal) {
		ControlModsEnum[] values = ControlModsEnum.values();
		if (ordinal < 0) {
			return null;
		}
		if (ordinal >= values.length) {
			return null;
		}
		return values[ordinal];
	}	
	
}
