package ru.hipdriver.j.support;

import java.util.Arrays;

import ru.hipdriver.d.Sensors;
import ru.hipdriver.j.ext.SensorData;

public abstract class SensorDataPusher {

	public final static float _100 = 100f;
	
	// Measure rate in second
	private float mr;// = ACC_MEASURE_RATE_IN_MILLIS / 1000f;

	private final float t = 40 / 1000f;// 40ms see min Human reaction
										// constant

	private boolean resetJ1J2J1N = false;

	private int eventsCount;
	long totalTimeDiff;
	long prevTimestamp;

	float[] ev = new float[3];
	//Gravitation constant in cm per second square
	private float g = 879.3386f;//978f;
	double[] gCalibration = new double[3];
	double avgG;
	int gCalibrationMeasureCount;
	
	private final SensorData accelerometerData;
	private final SensorData calibrationData;

	private class MeanBank {
		int pos;
		float[] data0 = new float[3];
		float[] data1 = new float[3];
		float[] data2 = new float[3];
		float[] data3 = new float[3];
		float[] data4 = new float[3];
		float[] data5 = new float[3];
		float[] data6 = new float[3];
		float[] data7 = new float[3];

		float[] result = new float[3];

		void addValue(float[] value) {
			int bankIndex = pos % 8;
			switch (bankIndex) {
			case 0:
				System.arraycopy(value, 0, data0, 0, 3);
				break;
			case 1:
				System.arraycopy(value, 0, data1, 0, 3);
				break;
			case 2:
				System.arraycopy(value, 0, data2, 0, 3);
				break;
			case 3:
				System.arraycopy(value, 0, data3, 0, 3);
				break;
			case 4:
				System.arraycopy(value, 0, data4, 0, 3);
				break;
			case 5:
				System.arraycopy(value, 0, data5, 0, 3);
				break;
			case 6:
				System.arraycopy(value, 0, data6, 0, 3);
				break;
			case 7:
				System.arraycopy(value, 0, data7, 0, 3);
				break;
			}
			pos++;
		}

		float[] getResult() {
			result[0] = (data0[0] + data1[0] + data2[0] + data3[0]
					+ data4[0] + data5[0] + data6[0] + data7[0]) / 8f;
			result[1] = (data0[1] + data1[1] + data2[1] + data3[1]
					+ data4[1] + data5[1] + data6[1] + data7[1]) / 8f;
			result[2] = (data0[2] + data1[2] + data2[2] + data3[2]
					+ data4[2] + data5[2] + data6[2] + data7[2]) / 8f;
			return result;
		}

	}

	private MeanBank bank = new MeanBank();
	private MeanBank bank2 = new MeanBank();
	private boolean calibrationMode;
	private float[] oldValues = new float[3];
	private float[] measured = new float[3];
	//long oldTimeStamp = System.nanoTime();
	//TODO: refactoring for below two fields
	protected float[] gravity = new float[3];
	protected int sensorTid = -1;

	public SensorDataPusher(SensorData sensorData) {
		this.accelerometerData = sensorData;
		this.calibrationData = Sensors.makeForNMeasures(9);
	}

	public SensorDataPusher(SensorData sensorData, SensorData calibrationData) {
		this.accelerometerData = sensorData;
		this.calibrationData = calibrationData;
	}

	public void resetJ1J2J1N() {
		resetJ1J2J1N = true;
	}

	public void setMrInMillis(long mrInMillis) {
		mr = mrInMillis / 1000f;
	}

	public void setBaseTime(long baseTimeInMillis) {
		accelerometerData.setBaseTime(baseTimeInMillis);
	}

	public void pushValues(long nanoTime, float[] data) {
		if (data.length < 3) {
			return;
		}
		long timestamp = nanoTime;
		//float deltaTimeInS = ((timestamp - oldTimeStamp) / 1000) / 1000000f;
		ev[0] = (data[0] - oldValues[0]) * _100;// / deltaTimeInS;
		ev[1] = (data[1] - oldValues[1]) * _100;// / deltaTimeInS;
		ev[2] = (data[2] - oldValues[2]) * _100;// / deltaTimeInS;

		measured[0] = data[0] * 100f;
		measured[1] = data[1] * 100f;
		measured[2] = data[2] * 100f;
		
		final SensorData sensorData;

		// Meaning values
		bank.addValue(ev);
		bank2.addValue(measured);
		// Hack TODO: refactoring
		if (bank.pos < 8) {
			return;
		}
		float[] acc = bank.getResult();
		float[] m = bank2.getResult();

		if (calibrationMode) {
			sensorTid = myTid();
			sensorData = calibrationData;
			gCalibration[0] += m[0];
			gCalibration[1] += m[1];
			gCalibration[2] += m[2];
			avgG += sqrt(m[0] * m[0] + m[1] * m[1] + m[2] * m[2]);
			gCalibrationMeasureCount++;
		} else {
			sensorData = accelerometerData;
			if (resetJ1J2J1N) {
				Arrays.fill(sensorData.J1, 0);
				Arrays.fill(sensorData.J12N, 0);
				Arrays.fill(sensorData.J1Minus4, 0);
				Arrays.fill(sensorData.J1Minus2, 0);
				Arrays.fill(sensorData.J1ERR, 0);
				resetJ1J2J1N = false;
			}
		}
		// Push in memory as old values
		System.arraycopy(data, 0, oldValues, 0, 3);
		//oldTimeStamp = timestamp;

		if (sensorData.preparingMode) {
			//Вычисление углов в предположении, что устройство находится
			//в однородном поле гравитации, в состоянии относительного покоя.
			//Под относительным покоем, понимается преимущественно продольные
			//колебания поверхности на которой находится телефон, при редких
			//поперечных колебаниях высокой частоты.
			
			//Тогда из условия data == g + a, получаем окружность решений
			//вектора g. Выбор конкретного решения получаем налагая условие максимальности
			//проекции вектора g на одну из осей. Пусть это будет ось z.
			
			//gravity calculation
			
			//Уравнения:
			//g[0] * g[0] + g[1] * g[1] + g[2] * g[2] == g * g
			//(m[0] - g[0]) * (m[0] - g[0]) + (m[1] - g[1]) * (m[1] - g[1]) + (m[2] - g[2]) * (m[2] - g[2]) == m * m - g * g
			//max g[2] => Применяя условие минимакса, производная по z равна 0
			//Делаем подстановку выражая g[0] через g[1] и g[2] первого уравнения во второе.
			//После чего диффиренциируем уравнение 2 по g[2], получаем равенство
			//g[2] = m[2] * g * g / (m[2] * m[2] + m[0] * m[0])
			//Далее решаем квадратичное уравнение 2 относительно g[1], выбирая корень, который максимизирует его по g[2]
			//После чего находим g[0] из уравнения 1.
			//FloatMath.sqrt(m[0] * m[0] + m[1] * m[1] + m[2] * m[2])
			//FloatMath.sqrt(gravity[0] * gravity[0] + gravity[1] * gravity[1] + gravity[2] * gravity[2])
			g = sqrt(m[0] * m[0] + m[1] * m[1] + m[2] * m[2]);
			gravity[2] = m[2];
			gravity[1] = m[1];
			gravity[0] = m[0];
//			gravity[2] = m[2] * g * g / (m[2] * m[2] + m[0] * m[0]);
//			//Дискриминант / (4 * g * g) 
//			float D = m[0] * m[0] * m[1] * m[1] + m[0] * m[0] * m[0] * m[0] +
//					m[2] * m[2] * g * g * (m[1] * m[1] + m[0] * m[0]) / (m[2] * m[2] + m[0] * m[0]) -
//					g * g * m[0] * m[0];
//			if (D < 0) {//Wrong value
//				return;
//			}
//			float g2plus = (m[1] * g * g + g * FloatMath.sqrt(D)) / (m[1] * m[1] + m[0] * m[0]);
//			float g2minus = (m[1] * g * g - g * FloatMath.sqrt(D)) / (m[1] * m[1] + m[0] * m[0]);
//			gravity[1] = Math.abs(g2minus) > Math.abs(g2plus) ? g2plus : g2minus;
//			gravity[0] = FloatMath.sqrt(g * g - gravity[1] * gravity[1] - gravity[2] * gravity[2]);
//			//Math.acos(g[0]/g);
			
			//Угол скалярного произведения вектора гравитации на единичный вектор оси x
			sensorData.ANGLES[0] = (float) Math.acos(gravity[0] / g) * 180f / (float) Math.PI;
			sensorData.ANGLES[1] = (float) Math.acos(gravity[1] / g) * 180f / (float) Math.PI;
			sensorData.ANGLES[2] = (float) Math.acos(gravity[2] / g) * 180f / (float) Math.PI;
			
			Sensors.caapDataInDebugMode(sensorData, acc[0], acc[1], acc[2],
					timestamp);
			return;
		}
		Sensors.caapData(sensorData, data[0] * _100, data[1] * _100, data[2] * _100,
				timestamp);

	}
	
	abstract protected int myTid();
	abstract protected float sqrt(float value);

	public long getMeasureRate() {
		return totalTimeDiff / (eventsCount * 1000000); // Converts from
														// nanos to millis
	}

	public void switchOnCalibration() {
		sensorTid = -1;
		calibrationMode = true;
		Arrays.fill(gCalibration, 0d);
		gCalibrationMeasureCount = 0;
		avgG = 0;
	}

	public void switchOffCalibration() {
		calibrationMode = false;
		gCalibration[0] /= (double) gCalibrationMeasureCount;
		gCalibration[1] /= (double) gCalibrationMeasureCount;
		gCalibration[2] /= (double) gCalibrationMeasureCount;
		//g = (float) Math.sqrt(gCalibration[0] * gCalibration[0] + gCalibration[1] * gCalibration[1] + gCalibration[2] * gCalibration[2]);
		g = (float) (avgG / gCalibrationMeasureCount);
	}
	
	public void reset() {
		bank.pos = 0;
	}

}
