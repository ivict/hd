package ru.hipdriver.android.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PasswordGenerator {

	private static char[] initCharacters() {
		final int initialCapacity = 63;
		// a vector is a variable-size array
		final List<Character> chars = new ArrayList<Character>(initialCapacity);

		// add digits 0–9
		for (char c = '0'; c <= '9'; c++) {
			chars.add(c);
		}

		// add uppercase A–Z and '@'
		for (char c = '@'; c <= 'Z'; c++) {
			chars.add(c);
		}

		// add lowercase a–z
		for (char c = 'a'; c <= 'z'; c++) {
			chars.add(c);
		}

		// Copy the chars over to a simple array, now that we know
		// the length. The .toArray method could have been used here,
		// but its usage is a pain.

		final char[] charArray = new char[chars.size()];

		for (int i = 0; i < chars.size(); i++) {
			charArray[i] = chars.get(i).charValue();
		}

		return charArray;
	}

	public String next(int passwordSize) {
		char[] characters = initCharacters();
		Random random = new Random();
		StringBuilder password = new StringBuilder(passwordSize);
		for (int i = 0; i < passwordSize; ++i) {
			//TODO: only unique symbols
			password.append(characters[random.nextInt(characters.length)]);
		}
		return password.toString();
	}

}
