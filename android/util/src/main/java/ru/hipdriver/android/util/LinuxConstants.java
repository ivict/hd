package ru.hipdriver.android.util;

public class LinuxConstants {
	
	private LinuxConstants() { }
	
	//@see linux/rtc.h
	public static int RTC_AIE_ON = 28673;
	public static int RTC_AIE_OFF = 28674;
	public static int RTC_UIE_ON = 28675;
	public static int RTC_UIE_OFF = 28676;
	public static int RTC_PIE_ON = 28677;
	public static int RTC_PIE_OFF = 28678;
	public static int RTC_WIE_ON = 28687;
	public static int RTC_WIE_OFF = 28688;

	public static int RTC_ALM_READ = -2145095672;
	public static int RTC_ALM_SET = 1076129799;
	public static int RTC_RD_TIME = -2145095671;
	public static int RTC_SET_TIME = 1076129802;
	public static int RTC_IRQP_READ = -2146930677;
	public static int RTC_IRQP_SET = 1074294796;
	public static int RTC_EPOCH_READ = -2146930675;
	public static int RTC_EPOCH_SET = 1074294798;

	public static int RTC_WKALM_RD = -2144833520;
	public static int RTC_WKALM_SET = 1076391951;

	public static int RTC_PLL_GET = -2145357807;
	public static int RTC_PLL_SET = 1075867666;

	public static int RTC_VL_READ = -2147192813;
	public static int RTC_VL_CLR = 28692;

	public static int SIZEOF_TIMESPEC = 16;
	public static int SIZEOF_RTC_TIME = 36;

	
	//@see android/android_alarm.h
	public static int ANDROID_ALARM_RTC_WAKEUP = 0;
	public static int ANDROID_ALARM_RTC = 1;
	public static int ANDROID_ALARM_ELAPSED_REALTIME_WAKEUP = 2;
	public static int ANDROID_ALARM_ELAPSED_REALTIME = 3;
	public static int ANDROID_ALARM_SYSTEMTIME = 4;
	public static int ANDROID_ALARM_TYPE_COUNT = 5;
	
	public static int ANDROID_ALARM_RTC_WAKEUP_MASK = (toSigned((short) 1) << ANDROID_ALARM_RTC_WAKEUP);
	public static int ANDROID_ALARM_RTC_MASK = (toSigned((short) 1) << ANDROID_ALARM_RTC);
	
	public static short toUnsigned(byte b) {
	    return (short)(b & 0xff);
	}
	 
	public static byte toSigned(short i) {
	     return (byte) i;
	}	

}
