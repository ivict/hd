package ru.hipdriver.android.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class TextTools {
	
	public static final String BIG_TAG = "ru.hipdriver";
	private static Map<String, DateFormat> dateFormats;
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	private TextTools() { }

	public static String extractLastSixNumbersFromPhoneNumber(String phoneNumber) {
		String withoutSignsAndSpaces = phoneNumber.replaceAll("\\s+|\\-|\\(|\\)", "");
		int len = withoutSignsAndSpaces.length();
		if (len > 6) {
			return withoutSignsAndSpaces.substring(len - 6, len);
		}
		return withoutSignsAndSpaces;
	}
	
	public static String extractPhoneNumber(String phoneNumber) {
		return phoneNumber.replaceAll("\\s+|\\-|\\(|\\)", "");
	}

	public static float safetyGetFloat(String textValue,
			float defaultValue) {
		try {
			String trimed = textValue.trim();
			return Float.valueOf(trimed);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	public static int safetyGetInt(String textValue, int defaultValue) {
		try {
			String trimed = textValue.trim();
			return Integer.valueOf(trimed);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	public static String getFirstNotEmptyLineInMessage(String message) {
		if (message == null) {
			return null;
		}
        String[] messageLines = message.split("\n");
		for (String messageLine : messageLines) {
			String trimmedLine = messageLine.trim();
			if (!trimmedLine.isEmpty()) {
				return trimmedLine;
			}
		}
		return null;
	}

	public static String format(String format, Date time) {
		DateFormat dateFormat = getDateFormat(format);
		return dateFormat.format(time);
	}

	private static DateFormat getDateFormat(String format) {
		if (dateFormats == null) {
			makeDateFormats();
		}
		DateFormat dateFormat = dateFormats.get(format);
		if (dateFormat == null) {
			dateFormat = makeDateFormat(dateFormats, format);
		}
		return dateFormat;
	}

	//By write synchronization
	private static synchronized DateFormat makeDateFormat(
			Map<String, DateFormat> dateFormats, String format) {
		DateFormat dateFormat = dateFormats.get(format);
		if (dateFormat != null) {
			return dateFormat;
		}
		dateFormat = new SimpleDateFormat(format);
		dateFormats.put(format, dateFormat);
		return dateFormat;
	}

	//By write synchronization
	private static synchronized void makeDateFormats() {
		if (dateFormats != null) {
			return;
		}
		dateFormats = new HashMap<String, DateFormat>();
	}
	
	public static String makeTag(Class<?> c) {
		if (c == null) {
			return null;
		}
		StringBuilder tag = new StringBuilder(BIG_TAG);
		tag.append(':').append(c.getSimpleName());
		return tag.toString();
	}
	
	public static String printIntoString( Throwable th ) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(out);
		th.printStackTrace(ps);
		ps.close();
		return out.toString();
	}

	public static CharSequence restrictLine(CharSequence line, int maxCharsCount,
			String suffixOnOverheadMaximum) {
		if (line == null) {
			return "";
		}
		if (line.length() <= maxCharsCount) {
			return line;
		}
		StringBuilder text = new StringBuilder(line.subSequence(0, maxCharsCount));
		text.append(suffixOnOverheadMaximum);
		return text.toString();
	}

	public static String extractUserName(String email) {
		if (email == null) {
			return null;
		}
		int pos = email.indexOf('@');
		if (pos < 0) {
			return email;
		}
		return email.substring(0, pos);
	}

	//@see http://stackoverflow.com/questions/9655181/convert-from-byte-array-to-hex-string-in-java
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}

	public static boolean isTimeLimited(String applicationVersion) {
		return applicationVersion.endsWith("]tl");
	}

	public static String withoutPackage(String className) {
		if (className == null) {
			return null;
		}
		int lastIndexOfPoint = className.lastIndexOf('.');
		if (lastIndexOfPoint < 0) {
			return className;
		}
		return className.substring(lastIndexOfPoint + 1);
	}	
	
}
