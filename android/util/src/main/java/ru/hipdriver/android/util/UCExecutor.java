package ru.hipdriver.android.util;

import ru.hipdriver.i.support.UserCommandTypesEnum;
import ru.hipdriver.j.StandardResponse;

public interface UCExecutor {

	StandardResponse exec(UserCommandTypesEnum commandType, Object... args);
	
}
