package ru.hipdriver.android.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * Tools for working with streams
 * @author ivict
 *
 */
public class StreamTools {
	
	private StreamTools() { }

	public static byte[] readNextBufferFromStream(InputStream inputStream,
			int maxSizeOfFragment) throws IOException {
		int b;
		byte buffer[] = new byte[maxSizeOfFragment];
		int i = 0;
		while ( i < maxSizeOfFragment && (b = inputStream.read()) > -1) {
			buffer[i++] = (byte) b;
		}
		if (i == 0) {
			return null;
		}
		if (i < maxSizeOfFragment) {
			return Arrays.copyOf(buffer, i);
		}
		return buffer;			
	}
	
	public static SizedDataSource makeDataSource(final byte[] data, final String name, final String contentType) {
		return new SizedDataSource() {

			@Override
			public String getContentType() {
				return contentType;
			}

			@Override
			public InputStream getInputStream() throws IOException {
				return new ByteArrayInputStream(data);
			}

			@Override
			public String getName() {
				return name;
			}

			@Override
			public OutputStream getOutputStream() throws IOException {
				throw new IllegalStateException("Input only datasource!");
			}

			@Override
			public int getInputStreamSize() {
				return data.length;
			}
			
		};
	}

	
}
