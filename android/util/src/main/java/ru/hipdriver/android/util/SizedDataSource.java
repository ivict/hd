package ru.hipdriver.android.util;

import javax.activation.DataSource;

public interface SizedDataSource extends DataSource {
	
	int getInputStreamSize();

}
