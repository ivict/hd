package ru.hipdriver.d;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import math.jwave.Transform;
import math.jwave.transforms.DiscreteFourierTransform;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import ru.hipdriver.d.Sensors.MinMaxValue;
import ru.hipdriver.j.EventWithAtach;
import ru.hipdriver.j.ext.SensorData;
import ru.hipdriver.j.support.SensorDataPusher;

public class RealDataTest {
	
	private SensorData load(String resource) throws Throwable {
		ObjectMapper om = new ObjectMapper();
		SensorData sensorData;
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream inputStream = classLoader.getResourceAsStream(resource);
			if (inputStream == null) {
				inputStream = new FileInputStream(resource);
			}
			Assert.assertNotNull(inputStream);
			EventWithAtach event = om.readValue(inputStream, EventWithAtach.class);
			byte[] content = event.getContent();
			sensorData = om.readValue(content, SensorData.class);
		} catch (Throwable th) {
			System.out.printf("Not event12 format, try to usual sensor-data format.").println();
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream inputStream = classLoader.getResourceAsStream(resource);
			if (inputStream == null) {
				inputStream = new FileInputStream(resource);
			}
			Assert.assertNotNull(inputStream);
			sensorData = om.readValue(inputStream, SensorData.class);
		}
		System.out.printf("Resource [%s] loaded", resource).println();
		byte[] data = sensorData.data;
		assertNotNull(data);
		//countOfValues is json ignored.
		if (sensorData.preparingMode) {
			sensorData.setCountOfValues(data.length / (4 * 5 * 3));
			System.out.printf("Prepared sensor data from accelerometer [%s] data length [%d] bytes.", sensorData.getName(), sensorData.data.length).println();
		} else {
			sensorData.setCountOfValues(data.length / (4 * 5));
			System.out.printf("Sensor data from accelerometer [%s] data length [%d] bytes.", sensorData.getName(), sensorData.data.length).println();
		}
		
		if (!sensorData.preparingMode) {
			float[] eventData = new float[3];
			int minCountOfValues = data.length / (4 * 5);
			SensorData preparedSensorData = Sensors.cloneSensorData(sensorData, new byte[minCountOfValues * 4 * 5 * 3]);
			preparedSensorData.preparingMode = true;
			preparedSensorData.setCountOfValues(0);
			//TODO: fix cyclic case in initial data
			SensorDataPusher sensorDataPusher = new SensorDataPusher(preparedSensorData){

				@Override
				protected int myTid() {
					return (int) Thread.currentThread().getId();
				}

				@Override
				protected float sqrt(float value) {
					return (float) Math.sqrt(value);
				}
				
			};
			//Move to initial offset of cyclic data
			int initialPosition = Sensors.getFirstPosition(sensorData);
			for (int i = initialPosition; i < minCountOfValues; i++) {
				ByteBuffer value = Sensors.readBuffer(data, i);
				eventData[0] = value.getFloat();
				eventData[1] = value.getFloat();
				eventData[2] = value.getFloat();
				long nanoTime = value.getLong();
				sensorDataPusher.pushValues(nanoTime, eventData);
			}
			for (int i = 0; i < initialPosition; i++) {
				ByteBuffer value = Sensors.readBuffer(data, i);
				eventData[0] = value.getFloat();
				eventData[1] = value.getFloat();
				eventData[2] = value.getFloat();
				long nanoTime = value.getLong();
				sensorDataPusher.pushValues(nanoTime, eventData);
			}
			sensorData = preparedSensorData;
			System.out.printf("Prepared sensor data from accelerometer [%s] data length [%d] bytes.", sensorData.getName(), sensorData.data.length).println();
		}
		System.out.printf("Count of values in sensor data [%s]", sensorData.getCountOfValues()).println();
		System.out.println();
		return sensorData;
	}
	
	@Test
	public void loadSensorTrack() throws Throwable {
		SensorData sensorData = load("event12/silence-car-stop.json");
		assertNotNull(sensorData);
		assertTrue(sensorData.data.length > 5 * 4);
		System.out.println(sensorData.data.length);
//		byte[] data = sensorData.data;
//		int countOfValues = sensorData.countOfValues;
//		CBuffer cbuff = new CBuffer(0, countOfValues, 0, data, 5 * 4);
//		for (ByteBuffer bb : cbuff.all()) {
//			
//		}
	}
	
	@Ignore
	public void dftTest() throws Throwable {
		System.out.println("====== TEST CASE U: ======");
		//SensorData sensorData = load("/tmp/sensor-huawei-1418.json");
		SensorData sensorData = load("event12/jack-test-case3.json");
		assertNotNull(sensorData);

		assertTrue(sensorData.data.length > 5 * 4);
		long timeInNanos = Sensors.getLastTimestamp(sensorData);
		
	    Transform t = new Transform( new DiscreteFourierTransform( ) );

	    long _60s = 60000000000L;

	    // arrTime = { r1, c1, r2, c2, ... }
	    //x and y angles variations
	    Sensors.VectorData2 data0 = Sensors.getData(sensorData, 2, 0, 1, timeInNanos - _60s, 2 * _60s); 
	    double[ ] arrTime0 = data0.values0;
	    double[ ] arrFreq0 = t.forward( arrTime0 ); // 1-D DFT forward
	    float[] spectr0 = new float[data0.count / 2];
	    float[] times0 = new float[data0.count / 2];
	    float period0 = (data0.t / 1000000) / 1000f;
	    for (int i = 1; i < data0.count / 2; i ++) {
	    	spectr0[i] = Math.abs((float) arrFreq0[2 * i] / (float) data0.count);
	    	times0[i] = period0 / i;
	    }
	    writeCsv("/tmp/spectr0.csv", spectr0, times0);
	    
	    Sensors.VectorData2 data1 = Sensors.getData(sensorData, 2, 0, 1, timeInNanos, _60s); 
	    double[ ] arrTime1 = data1.values0;
	    double[ ] arrFreq1 = t.forward( arrTime1 ); // 1-D DFT forward
	    float[] spectr1 = new float[data1.count / 2];
	    float[] times1 = new float[data1.count / 2];
	    float period1 = (data1.t / 1000000) / 1000f;
	    for (int i = 1; i < data1.count / 2; i ++) {
	    	spectr1[i] = Math.abs((float) arrFreq1[2 * i] / (float) data1.count);
	    	times1[i] = period1 / i;
	    }
	    writeCsv("/tmp/spectr1.csv", spectr1, times1);
//		long _10s = 10000000000L;
//		MinMaxValue minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, timeInNanos,  _60s, 0.001f, 4);
//		assertNotNull(minMaxAngles);
//		System.out.printf("Min-x [%s], Min-y [%s], Min-z [%s]", minMaxAngles.minValue[0], minMaxAngles.minValue[1], minMaxAngles.minValue[2]).println();
//		System.out.printf("Max-x [%s], Max-y [%s], Max-z [%s]", minMaxAngles.maxValue[0], minMaxAngles.maxValue[1], minMaxAngles.maxValue[2]).println();
//		float angleDeltaX = Math.abs(minMaxAngles.maxValue[0] - minMaxAngles.minValue[0]);
//		float angleDeltaY = Math.abs(minMaxAngles.maxValue[1] - minMaxAngles.minValue[1]);
//		float maxAngleDelta = Math.max(angleDeltaX, angleDeltaY);
//		System.out.printf("angleDeltaX [%s], angleDeltaY[%s], maxAngleDelta[%s]", angleDeltaX, angleDeltaY, maxAngleDelta).println();
//
//		MinMaxValue minMaxAngles1 = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, timeInNanos,  _60s, 0.1f, 1);
//		assertNotNull(minMaxAngles1);
//		System.out.printf("Min-x [%s], Min-y [%s], Min-z [%s]", minMaxAngles1.minValue[0], minMaxAngles1.minValue[1], minMaxAngles1.minValue[2]).println();
//		System.out.printf("Max-x [%s], Max-y [%s], Max-z [%s]", minMaxAngles1.maxValue[0], minMaxAngles1.maxValue[1], minMaxAngles1.maxValue[2]).println();
//		float angleDeltaX1 = Math.abs(minMaxAngles1.maxValue[0] - minMaxAngles1.minValue[0]);
//		float angleDeltaY1 = Math.abs(minMaxAngles1.maxValue[1] - minMaxAngles1.minValue[1]);
//		float maxAngleDelta1 = Math.max(angleDeltaX1, angleDeltaY1);
//		System.out.printf("angleDeltaX1 [%s], angleDeltaY1[%s], maxAngleDelta1[%s]", angleDeltaX1, angleDeltaY1, maxAngleDelta1).println();
//		
//		//Alert Angle Detection
//		Sensors.Value stdevAngles = Sensors.standardDeviationCyclic(sensorData, 2, timeInNanos - _10s,  _60s);
//		System.out.printf("stdev-angle-x [%s], stdev-angle-y [%s], stdev-angle-z [%s]", stdevAngles.value[0], stdevAngles.value[1], stdevAngles.value[2]).println();
//		Sensors.Value stdevAcc = Sensors.standardDeviationCyclic(sensorData, 1, timeInNanos - _10s,  _60s);
//		System.out.printf("stdev-acc-x [%s], stdev-acc-y [%s], stdev-acc-z [%s]", stdevAcc.value[0], stdevAcc.value[1], stdevAcc.value[2]).println();
//		Sensors.Value stdevJerk = Sensors.standardDeviationCyclic(sensorData, 0, timeInNanos - _10s,  _60s);
//		System.out.printf("stdev-jerk-x [%s], stdev-jerk-y [%s], stdev-jerk-z [%s]", stdevJerk.value[0], stdevJerk.value[1], stdevJerk.value[2]).println();
//		
//		Sensors.ScalarValue jerkProjection = Sensors.jerkProjectionOnGravityCyclic(sensorData, timeInNanos,  _60s);
//		System.out.printf("Jerk projection [%s]", jerkProjection.value).println();
		
//		byte[] data = sensorData.data;
//		int countOfValues = sensorData.countOfValues;
//		CBuffer cbuff = new CBuffer(0, countOfValues, 0, data, 5 * 4);
//		for (ByteBuffer bb : cbuff.all()) {
//			
//		}
//		ObjectMapper om = new ObjectMapper();
//		om.writeValue(new File("/tmp/sensor-data-last-test.json"), sensorData);
	}

	@Ignore
	public void angleVectorsTest() throws Throwable {
		System.out.println("====== TEST CASE U: ======");
		//SensorData sensorData = load("/tmp/sensor-huawei-1418.json");
		//SensorData sensorData = load("event12/jack-test-case5.json");
		SensorData sensorData = load("/tmp/event12-huawei2-2133.json");
		assertNotNull(sensorData);

		assertTrue(sensorData.data.length > 5 * 4);
	    long _5s =   5000000000L;
	    long _60s = 60000000000L;

		//Точка срабатывания тревоги
		long time1 = Sensors.getLastTimestamp(sensorData);
		long time0 = time1 - _60s;
		//TODO: разбить интервал по пять секунд
		List<Float> list0XDelta = new ArrayList<Float>();
		List<Float> list0YDelta = new ArrayList<Float>();
		for (int i = 0; i < 6; i++) {
			long t = time0 - (5 - i) * _5s;
			MinMaxValue minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, t, _5s, 0.2f, 1);
			if (minMaxAngles != null) {
				float xDelta = minMaxAngles.maxValue[0] - minMaxAngles.minValue[0];
				float yDelta = minMaxAngles.maxValue[1] - minMaxAngles.minValue[1];
				list0XDelta.add(xDelta);
				list0YDelta.add(yDelta);
			}
		}
		
		System.out.println(Arrays.toString(list0XDelta.toArray()));
		System.out.println(Arrays.toString(list0YDelta.toArray()));
		
		List<Float> list1XDelta = new ArrayList<Float>();
		List<Float> list1YDelta = new ArrayList<Float>();
		for (int i = 0; i < 6; i++) {
			long t = time1 - (5 - i) * _5s;
			MinMaxValue minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, t, _5s, 0.2f, 1);
			if (minMaxAngles != null) {
				float xDelta = minMaxAngles.maxValue[0] - minMaxAngles.minValue[0];
				float yDelta = minMaxAngles.maxValue[1] - minMaxAngles.minValue[1];
				list1XDelta.add(xDelta);
				list1YDelta.add(yDelta);
			}
		}
		
		System.out.println(Arrays.toString(list1XDelta.toArray()));
		System.out.println(Arrays.toString(list1YDelta.toArray()));
		
	}
	
	private void writeCsv(String fileName, float[] spectr, float[] times) throws Throwable {
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false)); 
		writer.append("t;f");
		writer.newLine();
		for (int i = 1; i < spectr.length; i++) {
			writer.append(String.valueOf(times[i])).append(';');
			writer.append(String.valueOf(spectr[i]));
			writer.newLine();
		}
		writer.flush();
		writer.close();
	}

	@Test
	public void jackTestCase1() throws Throwable {
		System.out.println("====== TEST CASE 1: JACK ======");
		SensorData sensorData = load("event12/jack-test-case1.json");
		
		assertNotNull(sensorData);
		assertTrue(sensorData.data.length > 5 * 4);
		long timeInNanos = Sensors.getLastTimestamp(sensorData);
		long _10s = 10000000000L;
		long _60s = 60000000000L;
		MinMaxValue minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, timeInNanos - _10s,  _60s, 0.001f, 4);
		assertNotNull(minMaxAngles);
		System.out.printf("Min-x [%s], Min-y [%s], Min-z [%s]", minMaxAngles.minValue[0], minMaxAngles.minValue[1], minMaxAngles.minValue[2]).println();
		System.out.printf("Max-x [%s], Max-y [%s], Max-z [%s]", minMaxAngles.maxValue[0], minMaxAngles.maxValue[1], minMaxAngles.maxValue[2]).println();
		float angleDeltaX = Math.abs(minMaxAngles.maxValue[0] - minMaxAngles.minValue[0]);
		float angleDeltaY = Math.abs(minMaxAngles.maxValue[1] - minMaxAngles.minValue[1]);
		float maxAngleDelta = Math.max(angleDeltaX, angleDeltaY);
		System.out.printf("angleDeltaX [%s], angleDeltaY[%s], maxAngleDelta[%s]", angleDeltaX, angleDeltaY, maxAngleDelta).println();
		assertTrue(maxAngleDelta > 0.9);
		//Alert Angle Detection
		Sensors.Value stdevAngles = Sensors.standardDeviationCyclic(sensorData, 2, timeInNanos - _10s,  _60s);
		System.out.printf("stdev-angle-x [%s], stdev-angle-y [%s], stdev-angle-z [%s]", stdevAngles.value[0], stdevAngles.value[1], stdevAngles.value[2]).println();
		Sensors.Value stdevAcc = Sensors.standardDeviationCyclic(sensorData, 1, timeInNanos - _10s,  _60s);
		System.out.printf("stdev-acc-x [%s], stdev-acc-y [%s], stdev-acc-z [%s]", stdevAcc.value[0], stdevAcc.value[1], stdevAcc.value[2]).println();
		Sensors.Value stdevJerk = Sensors.standardDeviationCyclic(sensorData, 0, timeInNanos - _10s,  _60s);
		System.out.printf("stdev-jerk-x [%s], stdev-jerk-y [%s], stdev-jerk-z [%s]", stdevJerk.value[0], stdevJerk.value[1], stdevJerk.value[2]).println();
//		byte[] data = sensorData.data;
//		int countOfValues = sensorData.countOfValues;
//		CBuffer cbuff = new CBuffer(0, countOfValues, 0, data, 5 * 4);
//		for (ByteBuffer bb : cbuff.all()) {
//			
//		}
//		ObjectMapper om = new ObjectMapper();
//		om.writeValue(new File("/tmp/sensor-data-last-test.json"), sensorData);
	}

	@Test
	public void silenceCase1() throws Throwable {
		System.out.println("====== TEST CASE 2: SILENCE ======");
		SensorData sensorData = load("event12/silence-car-stop.json");
		
		assertNotNull(sensorData);
		assertTrue(sensorData.data.length > 5 * 4);
		long timeInNanos = Sensors.getLastTimestamp(sensorData);
		long _60s = 60000000000L;
		long _80s = 60000000000L;
		MinMaxValue minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, timeInNanos - _80s,  _60s, 0.001f, 4);
		assertNotNull(minMaxAngles);
		System.out.printf("Min-x [%s], Min-y [%s], Min-z [%s]", minMaxAngles.minValue[0], minMaxAngles.minValue[1], minMaxAngles.minValue[2]).println();
		System.out.printf("Max-x [%s], Max-y [%s], Max-z [%s]", minMaxAngles.maxValue[0], minMaxAngles.maxValue[1], minMaxAngles.maxValue[2]).println();
		float angleDeltaX = Math.abs(minMaxAngles.maxValue[0] - minMaxAngles.minValue[0]);
		float angleDeltaY = Math.abs(minMaxAngles.maxValue[1] - minMaxAngles.minValue[1]);
		float maxAngleDelta = Math.max(angleDeltaX, angleDeltaY);
		System.out.printf("angleDeltaX [%s], angleDeltaY[%s], maxAngleDelta[%s]", angleDeltaX, angleDeltaY, maxAngleDelta).println();
		//assertTrue(maxAngleDelta > 0.9);
		//Alert Angle Detection
		Sensors.Value stdevAngles = Sensors.standardDeviationCyclic(sensorData, 2, timeInNanos - _80s,  _60s);
		System.out.printf("stdev-angle-x [%s], stdev-angle-y [%s], stdev-angle-z [%s]", stdevAngles.value[0], stdevAngles.value[1], stdevAngles.value[2]).println();
		Sensors.Value stdevAcc = Sensors.standardDeviationCyclic(sensorData, 1, timeInNanos - _80s,  _60s);
		System.out.printf("stdev-acc-x [%s], stdev-acc-y [%s], stdev-acc-z [%s]", stdevAcc.value[0], stdevAcc.value[1], stdevAcc.value[2]).println();
		Sensors.Value stdevJerk = Sensors.standardDeviationCyclic(sensorData, 0, timeInNanos - _80s,  _60s);
		System.out.printf("stdev-jerk-x [%s], stdev-jerk-y [%s], stdev-jerk-z [%s]", stdevJerk.value[0], stdevJerk.value[1], stdevJerk.value[2]).println();
//		byte[] data = sensorData.data;
//		int countOfValues = sensorData.countOfValues;
//		CBuffer cbuff = new CBuffer(0, countOfValues, 0, data, 5 * 4);
//		for (ByteBuffer bb : cbuff.all()) {
//			
//		}
//		ObjectMapper om = new ObjectMapper();
//		om.writeValue(new File("/tmp/sensor-data-last-test.json"), sensorData);
	}

	@Test
	public void silenceCase2() throws Throwable {
		System.out.println("====== TEST CASE 3: SILENCE ======");
		SensorData sensorData = load("event12/silence-heavy-road.json");
		
		assertNotNull(sensorData);
		assertTrue(sensorData.data.length > 5 * 4);
		long timeInNanos = Sensors.getLastTimestamp(sensorData);
		long _60s = 60000000000L;
		long _80s = 60000000000L;
		MinMaxValue minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, timeInNanos - _80s,  _60s, 0.001f, 4);
		assertNotNull(minMaxAngles);
		System.out.printf("Min-x [%s], Min-y [%s], Min-z [%s]", minMaxAngles.minValue[0], minMaxAngles.minValue[1], minMaxAngles.minValue[2]).println();
		System.out.printf("Max-x [%s], Max-y [%s], Max-z [%s]", minMaxAngles.maxValue[0], minMaxAngles.maxValue[1], minMaxAngles.maxValue[2]).println();
		float angleDeltaX = Math.abs(minMaxAngles.maxValue[0] - minMaxAngles.minValue[0]);
		float angleDeltaY = Math.abs(minMaxAngles.maxValue[1] - minMaxAngles.minValue[1]);
		float maxAngleDelta = Math.max(angleDeltaX, angleDeltaY);
		System.out.printf("angleDeltaX [%s], angleDeltaY[%s], maxAngleDelta[%s]", angleDeltaX, angleDeltaY, maxAngleDelta).println();
		//assertTrue(maxAngleDelta > 0.9);
		//Alert Angle Detection
		Sensors.Value stdevAngles = Sensors.standardDeviationCyclic(sensorData, 2, timeInNanos - _80s,  _60s);
		System.out.printf("stdev-angle-x [%s], stdev-angle-y [%s], stdev-angle-z [%s]", stdevAngles.value[0], stdevAngles.value[1], stdevAngles.value[2]).println();
		Sensors.Value stdevAcc = Sensors.standardDeviationCyclic(sensorData, 1, timeInNanos - _80s,  _60s);
		System.out.printf("stdev-acc-x [%s], stdev-acc-y [%s], stdev-acc-z [%s]", stdevAcc.value[0], stdevAcc.value[1], stdevAcc.value[2]).println();
		Sensors.Value stdevJerk = Sensors.standardDeviationCyclic(sensorData, 0, timeInNanos - _80s,  _60s);
		System.out.printf("stdev-jerk-x [%s], stdev-jerk-y [%s], stdev-jerk-z [%s]", stdevJerk.value[0], stdevJerk.value[1], stdevJerk.value[2]).println();
//		byte[] data = sensorData.data;
//		int countOfValues = sensorData.countOfValues;
//		CBuffer cbuff = new CBuffer(0, countOfValues, 0, data, 5 * 4);
//		for (ByteBuffer bb : cbuff.all()) {
//			
//		}
//		ObjectMapper om = new ObjectMapper();
//		om.writeValue(new File("/tmp/sensor-data-last-test.json"), sensorData);
	}

	@Test
	public void silenceCase3() throws Throwable {
		System.out.println("====== TEST CASE 4: SILENCE ======");
		SensorData sensorData = load("event12/silence-light-road.json");
		
		assertNotNull(sensorData);
		assertTrue(sensorData.data.length > 5 * 4);
		long timeInNanos = Sensors.getLastTimestamp(sensorData);
		long _60s = 60000000000L;
		long _80s = 60000000000L;
		MinMaxValue minMaxAngles = Sensors.minMaxValueAverageByNPointsLPFCyclic(sensorData, 2, timeInNanos - _80s,  _60s, 0.001f, 4);
		assertNotNull(minMaxAngles);
		System.out.printf("Min-x [%s], Min-y [%s], Min-z [%s]", minMaxAngles.minValue[0], minMaxAngles.minValue[1], minMaxAngles.minValue[2]).println();
		System.out.printf("Max-x [%s], Max-y [%s], Max-z [%s]", minMaxAngles.maxValue[0], minMaxAngles.maxValue[1], minMaxAngles.maxValue[2]).println();
		float angleDeltaX = Math.abs(minMaxAngles.maxValue[0] - minMaxAngles.minValue[0]);
		float angleDeltaY = Math.abs(minMaxAngles.maxValue[1] - minMaxAngles.minValue[1]);
		float maxAngleDelta = Math.max(angleDeltaX, angleDeltaY);
		System.out.printf("angleDeltaX [%s], angleDeltaY[%s], maxAngleDelta[%s]", angleDeltaX, angleDeltaY, maxAngleDelta).println();
		//assertTrue(maxAngleDelta > 0.9);
		//Alert Angle Detection
		Sensors.Value stdevAngles = Sensors.standardDeviationCyclic(sensorData, 2, timeInNanos - _80s,  _60s);
		System.out.printf("stdev-angle-x [%s], stdev-angle-y [%s], stdev-angle-z [%s]", stdevAngles.value[0], stdevAngles.value[1], stdevAngles.value[2]).println();
		Sensors.Value stdevAcc = Sensors.standardDeviationCyclic(sensorData, 1, timeInNanos - _80s,  _60s);
		System.out.printf("stdev-acc-x [%s], stdev-acc-y [%s], stdev-acc-z [%s]", stdevAcc.value[0], stdevAcc.value[1], stdevAcc.value[2]).println();
		Sensors.Value stdevJerk = Sensors.standardDeviationCyclic(sensorData, 0, timeInNanos - _80s,  _60s);
		System.out.printf("stdev-jerk-x [%s], stdev-jerk-y [%s], stdev-jerk-z [%s]", stdevJerk.value[0], stdevJerk.value[1], stdevJerk.value[2]).println();
//		byte[] data = sensorData.data;
//		int countOfValues = sensorData.countOfValues;
//		CBuffer cbuff = new CBuffer(0, countOfValues, 0, data, 5 * 4);
//		for (ByteBuffer bb : cbuff.all()) {
//			
//		}
//		ObjectMapper om = new ObjectMapper();
//		om.writeValue(new File("/tmp/sensor-data-last-test.json"), sensorData);
	}

}
