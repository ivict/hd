package ru.hipdriver.android.util;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TextToolsTest {
	
	final private String in;
	final private String expectedOut1;
	final private String expectedOut2;
	
    @Parameters
    public static Collection<Object[]> configs() {
    	String[] case0 = {"", "", ""};
    	String[] case1 = {"23 45", "2345", "2345"};
    	String[] case2 = {"12345678", "345678", "12345678"};
    	String[] case3 = {"1234 56 78", "345678", "12345678"};
    	String[] case4 = {"+7(929)1234 56 78", "345678", "+792912345678"};
    	String[] case5 = {" ", "", ""};
    	
        return Arrays.asList(new Object[][] {
                { case0 },
                { case1 },
                { case2 },
                { case3 },
                { case4 },
                { case5 },
        });
    }
 
    public TextToolsTest(String... args) {
        this.in = args[0];
        this.expectedOut1 = args[1];
        this.expectedOut2 = args[2];
    }
    
    @Test
    public void extractLastSixNumbersFromPhoneNumber() {
    	String lastSixNumbers = TextTools.extractLastSixNumbersFromPhoneNumber(in);
    	Assert.assertNotNull(lastSixNumbers);
		Assert.assertEquals(expectedOut1, lastSixNumbers);
    }

    @Test
    public void extractPhoneNumber() {
    	String extractPhoneNumber = TextTools.extractPhoneNumber(in);
    	Assert.assertNotNull(extractPhoneNumber);
		Assert.assertEquals(expectedOut2, extractPhoneNumber);
    }
    
}
