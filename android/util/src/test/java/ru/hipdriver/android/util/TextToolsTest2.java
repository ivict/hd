package ru.hipdriver.android.util;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TextToolsTest2 {
	
	final private String in;
	final private String expectedOut;
	
    @Parameters
    public static Collection<Object[]> configs() {
    	String[] case0 = {null, null};
    	String[] case1 = {"", null};
    	String[] case2 = {" \n\n\n", null};
    	String[] case3 = {"23 \n45", "23"};
    	String[] case4 = {" \n\n\n12345678", "12345678"};
    	String[] case5 = {"\n1234\ne", "1234"};
    	
        return Arrays.asList(new Object[][] {
                { case0 },
                { case1 },
                { case2 },
                { case3 },
                { case4 },
                { case5 },
        });
    }
 
    public TextToolsTest2(String... args) {
        this.in = args[0];
        this.expectedOut = args[1];
    }

    @Test
    public void getFirstNotEmptyLineInMessage() {
    	String firstNotEmptyLine = TextTools.getFirstNotEmptyLineInMessage(in);
    	Assert.assertEquals(expectedOut, firstNotEmptyLine);
    }
    
}
