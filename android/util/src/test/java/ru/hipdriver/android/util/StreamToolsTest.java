package ru.hipdriver.android.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

import org.junit.Test;

public class StreamToolsTest {

	@Test
	public void testStringStream() throws Throwable {
		byte[] testData1 = "123456789".getBytes("UTF-8");
		ByteArrayInputStream testData1Stream = new ByteArrayInputStream(testData1);
		int readCount = 0;
		ByteBuffer result = ByteBuffer.allocate(9);
		byte[] buffer;
		while ((buffer = StreamTools.readNextBufferFromStream(testData1Stream, 2)) != null) {
			result.put(buffer);
			readCount++;
		}
		assertEquals(5, readCount);
		assertTrue(Arrays.equals(testData1, result.array()));
	}
	
}
