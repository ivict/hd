package ru.hipdriver.android.util;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TextToolsTest3 {
	
	final private String in;
	final private String expectedOut;
	
    @Parameters
    public static Collection<Object[]> configs() {
    	String[] case0 = {null, null};
    	String[] case1 = {"", ""};
    	String[] case2 = {"Class", "Class"};
    	String[] case3 = {".Class", "Class"};
    	String[] case4 = {"com.Class", "Class"};
    	String[] case5 = {"com.Class.", ""};
    	String[] case6 = {"com.hipdriver.Class", "Class"};
    	String[] case7 = {"com.hipdriver.Class$Subclass", "Class$Subclass"};
    	String[] case8 = {"com.hipdriver.Class$Subclass$0", "Class$Subclass$0"};
    	
        return Arrays.asList(new Object[][] {
                { case0 },
                { case1 },
                { case2 },
                { case3 },
                { case4 },
                { case5 },
                { case6 },
                { case7 },
                { case8 },
        });
    }
 
    public TextToolsTest3(String... args) {
        this.in = args[0];
        this.expectedOut = args[1];
    }

    @Test
    public void withoutPackage() {
    	String nameWithoutPackage = TextTools.withoutPackage(in);
    	Assert.assertEquals(expectedOut, nameWithoutPackage);
    }
    
}
