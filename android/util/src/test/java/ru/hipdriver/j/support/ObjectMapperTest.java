package ru.hipdriver.j.support;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Date;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import ru.hipdriver.g.Locations;
import ru.hipdriver.j.ErrorMessage;
import ru.hipdriver.j.Event;
import ru.hipdriver.j.Sid;

public class ObjectMapperTest {

	@Test
	public void testEvent() throws JsonGenerationException, JsonMappingException,
			IOException {
		ObjectMapper om = new ObjectMapper();
		//AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
		//om.getDeserializationConfig().setAnnotationIntrospector(introspector);
		//om.getSerializationConfig().setAnnotationIntrospector(introspector);
		Event e = new Event();
		String eJson = om.writeValueAsString(e);
		assertEquals("{\"cls\":0}", eJson);
		e.setTime(new Date(0));
		Locations.packGpsLocation(e);
		e.setTime(null);
		String eJsonWithGps = om.writeValueAsString(e);
		assertEquals("{\"cls\":0,\"gps\":\"AAAAAAAAAACAAAAAAAAAAAAAAAAAAAAA\"}", eJsonWithGps);
		e.setTime(new Date(0));
		Locations.packGsmLocation(e);
		e.setTime(null);
		String eJsonWithGpsAndGsm = om.writeValueAsString(e);
		assertEquals("{\"cls\":0,\"gsm\":\"AAAAAAAAAAD//////////wAAAAAAAAAA\",\"gps\":\"AAAAAAAAAACAAAAAAAAAAAAAAAAAAAAA\"}", eJsonWithGpsAndGsm);
	}

	@Test
	public void testSid() throws JsonGenerationException, JsonMappingException,
			IOException {
		ObjectMapper om = new ObjectMapper();
		Sid sid = new Sid();
		String sidJson1 = om.writeValueAsString(sid);
		assertEquals("{\"a\":0}", sidJson1);
		//Include error message
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setDescription("What The Terrible Failure!");
		sid.setErrorMessage(errorMessage);
		String sidJson2 = om.writeValueAsString(sid);
		assertEquals("{\"a\":0,\"error-message\":{\"id\":0,\"description\":\"What The Terrible Failure!\"}}", sidJson2);
	}

}
