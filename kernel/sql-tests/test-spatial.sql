#create database `hd`;

use `hd`;

drop table if exists `Points2`;
create table `Points2` (
    `loc` Point not null,
    `time` datetime not null,
    `route_id` bigint not null,
    `obj_id` bigint not null,
    `st` timestamp default current_timestamp on update current_timestamp,
    spatial index ( `loc` ),
    index ( `time` )
) engine = MyISAM;


drop table if exists `Points`;
create table `Points` (
    `loc` Point not null,
    `time` datetime not null,
    `route_id` bigint not null,
    `obj_id` bigint not null,
    `st` timestamp default current_timestamp on update current_timestamp,
    `lat` int,
    `lon` int,
    spatial index ( `loc` ),
    index ( `lat` ),
    index ( `lon` ),
    index ( `time` )
) engine = MyISAM;

select count(*) from `Points2`;
select AsText(`loc`), `time`, `route_id`, `obj_id` from `Points2`;

delete from `Points2`;

call generate_data1( 1, 1, 1 );

#Generate routes as long as one day (every 30 second reporting).
call generate_data1( 1, 10000, 24*60*2 );

drop procedure if exists `generate_data1`;
#Generate data for geo-objects and routes for them in table points
delimiter //
create procedure generate_data1(
    in objects_count int,       #Count of geo-objects
    in routes_per_object int,   #Count of routes per geo-objects
    in points_per_route int )   #Count of points per route
begin
    declare i int;
    declare lat int;
    declare lon int;
    declare loc Point;
    declare route_id int;
    declare obj_id int;

    #declare progress int;
    #declare total_count int;
    #declare progress_pointer int;
    #declare progress_step int;
    #declare next_progress_step_to_display int;
    ##Setup progress initials
    #set total_count = objects_count * routes_per_object * points_per_route;
    #set progress_pointer = 1;
    #set progress_step = 10;
    #set next_progress_step_to_display = progress_step;

    set route_id = 1;
    while route_id <= routes_per_object do
        set i = 1;
        while i <= points_per_route do
            set obj_id = 1;
            while obj_id <= objects_count do
                set lat = rand() * 1000000;
                set lon = rand() * 1000000;
                set loc = GeomFromText(concat('POINT(', lat, ' ', lon, ')'));

                insert into `Points2` (`loc`, `time`, `route_id`, `obj_id`)
                values (loc, now(), route_id, obj_id);

                ##Show progress every ten percents
                #set progress = round(( total_count / progress_pointer ) * 100);
                #set progress_pointer = progress_pointer + 1;
                #if progress >= next_progress_step_to_display then
                #    select concat(progress, '%');
                #    set next_progress_step_to_display = next_progress_step_to_display + progress_step;
                #end if;

                set obj_id = obj_id + 1;
            end while;
            set i = i + 1;
        end while;
        set route_id = route_id + 1;
    end while;
end; //
delimiter ;

delimiter //
CREATE PROCEDURE p13 (IN parameter1 INT)
BEGIN
DECLARE variable1 INT;
SET variable1 = parameter1 + 1;
CASE variable1
WHEN 0 THEN INSERT INTO t VALUES (17);
WHEN 1 THEN INSERT INTO t VALUES (18);
ELSE INSERT INTO t VALUES (19);
END CASE;
END; //
delimiter ;

###############################################################################
#
###############################################################################


drop procedure if exists `recreate_datai`;
#Generate data for geo-objects and routes for them in table points
delimiter //
create procedure recreate_datai()
begin
    declare hour int;
    declare minute int;

    set hour = 0;
    while hour < 24 do
        set minute = 0;
        while minute < 60 do
            call recreate_table_datai( hour, minute );
            set minute = minute + 1;
        end while;
        set hour = hour + 1;
    end while;
end; //
delimiter ;
drop procedure if exists `recreate_table_datai`;
delimiter //
create procedure recreate_table_datai (
    in hour int,    #Hour of day
    in minute int)  #Minute of day
begin
     set @table_name = concat('Pointi_', lpad(hour, 2, '0'), '_', lpad(minute, 2, '0'));
     set @drop_table = concat('drop table if exists `', @table_name, '`');
     prepare stmt from @drop_table;
     execute stmt;

     set @create_table = concat('
create table `', @table_name, '` (
    `lat` int not null, 
    `lon` int not null,
    `time` datetime not null,
    `route_id` bigint not null,
    `obj_id` bigint not null,
    index( `time` )
) engine = MyISAM');
     prepare stmt from @create_table;
     execute stmt;

     deallocate prepare stmt;
end; //
delimiter ;

drop table if exists `Pointi`;
create table `Pointi` (
    #`id` bigint not null auto_increment primary key,
    `lat` int not null, 
    `lon` int not null,
    `time` datetime not null,
    `route_id` bigint not null,
    `obj_id` bigint not null,
    index( `time` )
) engine = MyISAM
partition by linear hash( hour(`time`) * 60 + minute(`time`) )
partitions 720;

select count(*) from `Pointi`;
select * from `Pointi`;

delete from `Pointi`;

call generate_datai( 1, 1, 1 );

#Generate routes as long as one day (every 30 second reporting).
call generate_datai( 1, 10000, 24*60*2 );

#############################################

drop procedure if exists `generate_datai`;
#Generate data for geo-objects and routes for them in table points
delimiter //
create procedure generate_datai(
    in objects_count int,       #Count of geo-objects
    in routes_per_object int,   #Count of routes per geo-objects
    in points_per_route int )   #Count of points per route
begin
    declare i int;
    declare lat int;
    declare lon int;
    declare route_id int;
    declare obj_id int;
    declare hour int;
    declare minute int;
    declare second int;
    declare time datetime;

    set route_id = 1;
    while route_id <= routes_per_object do
        set i = 1;
        while i <= points_per_route do
            set obj_id = 1;
            while obj_id <= objects_count do
                set lat = rand() * 1000000;
                set lon = rand() * 1000000;
                set hour = rand() * 23;
                set minute = rand() * 59;
                set second = rand() * 59;
                set time = str_to_date(concat('21,11,2013,', lpad(hour, 2, '0'), ',', lpad(minute, 2, '0'), ',', lpad(second, 2, '0')), '%d,%m,%Y,%H,%i,%s');
                call insert_datai(hour, minute, lat, lon, time, route_id, obj_id);

                set obj_id = obj_id + 1;
            end while;
            set i = i + 1;
        end while;
        set route_id = route_id + 1;
    end while;
end; //
delimiter ;


drop procedure if exists `insert_datai`;
delimiter //
create procedure insert_datai(
    in hour int,
    in minute int,
    in lat int,
    in lon int,
    in time datetime,
    in route_id bigint,
    in obj_id bigint)
begin
    set @table_name = concat('Pointi_', lpad(hour, 2, '0'), '_', lpad(minute, 2, '0'));
    set @insert_into_table = concat('
insert into `', @table_name, '` (`lat`, `lon`, `time`, `route_id`, `obj_id`)
values (?, ?, ?, ?, ?);');
    prepare stmt from @insert_into_table;
    set @p1 = lat;
    set @p2 = lon;
    set @p3 = time;
    set @p4 = route_id;
    set @p5 = obj_id;
    execute stmt using @p1, @p2, @p3, @p4, @p5;
    deallocate prepare stmt;
end; //
delimiter ;
#############################################

drop procedure if exists `delete_datai`;
delimiter //
create procedure delete_datai(
    in hour int,
    in minute int)
begin
    set @table_name = concat('Pointi_', lpad(hour, 2, '0'), '_', lpad(minute, 2, '0'));
    set @delete_from_table = concat('
delete from `', @table_name, '`');
    prepare stmt from @delete_from_table;
    execute stmt;
    deallocate prepare stmt;
end; //
delimiter ;

drop procedure if exists `delete_all_datai`;
#Generate data for geo-objects and routes for them in table points
delimiter //
create procedure delete_all_datai()
begin
    declare hour int;
    declare minute int;

    set hour = 0;
    while hour < 24 do
        set minute = 0;
        while minute < 60 do
            call delete_datai( hour, minute );
            set minute = minute + 1;
        end while;
        set hour = hour + 1;
    end while;
end; //
delimiter ;

#############################################
drop procedure if exists `count_datai`;
delimiter //
create procedure count_datai(
    in hour int,
    in minute int)
begin
    set @table_name = concat('Pointi_', lpad(hour, 2, '0'), '_', lpad(minute, 2, '0'));
    set @count_in_table = concat('
select count(*) from `', @table_name, '`');
    prepare stmt from @count_in_table;
    execute stmt;
    deallocate prepare stmt;
end; //
delimiter ;

drop procedure if exists `count_all_datai`;
#Generate data for geo-objects and routes for them in table points
delimiter //
create procedure count_all_datai()
begin
    declare hour int;
    declare minute int;

    set hour = 0;
    while hour < 24 do
        set minute = 0;
        while minute < 60 do
            call count_datai( hour, minute );
            set minute = minute + 1;
        end while;
        set hour = hour + 1;
    end while;
end; //
delimiter ;

select * from `users`;
select * from `mobile_agents`;
select * from `event_classes`;
select * from `events`;

insert into `users` (`name`) values ('first-user');
insert into `mobile_agents` (`user_id`, `name`) values (2, 'first-mobile-agent');
insert into `events` (`user_id`, `mobile_agent_id`, `event_class_id`, `time`)
values (2, 2, 7, now());


call insert_datai(0, 0, 1, 1, str_to_date(concat('21,11,2013,', lpad(0, 2, '0'), ',', lpad(0, 2, '0')), '%d,%m,%Y,%H,%i'), 1, 1);
select * from `Pointi_01_01`;
delete from `Pointi_00_00`;

select hour(now()) * 60 + minute(now());
select 24 * 60;
