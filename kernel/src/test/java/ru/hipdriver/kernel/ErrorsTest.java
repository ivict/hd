package ru.hipdriver.kernel;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class ErrorsTest {

	@Test
	public void testConsistency() {
		Set<Long> errorIds = new HashSet<Long>();
		int count = 0;
		for ( Errors err : Errors.values() ) {
			errorIds.add(err.ID);
			count ++;
		}
		assertEquals(errorIds.size(), count);
	}

}
