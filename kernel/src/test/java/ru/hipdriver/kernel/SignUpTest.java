package ru.hipdriver.kernel;

import static org.junit.Assert.assertEquals;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;
import org.junit.Assert;
import org.junit.Test;

public class SignUpTest {

	@Test
	public void testPasswordHash() throws Throwable {
		String password = "test123";
		String drupalHash = "$S$DzXFbiLwmnn0Zf89ZW/o36TSgA2cZTdqruwkt4k1GHznXCa1OXHp";
		Assert.assertEquals(DRUPAL_HASH_LENGTH, drupalHash.length());
		System.out.printf("Drupal hash length [%s]", drupalHash.length()).println();
		//int countLog2 = DRUPAL_HASH_COUNT;
		//String setting = passwordGenerateSalt(countLog2);
		String setting = drupalHash.substring(0, 12); 
		String jbossHash = passwordCrypt("SHA-512", password, setting);
		assertEquals(drupalHash, jbossHash);
	}

	private String passwordCrypt(String algo, String password, String setting) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		//The first 12 characters of an existing hash are its setting string
		String settingShort = setting.substring(0, 12);
		
		if (settingShort.charAt(0) != '$' || settingShort.charAt(2) != '$') {
			return null;
		}
		int countLog2 = passwordGetCountLog2(settingShort);
		//Hashes may be imported from elsewhere, so we allow != DRUPAL_HASH_COUNT
		if (countLog2 < DRUPAL_MIN_HASH_COUNT || countLog2 > DRUPAL_MAX_HASH_COUNT) {
			return null;
		}
		String salt = settingShort.substring(4, 4 + 8);
		//Hashes must have an 8 character salt
		if (salt.length() != 8) {
			return null;
		}
		
		//Convert the base 2 logarithm into an integer
		int count = 1 << countLog2;
		
		byte[] passwordBytes = password.getBytes("UTF-8");
		byte[] saltBytes = salt.getBytes("UTF-8");
		byte[] input = concat(saltBytes, passwordBytes);
		MessageDigest md = MessageDigest.getInstance(algo);
		byte[] hash = md.digest(input);
		do {
			hash = md.digest(concat(hash, passwordBytes));
			//System.out.printf("hash length [%s]", hash.length).println(); 
		} while (--count > 0);
		
		int len = hash.length;
		//nn0Zf89ZW/o36TSgA2cZTdqruwkt4k1GHznXCa1OXHp
		String output = settingShort + passwordBase64Encode(hash, len);
		
		int expected = 12 + (8 * len) / 6 + 1;//+1 for ceil equivalent in drupal
		
		if (output.length() == expected) {
			return output.substring(0, DRUPAL_HASH_LENGTH);
		}
		return null;
	}

	private String passwordBase64Encode(byte[] input, int count) {
		StringBuilder output = new StringBuilder();
		int i = 0;
		do {
			int value = input[i++] & 0xff;
			output.append(PASSWORD_ITOA64.charAt(value & 0x3f));
			if (i < count) {
				value |= (input[i] & 0xff) << 8;
			}
			output.append(PASSWORD_ITOA64.charAt((value >> 6) & 0x3f));
			if (i++ >= count) {
				break;
			}
			if (i < count) {
				value |= (input[i] & 0xff) << 16;
			}
			output.append(PASSWORD_ITOA64.charAt((value >> 12) & 0x3f));
			if (i++ > count) {
				break;
			}
			output.append(PASSWORD_ITOA64.charAt((value >> 18) & 0x3f));
		} while (i < count);
		
		return output.toString();
	}
	
//	private long signedByteToUnsignedLong(byte b) {
//		return b & 0xff;
//	}

	private byte[] concat(byte[] arr1, byte[] arr2) {
		byte[] output = new byte[arr1.length + arr2.length];
		System.arraycopy(arr1, 0, output, 0, arr1.length);
		System.arraycopy(arr2, 0, output, arr1.length, arr2.length);
		return output;
	}

	private int passwordGetCountLog2(String setting) {
		char ch = setting.charAt(3);
		return PASSWORD_ITOA64.indexOf(ch);
	}

	private String passwordGenerateSalt(int countLog2) {
		StringBuilder text = new StringBuilder("$S$");
		//Ensure that cntLog2 is within set bounds
		int cntLog2 = passwordEnforceLog2Boundaries(countLog2);
		//We encode the final log2 iteration count in base 64
		char itoa64 = PASSWORD_ITOA64.charAt(cntLog2);
		text.append(itoa64);
		//6 bytes is the standard salt for a portable phpass hash
		byte[] randomSalt = new byte[6];
		new Random().nextBytes(randomSalt);
		String base64EncodedSalt = new String(Base64.encodeBase64(randomSalt)); 
		text.append(base64EncodedSalt);
		return text.toString();
	}
	
	private static final int DRUPAL_MIN_HASH_COUNT = 7;
	private static final int DRUPAL_MAX_HASH_COUNT = 30;
	private static final int DRUPAL_HASH_COUNT = 15;
	private static final int DRUPAL_HASH_LENGTH = 55;
	
	private int passwordEnforceLog2Boundaries(int countLog2) {
		if (countLog2 < DRUPAL_MIN_HASH_COUNT) {
			return DRUPAL_MIN_HASH_COUNT;
		} else if (countLog2 > DRUPAL_MAX_HASH_COUNT) {
			return DRUPAL_MAX_HASH_COUNT;
		}
		return countLog2;
	}
	
	private static final String PASSWORD_ITOA64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

}
