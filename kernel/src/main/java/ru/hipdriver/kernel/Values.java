package ru.hipdriver.kernel;

import java.util.regex.Pattern;

public class Values {
	
	private Values() { }

	private static final Pattern NUMBER = Pattern.compile("\\d+");
	public static boolean isNotNumber(String number) {
		return !NUMBER.matcher(number).matches();
	}
	
	public static boolean isNotDecimalIntegerNumber(String number) {
		if ( !NUMBER.matcher(number).matches() ) {
			return true;
		}
		int length = number.length();
		if ( length > 9 ) {
			return true;
		}
		if (length == 9 && number.charAt(0) > 1) {
			return true;
		}
		return  false;
	}

	public static boolean isNotDecimalShortNumber(String number) {
		if ( !NUMBER.matcher(number).matches() ) {
			return true;
		}
		int length = number.length();
		if ( length > 5 ) {
			return true;
		}
		if (length == 5 && number.charAt(0) > 3) {
			return true;
		}
		return  false;
	}

	public static boolean isValidLong(String longValue) {
		try {
			Long.valueOf(longValue);
			return true;
		} catch ( NumberFormatException e ) { }
		return false;
	}

	public static String getSqlMask(String possibleMask) {
		if ( possibleMask.indexOf('*') == -1 ) {
			return possibleMask;
		}
		//Translate to sql mask
		//TODO: check it
		String sqlMask = possibleMask.replaceAll("%", "\\%").replace('*', '%');
		return sqlMask;
	}
	
}
