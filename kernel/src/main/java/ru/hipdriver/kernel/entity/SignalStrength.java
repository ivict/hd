/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.hipdriver.i.IMobileAgentState;

/**
 * Signal strength Entity.
 * 
 * @author ivict
 */
@Entity
@Table(name = "signal_strengths")
public class SignalStrength implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="mobile_agent_id")
	private long mobileAgentId;
	
	@OneToOne
	@JoinColumn(name="mobile_agent_id")
	private MobileAgent mobileAgent;

	@Column(name="gsm_signal_strength")
	private int gsmSignalStrength = IMobileAgentState.UNKNOWN_GSM_SIGNAL_STRENGTH;

	@Column(name="gsm_bit_error_rate")
	private int gsmBitErrorRate = IMobileAgentState.UNKNOWN_GSM_BIT_ERROR_RATE;

	public MobileAgent getMobileAgent() {
		return mobileAgent;
	}

	public void setMobileAgent(MobileAgent mobileAgent) {
		this.mobileAgent = mobileAgent;
	}

	public int getGsmSignalStrength() {
		return gsmSignalStrength;
	}

	public void setGsmSignalStrength(int gsmSignalStrength) {
		this.gsmSignalStrength = gsmSignalStrength;
	}

	public int getGsmBitErrorRate() {
		return gsmBitErrorRate;
	}

	public void setGsmBitErrorRate(int gsmBitErrorRate) {
		this.gsmBitErrorRate = gsmBitErrorRate;
	}

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}
	
}
