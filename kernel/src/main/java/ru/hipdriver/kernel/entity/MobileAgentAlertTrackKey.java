/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Композитный ключ для треков мобильных агентов в состоянии тревоги. 
 * @author ivict
 */
public class MobileAgentAlertTrackKey implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long mobileAgentId;
	private Date time;
	private short carStateId;
	private long groupByTrackId;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hash(groupByTrackId);
		result = prime * result + carStateId;
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		result = prime * result + hash(mobileAgentId);
		return result;
	}
	private int hash(long value) {
		//@see Long#hashCode()
		return (int) (value ^ (value >>> 32));
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobileAgentAlertTrackKey other = (MobileAgentAlertTrackKey) obj;
		if (groupByTrackId != other.groupByTrackId) {
			return false;
		}
		if (carStateId != other.carStateId) {
			return false;
		}
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		if (mobileAgentId != other.mobileAgentId) {
			return false;
		}
		return true;
	}
	
	public static final MobileAgentAlertTrackKey make(long mobileAgentId, Date time, short carStateId, long groupByTrackId) {
		MobileAgentAlertTrackKey key = new MobileAgentAlertTrackKey();
		key.mobileAgentId = mobileAgentId;
		key.time = time;
		key.carStateId = carStateId;
		key.groupByTrackId = groupByTrackId;
		return key;
	}
	
}
