/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Память пользователя.<br>
 * Зачем это: пользователь входит с нескольких устройств и
 * нескольких браузеров, до тех пор пока он не покинул
 * распределенную сессию, мы сохраняем его сессионные переменные.
 * Фактически сессия начинается с момента первого логина и не завершается.
 * Для уменьшения замусоривания базы используем "Архивный горизонт".
 * @see ru.hipdriver.Invariants#ARCHIVE_HORIZON_IN_MILLIS
 * Кроме того, эта сущность, реализует концепт "памяти приложения".
 * @author ivict
 */
@Entity
@Table(name="user_memory_entries")
@IdClass(UserMemoryEntryKey.class)
public class UserMemoryEntry implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="user_id")
	private int userId;
	
	@Id
	@Column(name="key1")
	private String key;
	
	@Column(name="value")
	private String value;

	public long getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	   
}
