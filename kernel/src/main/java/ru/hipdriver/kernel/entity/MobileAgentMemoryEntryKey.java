/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

/**
 * Композитный ключ для памяти мобильного приложения. 
 * @author ivict
 */
public class MobileAgentMemoryEntryKey implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long mobileAgentId;
	private String key;
	
	public MobileAgentMemoryEntryKey() {
	}
	
	public MobileAgentMemoryEntryKey(long mobileAgentId, String key) {
		this();
		this.mobileAgentId = mobileAgentId;
		this.key = key;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + hash(mobileAgentId);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobileAgentMemoryEntryKey other = (MobileAgentMemoryEntryKey) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (mobileAgentId != other.mobileAgentId)
			return false;
		return true;
	}

	private int hash(long value) {
		return (int) (value ^ (value >>> 32));
	}
	
	
}
