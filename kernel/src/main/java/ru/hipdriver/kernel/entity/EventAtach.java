/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Event atach.
 * @author ivict
 */
@Entity
@Table(name="event_ataches")
public class EventAtach implements Serializable {
	private static final String EVENT_ATACH_WITHOUT_EVENT = "Event atach without event";

	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="event_id")
	private long eventId;
	
	@OneToOne
	@JoinColumn(name="event_id")
	private Event event;
	
	@Column(name="description")
	private String description;
	
	@Lob
	@Column(name="content")
	private byte[] content;
	
	@Column(name="content_check_sum")
	private long contentCheckSum;

	public long getEventId() {
//		if ( event == null ) {
//			return 0;
//		}
//		return event.getEventId();
		return eventId;
	}

	public void setEventId(long eventId) {
//		if ( event == null ) {
//			throw new IllegalStateException(EVENT_ATACH_WITHOUT_EVENT);
//		}
//		this.event.setId(eventId);
		this.eventId = eventId;
	}

	public byte[] getContent() {
		return content;
//		if ( content == null ) {
//			return null;
//		}
//		try {
//			long length = content.length();
//			return content.getBytes(0, (int) length);
//		} catch (Throwable th) {
//			throw new RuntimeException(th);
//		}
	}

	public void setContent(byte[] content) {
		this.content = content;
//		if ( content == null ) {
//			this.content = null;
//			return;
//		}
//		try {
//			this.content = new SerialBlob(content);
//		} catch (Throwable th) {
//			throw new RuntimeException(th);
//		}
	}

	public long getContentCheckSum() {
		return contentCheckSum;
	}

	public void setContentCheckSum(long contentCheckSum) {
		this.contentCheckSum = contentCheckSum;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	
}
