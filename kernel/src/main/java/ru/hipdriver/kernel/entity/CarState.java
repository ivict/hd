/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <b>Состояние машины</b>, в качеcтве примера можно привести:
 * Машина угнана, колеса снимаются/сняты, машина эвакуируется/эвакуирована и т.д.
 * @author ivict
 */
@Entity
@Table(name="car_states")
public class CarState implements Serializable, ru.hipdriver.i.ICarState {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//GeneratedValue(strategy=GenerationType.AUTO, generator="car_state_seq_gen")
	//@SequenceGenerator(name="car_state_seq_gen", sequenceName="CAR_STATE_SEQ")
	private short id;
	
	/**
	 * Имя состояния - <b>уникальный атрибут</b>.
	 */
	@Column(name = "name")
	private String name;

	/**
	 * Описание состояния машины.
	 */
	@Column(name = "description")
	private String description;

	@Override
	public short getId() {
		return id;
	}

	@Override
	public void setId(short id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
}
