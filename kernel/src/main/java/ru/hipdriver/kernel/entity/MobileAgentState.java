/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Mobile Agent Entity.
 * 
 * @author ivict
 */
@Entity
@Table(name = "mobile_agent_states")
public class MobileAgentState implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="mobile_agent_id")
	private long mobileAgentId;
	
	@OneToOne
	@JoinColumn(name="mobile_agent_id")
	private MobileAgent mobileAgent;
	
	@Column(name="battery_status")
	private int batteryStatus;
	
	@Column(name="battery_pct")
	private float batteryPct;
	
	@Column(name="car_state_id")
	private short carStateId;

	@ManyToOne
	@JoinColumn(name="car_state_id")
	private CarState carState;
	
	@Column(name="app_state_id")
	private short appStateId;
	
	@ManyToOne
	@JoinColumn(name="app_state_id")
	private AppState appState;
	
	@Column(name="version_code")
	private int versionCode;
	
	@Column(name="version_name")
	private String versionName;
	
	/**
	 * Дата и Время последнего события обновляющего статус (событие первой очереди).
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private Date time;

	/**
	 * Дата и Время последнего события обновляющего трек (событие второй очереди).
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_alert_track_time")
	private Date lastAlertTrackTime;

	/**
	 * Дата и Время последнего события обновляющего почту (событие третьей очереди).
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_email_time")
	private Date lastEmailTime;
	
	/**
	 * Not usage this constructor directly, use {@link ru.hipdriver.kernel.ejb3.MASHelper#makeMobileAgentState(long, java.util.Date)} instead
	 */
	@Deprecated
	public MobileAgentState() {
		Date createTime = new Date();
		this.time = createTime;  
		this.lastAlertTrackTime = createTime;  
		this.lastEmailTime = createTime;  
	}

	public MobileAgent getMobileAgent() {
		return mobileAgent;
	}

	public void setMobileAgent(MobileAgent mobileAgent) {
		this.mobileAgent = mobileAgent;
	}

	public int getBatteryStatus() {
		return batteryStatus;
	}

	public void setBatteryStatus(int batteryStatus) {
		this.batteryStatus = batteryStatus;
	}

	public float getBatteryPct() {
		return batteryPct;
	}

	public void setBatteryPct(float batteryPct) {
		this.batteryPct = batteryPct;
	}

	public short getCarStateId() {
		return carStateId;
	}

	public void setCarStateId(short carStateId) {
		this.carStateId = carStateId;
	}

	public short getAppStateId() {
		return appStateId;
	}

	public void setAppStateId(short appStateId) {
		this.appStateId = appStateId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
	public Date getLastAlertTrackTime() {
		return lastAlertTrackTime;
	}

	public void setLastAlertTrackTime(Date lastAlertTrackTime) {
		this.lastAlertTrackTime = lastAlertTrackTime;
	}

	public Date getLastEmailTime() {
		return lastEmailTime;
	}

	public void setLastEmailTime(Date lastEmailTime) {
		this.lastEmailTime = lastEmailTime;
	}

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public int getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public CarState getCarState() {
		return carState;
	}

	public void setCarState(CarState carState) {
		this.carState = carState;
	}

	public AppState getAppState() {
		return appState;
	}

	public void setAppState(AppState appState) {
		this.appState = appState;
	}

}
