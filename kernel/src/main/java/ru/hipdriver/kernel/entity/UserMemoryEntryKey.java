/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

/**
 * Композитный ключ для сессионной памяти. 
 * @author ivict
 */
public class UserMemoryEntryKey implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int userId;
	private String key;
	
	public UserMemoryEntryKey() {
	}
	
	public UserMemoryEntryKey(int userId, String key) {
		this();
		this.userId = userId;
		this.key = key;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + userId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserMemoryEntryKey other = (UserMemoryEntryKey) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
	
}
