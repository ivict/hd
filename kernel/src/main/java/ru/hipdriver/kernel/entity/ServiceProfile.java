/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

/**
 * Service Profile Entity.
 * 
 * @author ivict
 */
@Entity
@Table(name = "service_profiles")
public class ServiceProfile implements Serializable, ru.hipdriver.i.IServiceProfile {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// @GeneratedValue(strategy=GenerationType.AUTO,
	// generator="service_profile_seq_gen")
	// @SequenceGenerator(name="service_profile_seq_gen",
	// sequenceName="SERVICE_PROFILE_SEQ")
	private short id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;

	/**
     * @see ru.hipdriver.i.IServiceProfile#getId()
     */
	@Override
	public short getId() {
		return id;
	}

	/**
     * @see ru.hipdriver.i.IServiceProfile#setId(short)
     */
	@Override
	public void setId(short id) {
		this.id = id;
	}

	/**
     * @see ru.hipdriver.i.IServiceProfile#getName()
     */
	@Override
	public String getName() {
		return name;
	}

	/**
     * @see ru.hipdriver.i.IServiceProfile#setName(String)
     */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
     * @see ru.hipdriver.i.IServiceProfile#getDescription()
     */
	@Override
	public String getDescription() {
		return description;
	}

	/**
     * @see ru.hipdriver.i.IServiceProfile#setDescription(String)
     */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}

}
