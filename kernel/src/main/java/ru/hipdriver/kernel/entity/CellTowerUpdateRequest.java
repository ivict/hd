/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Cell tower Entity.
 * 
 * @author ivict
 */
@Entity
@Table(name = "cell_tower_update_requests")
@IdClass(CellTowerKey.class)
public class CellTowerUpdateRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="mcc")
	private int mcc;
	
	@Id
	@Column(name="mnc")
	private int mnc;
	
	@Id
	@Column(name="lac")
	private int lac;
	
	@Id
	@Column(name="cid")
	private int cid;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private Date time;

	public int getMcc() {
		return mcc;
	}

	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	public int getMnc() {
		return mnc;
	}

	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	public int getLac() {
		return lac;
	}

	public void setLac(int lac) {
		this.lac = lac;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
}
