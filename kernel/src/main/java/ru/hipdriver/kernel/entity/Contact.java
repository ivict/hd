/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <b>Контакт пользователя.</b><br>
 * Skype, e-mail, телефон или что либо другое
 * @author ivict
 *
 */
@Entity
@Table(name="contacts")
public class Contact implements Serializable, ru.hipdriver.i.IContact {
	private static final long serialVersionUID = 1L;	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@GeneratedValue(strategy=GenerationType.AUTO, generator="event_seq_gen")
	//@SequenceGenerator(name="event_seq_gen", sequenceName="EVENT_SEQ")
	private long id;
	
	/**
	 * Пользователь. 
	 */
	@Column(name="user_id")
	private int userId;
	
	/**
	 * Тип контакта. 
	 */
	@Column(name="contact_type_id")
	private short contactTypeId;

	/**
	 * Имя несет в себе дополнительную нагрузку, это зависит от типа контакта.
	 */
	@Column (name = "name")
	private String name;

	/**
     * @see ru.hipdriver.i.IContact#getId()
     */
	@Override
	public long getId() {
		return id;
	}

	/**
     * @see ru.hipdriver.i.IContact#setId(long)
     */
	@Override
	public void setId(long id) {
		this.id = id;
	}

	/**
     * @see ru.hipdriver.i.IContact#getUserId()
     */
	@Override
	public int getUserId() {
		return userId;
	}

	/**
     * @see ru.hipdriver.i.IContact#setUserId(int)
     */
	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
     * @see ru.hipdriver.i.IContact#getContactTypeId()
     */
	@Override
	public short getContactTypeId() {
		return contactTypeId;
	}

	/**
     * @see ru.hipdriver.i.IContact#setContactTypeId(short)
     */
	@Override
	public void setContactTypeId(short contactTypeId) {
		this.contactTypeId = contactTypeId;
	}

	/**
     * @see ru.hipdriver.i.IContact#getName(int)
     */
	@Override
	public String getName() {
		return name;
	}

	/**
     * @see ru.hipdriver.i.IContact#setName(String)
     */
	@Override
	public void setName(String name) {
		this.name = name;
	}

}
