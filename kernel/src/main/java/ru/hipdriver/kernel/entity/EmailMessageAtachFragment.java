/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.hipdriver.i.IEmailMessageAtach;
import ru.hipdriver.i.IEmailMessageAtachFragment;

/**
 * Email message.
 * @author ivict
 */
@Entity
@Table(name="email_message_atach_fragments")
public class EmailMessageAtachFragment implements IEmailMessageAtachFragment, Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="id")
	private long id;
	
	@OneToOne
	@JoinColumn(name="email_message_atach_id")
	private EmailMessageAtach emailMessageAtach;
	
	@Column(name="identity")
	private int identity;
	
	@Lob
	@Column(name="content")
	private byte[] content;

	/**
	 * Номер фрагмента
	 */
	@Column(name="fragment_number")
	private int fragmentNumber;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getIdentity() {
		return identity;
	}

	public void setIdentity(int identity) {
		this.identity = identity;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public int getFragmentNumber() {
		return fragmentNumber;
	}

	public void setFragmentNumber(int fragmentNumber) {
		this.fragmentNumber = fragmentNumber;
	}

	public EmailMessageAtach getEmailMessageAtach() {
		return emailMessageAtach;
	}

	public void setEmailMessageAtach(EmailMessageAtach emailMessageAtach) {
		this.emailMessageAtach = emailMessageAtach;
	}

	@Override
	public int getMessageAtachIdentity() {
		if (emailMessageAtach == null) {
			throw new IllegalStateException("Entity (EmailMessageAtach) is detached?");
		}
		return emailMessageAtach.getIdentity();
	}

	@Override
	public void setMessageAtachIdentity(int atachIdentity) {
		if (emailMessageAtach == null) {
			throw new IllegalStateException("Entity (EmailMessageAtach) is detached?");
		}
		emailMessageAtach.setIdentity(atachIdentity);
	}

}
