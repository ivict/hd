/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

/**
 * Композитный ключ для вышек сотовых сетей. 
 * @author ivict
 */
public class CellTowerKey implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int mcc;
	private int mnc;
	private int lac;
	private int cid;
	
	public CellTowerKey() {	}//Default constructor for reflectors
	
	public CellTowerKey(int mcc, int mnc, int lac, int cid) {
		this();
		this.mcc = mcc;
		this.mnc = mnc;
		this.lac = lac;
		this.cid = cid;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mcc;
		result = prime * result + mnc;
		result = prime * result + lac;
		result = prime * result + cid;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CellTowerKey other = (CellTowerKey) obj;
		if (mcc != other.mcc) {
			return false;
		}
		if (mnc != other.mnc) {
			return false;
		}
		if (lac != other.lac) {
			return false;
		}
		if (cid != other.cid) {
			return false;
		}
		return true;
	}
	
}
