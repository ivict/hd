/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Память о текущих соединениях с мобильными агентами.
 * Обновляется редко порядка 1 раз в 15 мин. Необходима поскольку
 * при перезагрузке сервера, информация о текущих соединениях
 * может быть утеряна.
 * @author ivict
 *
 */
@Entity
@Table(name = "mobile_agent_connections")
public class MobileAgentConnection implements Serializable, ru.hipdriver.i.IMobileAgentConnection {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="mobile_agent_id")
	private long mobileAgentId;
	
	@Column(name="addr")
	private String addr;
	
	@Column(name="host")
	private String host;
	
	@Column(name="port")
	private int port;

	@Column(name="user")
	private String user;
	
	@Column(name="sign_in_time")
	private Date signInTime;

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	/**
     * @see ru.hipdriver.i.IMobileAgentConnection#getAddr()
     */
	@Override
	public String getAddr() {
		return addr;
	}

	/**
     * @see ru.hipdriver.i.IMobileAgentConnection#setAddr(String)
     */
	@Override
	public void setAddr(String addr) {
		this.addr = addr;
	}

	/**
     * @see ru.hipdriver.i.IMobileAgentConnection#getHost()
     */
	@Override
	public String getHost() {
		return host;
	}

	/**
     * @see ru.hipdriver.i.IMobileAgentConnection#setHost(String)
     */
	@Override
	public void setHost(String host) {
		this.host = host;
	}

	/**
     * @see ru.hipdriver.i.IMobileAgentConnection#getPort()
     */
	@Override
	public int getPort() {
		return port;
	}

	/**
     * @see ru.hipdriver.i.IMobileAgentConnection#setPort(short)
     */
	@Override
	public void setPort(int port) {
		this.port = port;
	}

	/**
     * @see ru.hipdriver.i.IMobileAgentConnection#getSignInTime()
     */
	@Override
	public Date getSignInTime() {
		return signInTime;
	}

	/**
     * @see ru.hipdriver.i.IMobileAgentConnection#setSignInTime(Date)
     */
	@Override
	public void setSignInTime(Date signInTime) {
		this.signInTime = signInTime;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
}
