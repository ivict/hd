/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * <b>Пользователь</b>,
 * любой человек который использует сервис тем или иным образом. 
 * @author ivict
 */
@Entity
@Table(name="users")
public class User implements Serializable, ru.hipdriver.i.IUser {
	private static final long serialVersionUID = 1L;

	/**
	 * Уникальность контролируется извне, drupal.
	 * Зарезервированный диапазон промо пользователей
	 * [2-30] без регистрации в drupal.
	 * TODO: После перехода на JDBC модуль авторизации для
	 * drupal, необходимо возобновить автогенерацию ID.
	 */
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	//GeneratedValue(strategy=GenerationType.AUTO, generator="user_seq_gen")
	//@SequenceGenerator(name="user_seq_gen", sequenceName="USER_SEQ")
	private int id;
	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinTable(name = "m2m_user_service_profile",
		joinColumns = { @JoinColumn(name = "user_id") },
		inverseJoinColumns = { @JoinColumn(name = "service_profile_id") })
	private Set<ServiceProfile> serviceProfiles;
	
	@OneToOne
	@JoinColumn(name="id")
	private UserLimit userLimit;
	
	/**
	 * Имя пользователя - <b>уникальный атрибут</b>.
	 */
	@Column(name="name")
	private String name;

	/**
	 * E-mail.
	 */
	@Column(name="email")
	private String email;

	/**
	 * Hash пароля.
	 */
	@Lob
	@Column(name="password_hash")
	private byte[] passwordHash;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public byte[] getPasswordHash() {
		return passwordHash;
	}

	@Override
	public void setPasswordHash(byte[] passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Set<ServiceProfile> getServiceProfiles() {
		return serviceProfiles;
	}

	public void setServiceProfiles(Set<ServiceProfile> serviceProfiles) {
		this.serviceProfiles = serviceProfiles;
	}

	synchronized public void addServiceProfile(ServiceProfile serviceProfile) {
		if ( serviceProfiles == null ) {
			serviceProfiles = new HashSet<ServiceProfile>();
		}
		serviceProfiles.add(serviceProfile);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserLimit getUserLimit() {
		return userLimit;
	}

	public void setUserLimit(UserLimit userLimit) {
		this.userLimit = userLimit;
	}
	
}
