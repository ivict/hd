/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Последний пинг мобильного агента.
 * @author ivict
 *
 */
@Entity
@Table(name="mobile_agent_pubkeys")
public class MobileAgentPubkey implements Serializable {

	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name = "mobile_agent_id")
	private long mobileAgentId;

	/**
	 * Открытая часть ключа для шифрования.<br>
	 * Обновляется мобильным агентом раз в сутки или чаще.<br>
	 */
	@Lob
	@Column(name = "pubkey")
	private byte[] pubkey;
	
	/**
	 * Дата и Время последнего обновления ключа.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private Date time;

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public byte[] getPubkey() {
		return pubkey;
	}

	public void setPubkey(byte[] pubkey) {
		this.pubkey = pubkey;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

}