/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <b>Тип контакта пользователя.</b><br>
 * Skype, e-mail, phone и etc.
 * @author ivict
 *
 */
@Entity
@Table(name="contact_types")
public class ContactType implements Serializable, ru.hipdriver.i.IContactType {
	private static final long serialVersionUID = 1L;	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@GeneratedValue(strategy=GenerationType.AUTO, generator="event_seq_gen")
	//@SequenceGenerator(name="event_seq_gen", sequenceName="EVENT_SEQ")
	private short id;
	
	/**
	 * Уникальное имя типа контакта.
	 */
	@Column (name = "name")
	private String name;

	/**
     * @see ru.hipdriver.i.IContactType#getId(int)
     */
	@Override
	public short getId() {
		return id;
	}

	/**
     * @see ru.hipdriver.i.IContactType#setId(short)
     */
	@Override
	public void setId(short id) {
		this.id = id;
	}

	/**
     * @see ru.hipdriver.i.IContactType#getName(int)
     */
	@Override
	public String getName() {
		return name;
	}

	/**
     * @see ru.hipdriver.i.IContactType#setName(String)
     */
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
}
