/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Дескрипторы потоков для соединения через stun сервер.
 * @author ivict
 *
 */
@Entity
@Table(name="mobile_agent_ice_agent_stream_descriptors")
public class MobileAgentIceAgentStreamDescriptor implements Serializable {

	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name = "mobile_agent_id")
	private long mobileAgentId;

	@Lob
	@Column(name = "local_stream_descriptor")
	private String localStreamDescriptor;
	
	@Lob
	@Column(name = "remote_stream_descriptor")
	private String remoteStreamDescriptor;
	
	/**
	 * Дата и Время последнего обновления дескриптора.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="local_stream_descriptor_update_time")
	private Date localStreamDescriptorUpdateTime;

	/**
	 * Дата и Время последнего обновления дескриптора.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="remote_stream_descriptor_update_time")
	private Date remoteStreamDescriptorUpdateTime;

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public String getLocalStreamDescriptor() {
		return localStreamDescriptor;
	}

	public void setLocalStreamDescriptor(String localStreamDescriptor) {
		this.localStreamDescriptor = localStreamDescriptor;
	}

	public String getRemoteStreamDescriptor() {
		return remoteStreamDescriptor;
	}

	public void setRemoteStreamDescriptor(String remoteStreamDescriptor) {
		this.remoteStreamDescriptor = remoteStreamDescriptor;
	}

	public Date getLocalStreamDescriptorUpdateTime() {
		return localStreamDescriptorUpdateTime;
	}

	public void setLocalStreamDescriptorUpdateTime(
			Date localStreamDescriptorUpdateTime) {
		this.localStreamDescriptorUpdateTime = localStreamDescriptorUpdateTime;
	}

	public Date getRemoteStreamDescriptorUpdateTime() {
		return remoteStreamDescriptorUpdateTime;
	}

	public void setRemoteStreamDescriptorUpdateTime(
			Date remoteStreamDescriptorUpdateTime) {
		this.remoteStreamDescriptorUpdateTime = remoteStreamDescriptorUpdateTime;
	}

}