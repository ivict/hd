/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;
/**
 * Пакет сущностей.
 * @author ivict
 * 
 * Маршруты @see #Route,
 * Точки @see #Point,
 * События @see #Event,
 * Пользователи @see #User,
 * Андроид клиенты, @see #MobileAgent
 * Память сессионных данных личного кабинета @see #SessionMemoryEntry,
 * РК @see #AdvertisingCompany,
 * Профиль пользователя @see #UserProfile,
 * Группа пользователей @see #UserGroup,
 * Профиль сервиса (совокупность характерных фич и инвариантов) @see #ServiceProfile,
 * Периодические инварианты (константы с историей) .
 */
