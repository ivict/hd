/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * <b>Ключ пользователя</b>,
 * Билинговый ключ.
 * @author ivict
 */
@Entity
@Table(name="user_keys")
public class UserKey implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="user_id")
	private int userId;
	
	/**
	 * Время когда ключ истекает.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="expired_time")
	private Date expiredTime;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getExpiredTime() {
		return expiredTime;
	}

	public void setExpiredTime(Date expiredTime) {
		this.expiredTime = expiredTime;
	}
	  
}
