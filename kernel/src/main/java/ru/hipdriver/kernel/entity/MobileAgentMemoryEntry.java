/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Память мобильного агента пользователя.<br>
 * Для уменьшения замусоривания базы используем "Архивный горизонт".
 * @see ru.hipdriver.Invariants#ARCHIVE_HORIZON_IN_MILLIS
 * Сущность, реализует концепт "Памяти приложения".
 * @author ivict
 */
@Entity
@Table(name="mobile_agent_memory_entries")
@IdClass(MobileAgentMemoryEntryKey.class)
public class MobileAgentMemoryEntry implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="mobile_agent_id")
	private long mobileAgentId;
	
	@Id
	@Column(name="key1")
	private String key;
	
	@Column(name="value")
	private String value;

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	   
}
