/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.hipdriver.i.IMobileAgentProfile;

/**
 * Mobile Agent Profile Entity.
 * Набор административных настроек для мобильного приложения.
 * @author ivict
 */
@Entity
@Table(name="mobile_agent_profiles")
public class MobileAgentProfile implements Serializable, ru.hipdriver.i.IMobileAgentProfile {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="mobile_agent_id")
	private long mobileAgentId;
	
	@OneToOne
	@JoinColumn(name="mobile_agent_id")
	private MobileAgent mobileAgent;
	
	@Column(name="delay_push_alert_event_in_seconds")
	private int delayPushAlertEventInSeconds = IMobileAgentProfile.DEFAULT_DELAY_PUSH_ALERT_EVENT_IN_SECONDS;
	
	@Column(name="delay_where_am_i_request_in_seconds")
	private int delayWhereAmIRequestInSeconds = IMobileAgentProfile.DEFAULT_DELAY_WHERE_AM_I_REQUEST_IN_SECONDS;
	
	@Column(name="enable_debug_mode")
	private boolean enableDebugMode = IMobileAgentProfile.DEFAULT_ENABLE_DEBUG_MODE;

	@Column(name="asd_max_jerk")
	private float asdMaxJerk = IMobileAgentProfile.DEFAULT_ASD_MAX_JERK;
	
	@Column(name="asd_max_acceleration")
	private float asdMaxAcceleration = IMobileAgentProfile.DEFAULT_ASD_MAX_ACCELERATION;
	
	@Column(name="asd_max_angle")
	private float asdMaxAngle = IMobileAgentProfile.DEFAULT_ASD_MAX_ANGLE;

	@Column(name="enable_energy_saving_mode")
	private boolean enableEnergySavingMode = IMobileAgentProfile.DEFAULT_ENABLE_ENERGY_SAVING_MODE;
	
	@Column(name="watchdog_service_start_delay_in_seconds")
	private int watchdogServiceStartDelayInSeconds = IMobileAgentProfile.DEFAULT_WATCHDOG_SERVICE_START_DELAY_IN_SECONDS;
	
	@Column(name="control_mode_id")
	private short controlModeId = IMobileAgentProfile.DEFAULT_CONTROL_MODE_ID;

	@Column(name="map_provider_id")
	private short mapProviderId = IMobileAgentProfile.DEFAULT_MAP_PROVIDER_ID;

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getMobileAgentId()
	 */
	@Override
	public long getMobileAgentId() {
		return mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setMobileAgentId(long)
	 */
	@Override
	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getDelayPushAlertEventInSeconds()
	 */
	@Override
	public int getDelayPushAlertEventInSeconds() {
		//Correction of logic
		if (delayPushAlertEventInSeconds <= 0) {
			return IMobileAgentProfile.DEFAULT_DELAY_PUSH_ALERT_EVENT_IN_SECONDS;
		}
		return delayPushAlertEventInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setDelayPushAlertEventInSeconds(int)
	 */
	@Override
	public void setDelayPushAlertEventInSeconds(int delayPushAlertEventInSeconds) {
		this.delayPushAlertEventInSeconds = delayPushAlertEventInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getDelayWhereAmIRequestInSeconds()
	 */
	@Override
	public int getDelayWhereAmIRequestInSeconds() {
		//Correction of logic
		if (delayWhereAmIRequestInSeconds <= 0) {
			return IMobileAgentProfile.DEFAULT_DELAY_WHERE_AM_I_REQUEST_IN_SECONDS;
		}
		return delayWhereAmIRequestInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setDelayWhereAmIRequestInSeconds(int)
	 */
	@Override
	public void setDelayWhereAmIRequestInSeconds(int delayWhereAmIRequestInSeconds) {
		this.delayWhereAmIRequestInSeconds = delayWhereAmIRequestInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#isEnableDebugMode()
	 */
	@Override
	public boolean isEnableDebugMode() {
		return enableDebugMode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setEnableDebugMode(boolean)
	 */
	@Override
	public void setEnableDebugMode(boolean enableDebugMode) {
		this.enableDebugMode = enableDebugMode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getAsdMaxJerk()
	 */
	@Override
	public float getAsdMaxJerk() {
		return asdMaxJerk;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setAsdMaxJerk(float)
	 */
	@Override
	public void setAsdMaxJerk(float asdMaxJerk) {
		this.asdMaxJerk = asdMaxJerk;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getAsdMaxAcceleration()
	 */
	@Override
	public float getAsdMaxAcceleration() {
		return asdMaxAcceleration;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setAsdMaxAcceleration(float)
	 */
	@Override
	public void setAsdMaxAcceleration(float asdMaxAcceleration) {
		this.asdMaxAcceleration = asdMaxAcceleration;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getAsdMaxAngle()
	 */
	@Override
	public float getAsdMaxAngle() {
		return asdMaxAngle;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setAsdMaxAngle(float)
	 */
	@Override
	public void setAsdMaxAngle(float asdMaxAngle) {
		this.asdMaxAngle = asdMaxAngle;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#isEnableEnergySavingMode()
	 */
	@Override
	public boolean isEnableEnergySavingMode() {
		return enableEnergySavingMode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setEnableEnergySavingMode(boolean)
	 */
	@Override
	public void setEnableEnergySavingMode(boolean enableEnergySavingMode) {
		this.enableEnergySavingMode = enableEnergySavingMode;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getWatchdogServiceStartDelayInSeconds()
	 */
	@Override
	public int getWatchdogServiceStartDelayInSeconds() {
		return watchdogServiceStartDelayInSeconds;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setWatchdogServiceStartDelayInSeconds(int)
	 */
	@Override
	public void setWatchdogServiceStartDelayInSeconds(
			int watchdogServiceStartDelayInSeconds) {
		this.watchdogServiceStartDelayInSeconds = watchdogServiceStartDelayInSeconds;
	}

	public MobileAgent getMobileAgent() {
		return mobileAgent;
	}

	public void setMobileAgent(MobileAgent mobileAgent) {
		this.mobileAgent = mobileAgent;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getControlModeId()
	 */
	public short getControlModeId() {
		return controlModeId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setControlModeId(short)
	 */
	public void setControlModeId(short controlModeId) {
		this.controlModeId = controlModeId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#getMapProviderId()
	 */
	public short getMapProviderId() {
		return controlModeId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentProfile#setMapProviderId(short)
	 */
	public void setMapProviderId(short mapProviderId) {
		this.mapProviderId = mapProviderId;
	}

}
