/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ILocation;

/**
 * Last location of mobile agent state.
 * 
 * @author ivict
 */
@Entity
@Table(name = "mobile_agent_last_locations")
public class MobileAgentLastLocation implements Serializable, ru.hipdriver.i.ILocation {
	private static final long serialVersionUID = 1L;

	public static final String MOBILE_AGENT_LAST_LOCATION_WITHOUT_MOBILE_AGENT = "Mobile agent last location without mobile agent";
	
	@Id
	@Column(name="mobile_agent_id")
	private long mobileAgentId;
	
	@OneToOne
	@JoinColumn(name="mobile_agent_id")
	private MobileAgent mobileAgent;

	/**
	 * Дата и Время события.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private Date time;
	
	/**
	 * Целочисленная широта, @see ru.hipdriver.Invariants#GEO_FACTOR.
	 * Если 0 то неизвестна.
	 */
	@Column(name="lat")
	private int lat;
	/**
	 * Целочисленная долгота, @see ru.hipdriver.Invariants#GEO_FACTOR.
	 * Если 0 то неизвестна.
	 */
	@Column(name="lon")
	private int lon;
	/**
	 * Высота над уровнем моря в метрах, если Integer.MIN_VALUE, то неизвестна.
	 */
	@Column(name="alt")
	private int alt = ILocation.UNKNOWN_ALTITUDE;
	/**
	 * Точность определения координат в метрах, если 0 то неизвестна.
	 */
	@Column(name="acc")
	private int acc;
	/**
	 * Мобильный код страны
	 */
	@Column(name="mcc")
	private int mcc;
	/**
	 * Код мобильной сети
	 */
	@Column(name="mnc")
	private int mnc;
	/**
	 * Local area code
	 */
	@Column(name="lac")
	private int lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
	/**
	 * Cell id
	 */
	@Column(name="cid")
	private int cid = ILocation.UNKNOWN_CELL_ID;
	
	public long getMobileAgentId() {
		return this.mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getLat()
     */
	@Override
	public int getLat() {
		return lat;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setLat()
     */
	@Override
	public void setLat(int lat) {
		this.lat = lat;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getLon()
     */
	@Override
	public int getLon() {
		return lon;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setLon(int)
     */
	@Override
	public void setLon(int lon) {
		this.lon = lon;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getAlt()
     */
	@Override
	public int getAlt() {
		return alt;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setAlt(int)
     */
	@Override
	public void setAlt(int alt) {
		this.alt = alt;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getAcc()
     */
	@Override
	public int getAcc() {
		return acc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setAcc(int)
     */
	@Override
	public void setAcc(int acc) {
		this.acc = acc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getMcc()
     */
	@Override
	public int getMcc() {
		return mcc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setMcc(int)
     */
	@Override
	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getMnc()
     */
	@Override
	public int getMnc() {
		return mnc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setMnc(int)
     */
	@Override
	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getLac()
     */
	@Override
	public int getLac() {
		return lac;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setLac(int)
     */
	@Override
	public void setLac(int lac) {
		this.lac = lac;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getCid()
     */
	@Override
	public int getCid() {
		return cid;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setCid(int)
     */
	@Override
	public void setCid(int cid) {
		this.cid = cid;
	}

	public MobileAgent getMobileAgent() {
		return mobileAgent;
	}

	public void setMobileAgent(MobileAgent mobileAgent) {
		this.mobileAgent = mobileAgent;
	}

	@Override
	public int getFactor() {
		return Locations.GEO_FACTOR;
	}

	@Override
	public void setFactor(int factor) {
		// TODO Auto-generated method stub
	}
}
