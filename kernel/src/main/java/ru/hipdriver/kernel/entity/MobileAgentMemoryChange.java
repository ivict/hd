/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Внешние изменения памяти мобильного агента пользователя.<br>
 * Для уменьшения замусоривания базы используем "Архивный горизонт".
 * Отправка изменения осуществляется только по соблюдению условия:
 * time >= sendingTime
 * @see ru.hipdriver.Invariants#ARCHIVE_HORIZON_IN_MILLIS
 * Сущность, реализует концепт "Памяти приложения".
 * @author ivict
 */
@Entity
@Table(name="mobile_agent_memory_changes")
@IdClass(MobileAgentMemoryEntryKey.class)
public class MobileAgentMemoryChange implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="mobile_agent_id")
	private long mobileAgentId;
	
	@Id
	@Column(name="key1")
	private String key;

	/**
	 * Дата и Время изменения внешним источником (не мобильным приложением).
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private Date time;
	
	/**
	 * Дата и Время отправки изменения.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="sending_time")
	private Date sendingTime;

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Date getSendingTime() {
		return sendingTime;
	}

	public void setSendingTime(Date sendingTime) {
		this.sendingTime = sendingTime;
	}

}
