/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Последний пинг мобильного агента.
 * @author ivict
 *
 */
@Entity
@Table(name="mobile_agent_pings")
public class MobileAgentPing implements Serializable {

	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name = "mobile_agent_id")
	private long mobileAgentId;
	
	/**
	 * Дата и Время последнего пинга.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private Date time;

	/**
	 * Признак on-line.<br>
	 * Изменяется сервисом DeadLinkTimer.
	 */
	@Column(name="online")
	private boolean online;

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}
	
}