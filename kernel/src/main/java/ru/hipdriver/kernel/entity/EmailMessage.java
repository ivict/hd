/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ru.hipdriver.i.IEmailMessage;

/**
 * Email message.
 * @author ivict
 */
@Entity
@Table(name="email_messages")
public class EmailMessage implements IEmailMessage, Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="id")
	private long id;
	
	@Column(name="mobile_agent_id")
	private long mobileAgentId;
	
	/**
	 * Если null то почта не отправлена.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="sending_time")
	private Date sendingTime;
	
	/**
	 * Если null то почта не принята полностью (отстутствует один или несколько фрагментов).
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="received_time")
	private Date receivedTime;
	
	@Column(name="email_from")
	private String emailFrom;

	@Column(name="email_to")
	private String emailTo;

	@Column(name="subject")
	private String subject;
	
	@Lob
	@Column(name="body")
	private String body;

	@Column(name="identity")
	private int identity;
	
	@Column(name="atachs_count")
	private int atachsCount;
	
	@Column(name="sending_fail")
	private boolean sendingFail;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public Date getSendingTime() {
		return sendingTime;
	}

	public void setSendingTime(Date sendingTime) {
		this.sendingTime = sendingTime;
	}

	public Date getReceivedTime() {
		return receivedTime;
	}

	/**
	 * Признак окончания сборки сообщения.
	 * Если receivedTime != null, то сообщение готово к отправке.
	 * @param receivedTime временная отметка последнего фрагмента сообщения, в том случае если сообщение содержит вложение.
	 * В случае сообщения без вложений, время приема сообщения.
	 */
	public void setReceivedTime(Date receivedTime) {
		this.receivedTime = receivedTime;
	}

	public String getFrom() {
		return emailFrom;
	}

	public void setFrom(String from) {
		this.emailFrom = from;
	}

	public String getTo() {
		return emailTo;
	}

	public void setTo(String to) {
		this.emailTo = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public int getIdentity() {
		return identity;
	}

	public void setIdentity(int identity) {
		this.identity = identity;
	}

	public int getAtachsCount() {
		return atachsCount;
	}

	public void setAtachsCount(int atachsCount) {
		this.atachsCount = atachsCount;
	}

	public boolean isSendingFail() {
		return sendingFail;
	}

	public void setSendingFail(boolean sendingFail) {
		this.sendingFail = sendingFail;
	}
	
}

