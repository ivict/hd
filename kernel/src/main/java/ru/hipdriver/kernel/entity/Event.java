/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ru.hipdriver.g.Locations;
import ru.hipdriver.i.IEventWithAtach;
import ru.hipdriver.i.ILocation;

/**
 * <b>Базовая сущность.</b><br>
 * События могут быть различных классов и содержать вложения.<br>
 * В общем случае, это сущность отвечающая на вопросы<br>
 * (WhoWhatWhereWhen):
 * <li>Кто</li>
 * <li>Что</li>
 * <li>Где</li>
 * <li>Когда</li>
 * @author ivict
 *
 */
@Entity
@Table(name="events")
public class Event implements Serializable, IEventWithAtach {

	public static final String EVENT_WITHOUT_MOBILE_AGENT = "Event without mobile agent";
	public static final String EVENT_WITHOUT_ATACH = "Event without atach";

	private static final long serialVersionUID = 1L;	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@GeneratedValue(strategy=GenerationType.AUTO, generator="event_seq_gen")
	//@SequenceGenerator(name="event_seq_gen", sequenceName="EVENT_SEQ")
	private long id;
	
	/**
	 * Пользователь, который взаимодействует с сервисом. 
	 */
	@Column(name="user_id")
	private int userId;
	/**
	 * Мобильный Андроид клиент. 
	 */
	@ManyToOne
	@JoinColumn(name="mobile_agent_id")
	private MobileAgent mobileAgent;
	
	/**
	 * Класс события. 
	 */
	@Column(name="event_class_id")
	private short eventClassId;
	
	/**
	 * Дата и Время события.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private Date time;
	
	/**
	 * Дата и Время приема информации о событии.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="received_time")
	private Date receivedTime;
	
	/**
	 * Целочисленная широта, @see ru.hipdriver.Invariants#GEO_FACTOR.
	 * Если 0 то неизвестна.
	 */
	@Column(name="lat")
	private int lat;
	/**
	 * Целочисленная долгота, @see ru.hipdriver.Invariants#GEO_FACTOR.
	 * Если 0 то неизвестна.
	 */
	@Column(name="lon")
	private int lon;
	/**
	 * Высота над уровнем моря в метрах, если Integer.MIN_VALUE, то неизвестна.
	 */
	@Column(name="alt")
	private int alt = ILocation.UNKNOWN_ALTITUDE;
	/**
	 * Точность определения координат в метрах, если 0 то неизвестна.
	 */
	@Column(name="acc")
	private int acc;
	/**
	 * Мобильный код страны
	 */
	@Column(name="mcc")
	private int mcc;
	/**
	 * Код мобильной сети
	 */
	@Column(name="mnc")
	private int mnc;
	/**
	 * Local area code
	 */
	@Column(name="lac")
	private int lac = ILocation.UNKNOWN_LOCATION_AREA_CODE;
	/**
	 * Cell id
	 */
	@Column(name="cid")
	private int cid = ILocation.UNKNOWN_CELL_ID;
	
	/**
	 * Атач к событию.
	 */
	@OneToOne(mappedBy="event")
	private EventAtach eventAtach;

	/**
     * @see ru.hipdriver.i.IEvent#getId()
     */
	@Override
	public long getId() {
		return id;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setId(long)
     */
	@Override
	public void setId(long id) {
		this.id = id;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getUserId()
     */
	@Override
	public int getUserId() {
		return userId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setUserId(int)
     */
	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getMobileAgent()
     */
	@Override
	public long getMobileAgentId() {
		if ( mobileAgent == null ) {
			return 0;
		}
		return mobileAgent.getId();
	}

	/**
     * @see ru.hipdriver.i.IEvent#setId(long)
     */
	@Override
	public void setMobileAgentId(long mobileAgentId) {
		if ( mobileAgent == null ) {
			throw new IllegalStateException(EVENT_WITHOUT_MOBILE_AGENT);
		}
		mobileAgent.setId(mobileAgentId);
	}

	/**
     * @see ru.hipdriver.i.IEvent#getEventClassId()
     */
	@Override
	public short getEventClassId() {
		return eventClassId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setEventClassId(short)
     */
	@Override
	public void setEventClassId(short eventClassId) {
		this.eventClassId = eventClassId;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getTime()
     */
	@Override
	public Date getTime() {
		return time;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setTime(Date)
     */
	@Override
	public void setTime(Date time) {
		this.time = time;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getLat()
     */
	@Override
	public int getLat() {
		return lat;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setLat()
     */
	@Override
	public void setLat(int lat) {
		this.lat = lat;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getLon()
     */
	@Override
	public int getLon() {
		return lon;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setLon(int)
     */
	@Override
	public void setLon(int lon) {
		this.lon = lon;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getAlt()
     */
	@Override
	public int getAlt() {
		return alt;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setAlt(int)
     */
	@Override
	public void setAlt(int alt) {
		this.alt = alt;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getAcc()
     */
	@Override
	public int getAcc() {
		return acc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setAcc(int)
     */
	@Override
	public void setAcc(int acc) {
		this.acc = acc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getMcc()
     */
	@Override
	public int getMcc() {
		return mcc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setMcc(int)
     */
	@Override
	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getMnc()
     */
	@Override
	public int getMnc() {
		return mnc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setMnc(int)
     */
	@Override
	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getLac()
     */
	@Override
	public int getLac() {
		return lac;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setLac(int)
     */
	@Override
	public void setLac(int lac) {
		this.lac = lac;
	}

	/**
     * @see ru.hipdriver.i.IEvent#getCid()
     */
	@Override
	public int getCid() {
		return cid;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setCid(int)
     */
	@Override
	public void setCid(int cid) {
		this.cid = cid;
	}

	/**
     * @see ru.hipdriver.i.IEventAtach#getEventId()
     */
	@Override
	public long getEventId() {
		return id;
	}

	/**
     * @see ru.hipdriver.i.IEventAtach#setEventId(long)
     */
	@Override
	public void setEventId(long eventId) {
		this.id = eventId;
	}

	/**
     * @see ru.hipdriver.i.IEventAtach#getDescription()
     */
	@Override
	public String getDescription() {
		if ( eventAtach == null ) {
			return null;
		}
		return eventAtach.getDescription();
	}

	/**
     * @see ru.hipdriver.i.IEventAtach#setDescription(String)
     */
	@Override
	public void setDescription(String description) {
		if ( eventAtach == null ) {
			throw new IllegalStateException(EVENT_WITHOUT_ATACH);
		}
		eventAtach.setDescription(description);
	}

	/**
     * @see ru.hipdriver.i.IEventAtach#getContent()
     */
	@Override
	public byte[] getContent() {
		if ( eventAtach == null ) {
			return null;
		}
		return eventAtach.getContent();
	}

	/**
     * @see ru.hipdriver.i.IEventAtach#setContent(byte[])
     */
	@Override
	public void setContent(byte[] content) {
		if ( eventAtach == null ) {
			throw new IllegalStateException(EVENT_WITHOUT_ATACH);
		}
		eventAtach.setContent(content);
	}

	/**
     * @see ru.hipdriver.i.IEventAtach#getContentCheckSum()
     */
	@Override
	public long getContentCheckSum() {
		if ( eventAtach == null ) {
			return 0;
		}
		return eventAtach.getContentCheckSum();
	}

	/**
     * @see ru.hipdriver.i.IEventAtach#setContentCheckSum(long)
     */
	@Override
	public void setContentCheckSum(long contentCheckSum) {
		if ( eventAtach == null ) {
			throw new IllegalStateException(EVENT_WITHOUT_ATACH);
		}
		eventAtach.setContentCheckSum(contentCheckSum);
	}

	public EventAtach getEventAtach() {
		return eventAtach;
	}

	public void setEventAtach(EventAtach eventAtach) {
		this.eventAtach = eventAtach;
	}

	public MobileAgent getMobileAgent() {
		return mobileAgent;
	}

	public void setMobileAgent(MobileAgent mobileAgent) {
		this.mobileAgent = mobileAgent;
	}

	/**
     * @see ru.hipdriver.i.ILocation#getFactor()
     */
	@Override
	public int getFactor() {
		return Locations.GEO_FACTOR;
	}

	/**
     * @see ru.hipdriver.i.ILocation#setFactor(int)
     */
	@Override
	public void setFactor(int factor) {
		throw new IllegalStateException("Read only field");
	}

	/**
     * @see ru.hipdriver.i.IEvent#getReceivedTime()
     */
	@Override
	public Date getReceivedTime() {
		return receivedTime;
	}

	/**
     * @see ru.hipdriver.i.IEvent#setReceivedTime(Date)
     */
	@Override
	public void setReceivedTime(Date receivedTime) {
		this.receivedTime = receivedTime;
	}
	
}
