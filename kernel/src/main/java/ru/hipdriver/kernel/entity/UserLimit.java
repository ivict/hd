/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.hipdriver.i.IUserLimit;

/**
 * <b>Ограничения пользователя</b>.
 * Ограничение по времени приложения. Максимальное количество установок и т.д.
 * @author ivict
 */
@Entity
@Table(name="user_limits")
public class UserLimit implements Serializable, IUserLimit {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_id")
	private int userId;

	@OneToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="time_limited")
	private boolean timeLimited = IUserLimit.DEFAULT_TIME_LIMITED;

	@Column(name="max_devices")
	private int maxDevices = IUserLimit.DEFAULT_MAX_DEVICES;
	
	@Column(name="used_as_referral")
	private boolean usedAsReferral;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#isTimeLimited()
	 */
	@Override
	public boolean isTimeLimited() {
		return timeLimited;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#setTimeLimited(boolean)
	 */
	@Override
	public void setTimeLimited(boolean timeLimited) {
		this.timeLimited = timeLimited;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#getMaxDevices()
	 */
	@Override
	public int getMaxDevices() {
		return maxDevices;
	}

	/**
	 * @see ru.hipdriver.i.IUserLimit#setMaxDevices(int)
	 */
	@Override
	public void setMaxDevices(int maxDevices) {
		this.maxDevices = maxDevices;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isUsedAsReferral() {
		return usedAsReferral;
	}

	public void setUsedAsReferral(boolean usedAsReferral) {
		this.usedAsReferral = usedAsReferral;
	}
	
}
