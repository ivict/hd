/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * <b>Тип команда пользователя</b>, команда, которую он отдает
 * мобильному агенту.
 * Важно, что количество аргументов команды не может быть разным у одинаковых
 * по имени команд, исходя из этого, это скорее не тип а вид команды.
 * Но в английском языке трактовка kind не столь однозначна как type.
 * @author ivict
 */
@Entity
@Table(name="user_command_types")
public class UserCommandType implements Serializable, ru.hipdriver.i.IUserCommandType {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//GeneratedValue(strategy=GenerationType.AUTO, generator="user_command_type_seq_gen")
	//@SequenceGenerator(name="user_command_type_seq_gen", sequenceName="USER_COMMAND_TYPE_SEQ")
	private short id;
	
	/**
	 * Имя типа команды пользователя - <b>уникальный атрибут</b>.
	 */
	@Column(name = "name")
	private String name;

	/**
	 * Описание типа команды пользователя.
	 */
	@Column(name = "description")
	private String description;

	@Override
	public short getId() {
		return id;
	}

	@Override
	public void setId(short id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
}
