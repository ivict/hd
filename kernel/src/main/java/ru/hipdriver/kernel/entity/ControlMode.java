/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * <b>Режим управления мобильным приложением (пульт->сторож)</b>.
 * @author ivict
 */
@Entity
@Table(name="control_modes")
public class ControlMode implements Serializable, ru.hipdriver.i.IUserCommandType {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//GeneratedValue(strategy=GenerationType.AUTO, generator="user_control_mode_seq_gen")
	//@SequenceGenerator(name="user_control_mode_seq_gen", sequenceName="USER_CONTROL_MODE_SEQ")
	private short id;
	
	/**
	 * Имя типа команды пользователя - <b>уникальный атрибут</b>.
	 */
	@Column(name = "name")
	private String name;

	/**
	 * Описание типа команды пользователя.
	 */
	@Column(name = "description")
	private String description;

	@Override
	public short getId() {
		return id;
	}

	@Override
	public void setId(short id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
}
