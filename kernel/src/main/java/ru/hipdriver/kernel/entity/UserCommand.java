/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * <b>Команда пользователя</b>, команда, которую он отдает
 * мобильному агенту.
 * Примеры команд: Включить("Сигнализацию"), 
 * Выключить ("Сигнализацию").
 * @author ivict
 */
@Entity
@Table(name="user_commands")
public class UserCommand implements Serializable, ru.hipdriver.i.IUserCommand {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//GeneratedValue(strategy=GenerationType.AUTO, generator="user_command_seq_gen")
	//@SequenceGenerator(name="user_command_seq_gen", sequenceName="USER_COMMAND_SEQ")
	private long id;
	
	@Column(name="user_id")
	private int userId;
	
	@Column(name="mobile_agent_id")
	private long mobileAgentId;

	@Column(name="user_command_type_id")
	private short userCommandTypeId;
	
	@Lob
	//byte array, java serialized array, contains
    //only simple types and strings
	private byte[] args;
	
	@Column(name="args_check_sum")
	private long argsCheckSum;
	
	/**
	 * Дата и Время отправки команды.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="send_time")
	private Date sendTime;

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int getUserId() {
		return userId;
	}

	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public long getMobileAgentId() {
		return mobileAgentId;
	}

	@Override
	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	@Override
	public short getUserCommandTypeId() {
		return userCommandTypeId;
	}

	@Override
	public void setUserCommandTypeId(short userCommandTypeId) {
		this.userCommandTypeId = userCommandTypeId;
	}

	@Override
	public byte[] getArgs() {
		return args;
	}

	@Override
	public void setArgs(byte[] args) {
		this.args = args;
	}

	@Override
	public long getArgsCheckSum() {
		return argsCheckSum;
	}

	@Override
	public void setArgsCheckSum(long argsCheckSum) {
		this.argsCheckSum = argsCheckSum;
	}

	@Override
	public Date getSendTime() {
		return sendTime;
	}

	@Override
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	
}
