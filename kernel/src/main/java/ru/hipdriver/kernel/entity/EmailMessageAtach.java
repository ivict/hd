/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.hipdriver.i.IEmailMessageAtach;
import ru.hipdriver.i.IEmailMessageAtachFragment;

/**
 * Email message.
 * @author ivict
 */
@Entity
@Table(name="email_message_atachs")
public class EmailMessageAtach implements IEmailMessageAtach, Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="id")
	private long id;
	
	@OneToOne
	@JoinColumn(name="email_message_id")
	private EmailMessage emailMessage;
	
	@Column(name="identity")
	private int identity;
	
	@Column(name="content_type")
	private String contentType;
	
	@Column(name="name")
	private String name;
	
	/**
	 * Номер атача
	 */
	@Column(name="atach_number")
	private int atachNumber;
	
	@Column(name="fragments_count")
	private int fragmentsCount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public EmailMessage getEmailMessage() {
		return emailMessage;
	}

	public void setEmailMessage(EmailMessage emailMessage) {
		this.emailMessage = emailMessage;
	}

	public int getIdentity() {
		return identity;
	}

	public void setIdentity(int identity) {
		this.identity = identity;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFragmentsCount() {
		return fragmentsCount;
	}

	public void setFragmentsCount(int fragmentsCount) {
		this.fragmentsCount = fragmentsCount;
	}

	@Override
	public int getMessageIdentity() {
		if (emailMessage == null) {
			throw new IllegalStateException("Entity (EmailMessage) is detached?");
		}
		return emailMessage.getIdentity();
	}

	@Override
	public void setMessageIdentity(int messageIdentity) {
		if (emailMessage == null) {
			throw new IllegalStateException("Entity (EmailMessage) is detached?");
		}
		emailMessage.setIdentity(messageIdentity);
	}

	@Override
	public int getAtachNumber() {
		return atachNumber;
	}

	@Override
	public void setAtachNumber(int atachNumber) {
		this.atachNumber = atachNumber;
	}

}
