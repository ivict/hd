/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * <b>Класс события</b>,
 * в качестве примера можно привести: Выключение сигнализации,
 * Включение сигнализации, Удар, Угон, Подъем, Потеря связи,
 * Возобновление связи, Прохождение геозоны и т.д. 
 * @author ivict
 */
@Entity
@Table(name="event_classes")
public class EventClass implements Serializable, ru.hipdriver.i.IEventClass {
	private static final long serialVersionUID = 1L;	
	 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@GeneratedValue(strategy=GenerationType.AUTO, generator="event_class_seq_gen")
	//@SequenceGenerator(name="event_class_seq_gen", sequenceName="EVENT_CLASS_SEQ")
	private short id;
	
	/**
	 * Наименование класса событий - <b>уникальный атрибут</b>.
	 */
	private String name;
	
	public EventClass() {
		super();
	} 
	   
	/**
     * @see ru.hipdriver.i.IEventClass#getName()
     */
	@Override
	public String getName() {
 		return this.name;
	}

	/**
     * @see ru.hipdriver.i.IEventClass#setName(String)
     */
	@Override
	public void setName(String name) {
		this.name = name;
	}
	   
	/**
     * @see ru.hipdriver.i.IEventClass#getId()
     */
	@Override
	public short getId() {
 		return this.id;
	}

	/**
     * @see ru.hipdriver.i.IEventClass#setId()
     */
	@Override
	public void setId(short id) {
		this.id = id;
	}
	
   
}
