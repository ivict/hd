/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.hipdriver.i.ILocation;

/**
 * Mobile Agent Entity.
 * 
 * @author ivict
 */
@Entity
@Table(name = "mobile_agents")
public class MobileAgent implements Serializable, ru.hipdriver.i.IMobileAgentWithState {
	private static final String MOBILE_AGENT_WITHOUT_USER = "Mobile agent without user";

	private static final String MOBILE_AGENT_WITHOUT_MOBILE_AGENT_STATE = "Mobile agent without mobile agent state";

	private static final String MOBILE_AGENT_WITHOUT_SIGNAL_STRENGTH = "Mobile agent without signal strength";

	private static final String MOBILE_AGENT_WITHOUT_MOBILE_AGENT_LAST_LOCATION = "Mobile agent without mobile agent last location";
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// @GeneratedValue(strategy=GenerationType.AUTO,
	// generator="mobile_agent_seq_gen")
	// @SequenceGenerator(name="mobile_agent_seq_gen",
	// sequenceName="MOBILE_AGENT_SEQ")
	private long id;

	@Column(name = "user_id")
	private int userId;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	/**
	 * Имя агента - <b>для одного пользователя уникальный атрибут</b>.
	 */
	@Deprecated //But used in some ejb-ql queries
	@Column(name = "name")
	private String name;

	/**
	 * IMEI мобильного устройства.
	 */
	@Column(name = "imei")
	private long imei;

	/**
	 * Текущее состояние устройства.
	 */
	@OneToOne(mappedBy = "mobileAgent")
	private MobileAgentState mobileAgentState;

	/**
	 * Текущее качество сигнала.
	 */
	@OneToOne(mappedBy = "mobileAgent")
	private SignalStrength signalStrength;

	/**
	 * Последнее зарегистрированное местоположение.
	 */
	@OneToOne(mappedBy = "mobileAgent")
	private MobileAgentLastLocation lastLocation;

	/**
	 * Профиль устройства.
	 */
	@OneToOne(mappedBy = "mobileAgent")
	private MobileAgentProfile mobileAgentProfile;
	
	/**
	 * Флаг удаления из пользовательского интерфейса.
	 */
	@Column(name = "disabled")
	private boolean disabled;
	
	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgent#getUserId()
	 */
	@Override
	public int getUserId() {
		return userId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgent#setUserId(int)
	 */
	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgent#getName()
	 */
	@Deprecated
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgent#setName(String)
	 */
	@Deprecated
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgent#getImei()
	 */
	@Override
	public long getImei() {
		return imei;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgent#setImei(long)
	 */
	@Override
	public void setImei(long imei) {
		this.imei = imei;
	}

	@Override
	public long getMobileAgentId() {
		return id;
	}

	@Override
	public void setMobileAgentId(long mobileAgentId) {
		this.id = mobileAgentId;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getGsmSignalStrength()
	 */
	@Override
	public int getGsmSignalStrength() {
		if (signalStrength == null) {
			return -1;
		}
		return signalStrength.getGsmSignalStrength();
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setGsmSignalStrength(int)
	 */
	@Override
	public void setGsmSignalStrength(int gsmSignalStrength) {
		if (signalStrength == null) {
			throw new IllegalStateException(
					MOBILE_AGENT_WITHOUT_SIGNAL_STRENGTH);
		}
		if (gsmSignalStrength <= 0) {
			throw new IllegalStateException("Reserved value 'no-connection'");
		}
		signalStrength.setGsmSignalStrength(gsmSignalStrength);
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getGsmBitErrorRate()
	 */
	@Override
	public int getGsmBitErrorRate() {
		if (signalStrength == null) {
			return -1;
		}
		return signalStrength.getGsmBitErrorRate();
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setGsmBitErrorRate(int)
	 */
	@Override
	public void setGsmBitErrorRate(int gsmBitErrorRate) {
		if (signalStrength == null) {
			throw new IllegalStateException(
					MOBILE_AGENT_WITHOUT_SIGNAL_STRENGTH);
		}
		signalStrength.setGsmBitErrorRate(gsmBitErrorRate);
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getBatteryStatus()
	 */
	@Override
	public int getBatteryStatus() {
		if (mobileAgentState == null) {
			return 0;
		}
		return mobileAgentState.getBatteryStatus();
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setBatteryStatus(int)
	 */
	@Override
	public void setBatteryStatus(int batteryStatus) {
		if (mobileAgentState == null) {
			throw new IllegalStateException(
					MOBILE_AGENT_WITHOUT_MOBILE_AGENT_STATE);
		}
		mobileAgentState.setBatteryStatus(batteryStatus);
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getBatteryPct()
	 */
	@Override
	public float getBatteryPct() {
		if (mobileAgentState == null) {
			return -1.0f;
		}
		return mobileAgentState.getBatteryPct();
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setBatteryPct(float)
	 */
	@Override
	public void setBatteryPct(float batteryPct) {
		if (mobileAgentState == null) {
			throw new IllegalStateException(
					MOBILE_AGENT_WITHOUT_MOBILE_AGENT_STATE);
		}
		mobileAgentState.setBatteryPct(batteryPct);

	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getAppStateId()
	 */
	@Override
	public short getAppStateId() {
		if (signalStrength == null) {
			return 0;
		}
		return mobileAgentState.getAppStateId();
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setAppStateId(short)
	 */
	@Override
	public void setAppStateId(short appStateId) {
		if (mobileAgentState == null) {
			throw new IllegalStateException(
					MOBILE_AGENT_WITHOUT_MOBILE_AGENT_STATE);
		}
		mobileAgentState.setAppStateId(appStateId);
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#getCarStateId()
	 */
	@Override
	public short getCarStateId() {
		if (mobileAgentState == null) {
			return 0;
		}
		return mobileAgentState.getCarStateId();
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentState#setCarStateId(short)
	 */
	@Override
	public void setCarStateId(short carStateId) {
		if (mobileAgentState == null) {
			throw new IllegalStateException(
					MOBILE_AGENT_WITHOUT_MOBILE_AGENT_STATE);
		}
		mobileAgentState.setCarStateId(carStateId);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public MobileAgentState getMobileAgentState() {
		return mobileAgentState;
	}

	public void setMobileAgentState(MobileAgentState mobileAgentState) {
		this.mobileAgentState = mobileAgentState;
	}

	public SignalStrength getSignalStrength() {
		return signalStrength;
	}

	public void setSignalStrength(SignalStrength signalStrength) {
		this.signalStrength = signalStrength;
	}

	public ILocation getMobileAgentLastLocation() {
		return lastLocation;
	}

	public void setLastLocation(MobileAgentLastLocation mobileAgentLastLocation) {
		this.lastLocation = mobileAgentLastLocation;
	}
	
	
	/**
     * @see ru.hipdriver.i.IMobileAgentState#getLastLocation()
     */
	@Override
	public MobileAgentLastLocation getLastLocation() {
		return lastLocation;
	}

	/**
     * @see ru.hipdriver.i.IMobileAgentState#setLastLocation(ILocation)
     */
	@Override
	public void setLastLocation(ILocation lastLocation) {
		if ( lastLocation == null ) {
			throw new IllegalStateException(
					MOBILE_AGENT_WITHOUT_MOBILE_AGENT_LAST_LOCATION);
		}
		this.lastLocation.setLat(lastLocation.getLat());
		this.lastLocation.setLon(lastLocation.getLon());
		this.lastLocation.setAlt(lastLocation.getAlt());
		this.lastLocation.setAcc(lastLocation.getAcc());
		this.lastLocation.setMcc(lastLocation.getMcc());
		this.lastLocation.setMnc(lastLocation.getMnc());
		this.lastLocation.setLac(lastLocation.getLac());
		this.lastLocation.setCid(lastLocation.getCid());
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentVersion#getVersionCode()
	 */
	@Override
	public int getVersionCode() {
		if (mobileAgentState == null) {
			return 0;
		}
		return mobileAgentState.getVersionCode();
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentVersion#setVersionCode(int)
	 */
	@Override
	public void setVersionCode(int versionCode) {
		if (mobileAgentState == null) {
			throw new IllegalStateException(
					MOBILE_AGENT_WITHOUT_MOBILE_AGENT_STATE);
		}
		mobileAgentState.setVersionCode(versionCode);
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentVersion#getVersionName()
	 */
	@Override
	public String getVersionName() {
		if (mobileAgentState == null) {
			return "";
		}
		return mobileAgentState.getVersionName();
	}

	/**
	 * @see ru.hipdriver.i.IMobileAgentVersion#setVersionName(String)
	 */
	@Override
	public void setVersionName(String versionName) {
		if (mobileAgentState == null) {
			throw new IllegalStateException(
					MOBILE_AGENT_WITHOUT_MOBILE_AGENT_STATE);
		}
		mobileAgentState.setVersionName(versionName);
	}

	public MobileAgentProfile getMobileAgentProfile() {
		return mobileAgentProfile;
	}

	public void setMobileAgentProfile(MobileAgentProfile mobileAgentProfile) {
		this.mobileAgentProfile = mobileAgentProfile;
	}
	
	
}
