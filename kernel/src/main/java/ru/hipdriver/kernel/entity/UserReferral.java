/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <b>Ссылка на рекомендателя</b>. Т.е на пользователя по ссылке которого зарегистрирован данный пользователь.
 * @author ivict
 */
@Entity
@Table(name="user_referrals")
public class UserReferral implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_id")
	private int userId;
	
	@Column(name="referral_user_id")
	private int referralUserId;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getReferralUserId() {
		return referralUserId;
	}

	public void setReferralUserId(int referralUserId) {
		this.referralUserId = referralUserId;
	}

	
	
}
