/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Cell tower update request Entity.
 * 
 * @author ivict
 */
@Entity
@Table(name = "cell_towers")
@IdClass(CellTowerKey.class)
public class CellTower implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="mcc")
	private int mcc;
	
	@Id
	@Column(name="mnc")
	private int mnc;
	
	@Id
	@Column(name="lac")
	private int lac;
	
	@Id
	@Column(name="cid")
	private int cid;
	
	@Column(name="lat")
	private int lat;
	
	@Column(name="lon")
	private int lon;
	
	@Column(name="samples")
	private int samples;
	
	@Column(name="changeable")
	private boolean changeable;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created")
	private Date created;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated")
	private Date updated;
	
	@Column(name="averageSignalStrength")
	private int averageSignalStrength;

	public int getMcc() {
		return mcc;
	}

	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	public int getMnc() {
		return mnc;
	}

	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	public int getLac() {
		return lac;
	}

	public void setLac(int lac) {
		this.lac = lac;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public int getLat() {
		return lat;
	}

	public void setLat(int lat) {
		this.lat = lat;
	}

	public int getLon() {
		return lon;
	}

	public void setLon(int lon) {
		this.lon = lon;
	}

	public int getSamples() {
		return samples;
	}

	public void setSamples(int samples) {
		this.samples = samples;
	}

	public boolean isChangeable() {
		return changeable;
	}

	public void setChangeable(boolean changeable) {
		this.changeable = changeable;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public int getAverageSignalStrength() {
		return averageSignalStrength;
	}

	public void setAverageSignalStrength(int averageSignalStrength) {
		this.averageSignalStrength = averageSignalStrength;
	}
	
}
