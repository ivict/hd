/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Рекламная кампания.
 * @author ivict
 */
public class AdvertisingCompany implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	//@GeneratedValue(strategy=GenerationType.AUTO, generator="advertising_company_seq_gen")
	//@SequenceGenerator(name="advertising_company_seq_gen", sequenceName="ADVERTISING_COMPANY_SEQ")
	private long id;
	
	@Column
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
