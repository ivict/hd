/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Внешние изменения памяти пользователя.<br>
 * Для уменьшения замусоривания базы используем "Архивный горизонт".
 * Отправка изменения осуществляется только по соблюдению условия:
 * time >= sendingTime
 * @see ru.hipdriver.Invariants#ARCHIVE_HORIZON_IN_MILLIS
 * Cущность, реализует концепт "памяти приложения".
 * @author ivict
 */
@Entity
@Table(name="user_memory_changes")
@IdClass(UserMemoryEntryKey.class)
public class UserMemoryChange implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="user_id")
	private int userId;
	
	@Id
	@Column(name="key1")
	private String key;
	
	/**
	 * Дата и Время изменения внешним источником (не страничкой на Drupal).
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private Date time;
	
	/**
	 * Дата и Время отправки изменения.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="sending_time")
	private Date sendingTime;

	public long getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Date getSendingTime() {
		return sendingTime;
	}

	public void setSendingTime(Date sendingTime) {
		this.sendingTime = sendingTime;
	}
	
}
