/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * <b>Группа пользователей</b>,
 * объединение пользователей со сходными особенностями.
 * @author ivict
 */
@Entity
@Table(name="user_groups")
public class UserGroup implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@GeneratedValue(strategy=GenerationType.AUTO, generator="user_group_seq_gen")
	//@SequenceGenerator(name="user_group_seq_gen", sequenceName="USER_GROUP_SEQ")
	private short id;
	
	/**
	 * Имя группы пользователей - <b>уникальный атрибут</b>.
	 */
	private String name;
	
	public UserGroup() {
		super();
	} 
	   
	public String getName() {
 		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
   
}
