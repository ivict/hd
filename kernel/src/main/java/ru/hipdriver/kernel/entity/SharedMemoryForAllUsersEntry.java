/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Общая память.<br>
 * TODO: refactoring separate by service - profiles
 * @author ivict
 */
@Entity
@Table(name="shared_memory_for_all_users_entries")
public class SharedMemoryForAllUsersEntry implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="key1")
	private String key;
	
	@Column(name="value")
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	   
}
