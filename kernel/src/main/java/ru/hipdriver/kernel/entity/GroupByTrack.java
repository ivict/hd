/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Group by track Entity.
 * Параметр группировки.
 * @author ivict
 */
@Entity
@Table(name="group_by_tracks")
public class GroupByTrack implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@GeneratedValue(strategy=GenerationType.AUTO, generator="group_by_track_seq_gen")
	//@SequenceGenerator(name="group_by_track_seq_gen", sequenceName="GROUP_BY_TRACK_SEQ")
	@Column(name="id")
	private long id;
	
	@Column(name="user_id")
	private int userId;

	@Column(name="mobile_agent_id")
	private long mobileAgentId;

	@Column(name="event_id")
	private long eventId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	
}
