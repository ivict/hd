/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Mobile Agent Alert Track Entity.
 * @author ivict
 */
@Entity
@Table(name="mobile_agent_alert_tracks")
@IdClass(MobileAgentAlertTrackKey.class)
public class MobileAgentAlertTrack implements Serializable, ru.hipdriver.i.ITrack {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="mobile_agent_id")
	private long mobileAgentId;

	/**
	 * Дата и Время начала кусочка трека (совпадает с первым элементом time-line).
	 */
	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private Date time;
	
	/**
	 * Дата и время обработки на сервере.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="received_time")
	private Date receivedTime;
	
	/**
	 * Общая продолжительность трека в миллисекундах.
	 * Если одна точка то 0.
	 */
	@Column(name="duration_in_millis")
	private long durationInMillis;
	
	/**
	 * Ломаная линия. Набор пар координат для отрисовки.
	 * Пример: {{52.1f, 50.1f}, {51.8f, 50.4f}}
	 */
	@Lob
	@Column(name="polyline")
	private float[][] polyline;

	/**
	 * Набор времен для ломанной линии.
	 */
	@Lob
	@Column(name="timestamps")
	private long[] timestamps;
	
	/**
	 * Состояние машины.
	 */
	@Id
	@Column(name="car_state_id")
	private short carStateId;

	/**
	 * Идентификатор трека, присваивается однократно после нового состояния тревоги.
	 */
	@Id
	@Column(name="group_by_track_id")
	private long groupByTrackId;

	/**
	 * Флаг фальшивого трека,
	 * необходим для превращения истории треков в историю алертов.
	 */
	@Column(name="fake_track")
	private boolean fakeTrack;
	
	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public void setMobileAgentId(long mobileAgentId) {
		this.mobileAgentId = mobileAgentId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public long getDurationInMillis() {
		return durationInMillis;
	}

	public void setDurationInMillis(long durationInMillis) {
		this.durationInMillis = durationInMillis;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#getPolyline()
	 */
	@Override
	public float[][] getPolyline() {
		return polyline;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#setPolyline(float[][])
	 */
	@Override
	public void setPolyline(float[][] polyline) {
		this.polyline = polyline;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#getTimestamps()
	 */
	@Override
	public long[] getTimestamps() {
		return timestamps;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#setTimestamps(long[])
	 */
	@Override
	public void setTimestamps(long[] timestamps) {
		this.timestamps = timestamps;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#getCarStateId()
	 */
	@Override
	public short getCarStateId() {
		return carStateId;
	}

	/**
	 * @see ru.hipdriver.i.ITrack#setCarStateId(short)
	 */
	@Override
	public void setCarStateId(short carStateId) {
		this.carStateId = carStateId;
	}

	public Date getReceivedTime() {
		return receivedTime;
	}

	public void setReceivedTime(Date receivedTime) {
		this.receivedTime = receivedTime;
	}

	public long getGroupByTrackId() {
		return groupByTrackId;
	}

	public void setGroupByTrackId(long groupByTrackId) {
		this.groupByTrackId = groupByTrackId;
	}

	public boolean isFakeTrack() {
		return fakeTrack;
	}

	public void setFakeTrack(boolean fakeTrack) {
		this.fakeTrack = fakeTrack;
	}

	@Override
	public long getTrackId() {
		return groupByTrackId;
	}

	@Override
	public void setTrackId(long trackId) {
		groupByTrackId = trackId;
	}

}
