/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User Profile Entity.
 * Набор административных настроек для пользователя.
 * @author ivict
 */
@Entity
@Table(name="user_profiles")
public class UserProfile implements Serializable {
	private static final long serialVersionUID = 1L;	

	@Id
	@Column(name="user_id")
	private int userId;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
