package ru.hipdriver.kernel;

import ru.hipdriver.j.ErrorMessage;

/**
 * Утилитарный класс описывающий все основные ошибки.
 * @author ivict
 *
 */
public enum Errors {
	UNDEFINED_ERROR(0x000000001L, "Undefined error"),
	INVALID_IMEI(0x100000001L, "Invalid imei"),
	INVALID_USER_IDENTITY(0x100000002L, "Invalid user identity"),
	INVALID_MOBILE_AGENT_ID(0x100000003L, "Invalid mobile agent id"),
	INVALID_USER_ID(0x100000004L, "Invalid user id"),
	
	TOO_BIG_DRUPAL_ID(0x200000001L, "Very big drupal id"),
	INVALID_DRUPAL_USER(0x200000002L, "Invalid drupal user identity or e-mail"),
	NEGATIVE_DRUPAL_ID(0x200000003L, "Negative drupal id")
	;
	
	public final long ID;
	public final String DESCRIPTION;
	
	private Errors(long errCode, String description) {
		this.ID = errCode;
		this.DESCRIPTION = description;
	}

	public static ErrorMessage mm(Errors err) {
		ErrorMessage errMsg = new ErrorMessage();
		errMsg.setId(err.ID);
		errMsg.setDescription(err.DESCRIPTION);
		return errMsg;
	}
	
	public static ErrorMessage undetermined(String errMessage) {
		ErrorMessage errMsg = new ErrorMessage();
		errMsg.setId(-1);
		errMsg.setDescription(errMessage);
		return errMsg;
	}
	
}
