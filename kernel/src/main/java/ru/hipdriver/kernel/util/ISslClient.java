package ru.hipdriver.kernel.util;

import java.io.IOException;
import java.net.BindException;

public interface ISslClient {
	
	boolean connect() throws BindException, IllegalArgumentException, IOException;
	
	void release();
	
}
