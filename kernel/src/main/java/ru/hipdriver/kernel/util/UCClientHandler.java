/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.util;

import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

public class UCClientHandler extends SimpleChannelHandler {
	
	private final ObjectMapper om;
	private final Class<?> resultClass;
	private Object result;
	private Object notifier;
	
    public UCClientHandler(Class<?> resultClass) {
		this.om = new ObjectMapper();
		this.resultClass = resultClass;
	}
    
	public UCClientHandler() {
		this(null);
	}
	
	@Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
        ChannelBuffer buf = (ChannelBuffer) e.getMessage();
        try {
        	result = om.readValue(buf.array(), resultClass);
        } catch (Throwable th) {
        	throw new RuntimeException(th);
        } finally {
        	releaseLock();
        	e.getChannel().close();
        }
    }

	@Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        e.getCause().printStackTrace();
        e.getChannel().close();
    }

	public Object getResult() {
		return result;
	}

	public void setNotifier(Object notifier) {
		this.notifier = notifier;
	}
	
	private void releaseLock() {
		if (notifier != null) {
			synchronized(notifier) {
				notifier.notify();
			}
		}
	}
    
}