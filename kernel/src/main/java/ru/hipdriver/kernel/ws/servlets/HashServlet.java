/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws.servlets;

import static ru.hipdriver.kernel.Invariants.HASH;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

import javax.inject.Inject;
import javax.servlet.AsyncContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.Base64Variants;
import org.slf4j.Logger;

import ru.hipdriver.kernel.Invariants;
import ru.hipdriver.kernel.ejb3.UserSessionManager;

/**
 * Сервис проверки регистрации в системе аккаунта с e-mail.
 * 
 * @author ivict
 */
@WebServlet(name = "HashServlet", urlPatterns = {HASH}, loadOnStartup=1, asyncSupported = true)
public class HashServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = null;//= LoggerFactory.getLogger(PingServlet.class);
	
    private ThreadPoolExecutor executor;
    
    @Inject
    private UserSessionManager userSessionManager;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        executor = new ThreadPoolExecutor(10, 100, 3000L,
    			TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(100));
    }	

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//req.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);		
		AsyncContext aCtx = req.startAsync(req, resp);
        executor.execute(new Hash(aCtx, userSessionManager, log));		
	}
	
	@Override
	public void destroy() {
		executor.shutdown();
		super.destroy();
	}
	
}

class Hash implements Runnable {
	
	private final AsyncContext aCtx;
	private final UserSessionManager userSessionManager;
	private final Logger log;

	public Hash(AsyncContext aCtx, UserSessionManager userSessionManager, Logger log) {
		this.aCtx = aCtx;
		this.userSessionManager = userSessionManager;
		this.log = log;
	}

	@Override
	public void run() {
		ServletRequest req = aCtx.getRequest();
		HttpServletResponse resp = (HttpServletResponse) aCtx.getResponse();
		try {
			String firstLine = req.getReader().readLine();
			if (firstLine == null) {
				resp.setStatus(400);
				return;
			}
			if (firstLine.isEmpty()) {
				resp.setStatus(400);
				return;
			}
			Matcher matcher = Invariants.EMAIL_ADDRESS.matcher(firstLine);
			if (!matcher.matches()) {
				resp.setStatus(400);
				return;
			}
			boolean emailExists = userSessionManager.checkEmail(firstLine);
			if (!emailExists) {
				resp.setStatus(400);
				return;
			}
			byte[] hash = userSessionManager.getEncodedPasswordHash(firstLine);
			PrintWriter writer = resp.getWriter();
			String hashAsString = Base64Variants.MIME_NO_LINEFEEDS.encode(hash);			
			writer.append(hashAsString);
			writer.flush();
			//Not cached response
			resp.setHeader("Cache-Control", "private");
			resp.setStatus(200);
		} catch (Throwable th) {
			log.error("Get mobile agent state fail by server reason.", th);
			resp.setStatus(500);
		} finally {
			aCtx.complete();
		}
	}
	
}
