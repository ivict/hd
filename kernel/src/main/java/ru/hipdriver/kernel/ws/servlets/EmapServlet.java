/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws.servlets;

import static ru.hipdriver.kernel.Invariants.EMAP;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.servlet.AsyncContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;

import ru.hipdriver.i.ISid;
import ru.hipdriver.kernel.ejb3.UserSessionManager;
import ru.hipdriver.util.EncDecTools;

/**
 * Сервис редактирования свойств мобильного агента.
 * 
 * @author ivict
 */
@WebServlet(name = "EmapServlet", urlPatterns = {EMAP}, loadOnStartup=1, asyncSupported = true)
public class EmapServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = null;//= LoggerFactory.getLogger(PingServlet.class);
	
    private ThreadPoolExecutor executor;
    private ObjectMapper om;
    
    @Inject
    private UserSessionManager userSessionManager;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        executor = new ThreadPoolExecutor(10, 100, 50000L,
    			TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(100));
        om = new ObjectMapper();
    }	

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//req.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);		
		AsyncContext aCtx = req.startAsync(req, resp);
        executor.execute(new Emap(aCtx, userSessionManager, om, log));		
	}
	
	@Override
	public void destroy() {
		executor.shutdown();
		super.destroy();
	}
	
}

class Emap implements Runnable {
	
	private final AsyncContext aCtx;
	private final UserSessionManager userSessionManager;
	private final ObjectMapper om;
	private final Logger log;

	public Emap(AsyncContext aCtx, UserSessionManager userSessionManager, ObjectMapper om,  Logger log) {
		this.aCtx = aCtx;
		this.userSessionManager = userSessionManager;
		this.log = log;
		this.om = om;
	}

	@Override
	public void run() {
		ServletRequest req = aCtx.getRequest();
		HttpServletResponse resp = (HttpServletResponse) aCtx.getResponse();
		try {
			String firstLine = req.getReader().readLine();
			if (firstLine == null) {
				resp.setStatus(400);
				return;
			}
			if (firstLine.isEmpty()) {
				resp.setStatus(400);
				return;
			}
			//TODO: MORE STRONG AUTHENTIFICATION
			long mobileAgentId = EncDecTools.decodeMobileAgentId(firstLine, ISid.SALTMA);
			if (mobileAgentId <= 0) {
				resp.setStatus(400);
				return;
			}
			Properties properties = new Properties();
			properties.load(req.getReader());
			//Second line and next lines - properties
			boolean success = userSessionManager.changeMobileAgentSettings(mobileAgentId, properties);
			String value = om.writeValueAsString(success);
			PrintWriter writer = resp.getWriter();
			writer.print(value);
			writer.flush();
			//Not cached response
			resp.setHeader("Cache-Control", "private");
			resp.setStatus(200);
		} catch (Throwable th) {
			log.error("Get mobile agent state fail by server reason.", th);
			resp.setStatus(500);
		} finally {
			aCtx.complete();
		}
	}
	
}
