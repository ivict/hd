/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import static ru.hipdriver.kernel.Invariants.SIGN;
import static ru.hipdriver.kernel.Invariants.SIGN_UP_PATH;
import static ru.hipdriver.kernel.Invariants.SIGN_UPW_PATH;
import static ru.hipdriver.kernel.Invariants.SIGN_IN_PATH;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ru.hipdriver.j.SKey;
import ru.hipdriver.j.SignInMaRequest;
import ru.hipdriver.j.SignInMaResponse;
import ru.hipdriver.j.SignUpMa;
import ru.hipdriver.j.SignUpPo;
import ru.hipdriver.j.StandardResponse;

/**
 * Сервис аутентификации.
 * 
 * @author ivict
 */
@Path(SIGN)
public interface SignService {

	/**
	 * Запрос на регистрацию нового мобильного агента.
	 * Если агент был удален из пользовательского интерфейса,
	 * то вызов этого сервиса восстановит его.
	 * @param signUpMa запрос на регистрацию
	 * @return Публичная часть серверного ключа и ответ на первый sign-in.
	 */
	@POST
	@Path(SIGN_UP_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SKey up(SignUpMa signUpMa);

	@POST
	@Path(SIGN_UPW_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StandardResponse upw(SignUpPo signUpPo);

	/**
	 * Вход в систему для мобильного агента.<br>
	 * Во время входа он посылает все изменения, которые делал пользователь в настройках.<br>
	 * В ответ получает изменения настроек, которые делал пользователь в личном кабинете.<br>
	 * Конфликт изменений разрешается на стороне мобильного агента после получения ответа.
	 * @param signInMa
	 * @return
	 */
	@POST
	@Path(SIGN_IN_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SignInMaResponse in(SignInMaRequest signInMa);

}
