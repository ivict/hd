/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.QueryParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.IAppState;
import ru.hipdriver.i.ICarState;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentWithState;
import ru.hipdriver.i.IPlacemark;
import ru.hipdriver.i.IServiceProfile;
import ru.hipdriver.i.ITrack;
import ru.hipdriver.i.IUser;
import ru.hipdriver.i.IUserLimit;
import ru.hipdriver.j.AlertTrack;
import ru.hipdriver.j.AppState;
import ru.hipdriver.j.CarState;
import ru.hipdriver.j.Location;
import ru.hipdriver.j.MobileAgentWithState;
import ru.hipdriver.j.MobileAgentWithStateFactory;
import ru.hipdriver.j.ServiceProfile;
import ru.hipdriver.j.User;
import ru.hipdriver.j.UserLimit;
import ru.hipdriver.kernel.Errors;
import ru.hipdriver.kernel.Values;
import ru.hipdriver.kernel.ejb3.Emulator;
import ru.hipdriver.kernel.ejb3.GeoMapper;
import ru.hipdriver.kernel.ejb3.UserSessionManager;

/**
 * Service /kernel/services/users 
 * @author ivict
 */
@Stateless
@LocalBean
public class UsersServiceBean implements UsersService {
	final Logger log = LoggerFactory.getLogger(UsersServiceBean.class);
	
	@EJB
	private UserSessionManager userSessionManager;

	@EJB
	private Emulator emulator;
	
	@EJB
	private GeoMapper geoMapper;

	public IUser whoAreYou(String userId) {
		if (userId == null) {//Not specified parameter return empty list
			return null;
		}
		if (Values.isNotDecimalIntegerNumber(userId)) {
			return null;
		}
		int userIdValue = Integer.valueOf(userId);
		IUser user = new User();
		user.setId(userIdValue);
		return user;
	}
	
	/**
     * @see ru.hipdriver.kernel.ws.UsersService#count()
     */
	@Override
	public int count(String serviceProfileName) {
		return userSessionManager.usersCount(serviceProfileName);
	}

	/**
     * @see ru.hipdriver.kernel.ws.UsersService#serviceProfiles(String)
     */
	@Override
	public List<ServiceProfile> serviceProfiles(String userId) {
		if (userId == null) {//Not specified parameter return all services
			List<IServiceProfile> serviceProfiles = userSessionManager.getServiceProfiles();
			return getServiceProfiles(serviceProfiles);
		}
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return Collections.emptyList();			
		}
		List<IServiceProfile> serviceProfiles = userSessionManager.getServiceProfiles(user.getId());
		return getServiceProfiles(serviceProfiles);
	}

	private List<ServiceProfile> getServiceProfiles(
			List<IServiceProfile> serviceProfiles) {
		List<ServiceProfile> serviceProfileList = new ArrayList<ServiceProfile>(serviceProfiles.size());
		for ( IServiceProfile e : serviceProfiles ) {
			ServiceProfile p = new ServiceProfile();
			p.setId(e.getId());
			p.setName(e.getName());
			p.setDescription(e.getDescription());
			serviceProfileList.add(p);
		}
		return serviceProfileList;
	}

	/**
     * @see ru.hipdriver.kernel.ws.UsersService#mobileAgents(String, String, String)
     */
	@Override
	public List<MobileAgentWithState> mobileAgents(String userId, String mobileAgentId, String mobileAgentName) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return Collections.emptyList();			
		}
		
		final List<IMobileAgentWithState> mobileAgentsWithStates;
		if ( mobileAgentId == null && mobileAgentName == null ) {
			mobileAgentsWithStates = userSessionManager.getMobileAgentsWithStates(user);
		} else if ( mobileAgentId == null ) {
			mobileAgentsWithStates = userSessionManager.getMobileAgentsWithStates(user, mobileAgentName);
		} else {
			if ( !Values.isValidLong(mobileAgentId) ) {
				return Collections.emptyList();
			}
			long mobileAgentIdValue = Long.valueOf(mobileAgentId);
			IMobileAgentWithState mobileAgentWithState = userSessionManager.getMobileAgentWithState(mobileAgentIdValue);
			if ( mobileAgentWithState == null ) {
				return Collections.emptyList();
			}
			mobileAgentsWithStates = Collections.singletonList(mobileAgentWithState);
		}
		
		List<MobileAgentWithState> mobileAgentList = new ArrayList<MobileAgentWithState>(mobileAgentsWithStates.size());
		for ( IMobileAgentWithState e : mobileAgentsWithStates ) {
			Map<String, Object> properties = userSessionManager.getMobileAgentProperties(e.getUserId(), e.getId());
			ILocation lastLocation = e.getLastLocation();
			IPlacemark placemark = null;
			if (lastLocation != null) {
				placemark = geoMapper.parse(lastLocation);
			}
			MobileAgentWithState m = MobileAgentWithStateFactory.newMobileAgent(e, properties, placemark);
			mobileAgentList.add(m);
		}
		return mobileAgentList;
	}

	/**
     * @see ru.hipdriver.kernel.ws.UsersService#cool()
     */
	@Override
	public List<User> cool() {
		List<IUser> coolUsers = userSessionManager.getUsers(2, 30);
		if ( coolUsers.isEmpty() ) {
			return Collections.emptyList();
		}
		List<User> coolUserList = new ArrayList<User>(coolUsers.size());
		for ( IUser e : coolUsers ) {
			User u = new User();
			u.setId(e.getId());
			u.setName(e.getName());
			coolUserList.add(u);
		}
		return coolUserList;
	}

	/**
     * @see ru.hipdriver.kernel.ws.UsersService#appStates()
     */
	@Override
	public List<AppState> appStates() {
		List<IAppState> appSattes = userSessionManager.getAppStates();
		return getAppStates(appSattes);
	}
	
	private List<AppState> getAppStates(
			List<IAppState> appStates) {
		List<AppState> appStateList = new ArrayList<AppState>(appStates.size());
		for ( IAppState e : appStates ) {
			AppState p = new AppState();
			p.setId(e.getId());
			p.setName(e.getName());
			p.setDescription(e.getDescription());
			appStateList.add(p);
		}
		return appStateList;
	}


	/**
     * @see ru.hipdriver.kernel.ws.UsersService#carStates()
     */
	@Override
	public List<CarState> carStates() {
		List<ICarState> carStates = userSessionManager.getCarStates();
		return getCarStates(carStates);
	}
	
	private List<CarState> getCarStates(
			List<ICarState> carStates) {
		List<CarState> carStateList = new ArrayList<CarState>(carStates.size());
		for ( ICarState e : carStates ) {
			CarState p = new CarState();
			p.setId(e.getId());
			p.setName(e.getName());
			p.setDescription(e.getDescription());
			carStateList.add(p);
		}
		return carStateList;
	}

	/**
     * @see ru.hipdriver.kernel.ws.UsersService#whereAreYou(String, String)
     */
	@Override
	public Location whereAreYou(String userId, String mobileAgentId) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return null;			
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return null;
		}
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		Location response = new Location();
		try {
			//ILocation location = userSessionManager.requestMobileAgentLocation(user.getId(), mobileAgentIdValue);
			ILocation location = emulator.requestMobileAgentLocation(user.getId(), mobileAgentIdValue);
			response.setFactor(location.getFactor());
			response.setLat(location.getLat());
			response.setLon(location.getLon());
			response.setAlt(location.getAlt());
			response.setAcc(location.getAcc());
			response.setMcc(location.getMcc());
			response.setMnc(location.getMnc());
			response.setLac(location.getLac());
			response.setCid(location.getCid());
		} catch (Throwable th) {
			log.error("Error", th);
			response.setErrorMessage(Errors.undetermined(th.getLocalizedMessage()));
		}
		return response;
	}

	/**
     * @see ru.hipdriver.kernel.ws.UsersService#activateCarAlarm(String, String)
     */
	@Override
	public boolean activateCarAlarm(String userId, String mobileAgentId) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return false;			
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return false;
		}
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		try {
			//return userSessionManager.activateCarAlarm(user.getId(), mobileAgentIdValue);
			return emulator.activateCarAlarm(user.getId(), mobileAgentIdValue);
		} catch (Throwable th) {
			return false;
		}
	}

	/**
     * @see ru.hipdriver.kernel.ws.UsersService#activateCarAlarm(String, String)
     */
	@Override
	public boolean switchOffCarAlarm(String userId, String mobileAgentId) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return false;			
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return false;
		}
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		try {
			//return userSessionManager.switchOffCarAlarm(user.getId(), mobileAgentIdValue);
			return emulator.switchOffCarAlarm(user.getId(), mobileAgentIdValue);
		} catch (Throwable th) {
			return false;
		}
	}

	/**
	 * @see ru.hipdriver.kernel.ws.UsersService#alertTracks(String, String)
	 */
	@Override
	public List<AlertTrack> alertTracks(String userId, String mobileAgentId) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return Collections.emptyList();			
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return Collections.emptyList();
		}
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		List<AlertTrack> result = new ArrayList<AlertTrack>();
		try {
			//From now and 3 month back
			Date date = new Date();
			long timeBackInMillis = 3 * 30 * 24 * 3600 * 1000L;
			//Max count of tracks - 5
			int maxCount = 10;
			List<ITrack> tracks = userSessionManager.getAlertTracks(user.getId(), mobileAgentIdValue, date, timeBackInMillis, maxCount);
			for (ITrack track : tracks) {
				float[][] polyline = track.getPolyline();
				if (polyline.length == 0) {
					continue;
				}
				//long[] timestamps = track.getTimestamps();
				AlertTrack alertTrack = new AlertTrack();
				alertTrack.setPolyline(polyline);
				//First point time
				alertTrack.setAlertTime(track.getReceivedTime());
				alertTrack.setCarStateId(track.getCarStateId());
				alertTrack.setTrackId(track.getTrackId());
				result.add(alertTrack);
			}
			return result;
		} catch (Throwable th) {
			//alertTrack.setErrorMessage(Errors.undetermined(th.getLocalizedMessage()));
			return Collections.emptyList();
		}
	}

	/**
	 * @see ru.hipdriver.kernel.ws.UsersService#mobileAgentsCount()
	 */
	@Override
	public int mobileAgentsCount() {
		return userSessionManager.getActiveMobileAgentsCount();
	}

	/**
	 * @see ru.hipdriver.kernel.ws.UsersService#mobileAgentsEditor(String, String, String, String, String)
	 */
	@Override
	public boolean mobileAgentsEditor(String userId,
			String mobileAgentId,
			String name,
			String alertPhoneNumber,
			String alertEmail) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return false;			
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return false;
		}
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		try {
			return userSessionManager.updateMobileAgent(user.getId(), mobileAgentIdValue, name, alertPhoneNumber, alertEmail);
		} catch (Throwable th) {
			return false;
		}
	}

	/**
	 * @see ru.hipdriver.kernel.ws.UsersService#mobileAgentsProperties(String, String)
	 */
	@Override
	public List<Map<String, Object>> mobileAgentsProperties(String userId, String mobileAgentId) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return Collections.emptyList();			
		}
		if (mobileAgentId == null) {
			try {
				List<Map<String, Object>> mobileAgentsProperties = userSessionManager.getMobileAgentsProperties(user.getId());
				return mobileAgentsProperties;
			} catch (Throwable th) {
				return Collections.emptyList();
			}
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return Collections.emptyList();
		}
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		try {
			Map<String, Object> mobileAgentProperties = userSessionManager.getMobileAgentProperties(user.getId(), mobileAgentIdValue);
			return Collections.singletonList(mobileAgentProperties);
		} catch (Throwable th) {
			return Collections.emptyList();
		}
	}

	/**
	 * @see ru.hipdriver.kernel.ws.UsersService#timeLimitedVersion(List)
	 */
	@Override
	public Map<Integer, Boolean> timeLimitedVersion(List<Long> uids) {
		if ( uids == null ) {
			return Collections.emptyMap();
		}
		if ( uids.isEmpty() ) {
			return Collections.emptyMap();
		}
		List<Integer> userIds = new ArrayList<Integer>();
		for ( Long uid : uids ) {
			if (uid == null) {
				continue;
			}
			userIds.add(uid.intValue());
		}
		if ( userIds.isEmpty() ) {
			return Collections.emptyMap();
		}
		Map<Integer, Boolean> timeLimitedFlags = userSessionManager.getTimeLimitedVersion(userIds);
		if (timeLimitedFlags.isEmpty()) {
			return Collections.emptyMap();
		}
		//Map<Long, Boolean> requestResult = new HashMap<Long, Boolean>(timeLimitedFlags.size());
		//for (Entry<Integer, Boolean> e : timeLimitedFlags.entrySet()) {
		//	requestResult.put(e.getKey().longValue(), e.getValue());
		//}
		return timeLimitedFlags;
	}

	/**
	 * @see ru.hipdriver.kernel.ws.UsersService#timeLimitedVersion(List)
	 */
	@Override
	public Map<Integer, Boolean> usedAsReferral(List<Long> uids) {
		if ( uids == null ) {
			return Collections.emptyMap();
		}
		if ( uids.isEmpty() ) {
			return Collections.emptyMap();
		}
		List<Integer> userIds = new ArrayList<Integer>();
		for ( Long uid : uids ) {
			if (uid == null) {
				continue;
			}
			userIds.add(uid.intValue());
		}
		if ( userIds.isEmpty() ) {
			return Collections.emptyMap();
		}
		Map<Integer, Boolean> usedAsReferralFlags = userSessionManager.getUsedAsReferral(userIds);
		if (usedAsReferralFlags.isEmpty()) {
			return Collections.emptyMap();
		}
		return usedAsReferralFlags;
	}

	/**
	 * @see ru.hipdriver.kernel.ws.UsersService#userLimits(String)
	 */
	@Override
	public UserLimit userLimits(@QueryParam("user-id") String userId) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return null;			
		}
		try {
			IUserLimit ul = userSessionManager.getUserLimit(user.getId());
			if (ul == null) {
				return null;
			}
			UserLimit userLimit = new UserLimit();
			userLimit.setTimeLimited(ul.isTimeLimited());
			userLimit.setMaxDevices(ul.getMaxDevices());
			return userLimit;
		} catch (Throwable th) {
			return null;
		}
	}

}
