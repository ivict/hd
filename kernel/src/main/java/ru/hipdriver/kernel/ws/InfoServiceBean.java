/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Service /kernel/services/events 
 * @author ivict
 */
@Stateless
@LocalBean
public class InfoServiceBean implements InfoService {
	
	@Override
	public int count() {
		return 1;
	}

}
