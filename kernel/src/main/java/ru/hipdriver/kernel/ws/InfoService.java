/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import static ru.hipdriver.kernel.Invariants.COUNT_PATH;
import static ru.hipdriver.kernel.Invariants.ROOT;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Утилитарная информация.
 * 
 * @author ivict
 */
@Path(ROOT)
public interface InfoService {

	/**
	 * Общее количество работающих сервисов.
	 * path: /kernel/services/count
	 */
	@GET
	@Path(COUNT_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public int count();

}
