/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
/**
 * All servlets for application kernel.
 * @author ivict
 *
 */
package ru.hipdriver.kernel.ws.servlets;