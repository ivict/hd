/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import static ru.hipdriver.kernel.Invariants.USERS;
import static ru.hipdriver.kernel.Invariants.USERS_ACTIVATE_CAR_ALARM_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_APP_STATES_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_CAR_STATES_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_COOL_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_COUNT_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_MOBILE_AGENTS_STATES_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_WHERE_ARE_YOU_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_SERVICE_PROFILES_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_SWITCH_OFF_CAR_ALARM_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_ALERT_TRACKS_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_MOBILE_AGENTS_COUNT_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_MOBILE_AGENTS_EDITOR_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_MOBILE_AGENTS_PROPERTIES_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_TIME_LIMITED_VERSION_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_USED_AS_REFERRAL_PATH;
import static ru.hipdriver.kernel.Invariants.USERS_LIMITS;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ru.hipdriver.j.AlertTrack;
import ru.hipdriver.j.AppState;
import ru.hipdriver.j.CarState;
import ru.hipdriver.j.Location;
import ru.hipdriver.j.MobileAgentWithState;
import ru.hipdriver.j.ServiceProfile;
import ru.hipdriver.j.User;
import ru.hipdriver.j.UserLimit;

import ru.hipdriver.i.IUser;
import ru.hipdriver.i.IMobileAgent;

/**
 * Сервис профилей пользователей.
 * 
 * @author ivict
 */
@Path(USERS)
public interface UsersService {

	/**
	 * Общее число пользователей для профиля сервиса (сервиса конкретного вида).
	 * Если параметр null (не указан в запросе), возвращаем общее число
	 * пользователей.
	 * 
	 * @return целое положительное число.
	 */
	@GET
	@Path(USERS_COUNT_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public int count(
			@QueryParam("service-profile-name") String serviceProfileName);

	/**
	 * Список всех доступных сервисов.
	 * 
	 * @return Список профилей сервисов.
	 */
	@GET
	@Path(USERS_SERVICE_PROFILES_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public List<ServiceProfile> serviceProfiles(@QueryParam(IUser.USER_ID_KEY) String userId);

	/**
	 * Список мобильных агентов с состоянием.
	 * Если указан mobileAgentId то ориентируемся по нему и возвращаем единственного агента.
	 * Если нет, то ориентируемся по имени (допустимо использование маски *).
	 * Если не указаны ни mobileAgentId ни mobileAgentName, ориентируемся по пользователю и
	 * возвращаем список всех агентов пользователя. 
	 * 
	 * 
	 * @return Список мобильных агентов.
	 */
	@GET
	@Path(USERS_MOBILE_AGENTS_STATES_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public List<MobileAgentWithState> mobileAgents(@QueryParam(IUser.USER_ID_KEY) String userId,
			@QueryParam(IMobileAgent.MOBILE_AGENT_ID_KEY) String mobileAgentId,
			@QueryParam(IMobileAgent.MOBILE_AGENT_NAME_KEY) String mobileAgentName);

	/**
	 * Список кошерных пользователей.
	 * 
	 * @return Список пользователей без регистрации в drupal через ЛК.
	 */
	@GET
	@Path(USERS_COOL_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> cool();
	
	/**
	 * Список состояний мобильного приложения.
	 * 
	 * @return Список состояний мобильного приложения.
	 */
	@GET
	@Path(USERS_APP_STATES_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public List<AppState> appStates();

	/**
	 * Список состояний машины.
	 * 
	 * @return Список состояний машины.
	 */
	@GET
	@Path(USERS_CAR_STATES_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public List<CarState> carStates();

	/**
	 * Запрос местоположения машины.<br/>
	 * Режим выполнения: синхронный.
	 * 
	 * @return Координаты локации.
	 */
	@GET
	@Path(USERS_WHERE_ARE_YOU_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public Location whereAreYou(@QueryParam(IUser.USER_ID_KEY) String userId,
			@QueryParam("mobile-agent-id") String mobileAgentId);

	/**
	 * Команда пользователя: <b>"включить приложение 'сигнализация'"</b><br/>
	 * Режим выполнения: асинхронный.
	 * 
	 * @return всегда true если пользователь известен.
	 */
	@GET
	@Path(USERS_ACTIVATE_CAR_ALARM_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean activateCarAlarm(@QueryParam(IUser.USER_ID_KEY) String userId,
			@QueryParam("mobile-agent-id") String mobileAgentId);

	/**
	 * Команда пользователя: <b>"выключить приложение 'сигнализация'"</b><br/> 
	 * Режим выполнения: асинхронный.
	 * 
	 * @return всегда true если пользователь известен.
	 */
	@GET
	@Path(USERS_SWITCH_OFF_CAR_ALARM_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean switchOffCarAlarm(@QueryParam(IUser.USER_ID_KEY) String userId,
			@QueryParam("mobile-agent-id") String mobileAgentId);

	/**
	 * Последние треки мобильного агента в состоянии тревоги. 
	 * 
	 * @return null если не было ни одного тревожного состояния.
	 */
	@GET
	@Path(USERS_ALERT_TRACKS_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public List<AlertTrack> alertTracks(@QueryParam(IUser.USER_ID_KEY) String userId,
			@QueryParam("mobile-agent-id") String mobileAgentId);
	
	/**
	 * Общее количество подключенных устройств.
	 * @return more then one.
	 */
	@GET
	@Path(USERS_MOBILE_AGENTS_COUNT_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public int mobileAgentsCount();
	
	/**
	 * Редактирование имени устройства и номера телефона. 
	 * @param userId идентификатор пользователя.
	 * @param mobileAgentId идентификатор мобильного агента.
	 * @param name новое имя мобильного агента (не пустое)
	 * @param alertPhoneNumber мобильный телефон для тревожного звонка.
	 * @param alertEmail e-mail по которому посылается информация о тревоге.
	 * @return true if success.
	 */
	@GET
	@Path(USERS_MOBILE_AGENTS_EDITOR_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean mobileAgentsEditor(@QueryParam(IUser.USER_ID_KEY) String userId,
			@QueryParam(IMobileAgent.MOBILE_AGENT_ID_KEY) String mobileAgentId,
			@QueryParam("name") String name,
			@QueryParam(IMobileAgent.ALERT_PHONE_NUMBER_KEY) String alertPhoneNumber,
			@QueryParam(IMobileAgent.ALERT_EMAIL_KEY) String alertEmail);
	
	/**
	 * Список пользовательских свойств мобильного устройства.<br/>
	 * Пример: Имя, номер телефона дозвона и т.д.<br/>
	 * Если mobile-agent-id не указан, то возвращается список настроек
	 * всех видимых мобильных агентов пользователя. 
	 * @param userId user identity
	 * @param mobileAgentId mobile agent identity, optional
	 * @return map of properties
	 */
	@GET
	@Path(USERS_MOBILE_AGENTS_PROPERTIES_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Map<String, Object>> mobileAgentsProperties(@QueryParam(IUser.USER_ID_KEY) String userId,
			@QueryParam(IMobileAgent.MOBILE_AGENT_ID_KEY) String mobileAgentId);

	/**
	 * Список флажков наличия "платных" версий.<br/>
	 * @param uids список drupal user identities
	 * @return map of flags.
	 */
	@POST
	@Path(USERS_TIME_LIMITED_VERSION_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<Integer, Boolean> timeLimitedVersion(List<Long> uids);
	
	/**
	 * Список признаков использования пользователей как рефералов.<br/>
	 * @param uids список drupal user identities
	 * @return map of flags.
	 */
	@POST
	@Path(USERS_USED_AS_REFERRAL_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<Integer, Boolean> usedAsReferral(List<Long> uids);
	
	/**
	 * Возвращает ограничения пользователя.
	 * @param userId user identity
	 * @return null если лимиты не установлены.
	 */
	@GET
	@Path(USERS_LIMITS)
	@Produces(MediaType.APPLICATION_JSON)
	public UserLimit userLimits(@QueryParam(IUser.USER_ID_KEY) String userId);
	
}
