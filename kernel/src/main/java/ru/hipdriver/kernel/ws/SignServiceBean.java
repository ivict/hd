/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import ru.hipdriver.i.ISKey;
import ru.hipdriver.i.ISignInMaResponse;
import ru.hipdriver.i.IUser;
import ru.hipdriver.j.SKey;
import ru.hipdriver.j.SignInMaRequest;
import ru.hipdriver.j.SignInMaResponse;
import ru.hipdriver.j.SignUpMa;
import ru.hipdriver.j.SignUpPo;
import ru.hipdriver.j.StandardResponse;
import ru.hipdriver.j.User;
import ru.hipdriver.kernel.Errors;
import ru.hipdriver.kernel.ejb3.MACManager;
import ru.hipdriver.kernel.ejb3.UserSessionManager;


/**
 * Service /kernel/services/sign 
 * @author ivict
 */
@Stateless
@LocalBean
public class SignServiceBean implements SignService {

	@EJB
	private UserSessionManager userSessionManager;
	
	@Context
	private HttpServletRequest servletRequest;

	@EJB
	private MACManager macManager;
	
	/**
	 * @see SignService#up(SignUpMa)
	 */
	@Override
	public SKey up(SignUpMa suma) {
		Date receivedEventTime = new Date(System.currentTimeMillis());
		//Пока заглушка, необходим скорее всего какой нить сервис генерации ключей и т.д.
		SKey key = new SKey();
		long imei = suma.getImei();
		if ( !checkImei(imei) ) {
			key.setErrorMessage(Errors.mm(Errors.INVALID_IMEI));
			return key;
		}
		String userName = suma.getName();
		if ( !checkUser(userName, suma.getPasswordHash() ) ) {
			key.setErrorMessage(Errors.mm(Errors.INVALID_USER_IDENTITY));
			return key;
		}
		int userId = 0;//Invalid user id
		byte[] passwordHash = suma.getPasswordHash();
		try {
			//Check user and pass-phrase
			//TODO: move hash generation onto mobile-agent side
			//Password hash always not null @see #checkUser
			userId = userSessionManager.authentificationByNameAndPassword(userName, passwordHash);
		} catch (Throwable th) {
			//key.setErrorMessage(Errors.undetermined(th.getLocalizedMessage()));
			return null;
		}
		IUser user = new User();
		user.setName(userName);
		user.setId(userId);
		String mobileAgentName = suma.getMobileAgentName();
		ISKey newKey = userSessionManager.signUp(user, imei, mobileAgentName, suma, suma, suma, suma, receivedEventTime);
		if ( newKey == null ) {
			key.setErrorMessage(Errors.mm(Errors.UNDEFINED_ERROR));
			return key;
		}

		int mobileAgentCount = newKey.getMobileAgentCount();
		long mobileAgentId = newKey.getMobileAgentId();
		updateConnectionInfoFromContext(mobileAgentId);
		
		key.setValue(newKey.getValue());
		key.setUserId(newKey.getUserId());
		key.setMobileAgentId(mobileAgentId);
		key.setMobileAgentCount(mobileAgentCount);
		ISignInMaResponse currentSignInMaResponse = newKey.getCurrentSignInMaResponse();
		SignInMaResponse signInMaResponse = new SignInMaResponse();
		key.setCurrentSignInMaResponse(signInMaResponse);
		key.setCurrentSignInMaResponse(currentSignInMaResponse);
		return key;
	}

	private void updateConnectionInfoFromContext(long mobileAgentId) {
		String remoteAddr = getClientIpAddr(servletRequest);
		String remoteHost = servletRequest.getRemoteHost();
		int remotePort = servletRequest.getRemotePort();
		String remoteUser = servletRequest.getRemoteUser();
		if (remoteAddr == null) {
			return;
		}
		macManager.updateConnectionInfo(mobileAgentId, remoteAddr, remoteHost, remotePort, remoteUser, new Date());
	}
	
	//@see http://stackoverflow.com/questions/4678797/how-do-i-get-the-remote-address-of-a-client-in-servlet
	public static String getClientIpAddr(HttpServletRequest request) {  
        String ip = request.getHeader("X-Forwarded-For");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        }  
        return ip;  
    }  	

	private boolean checkUser(String name, byte[] passwordHash) {
		//TODO: make real check
		return name != null && !name.isEmpty() && passwordHash != null;
	}

	private boolean checkImei(long imei) {
		//TODO: make real check
		return imei != 0;
	}
	
	/**
	 * @see SignService#upw(SignUpPo)
	 */
	@Override
	public StandardResponse upw(SignUpPo supo) {
		long userId = supo.getId();//Id in drupal
		StandardResponse response = new StandardResponse(){};
		if ( !checkDrupalIdForNegative(userId) ) {
			response.setErrorMessage(Errors.mm(Errors.NEGATIVE_DRUPAL_ID));
			return response;
		}
		if ( !checkDrupalIdForBig(userId) ) {
			response.setErrorMessage(Errors.mm(Errors.TOO_BIG_DRUPAL_ID));
			return response;
		}
		String userName = supo.getName();
		byte[] passwordHash = supo.getPasswordHash();
		if ( !checkUser(userName, passwordHash ) ) {
			response.setErrorMessage(Errors.mm(Errors.INVALID_DRUPAL_USER));
			return response;
		}
		String email = supo.getEmail();
		//Регистрация нового пользователя, после пилота необходимо убрать
		IUser user = new User();
		user.setName(userName);
		user.setId((int) userId);
		user.setPasswordHash(passwordHash);
		try {
			int referralId = (int) supo.getReferralId();
			if (!userSessionManager.refreshRegisteredUser((int) userId, userName, email, passwordHash, referralId)) {
				userSessionManager.registerUser(user, email, referralId);
			}
		} catch (Throwable th) {
			response.setErrorMessage(Errors.undetermined(th.getLocalizedMessage()));
		}
		return response;
	}

	private boolean checkDrupalIdForNegative(long id) {
		return id > 0;
	}

	private boolean checkDrupalIdForBig(long id) {
		int intId = (int) id;
		return id == intId;
	}

	/**
	 * @see SignService#in(SignInMaRequest)
	 */
	@Override
	public SignInMaResponse in(SignInMaRequest sima) {
		int userId = sima.getUserId();
		long mobileAgentId = sima.getMobileAgentId();
		if ( !checkMobileAgentId(mobileAgentId) ) {
			return null;
		}
		if ( !checkUserId(userId) ) {
			return null;
		}
		
		ISignInMaResponse signInMaResponse = userSessionManager.signIn(userId, mobileAgentId, sima.getProperties());
		if ( signInMaResponse == null ) {
			return null;
		}
		updateConnectionInfoFromContext(mobileAgentId);
		
		SignInMaResponse currentSignInMaResponse = new SignInMaResponse();
		
		currentSignInMaResponse.setA(signInMaResponse.getA());
		currentSignInMaResponse.setMobileAgentId(signInMaResponse.getMobileAgentId());
		currentSignInMaResponse.setDelayPushAlertEventInSeconds(signInMaResponse.getDelayPushAlertEventInSeconds());
		currentSignInMaResponse.setDelayWhereAmIRequestInSeconds(signInMaResponse.getDelayWhereAmIRequestInSeconds());
		currentSignInMaResponse.setEnableDebugMode(signInMaResponse.isEnableDebugMode());
		currentSignInMaResponse.setAsdMaxJerk(signInMaResponse.getAsdMaxJerk());
		currentSignInMaResponse.setAsdMaxAcceleration(signInMaResponse.getAsdMaxAcceleration());
		currentSignInMaResponse.setAsdMaxAngle(signInMaResponse.getAsdMaxAngle());
		currentSignInMaResponse.setProperties(signInMaResponse.getProperties());
		currentSignInMaResponse.setTimeLimited(signInMaResponse.isTimeLimited());
		currentSignInMaResponse.setMaxDevices(signInMaResponse.getMaxDevices());
		
		return currentSignInMaResponse;
	}

	private boolean checkUserId(int userId) {
		//TODO: make real check
		return true;
	}

	private boolean checkMobileAgentId(long mobileAgentId) {
		//TODO: make real check
		return true;
	}
	
	
}
