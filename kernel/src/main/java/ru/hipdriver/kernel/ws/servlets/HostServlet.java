/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws.servlets;

import static ru.hipdriver.kernel.Invariants.HOST;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.servlet.AsyncContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;

import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentConnection;
import ru.hipdriver.i.ISid;
import ru.hipdriver.kernel.ejb3.MACManager;
import ru.hipdriver.util.EncDecTools;

/**
 * Сервис обмена дескрипторами потоков ice агентов мобильных приложений.
 * 
 * @author ivict
 */
@WebServlet(name = "HostServlet", urlPatterns = {HOST}, loadOnStartup=1, asyncSupported = true)
public class HostServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = null;//= LoggerFactory.getLogger(PingServlet.class);
	
    private ThreadPoolExecutor executor;
    
    @Inject
    private MACManager macManager;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        executor = new ThreadPoolExecutor(10, 10, 50000L,
    			TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(1));        
    }	

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//req.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);		
		AsyncContext aCtx = req.startAsync(req, resp);
        executor.execute(new Host(aCtx, macManager, log));		
	}
	
	@Override
	public void destroy() {
		executor.shutdown();
		super.destroy();
	}
	
}

class Host implements Runnable {
	
	private final AsyncContext aCtx;
	private MACManager macManager;
	private final Logger log;

	public Host(AsyncContext aCtx, MACManager macManager, Logger log) {
		this.aCtx = aCtx;
		this.macManager = macManager;
		this.log = log;
	}

	@Override
	public void run() {
		ServletRequest req = aCtx.getRequest();
		HttpServletResponse resp = (HttpServletResponse) aCtx.getResponse();
		try {
			String firstLine = req.getReader().readLine();
			if (firstLine == null) {
				resp.setStatus(400);
				return;
			}
			if (firstLine.isEmpty()) {
				resp.setStatus(400);
				return;
			}
			//TODO: MORE STRONG AUTHENTIFICATION
			long mobileAgentId = EncDecTools.decodeMobileAgentId(firstLine, ISid.SALTMA);
			if (mobileAgentId <= 0) {
				resp.setStatus(400);
				return;
			}
			
			//TODO: check key for remote-control
			String localStreamDescriptor = null;
			String secondLine = req.getReader().readLine();
			if (secondLine != null) {
				localStreamDescriptor = req.getReader().readLine();
			}
			//Target mobile agent id
			long targetMobileAgentId;
			String remoteStreamDescriptor;			
			//Split server and client requests
			if (secondLine != null &&
					(targetMobileAgentId = EncDecTools.decodeMobileAgentId(secondLine, ISid.SALTMA)) != IMobileAgent.INVALID_ID) {
				remoteStreamDescriptor = macManager.getClientRemoteStreamDescriptor(mobileAgentId, targetMobileAgentId, localStreamDescriptor);
			} else {
				remoteStreamDescriptor = macManager.getServerRemoteStreamDescriptor(mobileAgentId, localStreamDescriptor);
			}
			//Write result to output if not null
			if (remoteStreamDescriptor != null) {
				resp.getWriter().write(remoteStreamDescriptor);
			}
			//Not cached response
			resp.setHeader("Cache-Control", "private");
			resp.setStatus(200);
		} catch (Throwable th) {
			log.error("Host detection fail by application server reason.", th);
			resp.setStatus(500);
		} finally {
			aCtx.complete();
		}
	}
	
}
