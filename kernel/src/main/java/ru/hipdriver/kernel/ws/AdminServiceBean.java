/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import ru.hipdriver.i.IUser;
import ru.hipdriver.i.report.ILiveReportRecord;
import ru.hipdriver.j.User;
import ru.hipdriver.j.report.LiveReportRecord;
import ru.hipdriver.kernel.Invariants;
import ru.hipdriver.kernel.Values;
import ru.hipdriver.kernel.ejb3.UserSessionManager;
import ru.hipdriver.kernel.ejb3.report.AdminReportsGenerator;


/**
 * Service /kernel/services/admin 
 * @author ivict
 */
@Stateless
@LocalBean
public class AdminServiceBean implements AdminService {

	@EJB
	private UserSessionManager userSessionManager;
	
	@EJB
	private AdminReportsGenerator adminReportsGenerator;
	
	public IUser whoAreYou(String userId) {
		if (userId == null) {//Not specified parameter return empty list
			return null;
		}
		if (Values.isNotDecimalIntegerNumber(userId)) {
			return null;
		}
		int userIdValue = Integer.valueOf(userId);
		IUser user = new User();
		user.setId(userIdValue);
		return user;
	}

	/**
	 * @see AdminService#deleteMobileAgent()
	 */
	@Override
	public boolean deleteMobileAgent(String userId, String mobileAgentId) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return false;			
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return false;
		}
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		try {
			userSessionManager.disableMobileAgent(user.getId(), mobileAgentIdValue);
			return true;
		} catch (Throwable th) {
			return false;
		}
	}

	/**
	 * @see AdminService#changeMobileAgentProfile(String, String, String, String, String)
	 */
	@Override
	public boolean changeMobileAgentProfile(String userId, String mobileAgentId,
			String delayPushAlertEventInSeconds, String delayWhereAmIRequestInSeconds,
			String enableDebugMode, String asdMaxJerk, String asdMaxAcceleration,
			String asdMaxAngle) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return false;			
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return false;
		}
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		try {
			userSessionManager.changeMobileAgentProfile(user.getId(), mobileAgentIdValue,
					delayPushAlertEventInSeconds, delayWhereAmIRequestInSeconds,
					enableDebugMode, asdMaxJerk, asdMaxAcceleration, asdMaxAngle);
			return true;
		} catch (Throwable th) {
			return false;
		}
	}

	/**
	 * @see AdminService#changeIncommingMessage(String, String, String)
	 */
	@Override
	public boolean changeIncommingMessage(String userId, String mobileAgentId,
			String message) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return false;			
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return false;
		}
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		try {
			userSessionManager.changeIncommingMessage(user.getId(), mobileAgentIdValue, message);
			return true;
		} catch (Throwable th) {
			return false;
		}
	}

	/**
	 * @see AdminService#changeFirstSignupIncommingMessage(String)
	 */
	@Override
	public boolean changeFirstSignupIncommingMessage(String message) {
		try {
			userSessionManager.changeSharedMemoryForAllUsersEntry(Invariants.FIRST_SIGNUP_INCOMING_MESSAGE_KEY, message);
			return true;
		} catch (Throwable th) {
			return false;
		}
	}

	/**
	 * @see AdminService#changeCurrentAndroidApplicationVersion(String)
	 */
	@Override
	public boolean changeCurrentAndroidApplicationVersion(String version) {
		try {
			userSessionManager.changeSharedMemoryForAllUsersEntry(Invariants.CURRENT_ANDROID_APPLICATION_VERSION_KEY, version);
			return true;
		} catch (Throwable th) {
			return false;
		}
	}

	/**
	 * @see AdminService#lrvUserLimits(String)
	 */
	@Override
	public List<LiveReportRecord> lrvUserLimits() {
		List<ILiveReportRecord> userLimits = adminReportsGenerator.userLimits();
		if (userLimits == null) {
			return Collections.emptyList();
		}
		if (userLimits.isEmpty()) {
			return Collections.emptyList();
		}
		return convertToJsonPowered(userLimits);
	}

	/**
	 * @see AdminService#lreUserLimits(String)
	 */
	@Override
	public boolean lreUserLimits(LiveReportRecord changedRecord) {
		if (changedRecord == null) {
			return false;
		}
		return adminReportsGenerator.changeUserLimits(changedRecord);
	}

	/**
	 * @see AdminService#lrvStateOfDevices(String)
	 */
	@Override
	public List<LiveReportRecord> lrvStateOfDevices() {
		List<ILiveReportRecord> stateOfDevices = adminReportsGenerator.stateOfDevices();
		if (stateOfDevices == null) {
			return Collections.emptyList();
		}
		if (stateOfDevices.isEmpty()) {
			return Collections.emptyList();
		}
		return convertToJsonPowered(stateOfDevices);
	}

	private List<LiveReportRecord> convertToJsonPowered(
			List<ILiveReportRecord> reportRecords) {
		List<LiveReportRecord> outputData = new ArrayList<LiveReportRecord>();
		for (ILiveReportRecord entry : reportRecords) {
			LiveReportRecord item = new LiveReportRecord(entry.getKey(), entry.getData());
			outputData.add(item);
		}
		return outputData;
	}

	/**
	 * @see AdminService#lrvSensitivitySettings(String)
	 */
	@Override
	public List<LiveReportRecord> lrvSensitivitySettings() {
		List<ILiveReportRecord> sensitivitySettings = adminReportsGenerator.sensitivitySettings();
		if (sensitivitySettings == null) {
			return Collections.emptyList();
		}
		if (sensitivitySettings.isEmpty()) {
			return Collections.emptyList();
		}
		return convertToJsonPowered(sensitivitySettings);
	}

	/**
	 * @see AdminService#lreSensitivitySettings(LiveReportRecord)
	 */
	@Override
	public boolean lreSensitivitySettings(LiveReportRecord changedRecord) {
		if (changedRecord == null) {
			return false;
		}
		return adminReportsGenerator.changeSensitivitySettings(changedRecord);
	}
	
	
}
