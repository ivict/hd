/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import static ru.hipdriver.kernel.Invariants.ADMIN;
import static ru.hipdriver.kernel.Invariants.ADMIN_DELETE_MOBILE_AGENT;
import static ru.hipdriver.kernel.Invariants.ADMIN_CHANGE_MOBILE_AGENT_PROFILE;
import static ru.hipdriver.kernel.Invariants.ADMIN_CHANGE_INCOMMING_MESSAGE;
import static ru.hipdriver.kernel.Invariants.ADMIN_CHANGE_FIRST_SIGN_UP_INCOMMING_MESSAGE;
import static ru.hipdriver.kernel.Invariants.ADMIN_CHANGE_CURRENT_ANDROID_APPLICATION_VERSION;
import static ru.hipdriver.kernel.Invariants.ADMIN_LRV_USER_LIMITS;
import static ru.hipdriver.kernel.Invariants.ADMIN_LRE_USER_LIMITS;
import static ru.hipdriver.kernel.Invariants.ADMIN_LRV_STATE_OF_DEVICES;
import static ru.hipdriver.kernel.Invariants.ADMIN_LRV_SENSITIVITY_SETTINGS;
import static ru.hipdriver.kernel.Invariants.ADMIN_LRE_SENSITIVITY_SETTINGS;

import java.util.List;

import static ru.hipdriver.i.IMobileAgentProfile.*;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ru.hipdriver.j.report.LiveReportRecord;

/**
 * Сервис административных функций пользователя.
 * 
 * @author ivict
 */
@Path(ADMIN)
public interface AdminService {

	/**
	 * Удаление мобильного агента из пользовательского интерфейса.
	 * @return
	 */
	@GET
	@Path(ADMIN_DELETE_MOBILE_AGENT)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean deleteMobileAgent(@QueryParam("user-id") String userId,
			@QueryParam("mobile-agent-id") String mobileAgentId);


	/**
	 * Изменение профиля мобильного агента.
	 * @return
	 */
	@GET
	@Path(ADMIN_CHANGE_MOBILE_AGENT_PROFILE)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean changeMobileAgentProfile(@QueryParam("user-id") String userId,
			@QueryParam("mobile-agent-id") String mobileAgentId,
			@QueryParam(DELAY_PUSH_ALERT_EVENT_IN_SECONDS) String delayPushAlertEventInSeconds,
			@QueryParam(DELAY_WHERE_AM_I_REQUEST_IN_SECONDS) String delayWhereAmIRequestInSeconds,
			@QueryParam(ENABLE_DEBUG_MODE) String enableDebugMode,
			@QueryParam(ASD_MAX_JERK) String asdMaxJerk,
			@QueryParam(ASD_MAX_ACCELERATION) String asdMaxAcceleration,
			@QueryParam(ASD_MAX_ANGLE) String asdMaxAngle);

	/**
	 * Изменение сообщения для пользователя мобильного приложения после sign-up.
	 * @return true if success
	 */
	@GET
	@Path(ADMIN_CHANGE_INCOMMING_MESSAGE)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean changeIncommingMessage(@QueryParam("user-id") String userId,
			@QueryParam("mobile-agent-id") String mobileAgentId,
			@QueryParam("message") String message);

	/**
	 * Изменение сообщения для пользователя мобильного приложения после первого sign-up.
	 * @return true if success
	 */
	@GET
	@Path(ADMIN_CHANGE_FIRST_SIGN_UP_INCOMMING_MESSAGE)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean changeFirstSignupIncommingMessage(@QueryParam("message") String message);
	
	/**
	 * Изменение сообщения для пользователя мобильного приложения после первого sign-up.
	 * @return true if success
	 */
	@GET
	@Path(ADMIN_CHANGE_CURRENT_ANDROID_APPLICATION_VERSION)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean changeCurrentAndroidApplicationVersion(@QueryParam("version") String version);
	
	/**
	 * Отчет по лимитам пользователей.
	 * Запрос генерации данных.
	 * @return данные для отчета.
	 */
	@GET
	@Path(ADMIN_LRV_USER_LIMITS)
	@Produces(MediaType.APPLICATION_JSON)
	public List<LiveReportRecord> lrvUserLimits();
	
	/**
	 * Отчет по лимитам пользователей.
	 * Запрос редактирования данных.
	 * @return true if success
	 */
	@POST
	@Path(ADMIN_LRE_USER_LIMITS)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean lreUserLimits(LiveReportRecord changedRecord);

	/**
	 * Отчет по состоянию устройств.
	 * Запрос генерации данных.
	 * @return данные для отчета.
	 */
	@GET
	@Path(ADMIN_LRV_STATE_OF_DEVICES)
	@Produces(MediaType.APPLICATION_JSON)
	public List<LiveReportRecord> lrvStateOfDevices();
	
	/**
	 * Отчет по параметрам чувствительности.
	 * Запрос генерации данных.
	 * @return данные для отчета.
	 */
	@GET
	@Path(ADMIN_LRV_SENSITIVITY_SETTINGS)
	@Produces(MediaType.APPLICATION_JSON)
	public List<LiveReportRecord> lrvSensitivitySettings();
	
	/**
	 * Отчет по параметрам чувствительности.
	 * Запрос редактирования данных.
	 * @return true if success
	 */
	@POST
	@Path(ADMIN_LRE_SENSITIVITY_SETTINGS)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean lreSensitivitySettings(LiveReportRecord changedRecord);
	
}
