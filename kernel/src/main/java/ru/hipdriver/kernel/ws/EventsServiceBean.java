/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import ru.hipdriver.g.Locations;
import ru.hipdriver.i.IEvent;
import ru.hipdriver.i.IEventClass;
import ru.hipdriver.i.IEventWithAtach;
import ru.hipdriver.i.IUser;
import ru.hipdriver.j.Event;
import ru.hipdriver.j.EventClass;
import ru.hipdriver.j.EventWithAtach;
import ru.hipdriver.j.Sid;
import ru.hipdriver.j.User;
import ru.hipdriver.kernel.Values;
import ru.hipdriver.kernel.ejb3.EventsManager;
import ru.hipdriver.kernel.ejb3.UserSessionManager;

/**
 * Service /kernel/services/events 
 * @author ivict
 */
@Stateless
@LocalBean
public class EventsServiceBean implements EventsService {
	
	@EJB
	private EventsManager eventsManager;  
	
	@EJB
	private UserSessionManager userSessionManager;
	
	public IUser whoAreYou(String userId) {
		if (userId == null) {//Not specified parameter return empty list
			return null;
		}
		if (Values.isNotDecimalIntegerNumber(userId)) {
			return null;
		}
		int userIdValue = Integer.valueOf(userId);
		IUser user = new User();
		user.setId(userIdValue);
		return user;
	}
	
	/**
     * @see ru.hipdriver.kernel.ws.EventsService#push(String, Event)
     */
	@Override
	public boolean push(String a, Event event) {
		if ( invalidValue(a) ) {
			return false;
		}
		Date receivedEventTime = new Date(System.currentTimeMillis());
		//Convert from string to long
		Sid s = new Sid();
		s.setA(Long.valueOf(a));
		if ( event.gpsLocation != null ) {
			Locations.unpackGpsLocation(event);
		}
		if ( event.gsmLocation != null ) {
			Locations.unpackGsmLocation(event);
		}
		return eventsManager.push(s, event, receivedEventTime);
	}

	/**
     * @see ru.hipdriver.kernel.ws.EventsService#count()
     */
	@Override
	public int count() {
		return eventsManager.count();
	}

	/**
     * @see ru.hipdriver.kernel.ws.EventsService#pusha(String, EventWithAtach)
     */
	@Override
	public boolean pusha(@HeaderParam("a") String a,
			EventWithAtach eventWithAtach) {
		if ( invalidValue(a) ) {
			return false;
		}
		Date receivedEventTime = new Date(System.currentTimeMillis());
		Sid s = new Sid();
		s.setA(Long.valueOf(a));
		if ( eventWithAtach.gpsLocation != null ) {
			Locations.unpackGpsLocation(eventWithAtach);
		}
		if ( eventWithAtach.gsmLocation != null ) {
			Locations.unpackGsmLocation(eventWithAtach);
		}
		return eventsManager.pusha(s, eventWithAtach, eventWithAtach, receivedEventTime);
	}

	private boolean invalidValue(String string) {
		return string == null || string.isEmpty();
	}

	/**
     * @see ru.hipdriver.kernel.ws.EventsService#list(String, Date, Date)
     */
	@Override
	public List<Event> list(String userName, Date dateFrom, Date dateTo) {
		if ( invalidValue(userName) ) {
			return Collections.emptyList();
		}
		if ( !userSessionManager.existUser(userName) ) {
			return Collections.emptyList();
		}
		List<IEvent> events = eventsManager.list(userName, dateFrom, dateTo);
		if ( events.isEmpty() ) {
			return Collections.emptyList();
		}
		List<Event> result = new ArrayList<Event>();
		for ( IEvent e : events ) {
			Event event = new Event();
			event.setEventClassId(e.getEventClassId());
			event.setMobileAgentId(e.getMobileAgentId());
			event.setTime(e.getTime());
			event.setLat(e.getLat());
			event.setLon(e.getLon());
			event.setAlt(e.getAlt());
			event.setAcc(e.getAcc());
			event.setMcc(e.getMcc());
			event.setMnc(e.getMnc());
			event.setLac(e.getLac());
			event.setCid(e.getCid());
			result.add(event);
		}
		return result;
	}

	/**
     * @see ru.hipdriver.kernel.ws.EventsService#classes(String)
     */
	@Override
	public List<EventClass> classes(String eventClassId) {
		List<IEventClass> eventClasses = eventsManager.classes();
		if ( eventClasses.isEmpty() ) {
			return Collections.emptyList();
		}
		List<EventClass> result = new ArrayList<EventClass>();
		for ( IEventClass e : eventClasses ) {
			EventClass eventClass = new EventClass();
			eventClass.setId(e.getId());
			eventClass.setName(e.getName());
			result.add(eventClass);
		}
		return result;
	}

	/**
     * @see ru.hipdriver.kernel.ws.EventsService#lastTenEvents(String, String, String, String)
     */
	@Override
	public String lastTenEvents(String userId, String eventClassId, String mobileAgentId, String onDate) {
		IUser user = whoAreYou(userId);
		if ( user == null ) {
			return null;			
		}
		if ( !Values.isValidLong(mobileAgentId) ) {
				return null;
		}
		
		long mobileAgentIdValue = Long.valueOf(mobileAgentId);
		
		if (eventClassId != null && Values.isNotDecimalShortNumber(eventClassId)) {
			return null;
		}
		short eventClassIdValue = eventClassId != null ? Short.valueOf(eventClassId) : 0;
		//TODO: parse onDate parameter
		Date onDateValue = new Date();
		final List<IEventWithAtach> events;
		if (eventClassIdValue == 0) {
			events = eventsManager.lastEvents(user.getId(), mobileAgentIdValue, onDateValue, 10);
		} else {
			events = eventsManager.lastEvents(user.getId(), eventClassIdValue, mobileAgentIdValue, onDateValue, 10);
		}
		if ( events.isEmpty() ) {
			return null;
		}
		
		StringBuilder text = new StringBuilder();
		text.append("event-class-id").append(';');
		text.append("mobile-agent-id").append(';');
		text.append("time").append(';');
		text.append("received-time").append(';');
		text.append("lat").append(';');
		text.append("lon").append(';');
		text.append("alt").append(';');
		text.append("acc").append(';');
		text.append("mcc").append(';');
		text.append("mnc").append(';');
		text.append("lac").append(';');
		text.append("cid").append(';');
		text.append("content").append(';');
		text.append("check-sum").append(';');
		text.append("description");
		text.append("\n");
		
		for (IEventWithAtach event: events) {
			byte[] content = event.getContent();
			String atachContent = content != null ? new String(content) : "";
			String description = event.getDescription();
			String descr = description != null ? description: "";
			text.append(event.getEventClassId()).append(';');
			text.append(event.getMobileAgentId()).append(';');
			text.append(event.getTime()).append(';');
			text.append(event.getReceivedTime()).append(';');
			text.append(event.getLat()).append(';');
			text.append(event.getLon()).append(';');
			text.append(event.getAlt()).append(';');
			text.append(event.getAcc()).append(';');
			text.append(event.getMcc()).append(';');
			text.append(event.getMnc()).append(';');
			text.append(event.getLac()).append(';');
			text.append(event.getCid()).append(';');
			text.append(atachContent).append(';');
			text.append(event.getContentCheckSum()).append(';');
			text.append(descr);
			text.append("\n");
		}
		return text.toString();
	}

}
