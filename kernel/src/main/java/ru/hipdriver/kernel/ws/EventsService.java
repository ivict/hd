/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ws;

import static ru.hipdriver.kernel.Invariants.*;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ru.hipdriver.j.Event;
import ru.hipdriver.j.EventClass;
import ru.hipdriver.j.EventWithAtach;

/**
 * Сервис событий.
 * 
 * @author ivict
 */
@Path(EVENTS)
public interface EventsService {

	/**
	 * Регистрация события мобильного приложения.
	 * 
	 * @param event событие.
	 * @return true если успешно.
	 */
	@POST
	@Path(EVENTS_PUSH_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean push(@HeaderParam("a") String a, Event event);

	/**
	 * Регистрация события мобильного приложения.
	 * С приложенным к нему атачем.
	 * 
	 * @param event событие.
	 * @return true если успешно.
	 */
	@POST
	@Path(EVENTS_PUSH_PATHA)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean pusha(@HeaderParam("a") String a, EventWithAtach eventWithAtach);

	/**
	 * Общее число событий.
	 * 
	 * @return целое число.
	 */
	@GET
	@Path(EVENTS_COUNT_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public int count();

	/**
	 * Список событий для пользователя.
	 * Предполагаемое использование - отладка.
	 * 
	 * @return Список событий для пользователя по имене в определенном интервале.
	 */
	@GET
	@Path(EVENTS_LIST_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Event> list(@QueryParam("user-name") String userName,
				@QueryParam("date-from") Date dateFrom,
				@QueryParam("date-to") Date dateTo);

	/**
	 * Список классов событий.
	 * 
	 * @return Идентификаторы классов событий.
	 */
	@GET
	@Path(EVENTS_CLASSES_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventClass> classes(@QueryParam("event-class-id") String eventClassId);

	/**
	 * Возвращает последнее десять событий заданного класса вместе с атачем.
	 * Если класс не указан, то возвращаем последние десять событий.
	 * Предполагаемое использование - отладка.
	 * @param mobileAgentId TODO
	 * @param onDate TODO
	 * 
	 * @return Одиночное событие с атачем или пустой объект.
	 */
	@GET
	@Path(EVENTS_LAST_TEN_EVENTS_PATH)
	@Produces(MediaType.TEXT_PLAIN)
	public String lastTenEvents(
			@QueryParam("user-id") String userId,
			@QueryParam("event-class-id") String eventClassId,
			@QueryParam("mobile-agent-id") String mobileAgentId,
			@QueryParam("on-date") String onDate);
	
}
