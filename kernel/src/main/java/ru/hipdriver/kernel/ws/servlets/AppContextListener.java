package ru.hipdriver.kernel.ws.servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {

	public void contextInitialized(ServletContextEvent servletContextEvent) {
//		// create the thread pool for ping service
//		ThreadPoolExecutor pingExecutor = new ThreadPoolExecutor(10, 200, 50000L,
//				TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(10));
//		servletContextEvent.getServletContext().setAttribute(Invariants.PING_EXECUTOR,
//				pingExecutor);
//		// create the thread pool for pub key update service
//		ThreadPoolExecutor pubkeyUpdateExecutor = new ThreadPoolExecutor(1, 10, 50000L,
//				TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(1));
//		servletContextEvent.getServletContext().setAttribute(Invariants.PUBKEY_UPDATE_EXECUTOR,
//				pubkeyUpdateExecutor);

	}

	public void contextDestroyed(ServletContextEvent servletContextEvent) {
//		ThreadPoolExecutor pingExecutor = (ThreadPoolExecutor) servletContextEvent
//				.getServletContext().getAttribute(Invariants.PING_EXECUTOR);
//		pingExecutor.shutdown();
//		ThreadPoolExecutor pubkeyUpdateExecutor = (ThreadPoolExecutor) servletContextEvent
//				.getServletContext().getAttribute(Invariants.PUBKEY_UPDATE_EXECUTOR);
//		pubkeyUpdateExecutor.shutdown();
	}

}