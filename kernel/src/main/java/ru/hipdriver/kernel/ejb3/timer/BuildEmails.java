/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.kernel.ejb3.EmailEventProcessor;
import ru.hipdriver.kernel.ejb3.MASHelper;

/**
 * Таймер сборки сообщений электронной почты. Обрабатывает события от
 * мобильных агентов.
 * 
 * @author ivict
 * 
 */
@Startup
@Singleton
@Local(EmailEventsTimer.class)
public class BuildEmails implements EmailEventsTimer {
	final Logger log = LoggerFactory.getLogger(EmailEventsTimer.class);

	@PersistenceContext
	private EntityManager em;

	private List<Short> eventClassesFilter;
	
	@EJB
	EmailEventProcessor emailEventProcessor;

	@EJB
	MASHelper masHelper;

	/**
	 * @see TracksTimer#invoke()
	 */
	@Schedule(hour = "*", minute = "*", second = "*/15", persistent = false)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void invoke() {
		// Select Last events
		// If event has class включился и отключился
		// смотрим на атач. Десериализуем, в что то, типа IMobileAgentState;
		// Обрабатываем mobile agent state, через установку мобильному агенту
		// Пропуск обработанных осуществляем по полю времени.

		// Old states
		List<?> updatesStates = em
				.createQuery(
						"select e.id, e.eventClassId, ma.id, e.receivedTime from EventAtach a join a.event e join e.mobileAgent ma join ma.mobileAgentState mas where e.receivedTime > mas.lastEmailTime and a.content is not null and e.eventClassId in (:eventClassesFilter) order by e.receivedTime asc")
				.setParameter("eventClassesFilter", getEventClassesFilter())
				.getResultList();
		for (Object o : updatesStates) {
			EventState updateState = new EventState(o);
			long eventId = updateState.getEventId();
			short eventClassId = updateState.getEventClassId();
			long mobileAgentId = updateState.getMobileAgentId();
			Date receivedEventTime = updateState.getReceivedEventTime();
			try {
				emailEventProcessor.process(eventId, eventClassId, mobileAgentId,
						receivedEventTime);
			} catch (RuntimeException e) {
				// Check communication link error
				if (findSubstring(e,
						"com.mysql.jdbc.exceptions.jdbc4.CommunicationsException")) {
					log.warn(String
							.format("Retry process of event with id[%d], class_id[%d], received_time[%s]",
									eventId, eventClassId, receivedEventTime));
					continue;
				}
				safetyMarkProcessed(mobileAgentId, receivedEventTime);
			}
		}

		// New states
		List<?> insertsStates = em
				.createQuery(
						"select e.id, e.eventClassId, ma.id, e.receivedTime from EventAtach a join a.event e join e.mobileAgent ma where ma.mobileAgentState is null and a.content is not null and e.eventClassId in (:eventClassesFilter) order by e.receivedTime asc")
				.setParameter("eventClassesFilter", getEventClassesFilter())
				.getResultList();
		for (Object o : insertsStates) {
			EventState insertState = new EventState(o);
			long eventId = insertState.getEventId();
			short eventClassId = insertState.getEventClassId();
			long mobileAgentId = insertState.getMobileAgentId();
			Date eventTime = insertState.getReceivedEventTime();
			try {
				emailEventProcessor.process(eventId, eventClassId, mobileAgentId,
						eventTime);
			} catch (RuntimeException e) {
				// Check communication link error
				if (findSubstring(e,
						"com.mysql.jdbc.exceptions.jdbc4.CommunicationsException")) {
					log.warn(String
							.format("Retry process of event with id[%d], class_id[%d], received_time[%s]",
									eventId, eventClassId, eventTime));
					continue;
				}
				safetyMarkProcessed(mobileAgentId, eventTime);
			}
		}
	}

	private boolean findSubstring(Throwable th, String className) {
		if (th == null) {
			return false;
		}
		if (className == null) {
			return false;
		}
		String throwableClassName = th.getClass().getName();
		if (className.equals(throwableClassName)) {
			return true;
		}
		Throwable parentThrowable = th.getCause();
		while (parentThrowable != null) {
			throwableClassName = parentThrowable.getClass().getName();
			if (className.equals(throwableClassName)) {
				return true;
			}
			Throwable cause = parentThrowable;
			parentThrowable = parentThrowable.getCause();
			if (parentThrowable == cause) {
				break;
			}
		}
		return false;
	}

	private void safetyMarkProcessed(long mobileAgentId, Date receivedEventTime) {
		try {
			emailEventProcessor.markProcessedEmail(mobileAgentId,
					receivedEventTime);
		} catch (RuntimeException e) {
			log.error("What the terrible failure", e);
		}
	}

	private List<Short> getEventClassesFilter() {
		if (eventClassesFilter != null) {
			return eventClassesFilter;
		}
		eventClassesFilter = new ArrayList<Short>();
		eventClassesFilter.add(masHelper.getEventClassId("send-email-event"));
		return eventClassesFilter;
	}

}
