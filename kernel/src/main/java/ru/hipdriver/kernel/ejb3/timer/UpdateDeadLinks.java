/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.Local;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.kernel.entity.MobileAgentPing;

/**
 * Таймер обновления состояния мобильных агентов.
 * Обрабатывает события от мобильных агентов.
 * @author ivict
 *
 */
@Startup
@Singleton
@Local(DeadLinkTimer.class)
public class UpdateDeadLinks implements DeadLinkTimer {
	final Logger log = LoggerFactory.getLogger(UpdateDeadLinks.class);
	
	@PersistenceContext
	private EntityManager em;
	/**
	 * @see UMASTimer#invoke()
	 */
	@Schedule(hour="*", minute="*/5", second="0", persistent=false)
	public void invoke() {
		Date latestPingsHorizonStart = new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(2));
		Date latestPingsHorizonEnd = new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(1));
		//Dead links candidates
		//Warning: If 68 version of mobile-agent then mobile-agent don't send ping in energy saving mode.
		List<?> offlineMobileAgentIds = em.createQuery("select p.mobileAgentId, p.time from MobileAgentPing p where p.time between :horizonStart and :horizonEnd and p.online = :online")
				.setParameter("horizonStart", latestPingsHorizonStart)
				.setParameter("horizonEnd", latestPingsHorizonEnd)
				.setParameter("online", true)
				.getResultList();
		for ( Object o : offlineMobileAgentIds ) {
			Object[] args = (Object[]) o;
			long mobileAgentId = ((Number) args[0]).longValue();
			MobileAgentPing mobileAgentPing = em.find(MobileAgentPing.class, mobileAgentId);
			if (mobileAgentPing == null) {
				continue;
			}
			mobileAgentPing.setOnline(false);
			em.merge(mobileAgentPing);
		}
		List<?> onlineMobileAgentIds = em.createQuery("select p.mobileAgentId, p.time from MobileAgentPing p where p.time >= :horizonStart and p.online = :online")
				.setParameter("horizonStart", latestPingsHorizonEnd)
				.setParameter("online", false)
				.getResultList();
		for ( Object o : onlineMobileAgentIds ) {
			Object[] args = (Object[]) o;
			long mobileAgentId = ((Number) args[0]).longValue();
			MobileAgentPing mobileAgentPing = em.find(MobileAgentPing.class, mobileAgentId);
			if (mobileAgentPing == null) {
				continue;
			}
			mobileAgentPing.setOnline(true);
			em.merge(mobileAgentPing);
		}
		
	}
	
}
