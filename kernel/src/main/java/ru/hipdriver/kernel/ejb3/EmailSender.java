/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;

/**
 * Интерфейс бина отправки электронной почты.
 * @author ivict
 *
 */
public interface EmailSender {
	
	boolean sendMessage(long messageId);
	void markProcessedFail(long messageId, Date sendingTime);
	
}
