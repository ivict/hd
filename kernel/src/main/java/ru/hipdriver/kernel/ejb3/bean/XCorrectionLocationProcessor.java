/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.bean;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.ILocation;
import ru.hipdriver.kernel.ejb3.CorrectionLocationProcessor;
import ru.hipdriver.kernel.ejb3.KernelException;
import ru.hipdriver.kernel.ejb3.MASHelper;
import ru.hipdriver.kernel.entity.MobileAgent;
import ru.hipdriver.kernel.entity.MobileAgentLastLocation;

/**
 * Процессор коррекции месторасположения мобильных агентов.
 * @author ivict
 *
 */
@Stateless
@Local(CorrectionLocationProcessor.class)
@EJB(name = "java:app/CorrectionLocationProcessor", beanInterface = CorrectionLocationProcessor.class)
public class XCorrectionLocationProcessor implements CorrectionLocationProcessor {
	private final Logger log = LoggerFactory.getLogger(XCorrectionLocationProcessor.class);
	
	@PersistenceContext
	private EntityManager em;

	@EJB
	private MASHelper masHelper;
	
	/**
	 * @see ru.hipdriver.kernel.ejb3.CorrectionLocationProcessor#process(long, short, long, Date)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void process(long eventId, short eventClassId, long mobileAgentId,
			Date receivedEventTime) {
		try {
			if (eventClassId == masHelper.getEventClassId("mobile-agent-send-correction-location-event")) {
				//TODO: more work on get cell
				ILocation location = getEventLastLocation(eventId);
				processLastLocation(mobileAgentId, location, receivedEventTime);
				return;
			}
			log.warn("Unknown type of event for this [" + this.getClass().getSimpleName() + "] processor, event with id = " + eventId);
			throw new EJBException();
		} catch (Throwable th) {
			log.warn("Fail processing for event with id = " + eventId, th);
			throw new EJBException();
			//throw new KernelException(th);
		}
	}
	
	private void processLastLocation(long mobileAgentId, ILocation location, Date receivedEventTime) {
		MobileAgentLastLocation mobileAgentLastLocation = em.find(MobileAgentLastLocation.class, mobileAgentId);
		if ( mobileAgentLastLocation == null ) {//First setup
			mobileAgentLastLocation = new MobileAgentLastLocation();
			mobileAgentLastLocation.setMobileAgentId(mobileAgentId);
			updateLastLocation(mobileAgentLastLocation, location, receivedEventTime);
			em.persist(mobileAgentLastLocation);
			return;
		}
		//Invalid location nothing do
		if (invalidLocation(location)) {
			throw new EJBException("Invalid location for mobile agent id [" + mobileAgentId + "]");
		}
		//Update only newest
		Date time = mobileAgentLastLocation.getTime();
		if (time != null && time.after(receivedEventTime)) {
			return;
		}
		//Update only well-known coordinate
		if (isChangeWellKnownOntoZero(mobileAgentLastLocation, location)) {
			//TODO: write request to cell towers mapping
			mobileAgentLastLocation.setTime(receivedEventTime);
			em.merge(mobileAgentLastLocation);
			return;
		}
		updateLastLocation(mobileAgentLastLocation, location, receivedEventTime);
		em.merge(mobileAgentLastLocation);
	}
	
	private ILocation getEventLastLocation(long eventId) {
		ILocation location = (ILocation) 
				em.createQuery("select new ru.hipdriver.kernel.ejb3.Location(e.lat, e.lon, e.alt, e.acc, e.mcc, e.mnc, e.lac, e.cid) from Event e where e.id = :eventId")
				.setParameter("eventId", eventId)
				.getSingleResult();
		return location;
	}
	
	private boolean isChangeWellKnownOntoZero(
			ILocation prevLocation,
			ILocation newLocation) {
		if (prevLocation.getLat() != ILocation.UNKNOWN_LATITUDE && newLocation.getLat() == ILocation.UNKNOWN_LATITUDE) {
			return true;
		}
		if (prevLocation.getLon() != ILocation.UNKNOWN_LONGITUDE && newLocation.getLon() == ILocation.UNKNOWN_LONGITUDE) {
			return true;
		}
		return false;
	}

	private boolean invalidLocation(ILocation location) {
		//Has GPS Position
		if ( ILocation.Validator.isUnknownLocation(location) ) {
			return false;
		}
		//Has GSM Position
		if ( ILocation.Validator.isUnknownGsmLocation(location) ) {
			return false;
		}
		return true;
	}

	private void updateLastLocation(MobileAgentLastLocation lastLocation,
			ILocation value, Date receivedEventTime) {
		lastLocation.setLat(value.getLat());
		lastLocation.setLon(value.getLon());
		lastLocation.setAlt(value.getAlt());
		lastLocation.setAcc(value.getAcc());
		lastLocation.setMcc(value.getMcc());
		lastLocation.setMnc(value.getMnc());
		lastLocation.setLac(value.getLac());
		lastLocation.setCid(value.getCid());
		lastLocation.setTime(receivedEventTime);
	}

	/**
	 * @see ru.hipdriver.kernel.ejb3.CorrectionLocationProcessor#markProcessed(long, Date)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void markProcessed(long mobileAgentId, Date eventTime) {
		try {
			//Setup unknown car state, unknown app state, and defauld field values
			MobileAgent mobileAgent = em.find(MobileAgent.class, mobileAgentId);
			if ( mobileAgent == null ) {
				return;
			}
			MobileAgentLastLocation mobileAgentLastLocation = em.find(MobileAgentLastLocation.class, mobileAgentId);
			if ( mobileAgentLastLocation == null ) {
				return;
			}
			//Update only newest
			Date time = mobileAgentLastLocation.getTime();
			if (time != null && time.after(eventTime)) {
				return;
			}
			mobileAgentLastLocation.setTime(eventTime);//PROCESSED FLAG
			em.merge(mobileAgentLastLocation);
		} catch (Throwable th) {
			throw new KernelException(th);
		}
	}

}
