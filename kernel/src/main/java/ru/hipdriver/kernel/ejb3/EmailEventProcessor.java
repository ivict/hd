/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;
import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.kernel.entity.MobileAgent;
import ru.hipdriver.kernel.entity.MobileAgentState;

/**
 * Процессор фрагментов электронной почты, которую посылают мобильные агенты.
 * @author ivict
 *
 */
public interface EmailEventProcessor {
	void process(long eventId, short eventClassId, long mobileAgentId,
			Date eventTime);
	void markProcessedEmail(long mobileAgentId, Date eventTime);
}
