/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;


/**
 * Таймер сборки электронной почты.
 * @author ivict
 *
 */
public interface EmailEventsTimer {
	void invoke();
}
