package ru.hipdriver.kernel.ejb3;

import java.io.IOException;
import java.util.Date;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentWithState;
import ru.hipdriver.j.Location;
import ru.hipdriver.j.MobileAgentWithState;
import ru.hipdriver.kernel.entity.Event;
import ru.hipdriver.kernel.entity.EventAtach;
import ru.hipdriver.kernel.entity.EventClass;
import ru.hipdriver.kernel.entity.MobileAgent;

/**
 * Emulator implementation
 */
@Stateless(mappedName = "emulator")
@Local(Emulator.class)
public class BasicEmulator implements Emulator {
	final Logger log = LoggerFactory.getLogger(BasicEmulator.class);

	private static final int CENTER_LAT = 53251132;
	private static final int CENTER_LON = 50222687;
	

	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private UserSessionManager userSessionManager;

	/**
	 * @see UserSessionManager#requestMobileAgentLocation(int, long)
	 */
	@Override
	public ILocation requestMobileAgentLocation(int userId, long mobileAgentId)
			throws Throwable {
		Random random = new Random();
		Location location = new Location();
		location.setFactor(Locations.GEO_FACTOR);
		int lonDeltaSign = random.nextBoolean() ? 1 : -1;
		int latDeltaSign = random.nextBoolean() ? 1 : -1;
		
		int latDelta = random.nextInt(50000);
		location.setLat(CENTER_LAT + latDeltaSign * latDelta);
		int lonDelta = random.nextInt(50000);
		location.setLon(CENTER_LON + lonDeltaSign * lonDelta);
		return location;
	}

	/**
	 * @see UserSessionManager#activateCarAlarm(int, long)
	 * При выключении, эмулируем путем генерации псевдо события о включении для данного мобильного клиента.
	 */
	@Override
	public boolean activateCarAlarm(int userId, long mobileAgentId) {
		try {
			//@see db.changelog*.xml
			String eventName = "user-push-switch-on-button-in-application-on-mobile-agent";
			String eventDescription = "Emulation of activate car alarm";
			String appStateName = "car-alarm-activated";
			String carStateName = "unknown";
			
			IMobileAgentWithState mobileAgent = getLastMobileAgentStateAndResetFields(mobileAgentId,
					appStateName, carStateName);
			
			Event event = makeEvent(userId, (MobileAgent) mobileAgent, eventName);
			em.persist(event);
			em.flush();
			long eventId = event.getEventId();
			if (eventId == 0) {
				throw new IllegalStateException("Event not persisted correctly, no sense to continue!");
			}
			EventAtach eventAtach = makeEventAtach(mobileAgent, eventDescription,
					appStateName, carStateName, eventId);

			eventAtach.setEvent(event);
			event.setEventAtach(eventAtach);
			em.persist(eventAtach);
			em.merge(event);
			
			return true;
		} catch (Throwable th) {
			log.error("Terrify error", th);
		}
		return false;
	}

	private EventAtach makeEventAtach(IMobileAgentWithState e, String eventDescription,
			String appStateName, String carStateName, long eventId)
			throws IOException, JsonGenerationException, JsonMappingException {
		//Add event atach with mobile agent state
		MobileAgentWithState m = new MobileAgentWithState();
		m.setMobileAgentId(e.getMobileAgentId());
		m.setUserId(e.getUserId());
		m.setName(e.getName());
		m.setImei(e.getImei());
		m.setAppStateId(e.getAppStateId());
		m.setCarStateId(e.getCarStateId());
		m.setBatteryStatus(e.getBatteryStatus());
		m.setBatteryPct(e.getBatteryPct());
		m.setGsmBitErrorRate(e.getGsmBitErrorRate());
		m.setGsmSignalStrength(e.getGsmSignalStrength());
		ObjectMapper om = new ObjectMapper();
		byte[] content = om.writeValueAsBytes(m);
		EventAtach eventAtach = new EventAtach();
		eventAtach.setEventId(eventId);
		eventAtach.setContent(content);
		eventAtach.setDescription(eventDescription);
		return eventAtach;
	}

	private IMobileAgentWithState getLastMobileAgentStateAndResetFields(long mobileAgentId, String appStateName, String carStateName) {
		IMobileAgentWithState mobileAgent = userSessionManager.getMobileAgentWithState(mobileAgentId);
		if ( mobileAgent == null ) {
			throw new IllegalStateException("Mobile agent without state!");
		}
		em.detach(mobileAgent);
		short appStateId = ((Number) em.createQuery("select e.id from AppState e where e.name like :appStateName")
				.setParameter("appStateName", appStateName)
				.getSingleResult()).shortValue();
		short carStateId = ((Number) em.createQuery("select e.id from CarState e where e.name like :carStateName")
				.setParameter("carStateName", carStateName)
				.getSingleResult()).shortValue();
		mobileAgent.setAppStateId(appStateId);
		mobileAgent.setCarStateId(carStateId);
		return mobileAgent;
	}

	private Event makeEvent(int userId, MobileAgent mobileAgent, String eventName) throws Throwable {
		long mobileAgentId = mobileAgent.getId();
		EventClass eventClass = getEventClassByName(eventName);
		Event event = new Event();
		ILocation location = requestMobileAgentLocation(userId, mobileAgentId);
		event.setLat(location.getLat());
		event.setLon(location.getLon());
		event.setAlt(location.getAlt());
		event.setAcc(location.getAcc());
		event.setMcc(location.getMcc());
		event.setMnc(location.getMnc());
		event.setLac(location.getLac());
		event.setCid(location.getCid());
		event.setUserId(userId);
		event.setMobileAgent(mobileAgent);
		event.setEventClassId(eventClass.getId());
		event.setTime(new Date());
		return event;
	}

	private EventClass getEventClassByName(String eventClassName) {
		Object eventClass = em.createQuery("select e from EventClass e where e.name like :eventClassName")
			.setParameter("eventClassName", eventClassName)
			.getSingleResult();
		return (EventClass) eventClass;
	}

	/**
	 * @see UserSessionManager#switchOffCarAlarm(int, long)
	 * При выключении, эмулируем путем генерации псевдо события о выключении для данного мобильного клиента.
	 */
	@Override
	public boolean switchOffCarAlarm(int userId, long mobileAgentId) {
		try {
			//@see db.changelog*.xml
			String eventName = "user-push-switch-off-button-in-application-on-mobile-agent";
			String eventDescription = "Emulation of switch off car alarm";
			
			String appStateName = "car-alarm-switched-off";
			String carStateName = "unknown";
			
			IMobileAgentWithState mobileAgent = getLastMobileAgentStateAndResetFields(mobileAgentId,
					appStateName, carStateName);
			
			Event event = makeEvent(userId, (MobileAgent) mobileAgent, eventName);
			em.persist(event);
			em.flush();
			long eventId = event.getEventId();
			if (eventId == 0) {
				throw new IllegalStateException("Event not persisted correctly, no sense to continue!");
			}
			EventAtach eventAtach = makeEventAtach(mobileAgent, eventDescription,
					appStateName, carStateName, eventId);

			eventAtach.setEvent(event);
			event.setEventAtach(eventAtach);
			em.persist(eventAtach);
			em.merge(event);
			return true;
		} catch (Throwable th) {
			log.error("Terrify error", th);
		}
		return false;
	}

}
