package ru.hipdriver.kernel.ejb3;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.rowset.serial.SerialBlob;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.IAppState;
import ru.hipdriver.i.ICarState;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentConnection;
import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.i.IMobileAgentVersion;
import ru.hipdriver.i.IMobileAgentWithState;
import ru.hipdriver.i.IProperties;
import ru.hipdriver.i.ISKey;
import ru.hipdriver.i.IServiceProfile;
import ru.hipdriver.i.ISid;
import ru.hipdriver.i.ISignInMaResponse;
import ru.hipdriver.i.ISignUpMa;
import ru.hipdriver.i.ITrack;
import ru.hipdriver.i.IUser;
import ru.hipdriver.i.IUserCommand;
import ru.hipdriver.i.IUserCommandType;
import ru.hipdriver.i.IUserLimit;
import ru.hipdriver.j.EventWithAtach;
import ru.hipdriver.j.Location;
import ru.hipdriver.j.MobileAgentWithState;
import ru.hipdriver.j.Sid;
import ru.hipdriver.kernel.Invariants;
import ru.hipdriver.kernel.Values;
import ru.hipdriver.kernel.entity.MobileAgent;
import ru.hipdriver.kernel.entity.MobileAgentAlertTrack;
import ru.hipdriver.kernel.entity.MobileAgentConnection;
import ru.hipdriver.kernel.entity.MobileAgentMemoryChange;
import ru.hipdriver.kernel.entity.MobileAgentMemoryEntry;
import ru.hipdriver.kernel.entity.MobileAgentMemoryEntryKey;
import ru.hipdriver.kernel.entity.MobileAgentPing;
import ru.hipdriver.kernel.entity.MobileAgentProfile;
import ru.hipdriver.kernel.entity.MobileAgentPubkey;
import ru.hipdriver.kernel.entity.MobileAgentState;
import ru.hipdriver.kernel.entity.ServiceProfile;
import ru.hipdriver.kernel.entity.SharedMemoryForAllUsersEntry;
import ru.hipdriver.kernel.entity.User;
import ru.hipdriver.kernel.entity.UserCommand;
import ru.hipdriver.kernel.entity.UserKey;
import ru.hipdriver.kernel.entity.UserLimit;
import ru.hipdriver.kernel.entity.UserReferral;
import ru.hipdriver.util.EncDecTools;

/**
 * Session Bean implementation class BasicUserSessionManager
 */
@Stateless(mappedName = "user-session-manager")
@Local(UserSessionManager.class)
@EJB(name = "java:app/UserSessionManager", beanInterface = UserSessionManager.class)
public class BasicUserSessionManager implements UserSessionManager {
	final Logger log = LoggerFactory.getLogger(BasicUserSessionManager.class);
	
	private static final int DRUPAL_HASH_LENGTH = 55;
	
	@PersistenceContext
	private EntityManager em;

	@EJB
	private MACManager macManager;
	
	@EJB
	private MASHelper masHelper;
	
	//TODO: remove after test service MASHelper
	private Map<String, Short> carStates;
	
	private ISignInMaResponse signIn(int userId, final long mobileAgentId) {
		MobileAgent ma = em.find(MobileAgent.class, mobileAgentId);
		if (ma == null) {
			return null;
		}
		final MobileAgentProfile map = em.find(MobileAgentProfile.class, mobileAgentId);
		final Map<String, Object> properties = findStandardProperties(userId, mobileAgentId);
		final UserLimit ul = em.find(UserLimit.class, userId);
		// TODO: Нормальная регистрация в диспетчере сессий
		return new ISignInMaResponse() {
			private final String READ_ONLY_RESPONSE = "Read only response";
			
			@Override
			public long getA() {
				return mobileAgentId;
			}

			@Override
			public void setA(long a) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public long getMobileAgentId() {
				return mobileAgentId;
			}

			@Override
			public void setMobileAgentId(long mobileAgentId) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public int getDelayPushAlertEventInSeconds() {
				return map != null ? map.getDelayPushAlertEventInSeconds() : IMobileAgentProfile.DEFAULT_DELAY_PUSH_ALERT_EVENT_IN_SECONDS;
			}

			@Override
			public void setDelayPushAlertEventInSeconds(
					int delayPushAlertEventInSeconds) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public int getDelayWhereAmIRequestInSeconds() {
				return map != null ? map.getDelayWhereAmIRequestInSeconds() : IMobileAgentProfile.DEFAULT_DELAY_WHERE_AM_I_REQUEST_IN_SECONDS;
			}

			@Override
			public void setDelayWhereAmIRequestInSeconds(
					int delayWhereAmIRequestInSeconds) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public boolean isEnableDebugMode() {
				return map != null ? map.isEnableDebugMode() : IMobileAgentProfile.DEFAULT_ENABLE_DEBUG_MODE;
			}

			@Override
			public void setEnableDebugMode(boolean enableDebugMode) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public float getAsdMaxJerk() {
				return map != null ? map.getAsdMaxJerk() : IMobileAgentProfile.DEFAULT_ASD_MAX_JERK;
			}

			@Override
			public void setAsdMaxJerk(float asdMaxJerk) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public float getAsdMaxAcceleration() {
				return map != null ? map.getAsdMaxAcceleration() : IMobileAgentProfile.DEFAULT_ASD_MAX_ACCELERATION;
			}

			@Override
			public void setAsdMaxAcceleration(float asdMaxAcceleration) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public float getAsdMaxAngle() {
				return map != null ? map.getAsdMaxAngle() : IMobileAgentProfile.DEFAULT_ASD_MAX_ANGLE;
			}

			@Override
			public void setAsdMaxAngle(float asdMaxAngle) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public Map<String, Object> getProperties() {
				return properties;
			}

			@Override
			public void setProperties(Map<String, Object> properties) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public boolean isEnableEnergySavingMode() {
				return map != null ? map.isEnableEnergySavingMode() : IMobileAgentProfile.DEFAULT_ENABLE_ENERGY_SAVING_MODE;
			}

			@Override
			public void setEnableEnergySavingMode(boolean energySavingMode) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public int getWatchdogServiceStartDelayInSeconds() {
				return map != null ? map.getWatchdogServiceStartDelayInSeconds() : IMobileAgentProfile.DEFAULT_WATCHDOG_SERVICE_START_DELAY_IN_SECONDS;
			}

			@Override
			public void setWatchdogServiceStartDelayInSeconds(
					int watchdogServiceStartDelayInSeconds) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public boolean isTimeLimited() {
				return ul != null ? ul.isTimeLimited() : IUserLimit.DEFAULT_TIME_LIMITED;
			}

			@Override
			public void setTimeLimited(boolean timeLimited) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public int getMaxDevices() {
				return ul != null ? ul.getMaxDevices() : IUserLimit.DEFAULT_MAX_DEVICES;
			}

			@Override
			public void setMaxDevices(int maxDevices) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public short getControlModeId() {
				return map != null ? map.getControlModeId() : IMobileAgentProfile.DEFAULT_CONTROL_MODE_ID;
			}

			@Override
			public void setControlModeId(short controlModeId) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

			@Override
			public short getMapProviderId() {
				return map != null ? map.getMapProviderId() : IMobileAgentProfile.DEFAULT_MAP_PROVIDER_ID;
			}

			@Override
			public void setMapProviderId(short mapProviderId) {
				throw new IllegalStateException(READ_ONLY_RESPONSE);
			}

		};
	}

	/**
	 * Составляем свойства по тем изменениям которые происходили, до очередного момента отправки изменений.
	 * @param userId TODO
	 * @param mobileAgentId mobile agent
	 * @return
	 */
	private Map<String, Object> findStandardProperties(int userId, long mobileAgentId) {
		Map<String, Object> properties = null;
		String value;
		//Sending time
		Date sendingTime = new Date(System.currentTimeMillis());
		//Collect changes by properties
		//Only not null values sending
		
		MobileAgentMemoryEntryKey key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.ALERT_PHONE_NUMBER_KEY); 
		MobileAgentMemoryChange change = em.find(MobileAgentMemoryChange.class, key);
		if (change != null && isNotSended(change) && (value = getValue(key)) != null) {
			if (properties == null) {
				properties = new HashMap<String, Object>();
			}
			properties.put(IMobileAgent.ALERT_PHONE_NUMBER_KEY, value);
			change.setSendingTime(sendingTime);
			em.merge(change);
		}

		key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.MOBILE_AGENT_NAME_KEY); 
		change = em.find(MobileAgentMemoryChange.class, key);
		if (change != null && isNotSended(change) && (value = getValue(key)) != null) {
			if (properties == null) {
				properties = new HashMap<String, Object>();
			}
			properties.put(IMobileAgent.MOBILE_AGENT_NAME_KEY, value);
			change.setSendingTime(sendingTime);
			em.merge(change);
		}
		
		key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.ALERT_EMAIL_KEY); 
		change = em.find(MobileAgentMemoryChange.class, key);
		if (change != null && isNotSended(change) && (value = getValue(key)) != null) {
			if (properties == null) {
				properties = new HashMap<String, Object>();
			}
			properties.put(IMobileAgent.ALERT_EMAIL_KEY, value);
			change.setSendingTime(sendingTime);
			em.merge(change);
		}
		
		key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.INCOMING_MESSAGE_KEY); 
		change = em.find(MobileAgentMemoryChange.class, key);
		if (change != null && isNotSended(change) && (value = getValue(key)) != null) {
			if (properties == null) {
				properties = new HashMap<String, Object>();
			}
			properties.put(IMobileAgent.INCOMING_MESSAGE_KEY, value);
			change.setSendingTime(sendingTime);
			em.merge(change);
		}

		UserKey userKey = em.find(UserKey.class, userId);
		if (userKey != null) {
			if (properties == null) {
				properties = new HashMap<String, Object>();
			}
			Date expiredTime = userKey.getExpiredTime();
			Date currentTime = new Date(System.currentTimeMillis());
			int minutes = (int) ((expiredTime.getTime() - currentTime.getTime()) / (60 * 1000));
			properties.put(IMobileAgent.USER_KEY_EXPIRED_IN_MINUTES_KEY, minutes);
		}
		
		return properties;
	}

	protected boolean isNotSended(MobileAgentMemoryChange change) {
		Date sendingTime = change.getSendingTime();
		if (sendingTime == null) {
			return true;
		}
		return change.getTime().after(sendingTime);
	}

	private String getValue(MobileAgentMemoryEntryKey key) {
		MobileAgentMemoryEntry entry = em.find(MobileAgentMemoryEntry.class, key);
		if (entry == null) {
			return null;
		}
		return entry.getValue();
	}

	/**
	 * @see UserSessionManager#signUp(IUser, IMobileAgent, String, ISignUpMa, IMobileAgentState, IMobileAgentVersion, IProperties, Date)
	 */
	public ISKey signUp(IUser user, long imei, String mobileAgentName, ISignUpMa signUpMa,
			IMobileAgentState mobileAgentState, IMobileAgentVersion mobileAgentVersion, IProperties properties,
			Date receivedEventTime) {
		// String userName = user.getName();
		// List<?> users =
		// em.createQuery("select e from User e where e.name like :userName")
		// .setParameter("userName", userName)
		// .getResultList();
		// //Check multi users
		// if ( users.size() > 1 ) {
		// return null;
		// }
		// final User realUser;
		// if ( users.isEmpty() ) {
		// realUser = new User();
		// realUser.setName(userName);
		// em.persist(realUser);
		// } else {
		// realUser = (User) users.get(0);
		// }
		final int userId = user.getId();
		if (userId == 0) {
			throw new IllegalStateException("Unknown user!");
		}

		List<?> mobileAgents = em
				.createQuery(
						"select e from MobileAgent e join e.user u where e.imei = :imei and u.id = :userId")
				.setParameter("imei", imei).setParameter("userId", userId)
				.getResultList();
		// Check multi imei's
		if (mobileAgents.size() > 1) {
			return null;
		}
		final MobileAgent realMobileAgent;
		if (mobileAgents.isEmpty()) {
			realMobileAgent = new MobileAgent();
			User existsUser = em.find(User.class, userId);
			realMobileAgent.setUserId(userId);
			realMobileAgent.setUser(existsUser);
			realMobileAgent.setName(mobileAgentName);
			realMobileAgent.setImei(imei);
			em.persist(realMobileAgent);
			em.flush();
		} else {
			realMobileAgent = (MobileAgent) mobileAgents.get(0);
			//realMobileAgent.setName(mobileAgentName);
			realMobileAgent.setDisabled(false);
			em.merge(realMobileAgent);
		}
		final long mobileAgentId = realMobileAgent.getId();
		
		//First sign-up
		if (signUpMa.isFirstSignup()) {
			String firstSignUpMessage = getSharedMemoryForAllUsersValue(Invariants.FIRST_SIGNUP_INCOMING_MESSAGE_KEY);
			putMobileAgentMemoryEntry(mobileAgentId, IMobileAgent.INCOMING_MESSAGE_KEY, firstSignUpMessage);			
		}
		//Change mobile agent name
		putMobileAgentMemoryEntryOnlyIfNull(mobileAgentId, IMobileAgent.MOBILE_AGENT_NAME_KEY, mobileAgentName);			
		
		String alertEmail = realMobileAgent.getUser().getEmail();  
		//Change alert email as registration email
		putMobileAgentMemoryEntryOnlyIfNull(mobileAgentId, IMobileAgent.ALERT_EMAIL_KEY, alertEmail);			
		
		//Make some properties for resend (working if delete mobile application and reinstall).
		makingPropertiesForResend(mobileAgentId);
		
		// Auto sign-in
		final ISignInMaResponse currentSignInMaResponse = signIn(userId, mobileAgentId, properties.getProperties());
		// Generate sign-up event
		internalChangeMobileAgentState(userId, mobileAgentId, mobileAgentState, receivedEventTime);
		//Support old versions
		int versionCode = mobileAgentVersion.getVersionCode();
		if (versionCode < 5) {
			internalChangeMobileAgentVersion(userId, mobileAgentId, mobileAgentVersion);
		}
		
		//Get enabled mobile agents count
		final int mobileAgentCount = getEnabledMobileAgentCount(userId);

		return new ISKey() {
			private final String READ_ONLY_KEY = "Read only key";

			@Override
			public byte[] getValue() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void setValue(byte[] value) {
				throw new IllegalStateException(READ_ONLY_KEY);
			}

			@Override
			public int getUserId() {
				return userId;
			}

			@Override
			public void setUserId(int userId) {
				throw new IllegalStateException(READ_ONLY_KEY);
			}

			@Override
			public long getMobileAgentId() {
				return mobileAgentId;
			}

			@Override
			public void setMobileAgentId(long mobileAgentId) {
				throw new IllegalStateException(READ_ONLY_KEY);
			}

			@Override
			public ISignInMaResponse getCurrentSignInMaResponse() {
				return currentSignInMaResponse;
			}

			@Override
			public void setCurrentSignInMaResponse(
					ISignInMaResponse currentSignInMaResponse) {
				throw new IllegalStateException(READ_ONLY_KEY);
			}

			@Override
			public int getMobileAgentCount() {
				return mobileAgentCount;
			}

			@Override
			public void setMobileAgentCount(int mobileAgentCount) {
				throw new IllegalStateException(READ_ONLY_KEY);
			}

		};
	}

	private int getEnabledMobileAgentCount(int userId) {
		return em
				.createQuery(
						"select e.id from MobileAgent e join e.user u where e.disabled = :disabled and u.id = :userId")
				.setParameter("disabled", false).setParameter("userId", userId)
				.getResultList().size();
	}

	private void makingPropertiesForResend(long mobileAgentId) {
		MobileAgentMemoryEntryKey key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.ALERT_PHONE_NUMBER_KEY); 
		MobileAgentMemoryChange change = em.find(MobileAgentMemoryChange.class, key);
		if (change != null) {
			change.setSendingTime(null);
			em.merge(change);
		}
		key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.MOBILE_AGENT_NAME_KEY); 
		change = em.find(MobileAgentMemoryChange.class, key);
		if (change != null) {
			change.setSendingTime(null);
			em.merge(change);
		}
		key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.ALERT_EMAIL_KEY); 
		change = em.find(MobileAgentMemoryChange.class, key);
		if (change != null) {
			change.setSendingTime(null);
			em.merge(change);
		} else {
			change = new MobileAgentMemoryChange();
			change.setMobileAgentId(mobileAgentId);
			change.setKey(IMobileAgent.ALERT_EMAIL_KEY);
			change.setTime(new Date());
			em.persist(change);
		}
	}

	/**
	 * @see UserSessionManager#sendCommand(IUserCommand userCommand)
	 */
	@Override
	public boolean sendCommand(IUserCommand userCommand) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @see UserSessionManager#getMobileAgent(ISid)
	 */
	@Override
	public IMobileAgent getMobileAgent(ISid sid) {
		// TODO: нормальный идентификатор сессии
		long mobileAgentId = sid.getA();
		List<?> mobileAgents = em
				.createQuery(
						"select e from MobileAgent e where e.id = :mobileAgentId")
				.setParameter("mobileAgentId", mobileAgentId).getResultList();
		if (mobileAgents.isEmpty()) {
			return null;
		}
		return (MobileAgent) mobileAgents.get(0);
	}

	/**
	 * @see UserSessionManager#authentificationByNameAndPassword(String, byte[])
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Override
	public int authentificationByNameAndPassword(String userName, byte[] passwordHash) {
		int userId = findUserByNameOrEmail(userName);
		if (userId == 0) {
			throw new IllegalStateException("User not found");
		}
		Object passwordHashObject = em.createQuery("select  e.passwordHash from User e where e.id = :userId")
		.setParameter("userId", userId)
		.getSingleResult();

		if (passwordHashObject == null) {
			throw new IllegalStateException("User without password");
		}
		
		byte[] drupalPasswordHash = convertToBytes(passwordHashObject);

		//Check if hash generated by mobile agent side
		if (drupalPasswordHash != null && passwordHash != null && Arrays.equals(drupalPasswordHash, passwordHash)) {
			return userId;
		}

		String drupalHash = new String(drupalPasswordHash);
		if (drupalHash.length() != DRUPAL_HASH_LENGTH) {
			throw new IllegalStateException("Not drupal hash");
		}
		String setting = drupalHash.substring(0, 12);
		
		String password = new String(passwordHash);
		
		//Plain password case
		String jbossHash;
		try {
			jbossHash = passwordCrypt("SHA-512", password, setting);
		} catch (Throwable th) {
			throw new RuntimeException(th);
		}
		
		if (!drupalHash.equals(jbossHash)) {
			throw new IllegalStateException("Not valid password");
		}
		
		return userId;
	}
	
	private byte[] convertToBytes(Object blobObject) {
		if (blobObject instanceof SerialBlob) {
			try {
				return ((SerialBlob) blobObject).getBytes(1, DRUPAL_HASH_LENGTH);
			} catch (Throwable th) {
				
			}
		} else if (blobObject instanceof byte[]) {
			return (byte[]) blobObject;
		}
		return null;
	}

	private String passwordCrypt(String algo, String password, String setting) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		//The first 12 characters of an existing hash are its setting string
		String settingShort = setting.substring(0, 12);
		
		if (settingShort.charAt(0) != '$' || settingShort.charAt(2) != '$') {
			return null;
		}
		int countLog2 = passwordGetCountLog2(settingShort);
		//Hashes may be imported from elsewhere, so we allow != DRUPAL_HASH_COUNT
		if (countLog2 < DRUPAL_MIN_HASH_COUNT || countLog2 > DRUPAL_MAX_HASH_COUNT) {
			return null;
		}
		String salt = settingShort.substring(4, 4 + 8);
		//Hashes must have an 8 character salt
		if (salt.length() != 8) {
			return null;
		}
		
		//Convert the base 2 logarithm into an integer
		int count = 1 << countLog2;
		
		byte[] passwordBytes = password.getBytes("UTF-8");
		byte[] saltBytes = salt.getBytes("UTF-8");
		byte[] input = concat(saltBytes, passwordBytes);
		MessageDigest md = MessageDigest.getInstance(algo);
		byte[] hash = md.digest(input);
		do {
			hash = md.digest(concat(hash, passwordBytes));
			//System.out.printf("hash length [%s]", hash.length).println(); 
		} while (--count > 0);
		
		int len = hash.length;
		//nn0Zf89ZW/o36TSgA2cZTdqruwkt4k1GHznXCa1OXHp
		String output = settingShort + passwordBase64Encode(hash, len);
		
		int expected = 12 + (8 * len) / 6 + 1;//+1 for ceil equivalent in drupal
		
		if (output.length() == expected) {
			return output.substring(0, DRUPAL_HASH_LENGTH);
		}
		return null;
	}

	private String passwordBase64Encode(byte[] input, int count) {
		StringBuilder output = new StringBuilder();
		int i = 0;
		do {
			int value = input[i++] & 0xff;
			output.append(PASSWORD_ITOA64.charAt(value & 0x3f));
			if (i < count) {
				value |= (input[i] & 0xff) << 8;
			}
			output.append(PASSWORD_ITOA64.charAt((value >> 6) & 0x3f));
			if (i++ >= count) {
				break;
			}
			if (i < count) {
				value |= (input[i] & 0xff) << 16;
			}
			output.append(PASSWORD_ITOA64.charAt((value >> 12) & 0x3f));
			if (i++ > count) {
				break;
			}
			output.append(PASSWORD_ITOA64.charAt((value >> 18) & 0x3f));
		} while (i < count);
		
		return output.toString();
	}
	
//	private long signedByteToUnsignedLong(byte b) {
//		return b & 0xff;
//	}

	private byte[] concat(byte[] arr1, byte[] arr2) {
		byte[] output = new byte[arr1.length + arr2.length];
		System.arraycopy(arr1, 0, output, 0, arr1.length);
		System.arraycopy(arr2, 0, output, arr1.length, arr2.length);
		return output;
	}

	private int passwordGetCountLog2(String setting) {
		char ch = setting.charAt(3);
		return PASSWORD_ITOA64.indexOf(ch);
	}

	private static final int DRUPAL_MIN_HASH_COUNT = 7;
	private static final int DRUPAL_MAX_HASH_COUNT = 30;
	
	private static final String PASSWORD_ITOA64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	protected int coolUserRegistration(String userName) {
		// Check Available Ids
		int[] ids = {};//coolUserIds();
		if (ids.length == 0) {
			throw new IllegalStateException("Overhead limit for cool users!");
		}
		int userId = ids[0];
		String serviceProfileName = "car-alarm-service";
		// Check service profile name
		List<?> serviceProfiles = em
				.createQuery(
						"select e from ServiceProfile e where e.name like :serviceProfileName")
				.setParameter("serviceProfileName", serviceProfileName)
				.getResultList();
		// User always be registered
		if (serviceProfiles.isEmpty()) {
			throw new IllegalStateException("Service profile "
					+ serviceProfileName + " not exists!");
		}
		ServiceProfile serviceProfile = (ServiceProfile) serviceProfiles.get(0);
		// Register new user for service
		User user = new User();
		user.setName(userName);
		user.addServiceProfile(serviceProfile);
		user.setId(userId);
		em.persist(user);
		em.flush();
		return user.getId();
	}

	protected int findUserByNameOrEmail(String userName) {
		List<?> users = em
				.createQuery("select e from User e where e.name like :userName")
				.setParameter("userName", userName).getResultList();
		// User always be registered
		if (!users.isEmpty()) {
			User user = (User) users.get(0);
			return user.getId();
		}
		users = em
				.createQuery("select e from User e where e.email like :email")
				.setParameter("email", userName).getResultList();
		// User always be registered
		if (!users.isEmpty()) {
			User user = (User) users.get(0);
			return user.getId();
		}
		return 0;
	}

	/**
	 * @see UserSessionManager#existUser(String)
	 */
	@Override
	public boolean existUser(String userName) {
		List<?> users = em
				.createQuery("select e from User e where e.name like :userName")
				.setParameter("userName", userName).getResultList();
		// User always be registered
		// TODO: check on block-user-policy
		return !users.isEmpty();
	}

	/**
	 * @see UserSessionManager#usersCount(String)
	 */
	@Override
	public int usersCount(String serviceProfileName) {
		Number count;
		if (serviceProfileName == null) {
			count = (Number) em.createQuery("select count(e) from User e")
					.getSingleResult();
			return count.intValue();
		}
		count = (Number) em
				.createQuery(
						"select count(u) from User as u join u.serviceProfiles as sp where sp.name like :serviceProfileName")
				.setParameter("serviceProfileName", serviceProfileName)
				.getSingleResult();
		return count.intValue();
	}

	/**
	 * @see UserSessionManager#getServiceProfiles(int)
	 */
	@Override
	public List<IServiceProfile> getServiceProfiles(int userId) {
		List<?> serviceProfiles = em
				.createQuery(
						"select e from User u join u.serviceProfiles e where u.id = :userId")
				.setParameter("userId", userId).getResultList();
		List<IServiceProfile> result = (List<IServiceProfile>) serviceProfiles;
		return result;
	}

	/**
	 * @see UserSessionManager#getServiceProfiles()
	 */
	@Override
	public List<IServiceProfile> getServiceProfiles() {
		List<?> serviceProfiles = em.createQuery(
				"select e from ServiceProfile e").getResultList();
		List<IServiceProfile> result = (List<IServiceProfile>) serviceProfiles;
		return result;
	}

	/**
	 * @see UserSessionManager#getMobileAgentsWithStates(IUser)
	 */
	@Override
	public List<IMobileAgentWithState> getMobileAgentsWithStates(IUser user) {
		long userId = user.getId();
		String userName = user.getName();
		List<?> mobileAgentsWithStates = em
				.createQuery(
						"select e from MobileAgent e join e.user u where e.disabled = false and u.id = :userId or u.name like :userName order by e.name")
				.setParameter("userId", userId)
				.setParameter("userName", userName).getResultList();
		List<IMobileAgentWithState> result = (List<IMobileAgentWithState>) mobileAgentsWithStates;
		return result;
	}

	/**
	 * @see UserSessionManager#getMobileAgentWithState(long)
	 */
	@Override
	public IMobileAgentWithState getMobileAgentWithState(long mobileAgentId) {
		List<?> mobileAgentWithStates = em
				.createQuery(
						"select e from MobileAgent e where e.disabled = false and e.id = :mobileAgentId")
				.setParameter("mobileAgentId", mobileAgentId).getResultList();
		if (mobileAgentWithStates.isEmpty()) {
			return null;
		}
		return (IMobileAgentWithState) mobileAgentWithStates.get(0);
	}

	/**
	 * @see UserSessionManager#getMobileAgent(IUser, String)
	 */
	@Override
	public List<IMobileAgentWithState> getMobileAgentsWithStates(IUser user,
			String mobileAgentName) {
		long userId = user.getId();
		String userName = user.getName();
		String sqlMask = Values.getSqlMask(mobileAgentName);
		final String mobileAgentNameOrSqlMask;
		if (sqlMask != null) {
			mobileAgentNameOrSqlMask = sqlMask;
		} else {
			mobileAgentNameOrSqlMask = mobileAgentName;
		}
		List<?> mobileAgentsWithStates = em
				.createQuery(
						"select e from MobileAgent e join e.user u where e.disabled = false and ( u.id = :userId or u.name like :userName ) and e.name like :mobileAgentName order by e.name")
				.setParameter("userId", userId)
				.setParameter("userName", userName)
				.setParameter("mobileAgentName", mobileAgentNameOrSqlMask)
				.getResultList();
		return (List<IMobileAgentWithState>) mobileAgentsWithStates;
	}

	/**
	 * @see UserSessionManager#registerUser(IUser, String, int)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Override
	public boolean registerUser(IUser user, String email, int referralId) {
		String userName = user.getName();
		List<?> users = em
				.createQuery("select e from User e where e.name like :userName")
				.setParameter("userName", userName).getResultList();
		// User with same name always be registered
		if (!users.isEmpty()) {
			throw new IllegalStateException(
					"User with same name always be registered");
		}
		int userId = user.getId();
		users = em.createQuery("select e from User e where e.id = :userId")
				.setParameter("userId", userId).getResultList();
		// User with same id always be registered
		if (!users.isEmpty()) {
			throw new IllegalStateException(
					"User with same id always be registered");
		}

		// TODO: времянка по auto-assign, определение какими сервисами
		// пользователь желает пользоваться, должно идти из вне, через
		// параметры.
		String serviceProfileName = "car-alarm-service";
		// Check service profile name
		List<?> serviceProfiles = em
				.createQuery(
						"select e from ServiceProfile e where e.name like :serviceProfileName")
				.setParameter("serviceProfileName", serviceProfileName)
				.getResultList();
		// User always be registered
		if (serviceProfiles.isEmpty()) {
			throw new IllegalStateException("Service profile "
					+ serviceProfileName + " not exists!");
		}
		ServiceProfile serviceProfile = (ServiceProfile) serviceProfiles.get(0);

		byte[] passwordHash = user.getPasswordHash();

		// Register new user for service
		User newUser = new User();
		newUser.setId(userId);
		newUser.setName(userName);
		newUser.setPasswordHash(passwordHash);
		newUser.addServiceProfile(serviceProfile);
		newUser.setEmail(email);
		em.persist(newUser);
		em.flush();
		if (referralId != 0) {//Exist user
			updateReferrals(userId, referralId);
		}
		updateLimits(userId);
		return true;
	}

	private void updateLimits(int userId) {
		UserLimit userLimit = em.find(UserLimit.class, userId);
		if (userLimit != null) {
			return;
		}
		userLimit = new UserLimit();
		userLimit.setUserId(userId);
		em.persist(userLimit);
	}

	private void updateReferrals(int userId, int referralId) {
		User referalUser = em.find(User.class, referralId);
		if (referalUser == null) {
			log.warn("REFERAL WITH ID [" + referralId + "] NOT REGISTERED.");
			return;
		}
		UserReferral userReferral = em.find(UserReferral.class, userId);
		if (userReferral != null) {
			userReferral.setReferralUserId(referralId);
			em.merge(userReferral);
		}
		userReferral = new UserReferral();
		userReferral.setUserId(userId);
		userReferral.setReferralUserId(referralId);
		em.persist(userReferral);
	}

	/**
	 * @see UserSessionManager#refreshRegisteredUser(int, String, String, byte[], int)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Override
	public boolean refreshRegisteredUser(int userId, String name, String email, byte[] passwordHash, int referralId) {
		User existUser = em.find(User.class, userId);
		if (existUser == null) {
			return false;
		}
		existUser.setName(name);
		existUser.setEmail(email);
		existUser.setPasswordHash(passwordHash);
		em.merge(existUser);
		em.flush();
		if (referralId != 0) {//Exist user
			updateReferrals(userId, referralId);
		}
		updateLimits(userId);
		return true;
	}

	/**
	 * @see UserSessionManager#coolUserIds()
	 */
	@Override
	public int[] coolUserIds() {
		List<?> unavailableUserIds = em
				.createQuery(
						"select e.id from User e where e.id between :lowBoundId and :highBoundId")
				.setParameter("lowBoundId", 1).setParameter("highBoundId", 35)
				.getResultList();
		Set<Integer> availableIds = new HashSet<Integer>(29);
		for (int i = 2; i < 35; i++) {
			availableIds.add(i);
		}
		for (Object oid : unavailableUserIds) {
			if (oid == null) {
				throw new IllegalStateException(
						"Exist null id for users in database!");
			}
			int id = (Integer) oid;
			availableIds.remove(id);
		}
		int i = 0;
		int[] coolIds = new int[availableIds.size()];
		for (Integer id : availableIds) {
			coolIds[i++] = id;
		}
		return coolIds;
	}

	/**
	 * @see UserSessionManager#getUsers(int, int)
	 */
	@Override
	public List<IUser> getUsers(int lowBoundId, int highBoundId) {
		List<?> users = em
				.createQuery(
						"select e from User e where e.id >= :lowBoundId and e.id <= :highBoundId")
				.setParameter("lowBoundId", lowBoundId)
				.setParameter("highBoundId", highBoundId).getResultList();
		return (List<IUser>) users;
	}

	/**
	 * @see UserSessionManager#getAppStates()
	 */
	@Override
	public List<IAppState> getAppStates() {
		List<?> appStates = em.createQuery("select e from AppState e")
				.getResultList();
		List<IAppState> result = (List<IAppState>) appStates;
		return result;
	}

	/**
	 * @see UserSessionManager#getCarStates()
	 */
	@Override
	public List<ICarState> getCarStates() {
		List<?> carStates = em.createQuery("select e from CarState e")
				.getResultList();
		List<ICarState> result = (List<ICarState>) carStates;
		return result;
	}

	/**
	 * @see UserSessionManager#requestMobileAgentLocation(int, long)
	 */
	@Override
	public ILocation requestMobileAgentLocation(int userId, long mobileAgentId)
			throws Throwable {
		IMobileAgentConnection conn = getConnection(mobileAgentId);
		String addr = conn.getAddr();
		int port = 1975;//conn.getPort();//TODO: check addr on null
		IUserCommand userCommand = makeUserCommand(userId, mobileAgentId,
				"where-are-you");
		try {
			Location location = null;//(Location) UcClientFactory.runCommand(addr, port, userCommand, Location.class);
			if (location == null) {
				throw new IllegalStateException("Invalid requests.");
			}
			return location;
		} catch (Throwable th) {
			throw new RuntimeException(th);
		}
	}

	private IUserCommandType getUserCommandTypeByName(String userCommandTypeName) {
		return (IUserCommandType) em
				.createQuery(
						"select e from UserCommandType e where e.name like :name")
				.setParameter("name", userCommandTypeName).getSingleResult();
	}

	/**
	 * @see UserSessionManager#activateCarAlarm(int, long)
	 */
	@Override
	public boolean activateCarAlarm(int userId, long mobileAgentId) {
		IMobileAgentConnection conn = getConnection(mobileAgentId);
		String addr = conn.getAddr();
		int port = 1975;//conn.getPort();
		IUserCommand userCommand = makeUserCommand(userId, mobileAgentId,
				"activate-car-alarm");
		try {
			//UcClientFactory.sendCommand(addr, port, userCommand);
			return true;
		} catch (Throwable th) {
			log.error("Send command to client failed with exception.", th);
		}
		return false;
	}

	private IUserCommand makeUserCommand(int userId, long mobileAgentId,
			String userCommandName) {
		IUserCommandType userCommandType = getUserCommandTypeByName(userCommandName);//@see db.changelog*
		short commandTypeId = userCommandType.getId();
		IUserCommand userCommand = new UserCommand();
		userCommand.setUserId(userId);
		userCommand.setMobileAgentId(mobileAgentId);
		userCommand.setUserCommandTypeId(commandTypeId);
		return userCommand;
	}

	/**
	 * @see UserSessionManager#switchOffCarAlarm(int, long)
	 */
	@Override
	public boolean switchOffCarAlarm(int userId, long mobileAgentId) {
		IMobileAgentConnection conn = getConnection(mobileAgentId);
		String addr = conn.getAddr();
		int port = 1975;//conn.getPort();
		IUserCommand userCommand = makeUserCommand(userId, mobileAgentId,
				"switch-off-car-alarm");
		try {
			//UcClientFactory.sendCommand(addr, port, userCommand);
			return true;
		} catch (Throwable th) {
			log.error("Send command to client failed with exception.", th);
		}
		return false;
	}

	private IMobileAgentConnection getConnection(long mobileAgentId) {
		IMobileAgentConnection conn = macManager.getConnectionInfo(mobileAgentId);
		if (conn == null) {
			throw new IllegalStateException("Mobile agent disconnected");
		}
		return conn;
	}
	
	/**
	 * @see UserSessionManager#getAlertTracks(int, long, Date, long, int)
	 */
	@Override
	public List<ITrack> getAlertTracks(int userId, long mobileAgentId, Date date, long timeBackInMillis, int maxCount) {
		long timeInMillis = date.getTime();
		Date dateFrom = new Date(timeInMillis - timeBackInMillis);
		//Saved order for reverse
		Map<Long, List<float[]>> generalPolylines = new LinkedHashMap<Long, List<float[]>>();
		Map<Long, List<Long>> generalTimelines = new HashMap<Long, List<Long>>();
		Map<Long, Date> receivedTimes = new HashMap<Long, Date>();
		//Calculate dateFrom for maxCount 
		Date calculatedHorizon = calculateHorizon(userId, mobileAgentId, dateFrom, date, maxCount);
		if (dateFrom.before(calculatedHorizon)) {
			dateFrom = calculatedHorizon;
		}
		List<?> alertTracks = em.createQuery("select e from MobileAgentAlertTrack e where e.mobileAgentId = :mobileAgentId and e.receivedTime between :dateFrom and :dateTo order by e.receivedTime asc")
		.setParameter("mobileAgentId", mobileAgentId)
		.setParameter("dateFrom", dateFrom)
		.setParameter("dateTo", date)
		.getResultList();
		
		if (alertTracks.isEmpty()) {
			return Collections.emptyList();
		}
		
		//Additional memory structures
		Map<Long, Short> generalCarStateIds = new HashMap<Long, Short>();
		Map<Long, Float> prevLats = new HashMap<Long, Float>();
		Map<Long, Float> prevLons = new HashMap<Long, Float>();
		Map<Long, Long> lastTimes = new HashMap<Long, Long>();
		Map<Long, Boolean> hasRealTrack = new HashMap<Long, Boolean>();
		
		//TODO: remove fake tracks if groupByTrackId has one or more real tracks
		
		for (Object t : alertTracks) {
			MobileAgentAlertTrack at = (MobileAgentAlertTrack) t;
			long groupByTrackId = at.getGroupByTrackId();
			List<float[]> generalPolyline = generalPolylines.get(groupByTrackId);
			//Lazy initialization for memory structures
			if (generalPolyline == null) {
				generalPolyline = new ArrayList<float[]>();
				generalPolylines.put(groupByTrackId, generalPolyline);
				generalTimelines.put(groupByTrackId, new ArrayList<Long>());
				prevLats.put(groupByTrackId, 0f);
				prevLons.put(groupByTrackId, 0f);
				lastTimes.put(groupByTrackId, 0L);
				receivedTimes.put(groupByTrackId, at.getReceivedTime());
				//Set initial value
				hasRealTrack.put(groupByTrackId, false);
			}
			//Get car state from last track info
			Short carStateId = generalCarStateIds.get(groupByTrackId);
			if (carStateId == null || knownAndNotAlarm(at)) {
				generalCarStateIds.put(groupByTrackId, at.getCarStateId());
			}
			//Collect time lines
			List<Long> generalTimeline = generalTimelines.get(groupByTrackId);
			float prevLat = prevLats.get(groupByTrackId);
			float prevLon = prevLons.get(groupByTrackId);

			boolean fakeTrackFlag = at.isFakeTrack();
			float[][] pl = at.getPolyline();
			//Switch to real track mode
			if (!fakeTrackFlag && !hasRealTrack.get(groupByTrackId) && hasValidCoordinats(pl)) {
				generalPolyline.clear();
				generalTimeline.clear();
				prevLat = 0f;
				prevLon = 0f;
			}
			//After switching real track mode suppress fake tracks
			if (hasRealTrack.get(groupByTrackId) && fakeTrackFlag) {
				continue;
			}
			
			//Process part of track
			long[] tl = at.getTimestamps();
			for (int i = 0, len = pl.length; i < len; i++) {
				float lat = pl[i][0];
				float lon = pl[i][1];
				//Compression of output data
				if ((prevLat != lat || prevLon != lon)
						&& (lat != ILocation.UNKNOWN_LATITUDE && lon != ILocation.UNKNOWN_LONGITUDE)) {
					generalPolyline.add(pl[i]);
					generalTimeline.add(tl[i]);
					lastTimes.put(groupByTrackId, 0L);
				} else {
					//Set last point marker
					lastTimes.put(groupByTrackId, tl[i]);
				}
				prevLats.put(groupByTrackId, lat);
				prevLons.put(groupByTrackId, lon);
			}
			//Mark if found real track
			if (!fakeTrackFlag) {
				hasRealTrack.put(groupByTrackId, true);
			}
		}
		//Add last point if track is single point
		for (Entry<Long, List<float[]>> e : generalPolylines.entrySet()) {
			long groupByTrackId = e.getKey();
			List<float[]> generalPolyline = e.getValue();
			List<Long> generalTimeline = generalTimelines.get(groupByTrackId);
			float prevLat = prevLats.get(groupByTrackId);
			float prevLon = prevLons.get(groupByTrackId);
			long lastTime = lastTimes.get(groupByTrackId);
			//Last point if prev points removed by compress of output data
			if (lastTime > 0) {
				float[] p = {prevLat, prevLon};
				generalPolyline.add(p);
				generalTimeline.add(lastTime);
			}
		}
		//Build tracks
		List<ITrack> tracks = new ArrayList<ITrack>();
		for (Entry<Long, List<float[]>> e : generalPolylines.entrySet()) {
			long groupByTrackId = e.getKey();
			List<float[]> generalPolyline = e.getValue();
			List<Long> generalTimeline = generalTimelines.get(groupByTrackId);
			short generalCarStateId = generalCarStateIds.get(groupByTrackId);
			Date receivedTime = receivedTimes.get(groupByTrackId);
			
			final int len = generalPolyline.size();
			final float[][] polyline = new float[len][2];
			final long[] timestamps = new long[len];
			final short carStateId = generalCarStateId;
			for (int i = 0; i < len; i++) {
				polyline[i] = generalPolyline.get(i); 
				timestamps[i] = generalTimeline.get(i);
			}
			ITrack track = makeReadOnlyTrack(polyline, timestamps, carStateId, receivedTime, groupByTrackId);
			tracks.add(track);
		}
		if (tracks.size() == 1) {
			return tracks;
		}
		//Reverse order for tracks, more actual - first
		Collections.reverse(tracks);
		return tracks.subList(0, Math.min(maxCount, tracks.size()));
	}

	private boolean hasValidCoordinats(float[][] pl) {
		if (pl == null) {
			return false;
		}
		for (float[] coordinats : pl) {
			if (coordinats == null) {
				continue;
			}
			if (coordinats.length != 2) {
				continue;
			}
			if (coordinats[0] != ILocation.UNKNOWN_LATITUDE && coordinats[1] != ILocation.UNKNOWN_LONGITUDE) {
				return true;
			}
		}
		return false;
	}

	private Date calculateHorizon(int userId, long mobileAgentId,
			Date dateFrom, Date date, int maxCount) {
		List<?> receivedTimes = em.createQuery("select min(e.receivedTime) from MobileAgentAlertTrack e where e.mobileAgentId = :mobileAgentId and e.receivedTime between :dateFrom and :dateTo group by e.groupByTrackId")
				.setParameter("mobileAgentId", mobileAgentId)
				.setParameter("dateFrom", dateFrom)
				.setParameter("dateTo", date)
				.getResultList();
		if (receivedTimes.isEmpty()) {
			return dateFrom;
		}
		List<Date> times = (List<Date>) receivedTimes;
		Collections.sort(times);
		if (times.size() <= maxCount) {
			return times.get(0);
		}
		return times.get(times.size() - maxCount);
	}

	protected boolean knownAndNotAlarm(MobileAgentAlertTrack at) {
		return at.getCarStateId() != getCarStateId("alarm") && at.getCarStateId() != getCarStateId("unknown");
	}
	
	//TODO: remove after test injection of service MASHelper#getCarStateId
	private short getCarStateId(String carStateName) {
		if (carStates == null) {
			//No sense to concurrency optimization feature
			carStates = new HashMap<String, Short>();
		}
		Short carStateId = carStates.get(carStateName);
		if (carStateId != null) {
			return carStateId;
		}
		Object id = em.createQuery("select e.id from CarState e where e.name like :carStateName")
				.setParameter("carStateName", carStateName)
				.getSingleResult();
		carStateId = ((Number) id).shortValue();
		carStates.put(carStateName, carStateId);
		return carStateId;
	}

	private ITrack makeReadOnlyTrack(final float[][] polyline,
			final long[] timestamps, final short carStateId, final Date receivedTime,
			final long groupByTrackId) {
		return new ITrack() {
			private final String READ_ONLY_TRACK = "Read only track";

			@Override
			public float[][] getPolyline() {
				return polyline;
			}

			@Override
			public void setPolyline(float[][] polyline) {
				throw new IllegalStateException(READ_ONLY_TRACK);
			}

			@Override
			public long[] getTimestamps() {
				return timestamps;
			}

			@Override
			public void setTimestamps(long[] timestamps) {
				throw new IllegalStateException(READ_ONLY_TRACK);
			}

			@Override
			public short getCarStateId() {
				return carStateId;
			}

			@Override
			public void setCarStateId(short carStateId) {
				throw new IllegalStateException(READ_ONLY_TRACK);
			}

			@Override
			public Date getReceivedTime() {
				return receivedTime;
			}

			@Override
			public void setReceivedTime(Date receivedTime) {
				throw new IllegalStateException(READ_ONLY_TRACK);
			}

			@Override
			public long getTrackId() {
				return groupByTrackId;
			}

			@Override
			public void setTrackId(long trackId) {
				throw new IllegalStateException(READ_ONLY_TRACK);
			}
			
		};
	}

	/**
	 * @see UserSessionManager#disableMobileAgent(int, long)
	 */
	@Override
	public void disableMobileAgent(int userId, long mobileAgentId) {
		MobileAgent mobileAgent = em.find(MobileAgent.class, mobileAgentId);
		if (mobileAgent == null) {
			throw new IllegalStateException("Mobile agent [" + mobileAgentId + "] not found.");
		}
		mobileAgent.setDisabled(true);
		em.merge(mobileAgent);
	}

	/**
	 * @see UserSessionManager#changeMobileAgentProfile(int, long, String, String, String, String, String, String)
	 */
	@Override
	public void changeMobileAgentProfile(int userId, long mobileAgentId,
			String delayPushAlertEventInSeconds,
			String delayWhereAmIRequestInSeconds, String enableDebugMode, String asdMaxJerk, String asdMaxAcceleration, String asdMaxAngle) {
		MobileAgentProfile mobileAgentProfile = em.find(MobileAgentProfile.class, mobileAgentId);
		if (mobileAgentProfile == null) {
			mobileAgentProfile = new MobileAgentProfile();
			mobileAgentProfile.setMobileAgentId(mobileAgentId);
			em.persist(mobileAgentProfile);
		}
		if (delayPushAlertEventInSeconds != null) {
			try {
				mobileAgentProfile.setDelayPushAlertEventInSeconds(Integer.valueOf(delayPushAlertEventInSeconds));
			} catch (NumberFormatException e) { }
		}
		if (delayWhereAmIRequestInSeconds != null) {
			try {
				mobileAgentProfile.setDelayWhereAmIRequestInSeconds(Integer.valueOf(delayWhereAmIRequestInSeconds));
			} catch (NumberFormatException e) { }
		}
		if (enableDebugMode != null) {
			try {
				mobileAgentProfile.setEnableDebugMode(Boolean.valueOf(enableDebugMode));
			} catch (NumberFormatException e) { }
		}
		if (asdMaxJerk != null) {
			try {
				mobileAgentProfile.setAsdMaxJerk(Float.valueOf(asdMaxJerk));
			} catch (NumberFormatException e) { }
		}
		if (asdMaxAcceleration != null) {
			try {
				mobileAgentProfile.setAsdMaxAcceleration(Float.valueOf(asdMaxAcceleration));
			} catch (NumberFormatException e) { }
		}
		if (asdMaxAngle != null) {
			try {
				mobileAgentProfile.setAsdMaxAngle(Float.valueOf(asdMaxAngle));
			} catch (NumberFormatException e) { }
		}
		em.merge(mobileAgentProfile);
	}
	
	private void internalChangeMobileAgentState(int userId, long mobileAgentId,
			IMobileAgentState mobileAgentState, Date receivedEventTime) {
		try {
			Context ctx = new InitialContext(); 
			UserSessionManager userSessionManager = (UserSessionManager) ctx.lookup("java:app/" + UserSessionManager.class.getSimpleName());
			userSessionManager.changeMobileAgentState(userId, mobileAgentId, mobileAgentState, receivedEventTime);
		} catch (Throwable th) {
			log.error(String.format("Error in change mobile agent [%s] state", mobileAgentId), th);
		}
	}

	private void internalChangeMobileAgentVersion(int userId, long mobileAgentId,
			IMobileAgentVersion mobileAgentVersion) {
		try {
			Context ctx = new InitialContext(); 
			UserSessionManager userSessionManager = (UserSessionManager) ctx.lookup("java:app/" + UserSessionManager.class.getSimpleName());
			userSessionManager.changeMobileAgentVersion(userId, mobileAgentId, mobileAgentVersion);
		} catch (Throwable th) {
			log.error(String.format("Error in change mobile agent [%s] version", mobileAgentId), th);
		}
	}

	
	/**
	 * @see UserSessionManager#changeMobileAgentState(int, long, IMobileAgentState, Date)
	 */
	@Asynchronous
	@Override
	public void changeMobileAgentState(int userId, long mobileAgentId,
			IMobileAgentState mobileAgentState, Date receivedEventTime) {
		try {
			Context ctx = new InitialContext(); 
			EventsManager eventsManager = (EventsManager) ctx.lookup("java:app/" + EventsManager.class.getSimpleName());
			MASHelper helper = (MASHelper) ctx.lookup("java:app/" + MASHelper.class.getSimpleName());
			short eventClassId = helper.getEventClassId("sign-up-mobile-agent");
			EventWithAtach ewa = new EventWithAtach();
			ewa.setEventClassId(eventClassId);
			ewa.setUserId(userId);
			ewa.setMobileAgentId(mobileAgentId);
			ObjectMapper om = new ObjectMapper();
			MobileAgentWithState maws = new MobileAgentWithState();
			maws.setAppStateId(mobileAgentState.getAppStateId());
			maws.setCarStateId(mobileAgentState.getCarStateId());
			maws.setBatteryPct(mobileAgentState.getBatteryPct());
			maws.setBatteryStatus(mobileAgentState.getBatteryStatus());
			maws.setGsmBitErrorRate(mobileAgentState.getGsmBitErrorRate());
			maws.setGsmSignalStrength(mobileAgentState.getGsmSignalStrength());
			maws.setLastLocation(mobileAgentState.getLastLocation());
			maws.setUserId(userId);
			maws.setMobileAgentId(mobileAgentId);
			byte[] content = om.writeValueAsBytes(maws);
			ewa.setContent(content);
			Sid sid = new Sid();
			//TODO: secure issue resolve
			sid.setA(mobileAgentId);
			eventsManager.pusha(sid, ewa, ewa, receivedEventTime);
		} catch (Throwable th) {
			log.error(String.format("Error in change mobile agent [%s] state", mobileAgentId), th);
		}
	}
	
	/**
	 * @see UserSessionManager#changeMobileAgentState(int, long, IMobileAgentState)
	 */
	@Asynchronous
	@Override
	public void changeMobileAgentVersion(int userId, long mobileAgentId,
			IMobileAgentVersion mobileAgentVersion) {
		try {
			//TODO: move below code in UMASTimer
			//Update version for mobile agent
			//Never create only update mobile agent state!
			MobileAgentState state = em.find(MobileAgentState.class, mobileAgentId);
			if (state != null) {
				state.setVersionCode(mobileAgentVersion.getVersionCode());
				state.setVersionName(mobileAgentVersion.getVersionName());
				em.merge(state);
			} else {
				Context ctx = new InitialContext(); 
				MASHelper helper = (MASHelper) ctx.lookup("java:app/" + MASHelper.class.getSimpleName());
				short appStateId = helper.getAppStateId("unknown");
				short carStateId = helper.getAppStateId("unknown");
				
				Date createTime = new Date();
				state = masHelper.makeMobileAgentState(mobileAgentId, createTime);
				state.setAppStateId(appStateId);
				state.setCarStateId(carStateId);
				state.setVersionCode(mobileAgentVersion.getVersionCode());
				state.setVersionName(mobileAgentVersion.getVersionName());
				em.persist(state);
			}
		} catch (Throwable th) {
			log.error(String.format("Error in change mobile agent [%s] version", mobileAgentId), th);
		}
		
	}
		

	/**
	 * @see UserSessionManager#getActiveMobileAgentsCount()
	 */
	@Override
	public int getActiveMobileAgentsCount() {
		int result = ((Number) em.createQuery("select count(e) from MobileAgent e where e.disabled = false")
		.getSingleResult()).intValue();
		return result;
	}

	/**
	 * @see UserSessionManager#updateMobileAgent(int, long, String, String, String)
	 */
	@Override
	public boolean updateMobileAgent(int userId, long mobileAgentId, String name,
			String alertPhoneNumber, String alertEmail) {
		MobileAgent mobileAgent = em.find(MobileAgent.class, mobileAgentId);
		if (mobileAgent == null) {
			return false;
		}
		if (mobileAgent.getUserId() != userId) {
			return false;
		}
		
		if (name != null && !name.trim().isEmpty()) {
			mobileAgent.setName(name);
			em.merge(mobileAgent);
			String key = IMobileAgent.MOBILE_AGENT_NAME_KEY;
			putMobileAgentMemoryEntry(mobileAgentId, key, name);
		}
		if (alertEmail != null) {
			String key = IMobileAgent.ALERT_EMAIL_KEY;
			putMobileAgentMemoryEntry(mobileAgentId, key, alertEmail);
		}
		if (alertPhoneNumber != null) {
			String key = IMobileAgent.ALERT_PHONE_NUMBER_KEY;
			putMobileAgentMemoryEntry(mobileAgentId, key, alertPhoneNumber);
		}
		return true;
	}
	
	private void putMobileAgentMemoryEntryOnly(long mobileAgentId, String key,
			String value) {
		MobileAgentMemoryEntryKey mobileAgentMemoryEntryKey = new MobileAgentMemoryEntryKey(mobileAgentId, key);
		MobileAgentMemoryEntry mobileAgentMemoryEntry = em.find(MobileAgentMemoryEntry.class, mobileAgentMemoryEntryKey);
		if (mobileAgentMemoryEntry == null) {
			mobileAgentMemoryEntry = new MobileAgentMemoryEntry();
			mobileAgentMemoryEntry.setMobileAgentId(mobileAgentId);
			mobileAgentMemoryEntry.setKey(key);
			mobileAgentMemoryEntry.setValue(value);
			em.persist(mobileAgentMemoryEntry);
		} else {
			mobileAgentMemoryEntry.setValue(value);
			em.merge(mobileAgentMemoryEntry);				
		}
	}

	private void putMobileAgentMemoryEntryOnlyIfNull(long mobileAgentId, String key,
			String value) {
		MobileAgentMemoryEntryKey mobileAgentMemoryEntryKey = new MobileAgentMemoryEntryKey(mobileAgentId, key);
		MobileAgentMemoryEntry mobileAgentMemoryEntry = em.find(MobileAgentMemoryEntry.class, mobileAgentMemoryEntryKey);
		if (mobileAgentMemoryEntry == null) {
			mobileAgentMemoryEntry = new MobileAgentMemoryEntry();
			mobileAgentMemoryEntry.setMobileAgentId(mobileAgentId);
			mobileAgentMemoryEntry.setKey(key);
			mobileAgentMemoryEntry.setValue(value);
			em.persist(mobileAgentMemoryEntry);
		}
	}

	protected void putMobileAgentMemoryEntry(long mobileAgentId, String key,
			String value) {
		Date currentTime = new Date(System.currentTimeMillis());
		putMobileAgentMemoryEntryOnly(mobileAgentId, key, value);
		MobileAgentMemoryEntryKey mobileAgentMemoryEntryKey = new MobileAgentMemoryEntryKey(mobileAgentId, key);
		MobileAgentMemoryChange mobileAgentMemoryChange = em.find(MobileAgentMemoryChange.class, mobileAgentMemoryEntryKey);
		if (mobileAgentMemoryChange == null) {
			mobileAgentMemoryChange = new MobileAgentMemoryChange();
			mobileAgentMemoryChange.setMobileAgentId(mobileAgentId);
			mobileAgentMemoryChange.setKey(key);
			mobileAgentMemoryChange.setTime(currentTime);
			em.persist(mobileAgentMemoryChange);
		} else {
			mobileAgentMemoryChange.setTime(currentTime);
			em.merge(mobileAgentMemoryChange);				
		}
	}

	protected void putMobileAgentMemoryEntryWithResetLastChanges(long mobileAgentId, String key,
			String value) {
		putMobileAgentMemoryEntryOnly(mobileAgentId, key, value);
		MobileAgentMemoryEntryKey mobileAgentMemoryEntryKey = new MobileAgentMemoryEntryKey(mobileAgentId, key);
		MobileAgentMemoryChange mobileAgentMemoryChange = em.find(MobileAgentMemoryChange.class, mobileAgentMemoryEntryKey);
		if (mobileAgentMemoryChange != null) {
			Date sendingTime = mobileAgentMemoryChange.getSendingTime();
			if (sendingTime == null) {
				sendingTime = new Date(System.currentTimeMillis());
				mobileAgentMemoryChange.setSendingTime(sendingTime);
			}
			mobileAgentMemoryChange.setTime(sendingTime);
			em.merge(mobileAgentMemoryChange);				
		}
	}

	/**
	 * @see UserSessionManager#changeIncommingMessage(int, long, String)
	 */
	@Override
	public void changeIncommingMessage(int userId, long mobileAgentId,
			String message) {
		String key = IMobileAgent.INCOMING_MESSAGE_KEY;
		putMobileAgentMemoryEntry(mobileAgentId, key, message);
	}

	/**
	 * @see UserSessionManager#getMobileAgentsProperties(int)
	 */
	@Override
	public List<Map<String, Object>> getMobileAgentsProperties(int userId) {
		// TODO throw Runtime exception if user not found
		List<?> mobileAgentIds = em.createQuery("select e.id from MobileAgent e join e.user u where u.id = :userId")
		.setParameter("userId", userId)
		.getResultList();
		if (mobileAgentIds.isEmpty()) {
			return Collections.emptyList();
		}
		
		List<Map<String, Object>> result = Collections.emptyList();
		for (Object mobileAgentIdValue : mobileAgentIds) {
			long mobileAgentId = ((Number) mobileAgentIdValue).longValue();
			Map<String, Object> mobileAgentProperties = getMobileAgentProperties(userId, mobileAgentId);
			if (mobileAgentProperties.isEmpty()) {
				continue;
			}
			if (result.isEmpty()) {
				result = new ArrayList<Map<String, Object>>();
			}
			result.add(mobileAgentProperties);
		}
		return result;
	}

	/**
	 * @see UserSessionManager#getMobileAgentProperties(int, long)
	 */
	@Override
	public Map<String, Object> getMobileAgentProperties(int userId,
			long mobileAgentId) {
		// TODO throw Runtime exception if user not found
		Map<String, Object> properties = new HashMap<String, Object>();
		String value;
		MobileAgentMemoryEntryKey key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.ALERT_PHONE_NUMBER_KEY); 
		if ((value = getValue(key)) != null) {
			properties.put(IMobileAgent.ALERT_PHONE_NUMBER_KEY, value);
		} else {
			properties.put(IMobileAgent.ALERT_PHONE_NUMBER_KEY, "");
		}
		key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.ALERT_EMAIL_KEY); 
		if ((value = getValue(key)) != null) {
			properties.put(IMobileAgent.ALERT_EMAIL_KEY, value);
		}
		key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.MOBILE_AGENT_NAME_KEY); 
		if ((value = getValue(key)) != null) {
			properties.put(IMobileAgent.MOBILE_AGENT_NAME_KEY, value);
		}
		MobileAgentState mobileAgentState = em.find(MobileAgentState.class, mobileAgentId);
		String versionName;
		//Version of mobile agent
		if (mobileAgentState != null && (versionName = mobileAgentState.getVersionName()) != null) {
			//Version extraction
			Matcher matcher = Invariants.VERSION_NAME_PATTERN.matcher(versionName);
			if (matcher.matches()) {
				String ver = matcher.group(1);
				properties.put(IMobileAgent.MOBILE_AGENT_VERSION_NAME_KEY, ver);
			}
		}
		SharedMemoryForAllUsersEntry sharedMemoryForAllUsersEntry = em.find(SharedMemoryForAllUsersEntry.class, Invariants.CURRENT_ANDROID_APPLICATION_VERSION_KEY);
		if (sharedMemoryForAllUsersEntry != null) {
			String currentVersionName = sharedMemoryForAllUsersEntry.getValue();
			properties.put(IMobileAgent.CURRENT_MOBILE_AGENT_VERSION_KEY, currentVersionName);
		}
		
		MobileAgentProfile mobileAgentProfile = em.find(MobileAgentProfile.class, mobileAgentId);
		//Mobile agent profile
		if (mobileAgentProfile != null) {
			boolean enableDebugMode = mobileAgentProfile.isEnableDebugMode();			
			properties.put(IMobileAgentProfile.ENABLE_DEBUG_MODE, enableDebugMode);
			float asdMaxJerk = mobileAgentProfile.getAsdMaxJerk();
			properties.put(IMobileAgentProfile.ASD_MAX_JERK, asdMaxJerk);
			float asdMaxAcceleration = mobileAgentProfile.getAsdMaxAcceleration();
			properties.put(IMobileAgentProfile.ASD_MAX_ACCELERATION, asdMaxAcceleration);
			float asdMaxAngle = mobileAgentProfile.getAsdMaxAngle();
			properties.put(IMobileAgentProfile.ASD_MAX_ANGLE, asdMaxAngle);
		} else {
			properties.put(IMobileAgentProfile.ENABLE_DEBUG_MODE, IMobileAgentProfile.DEFAULT_ENABLE_DEBUG_MODE);
			properties.put(IMobileAgentProfile.ASD_MAX_JERK, IMobileAgentProfile.DEFAULT_ASD_MAX_JERK);
			properties.put(IMobileAgentProfile.ASD_MAX_ACCELERATION, IMobileAgentProfile.DEFAULT_ASD_MAX_ACCELERATION);
			properties.put(IMobileAgentProfile.ASD_MAX_ANGLE, IMobileAgentProfile.DEFAULT_ASD_MAX_ANGLE);
		}
		//TODO: request service to get optimal settings
		properties.put(IMobileAgentProfile.OPTIMAL_ASD_MAX_JERK, IMobileAgentProfile.DEFAULT_ASD_MAX_JERK);
		properties.put(IMobileAgentProfile.OPTIMAL_ASD_MAX_ACCELERATION, IMobileAgentProfile.DEFAULT_ASD_MAX_ACCELERATION);
		properties.put(IMobileAgentProfile.OPTIMAL_ASD_MAX_ANGLE, IMobileAgentProfile.DEFAULT_ASD_MAX_ANGLE);
		
		if (properties.isEmpty()) {
			return properties;
		}
		properties.put(IMobileAgent.MOBILE_AGENT_ID_KEY, mobileAgentId);
		return properties;
	}

	/**
	 * @see UserSessionManager#changeSharedMemoryForAllUsersEntry(String, String)
	 */
	@Override
	public void changeSharedMemoryForAllUsersEntry(String key, String value) {
		SharedMemoryForAllUsersEntry sharedMemoryForAllUsersEntry = em.find(SharedMemoryForAllUsersEntry.class, key);
		if (sharedMemoryForAllUsersEntry == null) {
			sharedMemoryForAllUsersEntry = new SharedMemoryForAllUsersEntry();
			sharedMemoryForAllUsersEntry.setKey(key);
			sharedMemoryForAllUsersEntry.setValue(value);
			em.persist(sharedMemoryForAllUsersEntry);
			return;
		}
		sharedMemoryForAllUsersEntry.setValue(value);
		em.merge(sharedMemoryForAllUsersEntry);
	}
	
	protected String getSharedMemoryForAllUsersValue(String key) {
		SharedMemoryForAllUsersEntry sharedMemoryForAllUsersEntry = em.find(SharedMemoryForAllUsersEntry.class, key);
		if (sharedMemoryForAllUsersEntry == null) {
			return null;
		}
		return sharedMemoryForAllUsersEntry.getValue();
	}

	/**
	 * @see UserSessionManager#signIn(int, long, Map)
	 */
	@Override
	public ISignInMaResponse signIn(int userId, long mobileAgentId,
			Map<String, Object> properties) {
		if (userId == 0) {
			throw new IllegalStateException("Unknown user!");
		}
		if (mobileAgentId == 0) {
			throw new IllegalStateException("Unknown mobile agent!");
		}
		
		List<?> mobileAgents = em
				.createQuery(
						"select e from MobileAgent e join e.user u where e.id = :mobileAgentId and u.id = :userId")
				.setParameter("mobileAgentId", mobileAgentId).setParameter("userId", userId)
				.getResultList();
		// Check multi imei's
		if (mobileAgents.isEmpty()) {
			return null;
		}
		//Change properties
		if (properties == null) {
			return signIn(userId, mobileAgentId);
		}
		//Changed properties
		for (Map.Entry<String, Object> e : properties.entrySet()) {
			String key1 = e.getKey();
			Object value = e.getValue();
			if (value == null) {
				continue;
			}
			//TODO: Quick filter invalid keys
			//Changes on mobile agent more priority then changes from drupal 
			String name = String.valueOf(value);
			if (IMobileAgent.ALERT_PHONE_NUMBER_KEY.equals(key1)) {
				putMobileAgentMemoryEntryWithResetLastChanges(mobileAgentId, key1, name);
			} else if (IMobileAgent.MOBILE_AGENT_NAME_KEY.equals(key1)) {
				putMobileAgentMemoryEntryWithResetLastChanges(mobileAgentId, key1, name);
				//TODO: remove after refactoring
				MobileAgent mobileAgent = (MobileAgent) mobileAgents.get(0); 
				mobileAgent.setName(name);
				em.merge(mobileAgent);
			} else if (IMobileAgent.ALERT_EMAIL_KEY.equals(key1)) {
				putMobileAgentMemoryEntryWithResetLastChanges(mobileAgentId, key1, name);
			} else if (IMobileAgentProfile.ASD_MAX_ACCELERATION.equals(key1)) {
				//TODO: remove after refactoring, try to use direct service mobile-agent-profile
				MobileAgentProfile mobileAgentProfile = em.find(MobileAgentProfile.class, mobileAgentId);
				Float floatValue = getFloatValue(value);
				if (floatValue == null) {
					continue;
				}
				if (mobileAgentProfile == null) {
					mobileAgentProfile = makeMobileAgentProfile(mobileAgentId);
				}
				mobileAgentProfile.setAsdMaxAcceleration(floatValue);
				em.merge(mobileAgentProfile);
			} else if (IMobileAgentProfile.ASD_MAX_ANGLE.equals(key1)) {
				//TODO: remove after refactoring, try to use direct service mobile-agent-profile
				MobileAgentProfile mobileAgentProfile = em.find(MobileAgentProfile.class, mobileAgentId);
				Float floatValue = getFloatValue(value);
				if (floatValue == null) {
					continue;
				}
				if (mobileAgentProfile == null) {
					mobileAgentProfile = makeMobileAgentProfile(mobileAgentId);
				}
				mobileAgentProfile.setAsdMaxAngle(floatValue);
				em.merge(mobileAgentProfile);
			} else if (IMobileAgentProfile.ENABLE_ENERGY_SAVING_MODE.equals(key1)) {
				//TODO: remove after refactoring, try to use direct service mobile-agent-profile
				MobileAgentProfile mobileAgentProfile = em.find(MobileAgentProfile.class, mobileAgentId);
				Boolean booleanValue = getBooleanValue(value);
				if (booleanValue == null) {
					continue;
				}
				if (mobileAgentProfile == null) {
					mobileAgentProfile = makeMobileAgentProfile(mobileAgentId);
				}
				mobileAgentProfile.setEnableEnergySavingMode(booleanValue);
				em.merge(mobileAgentProfile);
			}
		}
		//Version name and version code
		Object versionCode = properties.get(IMobileAgent.MOBILE_AGENT_VERSION_CODE_KEY);
		Object versionName = properties.get(IMobileAgent.MOBILE_AGENT_VERSION_NAME_KEY);
		if (versionCode != null && versionName != null && versionCode instanceof Number) {
			IMobileAgentVersion mobileAgentVersion = new MobileAgentVersion((Number) versionCode, String.valueOf(versionName));
			internalChangeMobileAgentVersion(userId, mobileAgentId, mobileAgentVersion);
		}
		
		return signIn(userId, mobileAgentId);
	}

	private MobileAgentProfile makeMobileAgentProfile(long mobileAgentId) {
		MobileAgentProfile mobileAgentProfile = new MobileAgentProfile();
		mobileAgentProfile.setMobileAgentId(mobileAgentId);
		em.persist(mobileAgentProfile);
		return mobileAgentProfile;
	}

	private Boolean getBooleanValue(Object value) {
		if (value == null) {
			return null;
		}
		if (value instanceof Boolean) {
			return (Boolean) value;
		}
		if (value instanceof String) {
			return Boolean.valueOf((String) value);
		}
		return null;
	}

	private Float getFloatValue(Object value) {
		if (value == null) {
			return null;
		}
		if (value instanceof Number) {
			return ((Number) value).floatValue();
		}
		if (value instanceof String) {
			try {
				String stringValue = (String) value;
				return Float.valueOf(stringValue.trim());
			} catch (NumberFormatException e) {
				return null;
			}
		}
		return null;
	}

	/**
	 * @see UserSessionManager#getMobileAgentProperties(List)
	 */
	@Override
	public Map<Integer, Boolean> getTimeLimitedVersion(List<Integer> userIds) {
		List<?> queryResult = em.createQuery("select ul.userId, ul.timeLimited from UserLimit ul where ul.userId in (:userIds)")
			.setParameter("userIds", userIds)
			.getResultList();
		if (queryResult.isEmpty()) {
			return Collections.emptyMap();
		}
		Map<Integer, Boolean> timeLimitedVersion = new HashMap<Integer, Boolean>();
		for ( Object o : queryResult) {
			Object[] a = (Object[]) o;
			Integer key = (Integer) a[0];
			Boolean value = (Boolean) a[1];
			timeLimitedVersion.put(key, value);
		}
		return timeLimitedVersion;
	}

	/**
	 * @see UserSessionManager#getUsedAsReferral(List)
	 */
	@Override
	public Map<Integer, Boolean> getUsedAsReferral(List<Integer> userIds) {
			List<?> queryResult = em.createQuery("select ul.userId, ul.usedAsReferral from UserLimit ul where ul.userId in (:userIds)")
				.setParameter("userIds", userIds)
				.getResultList();
			if (queryResult.isEmpty()) {
				return Collections.emptyMap();
			}
			Map<Integer, Boolean> timeLimitedVersion = new HashMap<Integer, Boolean>();
			for ( Object o : queryResult) {
				Object[] a = (Object[]) o;
				Integer key = (Integer) a[0];
				Boolean value = (Boolean) a[1];
				timeLimitedVersion.put(key, value);
			}
			return timeLimitedVersion;
	}

	/**
	 * @see UserSessionManager#getUserLimit(int)
	 */
	@Override
	public IUserLimit getUserLimit(int userId) {
		if (userId == 0) {
			throw new IllegalStateException("Unknown user!");
		}
		UserLimit userLimit = em.find(UserLimit.class, userId);
		return userLimit;
	}

	/**
	 * @see UserSessionManager#ping(long)
	 */
	@Override
	public void ping(long mobileAgentId) {
		MobileAgentPing mobileAgentPing = em.find(MobileAgentPing.class, mobileAgentId);
		if (mobileAgentPing == null) {
			//New entity mobile-agent-ping
			mobileAgentPing = new MobileAgentPing();
			mobileAgentPing.setMobileAgentId(mobileAgentId);
			mobileAgentPing.setTime(new Date());
			em.persist(mobileAgentPing);
			return;
		}
		mobileAgentPing.setTime(new Date());
		em.merge(mobileAgentPing);
	}

	/**
	 * @see UserSessionManager#pubkeyUpdate(long, byte[])
	 */
	@Override
	public void pubkeyUpdate(long mobileAgentId, byte[] pubkey) {
		MobileAgentPubkey mobileAgentPubkey = em.find(MobileAgentPubkey.class, mobileAgentId);
		if (mobileAgentPubkey == null) {
			//New entity mobile-agent-pubkey
			mobileAgentPubkey = new MobileAgentPubkey();
			mobileAgentPubkey.setMobileAgentId(mobileAgentId);
			mobileAgentPubkey.setPubkey(pubkey);
			mobileAgentPubkey.setTime(new Date());
			em.persist(mobileAgentPubkey);
			return;
		}
		mobileAgentPubkey.setPubkey(pubkey);
		mobileAgentPubkey.setTime(new Date());
		em.merge(mobileAgentPubkey);
	}

	/**
	 * @see UserSessionManager#getPubkey(long)
	 */
	@Override
	public byte[] getPubkey(long mobileAgentId) {
		MobileAgentPubkey mobileAgentPubkey = em.find(MobileAgentPubkey.class, mobileAgentId);
		if (mobileAgentPubkey == null) {
			return null;
		}
		return mobileAgentPubkey.getPubkey();
	}

	/**
	 * @see UserSessionManager#getMyMobileAgents(long)
	 */
	@Override
	public List<String> getMyMobileAgents(long mobileAgentId) {
		MobileAgent mobileAgent = em.find(MobileAgent.class, mobileAgentId);
		if (mobileAgent == null) {
			return Collections.emptyList();
		}
		int userId = mobileAgent.getUserId();
		List<?> objects = em.createQuery("select e.id, e.name from MobileAgent e where e.userId = :userId and e.disabled = false")
		.setParameter("userId", userId)
		.getResultList();
		if (objects.isEmpty()) {
			return Collections.emptyList();
		}
		List<String> result = new ArrayList<String>();
		for (Object o : objects) {
			Object[] args = (Object[]) o;
			String name = String.valueOf(args[1]);
			long mid = ((Number) args[0]).longValue();
			String base64Bytes = EncDecTools.encodeMobileAgentId(mid, ISid.SALTSS);
			StringBuilder mobileAgentEntry = new StringBuilder();
			mobileAgentEntry.append(new String(base64Bytes));
			mobileAgentEntry.append('\t');
			mobileAgentEntry.append(name);
			result.add(mobileAgentEntry.toString());
		}
		return result;
	}

	/**
	 * @see UserSessionManager#getHost(long)
	 */
	@Override
	public String getHost(long mobileAgentId) {
		MobileAgentConnection mac = em.find(MobileAgentConnection.class, mobileAgentId);
		if (mac == null) {
			return null;
		}
		return mac.getAddr();
	}

	/**
	 * @see UserSessionManager#changeMobileAgentSettings(long, Map)
	 */
	@Override
	public boolean changeMobileAgentSettings(long mobileAgentId,
			Map<Object, Object> settings) {
		MobileAgentProfile mobileAgentProfile = em.find(MobileAgentProfile.class, mobileAgentId);
		if (mobileAgentProfile == null) {
			return false;
		}
		boolean success = false;
		Object controlModeId = settings.get(IMobileAgentProfile.CONTROL_MODE_ID);
		//TODO: more properties and more safety
		if (controlModeId != null) {
			short controlModeIdNum = Short.parseShort(String.valueOf(controlModeId)); 
			mobileAgentProfile.setControlModeId(controlModeIdNum);
			em.merge(mobileAgentProfile);
			success = true;
		}
		Object alertPhoneNumber = settings.get(IMobileAgent.ALERT_PHONE_NUMBER_KEY);
		if (alertPhoneNumber != null) {
			//TODO: remove this security leak
			MobileAgentMemoryEntryKey key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.ALERT_PHONE_NUMBER_KEY);
			MobileAgentMemoryEntry entry = em.find(MobileAgentMemoryEntry.class, key);
			if (entry == null) {
				entry = new MobileAgentMemoryEntry();
				entry.setKey(IMobileAgent.ALERT_PHONE_NUMBER_KEY);
				entry.setMobileAgentId(mobileAgentId);
				entry.setValue(String.valueOf(alertPhoneNumber));
				em.persist(entry);
			} else {
				entry.setValue(String.valueOf(alertPhoneNumber));
				em.merge(entry);
			}

			MobileAgentMemoryChange change = em.find(MobileAgentMemoryChange.class, key);
			if (change != null) {
				change.setTime(new Date());
				change.setSendingTime(null);
				em.merge(change);
			}
		}
		
		return success;
	}

	/**
	 * @see UserSessionManager#getMobileAgentSettings(long)
	 */
	@Override
	public Properties getMobileAgentSettings(long mobileAgentId) {
		MobileAgentProfile mobileAgentProfile = em.find(MobileAgentProfile.class, mobileAgentId);
		if (mobileAgentProfile == null) {
			return null;
		}
		Properties properties = new Properties();
		properties.put(IMobileAgentProfile.CONTROL_MODE_ID, String.valueOf(mobileAgentProfile.getControlModeId()));
		//TODO: remove this security leak
		MobileAgentMemoryEntryKey key = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.ALERT_PHONE_NUMBER_KEY); 
		MobileAgentMemoryEntry entry = em.find(MobileAgentMemoryEntry.class, key);
		if (entry != null) {
			properties.put(IMobileAgent.ALERT_PHONE_NUMBER_KEY, entry.getValue());
		}
		//Get map provider
		
		//Get version name (full name include square brackets) 
		
		//Get user limits (is-time-limited)
		
		return properties;
	}

	/**
	 * @see UserSessionManager#checkEmail(String) 
	 */
	@Override
	public boolean checkEmail(String email) {
		List result = em.createQuery("select u.id from User u where u.email like :email").setParameter("email", email).getResultList();
		return !result.isEmpty();
	}

	/**
	 * @see UserSessionManager#getEncodedPasswordHash(String) 
	 */
	@Override
	public byte[] getEncodedPasswordHash(String email) {
		List result = em.createQuery("select u.passwordHash from User u where u.email like :email").setParameter("email", email).getResultList();
		if (result.isEmpty()) {
			return null;
		}
		byte[] drupalPasswordHash = convertToBytes(result.get(0));
		if (drupalPasswordHash == null) {
			return null;
		}
		return EncDecTools.encodePasswordHash(drupalPasswordHash);
	}

}

class MobileAgentVersion implements IMobileAgentVersion {
	private static final String READONLY_MOBILE_AGENT_VERSION = "This mobile agent version is read only";
	
	private final int versionCode;
	private final String versionName;
	
	public MobileAgentVersion(Number versionCode, String versionName) {
		this.versionCode = versionCode.intValue();
		this.versionName = versionName;
	}

	@Override
	public int getVersionCode() {
		return versionCode;
	}

	@Override
	public void setVersionCode(int versionCode) {
		throw new IllegalStateException(READONLY_MOBILE_AGENT_VERSION);
	}

	@Override
	public String getVersionName() {
		return versionName;
	}

	@Override
	public void setVersionName(String versionName) {
		throw new IllegalStateException(READONLY_MOBILE_AGENT_VERSION);
	}
	
}