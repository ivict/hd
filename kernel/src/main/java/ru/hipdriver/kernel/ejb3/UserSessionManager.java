/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import ru.hipdriver.i.IAppState;
import ru.hipdriver.i.ICarState;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.i.IMobileAgentVersion;
import ru.hipdriver.i.IMobileAgentWithState;
import ru.hipdriver.i.IProperties;
import ru.hipdriver.i.ISKey;
import ru.hipdriver.i.IServiceProfile;
import ru.hipdriver.i.ISid;
import ru.hipdriver.i.ISignInMaResponse;
import ru.hipdriver.i.ISignUpMa;
import ru.hipdriver.i.ITrack;
import ru.hipdriver.i.IUser;
import ru.hipdriver.i.IUserCommand;
import ru.hipdriver.i.IUserLimit;

/**
 * Управление пользовательскими сессиями.<br>
 *  
 * @author ivict
 */
public interface UserSessionManager {
	
	/**
	 * Регистрация мобильного агента.
	 * После регистрации флаг 'disabled', всегда сбрасывается.
	 * @param user пользователь
	 * @param imei уникальный идентификационный номер телефона
	 * @param mobileAgentName имя мобильного агента для интерфейса пользователя.
	 * @param signUpMa TODO
	 * @param mobileAgentState состояние мобильного агента на момент авторизации.
	 * @param mobileAgentVersion TODO
	 * @param properties TODO
	 * @param receivedEventTime TODO
	 * @return публичная часть ключа и ответ на первый sign-in.
	 */
	ISKey signUp(IUser user, long imei, String mobileAgentName, ISignUpMa signUpMa, IMobileAgentState mobileAgentState, IMobileAgentVersion mobileAgentVersion, IProperties properties, Date receivedEventTime);
	
	/**
	 * Аутентификация пользователя по имени и паролю
	 * @param userName
	 * @param passwordHash 
	 * @return
	 */
	int authentificationByNameAndPassword(String userName, byte[] passwordHash);
	
	boolean sendCommand(IUserCommand userCommand);
	
	/**
	 * Возвращает мобильного агента сессии, если сессия invalid
	 * возращаем null.
	 * @param sid сессионный идентификатор.
	 * @return null, если сесcия invalid.
	 */
	IMobileAgent getMobileAgent(ISid sid);

	/**
	 * Проверяет существование пользователя с данным именемю
	 * @param userName имя пользователя
	 * @return false, если пользователя нет или он заблокирован.
	 */
	boolean existUser(String userName);
	
	/**
	 * Возвращает количество пользователей сервиса.
	 * @param serviceProfileName вид сервиса
	 * @return положительное целое число
	 */
	int usersCount(String serviceProfileName);

	/**
	 * Возвращает список профилей сервисов, пользователя.
	 * @return всегда содержит хотя бы один элемент.
	 */
	List<IServiceProfile> getServiceProfiles(int userId);

	/**
	 * Возвращает список всех профилей сервисов.
	 * @return всегда содержит хотя бы один елемент.
	 */
	List<IServiceProfile> getServiceProfiles();

	/**
	 * Возвращает список мобильных агентов пользователя c состоянием.
	 * В список включаются все агенты у которых не выставлен флаг 'disabled'.
	 * @param user содержит хотя бы одно заполненное поле либо name либо id.
	 * @return всегда список (т.е. not null).
	 */
	List<IMobileAgentWithState> getMobileAgentsWithStates(IUser user);

	/**
	 * Возвращает мобильного агента с состоянием по id.
	 * @return null если нет такого или 'disabled'.
	 */
	IMobileAgentWithState getMobileAgentWithState(long mobileAgentId);

	/**
	 * Возвращает список мобильных агентов с состоянием по имени для пользователя.
	 * В список включаются все агенты у которых не выставлен флаг 'disabled'.
	 * В строке mobileAgentName допустима маска *.
	 * @return null если нет такого.
	 */
	List<IMobileAgentWithState> getMobileAgentsWithStates(IUser user, String mobileAgentName);

	/**
	 * Регистрация нового пользователя.
	 * Если id > 0, пытаемся зарегистрировать именно с подобным id.
	 * (Требования к интеграции с drupal)
	 * @param email адрес электронной почты указанный при регистрации
	 * @param referralId пользователь по рекомендации которого произошла регистрация.
	 * @return false, если пользователь с id или именем уже существует в базе.
	 */
	boolean registerUser(IUser user, String email, int referralId);
	
	/**
	 * Обновление регистрации пользователя.
	 * Если пользователя нет, то ничего не делаем.
	 * @param name TODO
	 * @param email адрес электронной почты указанный при регистрации
	 * @param passwordHash TODO
	 * @param referralId пользователь по рекомендации которого произошла регистрация.
	 * @return false, если пользователь не найден (поиск по userId).
	 */
	boolean refreshRegisteredUser(int userId, String name, String email, byte[] passwordHash, int referralId);
	
	/**
	 * Возвращает список не занятых id кошерных пользователей,
	 * т.е. пользователей, для которых не требуется аутентификауия в drupal.
	 * @return всегда массив, если все id заняты возвращает пустой массив.
	 */
	int[] coolUserIds();

	/**
	 * Возвращает список пользователей для id в диапазоне.
	 * @param lowBoundId нижняя граница включительно.
	 * @param highBoundId верхняя граница включительно.
	 * @return пустой список если пользователей с id в диапазоне нет.
	 */
	List<IUser> getUsers(int lowBoundId, int highBoundId);

	/**
	 * Возвращает список всех состояний мобильного приложения.
	 * @return всегда содержит хотя бы один елемент.
	 */
	List<IAppState> getAppStates();

	/**
	 * Возвращает список всех состояний машины.
	 * @return всегда содержит хотя бы один елемент.
	 */
	List<ICarState> getCarStates();

	/**
	 * Посылает синхронный запрос мобильному приложению.<br>
	 * <b>"Определи текущее местоположение"</b>
	 * Если определить невозможно, например в базе мапинга mnn и mnc нет записи о GSM соте
	 * то выкидывает RuntimeException.
	 * @param userId id пользователя 
	 * @param mobileAgentId id мобильного агента
	 * @return текущее расположение - null, только в случае, если пользователь не зарегистрирован. 
	 */
	ILocation requestMobileAgentLocation(int userId, long mobileAgentId) throws Throwable;

	/**
	 * Посылает асинхронный запрос на включение режима "Сигнализация" на мобильном агенте.
	 * @param userId id пользователя
	 * @param mobileAgentId id мобильного агента
	 * @return false если пользователя нет в базе
	 */
	boolean activateCarAlarm(int id, long mobileAgentId);

	/**
	 * Посылает асинхронный запрос выключение режима "Сигнализация" на мобильном агенте.
	 * @param userId id пользователя
	 * @param mobileAgentId id мобильного агента
	 * @return false если пользователя нет в базе
	 */
	boolean switchOffCarAlarm(int id, long mobileAgentId);

	/**
	 * Возвращает список треков в состоянии тревоги.
	 * <b>Порядок</b> треков реверсивный относительно даты,
	 * т.е. последний по времени трек идет первым в списке.<br>
	 * В самих треках порядок обычный, по возрастанию времени.
	 * @param userId идентификатор пользователя.
	 * @param mobileAgentId идентификатор мобильного агента.
	 * @param date TODO
	 * @param timeBackInMillis TODO
	 * @param maxCount TODO
	 * @return empty list if no tracks
	 */
	List<ITrack> getAlertTracks(int userId, long mobileAgentId, Date date, long timeBackInMillis, int maxCount);

	/**
	 * Блокирует мобильного агента и удаляет его для интерфейса пользователя.
	 * @param userId идентификатор пользователя.
	 * @param mobileAgentId идентификатор мобильного агента.
	 */
	void disableMobileAgent(int userId, long mobileAgentId);

	void changeMobileAgentProfile(int userId, long mobileAgentId,
			String delayPushAlertEventInSeconds,
			String delayWhereAmIRequestInSeconds, String enableDebugMode, String asdMaxJerk, String asdMaxAcceleration, String asdMaxAngle);
	
	/**
	 * Make event inspired from server side (change state of mobile agent).
	 * @param userId user identity.
	 * @param mobileAgentId mobile agent identity.
	 * @param mobileAgentState state fo mobile agent.
	 * @param receivedEventTime TODO
	 */
	void changeMobileAgentState(int userId, long mobileAgentId, IMobileAgentState mobileAgentState, Date receivedEventTime);

	/**
	 * Change version of mobile agent.
	 * TODO: move code into UMASTimer or extract version from mobile-agent-state
	 * @param userId user identity.
	 * @param mobileAgentId mobile agent identity.
	 * @param mobileAgentVersion TODO
	 */
	void changeMobileAgentVersion(int userId, long mobileAgentId, IMobileAgentVersion mobileAgentVersion);

	/**
	 * Get active mobile agents.
	 * @return more then zero
	 */
	int getActiveMobileAgentsCount();

	/**
	 * Update info for mobile agent.
	 * @param userId user identity
	 * @param mobileAgentId mobile agent identity
	 * @param name new name for mobile agent
	 * @param alertPhoneNumber new alertPhone for mobile agent
	 * @param alertEmail TODO
	 */
	boolean updateMobileAgent(int userId, long mobileAgentId, String name,
			String alertPhoneNumber, String alertEmail);

	/**
	 * Change incomming message for mobile agent.
	 * All messages exclude message for {@link IMobileAgent#NOBODY_ID}
	 * are volatile, with time to life from setup to next sign-up.
	 * @param userId user identity.
	 * @param mobileAgentId mobile agent identity.
	 * @param message incomming message.
	 */
	void changeIncommingMessage(int userId, long mobileAgentId, String message);

	/**
	 * Get list of memories for all enabled mobile agents for user.
	 * Throw runtime exception if user not found.
	 * @param userId user identity
	 * @return empty list if user not found or without mobile agents.
	 */
	List<Map<String, Object>> getMobileAgentsProperties(int userId);

	/**
	 * Memory of mobile agent.
	 * Throw runtime exception if user not found.
	 * @param userId user identity
	 * @param mobileAgentId mobile agent identity
	 * @return empty map if mobile agent not has memory (newbie for example).
	 */
	Map<String, Object> getMobileAgentProperties(int userId, long mobileAgentId);

	/**
	 * Change shared memory for all users entry.
	 * @param key key of memory
	 * @param value of memory entry
	 */
	void changeSharedMemoryForAllUsersEntry(
			String key, String value);

	ISignInMaResponse signIn(int userId, long mobileAgentId,
			Map<String, Object> properties);

	/**
	 * Get time-limited flags for users 
	 * @param userIds list of user identities
	 * @return map of flags
	 */
	Map<Integer, Boolean> getTimeLimitedVersion(List<Integer> userIds);
	
	/**
	 * Get used-as-referral flags for users 
	 * @param userIds list of user identities
	 * @return map of flags
	 */
	Map<Integer, Boolean> getUsedAsReferral(List<Integer> userIds);

	/**
	 * Get user limits. 
	 * @param userId user identities
	 * @return limits or null if not found.
	 */
	IUserLimit getUserLimit(int userId);

	/**
	 * Ping for mobile agent.<br>
	 * Detection online state for mobile agent.
	 * @param mobileAgentId mobile agent identity.
	 */
	void ping(long mobileAgentId);

	/**
	 * Pubkey update service.<br>
	 * Every day or more evenly.
	 * @param mobileAgentId mobile agent identity.
	 * @param pubkey, public key for ssl connection to mobile agent.
	 */
	void pubkeyUpdate(long mobileAgentId, byte[] pubkey);

	/**
	 * Pubkey service.<br>
	 * @param mobileAgentId mobile agent identity
	 * @return public key for mobile agent remote control.
	 */
	byte[] getPubkey(long mobileAgentId);

	/**
	 * Get encoded mobile agents ids and mobile agent names.
	 * @param mobileAgentId mobile agent identity.
	 * @return Encoded strings (pair mobile agent id and mobile agent name).
	 */
	List<String> getMyMobileAgents(long mobileAgentId);

	/**
	 * Get last host for mobile-agent.
	 * @param mobileAgentId mobile agent identity.
	 * @return String host ip.
	 */
	String getHost(long mobileAgentId);

	/**
	 * Request for changing properties for mobile agent.
	 * @param mobileAgentId mobile agent identity.
	 * @param settings set of properties for mobile agent.
	 * @return true if success and false otherwise cause.
	 */
	boolean changeMobileAgentSettings(long mobileAgentId, Map<Object, Object> settings);

	/**
	 * Request settings for mobile agent.
	 * @param mobileAgentId mobile agent identity.
	 * @return properties or null if profile not found.
	 */
	Properties getMobileAgentSettings(long mobileAgentId);

	/**
	 * Request existence email registered in HipDriver.Ru service
	 * @param email
	 * @return true if e-mail exists.
	 */
	boolean checkEmail(String email);

	/**
	 * Request encoded password hash for existence email registered in HipDriver.Ru service
	 * @param email
	 * @return encoded hash from database.
	 */
	byte[] getEncodedPasswordHash(String email);

}
