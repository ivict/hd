/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class KernelException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public KernelException(String message, Throwable cause) {
		super(message, cause);
	}

	public KernelException(String message) {
		super(message);
	}

	public KernelException(Throwable cause) {
		super(cause);
	}
	

}
