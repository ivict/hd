/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;

/**
 * Процессор коррекция месторасположения мобильных агентов.
 * @author ivict
 *
 */
public interface CorrectionLocationProcessor {
	void process(long eventId, short eventClassId, long mobileAgentId,
			Date eventTime);
	/**
	 * Mark time of last change
	 * @param mobileAgentId
	 * @param receivedEventTime
	 */
	void markProcessed(long mobileAgentId, Date receivedEventTime);
}
