/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;


/**
 * Таймер обновления состояния мобильных агентов.
 * @author ivict
 *
 */
public interface TracksTimer {
	void invoke();
}
