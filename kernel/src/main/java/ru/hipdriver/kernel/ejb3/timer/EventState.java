/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;

import java.util.Date;

class EventState {
	final private long eventId;
	final private short eventClassId;
	final private long mobileAgentId;
	final private Date receivedEventTime;
	
	public EventState(long eventId, short eventClassId, long mobileAgentId, Date receivedEventTime) {
		super();
		this.eventId = eventId;
		this.eventClassId = eventClassId;
		this.mobileAgentId = mobileAgentId;
		this.receivedEventTime = receivedEventTime;
	}
	
	public EventState(Object params) {
		this((Long) ((Object[])params)[0], ((Number) ((Object[])params)[1]).shortValue(), (Long) ((Object[])params)[2], (Date) ((Object[])params)[3]);
	}

	public long getEventId() {
		return eventId;
	}

	public long getMobileAgentId() {
		return mobileAgentId;
	}

	public Date getReceivedEventTime() {
		return receivedEventTime;
	}

	public short getEventClassId() {
		return eventClassId;
	}

}

