/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IPlacemark;

/**
 * Мапер локаций мобильных агентов на геолокации на карте.
 * 
 * @author ivict
 *
 */
public interface GeoMapper {

	/**
	 * Преобразование нативных координат (gps или gsm), в координаты для карты.
	 * @param location координата мобильного агента.
	 * @return placemark для карты.
	 */
	IPlacemark parse(ILocation location);

	IPlacemark precisionParse(ICarLocation location);
	
}
