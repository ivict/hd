/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;

import ru.hipdriver.i.IMobileAgentConnection;

/**
 * Mobile agent connections manager.
 * Интерфейс управления текущими соединениями с мобильными агентами. 
 * @author ivict
 *
 */
public interface MACManager {

	/**
	 * Обновление информации о хосте мобильного агента.
	 * @param mobileAgentId id мобильного агента
	 * @param addr удаленный адрес хоста
	 * @param host имя удаленного хоста
	 * @param port порт на удаленном хосте
	 * @param user пользователь
	 * @param signInTime время начала последней сессии мобильного агента
	 */
	void updateConnectionInfo(long mobileAgentId, String addr,
			String host, int port, String user, Date signInTime);
	
	/**
	 * Возвращает информацию о соединении с мобильным агентом.
	 * @param mobileAgentId идентификатор мобильного агента.
	 * @return информация о хосте и других параметрах соединения
	 * @see ru.hipdriver.i.IMobileAgentConnection
	 */
	IMobileAgentConnection getConnectionInfo(long mobileAgentId);
	
	/**
	 * Возвращает дескриптор потока, который ожидает связи с мобильным агентом в режиме клиента.<br>
	 * Логика работы:<br>
	 * 1) Если дескриптор локального потока не указан,
	 * то берем дескриптор входящего потока из записи дескриптора целевого агента.
	 * 2) В противном случае, берем дескриптор входящего потока из записи
	 * дескриптора целевого агента, обновляем дескриптор локального потока для
	 * мобильного агента и дескриптор удаленного потока для целевого агента.
	 * @param mobileAgentId идентификатор мобильного агента.
	 * @param localStreamDescriptor дескриптор локального потока мобильного агента или null если он не менялся.
	 * @param targetMobileAgentId целевой получатель запроса на установление соединения. 
	 * @return дескриптор потока или null если нет потока ожидающего соединения с мобильным агентом.
	 */
	String getClientRemoteStreamDescriptor(long mobileAgentId, long targetMobileAgentId, String localStreamDescriptor);
	
	/**
	 * Возвращает дескриптор потока, который ожидает связи с мобильным агентом в режиме сервера.<br>
	 * Логика работы:<br>
	 * 1) Если дескриптор локального потока не указан,
	 * то ожидаем входящих соединений в режиме сервера.
	 * 2) В противном случае,
	 * обновляем дескриптор локального потока и ожидаем входящих соединений в
	 * режиме сервера.
	 * @param mobileAgentId идентификатор мобильного агента.
	 * @param localStreamDescriptor дескриптор локального потока мобильного агента или null если он не менялся.
	 * @return дескриптор потока или null если нет потока ожидающего соединения с мобильным агентом.
	 */
	String getServerRemoteStreamDescriptor(long mobileAgentId, String localStreamDescriptor);
	
}
