/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ILocation;

class Location implements ILocation {

	private static final String READ_ONLY_LOCATION = "Read only location";
	
	private final int lat;
	private final int lon;
	private final int alt;
	private final int acc;
	private final int mcc;
	private final int mnc;
	private final int lac;
	private final int cid;
	
	public Location(int lat, int lon, int alt, int acc, int mcc, int mnc,
			int lac, int cid) {
		this.lat = lat;
		this.lon = lon;
		this.alt = alt;
		this.acc = acc;
		this.mcc = mcc;
		this.mnc = mnc;
		this.lac = lac;
		this.cid = cid;
	}
	
	@Override
	public int getLat() {
		return lat;
	}

	@Override
	public int getLon() {
		return lon;
	}

	@Override
	public int getAlt() {
		return alt;
	}

	@Override
	public int getAcc() {
		return acc;
	}

	@Override
	public int getMcc() {
		return mcc;
	}

	@Override
	public int getMnc() {
		return mnc;
	}

	@Override
	public int getLac() {
		return lac;
	}

	@Override
	public int getCid() {
		return cid;
	}

	@Override
	public int getFactor() {
		return Locations.GEO_FACTOR;
	}

	@Override
	public void setFactor(int factor) {
		throw new IllegalStateException(READ_ONLY_LOCATION);
	}

	@Override
	public void setLat(int latitude) {
		throw new IllegalStateException(READ_ONLY_LOCATION);
	}

	@Override
	public void setLon(int longitude) {
		throw new IllegalStateException(READ_ONLY_LOCATION);
	}

	@Override
	public void setAlt(int alt) {
		throw new IllegalStateException(READ_ONLY_LOCATION);
	}

	@Override
	public void setAcc(int acc) {
		throw new IllegalStateException(READ_ONLY_LOCATION);
	}

	@Override
	public void setMcc(int mcc) {
		throw new IllegalStateException(READ_ONLY_LOCATION);
	}

	@Override
	public void setMnc(int mnc) {
		throw new IllegalStateException(READ_ONLY_LOCATION);
	}

	@Override
	public void setLac(int lac) {
		throw new IllegalStateException(READ_ONLY_LOCATION);
	}

	@Override
	public void setCid(int cid) {
		throw new IllegalStateException(READ_ONLY_LOCATION);
	}
	
}
