/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ru.hipdriver.kernel.ejb3.MASHelper;
import ru.hipdriver.kernel.entity.MobileAgentState;

/**
 * Хелпер состояний и классов событий.
 */
@Singleton
@Local(MASHelper.class)
@EJB(name = "java:app/MASHelper", beanInterface = MASHelper.class)
public class XMASHelper implements MASHelper {
	
	@PersistenceContext
	private EntityManager em;
	
	private Map<String, Short> eventClasses;
	private Map<String, Short> appStates;
	private Map<String, Short> carStates;

    /**
     * Default constructor. 
     */
    public XMASHelper() {
    	eventClasses = new HashMap<String, Short>();
    	appStates = new HashMap<String, Short>();
    	carStates = new HashMap<String, Short>();
    }
    
	/**
	 * @see MASHelper#getEventClassId(String)
	 */
	@Override
	public short getEventClassId(String eventClassName) {
		if (eventClasses == null) {
			//No sense to concurrency optimization feature
			eventClasses = new HashMap<String, Short>();
		}
		Short eventClassId = eventClasses.get(eventClassName);
		if (eventClassId != null) {
			return eventClassId;
		}
		Object id = em.createQuery("select e.id from EventClass e where e.name like :eventClassName")
				.setParameter("eventClassName", eventClassName)
				.getSingleResult();
		eventClassId = ((Number) id).shortValue();
		eventClasses.put(eventClassName, eventClassId);
		return eventClassId;
	}

	/**
	 * @see MASHelper#getAppStateId(String)
	 */
	@Override
	public short getAppStateId(String appStateName) {
		if (appStates == null) {
			//No sense to concurrency optimization feature
			appStates = new HashMap<String, Short>();
		}
		Short appStateId = appStates.get(appStateName);
		if (appStateId != null) {
			return appStateId;
		}
		Object id = em.createQuery("select e.id from AppState e where e.name like :appStateName")
				.setParameter("appStateName", appStateName)
				.getSingleResult();
		appStateId = ((Number) id).shortValue();
		appStates.put(appStateName, appStateId);
		return appStateId;
	}

	/**
	 * @see MASHelper#getCarStateId(String)
	 */
	@Override
	public short getCarStateId(String carStateName) {
		if (carStates == null) {
			//No sense to concurrency optimization feature
			carStates = new HashMap<String, Short>();
		}
		Short carStateId = carStates.get(carStateName);
		if (carStateId != null) {
			return carStateId;
		}
		Object id = em.createQuery("select e.id from CarState e where e.name like :carStateName")
				.setParameter("carStateName", carStateName)
				.getSingleResult();
		carStateId = ((Number) id).shortValue();
		carStates.put(carStateName, carStateId);
		return carStateId;
	}

	/**
	 * @see MASHelper#makeMobileAgentState(long, Date)
	 */
	@SuppressWarnings("deprecation")
	@Override
	public MobileAgentState makeMobileAgentState(long mobileAgentId,
			Date eventTime) {
		MobileAgentState mobileAgentState;
		mobileAgentState = new MobileAgentState();
		mobileAgentState.setMobileAgentId(mobileAgentId);
		mobileAgentState.setTime(eventTime);//PROCESSED FLAG1
		mobileAgentState.setLastAlertTrackTime(eventTime);//PROCESSED FLAG2
		mobileAgentState.setLastEmailTime(eventTime);//PROCESSED FLAG3
		return mobileAgentState;
	}

}
