/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;


/**
 * Таймер коррекции расположения мобильных агентов.
 * Помимо основной работы, инициирует коррекцию расположения сотовых вышек.
 * @author ivict
 *
 */
public interface CLTimer {
	void invoke();
}
