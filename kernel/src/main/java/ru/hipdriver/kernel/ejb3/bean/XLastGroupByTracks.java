package ru.hipdriver.kernel.ejb3.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ru.hipdriver.kernel.ejb3.LastGroupByTracks;
import ru.hipdriver.kernel.entity.GroupByTrack;

/**
 * Session Bean implementation class XLastGroupByTracks
 */
@Singleton
@Local(LastGroupByTracks.class)
public class XLastGroupByTracks implements LastGroupByTracks {
	
	@PersistenceContext
	private EntityManager em;
	
	private Map<Long, Long> lastGroupByTrackIds;
	private Map<Long, Long> cachedGroupByTrackIds;

    /**
     * Default constructor. 
     */
    public XLastGroupByTracks() {
    	//Hash map because single writer and multiply readers
    	lastGroupByTrackIds = new HashMap<Long, Long>();
    	//Hash map because no sense apply heavy synchronize mechanism 
    	cachedGroupByTrackIds = new HashMap<Long, Long>();
    }

    @Override
	public void createNewGroupByTrack(int userId, long mobileAgentId, long eventId) {
		GroupByTrack groupByTrack = new GroupByTrack();
		groupByTrack.setUserId(userId);
		groupByTrack.setMobileAgentId(mobileAgentId);
		groupByTrack.setEventId(eventId);
		em.persist(groupByTrack);
		//Generate id before "send-alert-track-event"
		em.flush();
		lastGroupByTrackIds.put(mobileAgentId, groupByTrack.getId());
	}
    
    @Override
	public long findGroupByTrack(long mobileAgentId) {
    	//Search in newest ids
    	Long groupByTrackId = lastGroupByTrackIds.get(mobileAgentId);
    	if (groupByTrackId != null) {
    		return groupByTrackId;
    	}
    	//Search in cache
    	groupByTrackId = cachedGroupByTrackIds.get(mobileAgentId);
    	if (groupByTrackId != null) {
    		return groupByTrackId;
    	}    	
		List<?> groupByTrackIds = em.createQuery("select e.id from GroupByTrack e where e.mobileAgentId = :mobileAgentId order by e.id desc")
		.setParameter("mobileAgentId", mobileAgentId)
		.setMaxResults(1)
		.getResultList();
		if (groupByTrackIds.isEmpty()) {
			return 0;
		}
		groupByTrackId = ((Number) groupByTrackIds.get(0)).longValue();
		//Check on valid id
		if (groupByTrackId != 0) {
			cachedGroupByTrackIds.put(mobileAgentId, groupByTrackId);
		}
		return groupByTrackId;
	}
    
}
