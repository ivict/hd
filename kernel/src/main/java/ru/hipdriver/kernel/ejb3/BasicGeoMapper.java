/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.g.Locations;
import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IPlacemark;
import ru.hipdriver.kernel.entity.CellTower;
import ru.hipdriver.kernel.entity.CellTowerKey;
import ru.hipdriver.kernel.entity.CellTowerUpdateRequest;

/**
 * Session Bean implementation class GeoMapper
 */
@Stateless(mappedName = "geo-mapper")
@Local(GeoMapper.class)
public class BasicGeoMapper implements GeoMapper {
	final Logger log = LoggerFactory.getLogger(BasicTrackProcessor.class);
	
	private static int CENTER_LAT = 53251132;
	private static int CENTER_LON = 50222687;
	

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public IPlacemark parse(ILocation location) {
		if (location == null) {
			throw new IllegalArgumentException("Can't parse null location");
		}
		//TODO: request to the opencellid database
		final float latitude;
		final float longitude;
		//Check gps coordinates
		if (hasValidGpsCoordinates(location)) {
			float factor = location.getFactor();
			latitude = location.getLat() / factor;
			longitude = location.getLon() / factor;
		} else {
			//Mapping gsm coordinates
			int mcc = location.getMcc();
			int mnc = location.getMnc();
			int lac = location.getLac();
			int cid = location.getCid();
			float factor = Locations.GEO_FACTOR;
			CellTowerKey cellTowerKey = new CellTowerKey(mcc, mnc, lac, cid);
			CellTower cellTower = em.find(CellTower.class, cellTowerKey);
			int lat;
			int lon;
			if (cellTower == null) {
				CellTowerUpdateRequest cellTowerUpdateRequest = new CellTowerUpdateRequest();
				cellTowerUpdateRequest.setMcc(mcc);
				cellTowerUpdateRequest.setMnc(mnc);
				cellTowerUpdateRequest.setLac(lac);
				cellTowerUpdateRequest.setCid(cid);
				cellTowerUpdateRequest.setTime(new Date());
				em.persist(cellTowerUpdateRequest);
				lat = CENTER_LAT;
				lon = CENTER_LON;
			} else {
				lat = cellTower.getLat();
				lon = cellTower.getLon();
			}
			latitude = lat / factor;
			longitude = lon / factor;
		}
		final float[] placemark = {latitude, longitude};
		return new IPlacemark() {
			private final String READ_ONLY_PLACEMARK = "Read only placemark"; 

			@Override
			public float[] getPlacemark() {
				return placemark;
			}

			@Override
			public void setPlacemark(float[] placemark) {
				throw new IllegalStateException(READ_ONLY_PLACEMARK);
			}
			
		};
	}

	private boolean hasValidGpsCoordinates(ILocation location) {
		if (location.getLat() == ILocation.UNKNOWN_LATITUDE) {
			return false;
		}
		if (location.getLon() == ILocation.UNKNOWN_LONGITUDE) {
			return false;
		}
		return true;
	}

	@Override
	public IPlacemark precisionParse(ICarLocation location) {
		if (location == null) {
			throw new IllegalArgumentException("Can't parse null location");
		}
		//TODO: request to the opencellid database
		final float latitude;
		final float longitude;
		//Check gps coordinates
		if (hasValidGpsCoordinates(location)) {
			float factor = location.getFactor();
			latitude = location.getLat() / factor;
			longitude = location.getLon() / factor;
		} else {
			//Mapping gsm coordinates
			int mcc = location.getMcc();
			int mnc = location.getMnc();
			int lac = location.getLac();
			int cid = location.getCid();
			float factor = Locations.GEO_FACTOR;
			CellTowerKey cellTowerKey = new CellTowerKey(mcc, mnc, lac, cid);
			CellTower cellTower = em.find(CellTower.class, cellTowerKey);
			int lat;
			int lon;
			if (cellTower == null) {
				CellTowerUpdateRequest cellTowerUpdateRequest = new CellTowerUpdateRequest();
				cellTowerUpdateRequest.setMcc(mcc);
				cellTowerUpdateRequest.setMnc(mnc);
				cellTowerUpdateRequest.setLac(lac);
				cellTowerUpdateRequest.setCid(cid);
				cellTowerUpdateRequest.setTime(new Date());
				em.persist(cellTowerUpdateRequest);
				return null;
			} else {
				lat = cellTower.getLat();
				lon = cellTower.getLon();
			}
			latitude = lat / factor;
			longitude = lon / factor;
		}
		final float[] placemark = {latitude, longitude};
		return new IPlacemark() {
			private final String READ_ONLY_PLACEMARK = "Read only placemark"; 

			@Override
			public float[] getPlacemark() {
				return placemark;
			}

			@Override
			public void setPlacemark(float[] placemark) {
				throw new IllegalStateException(READ_ONLY_PLACEMARK);
			}
			
		};
	}

}
