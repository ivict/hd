/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.ILocation;
import ru.hipdriver.i.IMobileAgentState;
import ru.hipdriver.j.MobileAgentWithState;
import ru.hipdriver.kernel.ejb3.KernelException;
import ru.hipdriver.kernel.ejb3.LastGroupByTracks;
import ru.hipdriver.kernel.ejb3.MASHelper;
import ru.hipdriver.kernel.ejb3.MobileAgentStateProcessor;
import ru.hipdriver.kernel.ejb3.TrackProcessor;
import ru.hipdriver.kernel.entity.MobileAgent;
import ru.hipdriver.kernel.entity.MobileAgentLastLocation;
import ru.hipdriver.kernel.entity.MobileAgentState;
import ru.hipdriver.kernel.entity.SignalStrength;

/**
 * Процессор состояний мобильных агентов.
 * @author ivict
 *
 */
@Stateless
@Local(MobileAgentStateProcessor.class)
@EJB(name = "java:app/MobileAgentStateProcessor", beanInterface = MobileAgentStateProcessor.class)
public class XMobileAgentStateProcessor implements MobileAgentStateProcessor {
	private final Logger log = LoggerFactory.getLogger(XMobileAgentStateProcessor.class);
	
	@PersistenceContext
	private EntityManager em;
	
	private ObjectMapper cachedOm;
	
	@EJB
	private MASHelper masHelper;
	
	@EJB
	private TrackProcessor trackProcessor;
	
	@EJB
	private LastGroupByTracks lastGroupByTracks;
	
	/**
	 * @see ru.hipdriver.kernel.ejb3.MobileAgentStateProcessor#process(long, short, long, Date, String)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void process(long eventId, short eventClassId, long mobileAgentId,
			Date receivedEventTime, String jsonObject) {
		try {
			ObjectMapper om = getObjectMapper();
			//@see db.changelog*.xml
			if (eventClassId == masHelper.getEventClassId("send-alert-track-event")) {
				List<ru.hipdriver.j.Location> track = om.readValue(jsonObject, new TypeReference<List<ru.hipdriver.j.Location>>(){});
				MobileAgent mobileAgent = findMobileAgentOrThrowIfNotFound(mobileAgentId);
				if (track.isEmpty()) {
					return;
				}
				trackProcessor.processAlertTrack(mobileAgent.getId(), track, receivedEventTime, false);
				ICarLocation location = track.get(track.size() - 1);
				processMobileAgentStateCarStateOnlyForAlertTrack(mobileAgent, location.getCarStateId(), receivedEventTime);
				processLastLocation(mobileAgent.getId(), location, receivedEventTime);
				return;
			}
			//TODO: refactoring, move json mapping code to another package
			IMobileAgentState mobileAgentStateValue = om.readValue(jsonObject, MobileAgentWithState.class);
			if (mobileAgentStateValue.getCarStateId() == 0 || mobileAgentStateValue.getAppStateId() == 0) {
				//Invalid mobile agent state
				log.info(String.format("Invalid mobile agent state in event [%s]", eventId));
				markProcessedMobileAgentState(mobileAgentId, receivedEventTime);
				return;
			}
			MobileAgent mobileAgent = findMobileAgentOrThrowIfNotFound(mobileAgentId);
			ILocation location = getEventLastLocation(eventId);
			MobileAgentLastLocation lastLocation = processLastLocation(mobileAgent.getId(), location, mobileAgentStateValue.getLastLocation(), receivedEventTime);
			processMobileAgentState(mobileAgent, mobileAgentStateValue, receivedEventTime);
			processSignalStrength(mobileAgent.getId(), mobileAgentStateValue);
			if (eventClassId == masHelper.getEventClassId("send-alert-event")) {
				//Initial new alert track only after first alert event in alert state
				//If mobileAgent.getVersionCode() < 3 any alert generate new GroupByTrack
				//If mobileAgent.getVersionCode() >= 3 Generate new GroupByTrack only if car state is alert
				short carStateId = mobileAgentStateValue.getCarStateId();
				//Last location set by user-push-switch-on-button-in-application-on-mobile-agent event
				if (carStateId == masHelper.getCarStateId("alarm")) {
					//versionCode >= 3 and car intermediate state is alarm
					lastGroupByTracks.createNewGroupByTrack(mobileAgent.getUserId(), mobileAgent.getId(), eventId);
					makeFakeTrack(receivedEventTime, mobileAgent, lastLocation,
							carStateId);
				} else {
					//versionCode >= 3 and car in final state or in corrected state
					//Prevent GPS issues
					makeFakeTrack(receivedEventTime, mobileAgent, lastLocation,
							carStateId);
				}
			}
			
		} catch (Throwable th) {
			log.warn("Wrong atach for event with id = " + eventId, th);
			throw new EJBException();
			//throw new KernelException(th);
		}
	}

	protected void makeFakeTrack(Date receivedEventTime,
			MobileAgent mobileAgent, ILocation location, short carStateId) {
		List<ru.hipdriver.j.Location> track = new ArrayList<ru.hipdriver.j.Location>();
		ru.hipdriver.j.Location stopLocation = new ru.hipdriver.j.Location();
		stopLocation.setAcc(location.getAcc());
		stopLocation.setAlt(location.getAlt());
		stopLocation.setLat(location.getLat());
		stopLocation.setLon(location.getLon());
		stopLocation.setMcc(location.getMcc());
		stopLocation.setMnc(location.getMnc());
		stopLocation.setLac(location.getLac());
		stopLocation.setCid(location.getCid());
		stopLocation.setCarStateId(carStateId);
		stopLocation.setTime(receivedEventTime);
		track.add(stopLocation);
		//Second point adding by alert track service 
		//track.add(stopLocation);
		trackProcessor.processAlertTrack(mobileAgent.getId(), track, receivedEventTime, true);
	}

	private MobileAgent findMobileAgentOrThrowIfNotFound(long mobileAgentId) {
		MobileAgent mobileAgent = em.find(MobileAgent.class, mobileAgentId);
		if ( mobileAgent == null ) {
			throw new IllegalStateException(exceptionMessage(mobileAgentId));
		}
		return mobileAgent;
	}
	
	/**
	 * @see ru.hipdriver.kernel.ejb3.MobileAgentStateProcessor#markProcessedState(long, Date)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void markProcessedState(long mobileAgentId, Date eventTime) {
		try {
			//Setup unknown car state, unknown app state, and defauld field values
			MobileAgent mobileAgent = em.find(MobileAgent.class, mobileAgentId);
			if ( mobileAgent == null ) {
				return;
			}
			MobileAgentState mobileAgentState = em.find(MobileAgentState.class, mobileAgentId);
			if ( mobileAgentState == null ) {
				mobileAgentState = masHelper.makeMobileAgentState(mobileAgentId, eventTime);
				mobileAgentState.setAppStateId(masHelper.getAppStateId("unknown"));
				mobileAgentState.setCarStateId(masHelper.getCarStateId("unknown"));
				em.persist(mobileAgentState);
				return;
			}
			mobileAgentState.setTime(eventTime);//PROCESSED FLAG
			em.merge(mobileAgentState);
		} catch (Throwable th) {
			throw new KernelException(th);
		}
	}

	/**
	 * @see ru.hipdriver.kernel.ejb3.MobileAgentStateProcessor#markProcessedAlertTrack(long, Date)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void markProcessedAlertTrack(long mobileAgentId, Date eventTime) {
		try {
			//Setup unknown car state, unknown app state, and defauld field values
			MobileAgent mobileAgent = em.find(MobileAgent.class, mobileAgentId);
			if ( mobileAgent == null ) {
				return;
			}
			MobileAgentState mobileAgentState = em.find(MobileAgentState.class, mobileAgentId);
			if ( mobileAgentState == null ) {
				mobileAgentState = masHelper.makeMobileAgentState(mobileAgentId, eventTime);
				mobileAgentState.setAppStateId(masHelper.getAppStateId("unknown"));
				mobileAgentState.setCarStateId(masHelper.getCarStateId("unknown"));
				em.persist(mobileAgentState);
				return;
			}
			mobileAgentState.setLastAlertTrackTime(eventTime);//PROCESSED FLAG
			em.merge(mobileAgentState);
		} catch (Throwable th) {
			throw new KernelException(th);
		}
	}

//	private short getUnknownCarStateId() {
//		if ( unknownCarStateId == 0 ) {
//			Object id = em.createQuery("select e.id from CarState e where e.name like :carStateName")
//					.setParameter("carStateName", "unknown")
//					.getSingleResult();
//			unknownCarStateId = ((Number) id).shortValue();
//		}
//		return unknownCarStateId;
//	}
//
//	private short getUnknownAppStateId() {
//		if ( unknownAppStateId == 0 ) { 
//			Object id = em.createQuery("select e.id from AppState e where e.name like :appStateName")
//					.setParameter("appStateName", "unknown")
//					.getSingleResult();
//			unknownAppStateId = ((Number) id).shortValue();
//		}
//		return unknownAppStateId;
//	}
	
	
	//На многопоточность забиваем, поскольку пара лишних телодвижений погоды не сделают
	//Сам ObjectMapper вполне себе защищеный
	private ObjectMapper getObjectMapper() {
		if ( cachedOm == null ) {
			cachedOm = new ObjectMapper();
		}
		return cachedOm;
	}

	private void processMobileAgentState(MobileAgent mobileAgent,
			IMobileAgentState mobileAgentStateValue, Date receivedEventTime) {
		MobileAgentState mobileAgentState = mobileAgent.getMobileAgentState();
		if ( mobileAgentState == null ) {//First setup
			mobileAgentState = masHelper.makeMobileAgentState(mobileAgent.getId(), receivedEventTime);
			updateMobileAgentState(mobileAgentState, mobileAgentStateValue);
			em.persist(mobileAgentState);
			return;
		}
		updateMobileAgentState(mobileAgentState, mobileAgentStateValue);
		mobileAgentState.setTime(receivedEventTime);
		em.merge(mobileAgentState);
	}
	
	private void markProcessedMobileAgentState(long mobileAgentId, Date receivedEventTime) {
		MobileAgentState mobileAgentState = em.find(MobileAgentState.class, mobileAgentId);
		//Not exists
		if ( mobileAgentState == null ) {
			return;
		}
		mobileAgentState.setTime(receivedEventTime);
		em.merge(mobileAgentState);
	}
	
	private void processMobileAgentStateCarStateOnlyForAlertTrack(MobileAgent mobileAgent,
			short carStateId, Date receivedEventTime) {
		//Check invalid car state
		if (carStateId == 0) {
			return;
		}
		MobileAgentState mobileAgentState = mobileAgent.getMobileAgentState();
		//Check existence
		if ( mobileAgentState == null ) {
			return;
		}
		mobileAgentState.setLastAlertTrackTime(receivedEventTime);
		//Check time horizon
		if (mobileAgentState.getTime().after(receivedEventTime)) {
			em.merge(mobileAgentState);
			return;
		}
		mobileAgentState.setCarStateId(carStateId);
		mobileAgentState.setTime(receivedEventTime);
		em.merge(mobileAgentState);
	}
	
	private String exceptionMessage(long mobileAgentId) {
		return "Where mobile agent with id = " + mobileAgentId + "?";
	}

	private void updateMobileAgentState(MobileAgentState state,
			IMobileAgentState value) {
		//Suppress intermediate state (User misunderstanding issue).
		if (value.getAppStateId() != masHelper.getAppStateId("unknown")) {
			state.setAppStateId(value.getAppStateId());
		}
		state.setCarStateId(value.getCarStateId());
		state.setBatteryStatus(value.getBatteryStatus());
		state.setBatteryPct(value.getBatteryPct());
	}
	
	private void processSignalStrength(long mobileAgentId,
			IMobileAgentState mobileAgentStateValue) {
		SignalStrength signalStrength = em.find(SignalStrength.class, mobileAgentId);
		if ( signalStrength == null ) {//First setup
			signalStrength = new SignalStrength();
			signalStrength.setMobileAgentId(mobileAgentId);
			updateSignalStrength(signalStrength, mobileAgentStateValue);
			em.persist(signalStrength);
			return;
		}
		updateSignalStrength(signalStrength, mobileAgentStateValue);
		em.merge(signalStrength);
	}

	//value.getGsmSignalStrength() == 0
	//Зарезервировано для состояния нет связи.
	private void updateSignalStrength(SignalStrength signalStrength,
			IMobileAgentState value) {
		signalStrength.setGsmBitErrorRate(value.getGsmBitErrorRate());
		int gsmSignalStrength = value.getGsmSignalStrength();
		if (gsmSignalStrength > 0) {
			signalStrength.setGsmSignalStrength(gsmSignalStrength);
		}
	}
	
	private void processLastLocation(long mobileAgentId, ILocation location, Date receivedEventTime) {
		//Invalid location nothing do
		if (invalidLocation(location)) {
			return;
		}
		MobileAgentLastLocation mobileAgentLastLocation = em.find(MobileAgentLastLocation.class, mobileAgentId);
		if ( mobileAgentLastLocation == null ) {//First setup
			mobileAgentLastLocation = new MobileAgentLastLocation();
			mobileAgentLastLocation.setMobileAgentId(mobileAgentId);
			updateLastLocation(mobileAgentLastLocation, location, receivedEventTime);
			em.persist(mobileAgentLastLocation);
			return;
		}
		//Update only new
		if (mobileAgentLastLocation.getTime().after(receivedEventTime)) {
			return;
		}
		//Update only well-known coordinate
		if (isChangeWellKnownOntoZero(mobileAgentLastLocation, location)) {
			//TODO: write request to cell towers mapping
			return;
		}
		updateLastLocation(mobileAgentLastLocation, location, receivedEventTime);
		em.merge(mobileAgentLastLocation);
	}
	
	/**
	 * Всегда не null
	 * @param mobileAgentId идентификатор мобильного агента
	 * @param eventLocation местоположение события
	 * @param fromAtachLocation местоположение из атача
	 * @param receivedEventTime время приема события
	 * @return последнее местоположение мобильного агента
	 */
	private MobileAgentLastLocation processLastLocation(long mobileAgentId, ILocation eventLocation, ILocation locationFromAttach, Date receivedEventTime) {
		ILocation location = chooseMaxQualityLocation(eventLocation, locationFromAttach);
		MobileAgentLastLocation mobileAgentLastLocation = em.find(MobileAgentLastLocation.class, mobileAgentId);
		if ( mobileAgentLastLocation == null ) {//First setup
			mobileAgentLastLocation = new MobileAgentLastLocation();
			mobileAgentLastLocation.setMobileAgentId(mobileAgentId);
			updateLastLocation(mobileAgentLastLocation, location, receivedEventTime);
			em.persist(mobileAgentLastLocation);
			return mobileAgentLastLocation;
		}
		//Update only new
		if (mobileAgentLastLocation.getTime().after(receivedEventTime)) {
			return mobileAgentLastLocation;
		}
		//Update only well-known coordinate
		if (isChangeWellKnownOntoZero(mobileAgentLastLocation, location)) {
			//TODO: write request to cell towers mapping
			return mobileAgentLastLocation;
		}
		updateLastLocation(mobileAgentLastLocation, location, receivedEventTime);
		em.merge(mobileAgentLastLocation);
		return mobileAgentLastLocation;
	}
	
	private ILocation chooseMaxQualityLocation(ILocation location1,
			ILocation location2) {
		//Null cheks
		if (location1 == null && location2 == null) {
			return null;
		}
		if (location1 == null && location2 != null) {
			return location2;
		}
		if (location1 != null && location2 == null) {
			return location1;
		}
		
		//Invalid checks
		if (invalidLocation(location1) && invalidLocation(location2)) {
			return location2;
		}
		if (invalidLocation(location1) && !invalidLocation(location2)) {
			return location2;
		}
		if (!invalidLocation(location1) && invalidLocation(location2)) {
			return location1;
		}
		
		//Known checks
		if (!isKnownLocation(location1) && !isKnownLocation(location2)) {
			return location2;
		}
		if (!isKnownLocation(location1) && isKnownLocation(location2)) {
			return location2;
		}
		if (isKnownLocation(location1) && !isKnownLocation(location2)) {
			return location1;
		}

		int acc1 = location1.getAcc();
		int acc2 = location2.getAcc();
		
		if (acc1 == ILocation.UNKNOWN_ACCURACY && acc2 == ILocation.UNKNOWN_ACCURACY) {
			return location2;
		}
		if (acc1 == ILocation.UNKNOWN_ACCURACY && acc2 != ILocation.UNKNOWN_ACCURACY) {
			return location2;
		}
		if (acc1 != ILocation.UNKNOWN_ACCURACY && acc2 == ILocation.UNKNOWN_ACCURACY) {
			return location1;
		}
		
		if (acc1 < acc2) {
			return location1;
		}
		
		return location2;
	}

	private ILocation getEventLastLocation(long eventId) {
		ILocation location = (ILocation) 
				em.createQuery("select new ru.hipdriver.kernel.ejb3.Location(e.lat, e.lon, e.alt, e.acc, e.mcc, e.mnc, e.lac, e.cid) from Event e where e.id = :eventId")
				.setParameter("eventId", eventId)
				.getSingleResult();
		return location;
	}
	
	private boolean isChangeWellKnownOntoZero(
			ILocation prevLocation,
			ILocation newLocation) {
		if (prevLocation.getLat() != ILocation.UNKNOWN_LATITUDE && newLocation.getLat() == ILocation.UNKNOWN_LATITUDE) {
			return true;
		}
		if (prevLocation.getLon() != ILocation.UNKNOWN_LONGITUDE && newLocation.getLon() == ILocation.UNKNOWN_LONGITUDE) {
			return true;
		}
		return false;
	}

	private boolean invalidLocation(ILocation location) {
		//Has GPS Position
		if ( ILocation.Validator.isUnknownLocation(location) ) {
			return false;
		}
		//Has GSM Position
		if ( ILocation.Validator.isUnknownGsmLocation(location) ) {
			return false;
		}
		return true;
	}

	private boolean isKnownLocation(ILocation location) {
		return location.getLat() != ILocation.UNKNOWN_LATITUDE && location.getLon() != ILocation.UNKNOWN_LONGITUDE;
	}

	private void updateLastLocation(MobileAgentLastLocation lastLocation,
			ILocation value, Date receivedEventTime) {
		lastLocation.setLat(value.getLat());
		lastLocation.setLon(value.getLon());
		lastLocation.setAlt(value.getAlt());
		lastLocation.setAcc(value.getAcc());
		lastLocation.setMcc(value.getMcc());
		lastLocation.setMnc(value.getMnc());
		lastLocation.setLac(value.getLac());
		lastLocation.setCid(value.getCid());
		lastLocation.setTime(receivedEventTime);
	}
}
