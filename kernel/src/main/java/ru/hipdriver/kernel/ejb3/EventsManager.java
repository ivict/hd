/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;
import java.util.List;

import ru.hipdriver.i.IEvent;
import ru.hipdriver.i.IEventAtach;
import ru.hipdriver.i.IEventClass;
import ru.hipdriver.i.IEventWithAtach;
import ru.hipdriver.i.ISid;

/**
 * Инкапсуляция логики управления событиями.<br>
 * В Качестве примера событий можно привести: Удар, Угон, Снятие колес, Эвакуатор и т.д.
 * @author ivict
 */
public interface EventsManager {

	/**
	 * Регистрирует новое событие.
	 * @param sid сессионный идентификатор
	 * @param event событие
	 * @param receivedEventTime TODO
	 * @return true если регистрация прошла успешно
	 */
	boolean push(ISid sid, IEvent event, Date receivedEventTime);

	/**
	 * Регистрирует новое событие c атачем.
	 * @param sid сессионный идентификатор
	 * @param event событие
	 * @param eventAtach атач к событию
	 * @param receivedEventTime TODO
	 * @return true если регистрация прошла успешно
	 */
	boolean pusha(ISid sid, IEvent event, IEventAtach eventAtach, Date receivedEventTime);

	/**
	 * Общее количество зарегистрированных событий.
	 * @return
	 */
	int count();

	/**
	 * Список событий пользователя.
	 * Предупреждение. Неприятный в отношении безопасности метод
	 * ставим TODO на его блокировку после выхода в production,
	 * Либо блокировку прямого запроса с frontend'а
	 * Фильтрация событий осуществляется по дате приема сообщения
	 * Без указания горизонта показываюттся только первые 100 событий
	 * 
	 * @param userName Имя пользователя, всегда не null
	 * @param dateFrom Нижняя граница горизонта событий, если null то снимаем нижнюю границу 
	 * @param dateTo Верхняя граница горизонта событий, если null то снимаем верхнюю границу
	 * @return Список событий либо пустой список если ничего нет.
	 */
	List<IEvent> list(String userName, Date dateFrom, Date dateTo);

	/**
	 * Список классов событий.
	 * 
	 * @return Список классов событий.
	 */
	List<IEventClass> classes();

	/**
	 * Возвращает последние события определенного класа для пользователя.
	 * @param userId идентификатор пользователя
	 * @param eventClassIdValue идентификатор класса события
	 * @param mobileAgentId идентификатор мобильного агента
	 * @param onDate TODO
	 * @param maxCount TODO
	 * @return null если событие не происходило
	 */
	List<IEventWithAtach> lastEvents(int userId, short eventClassIdValue, long mobileAgentId, Date onDate, int maxCount);

	/**
	 * Возвращает последние события для пользователя.
	 * @param userId идентификатор пользователя
	 * @param eventClassIdValue идентификатор класса события
	 * @param mobileAgentId идентификатор мобильного агента
	 * @param onDate TODO
	 * @param maxCount TODO
	 * @return null если событие не происходило
	 */
	List<IEventWithAtach> lastEvents(int userId, long mobileAgentId, Date onDate, int maxCount);

}
