/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.kernel.ejb3.MASHelper;
import ru.hipdriver.kernel.ejb3.MobileAgentStateProcessor;

/**
 * Таймер обновления состояния мобильных агентов.
 * Обрабатывает события от мобильных агентов.
 * @author ivict
 *
 */
@Startup
@Singleton
@Local(UMASTimer.class)
public class UpdateMobileAgentsStates implements UMASTimer {
	final Logger log = LoggerFactory.getLogger(UpdateMobileAgentsStates.class);
	
	@PersistenceContext
	private EntityManager em;
	
	private List<Short> eventClassesFilter;
	
	@EJB
	MobileAgentStateProcessor stateProcessor;
	
	@EJB
	MASHelper masHelper;

	/**
	 * @see UMASTimer#invoke()
	 */
	@Schedule(hour="*", minute="*", second="*/5", persistent=false)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void invoke() {
		//Select Last events
		//If event has class включился и отключился
		//смотрим на атач. Десериализуем, в что то, типа IMobileAgentState;
		//Обрабатываем mobile agent state, через установку мобильному агенту
		//Пропуск обработанных осуществляем по полю времени.
		//Кроме этого действует синхронизатор по id мобильного агента
		//Два события от одного агента не обрабатываются в один цикл. 

		//Distinct by mobile agents for one cycle
		Set<Long> mobileAgents = new HashSet<Long>(); 
		
		//Old states
		List<?> updatesStates = em.createQuery("select e.id, e.eventClassId, a.content, ma.id, e.receivedTime, mas.time from EventAtach a join a.event e join e.mobileAgent ma join ma.mobileAgentState mas where e.receivedTime > mas.time and a.content is not null and e.eventClassId in (:eventClassesFilter) order by e.receivedTime asc")
				.setParameter("eventClassesFilter", getEventClassesFilter()).getResultList();
		for ( Object o : updatesStates ) {
			UpdateState updateState = new UpdateState(o);
			long eventId = updateState.getEventId();
			short eventClassId = updateState.getEventClassId();
			long mobileAgentId = updateState.getMobileAgentId();
			//Same agents restriction 
			if (mobileAgents.contains(mobileAgentId)) {
				continue;
			}
			Date receivedEventTime = updateState.getReceivedEventTime();
			String jsonObject = getJsonString(updateState.getContent());
			try {
				stateProcessor.process(eventId, eventClassId, mobileAgentId, receivedEventTime, jsonObject) ;
				mobileAgents.add(mobileAgentId);
			} catch ( RuntimeException e ) {
				//Check communication link error
				if (findSubstring(e, "com.mysql.jdbc.exceptions.jdbc4.CommunicationsException") ) {
					log.warn(String.format("Retry process of event with id[%d], class_id[%d], received_time[%s]", eventId, eventClassId, receivedEventTime ));
					mobileAgents.add(mobileAgentId);
					continue;
				}
				safetyMarkProcessed(mobileAgentId, receivedEventTime);
			}
		}
		
		//New states
		List<?> insertsStates = em.createQuery("select e.id, e.eventClassId, a.content, ma.id, e.receivedTime from EventAtach a join a.event e join e.mobileAgent ma where ma.mobileAgentState is null and a.content is not null and e.eventClassId in (:eventClassesFilter) order by e.receivedTime asc")
				.setParameter("eventClassesFilter", getEventClassesFilter()).getResultList();
		for ( Object o : insertsStates ) {
			InsertState insertState = new InsertState(o);
			long eventId = insertState.getEventId();
			short eventClassId = insertState.getEventClassId();
			long mobileAgentId = insertState.getMobileAgentId();
			//Same agents restriction 
			if (mobileAgents.contains(mobileAgentId)) {
				continue;
			}
			Date eventTime = insertState.getReceivedEventTime();
			String jsonObject = getJsonString(insertState.getContent());
			try {
				stateProcessor.process(eventId, eventClassId, mobileAgentId, eventTime, jsonObject);
				mobileAgents.add(mobileAgentId);
			} catch (RuntimeException e) {
				//Check communication link error
				if (findSubstring(e, "com.mysql.jdbc.exceptions.jdbc4.CommunicationsException") ) {
					log.warn(String.format("Retry process of event with id[%d], class_id[%d], received_time[%s]", eventId, eventClassId, eventTime ));
					mobileAgents.add(mobileAgentId);
					continue;
				}
				safetyMarkProcessed(mobileAgentId, eventTime);
			}
		}
	}

	private boolean findSubstring(Throwable th, String className) {
		if (th == null) {
			return false;
		}
		if (className == null) {
			return false;
		}
		String throwableClassName = th.getClass().getName();
		if (className.equals(throwableClassName)) {
			return true;
		}
		Throwable parentThrowable = th.getCause(); 
		while (parentThrowable != null) {
			throwableClassName = parentThrowable.getClass().getName();
			if (className.equals(throwableClassName)) {
				return true;
			}
			Throwable cause = parentThrowable;
			parentThrowable = parentThrowable.getCause();
			if (parentThrowable == cause) {
				break;
			}
		}
		return false;
	}

	private String getJsonString(byte[] content) {
		if ( content == null ) {
			return "{}";//Empty json object
		}
		return new String(content);
	}

	private void safetyMarkProcessed(long mobileAgentId, Date receivedEventTime) {
		try {
			stateProcessor.markProcessedState(mobileAgentId, receivedEventTime);
		} catch (RuntimeException e) {
			log.error("What the terrible failure", e);
		}
	}

	
	private List<Short> getEventClassesFilter() {
		if (eventClassesFilter != null) {
			return eventClassesFilter;
		}
		eventClassesFilter = new ArrayList<Short>();
		eventClassesFilter.add(masHelper.getEventClassId("user-push-switch-on-button-in-application-on-mobile-agent"));
		eventClassesFilter.add(masHelper.getEventClassId("user-push-switch-off-button-in-application-on-mobile-agent"));
		eventClassesFilter.add(masHelper.getEventClassId("mobile-agent-send-gsm-location-event"));
		eventClassesFilter.add(masHelper.getEventClassId("mobile-agent-send-gps-location-event"));
		eventClassesFilter.add(masHelper.getEventClassId("send-alert-event"));
		return eventClassesFilter;
	}
	
}
