/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;
import java.util.List;

import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.ITrack;

/**
 * Транслятор треков мобильных агентов.
 * 
 * @author ivict
 *
 */
public interface TrackProcessor {

	/**
	 * Трансляция нативных координат (gps или gsm), на сетку дорог.
	 * @param track трек мобильного агента.
	 * @param carStateId TODO
	 * @param receivedTime TODO
	 * @return polyline на карте дорог.
	 */
	ITrack scaleOntoRoadmap(List<? extends ICarLocation> track, short carStateId, Date receivedTime);
	
//	/**
//	 * Равномерная трансляция нативных координат (gsm или gps), на сетку дорог с учетом времени.
//	 * @param track трек мобильного агента.
//	 * @param rateInMillis частота отметки точек.
//	 * @return список placemarks на карте дорог.
//	 */
//	List<IPlacemark> scaleOntoRoadmapEvenly(List<? extends ITimestampedLocation> track, long rateInMillis);
	
	/**
	 * Обработка нативного трека от мобильного агента.
	 * @param mobileAgentId идентификатор мобильного агента.
	 * @param track нативный трек в состоянии тревоги.
	 * @param receivedTime TODO
	 * @param fakeTrack флаг фальшивого трека.
	 */
	void processAlertTrack(long mobileAgentId, List<? extends ICarLocation> track, Date receivedTime, boolean fakeTrack);
	
}
