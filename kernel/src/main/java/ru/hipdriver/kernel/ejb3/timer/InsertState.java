/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;

import java.util.Date;

import javax.sql.rowset.serial.SerialBlob;

class InsertState extends EventState {
	final private byte[] content;

	public InsertState(long eventId, short eventClassId, byte[] content,
			long mobileAgentId, Date receivedEventTime) {
		super(eventId, eventClassId, mobileAgentId, receivedEventTime);
		this.content = content;
	}

	public InsertState(Object params) {
		this((Long) ((Object[]) params)[0], ((Number) ((Object[]) params)[1]).shortValue(),	getSerialBlob((SerialBlob) ((Object[]) params)[2]),	(Long) ((Object[]) params)[3], (Date) ((Object[]) params)[4]);
	}

	public byte[] getContent() {
		return content;
	}

	protected static byte[] getSerialBlob(SerialBlob contentBlob) {
		if (contentBlob == null) {
			return null;
		}
		try {
			long length = contentBlob.length();
			return contentBlob.getBytes(1, (int) length);
		} catch (Throwable th) {
			throw new RuntimeException(th);
		}

	}

}
