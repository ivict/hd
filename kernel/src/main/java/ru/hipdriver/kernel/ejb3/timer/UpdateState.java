/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;

import java.util.Date;


class UpdateState extends InsertState {

	final private Date mobileStateTime;
	
	public UpdateState(long eventId, short eventClassId, byte[] content, long mobileAgentId, Date receivedEventTime,
			Date mobileStateTime) {
		super(eventId, eventClassId, content, mobileAgentId, receivedEventTime);
		this.mobileStateTime = mobileStateTime;
	}
	
	public UpdateState(long eventId, short eventClassId, long mobileAgentId, Date receivedEventTime,
			Date mobileStateTime) {
		this(eventId, eventClassId, null, mobileAgentId, receivedEventTime,
				mobileStateTime);
	}
	
	public UpdateState(Object params) {
		super(params);
		this.mobileStateTime = (Date) ((Object[])params)[4];
	}
	
	public Date getMobileStateTime() {
		return mobileStateTime;
	}

}

