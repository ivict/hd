/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.IPlacemark;
import ru.hipdriver.i.ICarLocation;
import ru.hipdriver.i.ITrack;
import ru.hipdriver.kernel.entity.MobileAgentAlertTrack;
import ru.hipdriver.kernel.entity.MobileAgentAlertTrackKey;

/**
 * Session Bean implementation class TrackProcessor
 */
@Stateless(mappedName = "track-processor")
@Local(TrackProcessor.class)
public class BasicTrackProcessor implements TrackProcessor {
	final Logger log = LoggerFactory.getLogger(BasicTrackProcessor.class);

	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private UserSessionManager userSessionManager;
	
	@EJB
	private GeoMapper geoMapper;
	
	@EJB
	private LastGroupByTracks lastGroupByTracks;

	/**
	 * @see TrackProcessor#scaleOntoRoadmap(List, short, Date)
	 */
	@Override
	public ITrack scaleOntoRoadmap(List<? extends ICarLocation> track, short carStateId, Date receivedTime) {
		int n = track.size();
		float[][] polyline = new float[n][2];
		long[] timestamps = new long[n];
		
		int i = 0;
		for (ICarLocation location : track) {
			IPlacemark placemark = geoMapper.precisionParse(location);
			if (placemark == null) {
				continue;
			}
			float[] coordinates = placemark.getPlacemark();
			//TODO: get result from osrm server use nearest or location service
			polyline[i][0] = coordinates[0];
			polyline[i][1] = coordinates[1];
			Date time = location.getTime();
			if (time == null) {
				throw new IllegalStateException("Null time in location");
			}
			timestamps[i] = time.getTime();
			i++;
		}
		
		Track roadmapedTrack = new Track(polyline, timestamps, carStateId, receivedTime);
		return roadmapedTrack;
	}

//	/**
//	 * {@link TrackProcessor#scaleOntoRoadmapEvenly(List, long)}
//	 */
//	@Override
//	public List<IPlacemark> scaleOntoRoadmapEvenly(
//			List<? extends ITimestampedLocation> track, long rateInMillis) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	/**
	 * @see TrackProcessor#processAlertTrack(long,List, Date, boolean)
	 */
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Asynchronous
	@Override
	public void processAlertTrack(long mobileAgentId, List<? extends ICarLocation> track, Date receivedTime, boolean fakeTrack) {
		if (track == null) {
			return;
		}
		if (track.isEmpty()) {
			return;
		}
		long groupByTrackId = lastGroupByTracks.findGroupByTrack(mobileAgentId);
		//Invalid track (not received alert event)
		if (groupByTrackId == 0) {
			return;
		}
		//Assumption: All track in one state
		short carStateId = track.get(0).getCarStateId();
		ITrack roadmapedTrack = scaleOntoRoadmap(track, carStateId, receivedTime);
		float[][] polyline = roadmapedTrack.getPolyline();
		if (polyline == null) {
			return;
		}
		long[] timestamps = roadmapedTrack.getTimestamps();
		if (timestamps == null) {
			return;
		}
		if (timestamps.length != polyline.length) {
			return;
		}
		//Empty track not persist
		if (timestamps.length < 1) {
			return;
		}
		//Prevent event duplicates
		Date time = new Date(timestamps[0]);
		if (findMobileAgentAlertTrackByKey(mobileAgentId, time, carStateId, groupByTrackId)) {
			return;
		}
		//Check last track
		//MobileAgentAlertTrack lastTrack = getLastTrack(mobileAgentId, groupByTrackId);
		////Geo duplicates not persist
		//if (lastTrack != null && equals(polyline, lastTrack.getPolyline())) {
		//	//Update car state if it different
		//	if (lastTrack.getCarStateId() != carStateId) {
		//		lastTrack.setCarStateId(carStateId);
		//		em.merge(lastTrack);
		//	}
		//	return;
		//}
		MobileAgentAlertTrack alertTrack = new MobileAgentAlertTrack();
		long durationInMillis = timestamps[timestamps.length - 1] - timestamps[0];
		alertTrack.setDurationInMillis(durationInMillis);
		alertTrack.setMobileAgentId(mobileAgentId);
		alertTrack.setTime(time);
		alertTrack.setPolyline(polyline);
		alertTrack.setTimestamps(timestamps);
		alertTrack.setCarStateId(carStateId);
		alertTrack.setReceivedTime(receivedTime);
		alertTrack.setGroupByTrackId(groupByTrackId);
		alertTrack.setFakeTrack(fakeTrack);
		em.persist(alertTrack);
		
		//long defaultRateInMillis = 300000L;//5 min
		//List<IPlacemark> placemarks = trackProcessor.scaleOntoRoadmapEvenly(track, defaultRateInMillis);
		//Save into last alert track
	}

	private boolean findMobileAgentAlertTrackByKey(
			long mobileAgentId, Date time, short carStateId, long groupByTrackId) {
		List<?> mobileAgentAlertTracks = em.createQuery("select e.mobileAgentId from MobileAgentAlertTrack e where e.mobileAgentId = :mobileAgentId and e.time = :time and e.carStateId = :carStateId and e.groupByTrackId = :groupByTrackId")
				.setParameter("mobileAgentId", mobileAgentId)
				.setParameter("time", time)
				.setParameter("carStateId", carStateId)
				.setParameter("groupByTrackId", groupByTrackId)
				.setMaxResults(1)
				.getResultList();
		if (mobileAgentAlertTracks.isEmpty()) {
			return false;
		}
		return true;
	}

	private boolean equals(float[][] polyline1, float[][] polyline2) {
		if (polyline1.length != polyline2.length) {
			return false;
		}
		for (int i = 0,len = polyline1.length; i < len; i++) {
			if (!Arrays.equals(polyline1[i], polyline2[i])) {
				return false;
			}
		}
		return true;
	}

	private MobileAgentAlertTrack getLastTrack(long mobileAgentId, long groupByTrackId) {
		List<?> alertTracks = em.createQuery("select e from MobileAgentAlertTrack e where e.mobileAgentId = :mobileAgentId and e.groupByTrackId = :groupByTrackId order by e.time desc")
		.setParameter("mobileAgentId", mobileAgentId)
		.setParameter("groupByTrackId", groupByTrackId)
		.setMaxResults(1)
		.getResultList();
		if (alertTracks.isEmpty()) {
			return null;
		}
		return (MobileAgentAlertTrack) alertTracks.get(0);
	}

}

class Track implements ITrack {
	private final static String READ_ONLY_TRACK = "Read only track";
	
	private final float[][] polyline;
	private final long[] timestamps;
	private final short carStateId;
	private final Date receivedTime;
	
	public Track(float[][] polyline, long[] timestamps, short carStateId, Date receivedTime) {
		this.polyline = polyline;
		this.timestamps = timestamps;
		this.carStateId = carStateId;
		this.receivedTime = receivedTime;
	}

	@Override
	public float[][] getPolyline() {
		return polyline;
	}

	@Override
	public void setPolyline(float[][] polyline) {
		throw new IllegalStateException(READ_ONLY_TRACK);
	}

	@Override
	public long[] getTimestamps() {
		return timestamps;
	}

	@Override
	public void setTimestamps(long[] timestamps) {
		throw new IllegalStateException(READ_ONLY_TRACK);
	}

	@Override
	public short getCarStateId() {
		return carStateId;
	}

	@Override
	public void setCarStateId(short carStateId) {
		throw new IllegalStateException(READ_ONLY_TRACK);
	}

	@Override
	public Date getReceivedTime() {
		return receivedTime;
	}

	@Override
	public void setReceivedTime(Date receivedTime) {
		throw new IllegalStateException(READ_ONLY_TRACK);
	}

	@Override
	public long getTrackId() {
		return 0;//Not existent group-by-track identity.
	}

	@Override
	public void setTrackId(long trackId) {
		throw new IllegalStateException(READ_ONLY_TRACK);
	}
	
}