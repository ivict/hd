/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import static ru.hipdriver.kernel.Invariants.UNEXPECTED_ERROR;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.IEvent;
import ru.hipdriver.i.IEventAtach;
import ru.hipdriver.i.IEventClass;
import ru.hipdriver.i.IEventWithAtach;
import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.ISid;
import ru.hipdriver.kernel.entity.Event;
import ru.hipdriver.kernel.entity.EventAtach;
import ru.hipdriver.kernel.entity.MobileAgent;
import ru.hipdriver.kernel.entity.User;

/**
 * Session Bean implementation class BasicEventsManager
 */
@Stateless(mappedName = "events-manager")
@Local(EventsManager.class)
@EJB(name = "java:app/EventsManager", beanInterface = EventsManager.class)
public class BasicEventsManager implements EventsManager {
	final Logger log = LoggerFactory.getLogger(BasicEventsManager.class);

	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private UserSessionManager userSessionManager;
	
	/**
     * @see EventsManager#push(ISid, IEvent, Date)
     */
    public boolean push(ISid sid, IEvent event, Date receivedEventTime) {
    	try {
    		//Check session identity
    		IMobileAgent mobileAgent = userSessionManager.getMobileAgent(sid);
			if ( mobileAgent == null ) {
    			return false;
    		}
    		//TODO: check mobile agent id and user id
    		//Based on session identity
    		
	    	push(event, mobileAgent.getUserId(), (MobileAgent) mobileAgent, receivedEventTime);
	    	return true;
    	} catch (Throwable th) {
    		log.error(UNEXPECTED_ERROR, th);
    		return false;
    	}
    }

	Event push(IEvent event, int userId, MobileAgent mobileAgent, Date receivedEventTime) {
		Event o = new Event();
		o.setEventClassId(event.getEventClassId());
		o.setUserId(userId);
		o.setMobileAgent(mobileAgent);
		o.setTime(event.getTime());
		o.setReceivedTime(receivedEventTime);
		o.setLat(event.getLat());
		o.setLon(event.getLon());
		o.setAlt(event.getAlt());
		o.setAcc(event.getAcc());
		o.setMcc(event.getMcc());
		o.setMnc(event.getMnc());
		o.setLac(event.getLac());
		o.setCid(event.getCid());
		em.persist(o);
		return o;
	}

	/**
     * @see EventsManager#count()
     */
	@Override
	public int count() {
		Query query = em.createQuery("select count(e) from Event e");
		List<?> result = query.getResultList();
		if ( result.isEmpty() ) {
			return 0;
		}
		return ((Number) result.get(0)).intValue();
	}

	/**
     * @see EventsManager#pusha(ISid, IEvent, IEventAtach, Date)
     */
	@Override
	public boolean pusha(ISid sid, IEvent event, IEventAtach eventAtach, Date receivedEventTime) {
    	try {
    		//Check session identity
    		IMobileAgent mobileAgent = userSessionManager.getMobileAgent(sid);
			if ( mobileAgent == null ) {
    			return false;
    		}
    		//TODO: check mobile agent id and user id
    		//Based on session identity

    		Event e = push(event, mobileAgent.getUserId(), (MobileAgent) mobileAgent, receivedEventTime);
    		em.flush();//Get id for event MySQL feature
    		long eventId = e.getId();
	    	EventAtach o = new EventAtach();
	    	o.setEvent(e);
	    	o.setDescription(eventAtach.getDescription());
	    	o.setContent(eventAtach.getContent());
	    	o.setContentCheckSum(eventAtach.getContentCheckSum());
	    	o.setEventId(eventId);
	    	em.persist(o);
	    	e.setEventAtach(o);
	    	em.merge(e);
	    	return true;
    	} catch (Throwable th) {
    		log.error(UNEXPECTED_ERROR, th);
    		return false;
    	}
	}

	/**
     * @see EventsManager#list(String, Date, Date)
     */
	@Override
	public List<IEvent> list(String userName, Date dateFrom, Date dateTo) {
    	List<?> users = em.createQuery("select e from User e where e.name like :userName")
        		.setParameter("userName", userName)
        		.getResultList();
       	if ( users.isEmpty() ) {
       		throw new IllegalStateException("User with name " + userName + " not exists!");
       	}
   		User user = (User) users.get(0);
   		int userId = user.getId();
   		final List<IEvent> eventList;
   		if ( dateFrom != null && dateTo != null) {
	    	List<?> events = em.createQuery("select e from Event e where e.userId = :userId and e.receivedTime between :dateFrom and :dateTo")
	        		.setParameter("userId", userId)
	        		.setParameter("dateFrom", dateFrom)
	        		.setParameter("dateTo", dateTo)
	        		.getResultList();
	    	eventList = (List<IEvent>) events;
   		} else if ( dateFrom != null && dateTo == null ) {
	    	List<?> events = em.createQuery("select e from Event e where e.userId = :userId and e.receivedTime >= :dateFrom")
	        		.setParameter("userId", userId)
	        		.setParameter("dateFrom", dateFrom)
	        		.getResultList();
	    	eventList = (List<IEvent>) events;
   		} else if ( dateFrom == null && dateTo != null ) {
	    	List<?> events = em.createQuery("select e from Event e where e.userId = :userId and e.receivedTime <= :dateTo")
	        		.setParameter("userId", userId)
	        		.setParameter("dateTo", dateTo)
	        		.getResultList();
	    	eventList = (List<IEvent>) events;
   		} else {
	    	List<?> events = em.createQuery("select e from Event e where e.userId = :userId")
	        		.setParameter("userId", userId)
	        		.setMaxResults(100)
	        		.getResultList();
	    	eventList = (List<IEvent>) events;
   		}
		return eventList;
	}

	/**
     * @see EventsManager#classes()
     */
	@Override
	public List<IEventClass> classes() {
		List<IEventClass> eventClassList;
    	List<?> eventClasses = em.createQuery("select e from EventClass e")
        		.getResultList();
    	eventClassList = (List<IEventClass>) eventClasses;
		return eventClassList;
	}

	/**
     * @see EventsManager#lastEvents(int, short, long, Date, int)
     */
	@Override
	public List<IEventWithAtach> lastEvents(int userId, short eventClassId, long mobileAgentId, Date onDate, int maxCount) {
//    	List<?> users = em.createQuery("select e from User e where e.name like :userName")
//        		.setParameter("userName", userId)
//        		.getResultList();
//       	if ( users.isEmpty() ) {
//       		throw new IllegalStateException("User with name " + userId + " not exists!");
//       	}
//   		User user = (User) users.get(0);
//   		int userId = user.getId();
    	List<?> events = em.createQuery("select e from Event e join e.mobileAgent m where e.userId = :userId and e.eventClassId = :eventClassId and m.id = :mobileAgentId and e.receivedTime <= :onDate order by e.receivedTime desc")
        		.setParameter("userId", userId)
        		.setParameter("eventClassId", eventClassId)
        		.setParameter("mobileAgentId", mobileAgentId)
        		.setParameter("onDate", onDate)
        		.setMaxResults(maxCount)
        		.getResultList();
    	if (events.isEmpty()) {
    		return Collections.emptyList();
    	}
		List<IEventWithAtach> eventList = new ArrayList<IEventWithAtach>();
		for (Object event : events) {
			eventList.add((IEventWithAtach) event);
		}
    	return eventList;
	}

	/**
     * @see EventsManager#lastEvents(int, long, Date, int)
     */
	@Override
	public List<IEventWithAtach> lastEvents(int userId, long mobileAgentId, Date onDate, int maxCount) {
//    	List<?> users = em.createQuery("select e from User e where e.name like :userName")
//        		.setParameter("userName", userId)
//        		.getResultList();
//       	if ( users.isEmpty() ) {
//       		throw new IllegalStateException("User with name " + userId + " not exists!");
//       	}
//   		User user = (User) users.get(0);
//   		int userId = user.getId();
    	List<?> events = em.createQuery("select e from Event e join e.mobileAgent m where e.userId = :userId and m.id = :mobileAgentId and e.receivedTime <= :onDate order by e.receivedTime desc")
        		.setParameter("userId", userId)
        		.setParameter("mobileAgentId", mobileAgentId)
        		.setParameter("onDate", onDate)
        		.setMaxResults(maxCount)
        		.getResultList();
    	if (events.isEmpty()) {
    		return Collections.emptyList();
    	}
		List<IEventWithAtach> eventList = new ArrayList<IEventWithAtach>();
		for (Object event : events) {
			eventList.add((IEventWithAtach) event);
		}
    	return eventList;
	}

}
