/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;

import ru.hipdriver.kernel.entity.MobileAgentState;

/**
 * Набор методов облегчающих доступ к некоторым сущностям состояния мобильного агента.
 * @author ivict
 *
 */
public interface MASHelper {
	
	short getEventClassId(String eventClassName);
	
	short getAppStateId(String appStateName);

	short getCarStateId(String carStateName);
	
	/**
	 * Create initial state for mobile agent.
	 * @param mobileAgentId mobile agent identity
	 * @param eventTime received event time
	 * @return new MobileAgentState
	 * @see ru.hipdriver.kernel.entity.MobileAgentState
	 */
	MobileAgentState makeMobileAgentState(long mobileAgentId,
			Date eventTime);	
}
