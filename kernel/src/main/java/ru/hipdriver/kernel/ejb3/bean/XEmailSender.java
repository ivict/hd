/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.bean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceContext;
import javax.sql.rowset.serial.SerialBlob;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.kernel.Invariants;
import ru.hipdriver.kernel.ejb3.EmailSender;
import ru.hipdriver.kernel.entity.EmailMessage;
import ru.hipdriver.kernel.entity.MobileAgentMemoryEntry;
import ru.hipdriver.kernel.entity.MobileAgentMemoryEntryKey;
import ru.hipdriver.kernel.entity.SharedMemoryForAllUsersEntry;

/**
 * Бин инкапсулирующий логику отправки электронной почты.
 * 
 * @author ivict
 * 
 */
@Stateless
@Local(EmailSender.class)
@EJB(name = "java:app/EmailSender", beanInterface = EmailSender.class)
public class XEmailSender implements EmailSender {
	private final Logger log = LoggerFactory.getLogger(XEmailSender.class);

	@PersistenceContext
	private EntityManager em;
	
	@Resource(lookup="java:/HipdriverMail")
	private Session mailSession;

	/**
	 * @see ru.hipdriver.kernel.ejb3.EmailSender#process(long, short, long,
	 *      Date)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean sendMessage(long messageId) {
		try {
			EmailMessage message = em.find(EmailMessage.class, messageId);
			long mobileAgentId = message.getMobileAgentId();
			StringBuilder fromAddr = new StringBuilder();
			String mobileAgentName = getMobileAgentName(mobileAgentId);
			if (mobileAgentName != null) {
				fromAddr.append(mobileAgentName);
			}
			fromAddr.append("<");
			String from = message.getFrom();
			//Get from as standard e-mail
			if (from == null || from.trim().isEmpty()) {
				from = getDefaultFromAddress("dk4aol@gmail.com");
			}
			//TODO: check composition before appending
			fromAddr.append(from);
			fromAddr.append(">");
			boolean isSending;
			if (message.getAtachsCount() == 0) {
				isSending = sendEmail(fromAddr.toString(), message.getTo(), message.getSubject(),
						message.getBody());
			} else {
				isSending = sendEmailWithAtachs(fromAddr.toString(), message.getTo(), message.getSubject(),
						message.getBody(), messageId);
			}
			message.setSendingTime(new Date());
			message.setSendingFail(!isSending);
			em.merge(message);
			return isSending;
		} catch (Throwable th) {
			log.error("Email message sending fail, messageId = " + messageId,
					th);
			throw new EJBException();
			// throw new KernelException(th);
		}
	}

	private String getDefaultFromAddress(String addressIfDefaultNotDefined) {
		String defaultEmailFrom = getSharedMemoryForAllUsersValue(Invariants.DEFAULT_EMAIL_FROM_KEY);
		if (defaultEmailFrom == null) {
			return addressIfDefaultNotDefined;
		}
		return defaultEmailFrom;
	}
	
	protected String getSharedMemoryForAllUsersValue(String key) {
		SharedMemoryForAllUsersEntry sharedMemoryForAllUsersEntry = em.find(SharedMemoryForAllUsersEntry.class, key);
		if (sharedMemoryForAllUsersEntry == null) {
			return null;
		}
		return sharedMemoryForAllUsersEntry.getValue();
	}

	private String getMobileAgentName(long mobileAgentId) {
		MobileAgentMemoryEntryKey mobileAgentMemoryEntryKey = new MobileAgentMemoryEntryKey(mobileAgentId, IMobileAgent.MOBILE_AGENT_NAME_KEY);
		MobileAgentMemoryEntry mobileAgentMemoryEntry = em.find(MobileAgentMemoryEntry.class, mobileAgentMemoryEntryKey);
		if (mobileAgentMemoryEntry == null) {
			return null;
		}
		return mobileAgentMemoryEntry.getValue();
	}

	private boolean sendEmailWithAtachs(String from, String to, String subject, String body, long messageId) {
		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(mailSession);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			if (to != null) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(
						to));
			}

			// Set Subject: header field
			if (subject != null) {
				message.setSubject(subject);
			}

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText(body != null ? body : "");

			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			putAtachsIntoMessage(messageId, multipart);

			// Send the complete message parts
			message.setContent(multipart);

			// Send message
			Transport.send(message);
			return true;
		} catch (Throwable mex) {
			log.error("send-mail-exception", mex);
			return false;
		}
	}
	
	private void putAtachsIntoMessage(long messageId, Multipart multipart) {
		List<?> atachs = em.createQuery("select e.id, e.contentType, e.name, e.fragmentsCount from EmailMessageAtach e join e.emailMessage m where m.id = :messageId ")
				.setParameter("messageId", messageId)
				.getResultList();
		for (Object a : atachs) {
			Atach atach = new Atach(em, a);
			safetyAddAtach(multipart, atach);
		}
	}

	protected void safetyAddAtach(Multipart multipart, Atach atach) {
		try {
			BodyPart messageBodyPart = new MimeBodyPart();
			DataSource atachDatasource = atach.getDataSource();
			messageBodyPart.setDataHandler(new DataHandler(atachDatasource));
			messageBodyPart.setFileName(atach.getName());
			multipart.addBodyPart(messageBodyPart);
		} catch (Throwable th) {
			log.error("add-atach-to-email-error emailAtachId[" + atach.getId() + "]", th);
		}
	}

	private boolean sendEmail(String from, String to, String subject, String body) {
		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(mailSession);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			if (to != null) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(
						to));
			}

			// Set Subject: header field
			if (subject != null) {
				message.setSubject(subject);
			}

			// Now set the actual message
			if (body != null) {
				message.setText(body);
			}

			// Send message
			Transport.send(message);
			return true;
		} catch (Throwable mex) {
			log.error("send-mail-exception", mex);
			return false;
		}
	}

	@Override
	public void markProcessedFail(long messageId, Date sendingTime) {
		EmailMessage message = em.find(EmailMessage.class, messageId);
		message.setSendingTime(sendingTime);
		message.setSendingFail(true);
		em.merge(message);
	}

//	// @see http://www.tutorialspoint.com/java/java_sending_email.htm
//	private boolean sendEmail() {
//		// Recipient's email ID needs to be mentioned.
//		String to = "abcd@gmail.com";
//
//		// Sender's email ID needs to be mentioned
//		String from = "web@gmail.com";
//
//		// Assuming you are sending email from localhost
//		String host = "localhost";
//
//		// Get system properties
//		Properties properties = System.getProperties();
//
//		// Setup mail server
//		properties.setProperty("mail.smtp.host", host);
//
//		// Get the default Session object.
//		Session session = Session.getDefaultInstance(properties);
//
//		try {
//			// Create a default MimeMessage object.
//			MimeMessage message = new MimeMessage(session);
//
//			// Set From: header field of the header.
//			message.setFrom(new InternetAddress(from));
//
//			// Set To: header field of the header.
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
//					to));
//
//			// Set Subject: header field
//			message.setSubject("This is the Subject Line!");
//
//			// Create the message part
//			BodyPart messageBodyPart = new MimeBodyPart();
//
//			// Fill the message
//			messageBodyPart.setText("This is message body");
//
//			// Create a multipar message
//			Multipart multipart = new MimeMultipart();
//
//			// Set text message part
//			multipart.addBodyPart(messageBodyPart);
//
//			// Part two is attachment
//			messageBodyPart = new MimeBodyPart();
//			String filename = "file.txt";
//			DataSource source = new FileDataSource(filename); // <--
//			messageBodyPart.setDataHandler(new DataHandler(source));
//			messageBodyPart.setFileName(filename);
//			multipart.addBodyPart(messageBodyPart);
//
//			// Send the complete message parts
//			message.setContent(multipart);
//
//			// Send message
//			Transport.send(message);
//			return true;
//		} catch (MessagingException mex) {
//			log.error("send-mail-exception", mex);
//			return false;
//		}
//	}

}

class Atach {
	private final long id;
	private final String contentType;
	private final String name;
	private final int contentCount;
	private final EntityManager em;
	
	
	public Atach(EntityManager em, long id, String contentType, String name, int contentCount) {
		super();
		this.em = em;
		this.id = id;
		this.contentType = contentType;
		this.name = name;
		this.contentCount = contentCount;
	}

	public Atach(EntityManager em, Object args) {
		this(em, ((Number)((Object[]) args)[0]).longValue(), String.valueOf(((Object[]) args)[1]), String.valueOf(((Object[]) args)[2]), ((Number)((Object[]) args)[3]).intValue()); 
	}

	public long getId() {
		return id;
	}

	public int getContentCount() {
		return contentCount;
	}
	
	public DataSource getDataSource() throws IOException {
		//TODO: realize input stream instead simple buffered stream;
		byte[] binaryAtach = buildBinaryAtach();
		return new ByteArrayDataSource(new ByteArrayInputStream(binaryAtach), contentType);
	}
	
	private byte[] buildBinaryAtach() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<?> fragments = em.createQuery("select e.fragmentNumber, e.content from EmailMessageAtachFragment e join e.emailMessageAtach a where a.id = :id")
				.setParameter("id", id)
				.getResultList();
		for (Object o : fragments) {
			Fragment fragment = new Fragment(o);
			baos.write(fragment.getContent());
		}
		return baos.toByteArray();
	}

	public String getName() {
		return name;
	}

	public String getContentType() {
		return contentType;
	}
	
}

class Fragment {
	private final int fragmentNumber;
	private final byte[] content;
	
	public Fragment(int fragmentNumber, byte[] content) {
		super();
		this.fragmentNumber = fragmentNumber;
		this.content = content;
	}
	
	public Fragment(Object params) {
		this(((Number) ((Object[]) params)[0]).intValue(),	getSerialBlob((SerialBlob) ((Object[]) params)[1]));
	}

	public int getFragmentNumber() {
		return fragmentNumber;
	}

	public byte[] getContent() {
		return content;
	}
	
	protected static byte[] getSerialBlob(SerialBlob contentBlob) {
		if (contentBlob == null) {
			return null;
		}
		try {
			long length = contentBlob.length();
			return contentBlob.getBytes(1, (int) length);
		} catch (Throwable th) {
			throw new RuntimeException(th);
		}

	}
	
	
	
}
