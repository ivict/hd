/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;


class EmailState {
	final private long messageId;
	final private int atachsCount;
	
	public EmailState(long messageId, int atachsCount) {
		super();
		this.messageId = messageId;
		this.atachsCount = atachsCount;
	}
	
	public EmailState(Object params) {
		this((Long) ((Object[])params)[0], ((Number) ((Object[])params)[1]).intValue());
	}

	public long getMessageId() {
		return messageId;
	}

	public int getAtachsCount() {
		return atachsCount;
	}

}

