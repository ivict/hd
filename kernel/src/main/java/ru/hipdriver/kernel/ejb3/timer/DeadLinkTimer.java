/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;


/**
 * Таймер диагностики мобильных агентов.<br>
 * Мобильные агенты, которые не посылали пинг более часа<br>
 * помечаются как те, с которыми нет связи.<br>
 * Работает с мобильными агентами, версия которых старше 68 включительно.
 * @author ivict
 *
 */
public interface DeadLinkTimer {
	void invoke();
}
