/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import ru.hipdriver.i.ILocation;

/**
 * Эмулятор сервисов для отладки WebUI.<br>
 *  
 * @author ivict
 */
public interface Emulator {
	
	/**
	 * @see UserSessionManager#requestMobileAgentLocation(int, long)
	 */
	ILocation requestMobileAgentLocation(int userId, long mobileAgentId) throws Throwable;

	/**
	 * @see UserSessionManager#activateCarAlarm(int, long)
	 */
	boolean activateCarAlarm(int id, long mobileAgentId);

	/**
	 * @see UserSessionManager#switchOffCarAlarm(int, long)
	 */
	boolean switchOffCarAlarm(int id, long mobileAgentId);
	
}
