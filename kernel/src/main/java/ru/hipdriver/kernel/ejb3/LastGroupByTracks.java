/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;


/**
 * Группировки треков в маршрут.
 * 
 * @author ivict
 *
 */
public interface LastGroupByTracks {

	void createNewGroupByTrack(int userId, long mobileAgentId, long eventId);
	
	long findGroupByTrack(long mobileAgentId);
	
}
