/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Local;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.IMobileAgent;
import ru.hipdriver.i.IMobileAgentConnection;
import ru.hipdriver.kernel.entity.MobileAgentConnection;
import ru.hipdriver.kernel.entity.MobileAgentIceAgentStreamDescriptor;

/**
 * Менеджер соединений с мобильными агентами.
 * @author ivict
 *
 */
@Startup
@Singleton
@Local(MACManager.class)
public class BasicMACManager implements MACManager {
	
	final Logger log = LoggerFactory.getLogger(BasicMACManager.class);
	
	private ConcurrentMap<Long, MobileAgentConnection> connections;

	@PersistenceContext
	private EntityManager em;

	@PostConstruct
	void init() {
		connections = new ConcurrentHashMap<Long, MobileAgentConnection>();
		Date horizon = new Date(System.currentTimeMillis() - 48 * 3600 * 1000);
		List<?> mobileAgentConnectionList =
				em.createQuery("select e from MobileAgentConnection e where e.signInTime >= :horizon")
					.setParameter("horizon", horizon)
					.getResultList();
		for (Object mobileAgentConnection : mobileAgentConnectionList) {
			MobileAgentConnection conn = (MobileAgentConnection) mobileAgentConnection;
			//em.detach(conn);
			long mobileAgentId = conn.getMobileAgentId();
			//Prevent overriding newer info
			connections.putIfAbsent(mobileAgentId, conn);
		}
	}
	
	@PreDestroy
	@Schedule(hour="*", minute="*/15", persistent=false)
	void persist() {
		for (Map.Entry<Long, MobileAgentConnection> e : connections.entrySet()) {
			MobileAgentConnection mobileAgentConnection = e.getValue();
			MobileAgentConnection c = em.find(MobileAgentConnection.class, e.getKey());
			if (c == null) {
				em.persist(mobileAgentConnection);
			} else {
				em.merge(mobileAgentConnection);
			}
		}
		em.flush();
		log.info("Mobile agent connections persisted.");
	}
	

	/**
	 * @see MACManager#updateConnectionInfo(long, String, String, short, String, Date)
	 */
	@Override
	public void updateConnectionInfo(long mobileAgentId, String addr,
			String host, int port, String user, Date signInTime) {
		MobileAgentConnection n = new MobileAgentConnection();
		MobileAgentConnection t = connections.putIfAbsent(mobileAgentId, n);
		MobileAgentConnection mobileAgentConnection = t != null ? t : n;
		mobileAgentConnection.setAddr(addr);
		mobileAgentConnection.setHost(host);
		mobileAgentConnection.setPort(port);
		mobileAgentConnection.setUser(user);
		mobileAgentConnection.setSignInTime(signInTime);
		mobileAgentConnection.setMobileAgentId(mobileAgentId);
	}

	/**
	 * @see MACManager#getConnectionInfo(long)
	 */
	@Override
	public IMobileAgentConnection getConnectionInfo(long mobileAgentId) {
		final MobileAgentConnection mobileAgentConnection = connections.get(mobileAgentId);
		if (mobileAgentConnection == null) {
			return null;//throw new IllegalStateException("Mobile agent not connected");
		}
		
		return new IMobileAgentConnection() {

			@Override
			public String getAddr() {
				return mobileAgentConnection.getAddr();
			}

			@Override
			public void setAddr(String addr) {
				throw new UnsupportedOperationException();
			}

			@Override
			public String getHost() {
				return mobileAgentConnection.getHost();
			}

			@Override
			public void setHost(String host) {
				throw new UnsupportedOperationException();
			}

			@Override
			public int getPort() {
				return mobileAgentConnection.getPort();
			}

			@Override
			public void setPort(int port) {
				throw new UnsupportedOperationException();
			}

			@Override
			public Date getSignInTime() {
				return mobileAgentConnection.getSignInTime();
			}

			@Override
			public void setSignInTime(Date signInTime) {
				throw new UnsupportedOperationException();
			}
			
		};
	}

	/**
	 * @see MACManager#getClientRemoteStreamDescriptor(long, long, String)
	 */
	@Override
	public String getClientRemoteStreamDescriptor(long mobileAgentId, long targetMobileAgentId,
			String localStreamDescriptor) {
		//TODO: more complicated checks (but with same performance:)
		if (mobileAgentId == IMobileAgent.INVALID_ID) {
			throw new IllegalArgumentException("Not valid mobile-agent identity!");
		}
		if (targetMobileAgentId == IMobileAgent.INVALID_ID) {
			throw new IllegalArgumentException("Not valid mobile-agent identity!");
		}
		Date updateTime = new Date();

		MobileAgentIceAgentStreamDescriptor targetMobileAgentIceAgentStreamDescriptor = em.find(MobileAgentIceAgentStreamDescriptor.class, targetMobileAgentId);
		
		//Not update, request exist incoming stream only
		if (localStreamDescriptor == null) {
			//No one not update local stream descriptor
			if (targetMobileAgentIceAgentStreamDescriptor == null) {
				return null;
			}
			//Return waiting stream descriptor from remote agent
			return targetMobileAgentIceAgentStreamDescriptor.getLocalStreamDescriptor();
		}
		
		//Update incoming stream descriptor for target mobile agent
		if (targetMobileAgentIceAgentStreamDescriptor == null) {
			targetMobileAgentIceAgentStreamDescriptor = new MobileAgentIceAgentStreamDescriptor();
			targetMobileAgentIceAgentStreamDescriptor.setMobileAgentId(mobileAgentId);

			targetMobileAgentIceAgentStreamDescriptor.setRemoteStreamDescriptor(localStreamDescriptor);
			targetMobileAgentIceAgentStreamDescriptor.setRemoteStreamDescriptorUpdateTime(updateTime);
			
			em.persist(targetMobileAgentIceAgentStreamDescriptor);
		} else {
			targetMobileAgentIceAgentStreamDescriptor.setRemoteStreamDescriptor(localStreamDescriptor);
			targetMobileAgentIceAgentStreamDescriptor.setRemoteStreamDescriptorUpdateTime(updateTime);
			
			em.merge(targetMobileAgentIceAgentStreamDescriptor);
		}

		//Update outgoing stream descriptor for mobile agent (optional feature)
		MobileAgentIceAgentStreamDescriptor mobileAgentIceAgentStreamDescriptor = em.find(MobileAgentIceAgentStreamDescriptor.class, mobileAgentId);
		if (mobileAgentIceAgentStreamDescriptor == null) {
			mobileAgentIceAgentStreamDescriptor = new MobileAgentIceAgentStreamDescriptor();
			mobileAgentIceAgentStreamDescriptor.setMobileAgentId(mobileAgentId);

			mobileAgentIceAgentStreamDescriptor.setLocalStreamDescriptor(localStreamDescriptor);
			mobileAgentIceAgentStreamDescriptor.setLocalStreamDescriptorUpdateTime(updateTime);
			
			em.persist(mobileAgentIceAgentStreamDescriptor);
		} else {
			mobileAgentIceAgentStreamDescriptor.setLocalStreamDescriptor(localStreamDescriptor);
			mobileAgentIceAgentStreamDescriptor.setLocalStreamDescriptorUpdateTime(updateTime);
			
			em.merge(mobileAgentIceAgentStreamDescriptor);
		}
		
		//Return waiting stream descriptor
		return targetMobileAgentIceAgentStreamDescriptor.getLocalStreamDescriptor();
	}

	/**
	 * @see MACManager#getServerRemoteStreamDescriptor(long, String)
	 */
	@Override
	public String getServerRemoteStreamDescriptor(long mobileAgentId,
			String localStreamDescriptor) {
		//TODO: more complicated checks (but with same performance:)
		if (mobileAgentId == IMobileAgent.INVALID_ID) {
			throw new IllegalArgumentException("Not valid mobile-agent identity!");
		}
		Date updateTime = new Date();
		
		MobileAgentIceAgentStreamDescriptor mobileAgentIceAgentStreamDescriptor = em.find(MobileAgentIceAgentStreamDescriptor.class, mobileAgentId);
		
		//Not update, request exist incoming stream only
		if (localStreamDescriptor == null) {
			if (mobileAgentIceAgentStreamDescriptor == null) {
				return null;
			}
			//Return waiting stream descriptor
			return mobileAgentIceAgentStreamDescriptor.getRemoteStreamDescriptor();
		}
		
		//Update outgoing stream descriptor
		if (mobileAgentIceAgentStreamDescriptor == null) {
			mobileAgentIceAgentStreamDescriptor = new MobileAgentIceAgentStreamDescriptor();
			mobileAgentIceAgentStreamDescriptor.setMobileAgentId(mobileAgentId);

			mobileAgentIceAgentStreamDescriptor.setLocalStreamDescriptor(localStreamDescriptor);
			mobileAgentIceAgentStreamDescriptor.setLocalStreamDescriptorUpdateTime(updateTime);
			
			em.persist(mobileAgentIceAgentStreamDescriptor);
		} else {
			mobileAgentIceAgentStreamDescriptor.setLocalStreamDescriptor(localStreamDescriptor);
			mobileAgentIceAgentStreamDescriptor.setLocalStreamDescriptorUpdateTime(updateTime);
			
			em.merge(mobileAgentIceAgentStreamDescriptor);
		}

		//Return incoming stream descriptor
		return mobileAgentIceAgentStreamDescriptor.getRemoteStreamDescriptor();
	}
	
}
