/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.report;

import java.util.List;

import ru.hipdriver.i.report.ILiveReportRecord;


/**
 * Отчеты для администратора.<br>
 *  
 * @author ivict
 */
public interface AdminReportsGenerator {
	
	List<ILiveReportRecord> userLimits();

	boolean changeUserLimits(ILiveReportRecord record);
	
	List<ILiveReportRecord> stateOfDevices();
	
	List<ILiveReportRecord> sensitivitySettings();
	
	boolean changeSensitivitySettings(ILiveReportRecord record);

}
