/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.timer;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.kernel.ejb3.EmailSender;

/**
 * Таймер отправления электронной почты. Посылает сообщения после сборки.
 * 
 * @author ivict
 * 
 */
@Startup
@Singleton
@LocalBean
public class SendEmails {
	final Logger log = LoggerFactory.getLogger(SendEmails.class);

	@PersistenceContext
	private EntityManager em;
	
	@EJB
	EmailSender emailSender;

	/**
	 * @see TracksTimer#invoke()
	 */
	@Schedule(hour = "*", minute = "*", second = "*/15", persistent = false)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void invoke() {
		//Выбираем всю почту у которой sendingTime == null
		//И установлен признак окончания сборки receivingTime != null

		List<?> emailsStates = em
				.createQuery(
						"select e.id, e.atachsCount from EmailMessage e where (e.receivedTime is not null) and (e.sendingTime is null) order by e.receivedTime asc")
				.getResultList();
		for (Object o : emailsStates) {
			EmailState emailState = new EmailState(o);
			long messageId = emailState.getMessageId();
			try {
				if (emailState.getAtachsCount() == 0) {
					emailSender.sendMessage(messageId);
				} else {
					if (!isComplete(messageId)) {
						continue;
					}
					emailSender.sendMessage(messageId);
				}
			} catch (RuntimeException e) {
				// Check communication link error
				if (findSubstring(e,
						"com.mysql.jdbc.exceptions.jdbc4.CommunicationsException")) {
					log.warn(String
							.format("Retry process of sending e-mail with messageId[%d]",
									messageId));
					continue;
				}
				safetyMarkProcessed(messageId, new Date());
			}
		}
	}

	/**
	 * Check count of fragments
	 * @param messageId
	 * @return true if all fragments persisted in database
	 */
	private boolean isComplete(long messageId) {
//		List<?> stats = em.createQuery("select e.atachsCount, a.atachNumber, a.fragmentsCount, f.fragmentNumber from EmailMessageAtachFragment f join f.emailMessageAtach a join a.emailMessage e where e.id = :messageId")
//			.setParameter("messageId", messageId)
//			.getResultList();
//		if (stats.isEmpty()) {
//			return false;
//		}
//		//TODO: enable multiatachs
//		int fragmentsCount = ((Number)((Object[]) stats.get(0))[2]).intValue();
//		return stats.size() == fragmentsCount;
		return true;//Always be defined after set receivedTime
	}

	private boolean findSubstring(Throwable th, String className) {
		if (th == null) {
			return false;
		}
		if (className == null) {
			return false;
		}
		String throwableClassName = th.getClass().getName();
		if (className.equals(throwableClassName)) {
			return true;
		}
		Throwable parentThrowable = th.getCause();
		while (parentThrowable != null) {
			throwableClassName = parentThrowable.getClass().getName();
			if (className.equals(throwableClassName)) {
				return true;
			}
			Throwable cause = parentThrowable;
			parentThrowable = parentThrowable.getCause();
			if (parentThrowable == cause) {
				break;
			}
		}
		return false;
	}

	private void safetyMarkProcessed(long messageId, Date sendingTime) {
		try {
			emailSender.markProcessedFail(messageId,
					sendingTime);
		} catch (RuntimeException e) {
			log.error("What the terrible failure", e);
		}
	}

}
