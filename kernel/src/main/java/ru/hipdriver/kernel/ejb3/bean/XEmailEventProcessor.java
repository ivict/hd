/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3.bean;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.IEmailObject;
import ru.hipdriver.kernel.ejb3.EmailEventProcessor;
import ru.hipdriver.kernel.ejb3.KernelException;
import ru.hipdriver.kernel.ejb3.MASHelper;
import ru.hipdriver.kernel.entity.EventAtach;
import ru.hipdriver.kernel.entity.EmailMessage;
import ru.hipdriver.kernel.entity.EmailMessageAtach;
import ru.hipdriver.kernel.entity.EmailMessageAtachFragment;
import ru.hipdriver.kernel.entity.MobileAgent;
import ru.hipdriver.kernel.entity.MobileAgentState;

/**
 * Процессор состояний мобильных агентов.
 * @author ivict
 *
 */
@Stateless
@Local(EmailEventProcessor.class)
@EJB(name = "java:app/EmailEventProcessor", beanInterface = EmailEventProcessor.class)
public class XEmailEventProcessor implements EmailEventProcessor {
	private final Logger log = LoggerFactory.getLogger(XEmailEventProcessor.class);
	
	@PersistenceContext
	private EntityManager em;
	
	private ObjectMapper cachedOm;
	
	@EJB
	private MASHelper masHelper;
	/**
	 * @see ru.hipdriver.kernel.ejb3.EmailEventProcessor#process(long, short, long, Date)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void process(long eventId, short eventClassId, long mobileAgentId,
			Date receivedEventTime) {
		try {
			EventAtach eventAtach = em.find(EventAtach.class, eventId);
			if (eventAtach == null) {
				log.warn("Event without atach, eventId = " + eventId);
				return;
			}
			String jsonObject = getJsonString(eventAtach.getContent());
			Class<? extends IEmailObject> emailObjectClass = detectEmailObjectClass(jsonObject);
			if (emailObjectClass == null) {
				log.warn("Can't detect class for email-object, eventId = " + eventId);
				return;
			}
			ObjectMapper om = getObjectMapper();
			IEmailObject emailObject = om.readValue(jsonObject, emailObjectClass);
			if (emailObject instanceof ru.hipdriver.j.EmailMessage) {
				processMessage(mobileAgentId, receivedEventTime, (ru.hipdriver.j.EmailMessage) emailObject);
			} else if (emailObject instanceof ru.hipdriver.j.EmailMessageAtach) {
				processAtach(mobileAgentId, receivedEventTime, (ru.hipdriver.j.EmailMessageAtach) emailObject);
			} else if (emailObject instanceof ru.hipdriver.j.EmailMessageAtachFragment) {
				processFragment(mobileAgentId, receivedEventTime, (ru.hipdriver.j.EmailMessageAtachFragment) emailObject);
			} else {
				log.warn("Processing for IEmailObject of class " + emailObjectClass.getName() + " not implemented.");
			}
			markProcessedEmail(mobileAgentId, receivedEventTime);
		} catch (Throwable th) {
			log.error("Email event processing fail, eventId = " + eventId, th);
			throw new EJBException();
			//throw new KernelException(th);
		}
	}
	
	//TODO:
	//Проставляем по признак окончания сборки receivedTime = receivedEventTime:
	//Если
	//1) atachsCount == 0
	//2) atachsCount == count(EmailMessageAtach) && total-fragments-count == count(EmailMessageAtachFragment)
	private void processFragment(long mobileAgentId, Date receivedEventTime,
			ru.hipdriver.j.EmailMessageAtachFragment fragment) {
		int messageAtachIdentity = fragment.getMessageAtachIdentity();
		EmailMessageAtach atach = findEmailMessageAtach(mobileAgentId, messageAtachIdentity);
		if (atach == null) {
			throw new IllegalStateException(String.format("EmailMessageAtach with identity [%s] not found for fragment", messageAtachIdentity));
		}
		EmailMessageAtachFragment entity = new EmailMessageAtachFragment();
		entity.setEmailMessageAtach(atach);
		int fragmentNumber = fragment.getFragmentNumber();
		entity.setFragmentNumber(fragmentNumber);
		entity.setContent(fragment.getContent());
		entity.setIdentity(fragment.getIdentity());
		em.persist(entity);
		//Set feature end of message receiving
		EmailMessage emailMessage = atach.getEmailMessage();
		int atachNumber = atach.getAtachNumber();
		if (emailMessage.getAtachsCount() == atachNumber + 1
				&& atach.getFragmentsCount() == fragmentNumber + 1) {
			emailMessage.setReceivedTime(receivedEventTime);
			em.persist(emailMessage);
		}
	}

	private EmailMessageAtach findEmailMessageAtach(long mobileAgentId, int identity) {
		List<?> atachesIds = em.createQuery("select e.id from EmailMessageAtach e join e.emailMessage m where m.mobileAgentId = :mobileAgentId and e.identity = :identity order by e.id")
		.setParameter("mobileAgentId", mobileAgentId)
		.setParameter("identity", identity)
		.getResultList();
		if (atachesIds.isEmpty()) {
			return null;
		}
		long atachId = (Long) atachesIds.get(atachesIds.size() - 1);
		return em.find(EmailMessageAtach.class, atachId);
	}

	private void processAtach(long mobileAgentId, Date receivedEventTime,
			ru.hipdriver.j.EmailMessageAtach atach) {
		int messageIdentity = atach.getMessageIdentity();
		EmailMessage message = findEmailMessage(mobileAgentId, messageIdentity);
		if (message == null) {
			throw new IllegalStateException(String.format("EmailMessage with identity [%s] not found for atach", messageIdentity));
		}
		EmailMessageAtach entity = new EmailMessageAtach();
		entity.setEmailMessage(message);
		entity.setAtachNumber(atach.getAtachNumber());
		entity.setContentType(atach.getContentType());
		entity.setName(atach.getName());
		entity.setFragmentsCount(atach.getFragmentsCount());
		entity.setIdentity(atach.getIdentity());
		em.persist(entity);
		em.flush();
	}

	private EmailMessage findEmailMessage(long mobileAgentId, int identity) {
		List<?> messagesIds = em.createQuery("select e.id from EmailMessage e where e.mobileAgentId = :mobileAgentId and e.identity = :identity order by e.id")
		.setParameter("mobileAgentId", mobileAgentId)
		.setParameter("identity", identity)
		.getResultList();
		if (messagesIds.isEmpty()) {
			return null;
		}
		long messageId = (Long) messagesIds.get(messagesIds.size() - 1);
		return em.find(EmailMessage.class, messageId);
	}

	private void processMessage(long mobileAgentId, Date receivedEventTime,
			ru.hipdriver.j.EmailMessage message) {
		EmailMessage entity = new EmailMessage();
		int atachsCount = message.getAtachsCount();
		entity.setAtachsCount(atachsCount);
		entity.setBody(message.getBody());
		entity.setSubject(message.getSubject());
		entity.setFrom(message.getFrom());
		entity.setTo(message.getTo());
		entity.setIdentity(message.getIdentity());
		entity.setMobileAgentId(mobileAgentId);
		//Message without atachs ready to sending
		if (atachsCount == 0) {
			entity.setReceivedTime(receivedEventTime);
		}
		em.persist(entity);
		em.flush();
	}

	private Class<? extends IEmailObject> detectEmailObjectClass(
			String jsonObject) {
		if (jsonObject.indexOf(ru.hipdriver.j.EmailMessage.ATACHS_COUNT) >= 0) {
			return ru.hipdriver.j.EmailMessage.class;
		}
		if (jsonObject.indexOf(ru.hipdriver.j.EmailMessageAtachFragment.FRAGMENT_NUMBER) >= 0) {
			return ru.hipdriver.j.EmailMessageAtachFragment.class;
		}
		if (jsonObject.indexOf(ru.hipdriver.j.EmailMessageAtach.FRAGMENTS_COUNT) >= 0) {
			return ru.hipdriver.j.EmailMessageAtach.class;
		}
		return null;
	}

	/**
	 * @see ru.hipdriver.kernel.ejb3.MobileAgentStateProcessor#markProcessedEmail(long, Date)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void markProcessedEmail(long mobileAgentId, Date eventTime) {
		try {
			//Setup unknown car state, unknown app state, and defauld field values
			MobileAgent mobileAgent = em.find(MobileAgent.class, mobileAgentId);
			if ( mobileAgent == null ) {
				return;
			}
			MobileAgentState mobileAgentState = em.find(MobileAgentState.class, mobileAgentId);
			if ( mobileAgentState == null ) {
				mobileAgentState = masHelper.makeMobileAgentState(mobileAgentId, eventTime);
				mobileAgentState.setAppStateId(masHelper.getAppStateId("unknown"));
				mobileAgentState.setCarStateId(masHelper.getCarStateId("unknown"));
				em.persist(mobileAgentState);
				return;
			}
			mobileAgentState.setLastEmailTime(eventTime);//PROCESSED FLAG
			em.merge(mobileAgentState);
		} catch (Throwable th) {
			throw new KernelException(th);
		}
	}
	
	//На многопоточность забиваем, поскольку пара лишних телодвижений погоды не сделают
	//Сам ObjectMapper вполне себе защищеный
	private ObjectMapper getObjectMapper() {
		if ( cachedOm == null ) {
			cachedOm = new ObjectMapper();
		}
		return cachedOm;
	}
	
	private String getJsonString(byte[] content) {
		if ( content == null ) {
			return "{}";//Empty json object
		}
		return new String(content);
	}

}
