/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel.ejb3;

import java.util.Date;
import java.util.List;

import ru.hipdriver.i.ICarLocation;

/**
 * Процессор состояния мобильных агентов.
 * @author ivict
 *
 */
public interface MobileAgentStateProcessor {
	void process(long eventId, short eventClassId, long mobileAgentId,
			Date eventTime, String jsonObject);
	void markProcessedState(long mobileAgentId, Date eventTime);
	void markProcessedAlertTrack(long mobileAgentId, Date eventTime);
}
