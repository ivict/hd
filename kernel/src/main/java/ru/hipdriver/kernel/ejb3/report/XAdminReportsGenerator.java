package ru.hipdriver.kernel.ejb3.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.hipdriver.i.IMobileAgentProfile;
import ru.hipdriver.i.report.ILiveReportRecord;
import ru.hipdriver.kernel.entity.MobileAgent;
import ru.hipdriver.kernel.entity.UserLimit;

/**
 * Session Bean implementation class BasicUserSessionManager
 */
@Stateless(mappedName = "admin-reports-generator")
@Local(AdminReportsGenerator.class)
@EJB(name = "java:app/AdminReportsGenerator", beanInterface = AdminReportsGenerator.class)
public class XAdminReportsGenerator implements AdminReportsGenerator {
	final Logger log = LoggerFactory.getLogger(XAdminReportsGenerator.class);

	@PersistenceContext
	private EntityManager em;

	/**
	 * @see ru.hipdriver.kernel.ejb3.report.AdminReportsGenerator#userLimits()
	 */
	@Override
	public List<ILiveReportRecord> userLimits() {
		List<ILiveReportRecord> result = new ArrayList<ILiveReportRecord>();
		List<?> records = em
				.createQuery(
						"select u.id, u.name, u.email, ul.timeLimited, ul.maxDevices, count(ma.id), ul.usedAsReferral from MobileAgent ma join ma.user u join u.userLimit ul where ma.disabled = false group by u")
				.getResultList();
		for (Object record : records) {
			Object[] args = (Object[]) record;
			Record entry = new Record(args[0]);
			entry.putField("name", args[1]);
			entry.putField("email", args[2]);
			entry.putField("time-limited", args[3]);
			entry.putField("max-devices", args[4]);
			entry.putField("count-devices", args[5]);
			entry.putField("used-as-referral", args[6]);
			result.add(entry);
		}
		return result;
	}

	/**
	 * @see ru.hipdriver.kernel.ejb3.report.AdminReportsGenerator#changeUserLimits(ILiveReportRecord)
	 */
	@Override
	public boolean changeUserLimits(ILiveReportRecord record) {
		if (record == null) {
			throw new IllegalArgumentException();
		}
		Object key = record.getKey();
		if (key == null) {
			return false;
		}
		int userId = ((Number) key).intValue();
		Map<String, String> data = record.getData();
		if (data == null) {
			return false;
		}

		UserLimit userLimit = em.find(UserLimit.class, userId);
		if (userLimit == null) {
			return false;
		}

		boolean isLimitsChanged = false;

		String timeLimitedText = data.get("time-limited");
		if (timeLimitedText != null) {
			boolean timeLimited = "true".equalsIgnoreCase(timeLimitedText);
			userLimit.setTimeLimited(timeLimited);
			isLimitsChanged = true;
		}
		String usedAsReferralText = data.get("used-as-referral");
		if (usedAsReferralText != null) {
			boolean usedAsReferral = "true"
					.equalsIgnoreCase(usedAsReferralText);
			userLimit.setUsedAsReferral(usedAsReferral);
			isLimitsChanged = true;
		}
		if (!isLimitsChanged) {
			return false;
		}
		em.merge(userLimit);
		return true;
	}

	/**
	 * @see ru.hipdriver.kernel.ejb3.report.AdminReportsGenerator#stateOfDevices()
	 */
	@Override
	public List<ILiveReportRecord> stateOfDevices() {
		List<ILiveReportRecord> result = new ArrayList<ILiveReportRecord>();
		List<?> records = em
				.createQuery(
						"select u.id, ma.id, u.name, ma.name, s.name, cs.name, mas.batteryPct, mas.time, mas.lastAlertTrackTime, mas.lastEmailTime, mas.versionName from MobileAgent ma join ma.mobileAgentState mas join ma.user u join mas.appState s join mas.carState cs where ma.disabled = false")
				.getResultList();
		for (Object record : records) {
			Object[] args = (Object[]) record;
			Record entry = new Record(args[1]);
			entry.putField("user-id", args[0]);
			entry.putField("mobile-agent-id", args[1]);
			entry.putField("user-name", args[2]);
			entry.putField("mobile-agent-name", args[3]);
			entry.putField("app-state-name", args[4]);
			entry.putField("car-state-name", args[5]);
			entry.putField("battery-pct", args[6]);
			entry.putField("mobile-agent-state-time", args[7]);
			entry.putField("last-alert-track-time", args[8]);
			entry.putField("last-email-time", args[9]);
			entry.putField("mobile-agent-version-name", args[10]);
			result.add(entry);
		}
		return result;
	}

	/**
	 * @see ru.hipdriver.kernel.ejb3.report.AdminReportsGenerator#sensitivitySettings()
	 */
	@Override
	public List<ILiveReportRecord> sensitivitySettings() {
		List<ILiveReportRecord> result = new ArrayList<ILiveReportRecord>();
		List<?> records = em
				.createQuery(
						"select u.id, ma.id, u.name, ma.name, p.asdMaxAngle, p.asdMaxAcceleration, p.asdMaxJerk, p.enableEnergySavingMode, ma.disabled, mas.time, mas.versionName from MobileAgent ma left join ma.mobileAgentState mas join ma.user u left join ma.mobileAgentProfile p")
				.getResultList();
		for (Object record : records) {
			Object[] args = (Object[]) record;
			Record entry = new Record(args[1]);
			entry.putField("user-id", args[0]);
			entry.putField("mobile-agent-id", args[1]);
			entry.putField("user-name", args[2]);
			entry.putField("mobile-agent-name", args[3]);
			entry.putField("asd-max-angle", args[4] == null ? IMobileAgentProfile.DEFAULT_ASD_MAX_ANGLE : args[4]);
			entry.putField("asd-max-acceleration", args[5] == null ? IMobileAgentProfile.DEFAULT_ASD_MAX_ACCELERATION : args[5] );
			entry.putField("asd-max-jerk", args[6] == null ? IMobileAgentProfile.DEFAULT_ASD_MAX_JERK : args[6]);
			entry.putField("enable-energy-saving-mode", args[7] == null ? IMobileAgentProfile.DEFAULT_ENABLE_ENERGY_SAVING_MODE : args[7]);
			entry.putField("mobile-agent-disabled", args[8]);
			entry.putField("mobile-agent-state-time", args[9] == null ? "" : args[9]);
			entry.putField("mobile-agent-version-name", args[10] == null ? "" : args[10]);
			result.add(entry);
		}
		return result;
	}

	/**
	 * @see ru.hipdriver.kernel.ejb3.report.AdminReportsGenerator#changeUserLimits(ILiveReportRecord)
	 */
	@Override
	public boolean changeSensitivitySettings(ILiveReportRecord record) {
		if (record == null) {
			throw new IllegalArgumentException();
		}
		Object key = record.getKey();
		if (key == null) {
			return false;
		}
		long mobileAgentId = ((Number) key).longValue();
		Map<String, String> data = record.getData();
		if (data == null) {
			return false;
		}

		MobileAgent mobileAgent = em.find(MobileAgent.class, mobileAgentId);
		if (mobileAgent == null) {
			return false;
		}

		boolean isMobileAgentChanged = false;

		String disabledText = data.get("mobile-agent-disabled");
		if (disabledText != null) {
			boolean disabled = "true".equalsIgnoreCase(disabledText);
			mobileAgent.setDisabled(disabled);
			isMobileAgentChanged = true;
		}
		if (!isMobileAgentChanged) {
			return false;
		}
		em.merge(mobileAgent);
		return true;
	}

}

class Record implements ILiveReportRecord {

	private final static String READ_ONLY_LIVE_REPORT_RECORD = "Read only live report record.";

	private final Object key;

	private final Map<String, String> data;

	public Record(Object key) {
		super();
		this.key = key;
		this.data = new HashMap<String, String>();
	}

	@Override
	public Object getKey() {
		return key;
	}

	@Override
	public void setKey(Object key) {
		throw new IllegalStateException(READ_ONLY_LIVE_REPORT_RECORD);
	}

	@Override
	public Map<String, String> getData() {
		return data;
	}

	@Override
	public void setData(Map<String, String> data) {
		throw new IllegalStateException(READ_ONLY_LIVE_REPORT_RECORD);
	}

	public void putField(String fieldName, Object fieldValue) {
		if (fieldValue == null) {
			data.put(fieldName, null);
			return;
		}
		data.put(fieldName, String.valueOf(fieldValue));
	}

}