/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved. 
 */
package ru.hipdriver.kernel;

import static ru.hipdriver.kernel.Invariants.SERVICES;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Главное приложение.
 * @author ivict
 */
@ApplicationPath(SERVICES)
public class Services extends Application {

}
