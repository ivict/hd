/**
 * Copyright(C) 2014 Hipdriver.ru
 * All rights reserved.
 */
package ru.hipdriver.kernel;

import java.util.regex.Pattern;

/**
 * @author ivict
 */
public class Invariants {

	private Invariants() { }
	
	public static final String SERVICES = "services";
	
	public static final String ROUTES = "routes";
	
	public static final String ROOT = "/";
	
	/**
	 * Утилитарная информация.
	 * /kernel/services
	 */
	public static final String COUNT_PATH = "count";
	
	/**
	 * Сервис /kernel/services/events
	 */
	public static final String EVENTS = "events";
	public static final String EVENTS_PUSH_PATH = "push";
	public static final String EVENTS_PUSH_PATHA = "pusha";
	public static final String EVENTS_COUNT_PATH = "count";
	public static final String EVENTS_LIST_PATH = "list";//{user-name,date-from,date-to}";
	public static final String EVENTS_CLASSES_PATH = "classes";//{id};
	public static final String EVENTS_LAST_TEN_EVENTS_PATH = "last-ten-events";//{user-id,event-class-id,mobile-agent-id,on-date}";

	/**
	 * Сервис /kernel/services/users
	 */
	public static final String USERS = "users";
	public static final String USERS_COUNT_PATH = "count";//{service-profile-name}
	public static final String USERS_SERVICE_PROFILES_PATH = "service-profiles";//{user-id}
	public static final String USERS_MOBILE_AGENTS_STATES_PATH = "mobile-agents-states";//{user-id, user-name, mobile-agent-id, mobile-agent-name}
	public static final String USERS_COOL_PATH = "cool";
	public static final String USERS_APP_STATES_PATH = "app-states";//{user-id}optional
	public static final String USERS_CAR_STATES_PATH = "car-states";//{user-id}optional
	public static final String USERS_WHERE_ARE_YOU_PATH = "where-are-you";//{user-id, mobile-agent-id}
	public static final String USERS_ACTIVATE_CAR_ALARM_PATH = "activate-car-alarm";//{user-id, mobile-agent-id}
	public static final String USERS_SWITCH_OFF_CAR_ALARM_PATH = "switch-off-car-alarm";//{user-id, mobile-agent-id}
	public static final String USERS_ALERT_TRACKS_PATH = "alert-tracks";//{user-id, mobile-agent-id}
	public static final String USERS_MOBILE_AGENTS_COUNT_PATH = "mobile-agents-count";
	public static final String USERS_MOBILE_AGENTS_EDITOR_PATH = "mobile-agents-editor";//{user-id, mobile-agent-id, name, alert-phone-number}
	public static final String USERS_MOBILE_AGENTS_PROPERTIES_PATH = "mobile-agents-properties";//{user-id, mobile-agent-id}
	public static final String USERS_TIME_LIMITED_VERSION_PATH = "time-limited-version";//[user-id1, user-id2 ...]
	public static final String USERS_USED_AS_REFERRAL_PATH = "used-as-referral";//[user-id1, user-id2 ...]
	public static final String USERS_LIMITS = "limits";//{user-id}
	
	/**
	 * Сервис /kernel/services/sign
	 */
	public static final String SIGN = "sign";
	public static final String SIGN_UP_PATH = "up";
	public static final String SIGN_UPW_PATH = "upw";//POST{user-name,e-mail,password-hash,id}
	public static final String SIGN_IN_PATH = "in";
	
	/**
	 * Сервис /kernel/services/admin
	 */
	public static final String ADMIN = "admin";
	public static final String ADMIN_DELETE_MOBILE_AGENT = "delete-mobile-agent";//{user-id, mobile-agent-id}
	public static final String ADMIN_CHANGE_MOBILE_AGENT_PROFILE = "change-mobile-agent-profile";//{user-id, mobile-agent-id}
	public static final String ADMIN_CHANGE_INCOMMING_MESSAGE = "change-incomming-message";//{user-id, mobile-agent-id}
	public static final String ADMIN_CHANGE_FIRST_SIGN_UP_INCOMMING_MESSAGE = "change-first-sign-up-incomming-message";//{message}
	public static final String ADMIN_CHANGE_CURRENT_ANDROID_APPLICATION_VERSION = "change-current-android-application-version";//{version}
	//Live report `user-limits`
	public static final String ADMIN_LRV_USER_LIMITS = "lrv-user-limits";
	public static final String ADMIN_LRE_USER_LIMITS = "lre-user-limits";//{user-id, time-limited}
	public static final String ADMIN_LRV_STATE_OF_DEVICES = "lrv-state-of-devices";
	public static final String ADMIN_LRV_SENSITIVITY_SETTINGS = "lrv-sensitivity-settings";
	public static final String ADMIN_LRE_SENSITIVITY_SETTINGS = "lre-sensitivity-settings";
	
	/**
	 * Сервис /pub/ping (PUT requests only)
	 */
	public static final String PING = "/pub/ping";
	
	/**
	 * Сервис /pub/pubkey-update (PUT requests only)
	 */
	public static final String PUBKEY_UPDATE = "/pub/pubkey-update";
	
	/**
	 * Сервис /pub/pubkey (POST requests only)
	 */
	public static final String PUBKEY = "/pub/pubkey";
	
	/**
	 * Сервис /pub/mymas, my mobile-agents (POST requests only)
	 */
	public static final String MYMAS = "/pub/mymas";
	
	/**
	 * Сервис /pub/host (POST requests only)
	 */
	public static final String HOST = "/pub/host";
	
	/**
	 * Сервис /pub/mas, my mobile-agents (POST requests only)
	 */
	public static final String MAS = "/pub/mas";
	
	/**
	 * Сервис /pub/emap, edit mobile agent settings (POST requests only)
	 */
	public static final String EMAP = "/pub/emap";
	
	/**
	 * Сервис /pub/map, mobile agent settings (POST requests only)
	 */
	public static final String MAP = "/pub/map";
	
	/**
	 * Сервис /pub/cheml, check-emails (POST requests only)
	 */
	public static final String CHEML = "/pub/cheml";
	
	/**
	 * Сервис /pub/hash, get-encoded-password-hash-for-email (POST requests only)
	 */
	public static final String HASH = "/pub/hash";
	
	/**
	 * Горизонт архивирования по умолчанию.
	 */
	public static final long ARCHIVE_HORIZON_IN_MILLIS = 31536000000L;//One year
	
	/**
	 * Шаблоны сообщений Server-Side.
	 */
	public static final String UNEXPECTED_ERROR = "Unexpected error";

	/**
	 * Shared memory for all users.
	 */
	public final static String FIRST_SIGNUP_INCOMING_MESSAGE_KEY = "first-signup-incoming-message";
	public final static String CURRENT_ANDROID_APPLICATION_VERSION_KEY = "current-android-application-version";
	public final static String DEFAULT_EMAIL_FROM_KEY = "default-email-from";

	/**
	 * Regexp pattern for version name.
	 */
	public static final Pattern VERSION_NAME_PATTERN = Pattern.compile(".*\\[(.*)\\].*");

	public static final String PING_EXECUTOR = "ping-executor";
	public static final String PUBKEY_UPDATE_EXECUTOR = "pubkey-update-executor";
	
	//Copy-past from android.util.Patterns#EMAIL_ADDRESS
	public static final Pattern EMAIL_ADDRESS = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+"
    );	
}
